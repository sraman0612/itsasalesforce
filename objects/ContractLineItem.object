<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>AddContractLine</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>AddContractLine</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>AddContractLine</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>SFS_Contract_Line_Item_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>SFS_Contract_Line_Item_Record_Page</content>
        <formFactor>Small</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SFS_SALI_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>SFS_SALI_Compact_Layout</fullName>
        <fields>LineItemNumber</fields>
        <fields>ServiceContractId</fields>
        <fields>AssetId</fields>
        <label>SFS SALI Compact Layout</label>
    </compactLayouts>
    <enableFeeds>false</enableFeeds>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Clone_From__c</fullName>
        <externalId>false</externalId>
        <label>Clone From</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Oracle_Product_Number__c</fullName>
        <description>Product line for service agreement and SALI integration</description>
        <externalId>false</externalId>
        <formula>PricebookEntry.Product2.Product_Line__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Oracle Product Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_24_HR_Response_Time__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>24 HR Response Time</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SFS_Billable_Flag__c</fullName>
        <description>Used for outbound integrations</description>
        <externalId>false</externalId>
        <formula>IF(OR(SFS_Status__c  = &quot;Active&quot;,SFS_Status__c = &quot;Expired&quot;,SFS_Status__c = &quot;Draft&quot;) , &quot;Y&quot;, &quot;Y&quot;)</formula>
        <label>Billable Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Billing_Method_Flag__c</fullName>
        <description>Outbound Integrations</description>
        <externalId>false</externalId>
        <formula>IF((OR(ISPICKVAL(SFS_Billing_Method__c, &apos;Milestone&apos;), ISPICKVAL(SFS_Billing_Method__c,&apos;Schedule Based&apos;))) , &apos;Y&apos;, &apos;N&apos;)</formula>
        <label>Billing Method Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Billing_Method__c</fullName>
        <externalId>false</externalId>
        <label>Billing Method</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Schedule Based</fullName>
                    <default>false</default>
                    <label>Schedule Based</label>
                </value>
                <value>
                    <fullName>Service Based</fullName>
                    <default>false</default>
                    <label>Service Based</label>
                </value>
                <value>
                    <fullName>Milestone</fullName>
                    <default>false</default>
                    <label>Milestone</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SFS_CPQ_Currency__c</fullName>
        <externalId>false</externalId>
        <label>CPQ Currency</label>
        <length>3</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_CPQ_Display_First_Invoice_Date__c</fullName>
        <externalId>false</externalId>
        <formula>DATETIMEVALUE(SFS_CPQ_First_Invoice_Date__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>First Invoice Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>SFS_CPQ_External_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>used for Service Agreement Integration</description>
        <externalId>true</externalId>
        <label>CPQ External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>SFS_CPQ_First_Invoice_Date__c</fullName>
        <externalId>false</externalId>
        <label>CPQ First Invoice Date</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Cancel_PM__c</fullName>
        <defaultValue>false</defaultValue>
        <description>SIF - 52</description>
        <externalId>false</externalId>
        <label>Cancel PM</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SFS_Chargeable_Flag__c</fullName>
        <description>Outbound Integrations</description>
        <externalId>false</externalId>
        <formula>IF(OR(SFS_Status__c = &quot;Active&quot;,SFS_Status__c = &quot;Expired&quot;,SFS_Status__c = &quot;Draft&quot;) , &quot;N&quot;, &quot;N&quot;)</formula>
        <label>Chargeable Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Cloned_ContractLineItem_Id__c</fullName>
        <externalId>false</externalId>
        <label>Cloned ContractLineItem Id</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Entitlement_Process_Type__c</fullName>
        <externalId>false</externalId>
        <label>Uptime Category</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>24 HR Uptime</fullName>
                    <color>#FF0000</color>
                    <default>false</default>
                    <label>24 HR Uptime</label>
                </value>
                <value>
                    <fullName>96% Uptime</fullName>
                    <default>false</default>
                    <label>96% Uptime</label>
                </value>
                <value>
                    <fullName>97% Uptime</fullName>
                    <default>false</default>
                    <label>97% Uptime</label>
                </value>
                <value>
                    <fullName>Customized Uptime</fullName>
                    <default>false</default>
                    <label>Customized Uptime</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SFS_External_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>SFS_First_Invoice_Plan_Date__c</fullName>
        <externalId>false</externalId>
        <label>First Invoice Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SFS_Initial_Work_Scope__c</fullName>
        <externalId>false</externalId>
        <label>Initial Work Scope</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Integration_Response__c</fullName>
        <externalId>false</externalId>
        <label>Integration Response</label>
        <length>131072</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>SFS_Integration_Status__c</fullName>
        <externalId>false</externalId>
        <label>Integration Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>APPROVED</fullName>
                    <default>false</default>
                    <label>APPROVED</label>
                </value>
                <value>
                    <fullName>ERROR</fullName>
                    <default>false</default>
                    <label>ERROR</label>
                </value>
                <value>
                    <fullName>NA</fullName>
                    <default>true</default>
                    <label>NA</label>
                </value>
                <value>
                    <fullName>SYNCING</fullName>
                    <default>false</default>
                    <label>SYNCING</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SFS_Invoice_Plan_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Invoice Plan Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>SFS_Invoice_Plan_Day__c</fullName>
        <externalId>false</externalId>
        <label>Invoice Plan Day</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>First Day of Month</fullName>
                    <default>true</default>
                    <label>First Day of Month</label>
                </value>
                <value>
                    <fullName>Last Day of Month</fullName>
                    <default>false</default>
                    <label>Last Day of Month</label>
                </value>
                <value>
                    <fullName>1</fullName>
                    <default>false</default>
                    <label>1</label>
                </value>
                <value>
                    <fullName>2</fullName>
                    <default>false</default>
                    <label>2</label>
                </value>
                <value>
                    <fullName>3</fullName>
                    <default>false</default>
                    <label>3</label>
                </value>
                <value>
                    <fullName>4</fullName>
                    <default>false</default>
                    <label>4</label>
                </value>
                <value>
                    <fullName>5</fullName>
                    <default>false</default>
                    <label>5</label>
                </value>
                <value>
                    <fullName>6</fullName>
                    <default>false</default>
                    <label>6</label>
                </value>
                <value>
                    <fullName>7</fullName>
                    <default>false</default>
                    <label>7</label>
                </value>
                <value>
                    <fullName>8</fullName>
                    <default>false</default>
                    <label>8</label>
                </value>
                <value>
                    <fullName>9</fullName>
                    <default>false</default>
                    <label>9</label>
                </value>
                <value>
                    <fullName>10</fullName>
                    <default>false</default>
                    <label>10</label>
                </value>
                <value>
                    <fullName>11</fullName>
                    <default>false</default>
                    <label>11</label>
                </value>
                <value>
                    <fullName>12</fullName>
                    <default>false</default>
                    <label>12</label>
                </value>
                <value>
                    <fullName>13</fullName>
                    <default>false</default>
                    <label>13</label>
                </value>
                <value>
                    <fullName>14</fullName>
                    <default>false</default>
                    <label>14</label>
                </value>
                <value>
                    <fullName>15</fullName>
                    <default>false</default>
                    <label>15</label>
                </value>
                <value>
                    <fullName>16</fullName>
                    <default>false</default>
                    <label>16</label>
                </value>
                <value>
                    <fullName>17</fullName>
                    <default>false</default>
                    <label>17</label>
                </value>
                <value>
                    <fullName>18</fullName>
                    <default>false</default>
                    <label>18</label>
                </value>
                <value>
                    <fullName>19</fullName>
                    <default>false</default>
                    <label>19</label>
                </value>
                <value>
                    <fullName>20</fullName>
                    <default>false</default>
                    <label>20</label>
                </value>
                <value>
                    <fullName>21</fullName>
                    <default>false</default>
                    <label>21</label>
                </value>
                <value>
                    <fullName>22</fullName>
                    <default>false</default>
                    <label>22</label>
                </value>
                <value>
                    <fullName>23</fullName>
                    <default>false</default>
                    <label>23</label>
                </value>
                <value>
                    <fullName>24</fullName>
                    <default>false</default>
                    <label>24</label>
                </value>
                <value>
                    <fullName>25</fullName>
                    <default>false</default>
                    <label>25</label>
                </value>
                <value>
                    <fullName>26</fullName>
                    <default>false</default>
                    <label>26</label>
                </value>
                <value>
                    <fullName>27</fullName>
                    <default>false</default>
                    <label>27</label>
                </value>
                <value>
                    <fullName>28</fullName>
                    <default>false</default>
                    <label>28</label>
                </value>
                <value>
                    <fullName>29</fullName>
                    <default>false</default>
                    <label>29</label>
                </value>
                <value>
                    <fullName>30</fullName>
                    <default>false</default>
                    <label>30</label>
                </value>
                <value>
                    <fullName>31</fullName>
                    <default>false</default>
                    <label>31</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SFS_Invoice_Plan_Frequency__c</fullName>
        <externalId>false</externalId>
        <label>Invoice Plan Frequency</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Annually</fullName>
                    <default>false</default>
                    <label>Annually</label>
                </value>
                <value>
                    <fullName>Semi Annually</fullName>
                    <default>false</default>
                    <label>Semi Annually</label>
                </value>
                <value>
                    <fullName>Quarterly</fullName>
                    <default>false</default>
                    <label>Quarterly</label>
                </value>
                <value>
                    <fullName>3x per Year</fullName>
                    <default>false</default>
                    <label>3x per Year</label>
                </value>
                <value>
                    <fullName>Monthly</fullName>
                    <default>false</default>
                    <label>Monthly</label>
                </value>
                <value>
                    <fullName>Weekly</fullName>
                    <default>false</default>
                    <label>Weekly</label>
                </value>
                <value>
                    <fullName>Daily</fullName>
                    <default>false</default>
                    <label>Daily</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SFS_Oracle_Billing_Method__c</fullName>
        <description>Used for outbound integrations. Service Agreement with SALI Ready to Bill Flag.</description>
        <externalId>false</externalId>
        <formula>IF( 
   AND( 
        OR(ISPICKVAL(ServiceContract.SFS_Status__c , &quot;Active&quot;) , ISPICKVAL( 
         ServiceContract.SFS_Status__c , &quot;Expired&quot;), ISPICKVAL( 
        ServiceContract.SFS_Status__c , &quot;Draft&quot;)),
         OR( ISPICKVAL(SFS_Billing_Method__c , &quot;Milestone&quot;) , ISPICKVAL(SFS_Billing_Method__c , 
        &quot;Schedule Based&quot;))
         ) 
, &quot;Y&quot;, &quot;N&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Oracle Billing Method</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Oracle_End_Date__c</fullName>
        <description>Change Date format to Oracle format. Outbound Integration</description>
        <externalId>false</externalId>
        <formula>IF(DAY(EndDate) &lt; 10, &quot;0&quot; + text(DAY( EndDate)) +&apos;-&apos;+ CASE( MONTH( EndDate) , 1, &quot;JAN&quot;, 2, &quot;FEB&quot;, 3, &quot;MAR&quot;, 4, &quot;APR&quot;, 5, &quot;MAY&quot;, 6, &quot;JUN&quot;, 7, &quot;JUL&quot;, 8, &quot;AUG&quot;, 9, &quot;SEP&quot;, 10, &quot;OCT&quot;, 11, &quot;NOV&quot;, &quot;DEC&quot;) +&apos;-&apos;+ Text(YEAR( EndDate))
, text(DAY( EndDate)) +&apos;-&apos;+ CASE( MONTH( EndDate) , 1, &quot;JAN&quot;, 2, &quot;FEB&quot;, 3, &quot;MAR&quot;, 4, &quot;APR&quot;, 5, &quot;MAY&quot;, 6, &quot;JUN&quot;, 7, &quot;JUL&quot;, 8, &quot;AUG&quot;, 9, &quot;SEP&quot;, 10, &quot;OCT&quot;, 11, &quot;NOV&quot;, &quot;DEC&quot;) +&apos;-&apos;+ Text(YEAR( EndDate))
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Oracle End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Oracle_Quote_Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Oracle Quote Line Item</label>
        <referenceTo>cafsl__Oracle_Quote_Line_Item__c</referenceTo>
        <relationshipLabel>Service Agreement Line Items</relationshipLabel>
        <relationshipName>Service_Agreement_Line_Items</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SFS_Part_Number__c</fullName>
        <externalId>false</externalId>
        <label>Part Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Part_Number_from_Product__c</fullName>
        <description>Created to pull products part number as per SIF-3968</description>
        <externalId>false</externalId>
        <formula>PricebookEntry.Product2.Part_Number__c</formula>
        <label>Part Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Product_Code__c</fullName>
        <externalId>false</externalId>
        <formula>PricebookEntry.Product2.SFS_Product_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Product_Description__c</fullName>
        <description>Cross object reference for Outbound Integration.</description>
        <externalId>false</externalId>
        <formula>PricebookEntry.Product2.Description</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Product_Line_Formula__c</fullName>
        <description>Product line for service agreement and SALI integration</description>
        <externalId>false</externalId>
        <formula>IF(Oracle_Product_Number__c=NULL,&apos;26680&apos;,Oracle_Product_Number__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Line Formula</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Service Agreement Line Items</relationshipLabel>
        <relationshipName>Service_Agreement_Line_Items</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SFS_Quote_Line_Item_Number__c</fullName>
        <externalId>false</externalId>
        <label>Quote Line Item Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Ready_To_Bill__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Checkbox to determine if SALI is ready to bill.</description>
        <externalId>false</externalId>
        <label>Ready To Bill</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SFS_Ready_to_Bill_Flag__c</fullName>
        <description>Outbound Integrations</description>
        <externalId>false</externalId>
        <formula>IF(SFS_Ready_To_Bill__c = true,&quot;Y&quot;,&quot;N&quot;)</formula>
        <label>Ready to Bill Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Ready_to_Distribute1__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Ready to Distribute</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SFS_Ready_to_Distribute_Flag__c</fullName>
        <description>Outbound Integrations</description>
        <externalId>false</externalId>
        <formula>IF(AND(SFS_Billing_Method_Flag__c = &apos;Y&apos;,OR(SFS_Status__c = &quot;Active&quot;,SFS_Status__c = &quot;Expired&quot;,SFS_Status__c = &quot;Draft&quot;)), &quot;Y&quot;, &quot;N&quot;)</formula>
        <label>Ready to Distribute Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Ready_to_Distribute__c</fullName>
        <description>Used for outbound integrations</description>
        <externalId>false</externalId>
        <formula>IF(SFS_Ready_to_Distribute1__c = true, &quot;Y&quot;, &quot;N&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Ready to Distribute</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Recurring_Adjustment__c</fullName>
        <externalId>false</externalId>
        <label>Recurring Adjustment</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>SFS_Recurring_Amount__c</fullName>
        <description>Stores recurring amount from CPQ</description>
        <externalId>false</externalId>
        <label>Recurring Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>SFS_Related_Charge__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Related Charge</label>
        <referenceTo>CAP_IR_Charge__c</referenceTo>
        <relationshipLabel>Service Agreement Line Items</relationshipLabel>
        <relationshipName>Service_Agreement_Line_Items</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SFS_Renewal_Escalator_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Renewal Escalator Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SFS_SALI_Oracle_Start_Date__c</fullName>
        <externalId>false</externalId>
        <formula>IF(DAY(StartDate) &lt; 10, &quot;0&quot; + text(DAY( StartDate)) +&apos;-&apos;+ CASE( MONTH( StartDate) , 1, &quot;JAN&quot;, 2, &quot;FEB&quot;, 3, &quot;MAR&quot;, 4, &quot;APR&quot;, 5, &quot;MAY&quot;, 6, &quot;JUN&quot;, 7, &quot;JUL&quot;, 8, &quot;AUG&quot;, 9, &quot;SEP&quot;, 10, &quot;OCT&quot;, 11, &quot;NOV&quot;, &quot;DEC&quot;) +&apos;-&apos;+ Text(YEAR( StartDate )), text(DAY( StartDate)) +&apos;-&apos;+ CASE( MONTH( StartDate) , 1, &quot;JAN&quot;, 2, &quot;FEB&quot;, 3, &quot;MAR&quot;, 4, &quot;APR&quot;, 5, &quot;MAY&quot;, 6, &quot;JUN&quot;, 7, &quot;JUL&quot;, 8, &quot;AUG&quot;, 9, &quot;SEP&quot;, 10, &quot;OCT&quot;, 11, &quot;NOV&quot;, &quot;DEC&quot;) +&apos;-&apos;+ Text(YEAR( StartDate )))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Oracle Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Send_feed__c</fullName>
        <externalId>false</externalId>
        <formula>IF( EndDate = TODAY() +7, true, false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Send feed</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SFS_Service_Agreement_Record_Type__c</fullName>
        <externalId>false</externalId>
        <formula>ServiceContract.RecordType.Name</formula>
        <label>Service Agreement Record Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Siebel_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>false</externalId>
        <label>Siebel Id</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>SFS_Status__c</fullName>
        <description>Custom Status field</description>
        <externalId>false</externalId>
        <formula>IF( SFS_Cancel_PM__c ,&quot;Expired&quot; ,
IF( ISPICKVAL(ServiceContract.SFS_Status__c, &quot;Draft&quot;) , &quot;Draft&quot;,
 IF( ISPICKVAL(ServiceContract.SFS_Status__c, &quot;Pending&quot;) , &quot;Draft&quot;,
 IF( ISPICKVAL(ServiceContract.SFS_Status__c, &quot;APPROVED&quot;) , &quot;Active&quot;, 
 IF( ISPICKVAL(ServiceContract.SFS_Status__c, &quot;Expired&quot;), &quot;Expired&quot; ,
 IF( ISPICKVAL(ServiceContract.SFS_Status__c, &quot;Pending Closed&quot;) , &quot;Expired&quot;,
 IF( ISPICKVAL(ServiceContract.SFS_Status__c, &quot;Closed&quot;) , &quot;Closed&quot;, &quot;&quot;
  ) ) ) ) ) ) )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Store_Product_Name__c</fullName>
        <externalId>false</externalId>
        <formula>SFS_Product__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Store Product Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SFS_Track_End_Date__c</fullName>
        <externalId>false</externalId>
        <formula>TODAY()  + 7</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Track End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SFS_Unit_Price__c</fullName>
        <externalId>false</externalId>
        <label>Sales Price</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <profileSearchLayouts>
        <fields>LINEITEM.NAME</fields>
        <fields>SERVICE_CONTRACT</fields>
        <fields>PRODUCT.NAME</fields>
        <fields>LINEITEM.STARTDATE</fields>
        <fields>LINEITEM.ENDDATE</fields>
        <fields>LINEITEM.STATUS</fields>
        <fields>ASSET.NAME</fields>
        <profileName>SFS Service Technician</profileName>
    </profileSearchLayouts>
    <profileSearchLayouts>
        <fields>LINEITEM.NAME</fields>
        <fields>SERVICE_CONTRACT</fields>
        <fields>PRODUCT.NAME</fields>
        <fields>LINEITEM.STARTDATE</fields>
        <fields>LINEITEM.ENDDATE</fields>
        <fields>LINEITEM.STATUS</fields>
        <fields>ASSET.NAME</fields>
        <profileName>System Administrator</profileName>
    </profileSearchLayouts>
    <searchLayouts>
        <searchResultsAdditionalFields>LINEITEM.NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SERVICE_CONTRACT</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>PRODUCT.NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>LINEITEM.STARTDATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>LINEITEM.ENDDATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>LINEITEM.STATUS</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ASSET.NAME</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>SFS_Check_Active</fullName>
        <active>true</active>
        <errorConditionFormula>AND( OR( ISCHANGED( SFS_24_HR_Response_Time__c ), ISCHANGED( SFS_Entitlement_Process_Type__c ) ),
ISPICKVAL(ServiceContract.SFS_Status__c, &apos;APPROVED&apos;)
)</errorConditionFormula>
        <errorDisplayField>SFS_24_HR_Response_Time__c</errorDisplayField>
        <errorMessage>PC 24hr Response Time or Uptime Category cannot be edited on a active agreement</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>SFS_Check_Closed</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
$Permission.DataMigrationUser == False,
ISPICKVAL(ServiceContract.SFS_Status__c, &quot;Closed&quot;),
$Profile.Name &lt;&gt; &quot;System Administrator&quot;
)</errorConditionFormula>
        <errorMessage>Closed Service Agreement Line Item cannot be updated</errorMessage>
    </validationRules>
</CustomObject>