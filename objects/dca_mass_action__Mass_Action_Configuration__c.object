<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>dca_mass_action__Default_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>dca_mass_action__Default_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>dca_mass_action__Active__c</fields>
        <fields>dca_mass_action__Schedule_Frequency__c</fields>
        <fields>dca_mass_action__Source_Type__c</fields>
        <fields>dca_mass_action__Target_Type__c</fields>
        <fields>dca_mass_action__Target_Action_Name__c</fields>
        <fields>dca_mass_action__Last_Run_Completed_Date__c</fields>
        <label>Default Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>A configuration of inputs (reports, list views, queries) and actions (process builder, flows, apex) that can be scheduled.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>dca_mass_action__Active__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If active then action can be ran and scheduled. Inactive actions will not run and have their scheduled job aborted.</description>
        <externalId>false</externalId>
        <inlineHelpText>If active then action can be ran and scheduled. Inactive actions will not run and have their scheduled job aborted.</inlineHelpText>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>dca_mass_action__Batch_Size__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of source records to run through the target action. Correlates to the apex batch job size. Can be between 1 and 200.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of source records to run through the target action. Correlates to the apex batch job size. Can be between 1 and 200.</inlineHelpText>
        <label>Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>dca_mass_action__DeveloperName__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>A unique name to reference this record by without knowing its ID. This is particularly useful when invoking a configuration on-demand from Process Builder or Flow.</description>
        <externalId>true</externalId>
        <inlineHelpText>A unique name to reference this record by without knowing its ID.</inlineHelpText>
        <label>Mass Action Configuration Unique Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Last_Run_Completed_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>When the last run of this configuration completed. It is updated when the batch job finishes. Use this field to control when you chain other configurations together. For example, Process Builder may monitor when this field value changes and if the unique name is &quot;Config1&quot; then can decide to automatically run &quot;Config2&quot; to ensure &quot;Config2&quot; only runs after &quot;Config1&quot; completes.</description>
        <externalId>false</externalId>
        <inlineHelpText>When the last run of this configuration completed. It is updated when the batch job finishes. Use this field to control when you chain other configurations together. For example, to ensure &quot;Config2&quot; only runs after &quot;Config1&quot; completes.</inlineHelpText>
        <label>Last Run Completed Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>dca_mass_action__Last_Run_Completed_With_Errors__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates if the last run of this configuration completed with errors. It is updated when the batch job finishes. This field is checked if the last run of this configuration created log records reporting any errors, otherwise it is unchecked. Use this field with the Last Run Completed Date in Process Builder to control when to automatically run other configurations conditionally upon a configuration completing successfully or not. For example, run &quot;Config2&quot; only if &quot;Config1&quot; completed without errors.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates if the last run of this configuration completed with errors. It is updated when the batch job finishes. This field is checked if the last run of this configuration created log records reporting any errors, otherwise it is unchecked.</inlineHelpText>
        <label>Last Run Completed With Errors</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>dca_mass_action__Named_Credential__c</fullName>
        <deprecated>false</deprecated>
        <description>The Named Credential that provides authentication to Salesforce REST API when invoking target actions. This will be the context user for the record updates.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Named Credential that provides authentication to Salesforce REST API when invoking target actions. This will be the context user for the record updates.</inlineHelpText>
        <label>Named Credential</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Schedule_Cron__c</fullName>
        <deprecated>false</deprecated>
        <description>Schedule Cron Expression https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_scheduler.htm</description>
        <externalId>false</externalId>
        <inlineHelpText>(advanced) Specify your own cron expression to schedule this action</inlineHelpText>
        <label>Schedule Cron</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Schedule_DayOfMonth__c</fullName>
        <deprecated>false</deprecated>
        <description>Day of Month component of cron schedule expression. https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_scheduler.htm</description>
        <externalId>false</externalId>
        <inlineHelpText>Day of Month component of cron schedule expression.</inlineHelpText>
        <label>Schedule Day</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Schedule_DayOfWeek__c</fullName>
        <deprecated>false</deprecated>
        <description>Weekday component of cron schedule expression. https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_scheduler.htm</description>
        <externalId>false</externalId>
        <inlineHelpText>Weekday component of cron schedule expression.</inlineHelpText>
        <label>Schedule Weekday</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Schedule_Frequency__c</fullName>
        <deprecated>false</deprecated>
        <description>On Demand = not scheduled
Scheduled = user scheduled via simple options
Custom = user provided own cron expression</description>
        <externalId>false</externalId>
        <inlineHelpText>How often will this action run?</inlineHelpText>
        <label>Schedule Frequency</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Manual</fullName>
                    <default>true</default>
                    <label>Manual</label>
                </value>
                <value>
                    <fullName>Scheduled</fullName>
                    <default>false</default>
                    <label>Scheduled</label>
                </value>
                <value>
                    <fullName>Custom</fullName>
                    <default>false</default>
                    <label>Custom</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>dca_mass_action__Schedule_HourOfDay__c</fullName>
        <deprecated>false</deprecated>
        <description>Hour of Day component of cron schedule expression. https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_scheduler.htm</description>
        <externalId>false</externalId>
        <inlineHelpText>Hour of Day component of cron schedule expression. Salesforce uses the time zone of the user who schedules the job</inlineHelpText>
        <label>Schedule Hour</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Schedule_MinuteOfHour__c</fullName>
        <deprecated>false</deprecated>
        <description>Minute component of cron schedule expression. https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_scheduler.htm</description>
        <externalId>false</externalId>
        <inlineHelpText>Minute component of cron schedule expression.</inlineHelpText>
        <label>Schedule Minute</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Schedule_MonthOfYear__c</fullName>
        <deprecated>false</deprecated>
        <description>Month of Year component of cron schedule expression. https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_scheduler.htm</description>
        <externalId>false</externalId>
        <inlineHelpText>Month of Year component of cron schedule expression.</inlineHelpText>
        <label>Schedule Month</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Schedule_SecondOfMinute__c</fullName>
        <deprecated>false</deprecated>
        <description>Second component of cron schedule expression. https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_scheduler.htm</description>
        <externalId>false</externalId>
        <inlineHelpText>Second component of cron schedule expression.</inlineHelpText>
        <label>Schedule Second</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Source_List_View_ID__c</fullName>
        <deprecated>false</deprecated>
        <description>List View that provides the source records. Applicable when Source Type is List View.</description>
        <externalId>false</externalId>
        <inlineHelpText>Choose a list view that identifies the records to process.</inlineHelpText>
        <label>Source List View ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Source_Report_Column_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>Report column name that holds unique record values for sorting and paginating. Needed for workaround with Reports API to process more than 2,000 rows. Applicable when Source Type is Report.</description>
        <externalId>false</externalId>
        <inlineHelpText>Report column name that holds unique record values for sorting and paginating. Needed for workaround with Reports API to process more than 2,000 rows. Applicable when Source Type is Report.</inlineHelpText>
        <label>Source Report Column Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Source_Report_ID__c</fullName>
        <deprecated>false</deprecated>
        <description>Report that provides the source records. Applicable when Source Type is Report.</description>
        <externalId>false</externalId>
        <inlineHelpText>Report that provides the source records. Applicable when Source Type is Report.</inlineHelpText>
        <label>Source Report ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Source_Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Where source records come from to perform actions on, like reports or list views.</description>
        <externalId>false</externalId>
        <inlineHelpText>Where source records come from to perform actions on, like reports or list views.</inlineHelpText>
        <label>Source Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Report</fullName>
                    <default>false</default>
                    <label>Report</label>
                </value>
                <value>
                    <fullName>ListView</fullName>
                    <default>false</default>
                    <label>List View</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>dca_mass_action__Target_Action_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>Name of the action to process the source records (e.g. Process Builder, Flow, Invocable Apex class, Quick Action, Email Alert, etc). Should be blank for when Target Type is Workflow Rules.</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of the action to process the source records (e.g. Process Builder, Flow, Invocable Apex class, Quick Action, Email Alert, etc). Should be blank for when Target Type is Workflow Rules.</inlineHelpText>
        <label>Target Action Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Target_SObject_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Target SObject Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dca_mass_action__Target_Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Type of action to perform on the source records.</description>
        <externalId>false</externalId>
        <inlineHelpText>Type of action to perform on the source records.</inlineHelpText>
        <label>Target Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Flow</fullName>
                    <default>false</default>
                    <label>Process Builder &amp; Flows</label>
                </value>
                <value>
                    <fullName>Workflow</fullName>
                    <default>false</default>
                    <label>Workflow Rules</label>
                </value>
                <value>
                    <fullName>QuickAction</fullName>
                    <default>false</default>
                    <label>Quick Actions</label>
                </value>
                <value>
                    <fullName>EmailAlert</fullName>
                    <default>false</default>
                    <label>Email Alerts</label>
                </value>
                <value>
                    <fullName>Apex</fullName>
                    <default>false</default>
                    <label>Apex</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Mass Action Configuration</label>
    <listViews>
        <fullName>dca_mass_action__All</fullName>
        <columns>NAME</columns>
        <columns>dca_mass_action__Active__c</columns>
        <columns>dca_mass_action__Schedule_Frequency__c</columns>
        <columns>dca_mass_action__Source_Type__c</columns>
        <columns>dca_mass_action__Target_Type__c</columns>
        <columns>dca_mass_action__Target_Action_Name__c</columns>
        <columns>dca_mass_action__Last_Run_Completed_Date__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Mass Action Configuration Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Mass Action Configurations</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>dca_mass_action__Active__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>dca_mass_action__Schedule_Frequency__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>dca_mass_action__Source_Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>dca_mass_action__Target_Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>dca_mass_action__Target_Action_Name__c</customTabListAdditionalFields>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <lookupDialogsAdditionalFields>dca_mass_action__Active__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>dca_mass_action__Schedule_Frequency__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>dca_mass_action__Source_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>dca_mass_action__Target_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>dca_mass_action__Target_Action_Name__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>dca_mass_action__Active__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>dca_mass_action__Schedule_Frequency__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>dca_mass_action__Source_Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>dca_mass_action__Target_Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>dca_mass_action__Target_Action_Name__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>dca_mass_action__Active__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>dca_mass_action__Schedule_Frequency__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>dca_mass_action__Source_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>dca_mass_action__Target_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>dca_mass_action__Target_Action_Name__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
