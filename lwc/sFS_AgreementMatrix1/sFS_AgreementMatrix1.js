import { LightningElement,api,wire, track } from 'lwc';
import getYears from '@salesforce/apex/SFS_Agreement_Matrix1.getYears';
import getAssets from '@salesforce/apex/SFS_Agreement_Matrix1.getAssets';
import getWorkRules from '@salesforce/apex/SFS_Agreement_Matrix1.getWorkRules';
import getMaintenanceAsset from '@salesforce/apex/SFS_Agreement_Matrix1.getMaintenanceAsset';
import { updateRecord } from 'lightning/uiRecordApi';
import ID_FIELD from '@salesforce/schema/MaintenanceWorkRule.Id';
import Date_FIELD from '@salesforce/schema/MaintenanceWorkRule.NextSuggestedMaintenanceDate';
import MaintennaceDate_FIELD from '@salesforce/schema/MaintenanceWorkRule.SFS_Maintenance_Date__c';
import Utils from 'c/sFS_AgreementMatrixUtils';
const columns = [
    {
        label: 'Asset',fieldName: 'assetId',type: 'text',cellAttributes: { alignment: 'Center' },
        label: 'MA Number',fieldName: 'maNumber',type: 'text',cellAttributes: { alignment: 'Center' },
        label: 'Work Type Name',fieldName: 'workTypeName',type: 'text',cellAttributes: { alignment: 'Center' },
        label: 'Work Orders',fieldName: 'workOrders',type: 'any',cellAttributes: { alignment: 'Center' },
        label: 'Svc Agreement Start Date',fieldName: 'agreementStartDate',type: 'Date',cellAttributes: { alignment: 'Center' },
        label: 'Svc Agreement End Date',fieldName: 'agreementEndDate',type: 'Date',cellAttributes: { alignment: 'Center' }
    }];

export default class SFS_AgreementMatrix1 extends LightningElement {
    Years;
    allMonths;
    records=[];
    months=[
        { Name: ''},
        { Name: 'JAN'},
        { Name: 'FEB'},
        { Name: 'MAR'},
        { Name: 'APR'},
        { Name: 'MAY'},
        { Name: 'JUN'},
        { Name: 'JUL'},
        { Name: 'AUG'},
        { Name: 'SEP'},
        { Name: 'OCT'},
        { Name: 'NOV' },
        { Name: 'DEC'}];
    insideGetAsset=false;
    insideGetYears=false;
    insideGetWorkRules=false;
    insideGetmAsset=false;
    maintenanceAssets=[];
    assetRecords;
    @api recordId;
    @track isLoading=false;
    @track loadCmp=false;
    @track dataExists=false;

    connectedCallback(){
        getYears({svcAgreementId:this.recordId})
            .then(result => {
                this.Years = result;
                
            })
            this.allMonths= [
                { Name: 'JAN', Code: '01'},
                { Name: 'FEB', Code: '02'},
                { Name: 'MAR', Code: '03'},
                { Name: 'APR', Code: '04'},
                { Name: 'MAY', Code: '05'},
                { Name: 'JUN', Code: '06'},
                { Name: 'JUL', Code: '07'},
                { Name: 'AUG', Code: '08'},
                { Name: 'SEP', Code: '09'},
                { Name: 'OCT', Code: '10'},
                { Name: 'NOV', Code: '11'},
                { Name: 'DEC', Code: '12'}
             
            ];    

           this.handleLoad(); 

        getAssets({svcAgreementId:this.recordId}) 
          .then(result => {
            this.assetRecords = result;
              
           })
        
        getMaintenanceAsset({svcAgreementId:this.recordId}) 
        .then(result => {
            this.maintenanceAssets=result;
        
        })
    }

    handleLoad(){
        getWorkRules({svcAgreementId:this.recordId}) 
            .then(result => {
                //console.log('43--'+JSON.stringify(result));
                    this.records=result.map(row=>{
                        return{...row,assetId:row.ParentMaintenanceRecord.AssetId,
                            maNumber:row.ParentMaintenanceRecord.MaintenanceAssetNumber,
                            //workTypeName:row.WorkType.Name,
                            agreementStartDate:row.ParentMaintenanceRecord.MaintenancePlan.ServiceContract.StartDate,
                            agreementEndDate:row.ParentMaintenanceRecord.MaintenancePlan.ServiceContract.EndDate,
                            workOrders:row.WorkOrderLineItems,
                            locked:false};
                    });
                    if(this.records.length>0){
                        this.dataExists=true;
                    }
                    this.checkDragConditions();
                    //console.log('48---'+JSON.stringify(this.records));
             }).catch(error=>{
                 //console.log('91--'+JSON.stringify(error));
            }).finally(()=>{
                 this.loadCmp=true;
            });
    }
    handleReload(){
        this.loadCmp=false;
        this.handleIsLoading(true);
        this.handleLoad();
        this.handleIsLoading(false);
    }
    checkDragConditions(){
        this.records.forEach(record =>{
            if(record.workOrders){
                if(record.workOrders.length>0){
                    record.locked=true;
                }
            }
        });
    }
    workRuleId;
    svcAgreementStartDate;
    svcAgreementEndDate;
    nextSuggestedMaintenanceDate;
    generationHorizonDate;
    workOrdersLength;
    handleItemDrag(event){
        console.log('61 In Drag--'+event.detail);
        let temp=event.detail.split('&');
        this.workRuleId=temp[0];
        this.svcAgreementStartDate=temp[1];
        this.svcAgreementEndDate=temp[2];
        this.nextSuggestedMaintenanceDate=temp[3];
        this.generationHorizonDate=temp[4];
        this.workOrdersLength=temp[5];
        console.log('90--'+this.workOrdersLength);
    }
    month;
    year;
    handleItemDrop(event){
        event.preventDefault();
        console.log('66 In Drop--'+event.detail);
        let temp=event.detail.split('-');
        this.month=temp[0];
        this.year=temp[1];
        this.updateHandler();
    }

    monthValue;
    dateValue;
    updateHandler(){
        //Setting month value where the record is dropped
        for(let i=0;i<this.allMonths.length;i++){
            if(this.allMonths[i].Name==this.month){
                this.monthValue=this.allMonths[i].Code;
                break;
            }
        }
            //this.dateValue=new Date(this.year,this.monthValue,1).toISOString().slice(0, 10);
            //this.monthValue=parseInt(this.monthValue)+1;
            this.dateValue=new Date(this.year,this.monthValue,0).toLocaleDateString('en-CA');
            console.log('Updated Date...'+this.dateValue);
                console.log("List monthMatrix1" + this.month);
                console.log("List monthValueMatrix1" + this.monthValue);
            if(this.nextSuggestedMaintenanceDate!=this.dateValue){
                if(this.workOrdersLength==0){
                    if(this.dateValue>=this.svcAgreementStartDate && this.dateValue<=this.svcAgreementEndDate){
                        if(!(this.year==this.svcAgreementEndDate.substring(0,4) && this.monthValue==this.svcAgreementEndDate.substring(5,7))){
                            if(!((this.dateValue>=this.generationHorizonDate && this.dateValue<this.nextSuggestedMaintenanceDate)|| 
                                (this.monthValue== this.generationHorizonDate.substring(5,7) && this.year== this.generationHorizonDate.substring(0,4)))){
                                    const fields = {};
                                    fields[ID_FIELD.fieldApiName]=this.workRuleId;
                                    fields[Date_FIELD.fieldApiName]= this.dateValue;
                                    fields[MaintennaceDate_FIELD.fieldApiName]=this.dateValue;
                                    const recordInput ={fields};
                                    this.loadCmp=false;
                                    this.handleIsLoading(true);
                                    updateRecord(recordInput)
                                    .then(()=>{
                                        console.log("Updated Successfully");
                                        Utils.showToast(this,'Success','Updated Successfully','success');
                                        this.handleReload();
                                    }).catch(error=>{
                                        console.log('105'+error);
                                        Utils.showToast(this,'Error','Unable to update record','error');
                                    }).finally(()=>{
                                        this.handleIsLoading(false);
                                    });
                                }
                                else{
                                    Utils.showToast(this,'Error','Can\'t move in the generation horizon','error');
                                }
                        }   
                        else{
                            Utils.showToast(this,'Error','Can\'t move to the last month of service agreement','error');
                        }
                    }
                    else{
                        Utils.showToast(this,'Error','Can\'t move Maintenance Work Rule out of service agreement period','error');
                    }
                }else{
                    Utils.showToast(this,'Error','Can\'t update Maintenance Work Rule if Work Order exists','error');
                }
            }                         
    }

    handleIsLoading(isLoading) {
        this.isLoading = isLoading;
    }
    get calcWidth(){
        let len = this.allMonths.length;
        return `width: calc(1320px/ ${len})`
    }

}