import {LightningElement, api, wire} from "lwc";
import getCSRecommendationProducts from "@salesforce/apex/RecommendedProducts.getCSRecommendationProducts";
import getIRLogoUrl from "@salesforce/apex/RecommendedProducts.getIRLogoUrl";
import RecommendedProductsModal from "c/recommendedProductsModal";
import RECOMMENDED_PARTS from "@salesforce/label/c.Recommended_Parts";
import RECOMMENDATION_GUIDE from "@salesforce/label/c.Recommendation_Guide";
import FILTER_HELP_TEXT from "@salesforce/label/c.Filter_HelpText";
import SERIAL_NUMBER from "@salesforce/label/c.SerialNumber";
import MODEL_NUMBER from "@salesforce/label/c.Model_Number";
import CLEAR_FILTER from "@salesforce/label/c.Clear_Filter";
import assetMsgService from '@salesforce/messageChannel/AssetMessageChannel__c';
import spinnerMsgService from '@salesforce/messageChannel/SpinnerMessageChannel__c';
import {subscribe, publish, MessageContext} from 'lightning/messageService';

export default class RecommendedProductsList extends LightningElement {
	labels = {
		RECOMMENDED_PARTS,
		RECOMMENDATION_GUIDE,
		FILTER_HELP_TEXT,
		SERIAL_NUMBER,
		MODEL_NUMBER,
		CLEAR_FILTER
	};

	assetDetail;
	crossSellProductsList;
	//Number of items retrieved
	noOfRecommendedItems;
	frameTypeId;
	//showModal = false;

	@wire(MessageContext)
	messageContext;
	subscription;

	connectedCallback() {
		this.subscribeToChannel();
	}

	handleCompleteAction() {
		console.log("handleCloseModal");
		this.dispatchEvent(new CustomEvent("completeaction"));
	}

	@api
	handleOpenClick(assetDetail) {
		console.log("handleOpenClick");
		console.log("assetDetails>>>" + JSON.stringify(assetDetail));
		this.assetDetail = assetDetail;
		this.frameTypeId = assetDetail.asset.CTS_IOT_Frame_Type__c;
		this.loadCSRecommendationProducts();
	}

	async loadCSRecommendationProducts() {
		try {
			let irLogoUrl = await getIRLogoUrl();
			let crossSellProducts = await getCSRecommendationProducts({
				frameTypeId: this.frameTypeId
			});
			//console.log("JSON.stringify(myCrossSellProducts)>>" + JSON.stringify(crossSellProducts));
			this.tarnsformCrossSellProducts(crossSellProducts);
			this.handleCompleteAction();
			this.publishHandler();
			RecommendedProductsModal.open({
				label: this.labels.RECOMMENDED_PARTS,
				crossSellProductsList: crossSellProducts,
				assetDetail: this.assetDetail,
				labels: this.labels,
				irLogoUrl: irLogoUrl,
				onaddtocart: (e) => {
					// stop further propagation of the event
					e.stopPropagation();
					this.handleAddToCart(e.detail);
				}
			});
		} catch (error) {
			this.handleCompleteAction();
			window.console.log(error);
		}
	}

	tarnsformCrossSellProducts(myCrossSellProducts) {
		myCrossSellProducts.forEach((element) => {
			element.description
				?.replace(/&#39;/g, "'")
				?.replace("&lt;/p&gt;", "")
				?.replace("&lt;p&gt;", "")
				?.replace("&quot;", '"')
				?.replace("&lt;h1&gt;", "")
				?.replace("&lt;/h1&gt;", "")
				?.replace("&lt;br&gt;", "");
		});
	}

	subscribeToChannel() {
		//console.log('in sub');
		this.subscription = subscribe(this.messageContext, assetMsgService, (message) => this.handleOpenClick(message));
	}

	publishHandler() {
		const payload = {
			loading : false
		}
		publish(this.messageContext, spinnerMsgService, payload);
	}

	/*handleAddToCart(evtDetails) {
		console.log("evtDetails>>>" + JSON.stringify(evtDetails));
	}*/
}