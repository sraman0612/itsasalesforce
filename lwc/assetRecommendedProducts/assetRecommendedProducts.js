import {LightningElement, api, wire} from 'lwc';
import isPermissionAssigned from "@salesforce/apex/B2BCartController.checkBuyerPermission";
import getAsset from "@salesforce/apex/RecommendedProducts.getAssetData";
import assetMsgService from '@salesforce/messageChannel/AssetMessageChannel__c';
import spinnerMsgService from '@salesforce/messageChannel/SpinnerMessageChannel__c';
import {publish, subscribe, MessageContext} from 'lightning/messageService';

export default class AssetRecommendedProducts extends LightningElement {
    @api recordId;
    @api userId;
    userHasPermission = false;
    assetRecord;
    loading = false;

    @wire(MessageContext)
    messageContext;
    subscription;

    connectedCallback() {
        //console.log(this.recordId);
        this.subscribeToChannel();
        this.getAssetData();
    }

    isPermissionAssignedToUser(){
        isPermissionAssigned({userId: this.userId, frameTypeId: this.assetRecord.CTS_IOT_Frame_Type__c})
            .then((result) => {
                //console.log(result);
                this.userHasPermission = result;
            })
            .catch((error) => {
                const errorMessage = error.body.message;
                console.log(errorMessage);
            });
    }

    getAssetData(){
        getAsset({assetId: this.recordId})
            .then((result) => {
                //console.log(result);
                this.assetRecord = result;
                this.isPermissionAssignedToUser();
            })
            .catch((error) => {
                console.log(error);
            });
    }

    handleOnclick() {
        this.loading = true;
        this.publishHandler();
    }

    publishHandler() {
        const payload = {
            asset : this.assetRecord
        }
        publish(this.messageContext, assetMsgService, payload);
    }


    subscribeToChannel() {
        //console.log('in sub asset');
        this.subscription = subscribe(this.messageContext, spinnerMsgService, (message) => this.handleSpinner());
    }

    handleSpinner() {
        this.loading = false;
    }
}