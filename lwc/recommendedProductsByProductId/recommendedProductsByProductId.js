import { LightningElement, api } from "lwc";
import getCSRecommendationProductsByProductId from "@salesforce/apex/RecommendedProducts.getCSRecommendationProductsByProductId";
import ToastContainer from "lightning/toastContainer";
import { refreshCartSummary } from "commerce/cartApi";
//import { ShowToastEvent } from "lightning/platformShowToastEvent";

import RECOMMENDED_PARTS from "@salesforce/label/c.Recommended_Parts";
//import CART_SUCCESS_TITLE from "@salesforce/label/c.Cart_Success_Title";

export default class RecommendedProductsByProductId extends LightningElement {
	label = {
		RECOMMENDED_PARTS
		//CART_SUCCESS_TITLE
	};
	@api recordId;
	crossSellProductsList;
	//Number of items retrieved
	noOfRecommendedItems;
	productId;
	connectedCallback() {
		getCSRecommendationProductsByProductId({ productId: this.recordId })
			.then((result) => {
				//console.log("result>>>" + JSON.stringify(result));
				this.crossSellProductsList = result;
				this.noOfRecommendedItems = this.crossSellProductsList?.length;
			})
			.catch((error) => {
				console.log("error", error);
				this.noOfRecommendedItems = 0;
			});
	}

	get columnSize() {
		return 2;
	}
	handleAddToCart(event) {
		//console.log("Event>>>" + JSON.stringify(event));
		refreshCartSummary();
		const toastContainer = ToastContainer.instance();
		toastContainer.maxShown = 1;
		toastContainer.toastPosition = "top-center";
	}
}