import { LightningElement,api, track } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import Utils from 'c/sFS_AgreementMatrixUtils';
export default class SFS_AgreementMatrixList1 extends LightningElement {
    @api records;
    @api month;
    @api year;
    @api asset;
    @api assetId;
    @api massets;
    @track recordExists=false;
    @track isClicked =false;
    @track displaySave;
    @track outOfAgreementPeriod=false;
    @track workScope='';
    @track formattedDate;
    mAssetId='';
    workScopesList=['0', '1', '2','4','6','8','12','16', '24', '48'];

    allMonths= [
        { Name: 'JAN', Code: '01'},
        { Name: 'FEB', Code: '02'},
        { Name: 'MAR', Code: '03'},
        { Name: 'APR', Code: '04'},
        { Name: 'MAY', Code: '05'},
        { Name: 'JUN', Code: '06'},
        { Name: 'JUL', Code: '07'},
        { Name: 'AUG', Code: '08'},
        { Name: 'SEP', Code: '09'},
        { Name: 'OCT', Code: '10'},
        { Name: 'NOV', Code: '11'},
        { Name: 'DEC', Code: '12'}
    ];

    connectedCallback(){
        let monthNumber;
        for(let i=0;i<this.allMonths.length;i++){
            if(this.allMonths[i].Name==this.month){
                monthNumber=this.allMonths[i].Code;
                break;
            }
        }
        let startMonth, startYear,endMonth,endYear;
        if(this.records.length>0){
            startMonth=this.records[0].agreementStartDate.substring(5,7);
            startYear=this.records[0].agreementStartDate.substring(0,4);
            endMonth=this.records[0].agreementEndDate.substring(5,7);
            endYear=this.records[0].agreementEndDate.substring(0,4);
        }
        if((monthNumber<startMonth && this.year==startYear) || (monthNumber>=endMonth && this.year==endYear)){
            this.outOfAgreementPeriod=true;
        }
        for(let i=0;i<this.records.length;i++){
            if(this.month == this.records[i].SFS_Month__c && this.year == this.records[i].SFS_Year__c && this.assetId==this.records[i].assetId){
                this.recordExists=true;
                break;
            }
        }
    }
   
    handleItemDrag(evt){
        const event = new CustomEvent('itemdrag', {
            detail: evt.detail
        })
        this.dispatchEvent(event)
    }
    handleDragOver(evt){
        evt.preventDefault()
    }
    handleDrop(evt){
        const event = new CustomEvent('itemdrop', {
            detail: this.month +'-' + this.year
        })
        this.dispatchEvent(event)
    }

    callCreate(){
        this.isClicked=true;
        this.displaySave=true;
    }

    monthValue;
    createRecord(){
        if(this.workScope != '' && this.workScope!=null){
            if(this.workScopesList.includes(this.workScope)){
                for(let i=0;i<this.massets.length;i++){
                    if(this.massets[i].AssetId== this.assetId){
                            this.mAssetId=this.massets[i].Id;
                            break;
                    }
                }
                for(let i=0;i<this.allMonths.length;i++){
                    if(this.allMonths[i].Name==this.month){
                        this.monthValue=this.allMonths[i].Code;
                        break;
                    }
                }
                //this.dateValue=Utils.createDate(this.month,this.year);
            this.date=new Date(this.year,this.monthValue,0);
                console.log ("@@year"+this.year);
                console.log ("@@monthValue"+this.monthValue);
                console.log ("@@date"+this.date);
                
                //Date Format 
                const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
                const formatter = new Intl.DateTimeFormat('en-CA', options);
                console.log('formatter' + formatter) ;
                this.formattedDate = formatter.format(this.date);
                //

                console.log("Updated Successfully");
                const fields = {'SFS_Work_Scope__c':this.workScope, 
                                'Title':'AM-MWR', 
                                'SortOrder':1, 
                                'RecurrencePattern':'FREQ=MONTHLY;BYSETPOS=-1;BYDAY=SU,MO,TU,WE,TH,FR,SA;COUNT=1;', 
                                'ParentMaintenanceRecordId':this.mAssetId ,
                                'NextSuggestedMaintenanceDate':this.date, 
                                'SFS_Maintenance_Date__c': this.date};
                var objRecordInput = {'apiName' : 'MaintenanceWorkRule', fields};
                console.log("objRecordInput"+JSON.stringify(objRecordInput));

                createRecord(objRecordInput)
                .then(()=>{
                    console.log("Updated Successfully");
                    this.displaySave=false;
                    this.dispatchEvent(new CustomEvent('create'));
                    Utils.showToast(this,'Success','Created Successfully','success');
                }).catch(error=>{
                    console.log(JSON.stringify(error));
                    Utils.showToast(this,'Error','Unable to update the record','error');
                })
            }else{
                Utils.showToast(this,'Error','Please enter one of these values 0, 1, 2, 4, 6, 8, 12, 16, 24, 48','error');
            }
        }
    }

    handleNewWorkScope(event){
        this.workScope = event.target.value;
        console.log(this.workScope);
    }
}