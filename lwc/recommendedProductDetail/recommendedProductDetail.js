import {LightningElement, api, wire} from "lwc";
import addToCart from "@salesforce/apex/RecommendedProducts.addToCart";

import msgService from '@salesforce/messageChannel/CartMessageChannel__c';
import {publish,MessageContext} from 'lightning/messageService';

import { ShowToastEvent } from "lightning/platformShowToastEvent";

import ADD_TO_CART from "@salesforce/label/c.Add_To_Cart";
import SKU from "@salesforce/label/c.SKU";
import CART_SUCCESS_TITLE from "@salesforce/label/c.Cart_Success_Title";
import CART_FAILURE_TITLE from "@salesforce/label/c.Cart_Failure_Title";

export default class RecommendedProductDetail extends LightningElement {
	@api crossSellItem;
	label = {
		ADD_TO_CART,
		SKU,
		CART_SUCCESS_TITLE,
		CART_FAILURE_TITLE
	};
	//to store the map between productId & quantity
	qtyMap = new Map();
	quantity;
	loaded = true;

	@wire(MessageContext)
	messageContext;
	connectedCallback() {
		this.quantity = this.crossSellItem.quantity;
		this.qtyMap.set(this.crossSellItem.recId, this.quantity);
	}
	handleRemoveClick() {
		if (this.quantity > 1) {
			this.qtyMap.set(this.crossSellItem.recId, --this.quantity);
		} else {
			this.qtyMap.set(this.crossSellItem.recId, this.quantity);
		}
	}
	handleAddClick() {
		this.qtyMap.set(this.crossSellItem.recId, ++this.quantity);
	}
	handleQuantityChange(evt) {
		this.qtyMap.set(this.crossSellItem.recId, evt.currentTarget.value);
	}

	handleAddToCart() {
		// console.log("handleAddToCart>>>" + this.qtyMap.get(this.crossSellItem.recId));
		// console.log("label>>>" + JSON.stringify(this.label));
		this.loaded = false;
		addToCart({
			webStoreId: this.crossSellItem.webStoreId,
			productId: this.crossSellItem.recId,
			quantity: this.qtyMap.get(this.crossSellItem.recId),
			effectiveAccountId: this.crossSellItem.accountId
		})
			.then((result) => {
				this.loaded = true;
				// console.log("result>>>" + result);
				this.dispatchEvent(
					new CustomEvent("addtocart", {
						detail: {
							message: "item added"
						}
					})
				);
				this.dispatchEvent(
					new ShowToastEvent({
						title: this.label.CART_SUCCESS_TITLE,
						message: " ",
						variant: "success"
					})
				);
				this.publishHandler();
			})
			.catch((error) => {
				this.loaded = true;
				console.log("errors: " + JSON.stringify(error));
				this.dispatchEvent(
					new ShowToastEvent({
						title: this.label.CART_FAILURE_TITLE,
						message: error.message,
						variant: "error"
					})
				);
			});
	}


	publishHandler() {
		const payload = {
			messageToSend : 'cartItems'
		}
		publish(this.messageContext, msgService, payload);
	}
}