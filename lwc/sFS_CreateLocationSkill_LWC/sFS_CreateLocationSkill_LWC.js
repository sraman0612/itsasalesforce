import { LightningElement,  api, wire, track } from 'lwc';
import getRecords from "@salesforce/apex/sFS_LocationSkillController.getRecords";  
import getSkill from "@salesforce/apex/sFS_LocationSkillController.getSkill";  
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { CurrentPageReference } from 'lightning/navigation';
import { getRecord} from 'lightning/uiRecordApi';
import EndDate from '@salesforce/schema/Contract.EndDate';
//import LSR_OBJECT from '@salesforce/schema/SFS_Location_Skill_Requirement__c';

const fields = ['SFS_Location_Skill_Requirement__c.Location__c,SFS_Location_Skill_Requirement__c.Name '];
export default class SFS_CreateLocationSkill_LWC extends NavigationMixin(LightningElement) {
    //objectApi= LSR_OBJECT;
@track create=false;
@track edit=false;
currentPageReference = null;
@api recordId;
@track curPageRef =false;
@track locationId ;
@track skillName ;  
@track skillRecordId;  
@track Name;
@track location;
@track skillId;
@track skilName;

  testURL=false;
    
@wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
       if (currentPageReference && currentPageReference.state.ws !=null ) {
        console.log('curr'+JSON.stringify(currentPageReference));
          this.urlStateParameters = currentPageReference.state.ws;
          let temp =this.urlStateParameters.split('/');
          this.locationId=temp[4];
          this.curPageRef=true;
          //alert(temp[4]);
          this.testURL=true;
          
          
       }
    }    

   

    
   connectedCallback(){
        if (this.recordId==null) {
          this.create= true;
        }

        else{
           this.edit= true;
          
        }
        

   }
    @wire(getRecord, { recordId: '$recordId', fields })
    LSRRec;


   
    onskillSelection(event){  
       
     this.skillName = event.detail.selectedValue;  
    this.skillRecordId = event.detail.selectedRecordId;  
    console.log('10'+ JSON.stringify(this.skillName));
    
    } 
   
    
    handleOnSubmit(event){
        event.preventDefault();       
        const fields = event.detail.fields;
        console.log('40' +event.detail.fields );
        fields.Skill__c = this.skillName;
        this.oldLoc =fields.Location__c;
        fields.Skill_Id__c =this.skillRecordId;
        console.log('30'+ JSON.stringify(this.location));
        console.log('60'+ (this.skillName));
        console.log('80'+ (this.skillRecordId));
        
        if(this.skillName ==null ){
            const toast = new ShowToastEvent({
                  
                message: "Skill should not be blank",
                 variant: 'error'
          
               });
               this.dispatchEvent(toast); 
              
        }  
        else{
            
            if(this.location!=null){
                this.finalLocation=this.location;
            }else if(this.oldLoc!=null){
                this.finalLocation=this.oldLoc;
            }
         getRecords({locationName: this.finalLocation, skillCheck: this.skillName})
         .then((result) => {
             console.log('70' + result);
             if(result.length === 0){
                 this.template.querySelector('lightning-record-edit-form[data-id="createForm"]').submit(fields);

             }
             else{
                 console.log('100' + result);
                 const toast = new ShowToastEvent({
                  
                  message: "Location with this Skill already exist",
                   variant: 'error'
            
                 });
                 this.dispatchEvent(toast);
             }
         });
        }
        

        
        
    
      
    }
   


    handleOnSuccess(event){
       
       
        const events = new ShowToastEvent({
            title:"Successful",
            message: "Location Skill Requirement has been Created" ,
            variant: 'success'
            
            
        });
        console.log('onsuccess event recordEditForm', event.detail.id);
        
        this.dispatchEvent(events);
        this.handleEditCancel();
        this.handleNavigation(event.detail.id);
        
    }
    handleOnEditSuccess(event){
       
        const events = new ShowToastEvent({
            title:"Successful",
            message: "Location Skill Requirement is Successfully edited",
            variant: 'success'
            
        });
        console.log('onsuccess event recordEditForm', event.detail.id);
        
        this.dispatchEvent(events);
        this.handleEditCancel(); 

        this.handleNavigation(event.detail.id);
    }
    
    handleError(event){
        console.log(event.type);
        console.log(evnt.detail);
    }
   

    handleEditCancel(){
        var close = true;
            const closeclickedevt = new CustomEvent('closeclicked', {
                detail: { close },
            });
    
             // Fire the custom event
             this.dispatchEvent(closeclickedevt); 

       
        
    }
   
  
     handleCreateCancel(){ 
      
             this.handleEditCancel();
           
             if(!this.testURL){
                 console.log("insideif");
             

             this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'SFS_Location_Skill_Requirement__c',
                    actionName: 'list'
                },
                state: {
                   
                    filterName: 'Recent' 
                }
            });
        }
        
    }
        
        
    
    

    



    handleOnLoad(event){
        var record = event.detail.records;
    var fields = record[this.recordId].fields; // record['0010K000026Y******'].fields;
    const skiName = fields.Skill__c.value;
    this.skilName= skiName;
    console.log('lo'+JSON.stringify(this.skilName)) ;     
    //alert(skiName);
    getSkill({skillName: skiName})
    .then((result) => {
        console.log('result'+ JSON.stringify(result));
        this.skillId = result.Id;
        console.log('id'+(this.skillId ));
       
    
    })
}
    
    handleNavigation(recId){
    
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recId,
                objectApiName: 'SFS_Location_Skill_Requirement__c',
                actionName: 'view'
            },
            
        });

    }
    
    locationChange(event) {

        this.location= event.target.value;

    }
    
    

    handleOnEditSubmit(event){
        event.preventDefault();       
        const fields = event.detail.fields;
        
       
    this.locationEdit = fields.Location__c;
    console.log('hi' + this.locationEdit);
        fields.Skill__c = this.skillName;
        console.log('skillname'+this.skillName);
        console.log('skillid'+this.skillRecordId);
        fields.Skill_Id__c= this.skillRecordId;

        if( this.skillName != null){
            getRecords({locationName: this.locationEdit, skillCheck: this.skillName})
            .then((result) => {
                console.log('70' + result);
                if(result.length === 0){
                    this.template.querySelector('lightning-record-edit-form[data-id="editForm"]').submit(fields);
   
                }
                else{
                    console.log('100' + result);
                    const toast = new ShowToastEvent({
                     
                     message: "Location with this Skill already exist",
                      variant: 'error'
               
                    });
                    this.dispatchEvent(toast);
                }
            });
           }else{
            const toast = new ShowToastEvent({
                  
                message: "Update the skill",
                 variant: 'error'
          
               });
               this.dispatchEvent(toast); 
              
           }
           
        
    }
        
       


}