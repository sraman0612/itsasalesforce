import { LightningElement,  api, wire, track } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import { getRecord, updateRecord } from 'lightning/uiRecordApi';

//Address Fields
import COUNTRY from '@salesforce/schema/Address.Country';
import STATE from '@salesforce/schema/Address.State';
import STREET from '@salesforce/schema/Address.Street';
import CITY from '@salesforce/schema/Address.City';
import POSTAL_CODE from '@salesforce/schema/Address.PostalCode';
import LOC_TYPE from '@salesforce/schema/Address.LocationType';

//Location Fields
import VISITORADDRESS_ID from '@salesforce/schema/Location.VisitorAddressId';
import ID_FIELD from '@salesforce/schema/Location.Id';
import LOC_STREET from '@salesforce/schema/Location.CAP_IR_Street2__c';
import LOC_BUILDING from '@salesforce/schema/Location.SFS_Building__c';
import LOC_ROOM from '@salesforce/schema/Location.SFS_Room__c';

export default class SFS_CreateUpdateAddress_LWC extends LightningElement {

    //Location Record Id
    @api recordId;
    objectName = 'Address';
    //Address Id
    addressId;
    street;
    city;
    postalCode;
    state;
    country;
    @track isLoading = true;
    @track locationStreet;
    @track building;
    @track room;
    isCreate = true;

    //This variable will allow to Update Location only when the Address details refereshed from Database
    //After Create/Update
    @track allowLocationUpdate = false;

    //Wire method to get Visitor Address id from Location
    @wire(getRecord, { recordId: '$recordId', fields: [VISITORADDRESS_ID, LOC_BUILDING, LOC_ROOM]})
    getVisitorAddressId({ error, data }) {
        if (data) {
            console.log('data>>'+JSON.stringify(data));
            this.addressId = data.fields.VisitorAddressId.value;
            
            if(this.addressId !== undefined && this.addressId !== '' && this.addressId !== null) {
                this.isCreate = false;
            }
            this.building = data.fields.SFS_Building__c.value;
            this.room = data.fields.SFS_Room__c.value;
        }else{
            console.log('Error from VisitorAddress Wire method'+JSON.stringify(error));
        }    
    }

    //Wire method to fetch Address details if Existing on Location
    @wire(getRecord, { recordId: '$addressId', fields: [STREET, CITY, POSTAL_CODE, COUNTRY, STATE, LOC_TYPE]})
    getVisitorAddressDetails({ error, data }) {
        if (data) {
            console.log('data getVisitorAddressDetails>>'+JSON.stringify(data));
            
            this.street = data.fields.Street.value;
            this.locationStreet=data.fields.Street.value;
            console.log('street value : '+this.street+' '+this.locationStreet);
            this.city = data.fields.City.value;
            this.postalCode = data.fields.PostalCode.value;
            console.log('allow location update'+this.allowLocationUpdate);
            if(this.allowLocationUpdate) {
                this.state = data.fields.State.value;
                this.country = data.fields.Country.value;
                
                //Update Location
                this.handleLocationUpdate();
            }
            
        }else{
            console.log('Error from getVisitorAddressDetails Wire method'+JSON.stringify(error));
        }    
    }

    //Update Location with newly created Address
    handleLocationUpdate() {
        const fields = {};
        
        
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[VISITORADDRESS_ID.fieldApiName] = this.addressId;
        fields[LOC_STREET.fieldApiName] = this.locationStreet;
        fields[LOC_BUILDING.fieldApiName] = this.building;
        fields[LOC_ROOM.fieldApiName] = this.room;
        //console.log('street value 87: '+this.locationStreet);

        let displaymessage;

        if(this.isCreate) {
            displaymessage = 'Record Created Successfully';
        }else{
            displaymessage = 'Record Updated Successfully';
        }

        const recordInput = { fields };
        console.log('Updatable Record'+JSON.stringify(recordInput));

        updateRecord(recordInput)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: displaymessage,
                        variant: 'success'
                    })
                ); 
                this.isLoading = false;
                this.handleCancel();
            })
            .catch(error => {
                console.log('Record Edit Form Error Location>>'+JSON.stringify(error));
                this.isLoading = false;

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
                
            })
    }

    handleLoad(event) {
        this.isLoading = false;
    }

    handleSubmit(event){
        console.log('Submitted Field Details>>',JSON.stringify(event.detail.fields));
        
        event.preventDefault(); // do this so that you can modify the form values
        let fields = event.detail.fields; 
        
        this.template.querySelector('lightning-record-edit-form').submit(fields);
        this.isLoading = true;
        this.allowLocationUpdate = true;

        /*if(fields.CountryCode != null) {
            if(fields.State != null && fields.State.indexOf('XX') == 0) {
                fields.State = '';
                fields.StateCode = '';
            }
            fields.LocationType = 'L';
            
            console.log('After Change>>',JSON.stringify(fields));

            this.template.querySelector('lightning-record-edit-form').submit(fields); 
    
            this.isLoading = true;
        }else{
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: 'Country is required',
                    variant: 'error'
                })
            );
        }*/
        
    }

    //Perform Activity once Address is created/updated
    handleSuccess(event) {

        this.addressId = event.detail.id;
        //Set flag to true to update Location address fields
        //this.allowLocationUpdate = true;
        this.isLoading = true;
    }

    handleError(event) {
        console.log('Record Edit Form Error>>'+JSON.stringify(event));
        this.isLoading = false;

        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Error',
                message: event.detail.detail,
                variant: 'error'
            })
        );
    }
    assignBuilding(event) {
        this.building = event.detail.value;
        this.inputChanged = true;
    }

    assignRoom(event) {
        this.room = event.detail.value;
        this.inputChanged = true;
    }

    //Fire Close Event to close quick action
    handleCancel() {
        const qaction = new CustomEvent('close');
        // Dispatches the event.
        this.dispatchEvent(qaction);
    }
}