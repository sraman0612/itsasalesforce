import {LightningElement, api, wire} from 'lwc';
import msgService from '@salesforce/messageChannel/CartMessageChannel__c';
import {subscribe, MessageContext} from 'lightning/messageService';

import getCartItems from '@salesforce/apex/B2BCartController.getCartItems';
import getWebCartURL from "@salesforce/apex/RecommendedProducts.getWebCartUrl";
import isPermissionAssigned from "@salesforce/apex/B2BCartController.checkBuyerPermission";

import { NavigationMixin } from "lightning/navigation";
export default class CartItems extends NavigationMixin(LightningElement) {
    totalInCart;
    @api recordId;
    @api effectiveAccountId;
    @api userId;
    cartURL;
    userHasPermission = false;

    @wire(MessageContext)
    messageContext;
    subscription;

    connectedCallback() {
        // Initialize 'cartItems' list as soon as the component is inserted in the DOM.
        this.isPermissionAssignedToUser();
        this.subscribeToChannel();
        this.getCartData();
        this.getCartURL();
    }

    get resolvedEffectiveAccountId() {
        const effectiveAccountId = this.effectiveAccountId || '';
        let resolved = null;
        if (
            effectiveAccountId.length > 0 &&
            effectiveAccountId !== '000000000000000'
        ) {
            resolved = effectiveAccountId;
        }
        return resolved;
    }

    getCartData(){
        getCartItems({
            userId: this.userId,
            effectiveAccountId: this.resolvedEffectiveAccountId
        })
            .then((result) => {
                //console.log(result);
                if (result) {
                    this.cartItems = result.cartItems;
                    this.totalInCart = Number( result.cartSummary.totalProductCount);
                }
                else {
                    this.totalInCart = 0;
                }
            })
            .catch((error) => {
                const errorMessage = error.body.message;
                this.cartItems = undefined;
                console.log(errorMessage);
            });
    }

    getCartURL(){
        getWebCartURL({})
            .then((result) => {
                //console.log(result);
                this.cartURL = result;
            })
            .catch((error) => {
                const errorMessage = error.body.message;
                console.log(errorMessage);
            });
    }

    isPermissionAssignedToUser(){
        isPermissionAssigned({userId: this.userId})
            .then((result) => {
                //console.log(result);
                this.userHasPermission = result;
            })
            .catch((error) => {
                const errorMessage = error.body.message;
                console.log(errorMessage);
            });
    }

    subscribeToChannel() {
        this.subscription = subscribe(this.messageContext, msgService, (message) => this.getCartData());
    }

    handleNavigate() {
        const config = {
            type: 'standard__webPage',
            attributes: {
                url: this.cartURL
            }
        };
        this[NavigationMixin.Navigate](config);
    }
}