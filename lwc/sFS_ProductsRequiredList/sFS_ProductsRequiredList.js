import { LightningElement,track,api } from 'lwc';
import getProductsRequired from '@salesforce/apex/SFS_ProductsRequiredDataForLWC.getproductsRequired';
export default class SFS_ProductsRequiredList extends LightningElement {
@api recordId;
@track productReqList=[];
@track updatedproductReqList=[];
@track excludedList=[];
@track error;
    connectedCallback(){
        console.log('in connected call back '+this.recordId);
        getProductsRequired({assetId:this.recordId})
        .then(result=>{
            this.productReqList=result;
            this.error=undefined;
        })
        .catch(error=>{
            this.error=error;
        })
        for(let i=0;i<this.productReqList.length-1;i++){
            let exists=false;
            for(let j=0;j<this.productReqList.length;j++){
                if(this.productReqList[i].Product2Id==this.productReqList[j].Product2Id 
                    && this.productReqList[i].QuantityRequired==this.productReqList[j].QuantityRequired
                    && !(this.excludedList.includes(this.productReqList[i].Id))
                    && !(this.excludedList.includes(this.productReqList[j].Id))){
                        this.updatedproductReqList.push({QuantityRequired:this.productReqList[i].QuantityRequired+this.productReqList[j].QuantityRequired,
                            Product2Id:this.productReqList[i].Product2Id});
                        this.excludedList.push(this.productReqList[i].Id);
                        this.excludedList.push(this.productReqList[j].Id);
                        exists=true;
                        break;
                    }
            }
            if(exists==false){
                this.updatedproductReqList.push(this.productReqList[i]);
            }
            console.log('36---'+this.updatedproductReqList);
        }
    }
}