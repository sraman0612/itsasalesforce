import { LightningElement, track, api } from 'lwc';
import findRecords from "@salesforce/apex/SFS_LwcLookupController.findRecords";  
export default class SFS_CustomLookupComp_LWC extends LightningElement {
    @track recordsList;  
    @track searchKey = "";  
    @api selectedValue;  
    @api selectedRecordId;  
    @api objectApiName;  
    @api iconName;  
    @api lookupLabel; 
    
    @track message;  
      
    onLeave(event) {  
     setTimeout(() => {  
      this.searchKey = "";  
      this.recordsList = null;  
     }, 300);  
    }  
      
    onRecordSelection(event) {  
     this.selectedRecordId = event.target.dataset.key;  
     console.log('10'+ JSON.stringify(event.target.dataset));
     this.selectedValue = event.target.dataset.name;  
     this.searchKey = "";  
     this.onSeletedRecordUpdate();  
    } 
    
  
     
    handleKeyChange(event) {  
     const searchKey = event.target.value;  
     this.searchKey = searchKey;  
     this.getLookupResult();  
    }  
     
    removeRecordOnLookup(event) {  
     this.searchKey = "";  
     this.selectedValue = null;  
     this.selectedRecordId = null;  
     this.recordsList = null;  
     this.onSeletedRecordUpdate();  
   }  
   getLookupResult() {  
    findRecords({ searchKey: this.searchKey, objectName : this.objectApiName })  
     .then((result) => {  
      if (result.length===0) {  
        this.recordsList = [];  
        this.message = "No Records Found";  
       } else {  
        this.recordsList = result; 
        console.log('48'+JSON.stringify(this.recordsList));
        this.message = "";  
       }  
       this.error = undefined;  
     })  
     .catch((error) => {  
      this.error = error;  
      this.recordsList = undefined;  
     });  
   }  
    
   onSeletedRecordUpdate(){  
     //alert(this.selectedRecordId );
     //alert(this.selectedValue );

    const passEventr = new CustomEvent('recordselection', {  
      detail: { selectedRecordId: this.selectedRecordId, selectedValue: this.selectedValue }  
     });  
     this.dispatchEvent(passEventr);  
   }    
}