import { LightningElement,api,track} from 'lwc';
import { updateRecord} from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';
import BILLTOACC_FIELD from '@salesforce/schema/ServiceContract.SFS_Bill_To_Account__c';
import ID_FIELD from '@salesforce/schema/ServiceContract.Id';
import WOBILLTOACC_FIELD from '@salesforce/schema/WorkOrder.SFS_Bill_to_Account__c';
import WOID_FIELD from '@salesforce/schema/WorkOrder.Id';
import searchAccount from "@salesforce/apex/SFS_accountDetails.searchAccount";
import getAccount from "@salesforce/apex/SFS_accountDetails.getAccount";
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
const columns = [ { label: 'AccountName', fieldName: 'Name', sortable: 'true'},
                  { label: 'IRIT Customer Number', fieldName: 'IRIT_Customer_Number__c', sortable: 'true'},
                  { label: 'Street', fieldName: 'ShippingStreet', sortable: 'true'},
                  { label: 'City', fieldName: 'ShippingCity', sortable: 'true'},
                  { label: 'State', fieldName: 'ShippingState', sortable: 'true'},
                  { label: 'PostalCode', fieldName: 'ShippingPostalCode', sortable: 'true'},
                  { label: 'Country', fieldName: 'ShippingCountry', sortable: 'true'}
                  ];


export default class SFS_UpdateBillToOnSvcAgreement extends NavigationMixin(LightningElement) {
    @api records2=[];
    @track records2 = [];
    @api accType;
    @track records1 = [];
    @track columns = columns;
    @track sortBy;
    @track sortDirection;
    @track accountList;
    @track noRecordsFound = true;
    @api custNo;
    @api orclNo;
     @api objectApi;
     
@api recordId;
selectedAccountId;
availableActions=[];
    @track accSearch = '';
    @track isSearch = false;
    @track isNotSelected = true;
   
    connectedCallback(){
        console.log('custNo ----'+JSON.stringify(this.custNo));
        getAccount ( {accountType:this.accType, objectName:this.objectApi,customerNo:this.custNo,oracleNo:this.orclNo}) 
    .then(result => {
        this.records2 = result;
        console.log('result ----'+JSON.stringify(result));
        //this.noRecordsFound = false;
        console.log('recordList   '+ JSON.stringify(this.records2));
    })

   }
    
    

   doSorting(event) {
    //Console.log('sortby'+event.detail);
    //Console.log('sortby');
    this.sortBy = event.detail.fieldName;
    this.sortDirection = event.detail.sortDirection;
    console.log('sortby'+event.detail.fieldName);
    console.log('sortdirection'+event.detail.sortDirection);
    this.sortData(this.sortBy, this.sortDirection);
    this.sortDataOnSearch(this.sortBy, this.sortDirection);
    console.log('sortby'+this.sortBy);
    console.log('sortdirection'+this.sortDirection);
}

sortData(fieldname, direction) {
    let parseData = JSON.parse(JSON.stringify(this.records2));
    // Return the value stored in the field
    console.log('record2   '+ JSON.stringify(this.records2));
    let keyValue = (a) => {
        return a[fieldname];
    };
    // checking reverse direction
    let isReverse = direction === 'asc' ? 1: -1;
    // sorting data
    parseData.sort((x, y) => {
        x = keyValue(x) ? keyValue(x) : ''; // handling null values
        y = keyValue(y) ? keyValue(y) : '';
        // sorting values based on direction
        return isReverse * ((x > y) - (y > x));
    });
    console.log('parsedat   '+ JSON.stringify(parseData));
    this.records2 = parseData;
    console.log('record2afterparse   '+ JSON.stringify(this.records2));
} 

sortDataOnSearch(fieldname, direction) {
    let parseData = JSON.parse(JSON.stringify(this.records1));
    // Return the value stored in the field
    let keyValue = (a) => {
        return a[fieldname];
    };
    // checking reverse direction
    let isReverse = direction === 'asc' ? 1: -1;
    // sorting data
    parseData.sort((x, y) => {
        x = keyValue(x) ? keyValue(x) : ''; // handling null values
        y = keyValue(y) ? keyValue(y) : '';
        // sorting values based on direction
        return isReverse * ((x > y) - (y > x));
    });
    this.records1 = parseData;
} 





   
onSearch(event){
    this.accSearch = event.target.value;
    this.isSearch = true;
    this.records1 = [];
    console.log(this.accSearch);
    console.log(this.recordId);
    searchAccount ( {accName: this.accSearch, objectName:this.objectApi,customerNo:this.custNo, accountType:this.accType,oracleNo:this.orclNo}) 
    .then(result => {
        this.records1 = result;
        console.log('result ----'+JSON.stringify(result));
        //this.noRecordsFound = false;
        console.log('recordList   '+ JSON.stringify(this.records1));
    })
}
   







handleRowSelection (event) {
    const selectedRows = event.detail.selectedRows;
    // Display that fieldName of the selected rows
    for (let i = 0; i < selectedRows.length; i++){
      
        this.selectedAccountId=selectedRows[i].Id;
        if((this.selectedAccountId)!= null){
            this.isNotSelected=false;

        }
        

    }
   
}

handleUpdate(event){
    const fields = {};
    console.log('130'+this.objectApi);
    if(this.objectApi=='ServiceContract'){
    fields[BILLTOACC_FIELD.fieldApiName]=this.selectedAccountId;
    fields[ID_FIELD.fieldApiName]=this.recordId;
    console.log('onUpdate '+JSON.stringify(fields));
    }
    else{
        fields[WOBILLTOACC_FIELD.fieldApiName]=this.selectedAccountId;
        fields[WOID_FIELD.fieldApiName]=this.recordId; 
    }

    const recordInput ={fields};
    console.log(JSON.stringify(recordInput));
    
     updateRecord(recordInput)
    
    .then((result)=>{
       
        const events = new ShowToastEvent({
            title:"Successful",
            message: "Bill To Account is Updated Successfully ",
            variant: 'success'
            
        });
        this.dispatchEvent(events);
            this[NavigationMixin.Navigate]({
               type: 'standard__recordPage',
                attributes: {
                   
                    recordId: this.recordId,
                    actionName: 'view'
                }
            })
        
    })
    .catch((error) => {

        this.showToast('Error', error.body.output.errors[0].errorCode + '- '+ error.body.output.errors[0].message,'error');
       
     })
    


   
    
 }

 showToast(title,message,variant){
    this.dispatchEvent(
        new ShowToastEvent({
            title:title,
            message:message,
            variant:variant
        })
    )}


 




}