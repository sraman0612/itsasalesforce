import { ShowToastEvent } from 'lightning/platformShowToastEvent';
/*export function createDate(month,year){
    var endDate;
    if(month=='JAN' || month=='MAR' || month=='MAY' || month=='JUL' || month=='AUG' 
            || month=='OCT' || month=='DEC'){
                endDate=31;
            }else{
                if(month == 'FEB'){
                    if(((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)){
                        endDate=29;
                    }
                    else{
                        endDate=28;
                    }
                }else{
                    endDate=30;
                }
            }
            return endDate;
}*/
export function showToast(component,title,message,variant){
    component.dispatchEvent(new ShowToastEvent({
        title:title,
        message:message,
        variant:variant
    }));
}