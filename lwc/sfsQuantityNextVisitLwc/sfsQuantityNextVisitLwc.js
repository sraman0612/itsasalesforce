import { LightningElement, track, wire, api } from "lwc";
import getProductsRequired from "@salesforce/apex/SFS_ProductsRequiredDataForLWC.getproductsRequired";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { refreshApex } from "@salesforce/apex";
import { createRecord } from "lightning/uiRecordApi";
import { deleteRecord } from "lightning/uiRecordApi";
import PART_REQUIRED_OBJ from "@salesforce/schema/CAP_IR_Asset_Part_Required__c";
import ASSET_FIELD from "@salesforce/schema/CAP_IR_Asset_Part_Required__c.Asset__c";
import PRODUCT_FIELD from "@salesforce/schema/CAP_IR_Asset_Part_Required__c.Product__c";
import QUANTITY_FIELD from "@salesforce/schema/CAP_IR_Asset_Part_Required__c.SFS_Quantity__c";
import WORKSCOPE_FIELD from "@salesforce/schema/CAP_IR_Asset_Part_Required__c.SFS_0k__c";
import { updateRecord } from "lightning/uiRecordApi";
import ID_FIELD from "@salesforce/schema/CAP_IR_Asset_Part_Required__c.Id";
import updateRecordsDml from "@salesforce/apex/SFS_ProductsRequiredDataForLWC.updateRecords";
import createRecordsDml from "@salesforce/apex/SFS_ProductsRequiredDataForLWC.createPartsRequired";
export default class SfsQuantityNextVisitLwc extends LightningElement {
  @track allRecords = [];
  @track partsRequired = [];
  @track productsRequired = [];
  @track disabled = true;
  @track finalRecords = [];
  @track records1 = [];
  @api recordId;
  @track Asset;
  @track Qty;
  count = 0;
  @track hideSave = true;
  wiredRecords = [];

  productSelected;

  @wire(getProductsRequired, { assetId: "$recordId" })
  wiredResult(result) {
    this.wiredRecords = result;
    if (result.data) {
      this.allRecords = Array.from(
        result.data.map((row) => {
          return {
            ...row,
            newWorkScope: row.workScope,
            newWorkScope0: row.workScope0,
            newWorkScope1: row.workScope1,
            newWorkScope2: row.workScope2,
            newWorkScope4: row.workScope4,
            newWorkScope6: row.workScope6,
            newWorkScope8: row.workScope8,
            newWorkScope12: row.workScope12,
            newWorkScope16: row.workScope16,
            newWorkScope24: row.workScope24,
            newWorkScope48: row.workScope48,
            newQtyNextVisit: row.qtyNextvisit,
            displayRow: false
          };
        })
      );

      this.allRecords.forEach((record) => {
        if (
          (record.partRequired == true && record.quantity > 0) ||
          record.removeFromTable == false
        ) {
          record.displayRow = true;
        }
        if (record.partRequired == true) {
          this.partsRequired.push({
            rowId: record.rowId,
            productName: record.productName,
            productId: record.productId,
            quantity: record.quantity,
            consumableDate: record.consumableDate,
            active: record.active,
            oracle: record.oracle,
            workScope: record.workScope,
            quantityNextVisit: record.qtyNextvisit,
            deleteAfterUseRecId: record.deleteAfterUseRecId,
            productDescription: record.productDescription
          });
        }
      });
    } else if (result.error) {
      this.error = result.error;
    }
  }

  refreshPage() {
    window.location.reload(true);
  }

  @track openModal = false;
  showModal() {
    this.showMessage = false;
    this.openModal = true;
  }
  @track showMessage = false;
  handleSubmit(event) {
    event.preventDefault();
    const inputFields = event.detail.fields;
    if(!this.productSelected){
      console.log('not set');
    }
    else{
      console.log(this.productSelected);
    }
    let partExists = false;
    let assetPartRequiredId;
    for (let i = 0; i < this.partsRequired.length; i++) {
      if (this.partsRequired[i].productId == inputFields.Product__c) {
        partExists = true;
        assetPartRequiredId = this.partsRequired[i].rowId;
        break;
      }
    }
    if (partExists == false) {
      const fields = {};
      fields[ASSET_FIELD.fieldApiName] = this.recordId;
      fields[PRODUCT_FIELD.fieldApiName] = inputFields.Product__c;
      //fields[PRODUCT_FIELD.fieldApiName] = this.productSelected;
      fields[QUANTITY_FIELD.fieldApiName] = inputFields.SFS_Quantity__c;
      fields[WORKSCOPE_FIELD.fieldApiName] = true;
      const recordInput = { apiName: PART_REQUIRED_OBJ.objectApiName, fields };
      createRecord(recordInput)
        .then(() => {
          this.showToast(
            "Success",
            "Asset Part Required is created",
            "success"
          );
          return refreshApex(this.wiredRecords);
        })
        .catch((error) => {
          this.showToast(
            "Error",
            error.body.output.errors[0].errorCode +
              "- " +
              error.body.output.errors[0].message,
            "error"
          );
        });
    } else {
      const fields = {};
      fields[ID_FIELD.fieldApiName] = assetPartRequiredId;
      fields[QUANTITY_FIELD.fieldApiName] = inputFields.SFS_Quantity__c;
      fields[WORKSCOPE_FIELD.fieldApiName] = true;
      const recordInput = { fields };
      updateRecord(recordInput)
        .then(() => {
          this.showToast("Success", "Updated Successfully", "success");
          return refreshApex(this.wiredRecords);
        })
        .catch((error) => {
          this.showToast(
            "Error",
            error.body.output.errors[0].errorCode +
              "- " +
              error.body.output.errors[0].message,
            "error"
          );
        });
    }
    this.closeModal();
  }
  removeRow(event) {
    var selectedRow = event.currentTarget;
    var key = selectedRow.dataset.id;
    let partExists = false;
    let assetPartRequiredId;
    let deleteAfterUseId = "";
    let deletedRowId;
    for (let i = 0; i < this.partsRequired.length; i++) {
      if (this.partsRequired[i].productId == key) {
        partExists = true;
        assetPartRequiredId = this.partsRequired[i].rowId;
        deleteAfterUseId = this.partsRequired[i].deleteAfterUseRecId;
        deletedRowId = i;
        break;
      }
    }
    if (deleteAfterUseId != "" && deleteAfterUseId != undefined) {
      deleteRecord(deleteAfterUseId);
    }
    if (partExists == true) {
      const fields = {};
      fields[ID_FIELD.fieldApiName] = assetPartRequiredId;
      fields[QUANTITY_FIELD.fieldApiName] = 0;
      fields[WORKSCOPE_FIELD.fieldApiName] = true;
      const recordInput = { fields };
      updateRecord(recordInput)
        .then(() => {
          this.showToast("Success", "Deleted Successfully", "success");
          return refreshApex(this.wiredRecords);
        })
        .catch((error) => {
          this.showToast("Error creating record", error.body.message, "error");
        });
    }
  }
  editTable() {
    this.disabled = false;
    this.hideSave = false;
  }
  handleChange(event) {
    var selectedRow = event.currentTarget;
    var key = selectedRow.dataset.id;
    this.allRecords = [...this.allRecords];
    if (event.target.label == "0") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope0 = true;
      } else {
        this.allRecords[key].newWorkScope0 = false;
      }
    }
    if (event.target.label == "1") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope1 = true;
      } else {
        this.allRecords[key].newWorkScope1 = false;
      }
    }
    if (event.target.label == "2") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope2 = true;
      } else {
        this.allRecords[key].newWorkScope2 = false;
      }
    }
    if (event.target.label == "4") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope4 = true;
      } else {
        this.allRecords[key].newWorkScope4 = false;
      }
    }
    if (event.target.label == "6") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope6 = true;
      } else {
        this.allRecords[key].newWorkScope6 = false;
      }
    }
    if (event.target.label == "8") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope8 = true;
      } else {
        this.allRecords[key].newWorkScope8 = false;
      }
    }
    if (event.target.label == "12") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope12 = true;
      } else {
        this.allRecords[key].newWorkScope12 = false;
      }
    }
    if (event.target.label == "16") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope16 = true;
      } else {
        this.allRecords[key].newWorkScope16 = false;
      }
    }
    if (event.target.label == "24") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope24 = true;
      } else {
        this.allRecords[key].newWorkScope24 = false;
      }
    }
    if (event.target.label == "48") {
      if (event.target.checked) {
        this.allRecords[key].newWorkScope48 = true;
      } else {
        this.allRecords[key].newWorkScope48 = false;
      }
    }
    if (event.target.label == "Qty Next visit") {
      this.allRecords[key].newQtyNextVisit = event.target.value;
    }
  }
  updatedList = [];
  createdList = [];
  saveRecord() {
    this.updatedList = [];
    this.createdList = [];
    this.disabled = true;
    for (let i = 0; i < this.allRecords.length; i++) {
      if (
        this.allRecords[i].workScope0 != this.allRecords[i].newWorkScope0 ||
        this.allRecords[i].workScope1 != this.allRecords[i].newWorkScope1 ||
        this.allRecords[i].workScope2 != this.allRecords[i].newWorkScope2 ||
        this.allRecords[i].workScope4 != this.allRecords[i].newWorkScope4 ||
        this.allRecords[i].workScope6 != this.allRecords[i].newWorkScope6 ||
        this.allRecords[i].workScope8 != this.allRecords[i].newWorkScope8 ||
        this.allRecords[i].workScope12 != this.allRecords[i].newWorkScope12 ||
        this.allRecords[i].workScope16 != this.allRecords[i].newWorkScope16 ||
        this.allRecords[i].workScope24 != this.allRecords[i].newWorkScope24 ||
        this.allRecords[i].workScope48 != this.allRecords[i].newWorkScope48
      ) {
        if (this.allRecords[i].partRequired == true) {
          this.updatedList.push({
            recId: this.allRecords[i].rowId,
            workScope0: this.allRecords[i].newWorkScope0,
            workScope1: this.allRecords[i].newWorkScope1,
            workScope2: this.allRecords[i].newWorkScope2,
            workScope4: this.allRecords[i].newWorkScope4,
            workScope6: this.allRecords[i].newWorkScope6,
            workScope8: this.allRecords[i].newWorkScope8,
            workScope12: this.allRecords[i].newWorkScope12,
            workScope16: this.allRecords[i].newWorkScope16,
            workScope24: this.allRecords[i].newWorkScope24,
            workScope48: this.allRecords[i].newWorkScope48,
            updateWorkScope: true,
            updateQuantity: false
          });
        } else {
          this.createdList.push({
            workScope0: this.allRecords[i].newWorkScope0,
            workScope1: this.allRecords[i].newWorkScope1,
            workScope2: this.allRecords[i].newWorkScope2,
            workScope4: this.allRecords[i].newWorkScope4,
            workScope6: this.allRecords[i].newWorkScope6,
            workScope8: this.allRecords[i].newWorkScope8,
            workScope12: this.allRecords[i].newWorkScope12,
            workScope16: this.allRecords[i].newWorkScope16,
            workScope24: this.allRecords[i].newWorkScope24,
            workScope48: this.allRecords[i].newWorkScope48,
            quantity: this.allRecords[i].quantity,
            productRecId: this.allRecords[i].productId,
            assetId: this.recordId,
            deleteAfterUse: false
          });
        }
      }
    }
    for (let i = 0; i < this.allRecords.length; i++) {
      if (
        this.allRecords[i].qtyNextvisit != this.allRecords[i].newQtyNextVisit
      ) {
        let deleteAfterUseRecordId = this.allRecords[i].deleteAfterUseRecId;
        if (this.allRecords[i].deleteAfterUseRecId == null) {
          this.createdList.push({
            quantity: this.allRecords[i].newQtyNextVisit,
            productRecId: this.allRecords[i].productId,
            assetId: this.recordId,
            deleteAfterUse: true,
            workScope0: false,
            workScope1: false,
            workScope2: false,
            workScope4: false,
            workScope6: false,
            workScope8: false,
            workScope12: false,
            workScope16: false,
            workScope24: false,
            workScope48: false
          });
        } else {
          //update Asset Part Required
          if (this.allRecords[i].newQtyNextVisit == "") {
            this.updatedList.push({
              recId: deleteAfterUseRecordId,
              quantityNextVisit: null,
              updateWorkScope: false,
              updateQuantity: true
            });
          } else {
            this.updatedList.push({
              recId: deleteAfterUseRecordId,
              quantityNextVisit: this.allRecords[i].newQtyNextVisit,
              updateWorkScope: false,
              updateQuantity: true
            });
          }
        }
        this.handleSubmit;
      }
    }

    if (this.updatedList.length > 0) {
      updateRecordsDml({ wrapperText: JSON.stringify(this.updatedList) })
        .then(() => {
          this.showToast("Success", "Updated Successfully", "success");
          return refreshApex(this.wiredRecords);
        })
        .catch((error) => {
          this.showToast("Error creating record", error.body.message, "error");
          return refreshApex(this.wiredRecords);
        });
    }
    if (this.createdList.length > 0) {
      createRecordsDml({ wrapperText: JSON.stringify(this.createdList) })
        .then(() => {
          this.showToast("Success", "Created Successfully", "success");
          return refreshApex(this.wiredRecords);
        })
        .catch((error) => {
          this.showToast("Error creating record", error.body.message, "error");
        });
    }
    this.hideSave = true;
  }
  showToast(title, message, variant) {
    this.dispatchEvent(
      new ShowToastEvent({
        title: title,
        message: message,
        variant: variant
      })
    );
  }
  closeModal() {
    this.openModal = false;
  }

  onLookupSelection(event) {
    var recordId = event.detail.selectedRecordId;
    var selectedValueFromLookup = event.detail.selectedValue;
    console.log("@@@reached here@@@--->" + recordId);
    this.productSelected = recordId;
  }

}