import { LightningElement,api,track,wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { updateRecord,deleteRecord} from 'lightning/uiRecordApi';
import ID_FIELD from '@salesforce/schema/MaintenanceWorkRule.Id';
import WORKSCOPE_FIELD from '@salesforce/schema/MaintenanceWorkRule.SFS_Work_Scope__c';
import Utils from 'c/sFS_AgreementMatrixUtils';
export default class SFS_AgreementMatrixCard1 extends NavigationMixin(LightningElement) {
    @api month;
    @api year;
    @api record;
    
    @api massets;
    @api asset;
    @api assetId;
    @track displaySave;
    @track isClicked= false;
    @track workScope='';
    workType='';
    mAssetId='';
    workScopesList=['0', '1', '2','4','6','8','12','16', '24', '48'];
    allMonths= [
        { Name: 'JAN', Code: '01'},
        { Name: 'FEB', Code: '02'},
        { Name: 'MAR', Code: '03'},
        { Name: 'APR', Code: '04'},
        { Name: 'MAY', Code: '05'},
        { Name: 'JUN', Code: '06'},
        { Name: 'JUL', Code: '07'},
        { Name: 'AUG', Code: '08'},
        { Name: 'SEP', Code: '09'},
        { Name: 'OCT', Code: '10'},
        { Name: 'NOV', Code: '11'},
        { Name: 'DEC', Code: '12'}
     
    ];
    get isSameMonth(){
        return (this.month === this.record.SFS_Month__c && this.year == this.record.SFS_Year__c && this.assetId==this.record.assetId)
     }
    
     itemDragStart(){
         let woSize=0;
         if(this.record.workOrders){
            woSize=this.record.workOrders.length;
         }
        const event = new CustomEvent('itemdrag', {
            detail: this.record.Id+'&'+this.record.agreementStartDate+'&'+this.record.agreementEndDate+'&'+this.record.NextSuggestedMaintenanceDate+'&'+this.record.SFS_Generation_Horizon_Date__c+'&'+woSize,
        })
        this.dispatchEvent(event)
    }
    @track newValue;
    handleChange(event){
        this.displaySave=true;
        this.newValue=event.target.value;
    }
    saveWorkScope(){
        console.log('44--'+(this.newValue));
        if(this.newValue !=""){
            if(this.workScopesList.includes(this.newValue)){
                const fields = {};
                fields[ID_FIELD.fieldApiName]=this.record.Id;
                fields[WORKSCOPE_FIELD.fieldApiName]= this.newValue;
                const recordInput ={fields};
                console.log('43--'+JSON.stringify(recordInput));
                updateRecord(recordInput)
                .then(()=>{
                    console.log("Updated Successfully");
                    this.displaySave=false;
                    this.dispatchEvent(new CustomEvent('create', { bubbles: true, composed: true }));
                    Utils.showToast(this,'Success','Updated Successfully','success');
                }).catch(error=>{
                    console.log(JSON.stringify(error));
                    Utils.showToast(this,'Error','Unable to update the record','error');
                })
            }else{
                Utils.showToast(this,'Error','Please enter one of these values 0, 1, 2, 4, 6, 8, 12, 16, 24, 48','error');
            } 
        }
        else{
            deleteRecord(this.record.Id)
            .then(()=>{
                console.log("Deleted Successfully");
                this.displaySave=false;
                this.dispatchEvent(new CustomEvent('create', { bubbles: true, composed: true }));
                Utils.showToast(this,'Success','Deleted Successfully','success');
            }).catch(error=>{
                console.log(JSON.stringify(error));
                this.displaySave=false;
                this.dispatchEvent(new CustomEvent('create', { bubbles: true, composed: true }));
                Utils.showToast(this,'Error','Unable to delete the record','error');
            })

        }
    }
}