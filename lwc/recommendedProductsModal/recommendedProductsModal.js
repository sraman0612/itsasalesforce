import { api, wire } from "lwc";
import LightningModal from "lightning/modal";
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";
import PRODUCT_OBJECT from "@salesforce/schema/Product2";
import MAINTENANCE_INTERVAL from "@salesforce/schema/Product2.Maintenance_Interval__c";
import COMPRESSOR_AIR_FILTER_TYPE from "@salesforce/schema/Product2.Compressor_Air_Filter_type__c";

export default class RecommendedProductsModal extends LightningModal {
	@api labels = {};
	@api assetDetail = {};
	@api irLogoUrl = "";
	@api crossSellProductsList = [];
	filteredCrossSellProductsList = [];

	productRecordTypeId;

	compressorAirFilterHelpText = "";
	compressorAirFilterOptions = [];
	compressorAirFilterLabel = "(Compressor) Air Filter type";
	compressorAirFilterValue = "";
	maintenanceIntervalHelpText = "";
	maintenanceIntervalLabel = "Maintenance Interval";
	maintenanceIntervalOptions = [];
	maintenanceIntervalValue = "";
	error;

	connectedCallback() {
		this.filteredCrossSellProductsList = this.crossSellProductsList;
		console.log("irLogoUrl>>>" + this.irLogoUrl);
		//console.log("filteredCrossSellProductsList>>>" + JSON.stringify(this.filteredCrossSellProductsList));
	}

	@wire(getObjectInfo, { objectApiName: PRODUCT_OBJECT })
	results({ error, data }) {
		//console.log("product data>>>" + JSON.stringify(data));
		if (data) {
			this.productRecordTypeId = data.defaultRecordTypeId;
			this.compressorAirFilterLabel = data.fields.Compressor_Air_Filter_type__c.label;
			this.maintenanceIntervalLabel = data.fields.Maintenance_Interval__c.label;
			this.compressorAirFilterHelpText = data.fields.Compressor_Air_Filter_type__c.inlineHelpText;
			this.maintenanceIntervalHelpText = data.fields.Maintenance_Interval__c.inlineHelpText;
			this.error = undefined;
			console.log("this.compressorAirFilterHelpText>>>" + this.compressorAirFilterHelpText);
		} else if (error) {
			this.error = error;
			this.accountRecordTypeId = undefined;
		}
	}
	@wire(getPicklistValues, { recordTypeId: "$productRecordTypeId", fieldApiName: MAINTENANCE_INTERVAL })
	servIntvpicklistResults({ error, data }) {
		if (data) {
			//console.log("MAINTENANCE_INTERVAL>>>" + JSON.stringify(data));
			this.maintenanceIntervalOptions = data.values;
			this.error = undefined;
		} else if (error) {
			//console.log("MAINTENANCE_INTERVAL error>>>" + JSON.stringify(error));
		}
	}
	@wire(getPicklistValues, { recordTypeId: "$productRecordTypeId", fieldApiName: COMPRESSOR_AIR_FILTER_TYPE })
	inltFiltpicklistResults({ error, data }) {
		if (data) {
			//console.log("compressorAirFilterOptions>>>" + JSON.stringify(data));
			this.compressorAirFilterOptions = data.values;
			this.error = undefined;
		} else if (error) {
			//console.log("compressorAirFilterOptions error>>>" + JSON.stringify(error));
		}
	}

	get columnSize() {
		let arraySize = 0;
		switch (this.filteredCrossSellProductsList.length) {
			case 1:
			case 2:
			case 3:
			case 4:
				arraySize = 3;
				break;
			default:
				arraySize = 2;
		}
		return arraySize;
	}
	handleCompressorAirFilterChange(event) {
		this.compressorAirFilterValue = event.detail.value;
		//console.log("compressorAirFilterValue>>>" + this.compressorAirFilterValue);
		this.filterData();
	}
	handleMaintenanceIntervalChange(event) {
		this.maintenanceIntervalValue = event.detail.value;
		//console.log("this.maintenanceIntervalValue>>>" + this.maintenanceIntervalValue);
		this.filterData();
	}
	filterData() {
		//console.log("filterData called");
		console.log("this.maintenanceIntervalValue>>>" + this.maintenanceIntervalValue);
		console.log("compressorAirFilterValue>>>" + this.compressorAirFilterValue);
		console.log("crossSellProductsList>>>" + JSON.stringify(this.crossSellProductsList));
		this.filteredCrossSellProductsList = [];
		this.filteredCrossSellProductsList = this.crossSellProductsList.filter(function (item) {
			return (
				(item.compressorAirFilter == this.compressorAirFilterValue || this.compressorAirFilterValue == "") &&
				(item.maintenanceInterval == this.maintenanceIntervalValue || this.maintenanceIntervalValue == "")
			);
		}, this);
	}
	clearFilter() {
		this.compressorAirFilterValue = "";
		this.maintenanceIntervalValue = "";
		this.filteredCrossSellProductsList = this.crossSellProductsList;
	}
}