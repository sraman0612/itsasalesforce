<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value><!-- Price Dimension --></value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value><!-- Price Dimension --></value>
    </caseValues>
    <fields>
        <help>Controla si el costo de este producto puede modificarse en cotizaciones individuales.</help>
        <label>Costo editable</label>
        <name>SBQQ__CostEditable__c</name>
        <picklistValues>
            <masterLabel>Inherit</masterLabel>
            <translation><!-- Inherit --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No</masterLabel>
            <translation><!-- No --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Yes</masterLabel>
            <translation>Si</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Especifique la cantidad inicial del producto cuando se agregue.</help>
        <label>Cantidad predeterminada</label>
        <name>SBQQ__DefaultQuantity__c</name>
    </fields>
    <fields>
        <help><!-- Discount schedule for use with this dimension --></help>
        <label>Programa de descuento</label>
        <name>SBQQ__DiscountSchedule__c</name>
        <relationshipLabel><!-- Price Dimensions --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Marks this dimension as not discountable. --></help>
        <label>No descontable</label>
        <name>SBQQ__NonDiscountable__c</name>
        <picklistValues>
            <masterLabel>Inherit</masterLabel>
            <translation><!-- Inherit --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No</masterLabel>
            <translation><!-- No --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Yes</masterLabel>
            <translation>Si</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Marks this dimension as non-discountable for partners. --></help>
        <label>No socio descontable</label>
        <name>SBQQ__NonPartnerDiscountable__c</name>
        <picklistValues>
            <masterLabel>Inherit</masterLabel>
            <translation><!-- Inherit --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No</masterLabel>
            <translation><!-- No --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Yes</masterLabel>
            <translation>Si</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Associates this dimension to a particular Price Book --></help>
        <label>Libro de precios</label>
        <name>SBQQ__PriceBook__c</name>
        <relationshipLabel><!-- Price Dimensions --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Indicates this dimension allows for price to be freely editable when building a quote. --></help>
        <label>Precio editable</label>
        <name>SBQQ__PriceEditable__c</name>
        <picklistValues>
            <masterLabel>Inherit</masterLabel>
            <translation><!-- Inherit --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No</masterLabel>
            <translation><!-- No --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Yes</masterLabel>
            <translation>Si</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Select a Product to associate this Price Dimension with. --></help>
        <label>Producto</label>
        <name>SBQQ__Product__c</name>
        <relationshipLabel><!-- Price Dimensions --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Allow or prevent changing the quantity for this dimension. --></help>
        <label>Cantidad editable</label>
        <name>SBQQ__QuantityEditable__c</name>
        <picklistValues>
            <masterLabel>Inherit</masterLabel>
            <translation><!-- Inherit --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No</masterLabel>
            <translation><!-- No --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Yes</masterLabel>
            <translation>Si</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Especifica el número de posiciones decimales que se utilizan en el campo Cantidad.</help>
        <label>Escala de cantidad</label>
        <name>SBQQ__QuantityScale__c</name>
    </fields>
    <fields>
        <help>Indica si este producto está sujeto a impuestos.</help>
        <label>Gravable</label>
        <name>SBQQ__Taxable__c</name>
        <picklistValues>
            <masterLabel>Inherit</masterLabel>
            <translation><!-- Inherit --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No</masterLabel>
            <translation><!-- No --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Yes</masterLabel>
            <translation>Si</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- This is a schedule for discounts based on Subscription Term. Values entered into these fields will override the values entered at the Product level. --></help>
        <label>Programa de descuento por plazo</label>
        <name>SBQQ__TermDiscountSchedule__c</name>
        <relationshipLabel><!-- Price Dimensions (Term Discount Schedule) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Select the type of Price Dimension: One-Time or Yearly. Yearly Dimensions are always treated as Subscriptions. --></help>
        <label>Tipo</label>
        <name>SBQQ__Type__c</name>
        <picklistValues>
            <masterLabel>Custom</masterLabel>
            <translation>Personalizado</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Month</masterLabel>
            <translation><!-- Month --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>One-time</masterLabel>
            <translation>Una vez</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Quarter</masterLabel>
            <translation><!-- Quarter --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Year</masterLabel>
            <translation>Año</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Unit price for this Dimension. This will override the value from the Price Book. --></help>
        <label><!-- Unit Price --></label>
        <name>SBQQ__UnitPrice__c</name>
    </fields>
    
    <nameFieldLabel><!-- Dimension Name --></nameFieldLabel>
    <validationRules>
        <errorMessage><!-- Can’t create MDQ price dimensions for evergreen or renewable/evergreen products. --></errorMessage>
        <name>SBQQ__Cannot_Create_PriceDimension_Evergreen</name>
    </validationRules>
    <webLinks>
        <label>Traducir</label>
        <name>SBQQ__Translate</name>
    </webLinks>
</CustomObjectTranslation>
