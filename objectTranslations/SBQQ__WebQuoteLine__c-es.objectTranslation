<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value><!-- Web Quote Line --></value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value><!-- Web Quote Line --></value>
    </caseValues>
    <fieldSets>
        <label><!-- LineEditor --></label>
        <name>SBQQ__LineEditor</name>
    </fieldSets>
    <fieldSets>
        <label>Editor de líneas segmentadas</label>
        <name>SBQQ__SegmentedLineEditor</name>
    </fieldSets>
    <fieldSets>
        <label>Resumen del editor de líneas segmentadas</label>
        <name>SBQQ__SegmentedLineEditorSummary</name>
    </fieldSets>
    <fields>
        <help>Descuento adicional para el cliente extendido en este artículo de línea en forma de monto absoluto.</help>
        <label>Descuento adicional (Monto)</label>
        <name>SBQQ__AdditionalDiscountAmount__c</name>
    </fields>
    <fields>
        <help>Descuentos adicionales totales extendidos a este artículo de línea. El descuento adicional se define como el descuento extendido sobre el descuento por volumen o los precios negociados.</help>
        <label>Descuento adicional.</label>
        <name>SBQQ__AdditionalDiscount__c</name>
    </fields>
    <fields>
        <help><!-- Additional quantity for this product that contributes to volume discount calculations. --></help>
        <label>Cantidad adicional</label>
        <name>SBQQ__AdditionalQuantity__c</name>
    </fields>
    <fields>
        <help>Si los productos se venden en lotes, esto almacena la cantidad en cada lote, predeterminada del producto.</help>
        <label>Cantidad de lote</label>
        <name>SBQQ__BatchQuantity__c</name>
    </fields>
    <fields>
        <help><!-- Block Price record used to price this line item. --></help>
        <label>Precio del bloque</label>
        <name>SBQQ__BlockPrice__c</name>
        <relationshipLabel><!-- Web Quote Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Indicates this line item represents a package - -  meaning it includes other line items in this quote. --></help>
        <label>Paquete</label>
        <name>SBQQ__Bundle__c</name>
    </fields>
    <fields>
        <help>Para artículos con cantidades calculadas, este campo contiene la cantidad original ingresada por el usuario.</help>
        <label>Cantidad original</label>
        <name>SBQQ__BundledQuantity__c</name>
    </fields>
    <fields>
        <help>Indica que el producto en este artículo de línea está empaquetado (incluido) por otro producto en esta cotización. Los artículos de las líneas agrupadas tienen un precio cero y no permiten editar el precio ni la cantidad.</help>
        <label>Empaquetado</label>
        <name>SBQQ__Bundled__c</name>
    </fields>
    <fields>
        <help>Costo total de los componentes de este paquete.</help>
        <label>Costo del componente</label>
        <name>SBQQ__ComponentCost__c</name>
    </fields>
    <fields>
        <help>Corrige el descuento adicional para este componente al descuento del paquete principal.</help>
        <label>Componente con descuento por paquete</label>
        <name>SBQQ__ComponentDiscountedByPackage__c</name>
    </fields>
    <fields>
        <help><!-- Total list amount of components in this configuration. This field is only populated if this line item references a configurable product. --></help>
        <label>Total de la lista de componentes</label>
        <name>SBQQ__ComponentListTotal__c</name>
    </fields>
    <fields>
        <help><!-- Subscription scope set at the component level. This field overrides the subscription scope copied from the product. --></help>
        <label><!-- Component Subscription Scope --></label>
        <name>SBQQ__ComponentSubscriptionScope__c</name>
        <picklistValues>
            <masterLabel>Components</masterLabel>
            <translation>Componentes</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Entire Hierarchy</masterLabel>
            <translation>Jerarquía completa</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Package</masterLabel>
            <translation>Paquete</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Package &amp; Components</masterLabel>
            <translation>Paquete y componentes</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Sum total amount of components in this configuration. This field is only populated if this line item references a configurable product. --></help>
        <label>Total neto del componente</label>
        <name>SBQQ__ComponentTotal__c</name>
    </fields>
    <fields>
        <help>Corrige el aumento de este componente al aumento del paquete principal. Aumento solo se aplica a productos multisegmentados.</help>
        <label>Componente mejorado por paquete</label>
        <name>SBQQ__ComponentUpliftedByPackage__c</name>
    </fields>
    <fields>
        <help>Indica la visibilidad de la línea de pedido del componente.</help>
        <label>Visibilidad de componentes</label>
        <name>SBQQ__ComponentVisibility__c</name>
    </fields>
    <fields>
        <help>Tasa de descuento compuesta por cantidad (histórico).</help>
        <label>Descuento compuesto (%)</label>
        <name>SBQQ__CompoundDiscountRate__c</name>
    </fields>
    <fields>
        <help><!-- Select the checkbox if the quote line requires configuration. Can be used by custom code to request this product be re-configured. --></help>
        <label>Configuración requerida</label>
        <name>SBQQ__ConfigurationRequired__c</name>
    </fields>
    <fields>
        <help><!-- Contracted price if the product has negotiated pricing for the opportunity&apos;s account. --></help>
        <label>Precio Contratado</label>
        <name>SBQQ__ContractedPrice__c</name>
        <relationshipLabel>Líneas de cotización</relationshipLabel>
    </fields>
    <fields>
        <help>Especifica si el costo se puede editar en este artículo de línea.</help>
        <label>Costo editable</label>
        <name>SBQQ__CostEditable__c</name>
    </fields>
    <fields>
        <help><!-- Cost record used to cost this line item. --></help>
        <label>Costo</label>
        <name>SBQQ__Cost__c</name>
        <relationshipLabel><!-- Web Quote Lines --></relationshipLabel>
    </fields>
    <fields>
        <help>Precio unitario del cliente para el producto cotizado por este artículo de línea. Este es el precio unitario neto excluyendo los descuentos de socios.</help>
        <label>Precio unitario del cliente</label>
        <name>SBQQ__CustomerPrice__c</name>
    </fields>
    <fields>
        <help>Precio total del cliente para este artículo de línea de cotización. Este es el total neto excluyendo los descuentos para socios.</help>
        <label>Total del cliente</label>
        <name>SBQQ__CustomerTotal__c</name>
    </fields>
    <fields>
        <help>Duración de la suscripción, que solo se aplica si el producto es una suscripción. Se copia del Plazo de suscripción del producto relacionado cuando se genera por primera vez el artículo de línea.</help>
        <label>Plazo de suscripción predeterminado</label>
        <name>SBQQ__DefaultSubscriptionTerm__c</name>
    </fields>
    <fields>
        <help>Descripción de esta línea de pedido.</help>
        <label>Descripción</label>
        <name>SBQQ__Description__c</name>
    </fields>
    <fields>
        <help><!-- For MDQ products, each segment will be priced independently. --></help>
        <label>Dimensión de precio</label>
        <name>SBQQ__Dimension__c</name>
        <relationshipLabel><!-- Web Quote Lines --></relationshipLabel>
    </fields>
    <fields>
        <help>Tipo de programa de descuento. Copiado del campo Tipo de programa de descuento asociado.</help>
        <label>Tipo de programa de descuento</label>
        <name>SBQQ__DiscountScheduleType__c</name>
        <picklistValues>
            <masterLabel>Range</masterLabel>
            <translation>Rango</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Slab</masterLabel>
            <translation>Losa</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Discount schedule to be used in computing volume unit price. This field is copied from the corresponding product. --></help>
        <label>Programa de descuento</label>
        <name>SBQQ__DiscountSchedule__c</name>
        <relationshipLabel><!-- Web Quote Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Discount tier used by this line item. This field is only populated if a volume discounted was applied and discount schedule is of type &quot;Range&quot;. --></help>
        <label>Nivel de descuento</label>
        <name>SBQQ__DiscountTier__c</name>
        <relationshipLabel>Líneas de cotización</relationshipLabel>
    </fields>
    <fields>
        <help>Descuento adicional para clientes extendido a este artículo de línea expresado en forma de porcentaje.</help>
        <label>Descuento adicional (%)</label>
        <name>SBQQ__Discount__c</name>
    </fields>
    <fields>
        <help><!-- Distributor discount applied to this line. --></help>
        <label>Descuento para el distribuidor</label>
        <name>SBQQ__DistributorDiscount__c</name>
    </fields>
    <fields>
        <help>Fecha de finalización efectiva de esta suscripción.</help>
        <label>Fecha de finalización efectiva</label>
        <name>SBQQ__EffectiveEndDate__c</name>
    </fields>
    <fields>
        <help><!-- Effective quantity used in computing totals. Same as quantity unless the product is block priced or covered by &quot;Slab&quot; discount schedule. --></help>
        <label>Cantidad efectiva</label>
        <name>SBQQ__EffectiveQuantity__c</name>
    </fields>
    <fields>
        <help>Fecha de inicio efectiva para esta suscripción.</help>
        <label>Fecha de inicio efectiva</label>
        <name>SBQQ__EffectiveStartDate__c</name>
    </fields>
    <fields>
        <help>Fecha en que finalizará el servicio representado por esta línea de pedido (solo se aplica si el producto es una suscripción).</help>
        <label>Fecha final</label>
        <name>SBQQ__EndDate__c</name>
    </fields>
    <fields>
        <help><!-- Select the checkbox if the customer already owns this product. This is typically used for products with options during add-on quoting. The product is on the quote solely to drive the Configurator. --></help>
        <label>Existente</label>
        <name>SBQQ__Existing__c</name>
    </fields>
    <fields>
        <help>Importe de beneficio bruto en este artículo de línea.</help>
        <label>Beneficio bruto</label>
        <name>SBQQ__GrossProfit__c</name>
    </fields>
    <fields>
        <help><!-- Select the checkbox to hide this line item from generated PDF. --></help>
        <label>Oculto</label>
        <name>SBQQ__Hidden__c</name>
    </fields>
    <fields>
        <help><!-- Select the checbox to apply internal application logic, which controls when rules, such as validation rules, trigger. Do NOT add to page layout or otherwise update. --></help>
        <label>Incompleto</label>
        <name>SBQQ__Incomplete__c</name>
    </fields>
    <fields>
        <help><!-- List unit price for the product quoted by this line item. --></help>
        <label>Precio unitario de lista</label>
        <name>SBQQ__ListPrice__c</name>
    </fields>
    <fields>
        <help>Indique el precio total de esta línea de cotización.</help>
        <label>Total de la lista</label>
        <name>SBQQ__ListTotal__c</name>
    </fields>
    <fields>
        <help><!-- Markup on this line item in the form of an absolute amount. --></help>
        <label>Margen (Amt)</label>
        <name>SBQQ__MarkupAmount__c</name>
    </fields>
    <fields>
        <help><!-- Markup on this line item in the form of a percentage. --></help>
        <label>Margen (%)</label>
        <name>SBQQ__MarkupRate__c</name>
    </fields>
    <fields>
        <help><!-- Markup amount on this item. This value is automatically calculated by applying markup rate or absolute amount, whichever is specified. --></help>
        <label>Valor añadido</label>
        <name>SBQQ__Markup__c</name>
    </fields>
    <fields>
        <help><!-- Maximum unit price for this product. Used to enforce price ceiling for products with dynamically calculated prices. --></help>
        <label><!-- Maximum Unit Price --></label>
        <name>SBQQ__MaximumPrice__c</name>
    </fields>
    <fields>
        <help><!-- Minimum unit price for this product. Used to enforce price floor for products with dynamically calculated prices. --></help>
        <label><!-- Minimum Unit Price --></label>
        <name>SBQQ__MinimumPrice__c</name>
    </fields>
    <fields>
        <help><!-- List unit price for the product quoted by this line item. --></help>
        <label>Precio unitario neto</label>
        <name>SBQQ__NetPrice__c</name>
    </fields>
    <fields>
        <help>Precio total neto de esta partida de cotización.</help>
        <label>Total neto</label>
        <name>SBQQ__NetTotal__c</name>
    </fields>
    <fields>
        <help><!-- Select the checkbox to indicate if discounts are allowed on this line item. --></help>
        <label>No descontable</label>
        <name>SBQQ__NonDiscountable__c</name>
    </fields>
    <fields>
        <help><!-- Select the checkbox to make this line item non-discountable for partners. --></help>
        <label>No socio descontable</label>
        <name>SBQQ__NonPartnerDiscountable__c</name>
    </fields>
    <fields>
        <help>Número que indica la posición de esta línea dentro de su grupo o cotización.</help>
        <label>Número</label>
        <name>SBQQ__Number__c</name>
    </fields>
    <fields>
        <help>Descuento extendido para este producto porque es parte de un paquete.</help>
        <label>Descuento de opción (Amt)</label>
        <name>SBQQ__OptionDiscountAmount__c</name>
    </fields>
    <fields>
        <help>Si esta línea es para un SKU opcional, este campo captura cualquier descuento dado al paquete y se extendió a la opción.</help>
        <label>Descuento de opción (%)</label>
        <name>SBQQ__OptionDiscount__c</name>
    </fields>
    <fields>
        <help>Indica el nivel de nido de esta opción (solo se aplica a las líneas que se generan a partir de opciones).</help>
        <label>Nivel de opción</label>
        <name>SBQQ__OptionLevel__c</name>
    </fields>
    <fields>
        <help><!-- Option type copied from related Product Option. Determines how quantity is calculated. --></help>
        <label>Tipo de opción</label>
        <name>SBQQ__OptionType__c</name>
        <picklistValues>
            <masterLabel>Accessory</masterLabel>
            <translation>Accesorio</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Component</masterLabel>
            <translation>Componente</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Select the checkbox to make this line optional. Optional line items don&apos;t contribute to quote totals and are not transferred to the Opportunity. --></help>
        <label>Opcional</label>
        <name>SBQQ__Optional__c</name>
    </fields>
    <fields>
        <help>Costo total de este paquete, incluido el costo de sus componentes.</help>
        <label>Costo del paquete</label>
        <name>SBQQ__PackageCost__c</name>
    </fields>
    <fields>
        <help>Precio de lista total de este paquete, incluidos sus componentes.</help>
        <label>Total de la lista de paquetes</label>
        <name>SBQQ__PackageListTotal__c</name>
    </fields>
    <fields>
        <help>Lista generada automáticamente de códigos de producto para cada componente de este paquete.</help>
        <label>Código de producto del paquete</label>
        <name>SBQQ__PackageProductCode__c</name>
    </fields>
    <fields>
        <help>Precio total de este paquete incluidos sus componentes.</help>
        <label>Total del paquete</label>
        <name>SBQQ__PackageTotal__c</name>
    </fields>
    <fields>
        <help>Los descuentos para socios se aplican después de todos los descuentos automáticos y discrecionales. El descuento de socio se tiene en cuenta en el precio de socio.</help>
        <label>Descuento de Socio</label>
        <name>SBQQ__PartnerDiscount__c</name>
    </fields>
    <fields>
        <help>Precio unitario del socio. Precio después del descuento del socio pero antes del descuento del distribuidor.</help>
        <label>Precio unitario del socio</label>
        <name>SBQQ__PartnerPrice__c</name>
    </fields>
    <fields>
        <help>Total de socios</help>
        <label>Total de socios</label>
        <name>SBQQ__PartnerTotal__c</name>
    </fields>
    <fields>
        <help><!-- Select the checkbox to indicate that this line item&apos;s price is editable. --></help>
        <label>Precio editable</label>
        <name>SBQQ__PriceEditable__c</name>
    </fields>
    <fields>
        <help><!-- Select the checkbox to specify that the pricing method can be edited on this line item. --></help>
        <label>Método de fijación de precios editable</label>
        <name>SBQQ__PricingMethodEditable__c</name>
    </fields>
    <fields>
        <help><!-- Indicates how the price for this line item is calculated: List, by subtracting discount from list price or Cost, by adding markup to cost. --></help>
        <label>Método de calculo de precio</label>
        <name>SBQQ__PricingMethod__c</name>
        <picklistValues>
            <masterLabel>Block</masterLabel>
            <translation>Bloquear</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cost</masterLabel>
            <translation>Costo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Custom</masterLabel>
            <translation>Personalizado</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>List</masterLabel>
            <translation>Lista</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Cantidad combinada de este producto de compras anteriores.</help>
        <label>Cantidad previa</label>
        <name>SBQQ__PriorQuantity__c</name>
    </fields>
    <fields>
        <help>Código del producto al que hace referencia esta línea.</help>
        <label><!-- Product Code --></label>
        <name>SBQQ__ProductCode__c</name>
    </fields>
    <fields>
        <help><!-- Product Family for the product referenced by this line item. --></help>
        <label>Familia de productos</label>
        <name>SBQQ__ProductFamily__c</name>
    </fields>
    <fields>
        <help>Nombre del producto al que hace referencia este artículo de línea.</help>
        <label>nombre del producto</label>
        <name>SBQQ__ProductName__c</name>
    </fields>
    <fields>
        <help>Opción de producto que generó esta línea (si la hubiera).</help>
        <label>Opción de producto</label>
        <name>SBQQ__ProductOption__c</name>
        <relationshipLabel>Líneas de cotización</relationshipLabel>
    </fields>
    <fields>
        <help>Producto cotizado por este artículo de línea.</help>
        <label>Producto</label>
        <name>SBQQ__Product__c</name>
        <relationshipLabel><!-- Web Quote Lines --></relationshipLabel>
    </fields>
    <fields>
        <help>Relación calculada utilizada para calcular el precio prorrateado.</help>
        <label>Multiplicador prorrateado</label>
        <name>SBQQ__ProrateMultiplier__c</name>
    </fields>
    <fields>
        <help>Precio unitario de lista prorrateado.</help>
        <label>Precio de lista prorrateado</label>
        <name>SBQQ__ProratedListPrice__c</name>
    </fields>
    <fields>
        <help>Precio prorrateado del producto cotizado por este artículo de línea. Este precio solo diferirá del precio especial si el producto es una suscripción y el artículo de línea se prorratea.</help>
        <label>Precio unitario prorrateado</label>
        <name>SBQQ__ProratedPrice__c</name>
    </fields>
    <fields>
        <help><!-- Quantity of the line item being quoted. --></help>
        <label>Cantidad</label>
        <name>SBQQ__Quantity__c</name>
    </fields>
    <fields>
        <help><!-- Parent quote record. --></help>
        <label>Cotización</label>
        <name>SBQQ__Quote__c</name>
        <relationshipLabel>Líneas de cotización</relationshipLabel>
    </fields>
    <fields>
        <help><!-- Unit price before additional discounts are applied. --></help>
        <label>Precio unitario regular</label>
        <name>SBQQ__RegularPrice__c</name>
    </fields>
    <fields>
        <label>Total regular</label>
        <name>SBQQ__RegularTotal__c</name>
    </fields>
    <fields>
        <help>Indica que este artículo de línea representa un producto que se está renovando. Este campo no debe modificarse.</help>
        <label>Renovación</label>
        <name>SBQQ__Renewal__c</name>
    </fields>
    <fields>
        <help><!-- Asset being renewed if this line item represents a product renewal. --></help>
        <label>Activo renovado</label>
        <name>SBQQ__RenewedAsset__c</name>
        <relationshipLabel>Líneas de cotización</relationshipLabel>
    </fields>
    <fields>
        <help>Suscripción renovada por esta línea de cotización.</help>
        <label>Suscripción renovada</label>
        <name>SBQQ__RenewedSubscription__c</name>
        <relationshipLabel><!-- Web Quote Lines --></relationshipLabel>
    </fields>
    <fields>
        <help>Indica qué otro producto de esta cotización lo requiere.</help>
        <label>Requerido por</label>
        <name>SBQQ__RequiredBy__c</name>
        <relationshipLabel>Líneas de cotización</relationshipLabel>
    </fields>
    <fields>
        <help>Para productos multi segmentados, índice que representa la posición de esta línea de cotización en la tabla de segmentos.</help>
        <label>Índice de segmento</label>
        <name>SBQQ__SegmentIndex__c</name>
    </fields>
    <fields>
        <help><!-- This represents the segment that contains a unique value. --></help>
        <label>Clave de segmento</label>
        <name>SBQQ__SegmentKey__c</name>
    </fields>
    <fields>
        <help><!-- This is the Column header label associated with this line. --></help>
        <label>Etiqueta de segmento</label>
        <name>SBQQ__SegmentLabel__c</name>
    </fields>
    <fields>
        <help><!-- Source quote line item of cloned quote line item, if any. --></help>
        <label>Fuente</label>
        <name>SBQQ__Source__c</name>
        <relationshipLabel><!-- Web Quote Lines (Source) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Name of the Discount Tier or Contacted Price applied to get the special price for this line item. --></help>
        <label>Precio especial Descripción</label>
        <name>SBQQ__SpecialPriceDescription__c</name>
    </fields>
    <fields>
        <help><!-- Type of special pricing extended. --></help>
        <label>Tipo de precio especial</label>
        <name>SBQQ__SpecialPriceType__c</name>
        <picklistValues>
            <masterLabel>Contracted Price</masterLabel>
            <translation>Precio Contratado</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Custom</masterLabel>
            <translation>Personalizado</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Renewal</masterLabel>
            <translation>Renovación</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Volume Discount</masterLabel>
            <translation>Descuento por volumen</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Precio unitario especial para este artículo de línea, como descuento por volumen o precio contratado.</help>
        <label>Precio especial</label>
        <name>SBQQ__SpecialPrice__c</name>
    </fields>
    <fields>
        <help><!-- The date the subscription for which this product begins. --></help>
        <label>Fecha de inicio</label>
        <name>SBQQ__StartDate__c</name>
    </fields>
    <fields>
        <help>Lista de activos cubiertos por esta línea de pedido de suscripción. No actualice a menos que se le indique.</help>
        <label>ID de activos suscritos</label>
        <name>SBQQ__SubscribedAssetIds__c</name>
    </fields>
    <fields>
        <help><!-- Indicates if subscription pricing uses List, Customer, or Net price. --></help>
        <label><!-- Subscription Base --></label>
        <name>SBQQ__SubscriptionBase__c</name>
    </fields>
    <fields>
        <help><!-- Category defined on the product to limit percent-of-total calculations to certain products. --></help>
        <label><!-- Subscription Category --></label>
        <name>SBQQ__SubscriptionCategory__c</name>
        <picklistValues>
            <masterLabel>Hardware</masterLabel>
            <translation><!-- Hardware --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Software</masterLabel>
            <translation><!-- Software --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Percent of non-subscription product total to be used when calculating this line item price. --></help>
        <label><!-- Subscription Percent --></label>
        <name>SBQQ__SubscriptionPercent__c</name>
    </fields>
    <fields>
        <help><!-- Determines how the subscription price is calculated. If this is blank, the line items is not a subscription product. --></help>
        <label>Precios de suscripción</label>
        <name>SBQQ__SubscriptionPricing__c</name>
        <picklistValues>
            <masterLabel>Fixed Price</masterLabel>
            <translation>Precio fijo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Percent Of Total</masterLabel>
            <translation>Porcentaje del total</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Determines how dynamic subscription prices are calculated: Quote uses all the non-subscription products in the quote while Group uses only non-subscription products in the same group. --></help>
        <label><!-- Subscription Scope --></label>
        <name>SBQQ__SubscriptionScope__c</name>
        <picklistValues>
            <masterLabel>Group</masterLabel>
            <translation>Grupo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Quote</masterLabel>
            <translation>Cotización</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Price of the subscription target product on which maintenance should calculate. --></help>
        <label><!-- Subscription Target Price --></label>
        <name>SBQQ__SubscriptionTargetPrice__c</name>
    </fields>
    <fields>
        <help><!-- Term for the subscription product in this line. If the product is not a subscription this value has no effect. --></help>
        <label>Plazo de suscripción</label>
        <name>SBQQ__SubscriptionTerm__c</name>
    </fields>
    <fields>
        <help><!-- Select the checkbox to indicate that the line item is taxable. --></help>
        <label>Gravable</label>
        <name>SBQQ__Taxable__c</name>
    </fields>
    <fields>
        <help>Programe descuentos según el plazo de suscripción.</help>
        <label>Programa de descuento por plazo</label>
        <name>SBQQ__TermDiscountSchedule__c</name>
        <relationshipLabel><!-- Web Quote Lines (Term Discounted) --></relationshipLabel>
    </fields>
    <fields>
        <help>Nivel dentro del programa de descuento de término utilizado para descontar este artículo de línea.</help>
        <label>Nivel de descuento por plazo</label>
        <name>SBQQ__TermDiscountTier__c</name>
        <relationshipLabel><!-- Web Quote Lines (Term Discounted) --></relationshipLabel>
    </fields>
    <fields>
        <help>El descuento se calcula aplicando el plazo de suscripción contra el programa de descuentos por plazo.</help>
        <label>Descuento por plazo</label>
        <name>SBQQ__TermDiscount__c</name>
    </fields>
    <fields>
        <help>Descuento total para este artículo de línea, incluido el descuento por volumen, el descuento contratado y el descuento adicional.</help>
        <label>Descuento total (Amt)</label>
        <name>SBQQ__TotalDiscountAmount__c</name>
    </fields>
    <fields>
        <help>Costo de una unidad de producto cotizada en este artículo de línea.</help>
        <label>Costo unitario</label>
        <name>SBQQ__UnitCost__c</name>
    </fields>
    <fields>
        <help><!-- Asset upgraded by this quote line. When the quote is contracted this asset&apos;s Usage End Date will be populated to indicate that it&apos;s retired. --></help>
        <label>Activo actualizado</label>
        <name>SBQQ__UpgradedAsset__c</name>
        <relationshipLabel><!-- Web Quote Lines (Upgrade) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Subscription upgraded by this quote line. When the quote is contracted this subscription&apos;s Terminated Date field will be populated. --></help>
        <label>Suscripción mejorada</label>
        <name>SBQQ__UpgradedSubscription__c</name>
        <relationshipLabel><!-- Web Quote Lines (Upgrade) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- This is measured as a currency amount. --></help>
        <label>Aumento (Amt)</label>
        <name>SBQQ__UpliftAmount__c</name>
    </fields>
    <fields>
        <help><!-- Enter this as a percent. --></help>
        <label>Elevar</label>
        <name>SBQQ__Uplift__c</name>
    </fields>
    <fields>
        <help><!-- Volume discount amount applied to this line item. --></help>
        <label>Descuento por volumen</label>
        <name>SBQQ__VolumeDiscount__c</name>
    </fields>
    
    <nameFieldLabel><!-- Quote Line # --></nameFieldLabel>
</CustomObjectTranslation>
