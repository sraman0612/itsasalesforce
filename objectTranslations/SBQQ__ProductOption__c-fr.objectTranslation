<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Option de produit</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Options de produit</value>
    </caseValues>
    <fieldSets>
        <label><!-- AccessoryConfigurator --></label>
        <name>AccessoryConfigurator</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Compressor --></label>
        <name>Compressor</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Mobile Machines --></label>
        <name>Mobile_Machines</name>
    </fieldSets>
    <fieldSets>
        <label><!-- PD Blower --></label>
        <name>PD_Blower</name>
    </fieldSets>
    <fieldSets>
        <label><!-- OptionConfiguration --></label>
        <name>SBQQ__OptionConfiguration</name>
    </fieldSets>
    <fieldSets>
        <label><!-- OptionLookup --></label>
        <name>SBQQ__OptionLookup</name>
    </fieldSets>
    <fieldSets>
        <label><!-- SearchFilters --></label>
        <name>SBQQ__SearchFilters</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Test --></label>
        <name>Test</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Vacuum --></label>
        <name>Vacuum</name>
    </fieldSets>
    <fields>
        <label><!-- Feature Name --></label>
        <name>Feature_Name__c</name>
    </fields>
    <fields>
        <label><!-- Product Image --></label>
        <name>Product_Image__c</name>
    </fields>
    <fields>
        <help>L&apos;option « Appliquer immédiatement lors de la sélection » applique les modifications immédiatement après la sélection du produit, alors que l&apos;option « Appliquer immédiatement lors d&apos;un changement de quantité » applique les modifications lorsque la quantité change. L&apos;option « Toujours » applique les modifications dans les deux cas, lors de la sélection du produit et lors d&apos;un changement de quantité.</help>
        <label><!-- Apply Immediately Context --></label>
        <name>SBQQ__AppliedImmediatelyContext__c</name>
        <picklistValues>
            <masterLabel>Always</masterLabel>
            <translation>Toujours</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>On Quantity Change</masterLabel>
            <translation>Lors d&apos;un changement de quantité</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>On Selection</masterLabel>
            <translation>Sur sélection</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Sélectionnez la case pour appliquer immédiatement les modifications à cette option. À utiliser avec précaution afin d&apos;éviter de ralentir l&apos;expérience utilisateur.</help>
        <label><!-- Apply Immediately --></label>
        <name>SBQQ__AppliedImmediately__c</name>
    </fields>
    <fields>
        <help>Sélectionnez cette case pour indiquer que le produit associé est groupé avec le produit principal. Les options d&apos;offres groupées ont une quantité fixe et un prix nul, car le prix est inclus dans le produit principal. Laissez cette case désactivée si le prix doit être ajouté à l&apos;option.</help>
        <label><!-- Bundled --></label>
        <name>SBQQ__Bundled__c</name>
    </fields>
    <fields>
        <help>Position de ce composant dans le code produit généré pour la configuration.</help>
        <label><!-- Component Code Position --></label>
        <name>SBQQ__ComponentCodePosition__c</name>
    </fields>
    <fields>
        <help>Code du composant représenté par cette option.</help>
        <label><!-- Component Code --></label>
        <name>SBQQ__ComponentCode__c</name>
    </fields>
    <fields>
        <help>Position de ce composant dans la description produit générée pour la configuration.</help>
        <label><!-- Component Description Position --></label>
        <name>SBQQ__ComponentDescriptionPosition__c</name>
    </fields>
    <fields>
        <help>Description du composant représenté par cette option. Il est utilisé avec le champ Modèle de description configuré dans l&apos;objet Produit.</help>
        <label><!-- Component Description --></label>
        <name>SBQQ__ComponentDescription__c</name>
    </fields>
    <fields>
        <help>Unité de gestion de stock du produit de l&apos;offre groupée configurée avec cette option. Si vous créez l&apos;option depuis la page de détail du produit, elle doit déjà être renseignée.</help>
        <label><!-- Configured SKU --></label>
        <name>SBQQ__ConfiguredSKU__c</name>
        <relationshipLabel><!-- Options --></relationshipLabel>
    </fields>
    <fields>
        <help>Sélectionnez le tableau de tarification de ce produit à afficher par défaut lors de la définition des dimensions de prix.</help>
        <label><!-- Default Pricing Table --></label>
        <name>SBQQ__DefaultPricingTable__c</name>
        <picklistValues>
            <masterLabel>Segmented</masterLabel>
            <translation>Segmenté</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Standard</masterLabel>
            <translation>Standard</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Remise (montant) basée sur le champ, lorsque le produit référencé pour cette option est offert seul. Cependant, vous devez étendre une remise si le produit fait partie d&apos;une offre groupée.</help>
        <label><!-- Discount (Amt) --></label>
        <name>SBQQ__DiscountAmount__c</name>
    </fields>
    <fields>
        <help>Référence le barème de remise à appliquer au produit sélectionné par cette option. Ce barème de remise remplace le barème de remise sélectionné dans le produit ou la fonctionnalité.</help>
        <label><!-- Discount Schedule --></label>
        <name>SBQQ__DiscountSchedule__c</name>
        <relationshipLabel><!-- Product Options --></relationshipLabel>
    </fields>
    <fields>
        <help>Saisissez une remise (%) basée sur le champ, lorsque le produit référencé pour cette option est offert seul. Cependant, vous devez étendre une remise si le produit fait partie d&apos;une offre groupée.</help>
        <label><!-- Discount (%) --></label>
        <name>SBQQ__Discount__c</name>
    </fields>
    <fields>
        <help>Sélectionnez cette case pour appliquer automatiquement une remise supplémentaire au package parent de ce composant. Seule une remise en pourcentage peut être appliquée.</help>
        <label><!-- Discounted By Package --></label>
        <name>SBQQ__DiscountedByPackage__c</name>
    </fields>
    <fields>
        <help>Quantité existante de ce produit. Calculée en référence les ressources associées au compte.</help>
        <label><!-- Existing Quantity --></label>
        <name>SBQQ__ExistingQuantity__c</name>
    </fields>
    <fields>
        <help>Saisissez ou référencez la fonctionnalité qui inclut cette option. Elle dépend de l&apos;unité de gestion de stock configurée.</help>
        <label><!-- Feature --></label>
        <name>SBQQ__Feature__c</name>
        <relationshipLabel><!-- Product Options --></relationshipLabel>
    </fields>
    <fields>
        <help>Quantité maximale autorisée pour cette option.</help>
        <label><!-- Max Quantity --></label>
        <name>SBQQ__MaxQuantity__c</name>
    </fields>
    <fields>
        <help>Quantité minimale autorisée pour cette option.</help>
        <label><!-- Min Quantity --></label>
        <name>SBQQ__MinQuantity__c</name>
    </fields>
    <fields>
        <help>Définit l&apos;ordre d&apos;affichage de cette option dans la fonctionnalité à laquelle elle est associée.</help>
        <label><!-- Number --></label>
        <name>SBQQ__Number__c</name>
    </fields>
    <fields>
        <help>Saisissez ou référencez l&apos;unité de gestion de stock du produit qui doit être ajoutée lors de la sélection de ce produit.</help>
        <label><!-- Optional SKU --></label>
        <name>SBQQ__OptionalSKU__c</name>
        <relationshipLabel><!-- Optional For --></relationshipLabel>
    </fields>
    <fields>
        <help>Sélectionnez cette case pour marquer le prix comme modifiable, ce qui autorise les utilisateurs à apporter des modifications aux prix des options.</help>
        <label><!-- Price Editable --></label>
        <name>SBQQ__PriceEditable__c</name>
    </fields>
    <fields>
        <help>Code produit de l&apos;unité de gestion de stock produit facultative.</help>
        <label><!-- Product Code --></label>
        <name>SBQQ__ProductCode__c</name>
    </fields>
    <fields>
        <help>Extrait la valeur du Type de configuration de l&apos;objet Produit afin d&apos;éviter le chargement d&apos;un enregistrement produit associé.</help>
        <label><!-- Product Configuration Type --></label>
        <name>SBQQ__ProductConfigurationType__c</name>
    </fields>
    <fields>
        <help>Description de l&apos;unité de gestion de stock facultative.</help>
        <label><!-- Product Description --></label>
        <name>SBQQ__ProductDescription__c</name>
    </fields>
    <fields>
        <help>Famille de produits de l&apos;unité de gestion de stock produit facultative.</help>
        <label><!-- Product Family --></label>
        <name>SBQQ__ProductFamily__c</name>
    </fields>
    <fields>
        <help>Nom de l&apos;unité de gestion de stock facultative.</help>
        <label><!-- Product Name --></label>
        <name>SBQQ__ProductName__c</name>
    </fields>
    <fields>
        <help>Spécifie le nombre de décimales utilisées dans le champ Quantité.</help>
        <label><!-- Product Quantity Scale --></label>
        <name>SBQQ__ProductQuantityScale__c</name>
    </fields>
    <fields>
        <help>Type de tarification à utiliser pour cet abonnement. « Aucun » signifie que ce produit est sans abonnement.</help>
        <label><!-- Product Subscription Pricing --></label>
        <name>SBQQ__ProductSubscriptionPricing__c</name>
    </fields>
    <fields>
        <help>Sélectionnez cette case pour autoriser la modification de la quantité, même lorsque cela est spécifié sur l&apos;option.</help>
        <label><!-- Quantity Editable --></label>
        <name>SBQQ__QuantityEditable__c</name>
    </fields>
    <fields>
        <help>Quantité du produit associé qui doit être ajouté au devis.</help>
        <label><!-- Quantity --></label>
        <name>SBQQ__Quantity__c</name>
    </fields>
    <fields>
        <help>Configurez la visibilité de la ligne de devis générée par cette option.</help>
        <label><!-- Quote Line Visibility --></label>
        <name>SBQQ__QuoteLineVisibility__c</name>
        <picklistValues>
            <masterLabel>Always</masterLabel>
            <translation>Toujours</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Document Only</masterLabel>
            <translation>Document uniquement</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Editor Only</masterLabel>
            <translation>Éditeur uniquement</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Never</masterLabel>
            <translation>Jamais</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Option produit qui remplace cette option produit lors d&apos;un renouvellement. Utilisez-la pour générer un devis avec une Unité de gestion de stock différente lors du renouvellement.</help>
        <label><!-- Renewal Product Option --></label>
        <name>SBQQ__RenewalProductOption__c</name>
        <relationshipLabel><!-- Product Options --></relationshipLabel>
    </fields>
    <fields>
        <help>Sélectionnez cette case si ce produit est requis dans l&apos;offre groupée.</help>
        <label><!-- Required --></label>
        <name>SBQQ__Required__c</name>
    </fields>
    <fields>
        <help>Sélectionnez cette case si cette option produit doit être automatiquement sélectionnée par défaut.</help>
        <label><!-- Selected --></label>
        <name>SBQQ__Selected__c</name>
    </fields>
    <fields>
        <help>Sélectionnez le mode de calcul de l’option de pourcentage du total des produits. « Package » est spécifique au produit parent. « Composants » est spécifique aux composants semblables. « Hiérarchie complète » correspond à toutes les lignes de la configuration.</help>
        <label><!-- Percent of Total Scope --></label>
        <name>SBQQ__SubscriptionScope__c</name>
        <picklistValues>
            <masterLabel>Components</masterLabel>
            <translation>Composants</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Entire Hierarchy</masterLabel>
            <translation>Hiérarchie complète</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Package</masterLabel>
            <translation>Package</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Package &amp; Components</masterLabel>
            <translation>Package et composants</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Sélectionnez la case pour indiquer que cette option est gérée par le système, ce qui signifie que l&apos;utilisateur ne peut plus la sélectionner.</help>
        <label><!-- System --></label>
        <name>SBQQ__System__c</name>
    </fields>
    <fields>
        <help>Utilisez « Composant » si cette option, y compris la quantité, dépend du parent. Utilisez « Accessoire » si cette option dépend du parent, mais que la quantité est indépendante. Utilisez « Produit associé » pour associée, mais totalement indépendante des produits.</help>
        <label><!-- Type --></label>
        <name>SBQQ__Type__c</name>
        <picklistValues>
            <masterLabel>Accessory</masterLabel>
            <translation>Accessoire</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Component</masterLabel>
            <translation>Composant</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Related Product</masterLabel>
            <translation>Produit associé</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Prix unitaire de cette option. Cela remplace la valeur du catalogue de prix.</help>
        <label><!-- Unit Price --></label>
        <name>SBQQ__UnitPrice__c</name>
    </fields>
    <fields>
        <help>Sélectionnez cette case pour appliquer automatiquement l&apos;augmentation à partir du package parent de ce composant. S&apos;applique uniquement aux composants qui sont des produits à segments multiples.</help>
        <label><!-- Uplifted By Package --></label>
        <name>SBQQ__UpliftedByPackage__c</name>
    </fields>
    <fields>
        <label><!-- BL Adapter Type --></label>
        <name>adapter_type__c</name>
        <picklistValues>
            <masterLabel>45° Union Elbows W/Couplings</masterLabel>
            <translation><!-- 45° Union Elbows W/Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>90° Union Elbows W/couplings</masterLabel>
            <translation><!-- 90° Union Elbows W/couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Adapters</masterLabel>
            <translation><!-- Adapters --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Equal Tee W/Couplings</masterLabel>
            <translation><!-- Equal Tee W/Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Flange Adapters</masterLabel>
            <translation><!-- Flange Adapters --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Reducer</masterLabel>
            <translation><!-- Reducer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Reducer Union Couplings</masterLabel>
            <translation><!-- Reducer Union Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Reducing Tee W/Couplings</masterLabel>
            <translation><!-- Reducing Tee W/Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Straight Union Couplings</masterLabel>
            <translation><!-- Straight Union Couplings --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Application --></label>
        <name>application_ca__c</name>
        <picklistValues>
            <masterLabel>Dry Bulk</masterLabel>
            <translation><!-- Dry Bulk --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Hydraulic</masterLabel>
            <translation><!-- Hydraulic --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Liquid Bulk</masterLabel>
            <translation><!-- Liquid Bulk --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mobile Vac</masterLabel>
            <translation><!-- Mobile Vac --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- BL Bracket Type --></label>
        <name>bl_bracket_type__c</name>
        <picklistValues>
            <masterLabel>Butterfly Valve-Tube to Tube W/ Couplings</masterLabel>
            <translation><!-- Butterfly Valve-Tube to Tube W/ Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ELEVATION TUBE to ANSI  Flange Kit w/ Couplings</masterLabel>
            <translation><!-- ELEVATION TUBE to ANSI  Flange Kit w/ Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Hanging - Bracket</masterLabel>
            <translation><!-- Hanging - Bracket --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Hanging System</masterLabel>
            <translation><!-- Hanging System --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Outlet, Saddle Clamp</masterLabel>
            <translation><!-- Outlet, Saddle Clamp --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Plug - Cap End Fitting</masterLabel>
            <translation><!-- Plug - Cap End Fitting --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Wall Bracket Kit</masterLabel>
            <translation><!-- Wall Bracket Kit --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- BL Coupling Type --></label>
        <name>bl_coupling_type__c</name>
        <picklistValues>
            <masterLabel>45° Union Elbows  W/Couplings</masterLabel>
            <translation><!-- 45° Union Elbows  W/Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>90° Union Elbows W/couplings</masterLabel>
            <translation><!-- 90° Union Elbows W/couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Equal Tee  W/Couplings</masterLabel>
            <translation><!-- Equal Tee  W/Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Reducer</masterLabel>
            <translation><!-- Reducer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Reducer Union Couplings</masterLabel>
            <translation><!-- Reducer Union Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Reducing Tee W/Couplings</masterLabel>
            <translation><!-- Reducing Tee W/Couplings --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Straight Union Couplings</masterLabel>
            <translation><!-- Straight Union Couplings --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Big Lock Tubing Size --></label>
        <name>bl_tubing_size__c</name>
        <picklistValues>
            <masterLabel>115mm (4&quot;)</masterLabel>
            <translation><!-- 115mm (4&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>168mm (6&quot;)</masterLabel>
            <translation><!-- 168mm (6&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>220mm (8&quot;)</masterLabel>
            <translation><!-- 220mm (8&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>273mm (10&quot;)</masterLabel>
            <translation><!-- 273mm (10&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>70mm (2.5&quot;)</masterLabel>
            <translation><!-- 70mm (2.5&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>90mm (3&quot;)</masterLabel>
            <translation><!-- 90mm (3&quot;) --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Clamp &amp; Bracket Type --></label>
        <name>clamp_bracket_type__c</name>
        <picklistValues>
            <masterLabel>45° Double Outlet Elbow NPT x NPT w/ Mtg Bracket</masterLabel>
            <translation><!-- 45° Double Outlet Elbow NPT x NPT w/ Mtg Bracket --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>45° Double Outlet Elbow Tube x NPT w/ Mtg Bracket</masterLabel>
            <translation><!-- 45° Double Outlet Elbow Tube x NPT w/ Mtg Bracket --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>45° Double Outlet Elbow Tube x NPT w/ Valve &amp; Mtg Bracket</masterLabel>
            <translation><!-- 45° Double Outlet Elbow Tube x NPT w/ Valve &amp; Mtg Bracket --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Beam Clamp</masterLabel>
            <translation><!-- Beam Clamp --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Bracket, Hanging (3/8&quot; insert)</masterLabel>
            <translation><!-- Bracket, Hanging (3/8&quot; insert) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Bracket, Strut</masterLabel>
            <translation><!-- Bracket, Strut --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Bracket,Wall</masterLabel>
            <translation><!-- Bracket,Wall --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cantilever Bracket</masterLabel>
            <translation><!-- Cantilever Bracket --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Compact Saddle Clamp</masterLabel>
            <translation><!-- Compact Saddle Clamp --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Compact Saddle Drill Jig</masterLabel>
            <translation><!-- Compact Saddle Drill Jig --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Double Outlet Elbow c/w Mtg Bracket</masterLabel>
            <translation><!-- Double Outlet Elbow c/w Mtg Bracket --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Drain Assembly</masterLabel>
            <translation><!-- Drain Assembly --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Outlet Elbow c/w Mtg Bracket</masterLabel>
            <translation><!-- Outlet Elbow c/w Mtg Bracket --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Wire Hanging System</masterLabel>
            <translation><!-- Wire Hanging System --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Connector Type --></label>
        <name>connector_type__c</name>
        <picklistValues>
            <masterLabel>45° Union Elbows</masterLabel>
            <translation><!-- 45° Union Elbows --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>90° Swivel Elbows</masterLabel>
            <translation><!-- 90° Swivel Elbows --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>90° Union Elbows</masterLabel>
            <translation><!-- 90° Union Elbows --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Equal Tee</masterLabel>
            <translation><!-- Equal Tee --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Male Threaded Connectors</masterLabel>
            <translation><!-- Male Threaded Connectors --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Outlet Tee (With Water Trap Insert)</masterLabel>
            <translation><!-- Outlet Tee (With Water Trap Insert) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Straight Unions</masterLabel>
            <translation><!-- Straight Unions --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Tee Swivel</masterLabel>
            <translation><!-- Tee Swivel --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Discharge Location --></label>
        <name>discharge_location_ca__c</name>
        <picklistValues>
            <masterLabel>Bottom Discharge</masterLabel>
            <translation><!-- Bottom Discharge --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Center Timed</masterLabel>
            <translation><!-- Center Timed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Left Discharge</masterLabel>
            <translation><!-- Left Discharge --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Right Discharge</masterLabel>
            <translation><!-- Right Discharge --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Top Discharge</masterLabel>
            <translation><!-- Top Discharge --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Distribution Channel --></label>
        <name>distribution_channel__c</name>
    </fields>
    <fields>
        <label><!-- Fitting &amp; Adapter Type --></label>
        <name>fitting_adapter_type__c</name>
        <picklistValues>
            <masterLabel>Ball Valves - Tube to NPT</masterLabel>
            <translation><!-- Ball Valves - Tube to NPT --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ball Valves-NPT</masterLabel>
            <translation><!-- Ball Valves-NPT --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ball Valves-Tube to Tube</masterLabel>
            <translation><!-- Ball Valves-Tube to Tube --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Outlet Manifold</masterLabel>
            <translation><!-- Outlet Manifold --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pet Cock Drains</masterLabel>
            <translation><!-- Pet Cock Drains --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Plug - Cap End Fitting</masterLabel>
            <translation><!-- Plug - Cap End Fitting --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pressure Gauge</masterLabel>
            <translation><!-- Pressure Gauge --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Quick Couplers</masterLabel>
            <translation><!-- Quick Couplers --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Reducer - Fitting Body to Tube</masterLabel>
            <translation><!-- Reducer - Fitting Body to Tube --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Safety Coupler Universal Socket</masterLabel>
            <translation><!-- Safety Coupler Universal Socket --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Stem Adapters</masterLabel>
            <translation><!-- Stem Adapters --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Two-way Outlet Y Adaptor</masterLabel>
            <translation><!-- Two-way Outlet Y Adaptor --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- FLD Distribution Channel --></label>
        <name>fld_distribution_channel__c</name>
    </fields>
    <fields>
        <label><!-- Gear Size --></label>
        <name>gear_size_ca__c</name>
    </fields>
    <fields>
        <label><!-- Is Option --></label>
        <name>is_option__c</name>
    </fields>
    <fields>
        <label><!-- Optional Sku Active --></label>
        <name>optional_sku_active__c</name>
    </fields>
    <fields>
        <label><!-- Pipe Size --></label>
        <name>pipe_size__c</name>
    </fields>
    <fields>
        <label><!-- Piping Color --></label>
        <name>piping_color__c</name>
        <picklistValues>
            <masterLabel>Black</masterLabel>
            <translation><!-- Black --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Blue</masterLabel>
            <translation><!-- Blue --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Green</masterLabel>
            <translation><!-- Green --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Grey</masterLabel>
            <translation><!-- Grey --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Specify the pressure as a number only --></help>
        <label><!-- Pressure capability --></label>
        <name>pressure_capability_ca__c</name>
    </fields>
    <fields>
        <label><!-- Product Name --></label>
        <name>product_display_name__c</name>
    </fields>
    <fields>
        <label><!-- PSI --></label>
        <name>psi__c</name>
    </fields>
    <fields>
        <label><!-- Quick Lock Tubing Size --></label>
        <name>ql_tubing_size__c</name>
        <picklistValues>
            <masterLabel>14mm (0.5&quot;)</masterLabel>
            <translation><!-- 14mm (0.5&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>20mm (0.75&quot;)</masterLabel>
            <translation><!-- 20mm (0.75&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>25mm (1&quot;)</masterLabel>
            <translation><!-- 25mm (1&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>32mm (1.25&quot;)</masterLabel>
            <translation><!-- 32mm (1.25&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>40mm (1.5&quot;)</masterLabel>
            <translation><!-- 40mm (1.5&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>50mm (2&quot;)</masterLabel>
            <translation><!-- 50mm (2&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>63mm (2.5&quot;)</masterLabel>
            <translation><!-- 63mm (2.5&quot;) --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Quick Lock Tubing Type --></label>
        <name>ql_tubing_type__c</name>
        <picklistValues>
            <masterLabel>Compressed Air piping - Blue</masterLabel>
            <translation><!-- Compressed Air piping - Blue --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Inert Gases Piping - Black</masterLabel>
            <translation><!-- Inert Gases Piping - Black --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Nitrogen Piping - Green</masterLabel>
            <translation><!-- Nitrogen Piping - Green --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vacuum Piping - Grey</masterLabel>
            <translation><!-- Vacuum Piping - Grey --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Seal Type --></label>
        <name>seal_type_ca__c</name>
        <picklistValues>
            <masterLabel>Air Service</masterLabel>
            <translation><!-- Air Service --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Gas Service</masterLabel>
            <translation><!-- Gas Service --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Shaft Location --></label>
        <name>shaft_location_ca__c</name>
        <picklistValues>
            <masterLabel>Bottom Hand Drive</masterLabel>
            <translation><!-- Bottom Hand Drive --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Left Hand Drive</masterLabel>
            <translation><!-- Left Hand Drive --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Right Hand Drive</masterLabel>
            <translation><!-- Right Hand Drive --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Top Hand Drive</masterLabel>
            <translation><!-- Top Hand Drive --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Size --></label>
        <name>size2__c</name>
        <picklistValues>
            <masterLabel>1</masterLabel>
            <translation><!-- 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>2</masterLabel>
            <translation><!-- 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>3</masterLabel>
            <translation><!-- 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>4</masterLabel>
            <translation><!-- 4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>6</masterLabel>
            <translation><!-- 6 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>8</masterLabel>
            <translation><!-- 8 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Tubing Size --></label>
        <name>tubing_size__c</name>
        <picklistValues>
            <masterLabel>14mm (0.5&quot;)</masterLabel>
            <translation><!-- 14mm (0.5&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>20mm (0.75&quot;)</masterLabel>
            <translation><!-- 20mm (0.75&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>25mm (1&quot;)</masterLabel>
            <translation><!-- 25mm (1&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>32mm (1.25&quot;)</masterLabel>
            <translation><!-- 32mm (1.25&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>40mm (1.5&quot;)</masterLabel>
            <translation><!-- 40mm (1.5&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>40mm (1.50&quot;)</masterLabel>
            <translation><!-- 40mm (1.50&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>50mm (2&quot;)</masterLabel>
            <translation><!-- 50mm (2&quot;) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>63mm (2.5&quot;)</masterLabel>
            <translation><!-- 63mm (2.5&quot;) --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Tubing Type --></label>
        <name>tubing_type__c</name>
        <picklistValues>
            <masterLabel>Compressed Air piping - Blue</masterLabel>
            <translation><!-- Compressed Air piping - Blue --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Inert Gases Piping - Black</masterLabel>
            <translation><!-- Inert Gases Piping - Black --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Nitrogen Piping - Green</masterLabel>
            <translation><!-- Nitrogen Piping - Green --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vacuum Piping - Grey</masterLabel>
            <translation><!-- Vacuum Piping - Grey --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Vacuum --></label>
        <name>vacuum_ca__c</name>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Nom de l&apos;option</nameFieldLabel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <errorMessage><!-- The &apos;Apply Immediately&apos; field must be checked before providing an &apos;Apply Immediately Context&apos;. Otherwise, please change the &apos;Apply Immediately Context&apos; field to ‘None&apos;. --></errorMessage>
        <name>SBQQ__Apply_Immediately_Context_Validation</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Can’t include evergreen and renewable products in the same bundle. --></errorMessage>
        <name>SBQQ__Cannot_Bundle_Evergreen_Renewable_SKUs</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Can’t include evergreen and renewable products in the same bundle. --></errorMessage>
        <name>SBQQ__Cannot_Have_RenewableEvergreen_In_Bundle</name>
    </validationRules>
    <webLinks>
        <label><!-- Translate --></label>
        <name>SBQQ__Translate</name>
    </webLinks>
</CustomObjectTranslation>
