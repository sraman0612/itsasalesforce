({
    valueChanged:function(comp,event,helper)
    {
        console.log('value changed for input:' + comp.get('v.inputId'))
        var inputId = comp.get('v.inputId');
        var val = comp.get('v.val')
        var inputChanged = comp.getEvent('inputChanged')
        inputChanged.setParams({ "inputId" : inputId, "newVal" : val  });
        inputChanged.fire();
    }
})