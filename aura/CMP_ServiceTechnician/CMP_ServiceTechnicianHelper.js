({
    loadServiceTechnicians : function(component, event, helper) {
        var action = component.get("c.getServiceTechnicians");

        console.log('customerNumber=' + component.get("v.customerNumber"));
        console.log('salesOrg=' + component.get("v.salesOrg"));
        
        action.setParams({
            customerNumber: component.get("v.customerNumber"),
            salesOrg: component.get("v.salesOrg")
        });

        component.set("v.displaySpinner", true);

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);

                    component.set("v.serviceTechnicians", data);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
            component.set("v.displaySpinner", false);
        });

        $A.enqueueAction(action);
    }
})