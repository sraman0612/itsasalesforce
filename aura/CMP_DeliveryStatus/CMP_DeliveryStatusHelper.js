({
    search: function (component, helper) {
        var action = component.get("c.doSearch");
        var DeliveryNumber = component.get("v.DeliveryNumber");
        console.log("DeliveryNumber ====> " + DeliveryNumber);
        action.setParams({
            deliveryNumber: DeliveryNumber
        });

        component.set("v.displaySpinner", true);

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    console.log('values');
                    console.log(data);
                    console.log(data.Vendor);
                    if (data != null) {
                        var results = [];
                        results.push(data);
                        component.set("v.results", results);
                        console.log('v.results below ===>');
                        console.log(component.get('v.results'));
                    }
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
            component.set("v.displaySpinner", false);
        });
        $A.enqueueAction(action);
    }
})