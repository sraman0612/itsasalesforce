({
    onInit: function (cmp, event, helper) {
        cmp.set("v.messages", new Array());

        cmp.set('v.columns', [
            { label: $A.get("$Label.c.DeliveryDetail_ID"), fieldName: 'Delivery', type: 'text', sortable: false, initialWidth: 170 },
            { label: $A.get("$Label.c.DelSearch_Column_VendorName"), fieldName: 'VendorName', type: 'text', sortable: false, initialWidth: 250 },
            { label: $A.get("$Label.c.DeliveryDetail_TrackingNumber"), fieldName: 'TrackingNumber', type: 'text', sortable: false, initialWidth: 270 },
            { label: $A.get("$Label.c.DeliveryDetail_DeliveryDate"), fieldName: 'DeliveryDate', type: 'date-local', sortable: false, initialWidth: 150 },
            { label: $A.get("$Label.c.DeliveryDetail_DeliveryStatus"), fieldName: 'DeliveryStatus', type: 'text', sortable: false, initialWidth: 160 },
            { label: $A.get("$Label.c.DelSearch_Column_Tracking_URL"), fieldName: 'TrackingURL', type: 'url', sortable: false, initialWidth: 180,
                typeAttributes: { label: { fieldName: 'URL_Label' } }
            }
        ]);

        helper.search(cmp, helper);
    },

    redirect: function () {
        var url = window.location.href;
        var value = url.substr(0, url.lastIndexOf('/') + 1);
        console.log('URL value ==> '+value);
        window.location = value + 'shipment-tracking';
        //window.history.back();
        return false;

    }
})