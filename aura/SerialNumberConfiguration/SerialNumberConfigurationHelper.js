({
    doGetConfig : function(component) {
        console.log('doGetConfig()');
        var id = component.get("v.recordId");
        console.log(id);
        
        var action = component.get("c.getValues");
        action.setParams({recordId:id});
        
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                console.log(rtnValue);
                for(var i = 0; i < rtnValue.length; i++){
                    console.log(rtnValue[i].Label + " - " + rtnValue[i].Val);
                }
            }
            component.set("v.configs",rtnValue);
        });
        $A.enqueueAction(action);
    }
})