({
    doInit: function(component, event, helper)
    {
        var characteristic = component.get('v.characteristic');
        if(characteristic)
        {
            component.set('v.allowed', component.get('v.model').indexedAllowedValues[characteristic.CharacteristicName]);
            component.set('v.selected', component.get('v.model').indexedSelectedValues[characteristic.CharacteristicName]);
            helper.getInputType(component);
            // This is inverted since, the component renders the true by default.
            // We only want it to render during the "re-render phase" which means we need to wait
            // until after the code to asses the control type runs first. Then we only render the
            // specific ones we need.
        }
            component.set('v.initComplete', false);
    },
    collapse:function(component,event,helper)
    {
        var control = component.find("collapsableControl");
        control.collapse();
    },
    expand:function(component,event,helper)
    {
        var control = component.find("collapsableControl");
        control.expand();
    },
})