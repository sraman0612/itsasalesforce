({
    getInputType: function(component){
        var inputType = 'ShortText';
        var characteristic = component.get('v.characteristic');
        var allowedValues = component.get('v.allowed');
        
        if (characteristic.SingleValue === 'X')
        {
            //todo discuss values for format- fetching metadata (is that done in apex or lightining?
            if(allowedValues.length > 0)
            {
                inputType = 'InputPicklist';
            } 
            // This is all largely useless at the moment as we're just handling all of this as text at the moment.
            // This'll all need to be re-examined when we finally cut over to metdatata.
            else if(characteristic.DataType =='INT' || characteristic.DataType == 'NUM')
            {
                inputType = 'Number'; 
            } else if(characteristic.DataType == 'DATE')
            {
                inputType = 'Date';
            }
        }
        else
        {
            inputType = 'InputMultiple';
        }

        component.set('v.inputType', inputType);
    }
})