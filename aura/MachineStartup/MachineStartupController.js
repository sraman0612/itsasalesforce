({
    init : function (component, event, helper) {

        // grab flow container
        var flow = component.find("startupFlow");

        // invoke flow container with machine startup context and pass in url param for serialnumber
        flow.startFlow("Machine_Startup",[{
            name : "SerialNumbervar",
            type : "String",
            value : helper.getURLparameters(component, event, helper, 'SerialNumbervar')
        }]);
    }
})