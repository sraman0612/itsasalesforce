({
    morphItem: function(item) {
        // This function is takes a characteristic and strips out everything except the description and the value
        // This then gets converted to a Value, Label pair. This format is required for the
        // checkbox list to work correctly.
        var w = (({ CharacteristicValue, CharacteristicValueDescription }) => ({ CharacteristicValue, CharacteristicValueDescription }))(item);
          const { CharacteristicValue: value, CharacteristicValueDescription: label} = w
      
          return Object.assign(
          {},
          {
              value,
              label,
          }
      );
    }
});