({
  onInit: function(comp, event, helper) {
    var checkboxes = comp.find("checkboxes");
    for (var c in checkboxes) {
      var component = checkboxes[c];
    }
    var selectedValues = comp.get("v.selected");
    comp.set("v.val", selectedValues.map(value => value.CharacteristicValue));

    var opts = comp.get("v.possibleValues").map(item => helper.morphItem(item))
    comp.set("v.options", opts);
  },
  valueChanged: function(comp, event, helper) {
    comp.set("v.confirmOptions", true);
  },
  confirmOptions: function(comp, event, helper) {
    var inputChanged = comp.getEvent("inputChanged");
    var inputId = comp.get('v.inputId');
    var val = comp.get('v.val');

    inputChanged.setParam("inputId", inputId);
    inputChanged.setParam("newVal", val);
    //inputChanged.setParam("newValDesc", label);
    inputChanged.fire();
    comp.set("v.confirmOptions", false);
  }
});