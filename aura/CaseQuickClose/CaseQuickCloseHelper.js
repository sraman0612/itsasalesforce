({
    doInit : function(component, event, helper) {
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getCase');

        // params to the method are pass as an object with property names matching
        var params = {
            "caseId" : component.get("v.recordId")
        };

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.case", payload);
                console.log('case Id ===> '+component.get("v.case.Id"));
                console.log('case Number ===> '+component.get("v.case.CaseNumber"));
                console.log('case Status ===> '+component.get("v.case.Status"));
                console.log('case Description ===> '+component.get("v.case.Description"));
                console.log('case Re_opened__c ===> '+component.get("v.case.Re_opened__c"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.closeCase(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },
    closeCase: function(component, event, helper) {
        //prepare action for update case
        var updateCall = component.get("c.updateCase");
        updateCall.setParams({
            "currCase" : component.get("v.case")
        });
        //configure response handler for this action
        updateCall.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Case Updated",
                    "message" : "The case has been closed.",
                    "type" : "success"
                });

                //Update the UI: closePanel, show toast, refresh page
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }else if(state === "ERROR"){
                var msg = '';
                //var errors = updateCall.getError(); *** works the same as response.getError();
                //console.log('errors ===> '+errors);
                var errors = response.getError();
                console.log('errors ===> '+JSON.stringify(errors));
                //console.log('errors values ===> '+errors[0].fieldErrors.Work_Completed_Date__c[0].message);
                if (errors[0] && errors[0].fieldErrors && Object.keys(errors[0].fieldErrors).length > 0) {
                    //alert('First if '+Object.keys(errors[0].fieldErrors).length);
                    if (errors[0].fieldErrors.Work_Completed_Date__c[0]) {
                        msg = errors[0].fieldErrors.Work_Completed_Date__c[0].message;
                    }
                }
                else if (errors[0] && errors[0].pageErrors && Object.keys(errors[0].pageErrors).length > 0) {
                    //alert('second if '+Object.keys(errors[0].pageErrors).length);
                    if (errors[0].pageErrors[0]) {
                        msg = errors[0].pageErrors[0].message;
                    }
                }
                console.log('Problem updating case, response state '+state);
                console.log('Actual error message ===> '+msg);
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Case Update Error",
                    "message" : "The case could not be closed - " + msg,
                    "type" : "error"
                });

                //Update the UI: closePanel, show toast, refresh page
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }else{
                console.log('Unknown problem: '+state);
            }
        });
        //send the request to updateCall
        $A.enqueueAction(updateCall);
    },
    cancelCase: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})