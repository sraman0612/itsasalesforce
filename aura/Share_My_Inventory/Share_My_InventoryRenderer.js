({
    afterRender: function (component, helper) {
        
        this.superAfterRender();

        // Init event wasn't being handled properly when being called from the lightning community
        // so was forced to put it here.
        helper.init(component, null, helper);
    }
})