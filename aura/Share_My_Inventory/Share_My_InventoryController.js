({
//    init : function(component, event, helper) {
//
//        //if(!component.get('v.account')){
//         var action = component.get('c.doInit');
//
//        action.setCallback(this, function(actionResult) {
//
//            if(actionResult.getState() === 'SUCCESS'){
//                component.set('v.account', JSON.parse(actionResult.getReturnValue()).activeAccount);
//
//        console.dir('===============================');
//        console.dir(actionResult.getReturnValue().debugInfo);
//        console.dir('===============================');
//
//            }
//            else {
//
//                let toast = $A.get('e.force:showToast');
//                toast.setParams({
//                    title: 'Error',
//                    message: 'An error occurred while trying to access your account information. Please contact an administrator if the issue persists.',
//                    mode: 'sticky',
//                    type: 'error'
//                });
//                toast.fire();
//            }
//
//        });
//        $A.enqueueAction(action);
//        //}
//
//    },
    updateAcc : function(component, event, helper) {
        var action = component.get('c.updateAccount');

        let acc = component.get('v.account');
        acc.Share_Assets__c = !acc.Share_Assets__c;

        console.dir('-----------------');
        console.dir(acc);
        console.dir('-----------------');

         action.setParams({ 
             "account" : acc
         });
        action.setCallback(this, function(actionResult) {

            if(actionResult.getState() === 'SUCCESS'){
                component.set('v.account', actionResult.getReturnValue());
            }
            else {
                let toast = $A.get('e.force:showToast');
                toast.setParams({
                    title: 'Error',
                    message: 'An error occurred while trying to update your preference. Please contact an administrator if the issue persists.',
                    mode: 'sticky',
                    type: 'error'
                });
                toast.fire();
            }

        });
        $A.enqueueAction(action); 
    }
})