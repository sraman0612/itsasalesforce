({
    init: function(component, event, helper) {
        
        console.dir('=============== INIT ================');
        
        if (!component.get('v.account')) {
            var action = component.get('c.doInit');

            action.setCallback(this, function(actionResult) {

                if (actionResult.getState() === 'SUCCESS') {
                    component.set('v.account', actionResult.getReturnValue());
                } else {

                    console.dir('==============' + actionResult.getState() + '=================');

                    let toast = $A.get('e.force:showToast');
                    toast.setParams({
                        title: 'Error',
                        message: 'An error occurred while trying to access your account information. Please contact an administrator if the issue persists.',
                        mode: 'sticky',
                        type: 'error'
                    });
                    toast.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
})