({
    openCaseSurvey : function(component, event, helper) {
        var caseId = component.get("v.recordId");
        var url = '/apex/Survey?CaseId='+caseId;
        console.log(caseId);
        console.log(url);
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.openSubtab({
                parentTabId: focusedTabId,
                url:  url,
                focus: true
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    cancelCaseSurvey: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})