({
    onInit: function (component, event, helper) {
        console.log('init');
        var items = component.get("v.listitems");
        items.push({"qty":1});
        items.push({"qty":1});
        items.push({"qty":1});
        items.push({"qty":1});
        component.set("v.listitems", items);

        console.log('quote id=' + component.get("v.QuoteId"));
    },
    addRow: function (component, event, helper) {
        var items = component.get("v.listitems");
        var newobj = {"qty":1};
        items.push(newobj);
        component.set("v.listitems", items);
    },
    removeRow: function (component, event, helper) {
        var items = component.get("v.listitems");
        console.log('in removeRow');
        console.log(JSON.stringify(event.source));
        component.set("v.listitems", items);
    },
    itemChanged: function (component, event, helper) {
        var selectedIndex = event.getParam("selectedIndex");
        var item = event.getParam("item");

        console.log('selectedIndex=' + selectedIndex);
        console.log('item=' + JSON.stringify(item));

        if (!item) {
            item = {"qty": 1};
        }

        var items = component.get("v.listitems");
        items[selectedIndex] = item;

        console.log(JSON.stringify(items));
        component.set("v.listitems", items);
    },
    saveToQuote: function (component, event, helper) {
        var items = component.get("v.listitems");
        var ids = new Array();
        var quantities = new Array();
        let alertMessage;

        items.forEach(function(row, index) {
            if (row.val) {
                ids.push(row.val);
                quantities.push(row.qty);
            }
            else {
                alertMessage = "A valid value must be selected to save";
            }
        });

        if(alertMessage && ids.length == 0) {
            alert(alertMessage);
            return;
        }

        console.log('ids.length=' + ids.length);

        if (ids.length > 0) {
            var action = component.get("c.appendLineItems");

            action.setParams({
                quoteId: component.get("v.QuoteId"),
                productIds: ids,
                quantities: quantities
            });
    
            action.setCallback(this, function (response) {
                helper.backToEdit(component);
            });
    
            component.set("v.displaySpinner", true);
            $A.enqueueAction(action);
        } else {
            helper.backToEdit(component);
        }
    },
    backToEdit: function (component, event, helper) {
        helper.backToEdit(component);
    }
})