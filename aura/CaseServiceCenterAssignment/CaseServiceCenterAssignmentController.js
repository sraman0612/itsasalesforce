({
    createSCA : function(component, event, helper) {
        var caseId = component.get("v.recordId");
        console.log(caseId);
        var workspaceAPI = component.find("workspace");
        console.log('workspaceAPI ===> '+workspaceAPI);
        workspaceAPI.isConsoleNavigation().then(function(response) {
			var inSvcConsole = response;
            console.log('inSvcConsole ===> '+inSvcConsole);
            if (inSvcConsole == true) {
            	console.log('In the Service Console!!!')
                workspaceAPI.getFocusedTabInfo().then(function(response) {
            		var focusedTabId = response.tabId;
            		var url = '/apex/C_Tertiary_Service_Center_Assignment?Id='+caseId+'&tabId='+focusedTabId;
            		console.log(url);
            		workspaceAPI.openSubtab({
                		parentTabId: focusedTabId,
                		url:  url,
                		focus: true
					});
        		})
        		.catch(function(error) {
            		console.log(error);
        		});
            }
            else {
                console.log('Not in the Service Console!!!');
                //window.open('/apex/C_Tertiary_Service_Center_Assignment?Id='+caseId);
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  //"url": location.protocol+'//'+location.host+'/apex/C_Tertiary_Service_Center_Assignment?Id='+caseId
                  "url": '/apex/C_Tertiary_Service_Center_Assignment?Id='+caseId
                });
                console.log('urlEvent ==> '+urlEvent);
                urlEvent.fire();
            }
		})
		.catch(function(error) {
			console.log(error);
		});
    },
    cancelSCA: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})