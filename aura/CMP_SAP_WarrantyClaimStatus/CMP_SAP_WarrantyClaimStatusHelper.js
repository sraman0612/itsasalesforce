({
    loadStatus : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        
        if (recordId == null) {
            return;
        }
        
        var action = component.get("c.getStatus");
        
        action.setParams({
            warrantyClaimId: recordId
        });
        
        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = response.getReturnValue().data;
                    console.log(data);
                    
                    var intervalId = component.get("v.intervalId");
                    
                    if (data.SAP_Status__c == "SUCCESS" || data.SAP_Status__c == "ERROR") {
                        if (intervalId) {
                            console.log('clearing interval ' + intervalId);
	                        clearInterval(intervalId);
                            component.set("v.intervalId", null);
                        }
                    } else {
                        if (!intervalId) {
                            component.set("v.intervalId", setInterval(function() {
                                helper.loadStatus(component, event, helper);
                            }, 5000));
                        }
                    }
                    
                    var oldStatus = component.get("v.overallStatus");
                    
                    if (oldStatus) {
                        if ((data.SAP_Status__c == "SUCCESS" && oldStatus != "SUCCESS") || (data.SAP_Status__c == "ERROR" && oldStatus != "ERROR")) {
	                        $A.get('e.force:refreshView').fire();
                        }
                    }
                    
                    component.set("v.overallStatus", data.SAP_Status__c);
                    
                    var messages = data.SAP_Messages__c;
                    
                    if (messages) {
                        var theseMessages = messages.split("\|");
                        
                        var errorMessages = [];
                        var warningMessages = [];
                        var successMessages = [];
                        
                        var count = 0;
                        theseMessages.forEach(function (row) {
                            if (row) {
                                var messageDetail = row.split(":");
                                
                                var item = {
                                    id: count + 1,
                                    type: messageDetail[0].trim(),
                                    step: messageDetail[1].trim(),
                                    message: messageDetail[2].trim()
                                };
                                
                                var index = 3;
                                while (messageDetail.length > index) {
                                    item.message += ':' + messageDetail[index];
                                    index++;
                                }
                                
                                if (messageDetail[0].trim() == 'ERROR') {
                                    errorMessages.push(item);
                                } else if (messageDetail[0].trim() == 'Warning') {
                                    warningMessages.push(item);
                                } else if (messageDetail[0].trim() == 'Success') {
                                    successMessages.push(item);
                                }
                                
                                count++;
                            }
                        });
                    }
                    
                    console.log(errorMessages);
                    component.set("v.errors", errorMessages);
                    console.log(warningMessages);
                    component.set("v.warnings", warningMessages);
                    console.log(successMessages);
                    component.set("v.successes", successMessages);
                    
                    
                    component.set("v.displaySpinner", false);
                }
            }
        });
        
        $A.enqueueAction(action);
        component.set("v.displaySpinner", true);
    }
})