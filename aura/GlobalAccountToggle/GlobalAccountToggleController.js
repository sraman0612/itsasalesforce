({
    handleSave: function(component, event, helper){
        component.set('v.actionableValue', component.get('v.selectedValue'));
        component.find('overlayLib').notifyClose();
    },
    handleCancel: function(component, event, helper){
        component.set('v.selectedValue', component.get('v.initialValue'));
        component.set('v.actionableValue', component.get('v.selectedValue'));
        component.find('overlayLib').notifyClose();
    }
});