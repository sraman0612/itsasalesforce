({
    getOptions : function(cmp,evt,hlp){

        let serverAction = cmp.get('c.fetchOptions');

        serverAction.setParam('leadId', cmp.get('v.recordId'));

        serverAction.setCallback(this, function(serverResponse){

            let state = serverResponse.getState();

            if(state === 'SUCCESS'){
                cmp.set('v.options', serverResponse.getReturnValue());
            }
            else {

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "An error has occurred, please contact an administrator if the issue persists.",
                    "type": "error"
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
            }
        });

        $A.enqueueAction(serverAction);
    },
    buildColumns : function(cmp, evt, hlp){
        cmp.set('v.columns',[
            {
                label : 'Name',
                fieldName : 'Name',
                type : 'String'
            },{
                label : 'Account Number',
                fieldName : 'AccountNumber',
                type : 'String'
            },{
                label : 'City',
                fieldName : 'BillingCity',
                type : 'String'
            }
        ]);
    },
    assign : function(cmp){

        let serverAction = cmp.get('c.assignLead');

        serverAction.setParam('distAccId', cmp.find('theTable').get('v.selectedRows')[0]);
        serverAction.setParam('leadId', cmp.get('v.recordId'));

        serverAction.setCallback(this, function(serverResponse){

            let state = serverResponse.getState();

            if(state === 'SUCCESS'){

                $A.get("e.force:closeQuickAction").fire();
                $A.get("e.force:refreshView").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "message": "The lead has been assigned successfully.",
                    "type": "success"
                });
                toastEvent.fire();
            }
            else {

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "An error has occurred, please contact an administrator if the issue persists.",
                    "type": "error"
                });
                toastEvent.fire();
            }
        });

        $A.enqueueAction(serverAction);
    },
    cancel : function(cmp){
        $A.get("e.force:closeQuickAction").fire();
    }
});