({
    handleInit : function(cmp,evt,hlp){
        hlp.buildColumns(cmp);
        hlp.getOptions(cmp,evt,hlp);
    },
    handleClick : function(cmp,evt,hlp){

        if(evt.getSource().getLocalId() === 'assignBtn'){
            hlp.assign(cmp);
        }
        else if(evt.getSource().getLocalId() === 'cancelBtn'){
            hlp.cancel(cmp);
        }
    }
});