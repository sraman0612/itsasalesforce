({
	doInit : function(component, event, helper) {
		var action = component.get('c.getAssetRecord');
        
        action.setParams({ assetId: component.get("v.recordId") });
        action.setCallback(this, function(result) {
            console.log(result.getReturnValue());
            if (result.getState() !== 'SUCCESS') return this.toastMsg('error', 'An error occurred while retrieving the record');
            var theAsset = result.getReturnValue();
            component.set("v.thisAsset", theAsset);
            component.set("v.isCurrentServicer", theAsset.User_CurrentServicer__c);
            component.set("v.isSoldTo", theAsset.User_SoldTo__c);
        });
        $A.enqueueAction(action);
	},
    
    handleSubmit: function(component, event, helper) {
        console.log('handleSubmit');
        event.preventDefault();
        const fields = event.getParam('fields');
        fields.Id = component.get("v.recordId");
        component.find('recordEditForm').submit(fields);
    },
    
    handleSuccess: function(component, event, helper) {
        console.log('handleSuccess');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "type":"success",
            "message": "The Serial Number has been updated successfully."
        });
        toastEvent.fire();
        
        $A.get('e.force:refreshView').fire();
        // Call the navigation service here
    }
})