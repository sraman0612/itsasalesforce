({
    removeItem : function(component, event, helper) {
        var rowNumber = component.get("v.RowNumber");

        var removeEvent = component.getEvent("removeEvent");
        removeEvent.setParams({
            "RowNumber" : rowNumber
        });

        removeEvent.fire();
    },

    handleBlur : function(component, event, helper) {
        var changeEvent = component.getEvent("changeEvent");

        changeEvent.fire();
    }
})