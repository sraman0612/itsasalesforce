({
    onInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var recordText = component.get("v.recordText");
        var objectName = component.get("v.objectName");
        
        if (recordId) {
            console.log('recordId was provided ' + recordId);
            console.log('recordText was provided ' + recordText);
            component.set("v.selItem", { val : recordId, text : recordText, objName : objectName });
        }
    },
    
	itemChanged : function(component, event, helper) {
        console.log('item changed');
		var selItem = component.get("v.selItem");
        console.log(selItem);
        
        if (selItem) {
            component.set("v.recordId", selItem.val);
            component.set("v.recordText", selItem.text);
        } else {
            component.set("v.recordId", null);
            component.set("v.recordText", null);
        }
	}
})