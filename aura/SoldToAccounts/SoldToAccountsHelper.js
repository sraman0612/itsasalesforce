({
	 handleSelectHelper: function (component, event, helper){
        component.get("v.showSpinner", true);
        var opptyId = component.get("v.recordId");
         var accId = component.get("v.AccId");
         if(accId != null){

        var acId= component.get("v.AccId");
        console.log('Acc ID :', JSON.stringify(JSON.parse(acId)));
        var nos = component.get('v.selectedRowsCount');
        console.log("No. of rows selected :" +nos);
        
        var par=JSON.parse(acId);
         
        var accc= par[0];
        console.log("accc :"+accc.Id);
        
        var selectedAcc =  component.get("v.SelectedAccName");
        console.log('opptyId-> ' + opptyId + ' == ' + accc.Id + ' === ' + selectedAcc);
        
        var updateAction = component.get("c.setSoldToAccount");
        updateAction.setParams({"OppId" : opptyId , "AccId" : accc.Id});
        
        updateAction.setCallback(this,function(response){
            var state = response.getState();
            var respVals = response.getReturnValue();
            console.log("opptyId inside: " + respVals + JSON.stringify(response));   
            var toastEvent = $A.get("e.force:showToast");
            if (respVals == '') 
            {	
                console.log("Account Updated");
                component.get("v.showSpinner", true);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    'type': 'success',
                    'mode': 'pester',
                    "title": "Success!",
                    "message": 'Updated Sold to account as ' + selectedAcc + ' Successfully.'
                });
                toastEvent.fire();
                
            }
            else
            {
                var errors= response.getError();
             if(errors){
                   var toastEvent = $A.get("e.force:showToast");
                     toastEvent.setParams({
                         'type': 'error',
                         'mode': 'pester',
                         "title": "Error!",
                         "message": respVals
                     });
                     toastEvent.fire();       
              
                     
                 }
             }
             component.get("v.showSpinner", true); 
                                 
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(updateAction);
         }else{
              var toastEvent = $A.get("e.force:showToast");
                     toastEvent.setParams({
                         'type': 'error',
                         'mode': 'pester',
                         "title": "Error!",
                         "message": 'Please select at least one account to perform the action'
                     });
                     toastEvent.fire();       
              
         }
        component.set("v.isShowModel", false);
        
    }
})