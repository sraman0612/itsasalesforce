({
    openModel: function(component, event, helper) {
        // Set isModalOpen attribute to true
        component.set("v.isShowModel", true);
    },
    
    closeModel: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isShowModel", false);
    },
    
    submitDetails: function(component, event, helper) {
        // Set isModalOpen attribute to false
        //Add your code to call apex method or do some processing
        component.set("v.isShowModel", false);
        helper.handleSelectHelper(component, event, helper);
    },
    
    handleRecordChanged: function(component, event, helper) {
        switch(event.getParams().changeType) {
            case "ERROR":
                
                break;
            case "LOADED":
                //  console.log('Opportunity redcord is--->'+JSON.stringify(component.get("v.opptyRecord")));
                var OrcNum = component.get("v.opptyRecord")["Id"];
                component.set("v.BillCust", OrcNum);
                console.log("OrcNum : " +OrcNum);  
                
                break;
            case "REMOVED":
                
                break;
            case "CHANGED":
                
                break;
        }
        $A.enqueueAction(component.get('c.fetchAccounts'));
    }, 
    ShowModel : function(component, event, helper) {
        console.log('ShowModel fird.!');
        component.set("v.isShowModel", true);
    },
    
    fetchAccounts : function(component, event, helper) {
        
        var oppId = component.get("v.BillCust")+''; 
        if(oppId.startsWith('001')){
            component.set("v.isAccountRecord", true); 
        }
        component.set('v.myColumns',[
            {label:'Account Name', fieldName:'Name',type:'text'},
            {label: 'Type', fieldName: 'Type', type: 'text'},
            {label: 'Oracle Number', fieldName: 'Oracle_Number__c', type: 'text'},
            {label: 'Shipping Street', fieldName: 'ShippingStreet', type: 'text'},
            {label: 'Shipping City', fieldName: 'ShippingCity', type: 'text'},
            {label: 'Shipping State', fieldName: 'ShippingState', type: 'text'},            
            {label: 'Postal Code', fieldName: 'ShippingPostalCode', type: 'text'}
        ]);
        
        
        var action = component.get("c.fetchAccts");
        console.log('@@@@@@OracleNumber-------->'+oppId);
        action.setParams({"oppId" : oppId  });       
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.accList", response.getReturnValue());
                
            }
            
            
        });
        $A.enqueueAction(action);
        
         var action2 = component.get("c.getRecordTypeName");
        console.log('@@@@@@OracleNumber-------->'+oppId);
        action2.setParams({"OppId" : oppId  });       
        
        action2.setCallback(this, function(response){
            var state = response.getState();
            var recTyp = response.getReturnValue();
            console.log('recTyp-> ' + recTyp);
            if (state === "SUCCESS") {
                if(recTyp == 'Quick_Quote')component.set("v.ShowWidgt", false);   
            }
        });
        $A.enqueueAction(action2);
        
    },
    
    updateSelectedText: function (component, event,helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set('v.selectedRowsCount', selectedRows.length);

        if(selectedRows.length >= 1){
            component.set("v.SelectedAccName", selectedRows[0].Name);
            console.log('selectedRows[0].Name-> '+ selectedRows[0].Name + ' == ' + JSON.stringify(selectedRows[0]));
        }

        if(selectedRows.length > 1){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Kindly Select Only One 'Sold To' Account"
            })
            toastEvent.fire(); 
            let button = component.find('disablebuttonid');
            button.set('v.disabled',true);
        }
        else{
            let button = component.find('disablebuttonid');
            button.set('v.disabled',false);
            
        }
        
        
        //console.log(JSON.parse(JSON.stringify(component.get("v.selectedRows"))));
        let obj =[];
        for(var i=0; i < selectedRows.length; i++){
            obj.push({Id:selectedRows[i].Id});
        }
        //component.set("v.selectedRowsDetails" ,JSON.stringify(obj) );
        
        component.set("v.AccId",JSON.stringify(obj));
        
    },
    
    handleSelect: function (component, event, helper){
        helper.handleSelectHelper(component, event, helper);
    }
})