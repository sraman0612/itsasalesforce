({
    buildColumns: function(component) {
        let columns = [
            {label: 'Item Number', fieldName: 'BOMItemNumber', type: 'text'},
            {label: 'Material', fieldName: 'BOMComponent', type: 'text'},
            {label: 'Description', fieldName: 'ItemDescription', type: 'text'},
            {label: 'Quantity', fieldName: 'ComponentQuantity', type: 'number'},
            {label: 'UOM', fieldName: 'ComponentUnitOfMeasure', type: 'text'},
        ];

        component.set('v.columns', columns);
    },

    getMaterialBOM: function(component, pagingOptions) {
        return new Promise(function(resolve, reject) {
            console.log('getMaterialBOM');
            let action = component.get('c.getMaterialBOM');
            action.setParams({
                searchParams: component.get('v.searchParams'),
                pagingOptions: pagingOptions
            })

            action.setCallback(this, function(res) {
                console.log('return getMaterialBOM');
                if (res.getReturnValue()) {
                    let response = res.getReturnValue();
                    let data = response.data;
                    component.set('v.pagingOptions', response.pagingOptions);
                    if (data) {
                        let parsedData = JSON.parse(data);
                        component.set('v.displayResults', parsedData);
                        let selectedItems = parsedData.filter(obj => obj.SoldSeparately);
                        if (selectedItems.length) {
                            component.set('v.selectedItemsList', selectedItems);
                            let selectedRows = selectedItems.map(obj => obj.BOMComponent);
                            component.set('v.selectedRows', selectedRows);
                        }
                    }

                    if (response.messages) {
                        component.set('v.messages', response.messages);
                    }
                }
                resolve(true);
            })

            $A.enqueueAction(action);
        })
    }
})