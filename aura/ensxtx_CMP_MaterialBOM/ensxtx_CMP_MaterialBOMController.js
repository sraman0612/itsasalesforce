({
    doInit: function(component, event, helper) {
        helper.buildColumns(component);
        let pagingOptions = {pageNumber: 1, pageSize: component.get('v.maxNumberOfRows')};
        helper.getMaterialBOM(component, pagingOptions)
            .then($A.getCallback(function() {
                component.set('v.displaySpinner', false);
            }))
    },

    onPagerChanged: function(component, event, helper) {
        component.set('v.displaySpinner', true);
        helper.getMaterialBOM(component, event.getParam('options'))
            .then($A.getCallback(function() {
                component.set('v.displaySpinner', false);
            }))
    },

    onRowSelection: function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set('v.selectedItemsList', selectedRows);
    },

    addMaterials: function(component, event, helper) {
        let selectedItems = component.get('v.selectedItemsList');
        let plant = component.get('v.searchParams.plant');

        // Transform the object before sending to material search
        let itemsList = selectedItems.map(obj => {
            let newObj = {
                material: obj.BOMComponent,
                materialDescription: obj.ItemDescription,
                plant: plant,
                quantity: obj.ComponentQuantity
            };
            return newObj;
        })
        let evt = component.getEvent('selectMaterialsBOMEvent');
        evt.setParams({
            selectedMaterialsBOM: JSON.stringify(itemsList)
        });
        evt.fire();
        component.find('overlayMaterialBOM').notifyClose();
    },

    onClickCancel: function(component, event, helper) {
        component.find("overlayMaterialBOM").notifyClose();
    },
})