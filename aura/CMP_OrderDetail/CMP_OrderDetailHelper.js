({
    getUrlParameter : function (sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },

    getOrderDetail : function (component) {
        var SalesDocument = component.get("v.SalesDocument");

        if (!SalesDocument) {
            component.set("v.messages", { message: "No Sales Document was provided.", messageType: "ERROR" });
            return;
        }

        var action = component.get("c.getOrderDetail");

        action.setParams({
            SalesDocument: SalesDocument
        });

        action.setCallback(this, function (response) {
            console.log("response="  + response.getState());
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = response.getReturnValue().data;
                    console.log(data);

                    data.PARTNERS.asList.forEach(function(row) {
                        if (row.PartnerFunction == 'CR') {
                            if (data.CarrierName) {
                                data.CarrierName +=  ' / ' + row.PartnerName;
                            } else {
                                data.CarrierName = row.PartnerName;
                            }
                        }
                    });

                    var scheduleMap = new Map();

                    data.ITEMS_SCHEDULE.asList.forEach(function(row) {
                        if (row.ConfirmedQuantity > 0) {
                            var list = scheduleMap.get(row.ItemNumber);
                            if (!list) {
                                list = [];
                                scheduleMap.set(row.ItemNumber, list);
                            }

                            list.push(row);
                        }
                    });
                    data.ITEMS.asList.forEach(function(row) {
                        if (scheduleMap.get(row.ItemNumber)) {
                            var list = scheduleMap.get(row.ItemNumber);
                            if (list) {
                                console.log('we have ' + list.length + ' items.');

                                if (list.length > 0) {
                                    row.ScheduleIcon = 'standard:schedule_objective';
                                    row.ScheduleItems = [];

                                    list.forEach(function (scheduleItem) {
                                        row.ScheduleItems.push(scheduleItem);
                                    });
                                    var schedule = list[0];
                                    row.ConfirmedQuantity = schedule.ConfirmedQuantity;
                                    row.ScheduleLineDate = schedule.ScheduleLineDate;
                                }
                            } 
                        }
                    });

                    component.set("v.orderDetail", data);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }
            component.set("v.displaySpinner", false);
        });

        component.set("v.displaySpinner", true);
        $A.enqueueAction(action);
    },

    getDocumentFlow : function (component) {
        var SalesDocument = component.get("v.SalesDocument");

        if (!SalesDocument) {
            component.set("v.messages", { message: "No Sales Document was provided.", messageType: "ERROR" });
            return;
        }

        var action = component.get("c.getDocumentFlow");

        action.setParams({
            SalesDocument: SalesDocument
        });

        action.setCallback(this, function (response) {
            console.log("response="  + response.getState());
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = response.getReturnValue().data;
                    console.log(data);

                    var filteredList = [];

                    data.forEach(function (row) {
                        if (row.DocumentCategoryText === "Delivery") {
                            filteredList.push(row);
                            row.IconName = 'utility:topic';
                            row.DisableDownload = false;
                            row.ShowButton = 'slds-show';
                            row.ButtonTitle = 'Track Delivery';
                        } else if (row.DocumentCategoryText === "Invoice") {
                            filteredList.push(row);
                            row.IconName = 'utility:download';
                            row.DisableDownload = false;
                            row.ShowButton = 'slds-show';
                            row.ButtonTitle = 'Download Invoice';
                        } else if (row.DocumentCategoryText === "Order") {
                            filteredList.push(row);
                            row.DisableDownload = true;
                            row.ShowButton = 'slds-hide';
                        }
                    });

                    component.set("v.docFlow", filteredList);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }
            component.set("v.displaySpinner", false);
        });

        component.set("v.displaySpinner", true);
        $A.enqueueAction(action);
    },

    trackDelivery : function(SalesDocument, component) {
        console.log("SalesDocument ====> " + SalesDocument);
        var action = component.get("c.trackDelivery");
        action.setParams({
            SalesDocument: SalesDocument
        });
        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    console.log(data);

                    component.set("v.trackingData", data);
                    component.set("v.showTrackingModal", true);
                }
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
            component.set("v.displaySpinner", false);
        });
        $A.enqueueAction(action);
        component.set("v.displaySpinner", true);
    },

    downloadInvoice: function (SalesDocument, component) {
        console.log("SalesDocument ====> " + SalesDocument);
        var action = component.get("c.downloadInvoice");
        action.setParams({
            SalesDocument: SalesDocument
        });
        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = response.getReturnValue().data;
					console.log('PDF response data values below ===>');
                    console.log(data);
                    // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####
                    if (data.PDFB64String != null) {
                        var hiddenElement = document.createElement('a');
                        hiddenElement.href = 'data:application/pdf;base64,' + data.PDFB64String;
                        hiddenElement.target = '_self';
                        hiddenElement.download = 'INVOICE-' + SalesDocument + '.pdf';  // PDF file Name* you can change it.[only name not .csv]
                        document.body.appendChild(hiddenElement); // Required for FireFox browser
                        hiddenElement.click(); // using click() js function to download csv file
                    }
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
            component.set("v.displaySpinner", false);
        });
        $A.enqueueAction(action);
        component.set("v.displaySpinner", true);
    }

})