({
    onInit : function(component, event, helper) {
        console.log('in onInit');

        component.set('v.itemcolumns', [
            { label: 'Item', fieldName: 'ItemNumber', type: 'text', sortable: false, initialWidth: 75 },
            { label: 'Material', fieldName: 'Material', type: 'text', sortable: false, initialWidth: 100 },
            { label: 'Description', fieldName: 'ItemDescription', type: 'text', sortable: false, initialWidth: 320 },
            { label: 'Quantity', fieldName: 'OrderQuantity', type: 'number', sortable: false, initialWidth: 100 },
            { label: 'Unit Price', fieldName: 'NetItemPrice', type: 'currency', typeAttributes: { currencyCode: {fieldName: 'SalesDocumentCurrency'}},  sortable: false, initialWidth: 110 },
            { label: 'Net Total', fieldName: 'NetOrderValue', type: 'currency', typeAttributes: { currencyCode: {fieldName: 'SalesDocumentCurrency'}},  sortable: false, initialWidth: 110 },
            { label: 'Schedule', fieldName: 'ScheduleItems', type: 'button-icon', typeAttributes: {iconName: {fieldName: 'ScheduleIcon'}, name: 'schedule', title: 'View Schedule', variant: 'bare'}, sortable: false, initialWidth: 110 }
        ]);

        component.set('v.docflowcolumns', [
            { label: '', type: "button-icon", sortable: false, typeAttributes: {iconName: {fieldName: 'IconName'}, disabled: true, name: 'download', title: {fieldName: 'ButtonTitle'}, variant: 'bare', disabled: { fieldName: 'DisableDownload'}, class: {fieldName: 'ShowButton'}}, initialWidth: 50},
            { label: 'Date', fieldName: 'CreateDate', type: 'date-local', sortable: false, initialWidth: 100 },
            { label: 'Category', fieldName: 'DocumentCategoryText', type: 'text', sortable: false, initialWidth: 120 },
            { label: 'Sales Document', fieldName: 'SalesDocument', type: 'text', sortable: false, initialWidth: 150 },
            { label: 'Status', fieldName: 'DocumentStatus', type: 'text', sortable: false, initialWidth: 120 }
        ]);

        component.set('v.schedulecolumns', [
            { label: 'Confirmed Quantity', fieldName: 'ConfirmedQuantity', type: 'number', sortable: false, initialWidth: 200 },
            { label: 'Schedule Line Date', fieldName: 'ScheduleLineDate', type: 'date-local', sortable: false, initialWidth: 200 }
        ]);

        helper.getOrderDetail(component);
        helper.getDocumentFlow(component);
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'download':
                if (row.DocumentCategoryText === "Invoice") {
                    helper.downloadInvoice(row.SalesDocument, component);
                } else if (row.DocumentCategoryText === "Delivery") {
                    helper.trackDelivery(row.SalesDocument, component);
                }
                break;
        }
    },

    handleShowSchedule: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'schedule':
                console.log('selected row: ' + JSON.stringify(row));
                component.set("v.currentSchedule", row);
                component.set("v.showScheduleModal", true);
                break;
        }
    },

    closeScheduleModal: function (component, event, helper) {
        component.set("v.showScheduleModal", false);
    },

    closeTrackingModal: function (component, event, helper) {
        component.set("v.showTrackingModal", false);
    },

    showTracking: function (component, event, helper) {
        var trackingData = component.get("v.trackingData");

        if (trackingData.TrackingURL) {
            window.open(trackingData.TrackingURL, "_blank");
        }
    }
})