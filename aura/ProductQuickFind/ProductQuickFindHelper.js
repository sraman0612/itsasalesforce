({
    itemSelected: function (component, event, helper) {
        var target = event.target;
        var SelIndex = helper.getIndexFrmParent(target, helper, "data-selectedIndex");

        if (SelIndex) {
            var serverResult = component.get("v.server_result");
            var selItem = serverResult[SelIndex];
            if (selItem.val) {
                var oldSelItem = component.get("v.selItem");
                if (oldSelItem && oldSelItem.qty) {
                    selItem.qty = oldSelItem.qty;
                }
                component.set("v.selItem", selItem);
                console.log(component.get("v.selItem"));
                component.set("v.last_ServerResult", serverResult);
                helper.itemChanged(component);
            }
            component.set("v.server_result", null);
        }
    },

    itemChanged: function(component, event, helper) {
        var selItem = component.get("v.selItem");

        if (!selItem) {
            return;
        }

        if (!selItem.qty || selItem.qty === '') {
            selItem.qty = 1;
            component.set("v.selItem", selItem);
            return;
        }

        if (!selItem.val) {
            return;
        }

        var thisIndex = component.get("v.thisIndex");

        var cmpEvent = component.getEvent("cmpEvent");

        cmpEvent.setParams({
            "selectedIndex": thisIndex,
            "item": selItem
        });
        console.log("firing change event.");
        cmpEvent.fire();
    },
    serverCall: function (component, event, helper) {
        var target = event.target;
        var searchText = target.value;
        var last_SearchText = component.get("v.last_SearchText");
        //Escape button pressed 
        if (event.keyCode == 27 || !searchText.trim()) {
            helper.clearSelection(component, event, helper);
        } else if (searchText.trim() != last_SearchText) { //&& /\s+$/.test(searchText)
            //Save server call, if last text not changed
            //Search only when space character entered
            var quoteId = component.get("v.quoteId");
            var objectName = component.get("v.objectName");
            var field_API_text = component.get("v.field_API_text");
            var field_API_val = component.get("v.field_API_val");
            var field_API_search = component.get("v.field_API_search");
            var limit = component.get("v.limit");

            var action = component.get('c.searchDB');
            action.setStorable();

            action.setParams({
                quoteId: quoteId,
                objectName: objectName,
                fld_API_Text: field_API_text,
                fld_API_Val: field_API_val,
                lim: limit,
                fld_API_Search: field_API_search,
                searchText: searchText
            });

            action.setCallback(this, function (a) {
                this.handleResponse(a, component, helper);
            });

            component.set("v.last_SearchText", searchText.trim());
            $A.enqueueAction(action);
        } else if (searchText && last_SearchText && searchText.trim() == last_SearchText.trim()) {
            component.set("v.server_result", component.get("v.last_ServerResult"));
        }
    },
    handleResponse: function (res, component, helper) {
        if (res.getState() === 'SUCCESS') {
            var retObj = JSON.parse(res.getReturnValue());
            if (retObj.length <= 0) {
                var noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.server_result", noResult);
                component.set("v.last_ServerResult", noResult);
            } else {
                component.set("v.server_result", retObj);
                component.set("v.last_ServerResult", retObj);
            }
        } else if (res.getState() === 'ERROR') {
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    alert(errors[0].message);
                }
            }
        }
    },
    getIndexFrmParent: function (target, helper, attributeToFind) {
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while (!SelIndex) {
            target = target.parentNode;
            SelIndex = helper.getIndexFrmParent(target, helper, attributeToFind);
        }
        return SelIndex;
    },
    clearSelection: function (component, event, helper) {
        component.set("v.selItem", null);
        component.set("v.server_result", null);
        var thisIndex = component.get("v.thisIndex");
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "selectedIndex": thisIndex,
            "item": null
        });
        cmpEvent.fire();
}
})