({
    getConfiguration: function(component) {
        //var pricingCFG = component.get("v.pricingConfig");
        var bypassPricing = component.get("v.bypassPricing");

        var headerJSON = JSON.parse(component.get("v.headerJSON"));
        var itemJSON = JSON.parse(component.get("v.itemJSON"));

        var materialId = component.get('v.materialId');

        var pricingCFG =
        {
            "SalesDocumentType" : headerJSON.salesDocType,
            "SalesOrganization" : headerJSON.salesOrg,
            "DistributionChannel" : headerJSON.distChannel,
            "Division" : headerJSON.division,
            "SoldToParty" : headerJSON.soldToParty,
            "OrderQuantity" : itemJSON.OrderQuantity,
            "Plant" : itemJSON.plant
        };

        if (itemJSON && itemJSON.SalesDocumentCurrency && itemJSON.SalesDocumentCurrency.length > 0)
        {
            pricingCFG.SalesDocumentCurrency = itemJSON.SalesDocumentCurrency;
        }

        if (!materialId && (pricingCFG != null || bypassPricing)) return;
        var serializedpricingCFG = JSON.stringify(pricingCFG);

        component.set("v.busy", true);

        // This checkes whether or not there is a bill of materials on the object. Verifies taht
        // A). itemJSON exists, B). The selected characteristics Exists C). There are elements in the selected characteristics
        var isreconfig = itemJSON && itemJSON.selectedCharacteristics && itemJSON.selectedCharacteristics.length;
        var action = isreconfig ? component.get("c.initializeConfigurationWithBOM") : component.get("c.initializeCustomConfiguration");

        action.setParam("material", materialId);
        action.setParam("serializedPricingConfig",serializedpricingCFG);

        if(isreconfig){
            action.setParam("serializedBOM", JSON.stringify(itemJSON.selectedCharacteristics));
        }

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue().data;
                component.set("v.messages", response.getReturnValue().messages)
                result.characteristics.sort(function(a, b) {
                    return a.SequenceNumber - b.SequenceNumber;
                });
                component.set("v.model", result);
                component.set("v.busy", false);
                component.set("v.showClear", component.get('v.isReconfigure'));
                component.set("v.configValid", result.ConfigurationIsValid);
                component.set("v.allowFinalize", result.ConfigurationIsValid);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },
    // This was planned for a future version
    // variantSearch: function(component) {
    //     component.set("v.selectedVariant", "");
    //     var header = component.find("vcHead");
    //     header.clearConfig();
    //     var materialId = component.get("v.materialId");
    //     if (!materialId) return;
    //     component.set("v.busy", true);
    //     var action = component.get("c.getMaterialVariants");
    //     // action.setParams({ material : materialId });
    //     action.setCallback(this, function(response) {
    //         var state = response.getState();
    //         if (state === "SUCCESS") {
    //             var result = response.getReturnValue();
    //             //console.log(result);
    //             component.set("v.variants", result);
    //             component.set("v.busy", false);

    //         } else {
    //             var errors = response.getError();
    //             if (errors) {
    //                 if (errors[0] && errors[0].message) {
    //                     console.log("Error message: " + errors[0].message);
    //                 }
    //             } else {
    //                 console.log("Unknown error");
    //             }
    //         }
    //     });

    //     $A.enqueueAction(action);
    // },
    // variantSet: function(component, variant) {
    //     var materialId = component.get("v.materialId");
    //     if (!materialId) return;
    //     component.set("v.busy", true);
    //     var action = component.get("c.initializeConfiguration");
    //     action.setParams({
    //         material: materialId
    //     });
    //     action.setCallback(this, function(response) {
    //         var state = response.getState();
    //         if (state === "SUCCESS") {
    //             var result = response.getReturnValue();
    //             //console.log(result);
    //             result.Characteristics.sort(function(a, b) {
    //                 return a.SequenceNumber - b.SequenceNumber;
    //             });
    //             result.SelectedValues = [];
    //             if (
    //                 variant == null ||
    //                 variant == undefined ||
    //                 variant.CharacteristicValues.length == 0
    //             )
    //                 return;
    //             for (var charValue in variant.CharacteristicValues) {
    //                 var selValue = variant.CharacteristicValues[charValue];
    //                 var newSelectedValue = {
    //                     CharacteristicId: selValue.CharacteristicId,
    //                     Value: selValue.Value,
    //                     ValueDescription: selValue.ValueDescription
    //                 };
    //                 result.SelectedValues.push(newSelectedValue);
    //             }
    //             component.set("v.model", result);
    //             component.set("v.busy", false);
    //             component.set("v.showClear", true);
    //             var cmpconfigUpdated = component.getEvent("configUpdated");
    //             cmpconfigUpdated.fire();
    //         } else {
    //             var errors = response.getError();
    //             if (errors) {
    //                 if (errors[0] && errors[0].message) {
    //                     console.log("Error message: " + errors[0].message);
    //                 }
    //             } else {
    //                 console.log("Unknown error");
    //             }
    //         }
    //     });

    //     $A.enqueueAction(action);
    // },
    // applyModel : function(model){
    //     var result = response.getReturnValue();
    //     result.Characteristics.sort(function(a, b) {
    //         return a.SequenceNumber - b.SequenceNumber;
    //     });
    //     component.set("v.model", result);
    //     component.set("v.busy", false);
    //     component.set("v.configValid", result.ConfigurationIsValid);
    //     component.set("v.allowFinalize", result.ConfigurationIsValid);
    //     component.set("v.showClear", true);
    // },
    updateConfiguration: function(component) {
        var config = component.get("v.model");

        var mushedMap = Array.prototype.concat.apply([],Object.values(config.indexedSelectedValues));

        var serializedValues = JSON.stringify(mushedMap);

        delete config.indexedSelectedValues;
        delete config.indexedAllowedValues;
        delete config.characteristics;

        var serializedConfig = JSON.stringify(config);

        var action = component.get("c.processConfiguration");
        action.setParam("cfg", serializedConfig);
        action.setParam("vals", serializedValues);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var result = response.getReturnValue().data;
                component.set("v.messages", response.getReturnValue().messages)
                if(result !== null)
                {
                    result.characteristics.sort(function(a, b) {
                        return a.SequenceNumber - b.SequenceNumber;
                    });
                    component.set("v.model", result);
                    component.set("v.configValid", result.ConfigurationIsValid);
                    component.set("v.allowFinalize", result.ConfigurationIsValid);
                }
                component.set("v.busy", false);
                component.set("v.showClear", true);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },
    updateSettings: function(component) {
        var settings = component.get("v.vcSettings");
        var serializedSettings = JSON.stringify(settings);
        var action = component.get("c.updateSettings");
        action.setParam("settings", serializedSettings);
        component.set("v.busy", true);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue().data;
                component.set("v.messages", response.getReturnValue().messages)
                //console.log(JSON.stringify(result, null, 2));

                component.set("v.vcSettings", result);
                component.set("v.busy", false);
                component.set("v.showSettings", false);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },
    getSettings: function(component) {
        component.set("v.busy", true);
        var action = component.get("c.fetchInitialSettings");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue().data;
                component.set("v.messages", response.getReturnValue().messages)
                //console.log(result);
                component.set("v.vcSettings", result);

                // This is a stub for if we need to select a default frequency setting. For now it is a
                // hardcode. In the long run this this should be updated to actually pull a frequency from a saved
                // Metadata or cookie.
                // component.set("v.vcSettings.FetchConfigurationFrequency", 10);

                component.set("v.busy", false);

            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },
    setCharacteristicValuesFromBOM: function(component) {
        component.set("v.busy", true);
        var bomItems = component.get('v.reconfiguredValues');
        //bomItems.forEach(itm=>console.log(itm));
        var cfg = component.get('v.model')
        var action = component.get("c.setBOMandProcessConfiguration");
        action.setParam("serializedCfg", JSON.stringify(cfg));
        action.setParam("serializedBOM", JSON.stringify(bomItems));
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue().data;
                component.set("v.messages", response.getReturnValue().messages)
                //console.log(JSON.stringify(result, null, 2));
                result.Characteristics.sort(function(a, b) {
                    return a.SequenceNumber - b.SequenceNumber;
                });
                component.set("v.model", result);
                component.set("v.busy", false);
                component.set("v.configValid", result.ConfigurationIsValid);
                component.set("v.allowFinalize", result.ConfigurationIsValid);
                component.set("v.showClear", true);

            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    flowNavigate: function(component)
    {
        var isFinish = component.get('v.isFinish');
        var navigate = component.get("v.navigateFlow");

        if(isFinish)
        {
            navigate("FINISH");
        }else
        {
            navigate("NEXT");
        }
    },

    finishConfig: function(component, helper) {
        var headerJSON = JSON.parse(component.get("v.headerJSON"));
        var itemJSON = JSON.parse(component.get("v.itemJSON"));

        var materialId = component.get('v.materialId');

        var pricingCFG =
        {
            "SalesDocumentType" : headerJSON.salesDocType,
            "SalesOrganization" : headerJSON.salesOrg,
            "DistributionChannel" : headerJSON.distChannel,
            "Division" : headerJSON.division,
            "SoldToParty" : headerJSON.soldToParty,
            "OrderQuantity" : itemJSON.OrderQuantity,
            "Plant" : itemJSON.plant
        };

        if (itemJSON && itemJSON.SalesDocumentCurrency && itemJSON.SalesDocumentCurrency.length > 0)
        {
            pricingCFG.SalesDocumentCurrency = itemJSON.SalesDocumentCurrency;
        }

        var serializedpricingCFG = JSON.stringify(pricingCFG);

        component.set("v.busy", true);

        // This checkes whether or not there is a bill of materials on the object. Verifies taht
        // A). itemJSON exists, B). The selected characteristics Exists C). There are elements in the selected characteristics
        var action = component.get("c.simulateItem");

        action.setParam("material", materialId);
        action.setParam("serializedPricingConfig",serializedpricingCFG);
        action.setParam("serializedBOM", JSON.stringify(itemJSON.selectedCharacteristics));
        //console.log('serializedBOM in finishConfig ===> ' + JSON.stringify(itemJSON.selectedCharacteristics));
        action.setParam("quoteId", component.get('v.quoteId'));

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue().data;
                component.set("v.messages", response.getReturnValue().messages)
                component.set("v.addedItemsJSON", JSON.stringify(result));

                // We need to use the $A syntax if we want to be able to hook into this from outside
                // lighting components. (e.g. from a VFP)
                var finalizeEVT = $A.get("e.c:EVT_VCConfigurationFinalizedConfirmed");
                finalizeEVT.setParam("finalizedConfig", itemJSON.selectedCharacteristics);
                finalizeEVT.fire();

                helper.flowNavigate(component);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.messages", {"message":errors[0].message, "messageType": 'ERROR'});
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.busy", false);
        });

        $A.enqueueAction(action);
    }
});