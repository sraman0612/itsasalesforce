({

    doInit: function (cmp, event, helper) {
        cmp.set("v.searchResults", new Array());
        cmp.set("v.messages", new Array());
    },
    
    downloadInvoice: function (cmp, event, helper) {
        var InvoiceNumber = cmp.get("v.InvoiceNumber");
        console.log("InvoiceNumber ====> " + InvoiceNumber);
        var action = cmp.get("c.reprintInvoice");
        action.setParams({
            SalesDocument: InvoiceNumber
        });
        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = response.getReturnValue().data;
                    cmp.set("v.searchResults", response.getReturnValue().data);
					console.log('PDF response data values below ===>');
                    console.log(data);
                    // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####
                    if (data.PDFB64String != null) {
                        var hiddenElement = document.createElement('a');
                        hiddenElement.href = 'data:application/pdf;base64,' + data.PDFB64String;
                        hiddenElement.target = '_self';
                        hiddenElement.download = 'INVOICE-' + InvoiceNumber + '.pdf';  // PDF file Name* you can change it.[only name not .csv]
                        document.body.appendChild(hiddenElement); // Required for FireFox browser
                        hiddenElement.click(); // using click() js function to download csv file
                    }
                }
                cmp.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                cmp.set("v.messages", noResponse);
            }
            cmp.set("v.displaySpinner", false);
        });
        $A.enqueueAction(action);
        cmp.set("v.displaySpinner", true);
    }

});