({
    getPermissions : function(component) {
        console.log('getPermissions()');
        var action = component.get("c.getIsReadOnly");
        action.setCallback(this, function(result){   
            var state = result.getState();
            if(state === "SUCCESS"){
                var rt = result.getReturnValue();
                if(rt == false){
                    var editB = component.find("submitButton");
                    $A.util.toggleClass(editB,"slds-hide");
                }              
            }
        });
        $A.enqueueAction(action);                                            
    },
    getInfo : function(component) {
        console.log('getInfo()');
        var action = component.get("c.queryAsset");
        action.setParam("assetId", component.get("v.recordId"));
        action.setCallback(this, function(result){  
            var state = result.getState();
            if(state === "SUCCESS"){
                console.log(result.getReturnValue());
                component.set("v.SN", result.getReturnValue());
            }
        });
        $A.enqueueAction(action);                                            
    },
    getHistory : function(component) {
        console.log('getHistory()');
        var action = component.get("c.queryServiceHistory");
        action.setParam("assetId", component.get("v.recordId"));
        action.setCallback(this, function(result){  
            var state = result.getState();
            if(state === "SUCCESS"){
                console.log(result.getReturnValue());
                component.set("v.ServiceHistory", result.getReturnValue());
            }
        });
        $A.enqueueAction(action);                                            
    },
    togglePage : function(component){
        
        var typeD = component.find("typeField");
        $A.util.toggleClass(typeD,"slds-hide");
        
        var typeF = component.find("typeInput");
        
        $A.util.toggleClass(typeF,"slds-hide");
        
        var startD = component.find("startDate");
        $A.util.toggleClass(startD,"slds-hide");
        
        var startF = component.find("startDateInput");
        
        $A.util.toggleClass(startF,"slds-hide");
        var startI = component.find("startDateField");
        startI.set("v.displayDatePicker", true);
        
        var endD = component.find("endDate");
        $A.util.toggleClass(endD,"slds-hide");
        
        var endF = component.find("endDateInput");
        
        $A.util.toggleClass(endF,"slds-hide");
        var endI = component.find("endDateField");
        endI.set("v.displayDatePicker", true);
        
        $A.util.toggleClass(component.find("saveButton"),"slds-hide");
        $A.util.toggleClass(component.find("cancelButton"),"slds-hide");
        $A.util.toggleClass(component.find("submitButton"),"slds-hide");
    },
    saveAsset : function(component){
        var action = component.get("c.saveAsset");
        action.setParam("a", component.get("v.SN"));
        action.setCallback(this, function(result){  
            var state = result.getState();
            if(state === "SUCCESS"){
                console.log(result.getReturnValue());
                this.togglePage(component);
            }
        });
        $A.enqueueAction(action);
    }
})