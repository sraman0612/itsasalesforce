({
	doInit : function(component, event, helper) {
        console.log('Init');
		helper.getInfo(component);
        helper.getHistory(component);
        helper.getPermissions(component);
	},
    
    handleClick : function(component, event, helper){
        
       helper.togglePage(component);
        
    },
    cancel : function(component, event, helper) {
        helper.togglePage(component);
		helper.getInfo(component);
        helper.getHistory(component);
	},
    save : function(component, event, helper) {
        helper.saveAsset(component);
    }
})