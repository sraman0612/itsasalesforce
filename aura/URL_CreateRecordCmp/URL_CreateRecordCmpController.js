({
    // this method is called when component initializes
    onInit: function( component, event, helper ) {
        component.set('v.showSpinner', true);
        console.log('onInit');
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        console.log('sPageURL');
        console.log(sPageURL);
        var pageRef = component.get( 'v.pageReference' );
        
        console.log('PageRef1: ');
        console.log(JSON.stringify(pageRef));
        helper.handleShowCreateForm( component );
    },
    onPageReferenceChanged: function(component, event, helper) {
        console.log('onPageReferenceChanged');
        helper.handleShowCreateForm( component );
    }
})