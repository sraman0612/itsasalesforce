({
    onInit : function(component, event, helper) {
        var items = component.get("v.WarrantyClaimItems");

        if (items == null || items.length == 0) {
            items = [{
                "PartNo": "",
                "PartDescription": "",
                "Quantity": null,
                "Unit": "",
                "GDInvoiceNumber": "",
                "GDInvoiceItemNumber": ""
            }];

            component.set("v.WarrantyClaimItems", items);
        }
    },

    addItem : function(component, event, helper) {
        var items = component.get("v.WarrantyClaimItems");

        items.push({
            "PartNo": "",
            "PartDescription": "",
            "Quantity": null,
            "Unit": "",
            "GDInvoiceNumber": "",
            "GDInvoiceItemNumber": ""
        });

        component.set("v.WarrantyClaimItems", items);
    },

    handleChangeEvent : function(component, event, helper) {
        /*
        var rowStr = event.getParam("Row");
        var row = null;
        
        try {
            row = JSON.parse(rowStr);
        } catch (err) {
            console.log('got an error trying to update: ' + err);
            return;
        }
    
        var rowNumber = event.getParam("RowNumber");
        var items = component.get("v.WarrantyClaimItems");

        items.splice(rowNumber, 1, JSON.parse(rowStr));
        component.set("v.WarrantyClaimItems", items);
        */
    },

    handleRemoveEvent : function(component, event, helper) {

        var rowNumber = event.getParam("RowNumber");
        var items = component.get("v.WarrantyClaimItems");

        items.splice(rowNumber, 1);

        if (items.length == 0) {
            items.push({
                "PartNo": "",
                "PartDescription": "",
                "Quantity": null,
                "Unit": "",
                "GDInvoiceNumber": "",
                "GDInvoiceItemNumber": ""
            });
        }

        component.set("v.WarrantyClaimItems", items);
    }
})