({
    getFolders: function(component) {
        var action = component.get("c.getWrappers");
        action.setCallback(this, function(result) {
            var state = result.getState();
            if (state === "SUCCESS") {
                var parent = component.find('folderUL');
                var data = result.getReturnValue();
                console.log(data);
                for (var i = 0; i < data.length; ++i) {
                    $A.createComponent(
                        "aura:html", {
                            "aura:id": data[i].theFolder.Id + '-0',
                            "tag": "li",
                            "HTMLAttributes": {
                                "class": 'slds-nav-vertical__item',
                                id: data[i].theFolder.Id + '-0'
                            }
                        },
                        function(newEle, status, errorMessage) {
                            if (status === "SUCCESS") {
                                var body = parent.get("v.body");
                                body.push(newEle);
                                $A.createComponent(
                                    "aura:html", {
                                        "aura:id": data[i].theFolder.Id,
                                        "tag": "a",
                                        "HTMLAttributes": {
                                            class: 'slds-nav-vertical__action',
                                            id: data[i].theFolder.Id,
                                            'aura:id': data[i].theFolder.Id,
                                            onclick: component.getReference("c.testCall"),
                                            text: data[i].theFolder.Name
                                        }
                                    },
                                    function(newEle2, status2, errorMessage2) {
                                        if (status2 === "SUCCESS") {
                                            var iBody = newEle.get("v.body");
                                            iBody.push(newEle2);
                                            newEle.set("v.body", iBody);
                                        } else if (status2 === "INCOMPLETE") {
                                            console.log("No response from server or client is offline.");
                                        } else {
                                            console.log("Error: " + errorMessage2);
                                        }
                                    }
                                );
                                parent.set("v.body", body);
                            } else if (status === "INCOMPLETE") {
                                console.log("No response from server or client is offline.");
                            } else {
                                console.log("Error: " + errorMessage);
                            }
                        }
                    );
                    var rParent = component.find('reportHeader');
                    var reports = data[i].theReports;
                    $A.createComponent(
                        "aura:html", {
                            "aura:id": data[i].theFolder.Id + '-1',
                            "tag": "ul",
                            "HTMLAttributes": {
                                "class": 'slds-nav-vertical__item slds-hide slds-ID',
                                id: data[i].theFolder.Id + '-1'
                            }
                        },
                        function(newEle, status, errorMessage) {
                            if (status === "SUCCESS") {
                                var body = rParent.get("v.body");
                                body.push(newEle);
                                for (var j = 0; j < reports.length; j++) {
                                    $A.createComponent(
                                        "aura:html", {
                                            "tag": "li",
                                            "HTMLAttributes": {
                                                "class": 'slds-nav-vertical__item'
                                            }
                                        },
                                        function(newEle2, status2, errorMessage2) {
                                            if (status2 === "SUCCESS") {
                                                var body2 = newEle.get("v.body");
                                                body2.push(newEle2);
                                                $A.createComponent(
                                                    "aura:html", {
                                                        "tag": "a",
                                                        "HTMLAttributes": {
                                                            class: 'slds-nav-vertical__action',
                                                            text: reports[j].Name,
                                                            href : '/s/report/' + reports[j].Id
                                                        }
                                                    },
                                                    function(newEle3, status3, errorMessage3) {
                                                        if (status3 === "SUCCESS") {
                                                            var iBody = newEle2.get("v.body");
                                                            iBody.push(newEle3);
                                                            newEle2.set("v.body", iBody);
                                                        } else if (status3 === "INCOMPLETE") {
                                                            console.log("No response from server or client is offline.");
                                                        } else {
                                                            console.log("Error: " + errorMessage3);
                                                        }
                                                    }
                                                );
                                                newEle.set("v.body", body2);
                                            } else if (status2 === "INCOMPLETE") {
                                                console.log("No response from server or client is offline.");
                                            } else {
                                                console.log("Error: " + errorMessage2);
                                            }
                                        }
                                    );
                                }
                                rParent.set("v.body", body);
                            } else if (status === "INCOMPLETE") {
                                console.log("No response from server or client is offline.");
                            } else {
                                console.log("Error: " + errorMessage);
                            }
                        }
                    );
                    component.set("v.folders", data);
                }
            }
        });
        $A.enqueueAction(action);
    }
})