({
    checkRequestedDate: function(component) {
        // Requirements:
        // Requested Date cannot be in the past
        // If SalesDocType is 'OR' Requested Date cannot be today's date, default to the next date
        // If the requested date is on the weekend, default to the next business date
        let currentRequestedDate = component.get('v.lineItem.ScheduleLineDate');
        let salesDocType = component.get('v.salesDocType');

        if (currentRequestedDate) {
            let requestedDate = this.getDateCorrection(currentRequestedDate);
            let todaysDate = this.getTodaysDate();
            let isRequestedDateInvalid = false;

            // Check if the requested date is less than today's date
            if (salesDocType === 'OR') todaysDate.setDate(todaysDate.getDate() + 1);
            if (requestedDate < todaysDate) {
                isRequestedDateInvalid = true;
                    requestedDate = todaysDate;
            }

            // Check if the requested date is on the weekend
            let isWeekend = this.isDateOnWeekend(requestedDate);

            if (isWeekend) {
                isRequestedDateInvalid = true;
                requestedDate.setDate(requestedDate.getDate() + 1);
                if (this.isDateOnWeekend(requestedDate)) {
                    requestedDate.setDate(requestedDate.getDate() + 1)
                }
            }

            if (isRequestedDateInvalid) {
                let formattedDate = requestedDate.getUTCFullYear() + '-' + (requestedDate.getUTCMonth() + 1) + '-' + requestedDate.getUTCDate().toString().padStart(2, '0');
                component.set('v.lineItem.ScheduleLineDate', formattedDate);
            }
        }
    },

    getDateCorrection: function(date) {
        let newDate = new Date(date);
        let correctedDate = new Date((newDate.getUTCMonth() + 1) + '/' + newDate.getUTCDate() + '/' + newDate.getUTCFullYear());
        return correctedDate;
    },

    getTodaysDate: function() {
        let today = new Date();
        let todaysDate = new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear());
        return todaysDate;
    },

    isDateOnWeekend: function(date) {
        return date.getDay() === 6 || date.getDay() === 0;
    }
})