({
   init : function (component) {
      // Find the component whose aura:id is "flowData"
      var flow = component.find("CZ_New_Quote");
      var inputVariables = [
         // Sets values for fields in the account sObject variable. Id uses the
         // value of the component's accountId attribute. Rating uses a string.
         { name : "recordId", type : "SObject", value: {
             "Id" : component.get("v.opportunityId")
             }
          }
       ];
       flow.startFlow("myFlow", inputVariables);
   }
})