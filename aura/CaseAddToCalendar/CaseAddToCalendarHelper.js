({
    doInit : function(component, event, helper) {
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getCase');

        // params to the method are pass as an object with property names matching
        var params = {
            "caseId" : component.get("v.recordId")
        };

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.case", payload);
                console.log('case Id ===> '+component.get("v.case.Id"));
                console.log('case Primary_Ser_Tech_Contact__c ===> '+component.get("v.case.Primary_Ser_Tech_Contact__c"));
                console.log('case OwnerId ===> '+component.get("v.case.OwnerId"));
                console.log('case Current_Schedule_Date__c ===> '+component.get("v.case.Current_Schedule_Date__c"));
                console.log('case Serial_Number_CC__c ===> '+component.get("v.case.Serial_Number_CC__c"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.createCaseCalendarItem(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },
    createCaseCalendarItem: function(component, event, helper) {
        //prepare url to create the calendar event
        var caseId = component.get("v.recordId");
        var currCase = component.get("v.case");

        var createRecordEvent = $A.get("e.force:createRecord");

        createRecordEvent.setParams({
            "entityApiName": "Event",
            "defaultFieldValues": {
                'WhoId' : currCase.Primary_Ser_Tech_Contact__c,
                'WhatId': caseId,
                'OwnerId': currCase.OwnerId,
                'StartDateTime': currCase.Current_Schedule_Date__c,
                'EndDateTime': currCase.Current_Schedule_End_Date__c,
                'Serial_Number_Owner_Information_Name__c': currCase.Serial_Number_Owner_Information_Name__c,
                'Serial_Number_Owner_Information_C_S__c': currCase.Serial_Number_Owner_Information_C_S__c,
                'Belliss_Service_Type__c': currCase.Belliss_Service_Type__c,
                'Actual_Type_of_Maintenance__c': currCase.Actual_Type_of_Maintenance__c,
                'Serial_Number__c': currCase.Serial_Number_CC__c,
                'IsAllDayEvent': true
            }
        });
        createRecordEvent.fire();
    },
    cancelCaseCalendarItem: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})