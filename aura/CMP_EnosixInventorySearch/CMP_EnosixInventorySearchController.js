({
  onInit: function (component, event, helper) {
    helper.initializeRecordId(component, helper);
    
    component.set('v.columns', [
      { label: $A.get("$Label.c.InvSearch_Column_Quantity"), fieldName: 'Quantity', type: 'number', editable: true, typeAttributes: { max: { fieldName: 'Stock'}}, initialWidth: 100 },
      { label: $A.get("$Label.c.InvSearch_Column_Material"), fieldName: 'Material', type: 'text', sortable: true, initialWidth: 150 },
      { label: $A.get("$Label.c.InvSearch_Column_Material_Description"), fieldName: 'MaterialDescription', type: 'text', sortable: true, initialWidth: 850 },
      { label: $A.get("$Label.c.InvSearch_Column_Stock"), fieldName: 'Stock', type: 'number', sortable: true, cellAttributes: { alignment: 'center' }, initialWidth: 120 },
      { label: $A.get("$Label.c.InvSearch_Column_Scheduled"), fieldName: 'Scheduled', type: 'number', sortable: true, cellAttributes: { alignment: 'center' }, initialWidth: 120 }
    ]);
  },

  refresh: function(component, event, helper) {
    //component.set("v.searchString", '');
    component.set("v.sortedBy", "Material");
    component.set("v.sortedDirection", "asc");
    helper.search(component, helper);
  },

  filter: function(component, event, helper) {
    helper.filterResults(component);
    helper.doPagination(component);
  },

  pageSizeChange: function(component, event, helper) {
    helper.doPagination(component);
  },

  firstPage: function(component, event, helper) {
    component.set("v.currentPageNumber", 1);
    helper.doPagination(component);
  },

  previousPage: function(component, event, helper) {
    var currentPageNumber = component.get("v.currentPageNumber");
    if (currentPageNumber == 1) {
      return;
    }
    component.set("v.currentPageNumber", currentPageNumber - 1);
    helper.doPagination(component);
  },

  nextPage: function(component, event, helper) {
    var currentPageNumber = component.get("v.currentPageNumber");
    var totalPages = component.get("v.totalPages");
    if (currentPageNumber == totalPages) {
      return;
    }
    component.set("v.currentPageNumber", currentPageNumber + 1);
    helper.doPagination(component);
  },

  rowSelected: function(component, event, helper) {
    var selectedRows = event.getParam('selectedRows');

    component.set("v.selectedRows", selectedRows);
  },

  createQuote: function(component, event, helper) {
    helper.createQuote(component);
  },

  lastPage: function(component, event, helper) {
    var totalPages = component.get("v.totalPages");
    component.set("v.currentPageNumber", totalPages);
    helper.doPagination(component);
  },

  updateColumnSorting: function (component, event, helper) {
    component.set("v.displaySpinner", true);
    var fieldName = event.getParam('fieldName');
    var sortDirection = event.getParam('sortDirection');
    component.set("v.sortedBy", fieldName);
    component.set("v.sortedDirection", sortDirection);
    helper.sortData(component, fieldName, sortDirection, helper.columnIsDate(component, fieldName));
    component.set("v.displaySpinner", false);
    helper.doPagination(component);
  },

  statusChanged: function(component, event, helper) {
      component.set("v.displaySpinner", false);
      component.set("v.messages", ["Unable to create quote."]);
  }

})