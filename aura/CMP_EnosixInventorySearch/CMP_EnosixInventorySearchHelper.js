({
    search: function (component, helper) {
        var action = component.get("c.doSearch");
        action.setParams({
            distributionChannel: component.get("v.newDistributionChannel"),
            salesOrg: component.get("v.salesOrg")
        });

        component.set("v.displaySpinner", true);

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    component.set("v.results", data);
                    helper.filterResults(component);
                    helper.doPagination(component);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
            component.set("v.displaySpinner", false);
        });
        $A.enqueueAction(action);
    },

    filterResults: function (component) {
        var searchString = component.get("v.searchString");
        var results = component.get("v.results");

        var filteredResults = [];

        for (var i = 0; i < results.length; i++) {
            var filterString = (results[i].Material?results[i].Material.toUpperCase():"") + "|" + (results[i].MaterialFamily?results[i].MaterialFamily.toUpperCase():"") + "|" + (results[i].MaterialDescription?results[i].MaterialDescription.toUpperCase():"");
            if (!searchString || searchString === "" || filterString.includes(searchString.toUpperCase())) {
                filteredResults.push(results[i]);
            }
        }

        component.set("v.filteredResults", filteredResults);
    },

    doPagination: function (component) {
        component.set("v.displaySpinner", true);
        var filteredResults = component.get("v.filteredResults");
        var pageSizeVar = component.get("v.pageSize");
        var pageSize = parseInt(pageSizeVar);
        var currentPageNumber = component.get("v.currentPageNumber");

        var totalPages = Math.ceil(filteredResults.length / pageSize);

        component.set("v.totalPages", totalPages);

        if (!currentPageNumber || currentPageNumber > totalPages) {
            currentPageNumber = 1;

            component.set("v.currentPageNumber", currentPageNumber);
        }

        var currentPage = [];

        var offset = (currentPageNumber - 1) * pageSize;

        var endpoint = offset + pageSize;
        if (endpoint > filteredResults.length) {
            endpoint = filteredResults.length;
        }

        for (var i = offset; i < endpoint; i++) {
            currentPage.push(filteredResults[i]);
        }

        component.set("v.currentPage", currentPage);
        component.set("v.displaySpinner", false);
    },

    initializeRecordId: function(component, helper) {
        var recordId = component.get("v.recordId");

        component.set("v.displaySpinner", true);

        if (!recordId) {
            component.set("v.fromCommunity", true);
        }

        var action = component.get("c.getCommunityUserInfo");
        if (recordId) {
            action.setParams({
                "recordId" : component.get('v.recordId') 
            });
        }
        action.setCallback(this, function(response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var userInfo = JSON.parse(response.getReturnValue().data);

                    if (userInfo.AccountId != null) {
                        component.set("v.accountId", userInfo.AccountId);
                        console.log('Account id is ' + component.get("v.accountId"));
                    }
        
                    if (userInfo.OpportunityId != null) {
                        component.set("v.opportunityId", userInfo.OpportunityId);
                        console.log('Opportunity id is ' + component.get("v.opportunityId"));
                    }
        
                    if (userInfo.DistributionChannels != null && userInfo.DistributionChannels.length > 0) {
                        component.set("v.availableDistributionChannels", userInfo.DistributionChannels);
                        var distributionChannel = userInfo.DistributionChannels[0].value;
                        component.set("v.newDistributionChannel", distributionChannel);
                        console.log('distribution channel is ' + distributionChannel.value);
                    }
                }
            }
            component.set("v.displaySpinner", false);
            helper.search(component, helper);
        });

        $A.enqueueAction(action);
    },

    columnIsDate: function (component, fieldName) {
        var columnIsDate = false;

        var columns = component.get("v.columns");

        columns.forEach(function (arrayItem) {
            if (arrayItem.fieldName == fieldName) {
                if (arrayItem.type == 'date-local') {
                    columnIsDate = true;
                }
                return;
            }
        });

        return columnIsDate;
    },

    createQuote: function (component) {
        component.set("v.displaySpinner", true);

        var accountId = component.get("v.accountId");
        var draftValues = component.find('stockListDatatable').get("v.draftValues");

        var changedValues = new Map();

        if (draftValues) {
            draftValues.forEach((row) => {
                console.log('row.Quantity=' + row.Quantity);
                changedValues.set(row.Material, row.Quantity);
            });
        }

        var selectedRows = component.get("v.selectedRows");
        var distributionChannel = component.get("v.newDistributionChannel");

        if (selectedRows && changedValues.size > 0) {
            selectedRows.forEach((row) => {
                var curQuantity = 1;
                if (changedValues.get(row.Material)) {
                    curQuantity = changedValues.get(row.Material);
                }
                row.Quantity = curQuantity;
            });
        }

        var action = component.get("c.doCreateQuote");

        action.setParams({
            accountId: accountId,
            distributionChannel: distributionChannel,
            selectedRows: selectedRows
        });

        action.setCallback(this, function(response) {
            console.log('in callback');
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var cpqURL = response.getReturnValue().data;
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                      "url": cpqURL
                    });
                    urlEvent.fire();
                }
                component.set("v.messages", response.getReturnValue().messages);
            }
            component.set("v.displaySpinner", false);
        });

        $A.enqueueAction(action);
    },

    sortData: function (component, fieldName, sortDirection, columnIsDate) {
        var data = component.get("v.filteredResults");
        var reverse = sortDirection !== "asc";

        data = Object.assign(
            [],
            data.sort(this.sortBy(fieldName, columnIsDate, reverse ? -1 : 1))
        );
        component.set("v.filteredResults", data);
    },

    sortBy: function (field, columnIsDate, reverse, primer) {
        var key = primer
            ? function (x) {
                if (columnIsDate && primer(x[field]) == null) {
                    return reverse > 0 ? "2270-01-01" : "1970-01-01";
                }
                return primer(x[field]);
            }
            : function (x) {
                if (columnIsDate && x[field] == null) {
                    return reverse > 0 ? "2270-01-01" : "1970-01-01";
                }
                return x[field];
            };

        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    }
})