({
    
	doInit : function(cmp, event, helper) {
		cmp.set('v.columns', [
            { label: 'Account', fieldName: 'accountName', type: 'text', sortable: false, hideDefaultActions: true,
            	cellAttributes: { class: { fieldName: 'iConnCellFormat' } } },
            { label: 'Serial Number', fieldName: 'Name', type: 'text', sortable: true, hideDefaultActions: true,
             	cellAttributes: { class: { fieldName: 'iConnCellFormat' } } },
            { label: 'Model Number', fieldName: 'Model_Number__c', sortable: true, hideDefaultActions: true,
             	cellAttributes: { class: { fieldName: 'iConnCellFormat' } } },
            { label: 'Active iConn Contacts', fieldName: 'Active_iConn_Contacts__c', type: 'text', hideDefaultActions: true,
             	cellAttributes: { class: { fieldName: 'iConnCellFormat' } } }
        ]);
        
        cmp.set('v.selectedColumns', [
            { label: 'Account', fieldName: 'accountName', type: 'text', sortable: true, hideDefaultActions: true },
            { label: 'Serial Number', fieldName: 'Name', type: 'text', sortable: true, hideDefaultActions: true },
            { label: 'Model Number', fieldName: 'Model_Number__c', sortable: true, hideDefaultActions: true },
            { label: 'Alert Preferences', initialWidth: 140, fieldName: 'Alert_Preferences__c', hideDefaultActions: true },
            { type: 'button', initialWidth: 84, typeAttributes: {
                label: 'Email',
                name: 'Email',
                title: 'Email',
                value: 'Email',
                disabled: false,
                iconPosition: 'left'
            }},
            { type: 'button', initialWidth: 78, typeAttributes: {
                label: 'Text',
                name: 'Text',
                title: 'Text',
                value: 'Text',
                disabled: false,
                iconPosition: 'left'
            }},
            { type: 'button', initialWidth: 78, typeAttributes: {
                label: 'Both',
                name: 'Both',
                title: 'Both',
                value: 'Both',
                disabled: false,
                iconPosition: 'left'
            }}
        ]);
        
        helper.fetchContact(cmp);
        helper.fetchData(cmp);
	},
    
    loadMoreData: function(cmp, event, helper) {
        if (cmp.get('v.isLoading')) return;
        helper.fetchData(cmp);
    },
    
    onRowSelection: function (cmp, event, helper) {
        const MAX_ICONN_CONTACTS = 10;
        var selectedRows = event.getParam('selectedRows');
        if (selectedRows.find(r => r.Active_iConn_Contacts__c >= MAX_ICONN_CONTACTS)) {
            helper.toastMsg('error', 'Serial Number has max iConn contacts (' + MAX_ICONN_CONTACTS + ')');
        }
        cmp.set('v.selectedRows', selectedRows.filter(r => r.Active_iConn_Contacts__c < MAX_ICONN_CONTACTS).map(r => r.Id));
    },
    
    updateColumnSorting: function (cmp, event, helper) {
        var sortField = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        var selectedRows = cmp.get('v.selectedRows');
        
        if (selectedRows && selectedRows.length) {
            cmp.set('v.tmpSortField', sortField);
            cmp.set('v.tmpSortDirection', sortDirection);
            cmp.set('v.showSortClearSelectionModal', true);
            return;
        }
        
        cmp.set('v.sortField', sortField);
        cmp.set('v.sortDirection', sortDirection);
        helper.fetchData(cmp, true);
    },
    
    updateColumnSortingManual: function(cmp, event, helper) {
        cmp.set('v.sortField2', event.getParam('fieldName'));
        cmp.set('v.sortDirection2', event.getParam('sortDirection'));
        var data = helper.manualSort(cmp.get('v.selectedData'), event.getParam('fieldName'), event.getParam('sortDirection'));
        cmp.set('v.selectedData', data);
    },
    
    clickNext: function(cmp, event, helper) {
        cmp.set('v.isSelectTblVisible', false);
        var selectedRows = cmp.get('v.selectedRows');
        var data = cmp.get('v.data');
        var selectedData = data.filter(d => !!~selectedRows.indexOf(d.Id));
        cmp.set('v.selectedData', selectedData);
    },
    
    clickBack: function(cmp, event, helper) {
        cmp.set('v.isSelectTblVisible', true);
    },
    
    clickSave: function(cmp, event, helper) {
        var selectedData = cmp.get('v.selectedData');
        if (!(selectedData && selectedData.length)) return helper.toastMsg('error', 'Nothing selected');
        
        var contact = cmp.get('v.theContact');
        var needMobile = !!selectedData.find(d => ['Text', 'Both'].includes(d.Alert_Preferences__c)) && !contact.iConn_Mobile_Number__c;
        var needEmail = !!selectedData.find(d => ['Email', 'Both'].includes(d.Alert_Preferences__c)) && !contact.Email;
        
        if (needMobile || needEmail) {
            cmp.set('v.showContactModal', true);
            if (!needEmail) $A.util.addClass(cmp.find('contactModalEmail'), 'slds-hide');
            if (!needMobile) $A.util.addClass(cmp.find('contactModalMobile'), 'slds-hide');
            return;
        }
        
        helper.createiConnContacts(cmp);
    },
    
    clickModalSave: function(cmp, event, helper) {
        var emailCmp = cmp.find('contactModalEmail');
        var email = emailCmp.get('v.value').trim();
        var mobileCmp = cmp.find('contactModalMobile');
        var mobile = mobileCmp.get('v.value').trim();
        var contact = { Id: cmp.get('v.theContact').Id };
        
        if (email && !emailCmp.get('v.validity').valid) return;
        if (mobile && !mobileCmp.get('v.validity').valid) return;
        
        cmp.set('v.showContactModal', false);
        
        if (email) contact.Email = email;
        if (mobile) contact.iConn_Mobile_Number__c = mobile;
        
        helper.updateContact(cmp, contact);
        helper.createiConnContacts(cmp);
    },
    
    clickModalCancel: function(cmp, event, helper) {
        cmp.set('v.showContactModal', false);
    },
    
    clickSortModalCancel: function(cmp, event, helper) {
        cmp.set('v.showSortClearSelectionModal', false);
    },
    
    clickSortModalContinue: function(cmp, event, helper) {
        cmp.set('v.showSortClearSelectionModal', false);
        cmp.set('v.sortField', cmp.get('v.tmpSortField'));
        cmp.set('v.sortDirection', cmp.get('v.tmpSortDirection'));
        helper.fetchData(cmp, true);
        
    },
    
    handleRowAction : function(cmp, event, helper){
        var recId = event.getParam('row').Id;
        var actionName = event.getParam('action').name;
        if (['Email', 'Text', 'Both'].includes(actionName)) {
            var data = cmp.get('v.selectedData');
            data.find(r => r.Id === recId).Alert_Preferences__c = actionName;
            cmp.set('v.selectedData', data);
        }
    }
    
})