({
    fetchContact: function(cmp) {
        var contactAction = cmp.get('c.getContact');
        contactAction.setParams({ contactId: cmp.get('v.recordId') });
        contactAction.setCallback(this, function(result) {
            if (result.getState() !== 'SUCCESS') return this.toastMsg('error', 'An error occurred while loading Contact');
            if (result.getReturnValue().Inactive__c) return this.toastMsg('error', 'iConn contacts cannot be created for an inactive contact', null, 10000);
            cmp.set('v.theContact', result.getReturnValue());
        });
        $A.enqueueAction(contactAction);
    },
    
    updateContact: function(cmp, updContact) {
        var contactAction = cmp.get('c.updateContact');
        contactAction.setParams({ updContact: updContact });
        contactAction.setCallback(this, function(result) {
            if (result.getState() !== 'SUCCESS') return this.toastMsg('error', 'An error occurred while updating Contact');
            this.fetchContact(cmp);
        });
        $A.enqueueAction(contactAction);
    },
    
    fetchData : function(cmp, clearOldData) {
        cmp.set('v.isLoading', true);
        
        if (clearOldData) {
			this.clearOldData(cmp);
        }

		var action = cmp.get("c.getSerialNumbers");
        var rowsToLoad = cmp.get('v.rowsToLoad');
        var totalRowsLoaded = cmp.get('v.totalRowsLoaded');
        cmp.set('v.totalRowsLoaded', totalRowsLoaded + rowsToLoad)

        action.setParams({
            contactId: cmp.get('v.recordId'),
            sortField: cmp.get('v.sortField'),
            sortDirection: cmp.get('v.sortDirection'),
            recordOffset: totalRowsLoaded,
            recordLimit: rowsToLoad
        });

        action.setCallback(this, function(result){  
            cmp.set('v.isLoading', false);
            if(result.getState() !== "SUCCESS") {
                return this.toastMsg('error', 'An error occurred while loading available iConn Serial Numbers');
            }
            
            if (result.getReturnValue().totalRecords == 0) {
                return this.toastMsg('info', 'There are no available serial numbers', null, 10000);
            }
            
            var data = totalRowsLoaded ? cmp.get('v.data') : [];
            var newData = result.getReturnValue().results;
            newData.forEach(function(d) {
                d.Active_iConn_Contacts__c = d.Active_iConn_Contacts__c || 0;
                d.accountName = d.Account && d.Account.Name;
                d.Alert_Preferences__c = 'Email';
                d.iConnCellFormat = d.Active_iConn_Contacts__c >= 10 && 'iconn-asset-not-avail';
            });
            if (!newData.length) cmp.set('v.enableInfiniteLoading', false);
            cmp.set('v.data', data.concat(newData));
            cmp.set('v.totalRecords', result.getReturnValue().totalRecords);
        });
        
        $A.enqueueAction(action);
	},
    
    clearOldData: function(cmp) {
        cmp.set('v.totalRowsLoaded', 0);
        cmp.set('v.selectedRows', []);
        cmp.set('v.selectedData', []);
        cmp.set('v.data', []);
    },
    
    createiConnContacts: function(cmp) {
        cmp.set('v.isSaving', true);
        
        var action = cmp.get('c.insertiConnRecords');
        var updiConnContacts = cmp.get('v.selectedData').map(function(d) {
            return {
                Serial_Number__c: d.Id,
                Alert_Preferences__c: d.Alert_Preferences__c
            };
        });
        action.setParams({
            contactId: cmp.get('v.recordId'),
            updiConnContacts: updiConnContacts
        });
        action.setCallback(this, function(result) {
            cmp.set('v.isSaving', false);
            if (result.getState() !== 'SUCCESS' || !result.getReturnValue()) {
                return this.toastMsg('error', 'Error saving new iConn Contacts');
            }
            this.toastMsg('success', 'Saved');
            cmp.set('v.isSelectTblVisible', true);
            $A.get('e.force:refreshView').fire();
            this.fetchData(cmp, true);
        });
        $A.enqueueAction(action);
    },
    
    manualSort: function(data, fieldName, sortDirection) {
        var key = function(a) { return a[fieldName]; };
        var reverse = sortDirection === 'asc' ? 1 : -1;
        data.sort(function(a, b) {
            var aa = key(a) || '';
            var bb = key(b) || '';
            return reverse * ((aa > bb) - (bb > aa));
        });
        return data;
    },
    
    toastMsg: function(type, msg, mode, dur) {
        var showToast = $A.get('e.force:showToast')
        showToast.setParams({
            message: msg,
            type: type, // 'success', 'error', 'warning', 'info'
            mode: mode || 'dismissible', // 'dismissible', 'pester', 'sticky'
            duration: dur || 5000
        });
        showToast.fire();
    }
})