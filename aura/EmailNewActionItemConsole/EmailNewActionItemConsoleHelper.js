({
    doInit : function(component, event, helper) {
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getCase');

        // params to the method are pass as an object with property names matching
        var params = {
            "emailId" : component.get("v.recordId")
        };

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.case", payload);
                console.log('case Id ===> '+component.get("v.case.Id"));
                console.log('case OwnerId ===> '+component.get("v.case.OwnerId"));
                console.log('case Status ===> '+component.get("v.case.Status"));
                console.log('case AccountId ===> '+component.get("v.case.AccountId"));
                console.log('case Org_Wide_Email_Address_ID__c ===> '+component.get("v.case.Org_Wide_Email_Address_ID__c"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.getRecordType(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },
    getRecordType :function(component, event, helper){
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getCaseRecType');

        // params to the method are pass as an object with property names matching
        var params = {
            "name" : component.get("v.recordTypeName")
        };

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.recordTypeId", payload);
                console.log('case record type name ===> '+component.get("v.recordTypeName"));
                console.log('case record type Id ===> '+component.get("v.recordTypeId"));
                helper.createCaseAI(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
            }
        );
    },
    createCaseAI : function(component, event, helper) {
        var emailId = component.get("v.recordId");
        var caseId = component.get("v.case.Id");
        var acctId = component.get("v.case.AccountId");
        var orgWideEmail = component.get("v.case.Org_Wide_Email_Address_ID__c");
        console.log('orgWideEmail ====> '+orgWideEmail);
        var caseRecTypeId = component.get("v.recordTypeId");
        console.log('case,acct and recType ===> '+caseId+', '+acctId+', '+caseRecTypeId);
        if (orgWideEmail != "" && orgWideEmail != null) {
            var workspaceAPI = component.find("workspace");
            var url = '/apex/C_Action_Item?RecordTypeId='+caseRecTypeId+'&ParentId='+caseId+'&OriginatingId='+caseId+
                '&AccountId='+acctId+'&isFollowUp=FALSE&EmailId='+emailId;
            console.log(url);
            workspaceAPI.openTab({
                url:  url,
                focus: true
            });
            
            /*
            var urlEvent = $A.get("e.force:navigateToURL");
        	urlEvent.setParams({
            	"url": '/'+url,
            	"target": "_self"
        	});
        	urlEvent.fire();
            */
        }
        else {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "title" : "Error",
                "message" : "The case is missing an org wide email address.",
                "type": "warning"
            });
            resultsToast.fire();
        }
    }
})