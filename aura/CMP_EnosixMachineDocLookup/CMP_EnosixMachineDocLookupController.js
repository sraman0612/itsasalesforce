({
  onInit: function (component, event, helper) {
    component.set('v.columns', [
      { type: 'button-icon', fixedWidth: 40, typeAttributes: { iconName: 'utility:download', name: 'downloadDocument', title: 'Download File', variant: 'border-filled', alternativeText: 'Download File', disabled: false }},
      { label: 'Title', fieldName: 'title', type: 'text', sortable: false, initialWidth: 360 },
      { label: 'File Name', fieldName: 'filename', type: 'text', sortable: false, initialWidth: 255 },
      { label: 'Type', fieldName: 'type', type: 'text', sortable: false, initialWidth: 82 },
      { label: 'Description', fieldName: 'description', type: 'text', sortable: false, initialWidth: 260 },
      { label: 'File Size', fieldName: 'fileSizeString', type: 'text', sortable: false, initialWidth: 90, cellAttributes: { alignment: 'right' } }
    ]);

    helper.getScreenData(component, helper);
    helper.search(component, helper);
  },

  refresh: function (component, event, helper) {
    helper.clearSelectedRows(component, helper);
              
    helper.search(component, helper);
  },

  documentSourceChanged: function(component, event, helper) {
    var selectedDocumentSource = component.get("v.selectedDocumentSource");
    var quoteProducts = component.get("v.quoteProducts");
    var selectedQuoteProduct = '';

    if (quoteProducts.length > 0 && ("SAP Documents" === selectedDocumentSource || "GDInside Documents" === selectedDocumentSource)) {
      var firstQuoteProduct = quoteProducts[0];

      selectedQuoteProduct = firstQuoteProduct.value;
    }

    component.set("v.selectedQuoteProduct", selectedQuoteProduct);

    helper.search(component, helper);
  },

  quoteProductChanged: function(component, event, helper) {
    helper.search(component, helper);
  },

  filterDocumentList: function(component, event, helper) {
    helper.filterDocumentList(component, helper);
  },

  handleRowAction: function (component, event, helper) {
    var action = event.getParam("action");
    var row = event.getParam("row");

    switch (action.name) {
      case "downloadDocument":
        helper.download(row.id, row.filename, row.downloadLink, row.source, component, helper);
        break;
    }
  },

  downloadSelected: function(component, event, helper) {
    helper.downloadSelected(component, helper);
  },

  rowSelected: function(component, event, helper) {
    helper.manageSelectedRows(component, event, helper);
  },

  emailProposal: function(component, event, helper) {
    //helper.initializeEmail(component, helper);
    component.set("v.emailIsOpen", true);
  },

  closeEmailModal: function(component, event, helper) {
    component.set("v.emailIsOpen", false);
  },

  addSendToRecipient: function(component, event, helper) {
    var sendToEmailAddresses = component.get("v.sendToEmailAddresses");
    var selectedSendToEmailAddresses = component.get("v.selectedSendToEmailAddresses");
    
    var newRecipientName = component.get("v.newToRecipientName");
    var newRecipientEmail = component.get("v.newToRecipientEmail");
    if (!newRecipientEmail || !helper.validateEmail(newRecipientEmail)) {
      return;
    }

    var thisLabel = newRecipientName==null?newRecipientEmail:newRecipientName + " <" + newRecipientEmail + ">";

    sendToEmailAddresses.push({label: thisLabel, value: newRecipientEmail});
    selectedSendToEmailAddresses.push(newRecipientEmail);

    component.set("v.newToRecipientName", '');
    component.set("v.newToRecipientEmail", '');

    component.set("v.sendToEmailAddresses", sendToEmailAddresses);
    component.set("v.selectedSendToEmailAddresses", selectedSendToEmailAddresses);
  },

  addCCRecipient: function(component, event, helper) {
    var sendToEmailAddresses = component.get("v.ccEmailAddresses");
    var selectedSendToEmailAddresses = component.get("v.selectedCCEmailAddresses");
    
    var newRecipientName = component.get("v.newCCRecipientName");
    var newRecipientEmail = component.get("v.newCCRecipientEmail");
    if (!newRecipientEmail || !helper.validateEmail(newRecipientEmail)) {
      return;
    }

    var thisLabel = newRecipientName==null?newRecipientEmail:newRecipientName + " <" + newRecipientEmail + ">";

    sendToEmailAddresses.push({label: thisLabel, value: newRecipientEmail});
    selectedSendToEmailAddresses.push(newRecipientEmail);

    component.set("v.newCCRecipientName", '');
    component.set("v.newCCRecipientEmail", '');

    component.set("v.ccEmailAddresses", sendToEmailAddresses);
    component.set("v.selectedCCEmailAddresses", selectedSendToEmailAddresses);
  },

  sendEmail: function(component, event, helper) {
    helper.sendEmail(component, helper);
    component.set("v.emailIsOpen", false);
  }
})