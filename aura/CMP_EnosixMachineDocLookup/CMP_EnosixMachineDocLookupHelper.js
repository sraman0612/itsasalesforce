({
    search: function (component, helper) {
        var token = component.get("v.gditoken");
        var recordId = component.get("v.recordId");

        if (!token) {
            helper.getToken(component, helper, helper.search);
            return;
        }

        var action = component.get("c.doSearch");

        action.setParams({
            token: token,
            quoteId: recordId
        });

        helper.displaySpinner(component);

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    console.log(data);
                    component.set("v.results", data);

                    console.log('data.availableProductCategories.length=' + data.availableProductCategories.length);

                    component.set("v.selectedCategory", data.availableProductCategories[0].value);

                    helper.filterDocumentList(component, helper);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }
            helper.hideSpinner(component);
        });

        $A.enqueueAction(action);
    },

    getScreenData: function(component, helper) {
        console.log("getting screen data for machine doc lookup");
        var recordId = component.get("v.recordId");

        var action = component.get("c.getScreenData");

        action.setParams({
            quoteId: recordId
        });

        helper.displaySpinner(component);

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    console.log(data);

                    component.set("v.sendToEmailAddresses", data.sendToEmailAddresses);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }
            helper.hideSpinner(component);
        });

        $A.enqueueAction(action);
    },

    manageSelectedRows: function(component, event, helper) {
        var allSelectedRows = component.get("v.allSelectedRows");

        var oldSelectedIds = component.get("v.selectedIds");
    
        var selectedIds = new Array();
    
        var eventSelectedRows = event.getParam("selectedRows");
    
        eventSelectedRows.forEach(function (cur) {
          selectedIds.push(cur.id);
          if (!allSelectedRows.includes(cur)) {
            allSelectedRows.push(cur);
          }
        });
    
        oldSelectedIds = oldSelectedIds.filter(function(val) {
          return !selectedIds.includes(val);
        });
    
        allSelectedRows = allSelectedRows.filter(function(val) {
          return !oldSelectedIds.includes(val.id);
        });
        
        component.set("v.selectedIds", selectedIds);
        component.set("v.allSelectedRows", allSelectedRows);
    },

    clearSelectedRows: function(component, helper) {
        component.set("v.allSelectedRows", []);
        component.set("v.selectedIds", []);    
    },

    initializeEmail: function(component, helper) {
        var recordId = component.get("v.recordId");

        var action = component.get("c.getRelatedEmailAddresses");

        action.setParams({
            quoteId: recordId
        });

        helper.displaySpinner(component);

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    console.log(data);

                    if (data.length > 0) {

                        var sendToEmailAddresses = component.get("v.sendToEmailAddresses");
                        var selectedEmails = component.get("v.selectedSendToEmailAddresses");;

                        if (!sendToEmailAddresses) {
                            sendToEmailAddresses = [];
                        }

                        if (!selectedEmails) {
                            selectedEmails = [];
                        }

                        data.forEach(function(cur) {
                            if (!sendToEmailAddresses.includes(cur)) {
                                sendToEmailAddresses.push(cur);
                                selectedEmails.push(cur.value);
                            }
                        });
                        component.set("v.sendToEmailAddresses", data);
                        component.set("v.selectedSendToEmailAddresses", selectedEmails);
                    }

                    component.set("v.emailIsOpen", true);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
            helper.hideSpinner(component);
        });

        $A.enqueueAction(action);
    },

    filterDocumentList: function(component, helper) {
        var selectedCategory = component.get("v.selectedCategory");

        if (!selectedCategory) {
            selectedCategory = "All Documents";
        }

        const allSelectedRows = component.get("v.allSelectedRows");
        var allSelectedIds = [];
        allSelectedRows.forEach((row) => {
            allSelectedIds.push(row.id);
        });

        var selectedIds = [];
        var documentList = [];
    
        var results = component.get("v.results");

        results.documents.forEach((row) => {
            if (row.documentCategories.includes(selectedCategory)) {
                documentList.push(row);
                if (allSelectedIds.includes(row.id)) {
                    selectedIds.push(row.id);
                }
            }
        });

        component.set("v.selectedIds", selectedIds);
    
        component.set("v.documentList", documentList);
    },

    getToken: function(component, helper, callBack) {
        var action = component.get("c.getToken");

        helper.displaySpinner(component);

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = response.getReturnValue().data;
                    component.set("v.gditoken", data);
                    if (callBack) {
                        callBack(component, helper);
                    }
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    downloadSelected: function(component, helper) {
        var allSelectedRows = component.get("v.allSelectedRows");

        allSelectedRows.forEach(function (row) {
            helper.download(row.id, row.filename, row.downloadLink, row.source, component, helper);
        });
    },

    download: function (rowId, filename, download_link, source, component, helper) {
        helper.displaySpinner(component);

        var target = '_self';

        if (download_link) {
            var hiddenElement = document.createElement('a');
            hiddenElement.href = download_link;
            hiddenElement.target = '_blank';
            hiddenElement.download = filename;
            document.body.appendChild(hiddenElement);
            hiddenElement.click();

            helper.hideSpinner(component);
            return;
        }
        
        var token = component.get("v.gditoken");

        if (!token) {
            helper.getToken(component, helper, helper.downloadGDInside);
            helper.hideSpinner(component);
            return;
        }

        var action = component.get("c.doDownload");

        action.setParams({
            token: token,
            id: rowId,
            filename: filename,
            source: source
        });

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);

                    var blob = helper.b64toBlob(data.FileB64String, data.HTMLContentType);
                    var blobUrl = URL.createObjectURL(blob);

                    //console.log(data.HTMLContentType);
                    //console.log(data.FileName);
                    //console.log(data.FileB64String);

                    var hiddenElement = document.createElement('a');
                    hiddenElement.href = blobUrl;
                    hiddenElement.target = target;
                    hiddenElement.download = data.FileName;
                    document.body.appendChild(hiddenElement);
                    hiddenElement.click();

                    //console.log('hiddenElement.href=' + hiddenElement.href);
                } else {
                    component.set("v.gditoken", null);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    b64toBlob: function(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);
          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }
          var byteArray = new Uint8Array(byteNumbers);
          byteArrays.push(byteArray);
        }
      
        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
    },
      
      
    displaySpinner: function(component) {
        var displaySpinner = component.get("v.displaySpinner");

        component.set("v.displaySpinner", displaySpinner + 1)
    },

    hideSpinner: function(component) {
        var displaySpinner = component.get("v.displaySpinner");

        component.set("v.displaySpinner", displaySpinner - 1)
    },

    sendEmail: function(component, helper) {
        var selectedRows = component.get("v.allSelectedRows");

        var rowOutput = [];

        selectedRows.forEach(function(row) {
            rowOutput.push(row.source + "," + (row.downloadLink?row.downloadLink:row.id) + ',' + row.filename);
        });

        var action = component.get("c.doSendEmail");

        action.setParams({
            quoteId: component.get("v.recordId"),
            sendToEmailAddresses: component.get("v.selectedSendToEmailAddresses"),
            ccEmailAddresses: component.get("v.selectedCCEmailAddresses"),
            includeme: component.get("v.includeme"),
            subject: component.get("v.emailSubject"),
            body: component.get("v.emailBody"),
            selectedRows: rowOutput
        });

        action.setCallback(this, function (response) {
            if (response.getReturnValue().messages.length > 0) {
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                component.set("v.messages", []);
                var toastEvent = $A.get("e.force:showToast");

                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The proposal email was sent successfully."
                });

                toastEvent.fire();
            }
        });

        $A.enqueueAction(action);
    },

    validateEmail: function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
})