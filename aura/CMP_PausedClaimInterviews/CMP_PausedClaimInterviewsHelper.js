({
    loadScreenData : function(component, helper) {
        var action = component.get("c.getPausedClaims");

        action.setCallback(this, function (response) {
            console.log("response="  + response.getState());
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = response.getReturnValue().data;
                    console.log(data);
                    component.set("v.SCREEN_DATA", data);
                    helper.filter(component, helper);

                    if (data.interviews.length > 0) {
                        var uniqueUsers = new Set();

                        data.interviews.forEach(function (row) {
                            uniqueUsers.add(row.CreatedBy.Name);
                        });

                        console.log('warranty admin ' + data.warrantyAdmin);
                        if (data.warrantyAdmin) {
                            component.set("v.showUserFilter", true);
                        }

                        var userDropdown = [];

                        var uniqueUsersList = Array.from(uniqueUsers).sort();

                        userDropdown.push({"label":"All","value:":""});
                        uniqueUsersList.forEach(function (row) {
                            userDropdown.push({"label":row,"value:":row});
                        });

                        component.set("v.userDropdown", userDropdown);
                    }
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }

            component.set("v.displaySpinner", false);
        });

        component.set("v.displaySpinner", true);
        $A.enqueueAction(action);
    },

    filter : function(component, helper) {
        var SCREEN_DATA = component.get("v.SCREEN_DATA");
        var selectedOwner = component.get("v.selectedOwner");

        console.log("selectedOwner=" + selectedOwner);

        var interviews = [];

        SCREEN_DATA.interviews.forEach(function(row) {
            if (!selectedOwner || selectedOwner == "All") {
                interviews.push(row);
            } else if (row.CreatedBy.Name == selectedOwner) {
                interviews.push(row);
            }
        });

        if (interviews.length > 4) {
            component.set("v.scrollableClass", "slds-scrollable slds-border_top slds-m-top_xx-small");
            component.set("v.scrollableStyle", "height:36.4rem");
            component.set("v.containerClass", "slds-p-left_medium slds-p-right_small");
        } else {
            component.set("v.scrollableClass", "slds-border_top slds-m-top_xx-small");
            component.set("v.scrollableStyle", "");
            component.set("v.containerClass", "slds-p-left_medium slds-p-right_xxx-large");
        }

        component.set("v.title", "Paused Claims (" + interviews.length + ")");

        component.set("v.interviews", interviews);
    },

    delete : function(component, helper) {
        var action = component.get("c.deleteInterview");

        action.setParams({
            interviewId: component.get("v.selectedId")
        });

        action.setCallback(this, function (response) {
            console.log("response="  + response.getState());
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = response.getReturnValue().data;
                    console.log(data);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }

            component.set("v.displaySpinner", false);
            helper.loadScreenData(component, helper);
        });

        component.set("v.displaySpinner", true);
        $A.enqueueAction(action);
    }
})