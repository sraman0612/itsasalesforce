({
    onInit : function(component, event, helper) {
        helper.loadScreenData(component, helper);
    },

    refresh : function(component, event, helper) {
        helper.loadScreenData(component, helper);
    },

    delete : function(component, event, helper) {
        component.set("v.selectedId", event.getSource().get("v.name"));
        console.log(event.getSource().get("v.name"));
        component.set("v.showDeleteModal", true);
    },

    resume : function(component, event, helper) {
        var interviewId = event.getSource().get("v.name");

        var SCREEN_DATA = component.get("v.SCREEN_DATA");

        var selectedInterview = null;

        SCREEN_DATA.interviews.forEach(function (row) {
            if (row.Id == interviewId) {
                selectedInterview = row;
            }
        });

        if (selectedInterview) {
            component.set("v.resumeModalTitle", "Resuming: " + selectedInterview.InterviewLabel);
        }

        component.set("v.showResumeModal", true);

        var flow = component.find("flowData");
        flow.resumeFlow(interviewId);
    },

    filter : function(component, event, helper) {       
        helper.filter(component, helper);
    },

    newWarrantyClaim : function(component, event, helper) {
        component.set("v.resumeModalTitle", "New Warranty Claim");

        component.set("v.showResumeModal", true);
        
        var flow = component.find("flowData");
        flow.startFlow("Warranty_Claim_Form_New");
    },

    newStartupClaim : function(component, event, helper) {
        component.set("v.resumeModalTitle", "New Startup Claim");

        component.set("v.showResumeModal", true);
        
        var flow = component.find("flowData");
        flow.startFlow("Machine_Startup");
    },

    closeResume : function(component, event, helper) {
        component.set("v.showResumeModal", false);
        helper.loadScreenData(component, helper);
    },

    handleConfirmDialogYes : function(component, event, helper) {
        helper.delete(component, helper);
        component.set("v.showDeleteModal", false);
    },

    handleConfirmDialogNo : function(component, event, helper) {
        component.set("v.showDeleteModal", false);
    },

    statusChanged : function(component, event, helper) {
        console.log(event.getParam('status'));
        if (event.getParam('status') === "FINISHED" || event.getParam('status') === "WAITING") {
            component.set("v.showResumeModal", false);
            helper.loadScreenData(component, helper);
        }
    }
})