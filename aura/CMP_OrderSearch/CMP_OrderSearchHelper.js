({
    search : function(component, soldTo, orderStatus, poNumber, orderNumber, createdFrom, createdTo) {
        var action = component.get("c.searchSAP");

        action.setParams({
            soldTo: soldTo,
            orderStatus: orderStatus,
            poNumber: poNumber,
            orderNumber: orderNumber,
            createdFrom: createdFrom,
            createdTo: createdTo,
            sortBy: component.get("v.sortBy"),
            sortDirection: component.get("v.sortDirection"),
            pageSize: component.get("v.pageSize"),
            pageNumber: component.get("v.pageNumber")
        });

        action.setCallback(this, function (response) {
            console.log("response="  + response.getState());
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    console.log(data);

                    if (data.orders) {
                        component.set("v.results", data);
                        component.set("v.pageNumber", data.pageNumber);
                        component.set("v.pageSize", data.pageSize);
                        component.set("v.totalRecords", data.totalRecords);
                        component.set("v.totalPages", data.totalPages);
                    }
                    
                    if (!data || data.totalRecords == 0) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "error",
                            "title": "Order Search",
                            "message": "No results found."
                        });
                        toastEvent.fire();                        
                    }
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }
            component.set("v.displaySpinner", false);
        });

        component.set("v.displaySpinner", true);
        $A.enqueueAction(action);
    },

    modifysearch : function(component, helper) {
        var results = component.get("v.results");
        helper.search(component, 
            results.soldTo, 
            results.orderStatus, 
            results.poNumber, 
            results.orderNumber, 
            results.createdFrom, 
            results.createdTo);
    },

    navigateToOrderDetail : function (component, SalesDocument) {
        component.set("v.viewSalesDocument", SalesDocument);
    }
})