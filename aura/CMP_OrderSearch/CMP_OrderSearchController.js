({
    onInit : function(component, event, helper) {
        var action = component.get("c.getScreenData");

        action.setCallback(this, function (response) {
            console.log("response="  + response.getState());
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    console.log(data);

                    if (data.soldToAccounts) {
                        component.set("v.soldToOptions", data.soldToAccounts);
                    }

                    if (data.orderStatuses) {
                        component.set("v.orderStatuses", data.orderStatuses);
                    }
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
                component.set("v.gditoken", null);
            }
            component.set("v.displaySpinner", false);
        });

        component.set("v.displaySpinner", true);
        $A.enqueueAction(action);

        var formFactor = component.get("v.formFactor");
        console.log('formFactor=' + formFactor);

        if (formFactor === "DESKTOP") {
            component.set('v.columns', [
                { label: 'Order Number', type: "button", sortable: false, typeAttributes: {label:  {fieldName: 'SalesDocument'}, name: 'orderDetail', title: 'View Detail', variant: 'base', disabled: false}, initialWidth: 130},
                { label: 'Order Date', fieldName: 'CreateDate', type: 'text', sortable: true, initialWidth: 130 },
                { label: 'Value', fieldName: 'NetOrderValue', type: 'currency', typeAttributes: { currencyCode: {fieldName: 'SalesDocumentCurrency'}},  sortable: true, initialWidth: 100 },
                { label: 'Order Status', fieldName: 'OrderStatus', type: 'text', sortable: false, initialWidth: 120 },
                { label: 'PO Number', fieldName: 'CustomerPONumber', type: 'text', sortable: true, initialWidth: 120 },
                { label: 'Sold To', fieldName: 'SoldToParty', type: 'text', sortable: false, initialWidth: 100 },
                { label: 'Sold To Name', fieldName: 'SoldToName', type: 'text', sortable: false, initialWidth: 200 },
                { label: 'Ship To', fieldName: 'ShipToParty', type: 'text', sortable: false, initialWidth: 120 },
                { label: 'Ship To Name', fieldName: 'ShipToName', type: 'text', sortable: false, initialWidth: 200 },
                { label: 'Ship To City', fieldName: 'ShipToCity', type: 'text', sortable: false, initialWidth: 120 },
                { label: 'Sold To Region', fieldName: 'SoldToRegion', type: 'text', sortable: false, initialWidth: 200 }
            ]);
        } else if (formFactor === "TABLET") {
            component.set('v.columns', [
                { label: 'Order', type: "button", sortable: false, typeAttributes: {label:  {fieldName: 'SalesDocument'}, name: 'orderDetail', title: 'View Detail', variant: 'base', disabled: false}, initialWidth: 90},
                { label: 'Date', fieldName: 'CreateDate', type: 'text', sortable: true, initialWidth: 90 },
                { label: 'Value', fieldName: 'NetOrderValue', type: 'currency', typeAttributes: { currencyCode: {fieldName: 'SalesDocumentCurrency'}},  sortable: true, initialWidth: 100 },
                { label: 'Status', fieldName: 'OrderStatus', type: 'text', sortable: false, initialWidth: 90 },
                { label: 'PO #', fieldName: 'CustomerPONumber', type: 'text', sortable: true, initialWidth: 90 }
            ]);
        } else if (formFactor === "PHONE") {
            component.set('v.columns', [
                { label: 'Order', type: "button", sortable: false, typeAttributes: {label:  {fieldName: 'SalesDocument'}, name: 'orderDetail', title: 'View Detail', variant: 'base', disabled: false}, initialWidth: 90},
                { label: 'Date', fieldName: 'CreateDate', type: 'text', sortable: true, initialWidth: 90 },
                { label: 'Value', fieldName: 'NetOrderValue', type: 'currency', typeAttributes: { currencyCode: {fieldName: 'SalesDocumentCurrency'}},  sortable: true, initialWidth: 100 },
                { label: 'Status', fieldName: 'OrderStatus', type: 'text', sortable: false, initialWidth: 90 }
            ]);
        }
    },

    search : function(component, event, helper) {
        component.set("v.pageNumber", 1);
        component.set("v.sortBy", "CreateDate");
        component.set("v.sortDirection", "desc");

        helper.search(component,
            component.get("v.soldTo"),
            component.get("v.orderStatus"),
            component.get("v.poNumber"),
            component.get("v.orderNumber"),
            component.get("v.createdFrom"),
            component.get("v.createdTo"))
    },

    handlePageSizeChange : function(component, event, helper) {
        component.set("v.pageNumber", 1);
        helper.modifysearch(component, helper);
    },

    handleFirstPress : function(component, event, helper) {
        component.set("v.pageNumber", 1);
        helper.modifysearch(component, helper);
    },

    handlePreviousPress : function(component, event, helper) {
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber - 1);
        helper.modifysearch(component, helper);
    },

    handleNextPress : function(component, event, helper) {
        console.log('in next');
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber + 1);
        helper.modifysearch(component, helper);
    },

    handleLastPress : function(component, event, helper) {
        var totalPages = component.get("v.totalPages");
        component.set("v.pageNumber", totalPages);
        helper.modifysearch(component, helper);
    },

    handleSort : function (component, event, helper) {
        var sortBy = event.getParam("fieldName");
        var sortDirection = event.getParam("sortDirection");

        console.log('sortBy=' + sortBy);
        console.log('sortDirection=' + sortDirection);

        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);

        helper.modifysearch(component, helper);
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'orderDetail':
                helper.navigateToOrderDetail(component, row.SalesDocument);
                break;
        }
    },

    closeModal : function(component, event, helper) {
        component.set("v.viewSalesDocument", null);
    }
})