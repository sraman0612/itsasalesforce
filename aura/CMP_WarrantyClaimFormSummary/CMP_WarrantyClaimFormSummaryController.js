({
    onInit: function (component, event, helper) {
        helper.setupColumns(component);
        component.set("v.allowNext", false);
        helper.processExpenseItems(component);
    },

    handleNavigate: function (component, event, helper) {
        console.log(event.getParam("action") + ' pressed');
        var navigate = component.get("v.navigateFlow");

        var navAction = event.getParam("action");


        if (navAction == 'SUBMIT') {
            var sapServiceNotificationNumber = component.get("v.sapServiceNotificationNumber");

            if (!sapServiceNotificationNumber) {
                helper.createServiceNotification(component, helper);
            } else {
                var sapSalesOrderNumber = component.get("v.sapSalesOrderNumber");
    
                if (!sapSalesOrderNumber) {
                    helper.createSalesOrder(component, helper);
                }
            }
        } else if (navAction == 'NEXT') {
            var sapSalesOrderNumber = component.get("v.sapSalesOrderNumber");

            if (sapSalesOrderNumber) {
                navigate(navAction);
            }
        } else {
            navigate(navAction);
        }
    }
})