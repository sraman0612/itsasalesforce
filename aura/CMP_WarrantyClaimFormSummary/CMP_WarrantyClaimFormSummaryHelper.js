({
  hasErrorMessages: function (component) {
    var messages = component.get('v.messages');

    const found = messages.find(function (row) {
      return row.messageType == 'ERROR';
    });

    return found ? true : false;
  },

  createServiceNotification: function (component, helper) {
    var action = component.get('c.createServiceNotificationImpl');

    var claimText = helper.generateClaimText(component, helper);

    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }

    var formattedDate = year + '-' + month + '-' + day;

    var warrantyClaim = {
      accountId: component.get('v.accountId'),
      serialNumberId: component.get('v.serialNumberId'),
      failedPartNumber: component.get('v.failedPartNo'),
      failedSerialNumber: component.get('v.failedSerialNo'),
      salesOrg: component.get('v.salesOrg'),
      distributionChannel: component.get('v.distributionChannel'),
      dateOfMalfunction: formattedDate,
      serviceDate: component.get('v.serviceDate'),
      claimText: claimText,
      locationNameLine1: component.get('v.locationNameLine1'),
      locationNameLine2: component.get('v.locationNameLine2'),
      locationStreet: component.get('v.locationStreet'),
      locationCity: component.get('v.locationCity'),
      locationPostalCode: component.get('v.locationPostalCode'),
      locationRegion: component.get('v.locationRegion'),
      locationCountry: component.get('v.locationCountry'),
      locationCounty: component.get('v.locationCounty'),
      locationPhone: component.get('v.locationPhone'),
      locationFax: component.get('v.locationFax'),
      partsClaim: component.get('v.partsClaim')
    };

    action.setParams({
      warrantyClaimStr: JSON.stringify(warrantyClaim)
    });

    component.set('v.displaySpinner', true);

    action.setCallback(this, function (response) {
      if (response.getReturnValue()) {
        helper.getServiceNotificationNumber(component, response.getReturnValue().messages);
        helper.mergeMessages(component, response.getReturnValue().messages);
        component.set('v.messages', response.getReturnValue().messages);
        if (response.getReturnValue().data) {
          var notification = response.getReturnValue().data;
          console.log(notification);

          if (!helper.hasErrorMessages(component)) {
            helper.createSalesOrder(component, helper);
          } else {
            component.set('v.displaySpinner', false);
            component.set('v.allowPrevious', true);
            component.set('v.showMessages', true);
          }
        }
      } else {
        var labelReference = $A.get('$Label.c.Error_NoResponse');
        var noResponse = { message: labelReference, messageType: 'ERROR' };
        component.set('v.messages', noResponse);
        component.set('v.displaySpinner', false);
        component.set('v.allowPrevious', true);
        component.set('v.showMessages', true);
      }
    });

    $A.enqueueAction(action);
  },

  processExpenseItems: function (component, helper) {
    var expenseItems = [];

    var laborHours = component.get('v.serviceLaborHoursD');

    if (laborHours) {
      expenseItems.push({
        ItemNumber: '000030',
        Material: 'LABOR',
        OrderQuantity: laborHours
      });
    }

    var travelHours = component.get('v.serviceTravelHoursD');

    if (travelHours) {
      expenseItems.push({
        ItemNumber: '000040',
        Material: 'TRAVEL',
        OrderQuantity: travelHours
      });
    }

    var travelMiles = component.get('v.serviceTravelDistance');

    if (travelMiles) {
      expenseItems.push({
        ItemNumber: '000050',
        Material: 'MILEAGE',
        OrderQuantity: travelMiles
      });
    }

    var otherExpenses = component.get('v.serviceOtherExpenses');

    if (otherExpenses) {
      expenseItems.push({
        ItemNumber: '000060',
        Material: 'EXPENSES',
        OrderQuantity: otherExpenses
      });
    }

    var localMaterial = component.get('v.serviceLocalMaterial');

    if (localMaterial) {
      expenseItems.push({
        ItemNumber: '000070',
        Material: 'LOCAL-MAT',
        OrderQuantity: localMaterial
      });
    }

    var freight = component.get('v.serviceFreight');

    if (freight) {
      expenseItems.push({
        ItemNumber: '000080',
        Material: 'FREIGHT',
        OrderQuantity: freight
      });
    }

    var expenseItemJSON = component.get('v.serviceExpenseItemsJSON');
    console.log(expenseItemJSON);
    if (expenseItemJSON) {
      var inputList = JSON.parse(expenseItemJSON);

      var itemNumber = 100;
      if (inputList && inputList.length > 0) {
        inputList.forEach(function (row) {
          if (row.PartNo) {
            expenseItems.push({
              ItemNumber: '000' + itemNumber,
              Material: row.PartNo,
              OrderQuantity: row.Quantity,
              GDInvoice: row.GDInvoiceNumber,
              GDInvoiceItem: row.GDInvoiceItemNumber
            });

            itemNumber++;
          }
          console.log(row);
        });
      }
    }

    component.set('v.expenseItems', expenseItems);
  },

  createSalesOrder: function (component, helper) {
    component.set('v.allowPrevious', false);
    component.set('v.allowSubmit', false);

    var action = component.get('c.simulateSalesOrder');

    var claimText = helper.generateClaimText(component, helper);

    console.log(claimText);

    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }

    var formattedDate = year + '-' + month + '-' + day;

    var warrantyClaim = {
      accountId: component.get('v.accountId'),
      serialNumber: component.get('v.serialNumber'),
      serialNumberId: component.get('v.serialNumberId'),
      modelNumber: component.get('v.modelNumber'),
      salesOrg: component.get('v.salesOrg'),
      serviceDate: component.get('v.serviceDate'),
      distributionChannel: component.get('v.distributionChannel'),
      sapServiceNotificationNumber: component.get('v.sapServiceNotificationNumber'),
      customerReferenceNumber: component.get('v.serviceCustomerRefNumber'),
      serviceTechnicianNumber: component.get('v.serviceTechnicianNumber'),
      claimReportedBy: component.get('v.claimReportedBy'),
      hoursInService: component.get('v.hoursInService'),
      failedPartNumber: component.get('v.failedPartNo'),
      failedSerialNumber: component.get('v.failedSerialNo'),
      failedHoursInService: component.get('v.failedHoursInService'),
      failedStartupDate: component.get('v.failedStartupDate'),
      laborHours: component.get('v.serviceLaborHoursD'),
      travelHours: component.get('v.serviceTravelHoursD'),
      travelMiles: component.get('v.serviceTravelDistance'),
      dateOfMalfunction: formattedDate,
      freight: component.get('v.serviceFreight'),
      localMaterial: component.get('v.serviceLocalMaterial'),
      otherExpenses: component.get('v.serviceOtherExpenses'),
      replacementPartNumber: component.get('v.replacementPartNo'),
      replacementSerialNumber: component.get('v.replacementSerialNo'),
      replacementGDInvoiceNumber: component.get('v.replacementGDInvoiceNumber'),
      replacementGDInvoiceItemNumber: component.get('v.replacementGDInvoiceItemNumber'),
      claimText: claimText,
      locationNameLine1: component.get('v.locationNameLine1'),
      locationNameLine2: component.get('v.locationNameLine2'),
      locationStreet: component.get('v.locationStreet'),
      locationCity: component.get('v.locationCity'),
      locationPostalCode: component.get('v.locationPostalCode'),
      locationRegion: component.get('v.locationRegion'),
      locationCountry: component.get('v.locationCountry'),
      locationCounty: component.get('v.locationCounty'),
      locationPhone: component.get('v.locationPhone'),
      locationFax: component.get('v.locationFax'),
      partsClaim: component.get('v.partsClaim'),
      itemJSON: component.get('v.serviceExpenseItemsJSON')
    };

    action.setParams({
      warrantyClaimStr: JSON.stringify(warrantyClaim),
      saveToo: true
    });

    action.setCallback(this, function (response) {
      if (response.getReturnValue()) {
        helper.getSalesOrderNumber(component, response.getReturnValue().messages);
        helper.mergeMessages(component, response.getReturnValue().messages);
        if (helper.hasErrorMessages(component)) {
          component.set('v.allowPrevious', true);
          component.set('v.displaySpinner', false);
        } else if (response.getReturnValue().data) {
          var salesOrder = response.getReturnValue().data;
          console.log(salesOrder);

          var sapSalesOrderNumber = component.get('v.sapSalesOrderNumber');

          console.log('sapSalesOrderNumber=' + sapSalesOrderNumber);

          if (sapSalesOrderNumber) {
            var action = component.get('c.updateServiceNotification');

            action.setParams({
              sapSalesOrderNumber: sapSalesOrderNumber
            });

            action.setCallback(this, function (response) {
              if (response.getReturnValue()) {
                helper.mergeMessages(component, response.getReturnValue().messages);
                if (helper.hasErrorMessages(component)) {
                  component.set('v.displaySpinner', false);
                  component.set('v.allowPrevious', true);
                  component.set('v.showMessages', true);
                } else if (response.getReturnValue().data) {
                  var serviceNotificationUpdate = response.getReturnValue().data;

                  console.log(serviceNotificationUpdate);
                  //component.set("v.allowNext", true);
                  var navigate = component.get('v.navigateFlow');
                  navigate('NEXT');
                } else {
                  component.set('v.displaySpinner', false);
                  component.set('v.allowPrevious', true);
                  component.set('v.showMessages', true);
                }
              } else {
                component.set('v.displaySpinner', false);
                component.set('v.allowPrevious', true);
                component.set('v.showMessages', true);
              }
              helper.getSalesOrder(component, helper);
            });

            $A.enqueueAction(action);
          } else {
            component.set('v.displaySpinner', false);
            component.set('v.allowPrevious', true);
            component.set('v.showMessages', true);
          }
        }
      } else {
        var labelReference = $A.get('$Label.c.Error_NoResponse');
        var noResponse = { message: labelReference, messageType: 'ERROR' };
        component.set('v.messages', noResponse);
        component.set('v.displaySpinner', false);
        component.set('v.allowPrevious', true);
        component.set('v.showMessages', true);
      }
    });

    $A.enqueueAction(action);
  },

  getSalesOrder: function (component, helper) {
    var sapSalesOrderNumber = component.get('v.sapSalesOrderNumber');

    if (!sapSalesOrderNumber) {
      return;
    }

    var action = component.get('c.getSalesOrderDetail');

    action.setParams({
      sapSalesOrderNumber: sapSalesOrderNumber
    });

    action.setCallback(this, function (response) {
      if (response.getReturnValue()) {
        if (response.getReturnValue().data) {
          var salesOrder = response.getReturnValue().data;

          console.log(salesOrder.ITEMS.asList);
          component.set('v.sapSalesOrder', salesOrder);

          //setup output json to v.serviceExpenseItemsJSON
          if (salesOrder.ITEMS) {
            console.log(salesOrder.ITEMS.asList);
            var items = new Array();

            salesOrder.ITEMS.asList.forEach(function (row) {
              items.push({
                ItemNo: row.ItemNumber,
                PartNo: row.Material,
                PartDescription: row.ItemDescription,
                Quantity: row.OrderQuantity,
                GDInvoiceNumber: row.GDInvoiceNumber,
                GDInvoiceItemNumber: row.GDInvoiceItemNumber,
                Price: row.NetItemPrice,
                TotalNet: row.NetOrderValue
              });
            });

            console.log(items);
            if (items.length > 0) {
              component.set('v.serviceExpenseItemsJSON', JSON.stringify(items));
            }
          }
        }
        component.set('v.displaySpinner', false);
      } else {
        var labelReference = $A.get('$Label.c.Error_NoResponse');
        var noResponse = { message: labelReference, messageType: 'ERROR' };
        component.set('v.messages', noResponse);
        component.set('v.displaySpinner', false);
      }
    });

    $A.enqueueAction(action);
  },

  getServiceNotificationNumber: function (component, messages) {
    messages.forEach(function (row) {
      if (row.messageType === 'SUCCESS') {
        var serviceNotificationNumber = row.message.substring(21, 30);
        component.set('v.sapServiceNotificationNumber', serviceNotificationNumber);
      }
    });
  },

  getSalesOrderNumber: function (component, messages) {
    messages.forEach(function (row) {
      if (row.messageType === 'SUCCESS' && row.message.startsWith('WarrantyInternet')) {
        var salesOrderNumber = row.message.substring(21, 30);
        component.set('v.sapSalesOrderNumber', salesOrderNumber);
      }
    });
  },

  mergeMessages: function (component, addThese) {
    var messages = component.get('v.messages');

    var sapErrorMessages = component.get('v.sapErrorMessages');
    if (!sapErrorMessages) {
      sapErrorMessages = '';
    }

    addThese.forEach(function (row) {
      console.log(row);
      if (!row.message.startsWith('Job') && !row.message.startsWith('SALES')) {
        if (row.messageType == 'SUCCESS') {
          sapErrorMessages += ' Success: ';
        } else if (row.messageType == 'WARNING') {
          sapErrorMessages += ' Warning: ';
        } else if (row.messageType == 'ERROR') {
          sapErrorMessages += ' ERROR: ';
        } else {
          sapErrorMessages += ' Info: ';
        }

        sapErrorMessages += row.message;

        messages.push(row);
      }
    });

    console.log('sapErrorMessages = ' + sapErrorMessages);
    component.set('v.sapErrorMessages', sapErrorMessages);
    component.set('v.messages', messages);
  },

  generateClaimText: function (component, helper) {
    var claimText = 'The original complaint of the customer?  ' + component.get('v.originalComplaint');
    claimText += '\n';
    claimText += 'Steps taken to troubleshoot complaint?  ' + component.get('v.stepsTroubleshoot');
    claimText += '\n';
    claimText += 'What was determined to be the root cause?  ' + component.get('v.rootCause');
    claimText += '\n';
    claimText += 'Actions taken to bring the unit back to proper operation?  ' + component.get('v.actionsTaken');
    claimText += '\n';
    claimText += 'Is the unit now running satisfactorily?  ' + component.get('v.runningSatisfactorily');
    claimText += '\n';
    claimText += 'Additional Comments:  ' + (component.get('v.additionalComments') || '');
    claimText += '\n';
    claimText += 'Contact Email: ' + component.get('v.emailContact');
    claimText += '\n';

    claimText += component.get('v.sapErrorMessages');

    return claimText;
  },

  setupColumns: function (component) {
    component.set('v.columns', [
      { label: 'Item Number', fieldName: 'ItemNumber', type: 'text', sortable: false, initialWidth: 170 },
      { label: 'Material', fieldName: 'Material', type: 'text', sortable: false, initialWidth: 170 },
      { label: 'Quantity', fieldName: 'OrderQuantity', type: 'integer', sortable: false, initialWidth: 170 },
      { label: 'GD Invoice #', fieldName: 'GDInvoice', type: 'text', sortable: false, initialWidth: 170 },
      { label: 'GD Invoice Item #', fieldName: 'GDInvoiceItem', type: 'text', sortable: false, initialWidth: 170 }
    ]);
  }
});