({
    init : function(component, event, helper) {
        var navigate = component.get("v.navigateFlow");

        navigate("FINISH");
        
        $A.get( "e.force:navigateToSObject" ).setParams( {
            "recordId": component.get( "v.recId" ),
            "slideDevName": "related"
        } ).fire();
    }
})