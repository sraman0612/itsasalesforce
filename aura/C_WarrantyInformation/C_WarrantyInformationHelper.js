({
	getInfo : function(component) {
		var action = component.get("c.getAsset");
        action.setParam("recordId", component.get("v.recordId"));
        action.setCallback(this, function(result){  
            var state = result.getState();
            if(state === "SUCCESS"){
                console.log(result.getReturnValue());
                 component.set("v.SN", result.getReturnValue());
            }
        });
        $A.enqueueAction(action);                                            
	}
})