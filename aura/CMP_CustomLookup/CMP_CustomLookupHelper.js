({
    itemSelected : function(component, event, helper) {
        var target = event.target;   
        var SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");  
        if(SelIndex){
            var serverResult = component.get("v.server_result");
            var selItem = serverResult[SelIndex];
            if(selItem.val){
                component.set("v.selItem",selItem);
                component.set("v.last_ServerResult",serverResult);
                
                component.getEvent("cmpEvent").fire();
            } 
            component.set("v.server_result",null); 
        } 
    }, 
    serverCall : function(component, event, helper) {  
        var target = event.target;  
        var searchText = target.value; 
        var last_SearchText = component.get("v.last_SearchText");
        //Escape button pressed 
        if (event.keyCode == 27 || !searchText.trim()) { 
            helper.clearSelection(component, event, helper);
        }else { 
            var sequenceNumber = component.get("v.sequenceNumber");
            component.set("v.sequenceNumber", sequenceNumber + 1);
            var objectName = component.get("v.objectName");
            var field_API_text = component.get("v.field_API_text");
            var field_API_val = component.get("v.field_API_val");
            var field_API_search = component.get("v.field_API_search");
            var field_API_filter = component.get("v.field_API_filter");
            var field_is_not_null = component.get("v.field_is_not_null");
            var filter_value = component.get("v.filter_value");
            var field2_API_filter = component.get("v.field2_API_filter");
            var field2_is_not_null = component.get("v.field2_is_not_null");
            var filter2_value = component.get("v.filter2_value");
            var limit = component.get("v.limit");
            
            var action = component.get('c.searchDB');
            action.setStorable();
            
            action.setParams({
                objectName : objectName,
                fld_API_Text : field_API_text,
                fld_API_Val : field_API_val,
                fld_API_filter : field_API_filter,
                field_is_not_null : field_is_not_null,
                filter_value : filter_value,
                fld2_API_filter : field2_API_filter,
                field2_is_not_null : field2_is_not_null,
                filter2_value : filter2_value,
                lim : limit, 
                fld_API_Search : field_API_search,
                searchText : searchText
            });
            
            action.setCallback(this,function(a){
                var newSequenceNumber = component.get("v.sequenceNumber");
                
                if (sequenceNumber == newSequenceNumber - 1) {
                    this.handleResponse(a,component,helper);
                }
            });
            
            component.set("v.last_SearchText",searchText.trim());
            console.log('Server call made');
            $A.enqueueAction(action); 
        }         
    },
    handleResponse : function (res,component,helper){
        if (res.getState() === 'SUCCESS') {
            var retObj = JSON.parse(res.getReturnValue());
            if(retObj.length <= 0){
                var noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.server_result",noResult); 
                component.set("v.last_ServerResult",noResult);
            }else{
                component.set("v.server_result",retObj); 
                component.set("v.last_ServerResult",retObj);
            }  
        }else if (res.getState() === 'ERROR'){
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    alert(errors[0].message);
                }
            } 
        }
    },
    getIndexFrmParent : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
            SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    clearSelection: function(component, event, helper){
        component.set("v.selItem",null);
        component.set("v.server_result",null);
        component.getEvent("cmpEvent").fire();
    } 
})