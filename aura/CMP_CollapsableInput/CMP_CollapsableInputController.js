({
    collapse:function(component,event,helper)
    {
        component.set('v.isExpanded',false)
        helper.setExpandIconBasedOnExpanded(component)
    },
    expand:function(component,event,helper)
    {
        component.set('v.isExpanded',true)
        helper.setExpandIconBasedOnExpanded(component)
    },
    handleSwitchExpansionState:function(component,event,helper)
    {
        component.set('v.isExpanded',!component.get('v.isExpanded'))
        helper.setExpandIconBasedOnExpanded(component)
    },
    modelChanged:function(component,event)
    {
        var containerTarget = component.find('container');
        $A.util.addClass(containerTarget, 'active');
        setTimeout(function(){
            $A.util.removeClass(containerTarget, 'active');
        }, 700,containerTarget)
    }
})