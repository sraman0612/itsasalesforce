({
    init : function (component) {
        
        let input = {
            flowName : "Machine_Startup",
            //flowName : "FLOW_NAME_HEADER_OVERRIDE",
            inputVariables : [{
                name:"recordId",
                type:"String",
                value: component.get('v.recordId')
            }]
        }        
        
        let flow = component.find("flowContainer");
        flow.startFlow(input.flowName, input.inputVariables);
    },
    
    statusChange : function (cmp, event) {
        
        
        let status = event.getParam('status');        
        
        if (status in {"FINISHED":"","WAITING":""}) {
            
            var navEvt = $A.get("e.force:navigateToSObject");
            
            navEvt.setParams({
                "recordId": cmp.get('v.recordId'),
                "slideDevName": "details"
            });
            
            navEvt.fire();
            //adding in a hard reload at the end to get around a lightning bug where the data isn't properly refreshing
            window.location.reload();
            
        }
    }
})