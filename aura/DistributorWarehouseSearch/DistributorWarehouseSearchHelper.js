({
    init : function(component, event, helper){
        let action = component.get('c.doInit');
		//console.log(component.get('v.channel'));
        action.setParam('accId', component.get('v.activeAccountId'));
        action.setParam('dcAccountId', component.get('v.dcAccountId'));
        if(component.get('v.acctChange')){
            action.setParam('channel', null);
            component.set('v.channel','');
        }
        else{
        	action.setParam('channel', component.get('v.channel'));
        }
		//component.set('v.myInventoryDCOptions', []);
        action.setCallback(this, function(actionResult) {

            helper.stopLoading(component);

            let state = actionResult.getState();
            console.dir(state);

            if(state === 'SUCCESS'){
                let myInventory = actionResult.getReturnValue().myInventory;

                component.set('v.activeAccountId', actionResult.getReturnValue().activeAccountId);
                component.set('v.selectedAccount', actionResult.getReturnValue().activeAccount);
                component.set('v.dcAccountId', myInventory.dcAccountId);
                component.set('v.dcAccount', myInventory.dcAccount);
                component.set('v.isGlobalUser', actionResult.getReturnValue().isGlobalUser);
                component.set('v.myInventory', myInventory.inventoryItems);
                component.set('v.accountOptions', actionResult.getReturnValue().accountOptions);
                
                if(component.get('v.acctChange')){
                    component.set('v.acctChange', false);
                    component.set('v.myInventoryDCOptions', actionResult.getReturnValue().availableChannels);
                }

                
                if (component.get('v.myInventoryDCOptions').length == 0) {
	                component.set('v.myInventoryDCOptions', actionResult.getReturnValue().availableChannels);
                }else{
                    component.find('selectDC_search_myInv').set('v.value', component.get('v.channel'));
                }
                
                component.set('v.searchDCOptions', actionResult.getReturnValue().availableChannels);
                console.dir(actionResult.getReturnValue());
            }
            else {
                console.dir( JSON.parse(JSON.stringify(actionResult.getError())));
            }
        });
        $A.enqueueAction(action);
    },
    search : function(component, event, helper){

		let action = component.get('c.search');
        let rowsPerPage = component.get('v.search_rowsPerPage');
        let pageNumber = component.get('v.search_pageNumber');
        let model = component.find("model").get("v.value");
        let desc = component.find("desc").get("v.value");
        let power = component.find("power").get("v.value");
        let pressure = component.find("pressure").get("v.value");
        let volt = component.find("volt").get("v.value");
        let channel = component.find("selectDC_search").get("v.value");

        if (!volt || volt < 0) volt = 0;
        if (!pressure || pressure < 0) pressure = 0;
        if (!power || power < 0) power = 0;

        action.setParams({
            "rowsPerPage" : rowsPerPage,
            "pageNumber" : pageNumber,
            "modelNumber" : model,
            "descr" : desc,
            "power" : power,
            "pressure" : pressure,
            "voltage" : volt,
            "channel" : channel
        });

        // Set up the callback
        action.setCallback(this, function(actionResult) {

            helper.stopLoading(component);
            let state = actionResult.getState();

            console.dir(state);

            if(state === 'SUCCESS'){
                component.set('v.searchResults', actionResult.getReturnValue().resultItems);
                component.set('v.search_totalRowCount', actionResult.getReturnValue().totalRowCount);
                component.set('v.search_totalPageCount', actionResult.getReturnValue().totalPageCount);
            }
            else {
                console.dir(JSON.parse(JSON.stringify(actionResult.getError())));
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "An error occurred while performing the search. If the error persists, please contact an administrator",
                    "mode": "sticky",
                    "type": "error"
                });
                toastEvent.fire();
            }

            $A.util.addClass(component.find('loadingSpinner'), "slds-hide");
        });

        $A.enqueueAction(action);
    },
    buildSearchResultColumns : function(component, event, helper){

        let actions = [
            { label: 'View Config', name: 'search_view_config' }
        ];

        component.set('v.searchResultColumns',[
            {
                label : 'Model Number',
                fieldName : 'modelNumber',
                type : 'String'
            },{
                label : 'Serial Number',
                fieldName : 'name',
                type : 'String'
            },{
                label : 'H.P.',
                fieldName : 'horsePower',
                type : 'number'
            },{
                label : 'Voltage',
                fieldName : 'voltage',
                type : 'number'
            },{
                label : 'Pressure',
                fieldName : 'pressure',
                type : 'number'
            },{
                label : 'Enclosure',
                fieldName : 'enclosure',
                type : 'String'
            },{
                label : 'Distributor Name',
                fieldName : 'distName',
                type : 'String'
            },{
                label : 'Distributor City',
                fieldName : 'distCity',
                type : 'String'
            },{
                label : 'Distributor Phone',
                fieldName : 'distPhone',
                type : 'Phone'
            },{
                type: 'action',
                typeAttributes: {
                    rowActions: actions
                }
            }
        ]);
    },
    buildMyInventoryColumns : function(component, event, helper){

        let actions = [
            { label: 'View Config', name: 'my_inventory_view_config' }
        ];

        component.set('v.myInventoryColumns',[
            {
                label : 'Model Number',
                fieldName : 'modelNumber',
                type : 'String'
            },{
                label : 'Serial Number',
                fieldName : 'name',
                type : 'String'
            },{
                label : 'H.P.',
                fieldName : 'horsePower',
                type : 'Decimal'
            },{
                label : 'Ship Date',
                fieldName : 'shipDate',
                type : 'Date'
            },{
                label : 'Points',
                fieldName : 'points',
                type : 'text'
            },{
                label: 'Qualifies',
                fieldName: 'placeHolder',
                type: 'text',
                cellAttributes: {
                    iconName: { fieldName: 'iconName' },
                    iconPosition: 'right'
                }
            },{
                type: 'action',
                typeAttributes: {
                    rowActions: actions
                }
            }
        ]);
    },
    startLoading : function(component){

        $A.util.removeClass(component.find('loadingSpinner'), "slds-hide");
    },
    stopLoading : function(component){

        $A.util.addClass(component.find('loadingSpinner'), "slds-hide");
    },
    remove : function(component, event, helper){

        let serverAction = component.get('c.removeFromDW');

        serverAction.setParam('assetIds', component.find('myInventoryTable').get('v.selectedRows'));

        serverAction.setCallback(this, function(serverResponse){

            let state = serverResponse.getState();
            console.dir(state);

            if(state === 'SUCCESS'){
                this.setResetButtonLabel(component, []);
                component.set('v.myInventory', []);
                helper.init(component, event, helper);
            }
            else {

            }

            helper.stopLoading(component);
        });

        $A.enqueueAction(serverAction);

    },
    resetSearchFields : function(component){

        component.find("model").set('v.value', null);
        component.find("desc").set('v.value', null);
        component.find("power").set('v.value', null);
        component.find("pressure").set('v.value', null);
        component.find("volt").set('v.value', null);
        component.find("enclosure").set('v.value', null);
        component.find("selectDC_search").set('v.value', null);
    },
    setResetButtonLabel : function(component, selectedRows){

        if(selectedRows.length > 0){
            component.set('v.removeButtonDisabled', false);
            component.find('removeButton').set('v.label', 'Remove('+ selectedRows.length +')');
        }
        else{
            component.set('v.removeButtonDisabled', true);
            component.find('removeButton').set('v.label', 'Remove');
            component.find('myInventoryTable').set('v.selectedRows',selectedRows);
        }
    },
    viewConfig : function(component, event, helper){

        let row = event.getParam('row');
        console.dir(JSON.parse(JSON.stringify(row)));

        let modalBody;

        $A.createComponent("c:SerialNumberConfiguration", {"aura:id":'config','recordId': row.Id},
            function(content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('overlayLib').showCustomModal({
                        header: "Configuration",
                        body: modalBody,
                        showCloseButton: true,
                        //cssClass: "mymodal",
                        closeCallback: function() {
                            //alert('You closed the alert!');
                        }
                    })
                }
            }
        );
    },
    showAccountSwap : function(component, event, helper){

        let modalBody;

        $A.createComponent("c:GlobalAccountToggle",
            {
                "aura:id":'accOptions',
                'options': component.get('v.accountOptions'),
                'selectedValue': component.get('v.activeAccountId'),
                'actionableValue': component.getReference('v.activeAccountId'),
                'initialValue': component.get('v.activeAccountId')
            },
            function(content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('overlayLib').showCustomModal({
                        header: "Change Account",
                        body: modalBody,
                        showCloseButton: true,
                        closeCallback: function() {
                            helper.startLoading(component);
                            helper.init(component, event, helper);
                        }
                    })
                }
            }
        );



//        $A.createComponent("lightning:radioGroup",
//            {
//                "aura:id":'accOptions',
//                'name': 'accOptions',
//                'label': 'Accounts',
//                'options': component.get('v.accountOptions'),
//                'value': component.getReference('v.activeAccountId'),
//                'type': 'radio'
//            },
//            function(content, status) {
//                if (status === "SUCCESS") {
//                    modalBody = content;
//                    component.find('overlayLib').showCustomModal({
//                        header: "Change Account",
//                        body: modalBody,
//                        showCloseButton: true,
//                        closeCallback: function() {
//                            helper.startLoading(component);
//                            helper.init(component, event, helper);
//                        }
//                    })
//                }
//            }
//        );
    }
});