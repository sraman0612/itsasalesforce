({
    handleInit : function(component, event, helper) {
        console.log('in init');
        helper.startLoading(component);
        helper.buildSearchResultColumns(component, event, helper);
        helper.buildMyInventoryColumns(component, event, helper);
        helper.init(component, event, helper);
    },
	handleClick : function(component, event, helper) {

        // when the search button is clicked
        if(event.getSource().getLocalId() == 'searchButton'){

            helper.startLoading(component);
            helper.search(component, event, helper);
        }

        // when the remove button is clicked
        else if(event.getSource().getLocalId() == 'removeButton'){

            if(confirm('Are you sure?')){
                helper.startLoading(component);
                helper.remove(component, event, helper);
            }
        }

        // when the reset button is clicked
        else if(event.getSource().getLocalId() == 'resetButton'){

            helper.resetSearchFields(component);
        }
	},
	handleInventorySelection : function(component, event, helper){

        let selectedRows = event.getParam('selectedRows');
        helper.setResetButtonLabel(component, selectedRows);
    },
    handleRowAction : function(component, event, helper){

        var action = event.getParam('action');

        switch (action.name) {
            case 'my_inventory_view_config':
                helper.viewConfig(component, event, helper);
                break;
            case 'search_view_config':
                helper.viewConfig(component, event, helper);
                break;
        }
    },
    handleSelect : function(component, event, helper){

        let target = event.getParam('value');

        if(target === 'swap_account'){
            helper.showAccountSwap(component, event, helper);
        }
    },
    handleRowsPerPageChange : function(component, event, helper){

        helper.startLoading(component);
        component.set('v.search_pageNumber', 0);
        helper.search(component, event, helper);
    },
    handleAccountSwap : function(component, event, helper){
        if(event.getParam('value') != event.getParam('oldValue')){
            component.set('v.acctChange', true);
            helper.startLoading(component);
            helper.init(component, event, helper);
        }
    },
    prev: function(component, event, helper){

        let pageNumber = component.get('v.search_pageNumber');

        if(pageNumber > 0){
            helper.startLoading(component);
            pageNumber -= 1;
            component.set('v.search_pageNumber', pageNumber);
            helper.search(component, event, helper);
        }
    },
    next: function(component, event, helper){

        let pageNumber = component.get('v.search_pageNumber');
        let numberOfPages = component.get('v.search_totalPageCount');

        if( (pageNumber + 1) < numberOfPages){
            helper.startLoading(component);
            pageNumber += 1;
            component.set('v.search_pageNumber', pageNumber);
            helper.search(component, event, helper);
        }
    }
})