({
	getInfo : function(component) {
		var action = component.get("c.queryAsset");
        action.setParam("assetId", component.get("v.recordId"));
        action.setCallback(this, function(result){  
            var state = result.getState();
            if(state === "SUCCESS"){
                console.log(result.getReturnValue());
                 component.set("v.SN", result.getReturnValue());
            }
        });
        $A.enqueueAction(action);                                            
	},
    getHistory : function(component) {
		var action = component.get("c.queryServiceHistory");
        action.setParam("assetId", component.get("v.recordId"));
        action.setCallback(this, function(result){  
            var state = result.getState();
            if(state === "SUCCESS"){
                console.log(result.getReturnValue());
                 component.set("v.ServiceHistory", result.getReturnValue());
            }
        });
        $A.enqueueAction(action);                                            
	}
})