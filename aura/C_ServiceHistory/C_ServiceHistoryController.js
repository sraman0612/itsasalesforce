({
	doInit : function(component, event, helper) {
		helper.getInfo(component);
        helper.getHistory(component);
	}
})