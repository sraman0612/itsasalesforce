({
    doInit : function(component, event, helper) {
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getContact');

        // params to the method are pass as an object with property names matching
        var params = {
            "contactId" : component.get("v.recordId")
        };

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.contact", payload);
                console.log('Contact Id ===> '+component.get("v.contact.Id"));
                console.log('Contact Account Id ===> '+component.get("v.contact.AccountId"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.getUserData(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },
    getUserData :function(component, event, helper){
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getUserData');

        // params to the method are pass as an object with property names matching
        var params = {};

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.currUser", payload);
                console.log('currUser ===> '+component.get("v.currUser.Id"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.createDRTcase(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log('Error in getUserData '+JSON.stringify(payload));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },
    createDRTcase: function(component, event, helper) {
        //prepare code to create the calendar event
        var contact = component.get("v.contact");
        var currUser = component.get("v.currUser");
        var contactId = component.get("v.recordId");

        component.set("v.usrBrand", currUser.Brand__c);
        component.set("v.usrDepartment", currUser.Department__c);
        component.set("v.usrProductCategory", currUser.Default_Product_Category__c);

        var createRecordCase = $A.get("e.force:createRecord");

        createRecordCase.setParams({
            "entityApiName": "Case",
            "defaultFieldValues": {
                'ContactId': contactId,
                'AccountId': contact.AccountId,
                'Brand__c': component.set("v.usrBrand"),
                'GDI_Department__c': currUser.Department__c,
                'Product_Category__c': currUser.Default_Product_Category__c
            }
        });
        createRecordCase.fire();
    },
    cancelNewCase: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})