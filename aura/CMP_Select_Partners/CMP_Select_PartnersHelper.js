({
    getSavedPartnerValuesFromJSON: function(component)
    {
        var msgJSON = component.get('v.headerJSON') && JSON.parse(component.get('v.headerJSON'));
        if(msgJSON)
        {
            var partnerNumber = msgJSON[component.get('v.partnerNumber')];
            var partnerField = msgJSON[component.get('v.partnerField')];
            component.set('v.partnerNumber', partnerNumber);
            component.set('v.partnerField', partnerField);

            if (partnerNumber && partnerNumber.length && partnerField && partnerField.length)
            {
                component.set('v.isSelected', true);
            }
        }
    },

    savePartnerValuesToJSON: function(component)
    {
        var newJSON = {};
        if(component.get('v.headerJSON'))
        {
            newJSON = JSON.parse(component.get('v.headerJSON'));
        }

        newJSON.partnerNumber = component.get('v.partnerListSelected.PartnerNumber');
        newJSON.partnerField = component.get('v.partnerField');
        component.set('v.headerJSON', JSON.stringify(newJSON));
    },
})