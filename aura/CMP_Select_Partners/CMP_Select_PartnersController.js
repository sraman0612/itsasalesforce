({
    onInit: function(component,event,helper)
    {
        var cpqQuoteId = component.get('v.quoteId');
        var partnerFunction = component.get('v.partnerFunction');
        var msg = JSON.parse(component.get('v.cpqMsg'));
        var customerNumber = msg.readOnly.accountSoldTo;
        var salesOrg = component.get('v.salesOrg');
        var salesDistribution = component.get('v.salesDistribution');
        var salesDivision = component.get('v.salesDivision');
        var data = msg.data;
        var partnerFunction = component.get('v.partnerFunction');
        var isFinish = component.get('v.isFinish');
 
        
        if (isFinish) {
            component.set('v.lblFlowNavigate', $A.get("$Label.c.CPQ_Quote_SelectPartner_YesBtnFinish"));
        } else {
            component.set('v.lblFlowNavigate', $A.get("$Label.c.CPQ_Quote_SelectPartner_YesBtnNext"));
        }

        switch (partnerFunction) {
            case "SH":
                component.set('v.lblHeader', $A.get("$Label.c.CPQ_Quote_ShipTo"));
                break;
            case "BP":
                component.set('v.lblHeader', $A.get("$Label.c.CPQ_Quote_BillToParty"));
                break;
            case "SP":
                component.set('v.lblHeader', $A.get("$Label.c.CPQ_Quote_SoldToParty"));
                break;
            default: 
                component.set('v.lblHeader', 'NA');
        }

        component.set('v.customerNumber', customerNumber);

        component.set('v.columns', [
            {label: 'Partner Name', fieldName: 'PartnerName', type: 'text', sortable: false},
            {label: 'Partner Number', fieldName: 'PartnerNumber', type: 'text', sortable: false},
            {label: 'Contact First Name', fieldName: 'ContactFirstName', type: 'text', sortable: false},
            {label: 'Contact Last Name', fieldName: 'ContactLastName', type: 'text', sortable: false},
            {label: 'House Number', fieldName: 'HouseNumber', type: 'text', sortable: false},
            {label: 'Street', fieldName: 'Street', type: 'text', sortable: false},
            {label: 'City', fieldName: 'City', type: 'text', sortable: false},
            {label: 'Region', fieldName: 'Region', type: 'text', sortable: false},     
            {label: 'Postal Code', fieldName: 'PostalCode', type: 'text', sortable: false},  
            {label: 'Country', fieldName: 'Country', type: 'text', sortable: false}
        ]);

        if (!data.header) //first, we see if the header is already populated
        {
            component.set('v.headerJSON', data.header);
        }

        var action = component.get('c.getPartnerList');
        action.setCallback(this, function (fdata) {
            if (fdata.getReturnValue())
            {
                var state = fdata.getState();
                if(state === 'SUCCESS')
                {

                    var partnerData = fdata.getReturnValue().data;
                    if (partnerData != null)
                    {
                        component.set('v.partnerList', partnerData);
                        component.set('v.messages', fdata.getReturnValue().messages);
                        component.superRerender();
                        component.set('v.displaySpinner', false);
                    }            
                    else //nothing returned from Apex call`
                    {
                        var labelReference = $A.get("$Label.c.Error_NoResponse");
                        var noResponse = {message: labelReference, messageType: "ERROR"};
                        component.set('v.messages', noResponse);
                    }
                }
            }

        })
        action.setParams({customerNumber: customerNumber, partnerFunction: partnerFunction, salesOrg: salesOrg, salesDistribution: salesDistribution,  salesDivision: salesDivision});
        $A.enqueueAction(action);
    },


    handleRowSelection: function (component, event, helper) {

        var selectedRows = event.getParam('selectedRows');
        if(selectedRows.length>0)
        {
            component.set('v.partnerListSelected', selectedRows[0]);
            component.set('v.isSelected', true);
        }
    },

    clearPartnerValues:  function(component, event, helper)
    {
        // PartnerNumber, PartnerName, ContactFirstName, ContactLastName, Country, Region, HouseNumber, Street, City, PostalCode (inside partnerListSelected) 
        component.set('v.partnerListSelected', null);
        component.set('v.isSelected', false);
    },

    savePartnerValues: function(component, event, helper)
    {
        helper.savePartnerValuesToJSON(component);
    },

    flowNavigate: function(component, event, helper)
    {     
        helper.savePartnerValuesToJSON(component);

        var isFinish = component.get('v.isFinish');
        var navigate = component.get("v.navigateFlow");
        
        if(isFinish)
        {
            navigate("FINISH");
        }else
        {
            navigate("NEXT");
        }
    },
})