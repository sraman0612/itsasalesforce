({
    doInit : function(component, event, helper) {
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getCase');

        // params to the method are pass as an object with property names matching
        var params = {
            "caseId" : component.get("v.recordId")
        };

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.case", payload);
                console.log('case Id ===> '+component.get("v.case.Id"));
                console.log('case AccountId ===> '+component.get("v.case.ContactId"));
                console.log('case Active_Service_Center__c ===> '+component.get("v.case.Active_Service_Center__c"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.createNewSiteVisit(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },
    createNewSiteVisit: function(component, event, helper) {
        //prepare url to create the site visit
        var caseId = component.get("v.recordId");
        var currCase = component.get("v.case");

        var createRecordEvent = $A.get("e.force:createRecord");

        createRecordEvent.setParams({
            "entityApiName": "Site_Visit__c",
            "defaultFieldValues": {
                'Customer__c' : currCase.AccountId,
                'Case__c': caseId,
                'Service_Center__c': currCase.Active_Service_Center__c
            }
        });
        createRecordEvent.fire();
    },
    cancelNewSiteVisit: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})