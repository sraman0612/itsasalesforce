({

    getSavedSalesAreaValuesFromJSON: function(component)
    {
        var msgJSON = component.get('v.headerJSON') && JSON.parse(component.get('v.headerJSON'));
        if(msgJSON)
        {
            component.set('v.salesOrg', msgJSON.salesOrg);
            component.set('v.salesDistribution', msgJSON.distChannel);
            component.set('v.salesDivision', msgJSON.division);

            if (msgJSON.salesOrg && msgJSON.salesOrg.length &&
                msgJSON.distChannel && msgJSON.distChannel.length &&
                msgJSON.division && msgJSON.division.length)
            {
                component.set('v.isSelected', true);
            }
        }
    },

    saveSalesAreaValuesToJSON: function(component)
    {
        var headerJSON;
        var newJSON;

        if(!component.get('v.headerJSON'))
        {
            newJSON = 
            {
                'salesOrg': component.get('v.salesOrg'),
                'distChannel': component.get('v.salesDistribution'),
                'division': component.get('v.salesDivision'),
                'soldToParty': component.get('v.customerNumber'),
                'shipToParty': component.get('v.defaultShipTo')
            };
        } 
        else
        {
            newJSON = JSON.parse(component.get('v.headerJSON'));
            newJSON.salesOrg = component.get('v.salesOrg');
            newJSON.distChannel = component.get('v.salesDistribution');
            newJSON.division = component.get('v.salesDivision');
            newJSON.shipToParty = component.get('v.defaultShipTo')

        }

        component.set('v.headerJSON', JSON.stringify(newJSON));

    },


})