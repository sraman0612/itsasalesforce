({
    onInit: function(component,event,helper)
    {
        var cpqQuoteId = component.get('v.quoteId');
        var msg = JSON.parse(component.get('v.cpqMsg'));
        var data = msg.data;
        var customerNumber = msg.readOnly.accountSoldTo;
        var productMaterial = msg.readOnly.productMaterial;


        component.set('v.customerNumber', customerNumber);
        component.set('v.columns', [                
            {label: $A.get("$Label.c.CustomerSalesAreas_SalesOrg"), fieldName: 'SalesOrganizationDisplay', type: 'text', sortable: false},
            {label: $A.get("$Label.c.CustomerSalesAreas_DistributionChannel"), fieldName: 'DistributionChannelDisplay', type: 'text', sortable:false},
            {label: $A.get("$Label.c.CustomerSalesAreas_Division"), fieldName: 'DivisionDisplay', type: 'text', sortable:false}
        ]);

        if (data.header != '') //first, we see if the header is already populated
        {
            component.set('v.headerJSON',data.header);
            helper.getSavedSalesAreaValuesFromJSON(component);
        } else
        {
            //check to see if defaults have been set
            var salesOrg = component.get('v.salesOrg');
            var salesDistribution = component.get('v.salesDistribution');
            var salesDivision = component.get('v.salesDivision');

            if (salesOrg && salesOrg.length &&
                salesDistribution && salesDistribution.length &&
                salesDivision && salesDivision.length)
            {
                component.set('v.isSelected', true);
            }
        }

        var action = component.get('c.getSalesAreaList')
        action.setCallback(this, function (data) {
            var state = data.getState();
            if(state === 'SUCCESS')
            {
                helper.getSavedSalesAreaValuesFromJSON(component);
                var customer = data.getReturnValue().data;
                component.set('v.Customer', customer);
                var i, len, sa, salesAreaList = component.get('v.Customer.SALES_DATA.asList');
                for (len = salesAreaList.length, i=0; i<len; ++i)
                {
                    sa = salesAreaList[i];
                    sa.keyField = sa.SalesOrganization + '|' + sa.DistributionChannel + '|' + sa.Division;
                    sa.SalesOrganizationDisplay = sa.SalesOrganization + ' - ' + sa.SalesOrganizationName;
                    sa.DistributionChannelDisplay = sa.DistributionChannel + ' - ' + sa.DistributionChannelName;
                    sa.DivisionDisplay = sa.Division + ' - ' + sa.DivisionName;
                }
                component.set('v.salesAreaList', salesAreaList);
                component.set('v.messages', data.getReturnValue().messages);
                component.superRerender();
                component.set('v.displaySpinner', false);
            }
        });
        action.setParams({customerNumber: customerNumber});
        $A.enqueueAction(action);
    },


    handleRowSelection: function (component, event, helper) {

        var selectedRows = event.getParam('selectedRows');
        if(selectedRows.length>0)
        {
            component.set('v.salesOrg',selectedRows[0].SalesOrganization);
            component.set('v.salesDistribution', selectedRows[0].DistributionChannel);
            component.set('v.salesDivision', selectedRows[0].Division);
            component.set('v.isSelected', true );
        }
    },

    clearSalesAreaValues:  function(component, event, helper)
    {
        component.set('v.salesOrg', null);
        component.set('v.salesDistribution', null);
        component.set('v.salesDivision', null);
        component.set('v.isSelected', false);
    },

    saveSalesAreaValues: function(component,event,helper)
    {
        helper.saveSalesAreaValuesToJSON(component);
    },

    flowNavigate: function(component, event, helper)
    {   
        helper.saveSalesAreaValuesToJSON(component);

        var isFinish = component.get('v.isFinish');
        var navigate = component.get("v.navigateFlow");
        
        if(isFinish)
        {
            navigate("FINISH");
        }else
        {
            navigate("NEXT");
        }
    },

})