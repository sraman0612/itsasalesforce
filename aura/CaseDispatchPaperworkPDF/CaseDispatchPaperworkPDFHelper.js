({
    doInit : function(component, event, helper) {
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');
        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getCase');
        // params to the method are pass as an object with property names matching
        var params = {
            "caseId" : component.get("v.recordId")
        };
        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.case", payload);
                console.log('case Id ===> '+component.get("v.case.Id"));
                console.log('case Status ===> '+component.get("v.case.Status"));
                console.log('case Re_Opened__c ===> '+component.get("v.case.Re_Opened__c"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.updateCaseStatus(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },

    updateCaseStatus : function(component, event, helper) {
        //prepare action for update case
        var updateCall = component.get("c.updateCase");
        updateCall.setParams({
            "currCase" : component.get("v.case")
        });
        //configure response handler for this action
        updateCall.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log('Update Successful, state ===> '+ state);
                /*
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Case Updated",
                    "message" : "The case has been assigned.",
                    "type" : "success"
                });
                //Update the UI: closePanel, show toast, refresh page
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                */
            }else if(state === "ERROR"){
                var errors = response.getError();
                console.log('errors ===> '+JSON.stringify(errors));
                console.log('Problem updating case, response state = '+state);
            }else{
                console.log('Unknown problem: '+state);
            }
        });
        //send the request to updateCall
        $A.enqueueAction(updateCall);
        helper.dispatchPDF(component, event, helper);
    },

    dispatchPDF : function(component, event, helper) {
        var caseId = component.get("v.recordId");
        var url = '/apex/SDOC__SDCreate1?id='+caseId+'&Object=Case';
        console.log(caseId);
        console.log(url);
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.openSubtab({
                parentTabId: focusedTabId,
                url:  url,
                focus: true
            });
        })
            .catch(function(error) {
                console.log(error);
            });
    },

    cancelPDF: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})