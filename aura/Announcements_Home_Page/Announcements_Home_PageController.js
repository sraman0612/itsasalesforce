({
	doInit : function(component, event, helper) {

        const fakeData = [
            { Name: 'Cake Day!', Posted_Date__c: new Date('2021-07-21'),
             link: 'google.com/1'
            },
            { Name: 'Chris\'s birthday isn\'t today', Posted_Date__c: new Date('2021-07-20'),
             link: 'google.com/2'
            },
            { Name: 'Marvel\'s Black Widow Released' , Posted_Date__c: new Date('2021-07-09'),
             link: 'google.com/3'
            }
        ];
        
		component.set('v.columns', [
            { label: 'Subject', fieldName: 'link', type: 'url', sortable: false, hideDefaultActions: true,
             typeAttributes: { label: { fieldName: 'Subject__c' }}
            },
            { label: 'Posted', fieldName: 'Posted_Date__c', type: 'date', sortable: false, hideDefaultActions: true, initialWidth: 110 }
        ]);
        
		const action = component.get('c.getAnnouncements');
        action.setCallback(this, function(response) {
            console.log('Callback Response: ' + JSON.stringify(response.getReturnValue()));
            if (response.getState() === "SUCCESS") {
                const data = response.getReturnValue();
                
                data.forEach(d => d.link = '/announcements/' + d.Id);
                
                component.set('v.data', data);
                //component.set('v.data', fakeData);
            }
        });
        $A.enqueueAction(action);
	}
})