({
	getInfo : function(component) {
		var action = component.get("c.queryOilSample");
        action.setParam("assetId", component.get("v.recordId"));
        action.setCallback(this, function(result){  
            var state = result.getState();
            if(state === "SUCCESS"){
                console.log(result.getReturnValue());
                 component.set("v.LubricantSample", result.getReturnValue());
            }
        });
        $A.enqueueAction(action);                                            
	}
})