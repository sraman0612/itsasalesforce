({
	doInit : function(component, event, helper) {
		var action = component.get("c.getAsset");
        var recordId = component.get("v.recordId");
        console.log(recordId);
        action.setParams({
            recordId : component.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.serialNumber", response.getReturnValue());
                console.log(response.getReturnValue());
            }
         });
         $A.enqueueAction(action); 
        
        
	}
})