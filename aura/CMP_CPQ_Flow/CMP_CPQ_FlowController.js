({
    init : function (component) {
        var flow = component.find("cpqFlow");
        var flowVariables = [
            {name : 'customerNumber', type: 'String' , value: component.get('v.customerNumber')},
            {name : 'quoteId', type:'String', value: component.get('v.quoteId')},
            {name : 'itemId', type:'String', value: component.get('v.itemId')},
            {name : 'itemJSON', type:'String', value: component.get('v.itemJSON')},
            {name : 'headerJSON', type:'String', value: component.get('v.headerJSON')},
            {name : 'isItem', type:'Boolean', value: component.get('v.isItem')},
            {name : 'isHeader', type:'Boolean', value: component.get('v.isHeader')},
            {name : 'cpqMsg', type:'String', value: component.get('v.cpqMsg')},
            {name : 'addedItemsJSON', type:'String', value: component.get('v.addedItemsJSON')}
        ]

        // In that component, start your flow. Reference the flow's Unique Name.
        flow.startFlow("cpqVCFlow", flowVariables);
    },

    handleStatusChange: function(component, event)
    {
        var status = event.getParam('status');
        var outputVariables = event.getParam('outputVariables');

        if(status === 'FINISHED')
        {
            //loop through outputVariables to get values to set to component attributes
            for(var i = 0; i < outputVariables.length; i++) 
            {
                var outputVar = outputVariables[i];

                if(outputVar.name == 'itemJSON')
                {
                    component.set('v.itemJSON', outputVar);
                }
                if(outputVar.name == 'headerJSON')
                {
                    component.set('v.headerJSON', outputVar);
                }
                if(outputVar.name == 'cpqMsg')
                {
                    component.set('v.cpqMsg', outputVar);
                }
                if(outputVar.name == 'addedItemsJSON')
                {
                    component.set('v.addedItemsJSON', outputVar);
                }
            }

            ///write back to VFP, close flow window
            var cpqMsg = JSON.parse(component.get('v.cpqMsg').value);
            var headerJSON = JSON.parse(component.get('v.headerJSON').value);
            var itemJSON = JSON.parse(component.get('v.itemJSON').value);
            var addedItems = JSON.parse(component.get('v.addedItemsJSON').value);

            var newCpqMsg = cpqMsg;
            newCpqMsg.data.header = headerJSON;
            newCpqMsg.data.item = itemJSON;
            newCpqMsg.data = Object.assign({'addedItems': addedItems}, newCpqMsg.data);

            var msgString = JSON.stringify(newCpqMsg);
            component.set('v.cpgMsg', msgString);

            var quoteId = component.get('v.quoteId');

            var finishEVT = $A.get("e.c:EVT_CPQ_Flow_Finished");
            finishEVT.setParam("quoteId", quoteId);
            finishEVT.setParam('cpqMsg', msgString);
            finishEVT.fire();
            component.destroy();
        }        
    }
})