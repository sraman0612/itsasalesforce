({
	onInit : function(component, event, helper) {
        component.set("v.columns", [
                                { label: 'Message', fieldName: 'message', type: 'text', sortable: false, initialWidth: 760 },
                                { label: 'Step', fieldName: 'step', type: 'text', sortable: false, initialWidth: 120 }
        ]);
            
		helper.loadStatus(component, event, helper);
	},
    
    refresh : function(component, event, helper) {
		helper.loadStatus(component, event, helper);
    }
})