({
    getScreenData: function(component, helper) {
        var recordId = component.get("v.recordId");
        component.set("v.displaySpinner", true);

        var action = component.get("c.getScreenData");

        action.setParams({
            quoteId: recordId
        });

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var data = JSON.parse(response.getReturnValue().data);
                    console.log('getting screen data');
                    console.log(data);
                    if (data.templateIdMap && data.templateIdMap.length > 0) {
                        console.log('data.templateIdMap[0].value=' + data.templateIdMap[0].value);
                        component.set("v.templateId", data.templateIdMap[0].value);
                    }
                    component.set("v.screendata", data);

                    component.set("v.documentName", data.quoteDocumentName);

                    //component.set("v.showPreview", true);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
            component.set("v.displaySpinner", false);
        });
        $A.enqueueAction(action);
    },

    generate: function (component, helper) {
        var recordId = component.get("v.recordId");
        var templateId = component.get("v.templateId");
        var paperSize = component.get("v.paperSize");
        var outputFormat = component.get("v.outputFormat");
        var language = component.get("v.language");
        var documentName = component.get("v.documentName");

        var action = component.get("c.generateQuoteDocument");

        action.setParams({
            quoteDocumentName: documentName, 
            paperSize: paperSize, 
            quoteId: recordId, 
            templateId: templateId, 
            outputFormat: outputFormat, 
            language: language
        });

        component.set("v.displaySpinner", true);

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var interval = setInterval($A.getCallback(function () {
                    var jobStatus = component.get("c.getJobStatus");
                    jobStatus.setParams({ jobId : response.getReturnValue().data});
                    jobStatus.setCallback(this, function(jobStatusResponse){
                        var state = jobStatus.getState();
                        if (state === "SUCCESS"){
                            var status = jobStatusResponse.getReturnValue().data;
                            console.log('status=' + status);
                            if (status === "Completed") {
                                var toastEvent = $A.get("e.force:showToast");
        
                                toastEvent.setParams({
                                    "title": "Success!",
                                    "type" : "success",
                                    "message": "The quote document was generated successfully."
                                });
        
                                toastEvent.fire();
        
                                var showPreview = component.get("v.showPreview");
        
                                if (showPreview) {
                                    helper.loadPreview(component, helper);
                                }
        
                                $A.get('e.force:refreshView').fire();
                                component.set("v.displaySpinner", false);
                                window.clearInterval(interval);
                            } else if (status == "Failed") {
                                var toastEvent = $A.get("e.force:showToast");
        
                                toastEvent.setParams({
                                    "title": "Error!",
                                    "message": "The Document has failed to generate."
                                });
        
                                toastEvent.fire();
                                component.set("v.displaySpinner", false);
                                window.clearInterval(interval);
                            }
                        }
                    });

                    $A.enqueueAction(jobStatus);
                }), 2000);
            } else if (state === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error!",
                    "message": "An Error has occured. Unable to Generate Document."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },

    pollQuoteDocument: function (jobId, component, helper) {
        console.log("inside pollQuoteDocument " + jobId);
        var action = component.get("c.getJobStatus");

        action.setParams({
            jobId: jobId
        });

        action.setCallback(this, function (response) {
            helper.clearClickInterval(component,helper);
            if (response.getReturnValue()) {
                if (response.getReturnValue().data) {
                    var status = response.getReturnValue().data;
                    console.log('status=' + status);
                    if (status === "Completed") {
                        var toastEvent = $A.get("e.force:showToast");

                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "The quote document was generated successfully."
                        });

                        toastEvent.fire();

                        var showPreview = component.get("v.showPreview");

                        if (showPreview) {
                            helper.loadPreview(component, helper);
                        }

                        $A.get('e.force:refreshView').fire();
                        component.set("v.displaySpinner", false);
                    } else if (status == "Failed") {
                        var toastEvent = $A.get("e.force:showToast");

                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "The quote document has failed to generate."
                        });

                        toastEvent.fire();
                        component.set("v.displaySpinner", false);
                    }
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
        });
        console.log('we are enqueuing');
        $A.enqueueAction(action);
        console.log('after enqueuing');
    },

    loadPreview: function(component, helper) {
        var recordId = component.get("v.recordId");
        var documentName = component.get("v.documentName");

        var action = component.get("c.getQuoteDocument");

        action.setParams({
            quoteDocumentName: documentName, 
            quoteId: recordId
        });

        action.setCallback(this, function (response) {
            if (response.getReturnValue()) {
                var docData = response.getReturnValue().data;

                if (docData) {
                    console.log(docData);
                    component.set("v.previewData", docData);
                    component.set("v.previewTitle", "Preview: " + documentName + '.PDF');
                    component.set("v.isOpen", true);
                }
                component.set("v.messages", response.getReturnValue().messages);
            } else {
                var labelReference = $A.get("$Label.c.Error_NoResponse");
                var noResponse = { message: labelReference, messageType: "ERROR" };
                component.set("v.messages", noResponse);
            }
        });
        $A.enqueueAction(action);
    }
})