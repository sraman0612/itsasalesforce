({
  onInit: function (component, event, helper) {
    helper.getScreenData(component, helper);
  },

  generate: function (component, event, helper) {
    helper.generate(component, helper);
  },
  
  closeModal: function(component, event, helper) {
    component.set("v.isOpen", false);
  },

  outputFormatChanged: function(component, event, helper) {
    component.set("v.showPreview", false);
  }
})