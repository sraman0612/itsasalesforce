({
    buildColumns: function(component, isKit) {
        var columns = [];

        if (isKit) {
            columns.push({fieldName: 'bomUsage', type: 'button', typeAttributes: {name:'Explode', label: $A.get('$Label.c.Enosix_SalesDoc_Table_KitExplode')},
                cellAttributes: {class: {fieldName: 'bomUsageCssClass'}}});
            columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_KitName'), fieldName: 'material', type: 'Boolean'});
            columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_KitDescription'), fieldName: 'materialDescription', type: 'text'});
            columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_KitStock'), fieldName: 'stock', type: 'text'});
            columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_KitAvailable'), fieldName: 'available', type: 'decimal'});
        }
        else {
            columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_Material'), fieldName: 'material', type: 'text'});
            columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_MaterialType'), fieldName: 'materialType', type: 'text'});
            columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_ItemDescription'), fieldName: 'materialDescription', type: 'text'});
            columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_Category'), fieldName: 'productHierarchyField', type: 'text'});
            if (component.get('v.isQuantityEnabled')) {
                columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_Quantity'), fieldName: 'quantity', type: 'number', editable: true, typeAttributes: { maximumFractionDigits: '3' }});
                columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_UnitOfMeasure'), fieldName: 'unitOfMeasure', type: 'text'});
            }
            if (component.get('v.isScheduleDateEnabled')) {
                columns.push({label: $A.get('$Label.c.Enosix_SalesDoc_Table_RequestedDate'), fieldName: 'scheduleDate', type: 'date-local', editable: true});
            }
        }

        component.set('v.columns', columns);
    },

    getMaterialTypes: function(component) {
        return new Promise(function(resolve, reject) {
            if (component.get('v.isMaterialTypesDisplayed')) {
                var action = component.get('c.loadMaterialTypes');
                action.setParams({
                    defaultMaterialTypes: component.get('v.defaultMaterialTypes')
                });        
                action.setCallback(this, function (data) {
                    if (data.getReturnValue()) {
                        var response = data.getReturnValue();
                        if (response.data.length > 0) {
                            var data = response.data;
                            component.set('v.materialTypeSelectOptions', data);
    
                            var materialTypeValues = [];
                            for (var dataCnt = 0; dataCnt < data.length; dataCnt++) {
                                materialTypeValues[dataCnt] = data[dataCnt].value;
                            }
                            component.set('v.searchParams.MaterialTypeValues', materialTypeValues);
                        }
                        else component.set('v.isMaterialTypesDisplayed', false);
                    }
                    resolve(true);
                });
    
                $A.enqueueAction(action);
            }
            else resolve(true);
        });          
    },

    getProductHierarchies: function(component) {
        return new Promise(function(resolve, reject) {
            if (component.get('v.isProductHierarchyDisplayed')) {    
                var action = component.get('c.loadProductHierarchies');       
                action.setCallback(this, function (data) {
                    if (data.getReturnValue()) {
                        var response = data.getReturnValue();
                        if (response.data.length > 0) {
                            var data = response.data;
                            component.set('v.productHierarchies', data);
                        }
                    }
                    resolve(true);
                });
                $A.enqueueAction(action);
            }
            else resolve(true);
        });
    },

    getMaterialGroups: function(component) {
        return new Promise(function(resolve, reject) {
            var action = component.get('c.getMaterialGroups');
            action.setParams({
                salesOrg: component.get('v.salesOrganization'),
                distChannel: component.get('v.distributionChannel'),
                division: component.get('v.division')
            })
            action.setCallback(this, function (data) {
                if (data.getReturnValue()) {
                    var response = data.getReturnValue();
                    if (response.data) {
                        var data = response.data;
                        let matGroups = data.ET_MATERIAL_GROUPS_List.filter(item => item.MATKL);
                        component.set('v.materialGroups', matGroups);
                    }
                }
                resolve(true);
            });
            $A.enqueueAction(action);
        });
    },

    searchMaterials: function(component, pagingOptions, helper) {
        return new Promise(function(resolve, reject) {
            var action = helper.setSearchAction(component);
            action.setParams({
                searchParams: component.get('v.searchParams'), 
                pagingOptions: pagingOptions
            });

            action.setCallback(this, function (data) {
                if (data.getReturnValue()) {
                    var response = data.getReturnValue();
                    response.data.forEach(function(row) {
                        row.productHierarchyField = row.productHierarchy + ' - ' + row.productHierarchyDescription;
                        if (!row.bomUsage) {
                            row.bomUsageCssClass = 'hideActionButton';
                        }
                    })

                    component.set('v.searchResults', response.data);
                    component.set('v.messages', response.messages);

                    if(response.data.length == 0) {
                        var errorMessage = {
                            message: $A.get('$Label.c.Enosix_SalesDoc_Message_SearchNoResult'), 
                            messageType: "INFO"
                        };
                        component.set('v.messages', errorMessage);
                        component.set('v.displayResults', []);
                    }
                    else {
                        helper.displayInitialResults(component);
                    }
                }
                resolve(true);            
            });
            $A.enqueueAction(action);
        });
    },

    setSearchAction : function(component) {
        var action = component.get('c.searchMaterials');
        component.set('v.searchParams.SalesOrganization', component.get('v.salesOrganization'));
        component.set('v.searchParams.DistributionChannel', component.get('v.distributionChannel'));
        return action;
    },

    displayInitialResults: function(component) {
        var searchResults = component.get('v.searchResults');
        var filterRows;
        var initialRow = 20;
        if(searchResults.length > initialRow) {
            filterRows = searchResults.slice(0, initialRow);
            component.set('v.enableInfiniteLoading', true);
            component.set('v.loadCount', initialRow);
        } else {
            filterRows = searchResults;
            component.set('v.enableInfiniteLoading', false);
        }
        component.set('v.displayResults', filterRows);
    },

    saveToItems: function(component, event)
    {
        this.updateSelectedDraftValues(component, event);
        //convert Map to JSON
        var itemJSONList = JSON.stringify(component.get('v.selectedItemsList'));

        this.addItems(component, itemJSONList)
    },

    addItems: function(component, itemJSONList)
    {
        let isInFlow = component.get('v.isSeparateFlowComponent');
        //if added directly to a flow, set as variable, navigate to next
        if(isInFlow)
        {
            component.set('v.selectedItemsJSON', itemJSONList);
            //TODO:navigate next
        }
        else //if called from a flow component: set event value, fire event, close    
        {
            var evt = component.getEvent('selectMaterialsEvent');
            evt.setParams({selectedItems:itemJSONList});
            evt.fire();
            component.find('overlayLib').notifyClose();
        }
    },

    updateSelectedDraftValues: function(component, event) {

        var selectedItems = component.get('v.selectedItemsList');
        var draftValues = component.get('v.draftValuesList');

        //roll through selected materials, and apply draftValues
        selectedItems.forEach(function(thisItem, i) {
            var material = thisItem.material;
            var filteredDraft = draftValues.filter(mDraft => mDraft.material == material);

            if(filteredDraft.length>0)
            {
                filteredDraft.forEach(function(thisDraft)
                {
                    if(thisDraft.quantity!= undefined)
                    {
                        thisItem.quantity = thisDraft.quantity;
                    }
                    if(thisDraft.scheduleDate != undefined)
                    {
                        thisItem.scheduleDate = thisDraft.scheduleDate;
                    }
                });
            }
            selectedItems.splice(i,1,thisItem);
        });

        //update selectedItemsList
        component.set('v.selectedItemsList', selectedItems);
    },

    search: function(component, event, helper) {
        var pagingOptions = {pageNumber: 1, pageSize: component.get('v.maxNumberOfRows')};
        component.set('v.displaySpinner', true);
        helper.searchMaterials(component, pagingOptions, helper)
            .then($A.getCallback(function() {
                component.set('v.displaySpinner', false);
            }))
    },

    openBOMUI: function(component, row) {
        let searchParams = {
            material: row.material,
            plant: row.plant,
            salesOrganization: component.get('v.salesOrganization'),
            distributionChannel: component.get('v.distributionChannel')
        }

        $A.createComponent('c:ensxtx_CMP_MaterialBOM', {
            'searchParams': searchParams
        },
        function (content, status, errorMessage) {
            if (status === 'SUCCESS') {
                content.addEventHandler('selectMaterialsBOMEvent',
                    component.getReference('c.onSelectedMaterialsBOM'));
                component.find('overlayLib')
                    .showCustomModal({
                        body: content,
                        showCloseButton: true,
                        closeCallback: function () {}
                    })
            }
        })
    }
})