({
    init : function (component) {
        
        let input = {
            flowName : "Sales_Channel_Selection",
            //flowName : "FLOW_NAME_HEADER_OVERRIDE",
            inputVariables : [{
                name:"CurrentRecordvar",
                type:"String",
                value: component.get('v.recordId')
            }]
        }        
        
        let flow = component.find("flowContainer");
        flow.startFlow(input.flowName, input.inputVariables);
    },
    
    statusChange : function (cmp, event) {
        
        
        // Control what happens when the interview finishes
			if(event.getParam("status") === "FINISHED") {
                  var outputVariables = event.getParam("outputVariables");
                  var key;
                  var accountId, recordTypeId, salesChannel;
                 
                  for(key in outputVariables) {
                     console.log('key=' + key);
                     if(outputVariables[key].name === "SelectedSalesChannelChoicevar") {
                         salesChannel = outputVariables[key].value;
                     } else if (outputVariables[key].name === "CurrentRecordvar") {
                         accountId = outputVariables[key].value;
                     } else if (outputVariables[key].name === "OppRecordTypeIdvar") {
                         recordTypeId = outputVariables[key].value;
                     }
                  }
                 console.log('accountId=' + accountId);
                console.log('salesChannel=' + salesChannel);
                console.log('recordTypeId=' + recordTypeId);
                  var url = "/lightning/cmp/c__URL_CreateRecordCmp?c__objectName=Opportunity&c__Sales_Channel--c=" + salesChannel + "&c__recordid=" + accountId + "&c__recordTypeId=" + recordTypeId + "&c__AccountId=" + accountId;
                 
                  //now we just need to redirect to this url.
                  var urlEvent = $A.get("e.force:navigateToURL");
                  urlEvent.setParams({
           "url": url
          });
                 urlEvent.fire();
            }
    }
})