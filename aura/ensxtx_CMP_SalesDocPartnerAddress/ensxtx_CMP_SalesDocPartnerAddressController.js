({
    onInit: function(component, event, helper) {
        helper.getCountries(component)
            .then($A.getCallback(function() {
                console.log('final resolve');
                helper.setCountryAndRegion(component);
                return helper.getTaxJurisdictions(component);
            }))
            .then($A.getCallback(function() {
                helper.setTaxJurisdiction(component);
                component.set('v.displaySpinner', false);
            }))
    },

    onSelectChange: function(component, event, helper) {
        let inputName = event.getSource().get('v.name');
        if (inputName === 'Country') {
            helper.setCountryAndRegion(component);
        }
        if (inputName === 'Country' || inputName === 'Region') {
            helper.searchTaxJurisdictions(component, helper);
        }
    },

    onInputFocus: function(component, event, helper) {
        let currentValue = event.getSource().get('v.value');
        component.set('v.onFocusInputValue', currentValue);
    },

    onInputBlur: function(component, event, helper) {
        let oldValue = component.get('v.onFocusInputValue');
        let newValue = event.getSource().get('v.value');

        if (oldValue != newValue) {
            let inputName = event.getSource().get('v.name');
            if (inputName === 'City' || inputName === 'PostalCode') {
                helper.searchTaxJurisdictions(component, helper);
            }
        }
        component.set('v.onFocusInputValue', null);
    },

    onSave: function(component, event, helper) {
        let partner = component.get('v.partner');
        let evt = component.getEvent('partnerAddressSaveEvent');
        partner.isChanged = true;

        evt.setParams({
            partner: partner
        });
        evt.fire();
        helper.closeComponent(component);
    },

    onCancel: function(component, event, helper) {
        helper.closeComponent(component);
    }
})