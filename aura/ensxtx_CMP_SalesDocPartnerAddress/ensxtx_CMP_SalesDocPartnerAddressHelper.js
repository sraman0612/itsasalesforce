({
    getCountries: function(component) {
        return new Promise(function(resolve, reject) {
            console.log('get Countries');
            let action = component.get('c.getCountries');
            action.setCallback(this, function(res) {
                let response = res.getReturnValue();       
                if (response && response.data) {
                    let filteredCountries = new Set(component.get('v.filteredCountries'));
                    let countries = response.data.ET_OUTPUT_List.filter(item => filteredCountries.has(item.LAND1));
                    let allRegions = response.data.ET_REGIONS_List;
                    component.set('v.countries', countries);
                    component.set('v.allRegions', allRegions);
                }
                else {
                    component.set('v.messages', response.messages);
                }
                resolve(true);
            })

            $A.enqueueAction(action);
        })
    },

    searchTaxJurisdictions: function(component, helper) {
        component.set('v.displaySpinner', true);
        helper.getTaxJurisdictions(component)
            .then($A.getCallback(function() {
                helper.setTaxJurisdiction(component);
                component.set('v.displaySpinner', false);
            }))
    },

    getTaxJurisdictions: function(component) {
        return new Promise(function(resolve, reject) {
            let partner = component.get('v.partner');
            if (partner.City && partner.Country && partner.Region && partner.PostalCode)
            {
                console.log('get Tax Jurisdictions');
                let action = component.get('c.getTaxJurisdictions');
                action.setParams({
                    city: partner.City,
                    country: partner.Country,
                    region: partner.Region,
                    postalCode: partner.PostalCode
                })
                action.setCallback(this, function(res) {
                    let response = res.getReturnValue();       
                    if (response && response.data) {
                        let counties = response.data.ET_LOCATION_RESULTS_List.filter(item => item.COUNTY);
                        component.set('v.counties', counties);
                    }
                    else {
                        component.set('v.messages', response.messages);
                    }
                    resolve(true);
                })

                $A.enqueueAction(action);
            }
            else resolve(true);
        })
    },

    setTaxJurisdiction: function(component) {
        let partner = component.get('v.partner');
        if (partner.District) {
            let counties = component.get('v.counties');
            let selectedDistrict = counties.find(county => {
                let district = county.COUNTY + ';' + county.TXJCD;
                if (district === partner.District) {
                    return county;
                }
            });

            if (!selectedDistrict) partner.District = '';
            component.set('v.partner', partner);
        }
    },

    setCountryAndRegion: function(component) {
        let partner = component.get('v.partner');
        let regions = [];

        // Default to US
        if (!partner.Country) {
            partner.Country = 'US';
            component.set('v.partner', partner);
        }

        if (partner.Country) {
            component.set('v.displaySpinner', true);
            let allRegions = component.get('v.allRegions');
            regions = allRegions.filter(region => region.LAND1 === partner.Country);
            component.set('v.regions', regions);

            setTimeout(function() {
                // This needs to wait inside a timeout because regions need to finish rendering
                if (partner.Region) {
                    let region = regions.find(reg => reg.REGIO === partner.Region);

                    if (region) {
                        partner.Region = region.REGIO;
                    }
                    else partner.Region = '';
                    component.set('v.partner', partner);
                }
                component.set('v.displaySpinner', false);
            }, 50)
        }
    },

    closeComponent: function(component) {
        component.find("overlayLibPartnerAddress").notifyClose();
    }
})