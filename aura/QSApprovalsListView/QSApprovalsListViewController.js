({
    "init" : function(cmp) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c.instantiate");
        
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            if (state === "SUCCESS") {
				//approvals
				cmp.set("v.approvals", response.getReturnValue());
            }

        });

        $A.enqueueAction(action);
    }
})