({
  onInit: function (component, event, helper) {
    console.log('accountId=' + component.get('v.accountId'));
    console.log('sapServiceNotificationNumber=' + component.get('v.sapServiceNotificationNumber'));
    console.log('sapSalesOrderNumber=' + component.get('v.sapSalesOrderNumber'));
    console.log('serialNumberId=' + component.get('v.serialNumberId'));
    console.log('hoursInService=' + component.get('v.hoursInService'));
    console.log('emailContact=' + component.get('v.emailContact'));
    console.log('locationNameLine1=' + component.get('v.locationNameLine1'));
    console.log('locationNameLine2=' + component.get('v.locationNameLine2'));
    console.log('locationStreet=' + component.get('v.locationStreet'));
    console.log('locationCity=' + component.get('v.locationCity'));
    console.log('locationPostalCode=' + component.get('v.locationPostalCode'));
    console.log('locationRegion=' + component.get('v.locationRegion'));
    console.log('locationCountry=' + component.get('v.locationCountry'));
    console.log('locationCounty=' + component.get('v.locationCounty'));
    console.log('locationPhone=' + component.get('v.locationPhone'));
    console.log('locationFax=' + component.get('v.locationFax'));
    console.log('failedPartNo=' + component.get('v.failedPartNo'));
    console.log('failedSerialNo=' + component.get('v.failedSerialNo'));
    console.log('failedHoursInService=' + component.get('v.failedHoursInService'));
    console.log('failedStartupDate=' + component.get('v.failedStartupDate'));
    console.log('replacementPartNo=' + component.get('v.replacementPartNo'));
    console.log('replacementSerialNo=' + component.get('v.replacementSerialNo'));
    console.log('replacementGDInvoiceNumber=' + component.get('v.replacementGDInvoiceNumber'));
    console.log('replacementGDInvoiceItemNumber=' + component.get('v.replacementGDInvoiceItemNumber'));
    console.log('serviceDate=' + component.get('v.serviceDate'));
    console.log('serviceCustomerRefNumber=' + component.get('v.serviceCustomerRefNumber'));
    console.log('serviceTechnicianName=' + component.get('v.serviceTechnicianName'));
    console.log('serviceTechnicianNumber=' + component.get('v.serviceTechnicianNumber'));
    console.log('serviceLaborHours=' + component.get('v.serviceLaborHoursD'));
    console.log('serviceTravelHours=' + component.get('v.serviceTravelHoursD'));
    console.log('serviceTravelDistance=' + component.get('v.serviceTravelDistance'));
    console.log('serviceOtherExpenses=' + component.get('v.serviceOtherExpenses'));
    console.log('serviceLocalMaterial=' + component.get('v.serviceLocalMaterial'));
    console.log('serviceFreight=' + component.get('v.serviceFreight'));
    console.log('serviceExpenseItemsJSON=' + component.get('v.serviceExpenseItemsJSON'));
    console.log('serviceExpenseItemsJSONOutput=' + component.get('v.serviceExpenseItemsJSONOutput'));
      
    component.set('v.maxDate', new Date().toJSON().slice(0,10).replace(/-/g,'/'));

    var serviceTechnicianName = component.get('v.serviceTechnicianName');
    var serviceTechnicianNumber = component.get('v.serviceTechnicianNumber');

    if (serviceTechnicianName && serviceTechnicianNumber) {
      var selectedServiceTechnician = serviceTechnicianNumber + '|' + serviceTechnicianName;

      component.set('v.selectedServiceTechnician', selectedServiceTechnician);
    }

    helper.populateServiceExpenseItems(component);
  },

  handleChangeEvent: function (component, event, helper) {
    console.log('inside change');
    var rowNumber = event.getParam('RowNumber');
    var row = event.getParam('Row');
    var items = component.get('v.serviceExpenseItems');

    if (rowNumber < items.length) {
      items.splice(rowNumber, 1, JSON.parse(row));
      component.set('v.serviceExpenseItems', items);
    }
  },

  handleNavigate: function (component, event, helper) {
    console.log(event.getParam('action') + ' pressed');
    var navigate = component.get('v.navigateFlow');

    var navAction = event.getParam('action');
    if (navAction == 'NEXT' || navAction == 'FINISH') {
      helper.validate(component, helper);

      var partsClaim = component.get('v.partsClaim');

      if (!partsClaim) {
        var selectedServiceTechnician = component.get('v.selectedServiceTechnician');
        if (!selectedServiceTechnician) {
          helper.addErrorMessage(component, 'Service Technician must not be blank.');
          component.set('v.selectedServiceTechnician', null);
        } else {
          var serviceTechArr = selectedServiceTechnician.split('|');
          component.set('v.serviceTechnicianNumber', serviceTechArr[0]);
          component.set('v.serviceTechnicianName', serviceTechArr[1]);
        }
      } else {
        var failedPartNo = component.get('v.failedPartNo');

        if (!failedPartNo) {
          helper.addErrorMessage(component, 'Failed component data must be completed.');
        }
      }

      console.log('v.messages.length=' + component.get('v.messages').length);

      if (!helper.needsSimulation(component, helper) && !helper.hasErrorMessages(component)) {
        var serviceExpenseItems = component.get('v.serviceExpenseItems');

        component.set('v.serviceExpenseItemsJSONOutput', JSON.stringify(serviceExpenseItems));
        console.log(component.get('v.serviceExpenseItemsJSONOutput'));

        navigate(navAction);
      } else {
        component.set('v.pendingNavAction', navAction);
        helper.clearMessages(component);
        helper.simulateOrder(component, helper);
      }
    } else {
      navigate(navAction);
    }
  }
});