({
  hasErrorMessages: function (component) {
    var messages = component.get('v.messages');

    const found = messages.find(function (row) {
      return row.messageType == 'ERROR';
    });

    return found ? true : false;
  },

  clearErrorMessages: function (component, category) {
    var oldmessages = component.get('v.messages');

    var messages = new Array();

    oldmessages.forEach(function (row) {
      if (!(row.messageType == 'ERROR' && row.messageCategory == category)) {
        messages.push(row);
      }
    });

    component.set('v.messages', messages);
  },

  clearWarningMessages: function (component, category) {
    var oldmessages = component.get('v.messages');

    var messages = new Array();

    oldmessages.forEach(function (row) {
      if (!(row.messageType == 'WARNING' && row.messageCategory == category)) {
        messages.push(row);
      }
    });

    component.set('v.messages', messages);
  },

  hasSuccessMessages: function (component) {
    var messages = component.get('v.messages');

    const found = messages.find(function (row) {
      return row.messageType == 'SUCCESS';
    });

    return found ? true : false;
  },

  hasWarningMessages: function (component) {
    var messages = component.get('v.messages');

    const found = messages.find(function (row) {
      return row.messageType == 'WARNING';
    });

    return found ? true : false;
  },

  addMessage: function (component, message, category) {
    var messages = component.get('v.messages');
    message.messageCategory = category;
    messages.push(message);

    component.set('v.messages', messages);
  },

  addErrorMessage: function (component, message, category) {
    var messages = component.get('v.messages');

    messages.push({
      messageType: 'ERROR',
      message: message,
      messageCategory: category
    });

    component.set('v.messages', messages);
  },

  addWarningMessage: function (component, message, category) {
    var messages = component.get('v.messages');

    messages.push({
      messageType: 'WARNING',
      message: message,
      messageCategory: category
    });

    component.set('v.messages', messages);
  },

  addInfoMessage: function (component, message, category) {
    var messages = component.get('v.messages');

    messages.push({
      messageType: 'INFO',
      message: message,
      messageCategory: category
    });

    component.set('v.messages', messages);
  },

  addSuccessMessage: function (component, message, category) {
    var messages = component.get('v.messages');

    messages.push({
      messageType: 'SUCCESS',
      message: message,
      messageCategory: category
    });

    component.set('v.messages', messages);
  },

  clearMessages: function (component) {
    component.set('v.messages', new Array());
  },

  validate: function (component, helper) {
    var serviceDate = component.get('v.serviceDate');
    if (!serviceDate) {
      helper.addErrorMessage(component, 'Date of Final Service must not be blank.');
      component.set('v.serviceDate', null);
    } else {
      var thisServiceDate = new Date(
        parseInt(serviceDate.substring(0, 4)),
        parseInt(serviceDate.substring(5, 7)) - 1,
        parseInt(serviceDate.substring(8, 10))
      );
      var now = new Date();

      if (thisServiceDate > now) {
        helper.addErrorMessage(component, 'Date of Final Service must not be in the future.');
      }
    }

    helper.validateNumberPositive(component, helper, 'v.serviceLaborHoursD', 'Labor (Hours) must be non-negative.');
    helper.validateNumberPositive(component, helper, 'v.serviceTravelHoursD', 'Travel (Hours) must be non-negative.');
    helper.validateNumberPositive(component, helper, 'v.serviceTravelDistance', 'Travel Distance (Miles) must be non-negative.');
    helper.validateNumberPositive(component, helper, 'v.serviceOtherExpenses', 'Other Expenses must be non-negative.');
    helper.validateNumberPositive(component, helper, 'v.serviceLocalMaterial', 'Local Materials must be non-negative.');
    helper.validateNumberPositive(component, helper, 'v.serviceFreight', 'Freight must be non-negative.');

    var failedPartNo = component.get('v.failedPartNo');
    var failedSerialNo = component.get('v.failedSerialNo');

    if (!failedPartNo && failedSerialNo) {
      helper.addErrorMessage(component, 'Failed Part Number required when Failed Serial Number is entered.');
    }
  },

  validateNumberPositive: function (component, helper, attributeName, errorMessage) {
    var value = component.get(attributeName);

    if (value && value < 0) {
      helper.addErrorMessage(component, errorMessage);
    }
  },

  needsSimulation: function (component, helper) {
    if (!component.get('v.warrantyClaimStr')) {
      return true;
    }

    var currentWarrantyClaim = JSON.parse(component.get('v.warrantyClaimStr'));

    var warrantyClaim = helper.createWarrantyClaim(component, helper);

    return helper.isDifferent(currentWarrantyClaim, warrantyClaim, helper);
  },

  createWarrantyClaim: function (component, helper) {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }

    var formattedDate = year + '-' + month + '-' + day;

    var warrantyClaim = {
      accountId: component.get('v.accountId'),
      serialNumber: component.get('v.serialNumber'),
      serviceDate: component.get('v.serviceDate'),
      modelNumber: component.get('v.modelNumber'),
      failedPartNumber: component.get('v.failedPartNo'),
      failedSerialNumber: component.get('v.failedSerialNo'),
      failedHoursInService: component.get('v.failedHoursInService'),
      failedStartupDate: component.get('v.failedStartupDate'),
      salesOrg: component.get('v.salesOrg'),
      distributionChannel: component.get('v.distributionChannel'),
      dateOfMalfunction: formattedDate,
      sapServiceNotificationNumber: component.get('v.sapServiceNotificationNumber'),
      customerReferenceNumber: component.get('v.serviceCustomerRefNumber'),
      serviceTechnicianNumber: component.get('v.serviceTechnicianNumber'),
      hoursInService: component.get('v.hoursInService'),
      laborHours: component.get('v.serviceLaborHoursD'),
      travelHours: component.get('v.serviceTravelHoursD'),
      travelMiles: component.get('v.serviceTravelDistance'),
      freight: component.get('v.serviceFreight'),
      localMaterial: component.get('v.serviceLocalMaterial'),
      otherExpenses: component.get('v.serviceOtherExpenses'),
      replacementPartNumber: component.get('v.replacementPartNo'),
      replacementSerialNumber: component.get('v.replacementSerialNo'),
      replacementGDInvoiceNumber: component.get('v.replacementGDInvoiceNumber'),
      replacementGDInvoiceItemNumber: component.get('v.replacementGDInvoiceItemNumber'),
      locationNameLine1: component.get('v.locationNameLine1'),
      locationNameLine2: component.get('v.locationNameLine2'),
      locationStreet: component.get('v.locationStreet'),
      locationCity: component.get('v.locationCity'),
      locationPostalCode: component.get('v.locationPostalCode'),
      locationRegion: component.get('v.locationRegion'),
      locationCountry: component.get('v.locationCountry'),
      locationCounty: component.get('v.locationCounty'),
      locationPhone: component.get('v.locationPhone'),
      locationFax: component.get('v.locationFax'),
      partsClaim: component.get('v.partsClaim')
    };

    var serviceExpenseItems = component.get('v.serviceExpenseItems');
    var filteredItems = serviceExpenseItems.filter(function (row) {
      return row.PartNo ? true : false;
    });

    if (filteredItems.length > 0) {
      console.log('we have filtered items');
      var itemJSON = JSON.stringify(filteredItems);
      warrantyClaim.itemJSON = itemJSON;
      component.set('v.serviceExpenseItemsJSONOutput', itemJSON);
    } else {
      console.log('we do not have filtered items');
      component.set('v.serviceExpenseItemsJSONOutput', '[]');
    }

    return warrantyClaim;
  },

  isDifferentImpl: function (one, two, defaultNullVal) {
    if (one == null) {
      one = defaultNullVal;
    }

    if (two == null) {
      two = defaultNullVal;
    }

    return one !== two;
  },

  isDifferent: function (one, two, helper) {
    if (helper.isDifferentImpl(one.accountId, two.accountId, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.serialNumber, two.serialNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.serviceDate, two.serviceDate, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.modelNumber, two.modelNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.failedPartNumber, two.failedPartNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.failedSerialNumber, two.failedSerialNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.failedHoursInService, two.failedHoursInService, 0)) {
      return true;
    }

    if (helper.isDifferentImpl(one.failedStartupDate, two.failedStartupDate, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.salesOrg, two.salesOrg, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.distributionChannel, two.distributionChannel, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.dateOfMalfunction, two.dateOfMalfunction, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.sapServiceNotificationNumber, two.sapServiceNotificationNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.customerReferenceNumber, two.customerReferenceNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.serviceTechnicianNumber, two.serviceTechnicianNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.hoursInService, two.hoursInService, 0)) {
      return true;
    }

    if (helper.isDifferentImpl(one.laborHours, two.laborHours, 0)) {
      return true;
    }

    if (helper.isDifferentImpl(one.travelHours, two.travelHours, 0)) {
      return true;
    }

    if (helper.isDifferentImpl(one.travelMiles, two.travelMiles, 0)) {
      return true;
    }

    if (helper.isDifferentImpl(one.freight, two.freight, 0)) {
      return true;
    }

    if (helper.isDifferentImpl(one.localMaterial, two.localMaterial, 0)) {
      return true;
    }

    if (helper.isDifferentImpl(one.otherExpenses, two.otherExpenses, 0)) {
      return true;
    }

    if (helper.isDifferentImpl(one.replacementPartNumber, two.replacementPartNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.replacementSerialNumber, two.replacementSerialNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.replacementGDInvoiceNumber, two.replacementGDInvoiceNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.replacementGDInvoiceItemNumber, two.replacementGDInvoiceItemNumber, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationNameLine1, two.locationNameLine1, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationNameLine2, two.locationNameLine2, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationStreet, two.locationStreet, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationCity, two.locationCity, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationPostalCode, two.locationPostalCode, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationRegion, two.locationRegion, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationCountry, two.locationCountry, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationCounty, two.locationCounty, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationPhone, two.locationPhone, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationFax, two.locationFax, '')) {
      return true;
    }

    if (helper.isDifferentImpl(one.locationFax, two.locationFax, false)) {
      return true;
    }

    if (helper.isDifferentImpl(one.itemJSON, two.itemJSON, '')) {
      return true;
    }

    return false;
  },

  populateServiceExpenseItems: function (component) {
    console.log('updating expense items');

    var outputItemJSON = component.get('v.serviceExpenseItemsJSONOutput');

    if (outputItemJSON) {
      try {
        var serviceExpenseItems = JSON.parse(outputItemJSON);

        if (serviceExpenseItems && serviceExpenseItems.length == 0) {
          serviceExpenseItems.push({
            PartNo: '',
            PartDescription: '',
            Quantity: null,
            Unit: '',
            GDInvoiceNumber: '',
            GDInvoiceItemNumber: ''
          });
        }
        component.set('v.serviceExpenseItems', serviceExpenseItems);
      } catch (err) {
        console.log('failed to parse service expense items with error ' + err);
      }
    }
  },

  simulateOrder: function (component, helper) {
    component.set('v.serviceLaborHoursD', component.get('v.serviceLaborHoursD') ? component.get('v.serviceLaborHoursD') : '0');
    component.set('v.serviceTravelHoursD', component.get('v.serviceTravelHoursD') ? component.get('v.serviceTravelHoursD') : '0');
    component.set('v.serviceTravelDistance', component.get('v.serviceTravelDistance') ? component.get('v.serviceTravelDistance') : '0');
    component.set('v.serviceFreight', component.get('v.serviceFreight') ? component.get('v.serviceFreight') : '0');
    component.set('v.serviceLocalMaterial', component.get('v.serviceLocalMaterial') ? component.get('v.serviceLocalMaterial') : '0');
    component.set('v.serviceOtherExpenses', component.get('v.serviceOtherExpenses') ? component.get('v.serviceOtherExpenses') : '0');

    var action = component.get('c.simulateSalesOrder');

    var warrantyClaim = helper.createWarrantyClaim(component, helper);

    if (!warrantyClaim.failedHoursInService) {
      warrantyClaim.failedHoursInService = 0;
    }

    var warrantyClaimStr = JSON.stringify(warrantyClaim);

    component.set('v.warrantyClaimStr', warrantyClaimStr);

    action.setParams({
      warrantyClaimStr: warrantyClaimStr,
      saveToo: false
    });

    component.set('v.displaySpinner', true);

    action.setCallback(this, function (response) {
      if (response.getReturnValue()) {
        helper.mergeMessages(component, response.getReturnValue().messages, 'simulateOrder');
        if (response.getReturnValue().data) {
          var salesOrder = response.getReturnValue().data;
          console.log(salesOrder);

          if (!helper.hasErrorMessages(component, 'simulateOrder') && !helper.hasWarningMessages(component, 'simulateOrder')) {
            var navigate = component.get('v.navigateFlow');
            navigate(component.get('v.pendingNavAction'));
          }

          if (helper.hasErrorMessages(component, 'simulateOrder')) {
            component.set('v.warrantyClaimStr', null);
          }

          component.set('v.displaySpinner', false);
        } else {
          component.set('v.displaySpinner', false);
        }
      } else {
        var labelReference = $A.get('$Label.c.Error_NoResponse');
        var noResponse = { message: labelReference, messageType: 'ERROR' };
        component.set('v.messages', noResponse);
        component.set('v.displaySpinner', false);
        component.set('v.warrantyClaimStr', null);
      }
    });

    $A.enqueueAction(action);
  },

  mergeMessages: function (component, addThese, category) {
    var messages = component.get('v.messages');

    addThese.forEach(function (row) {
      row.messageCategory = category;
      if (row.message.startsWith('Either invoice') || row.message.startsWith('Invoice')) {
        row.messageType = 'ERROR';
      } else if (row.message.startsWith('Please explain')) {
        row.message = 'Please explain in the "Additional Comments" on the following page.';
      }

      messages.push(row);
    });

    component.set('v.messages', messages);
  },

  trimExpensesJSON: function (component) {
    const itemstr = component.get('v.serviceExpenseItemsJSON');
    console.log(itemstr);
    if (itemstr) {
      var items = JSON.parse(itemstr);

      if (items && items.length > 0) {
        var newItems = new Array();
        items.forEach(function (row) {
          console.log('row.PartNo=' + row.PartNo);
          if (row.PartNo) {
            newItems.push(row);
          }
        });
        if (newItems.length > 0) {
          console.log('newItems=' + newItems);
          console.log('JSON.stringify(newItems)=' + JSON.stringify(newItems));
          component.set('v.serviceExpenseItemsJSON', JSON.stringify(newItems));
        } else {
          component.set('v.serviceExpenseItemsJSON', JSON.stringify([]));
        }
      }
    } else {
      component.set('v.serviceExpenseItemsJSON', JSON.stringify([]));
    }
  }
});