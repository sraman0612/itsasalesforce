<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>smagicinteract__SM_Home</defaultLandingTab>
    <description>Send bulk SMS or MMS campaigns. Converse in real time with Conversation View. Automate messages on workflows. Customize templates. Text Event Reminders. Seamless integration with Salesforce. Robust app for all your texting needs. Known for fast support.</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>SMS Magic Interact</label>
    <logo>smagicinteract__SMS_Magic/smagicinteract__SMS_Magic_Interact_Logo.png</logo>
    <tabs>smagicinteract__SM_Home</tabs>
    <tabs>smagicinteract__SMS_Magic_Settings</tabs>
    <tabs>smagicinteract__SMS_Template__c</tabs>
    <tabs>smagicinteract__Conversation_View_Classic</tabs>
    <tabs>smagicinteract__SMS_Magic_Help</tabs>
    <tabs>smagicinteract__smsMagic__c</tabs>
    <tabs>smagicinteract__Incoming_SMS__c</tabs>
    <tabs>Lookup_Quote_Docs_Distinct_Benefits__c</tabs>
    <tabs>Company_Brand__c</tabs>
    <tabs>Asset_Share__c</tabs>
    <tabs>GDI_Delivery_Vendors__c</tabs>
    <tabs>Lookup_PD_Blower_Machines__c</tabs>
    <tabs>Machine_Data_Lookup__c</tabs>
</CustomApplication>
