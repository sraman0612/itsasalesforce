<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#EB8C30</headerColor>
        <logo>ensxapp__EF_App_Logo</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <defaultLandingTab>ensxapp__enosiX_Connection_Test</defaultLandingTab>
    <description>Tools specific for testing and utilizing the enosiX Framework</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>enosiX Framework</label>
    <navType>Standard</navType>
    <tabs>ensxapp__enosiX_Connection_Test</tabs>
    <tabs>ensxapp__enosiX_Connection_Settings</tabs>
    <tabs>ensxapp__enosiX_Log</tabs>
    <uiType>Lightning</uiType>
</CustomApplication>
