public class UTIL_MachineDataLookup {
    public static List<Machine_Data_Lookup__c> getMachineDataLookupForQuoteLine(String quoteLineId) {
        SBQQ__QuoteLine__c quoteLine = [
            SELECT Id, 
            	Distribution_Channel__c, 
            	Frame_Size__c, 
            	SBQQ__Product__r.Name,
            	SBQQ__Product__r.MPG4_Code__r.MPG4_Number__c,
            	SBQQ__Product__r.Gear_Size__c,
            	Model_Matrix__c,
            	Horsepower__c,
            	Pressure__c,
            	Recip_Model__c,
            	Voltage__c,
            	SBQQ__Product__r.SBQQ__ExternallyConfigurable__c
            FROM SBQQ__QuoteLine__c
            WHERE Id =: quoteLineId 
            LIMIT 1
        ];

        return UTIL_MachineDataLookup.getMachineDataLookupForQuoteLine(quoteLine);
    }

    public static List<Machine_Data_Lookup__c> getMachineDataLookupForQuoteLine(SBQQ__QuoteLine__c quoteLine) {
        return UTIL_MachineDataLookup.getMachineDataLookupForQuoteLine(
            quoteLine.Distribution_Channel__c,
            quoteLine.Frame_Size__c,
            quoteLine.SBQQ__Product__r.SBQQ__ExternallyConfigurable__c?null:quoteLine.SBQQ__Product__r.Name,
            quoteLine.SBQQ__Product__r.MPG4_Code__r.MPG4_Number__c,
            quoteLine.SBQQ__Product__r.Gear_Size__c,
            quoteLine.Model_Matrix__c,
            quoteLine.Horsepower__c,
            quoteLine.Pressure__c,
            quoteLine.Recip_Model__c,
            quoteLine.Voltage__c
        );
    }
    
    public static List<Machine_Data_Lookup__c> getMachineDataLookupForQuoteLine(
        String distributionChannel, 
        String frameSize, 
        String configurator, 
        String mpg4code, 
        Decimal gearSize, 
        String modelMatrix,
        String horsepower, 
        String pressure,
        String recipModel, 
        String voltage) {
            
        String whereClause = '';
        boolean needsAnd = false;
        
        if (!String.isBlank(distributionChannel)) {
	        whereClause += 'DChl__c = \'' + distributionChannel + '\'';
            needsAnd = true;
        }
        if (!String.isBlank(frameSize)) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
	        whereClause += ' Frame_Size__c = \'' + frameSize + '\'';
        }
        if (!String.isBlank(configurator)) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
	        whereClause += ' Configurator__c = \'' + configurator + '\'';
        }
        if (!String.isBlank(mpg4code)) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
	        whereClause += ' MPG4__c = \'' + mpg4code + '\'';
        }
        if (gearSize != null) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
	        whereClause += ' Gear_Size__c = \'' + gearSize + '\'';
        }
        if (!String.isBlank(horsepower)) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
	        whereClause += ' HP__c = \'' + horsepower + '\'';
        }
        if (!String.isBlank(pressure)) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
	        whereClause += ' Pressure__c = \'' + pressure + '\'';
        }
        if (!String.isBlank(modelMatrix)) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
	        whereClause += ' Model_Matrix__c = \'' + modelMatrix + '\'';
        }
        if (!String.isBlank(recipModel)) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
	        whereClause += ' Recip_Model__c = \'' + recipModel + '\'';
        }
        if (!String.isBlank(voltage)) {
            if (needsAnd) {
                whereClause += ' AND';
            } else {
	            needsAnd = true;
            }
            
	        whereClause += ' Voltage__c = \'' + voltage + '\'';
        }

        System.debug(whereClause);
        
        String query = 'SELECT Id, Name, CFM__c, Configurator__c, DChl__c,' +
            ' Distinct_Benefit_1__c, Distinct_Benefit_2__c, Distinct_Benefit_3__c,' +
            ' Feature_Benefits__c, Frame_Size__c, Gear_Size__c, HP__c, Model_Matrix__c, Model__c,' +
            ' Model_Description__c, MPG4__c, MPG4_Code__c, OwnerId, Pressure__c,' +
            ' Recip_Model__c, Tech_Specs__c, Voltage__c FROM Machine_Data_Lookup__c';

        if (!String.isBlank(whereClause)) {
            query += ' WHERE ' + whereClause;
        }
        
        System.debug(query);
        
        try {
	        return Database.query(query);
        } catch (Exception e) {
            return new List<Machine_Data_Lookup__c>();
        }
    }
    
    public static List<Product_Images__c> getMachineImages(SBQQ__QuoteLine__c quoteLine) {
        return [
            SELECT Id, Image_URL__c 
            FROM Product_Images__c 
            WHERE MPG4_Code__c = : quoteLine.SBQQ__Product__r.MPG4_Code__c
            AND DC__c LIKE : quoteLine.SBQQ__Product__r.FLD_Distribution_Channel__c + '%'
            AND Image_Selection_Criteria__c = : quoteLine.SBQQ__Product__r.Image_Selection_Criteria__c
            AND Attribute__c = : quoteLine.Mounting_Attribute__c
            AND Horsepower__c = : quoteLine.Horsepower__c
        ];
    }

}