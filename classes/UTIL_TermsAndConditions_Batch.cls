public class UTIL_TermsAndConditions_Batch implements Database.Batchable<sObject> {

    public final Id accountId;
    public final String termsAndConditions;
    
    public UTIL_TermsAndConditions_Batch(Id acctId, String TNC){
        accountId = acctId;
        termsAndConditions = TNC;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug(accountId);
		return Database.getQueryLocator([SELECT Id, SBQQ__Account__c, My_Quote_Terms_and_Conditions__c from SBQQ__Quote__c where SBQQ__Status__c = 'Draft' AND SBQQ__Account__c =: accountId]);
    }
    
    public void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> quotes){
        
        for (SBQQ__Quote__c quote : quotes) {
            quote.My_Quote_Terms_and_Conditions__c = termsAndConditions;
        }
        
        update quotes;
        
    }

    public void finish(Database.BatchableContext BC){
        
        EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'CC360_T_C_Update_Completed']; 

        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setTemplateId(et.Id);
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSenderDisplayName('CC360 Team');
        mail.setReplyTo('sfadmin.qcy@gardnerdenver.com');
        mail.setSaveAsActivity(false);

        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {mail};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        system.debug('results : '+results);
        if (!results.get(0).isSuccess()) {
            System.StatusCode statusCode = results.get(0).getErrors()[0].getStatusCode();
            String errorMessage = results.get(0).getErrors()[0].getMessage();
        }

        
    }
    
}