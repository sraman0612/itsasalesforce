@isTest
public class ensxtx_TSTU_SalesDoc
{
    @isTest
    public static void test_saveHeaderAndLineItems()
    {
        Account newAccount = ensxtx_TSTU_SFTestObject.createTestAccount();
        newAccount.put(ensxtx_UTIL_SFAccount.CustomerFieldName, '1111');
        ensxtx_TSTU_SFTestObject.upsertWithRetry(newAccount);

        Id pricebookId = ensxtx_UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = ensxtx_TSTU_SFTestObject.createTestOpportunity();

        Product2 product = ensxtx_TSTU_SFTestObject.createTestProduct2();
        product.put(ensxtx_UTIL_SFProduct.MaterialFieldName, 'Material1');

        List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();
        OpportunityLineItem lineItem1 = ensxtx_TSTU_SFTestObject.createTestOpportunityLineItem();
		lineItem1.Quantity = 10;
		lineItem1.UnitPrice = .95;
		lineItem1.Description = 'test Desciption';
        lineItem1.ServiceDate = System.today();
        lineItems.add(lineItem1);

        OpportunityLineItem lineItem2 = ensxtx_TSTU_SFTestObject.createTestOpportunityLineItem();
        lineItem2.FLD_SAP_Item_Number__c = '000010';
		lineItem2.Quantity = 10;
		lineItem2.UnitPrice = .95;
		lineItem2.Description = 'test Desciption';
        lineItem2.ServiceDate = System.today();
        lineItems.add(lineItem2);

        ensxtx_UTIL_SalesDoc.SFObject sfObject = new ensxtx_UTIL_SalesDoc.SFObject();
        sfObject.sfObjectType = 'Opportunity';
        sfObject.sfObjectLabel = 'Opportunity';
        sfObject.initFromSObject = true;
        sfObject.sfMainObject = opp;
        sfObject.sfLineItems = lineItems;
        sfObject.customerNumber = '1111';

        ensxtx_DS_Document_Detail salesDocDetail = ensxtx_TSTD_Document_Detail.buildSalesDocDetail();

        ensxtx_DS_SalesDocAppSettings appSettings = new ensxtx_DS_SalesDocAppSettings();
        appSettings.updateLineItems = true;
        appSettings.deleteLineItems = false;
        ensxtx_UTIL_SalesDoc.saveHeaderAndLineItems(sfObject, salesDocDetail, appSettings);

        OpportunityLineItem lineItem3 = ensxtx_TSTU_SFTestObject.createTestOpportunityLineItem();
        lineItem3.FLD_SAP_Item_Number__c = '000020';
		lineItem3.Quantity = 10;
		lineItem3.UnitPrice = .95;
		lineItem3.Description = 'test Desciption';
        lineItem3.ServiceDate = System.today();
        lineItems.clear();
        lineItems.add(lineItem3);

        salesDocDetail.ITEMS[0].sfId = 'testId';

        ensxtx_UTIL_SalesDoc.saveHeaderAndLineItems(sfObject, salesDocDetail, appSettings);

        Set<String> materialNumbers = new Set<String>{product.Name, 'Not Found Material'};
        try {
            ensxtx_UTIL_SalesDoc.validateMaterials(materialNumbers, pricebookId, new Map<String, PricebookEntry>());
            PriceBookEntry pbe = ensxtx_TSTU_SFTestObject.createTestPriceBookEntry();
            pbe.UnitPrice = 100;
            pbe.Product2 = product;
            pbe.UseStandardPrice = false;
            pbe.IsActive = true;
            List<PriceBookEntry> entries = new List<PriceBookEntry>{pbe};
            ensxtx_UTIL_SalesDoc.validateMaterialsFromPBEList(materialNumbers, pricebookId, new Map<String, PricebookEntry>(), entries);
        }
        catch (Exception ex) {}
    }

    @isTest
    public static void test_getSFObjectInfo()
    {
        Account newAccount = ensxtx_TSTU_SFTestObject.createTestAccount();
        ensxtx_TSTU_SFTestObject.upsertWithRetry(newAccount);

        Opportunity opp = ensxtx_TSTU_SFTestObject.createTestOpportunity();
        opp.AccountId = newAccount.Id;
        ensxtx_TSTU_SFTestObject.upsertWithRetry(opp);

        ensxtx_UTIL_SalesDoc.SFObject result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(newAccount.Id, 'Quote');
        newAccount.put(ensxtx_UTIL_SFAccount.CustomerFieldName, '1111');
        ensxtx_TSTU_SFTestObject.upsertWithRetry(newAccount);

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(opp.Id, 'Order');

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(opp.Id, 'Order');

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(opp.Id, 'Quote');

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(opp.Id, 'Contract');

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(opp.Id, 'Inquiry');

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(opp.Id, 'Credit Memo');

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(opp.Id, 'Debit Memo');

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(opp.Id, 'Return Order');

        result = ensxtx_UTIL_SalesDoc.getSFObjectInfo('bad id', 'Order');
    }

    @isTest
    public static void test_mapSalesDocDetailFromSFObject()
    {
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();
        ensxtx_UTIL_SalesDoc.SFObject sfObject = new ensxtx_UTIL_SalesDoc.SFObject();
        ensxtx_DS_SalesDocAppSettings appSettings = new ensxtx_DS_SalesDocAppSettings();

        ensxtx_DS_Document_Detail newSalesDocDetail = ensxtx_UTIL_SalesDoc.mapSalesDocDetailFromSFObject(salesDocDetail, sfObject, appSettings);
    }

    @isTest
    public static void test_convertAppSettingPartners()
    {
        List<ensxtx_DS_SalesDocAppSettings.PartnerSetting> appSettingPartners = new List<ensxtx_DS_SalesDocAppSettings.PartnerSetting>();
        ensxtx_DS_SalesDocAppSettings.PartnerSetting partnerSetting = new ensxtx_DS_SalesDocAppSettings.PartnerSetting();
        partnerSetting.PartnerFunction = 'SH';
        partnerSetting.PartnerFunctionName = 'Ship';
        partnerSetting.ComponentType = 'TEST';
        appSettingPartners.add(partnerSetting);

        List<ensxtx_DS_Document_Detail.PARTNERS> partners = ensxtx_UTIL_SalesDoc.convertAppSettingPartnersToPartners(appSettingPartners);
    }

    @isTest
    public static void test_convertAppSettingTexts()
    {
        Map<String, String> appSettingTexts = new Map<String, String>{
            '0001' => 'tests'
        };

        List<ensxtx_DS_Document_Detail.TEXTS> texts = ensxtx_UTIL_SalesDoc.convertAppSettingTextsToTexts(appSettingTexts, 'EN');
    }
}