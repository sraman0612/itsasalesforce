@isTest
public class SFS_ServiceReportHandlerTest {
    @isTest
    public static void testmethod1(){
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> bill2 = SFS_TestDataFactory.createAccounts(1, False);
        bill2[0].IRIT_Customer_Number__c='123';
        bill2[0].Currency__c = 'USD';
        bill2[0].RecordTypeId=accRecID;
        insert bill2;
        List<Account> acc = SFS_TestDataFactory.createAccounts(1, False);
        acc[0].IRIT_Customer_Number__c='123';        
        acc[0].Bill_To_Account__c = bill2[0].Id;
        acc[0].Type = 'Prospect';
        insert acc[0];
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, True);
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1, div[0].Id, True);
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acc[0].Id, false);
        sc[0].SFS_Rental_Location1__c = loc[0].Id;
        insert sc;
        
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(2, acc[0].Id, loc[0].Id, div[0].Id, sc[0].Id, True);
        wo[0].ServiceReportTemplateId = '0SL3a000000blKMGAY';
        update wo[0];
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'test',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = wo[0].Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        
        ServiceReport sr = new ServiceReport();        
        sr.DocumentBody = Blob.valueOf('Test Content') ; 
        sr.DocumentContentType ='application/pdf';
        sr.DocumentName='Test';
        sr.ParentId = wo[0].Id ; 
        
        List<ServiceReport> srlist = new List<ServiceReport>();        
        srlist.add(sr);
        insert srlist;
        SFS_ServiceReportHandler.sendServiceReportAttachmentToWOContacts(srlist);
        
    }
}