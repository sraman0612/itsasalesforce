public without sharing class AddDistributors implements Queueable, Database.AllowsCallouts {

    private Lead currLead;
    private String salesOrg;
    private String dcVal;
    private String country;
    private String region;
    private String county;
    private String repAccount;
    private String repSalesDistrict;
    private String repSalesDistrictName;
    private String repAccountId = null;
    private String repUserId = null;

    public AddDistributors(Lead CurrLead,String SalesOrg,String DCval,String Country,String Region,String County,String RepAccount,String RepSalesDistrict,String RepSalesDistrictName) {
        this.currLead = CurrLead;
        this.salesOrg = SalesOrg;
        this.dcVal = DCval;
        this.country = Country;
        this.region = Region;
        this.county = County;
        this.repAccount = RepAccount;
        this.repSalesDistrict = RepSalesDistrict;
        this.repSalesDistrictName = RepSalesDistrictName;
        for (Account a : [SELECT Id FROM Account WHERE Account_Number__c = :RepAccount LIMIT 1]) {
            this.repAccountId = a.Id;
        }
        System.debug('RepSalesDistrictName=' + RepSalesDistrictName);
        if (String.isNotBlank(RepSalesDistrictName)) {
            for (User u : [SELECT Id FROM User WHERE Name = :RepSalesDistrictName]) {
                this.repUserId = u.Id;
            }
        }
    }

    public void execute(QueueableContext context) {

        system.debug('currLead = '+currLead);
        system.debug('salesOrg = '+salesOrg);
        system.debug('dcVal = '+dcVal);
        system.debug('country = '+country);
        system.debug('region = '+region);
        system.debug('county = '+county);
        system.debug('repAccount = '+repAccount);

        SBO_EnosixZAPR_Search.EnosixZAPR_SR result = UTIL_EnosixZAPRSearch.searchZAPR(salesOrg, dcVal, country, region, county, false);
        
        List<String> parentAccountCustomerNumbers = new List<String>();

        for (SBO_EnosixZAPR_Search.SEARCHRESULT vals : result.getResults()) {
            parentAccountCustomerNumbers.add(vals.CustomerNumber);
        }

        List<Account> accounts = [SELECT Id, Name, Parent_Account__c FROM Account WHERE Rep_Account__c = : repAccount AND Parent_Account__c in :parentAccountCustomerNumbers];

        Set<String> uniqueCustomerNumbers = new Set<String>();

        for (Account account : accounts) {
            uniqueCustomerNumbers.add(account.Parent_Account__c);
        }

        List<String> customerNumbers = new List<String>(uniqueCustomerNumbers);

        if (customerNumbers != null && !customerNumbers.isEmpty()) currLead.FLD_Customer_Numbers__c = String.join(customerNumbers, ',');
        if (repAccountId != null) {
            currLead.Rep_Account__c = repAccountId;

            Account parentAccount = [SELECT Id, AccountNumber from Account where Id = : repAccountId LIMIT 1];

            String acctNumber = parentAccount.AccountNumber + ' - CP';
            System.debug('acctNumber=' + acctNumber);

            Account childAccount = null;
            
            try {
                childAccount = [SELECT Id, Default_Lead_Assignee__c, TSM_of_DC_Account__c FROM Account WHERE AccountNumber = : acctNumber LIMIT 1];
            } catch (Exception e) {

            }

            if (childAccount != null) {
                if (childAccount.Default_Lead_Assignee__c != null) {
                    try {
                        System.debug('childAccount.Default_Lead_Assignee__c= ' + childAccount.Default_Lead_Assignee__c);
                        User defaultLeadAssignee = [Select Id, Name from User where ContactId = : childAccount.Default_Lead_Assignee__c LIMIT 1];
                        currLead.Rep_Lead_Owner__c = defaultLeadAssignee.Id;
                    } catch (Exception e) {

                    }
                }

                if (childAccount.TSM_of_DC_Account__c != null) {
                    System.debug('childAccount.TSM_of_DC_Account__c= ' + childAccount.TSM_of_DC_Account__c);
                    currLead.OwnerId = childAccount.TSM_of_DC_Account__c;
                }
            }
        }

        update currLead;
    }
}