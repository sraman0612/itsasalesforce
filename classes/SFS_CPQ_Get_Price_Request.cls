/* Author: Srikanth P - Capgemini
 * Date: 02/03/22
 * Description: Handler class to invoke CPQ Get price interface and receive DN cost and unit price as a response
 * TODO:
 */

global class SFS_CPQ_Get_Price_Request {
    
    global class getPriceRequest{
        @InvocableVariable
        public ID recId;

        @InvocableVariable
        public String currencyCode;

        @InvocableVariable
        public String divOrg;
    }
        
    @invocableMethod(label = 'GetPrice') 
    global static void GetPrice(List<getPriceRequest>priceRequests){
        For(getPriceRequest GPR:priceRequests)
        {
        ID recordID = GPR.recId;
        string objectName = recordID.getSObjectType().getDescribe().getName();
        
        List<ProductRequestLineItem> prliList = new List<ProductRequestLineItem>();
        List<ProductConsumed> pcList = new List<ProductConsumed>();
        List<CAP_IR_Labor__c> laborList = new List<CAP_IR_Labor__c>();
        
        Map<String,String> objectMap = new Map<String,String>();
        if(objectName == 'ProductRequestLineItem'){
            prliList = [Select Id,SFS_External_Id__c,Product2.ProductCode from ProductRequestLineItem where Id  =:recordID];
            for(ProductRequestLineItem prli:prliList){
                objectMap.put(prli.SFS_External_Id__c,prli.Product2.ProductCode);
            }
        }
        if(objectName == 'WorkOrderLineItem'){
             laborList = [Select Id,SFS_External_Id__c,SFS_Product__r.ProductCode from CAP_IR_Labor__c where CAP_IR_Work_Order_Line_Item__c  =:recordID];           
             if(!laborList.isEmpty()){     
                 for(CAP_IR_Labor__c la:laborList){
                     objectMap.put(la.SFS_External_Id__c,la.SFS_Product__r.ProductCode);
                 }
            }         
        }  
        if(objectName == 'ProductConsumed'){
            pcList = [Select Id,SFS_External_Id__c,Product2.ProductCode from ProductConsumed where Id  =:recordID];
            if(!pcList.isEmpty()){ 
                 for(ProductConsumed pc:pcList){
                     objectMap.put(pc.SFS_External_Id__c,pc.Product2.ProductCode); 
                 }
            }   
        }
        buildJSONRequest(objectMap,priceRequests);
        }    
    }
    //Logic to build JSON structrue
    public static void buildJSONRequest(Map<String,String> objectMap,List<getPriceRequest>priceRequests){
        
        getPriceHeader wrapper = new getPriceHeader();
        wrapper.currencyCode = priceRequests[0].currencyCode;
        //wrapper.HeaderAttributeValues.divisionH = priceRequests[0].divOrg;
        wrapper.HeaderAttributeValues.divisionH = 'ITS';
        wrapper.HeaderAttributeValues.serviceType ='ListPrice';
        for(String objectString :objectMap.KeySet()){
               Items itemWrap = new Items();
               itemWrap.partNumber=objectMap.get(objectString);
               itemWrap.itemIdentifier = objectString;
               wrapper.items.add(itemWrap);
           }    
        
       String jsonString = json.serialize(wrapper);
       system.debug('@@@jsonString'+jsonString); 
       SFS_Outbound_Integration_Callout.HttpMethod(jsonString,'CPQ Get Price','CPQ Get Price Interface'); 
        
    }
    
    //Parsing the response
    public static void getPriceResponse(HttpResponse response){
        Map<String,Decimal> unitPriceMap = new Map<String,Decimal>();
        Map<String,Decimal> dnCostMap = new Map<String,Decimal>();
        Map<String,Object> getPriceResponse = (Map<String,Object>)JSON.deserializeUntyped(response.getBody());
        List<ProductRequestLineItem> prliToUpdate = new List<ProductRequestLineItem>();
        List<ProductConsumed> pcToUpdate = new List<ProductConsumed>();
        List<CAP_IR_Labor__c> lhToUpdate = new List<CAP_IR_Labor__c>();
        
        for(Object items : (List<Object>)getPriceResponse.get('items')){
            Map<String,Object> item = (Map<String,Object>)items;
            
            List<Object> calInfo = (List<Object>)item.get('calculationInfo');
            Map<String, Object> a = (Map<String, Object>)calInfo[0];
            
            unitPriceMap.put(string.ValueOf(item.get('itemIdentifier')),(Decimal)a.get('unitPrice'));  
            dnCostMap.put(string.ValueOf(item.get('itemIdentifier')),(Decimal)a.get('DNCost'));
            system.debug('dnCostMap'+dnCostMap);
      }
        System.debug('unitPriceMap'+unitPriceMap);  
        for(ProductRequestLineItem prli : [select Id,SFS_List_Price__c,SFS_External_Id__c,SFS_DN_Cost__c from ProductRequestLineItem where SFS_External_Id__c IN: unitPriceMap.KeySet()]){
            If(unitPriceMap.containsKey(prli.SFS_External_Id__c)){
                prli.SFS_List_Price__c = unitPriceMap.get(prli.SFS_External_Id__c);
                prli.SFS_DN_Cost__c=dnCostMap.get(prli.SFS_External_Id__c);
                prliToUpdate.add(prli);
                system.debug('prliToUpdate.add(prli)'+prliToUpdate);
               }    
        }
        for(ProductConsumed pc : [select Id,UnitPrice,SFS_External_Id__c,SFS_DN_Cost__c from ProductConsumed where SFS_External_Id__c IN: unitPriceMap.KeySet()]){
            If(unitPriceMap.containsKey(pc.SFS_External_Id__c)){
                pc.UnitPrice = unitPriceMap.get(pc.SFS_External_Id__c);
                pc.SFS_DN_Cost__c = dnCostMap.get(pc.SFS_External_Id__c);
                pcToUpdate.add(pc);
               }    
        }
        
       for(CAP_IR_Labor__c lh : [select Id,CAP_IR_Unit_Price__c,SFS_External_Id__c from CAP_IR_Labor__c where SFS_External_Id__c IN: unitPriceMap.KeySet()]){
            If(unitPriceMap.containsKey(lh.SFS_External_Id__c)){
                lh.CAP_IR_Unit_Price__c = unitPriceMap.get(lh.SFS_External_Id__c);
                lhToUpdate.add(lh);
               }    
        } 
        if(!prliToUpdate.isEmpty()){           
           Database.update(prliToUpdate); 
         }
        
        if(!pcToUpdate.isEmpty()){
            Database.update(pcToUpdate);
        }
       
        
        if(!lhToUpdate.isEmpty()){
           Database.update(lhToUpdate); 
        }
    }
    
    public class getPriceHeader{
        public String currencyCode;
        public headerAttributeDataType headerAttributeValues;
        public List<Items> items;
        public getPriceHeader(){
            headerAttributeValues = new headerAttributeDataType();
            items = new List<items>();
        }
     }  
     public class headerAttributeDataType {
		public String divisionH;
        public String serviceType ;  
	 }  
     public class Items {
		 public String itemIdentifier;
		 public String partNumber;
	  }
   
}