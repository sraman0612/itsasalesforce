global class GDInside_Auth_Handler implements Auth.RegistrationHandler {

	global static final String INTERNAL_USER = 'INTERNAL_USER';
	global static final String COMMUNITY_POSTFIX = '.gdinside';
	global User createUser(Id portalId, Auth.UserData data){



		GDInsideUserWrapper gdUser = (GDInsideUserWrapper) JSON.deserialize(JSON.serialize(data.attributeMap), GDInsideUserWrapper.class);
		gdUser.identifier = data.identifier;
		System.debug('gdUser.username - ' + gdUser.username);

		if (String.isEmpty(data.attributeMap.get('username')))
			throw new Auth.AuthProviderPluginException ('Username was not specified');

		String sapAccountNumber = gdUser.sap_account_number;
		sapAccountNumber = (sapAccountNumber == '1234' || String.isEmpty(sapAccountNumber)) ? INTERNAL_USER : sapAccountNumber;


		System.debug('SAP Account Number : ' + sapAccountNumber);

		//check to see if the user already exists
		String username = sapAccountNumber == INTERNAL_USER ? gdUser.username : gdUser.username + COMMUNITY_POSTFIX;
		List <User> findUserList = new List <User> ([SELECT Id FROM User WHERE Username = : username LIMIT 1]);


		if (findUserList.isEmpty() && sapAccountNumber == INTERNAL_USER) {
			// per Nathan - we do not want to provision internal users from GDInside.
			throw new  Auth.AuthProviderPluginException ('Internal users need to be created first in Salesforce.');
		} else if (findUserList.isEmpty()) {
			return createCommunityUser (sapAccountNumber, gdUser);
		} else { //user already exists and linked
			return findUserList[0];
		}

	}

	private User createCommunityUser (String sapAccountNumber, GDInsideUserWrapper gdUser) {
		List <Account> sapAccount = new List <Account> ([SELECT Id FROM Account WHERE Account_Number__c =: sapAccountNumber LIMIT 1]);

		if (sapAccount.isEmpty())
			throw new  Auth.AuthProviderPluginException ('SAP Account not found for ' + sapAccountNumber);


		Contact userContact;

		try{
			//Check if a Contact already exists for that Email Address (GDInside username)
			userContact = [SELECT Id FROM Contact WHERE
			               Email = :gdUser.username LIMIT 1];
			System.debug('Found Contact : ' + userContact);
		} catch(QueryException qe) {
			//Create a new Contact and associate it to the Account we found earlier
			userContact = new Contact();
			userContact.accountId = sapAccount[0].Id;
			userContact.email = gdUser.username;
			userContact.firstName = gdUser.first_name;
			userContact.lastName = gdUser.last_name;
			userContact.MailingStreet = gdUser.address_1;
			insert(userContact);
			System.debug('New Contact : ' + userContact);
		}

		Profile singleaccountprof = [SELECT Id FROM Profile WHERE Name='Partner Community - Single Account User'];


		User u = new User();

		u.username = gdUser.username + COMMUNITY_POSTFIX;
		u.email = gdUser.username;
		u.firstName = gdUser.first_name;
		u.lastName = gdUser.last_name;
		String alias = gdUser.username;
		//Alias must be 8 characters or less
		if(alias.length() > 8) {
			alias = alias.substring(0, 8);
		}
		u.alias = alias;
		u.languagelocalekey = UserInfo.getLocale();
		u.localesidkey = UserInfo.getLocale();
		u.emailEncodingKey = 'UTF-8';
		u.timeZoneSidKey = 'America/Chicago';
		u.profileId = singleaccountprof.Id;
		u.Community_User_Type__c = 'Single User';
		u.contactId = userContact.Id;
		u.GDInside_User_Id__c = gdUser.identifier;

		insert u;
		System.debug(u);

		return u;
	}


	global void updateUser(Id userId, Id portalId, Auth.UserData data){
		System.debug('Existing user login - ' + data.identifier);
		User u = new User (Id = userId);
		u.GDInside_User_Id__c = data.identifier;
		update u;
	}

}