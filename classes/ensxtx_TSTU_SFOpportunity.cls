/*
* Test cases for opportunity utility class
*/
@isTest 
private class ensxtx_TSTU_SFOpportunity
{
    @isTest
    public static void test_getSObject()
    {
        SObject testSObject = createTestObjects();

        Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        utility.getSObject('bad Id');
        utility.getSObject(testSObject.Id);
        utility.getSObject(testSObject.Id);
        utility.getSObject('Quote', 'sapQuoteNumber');
        utility.getSObject('Order', 'sapOrderNumber');
        utility.getSObject('Contract', 'sapContractNumber');
        utility.getSObject('bad SAP Type', 'bad SAP Type');
        Test.stopTest();
    }

    @isTest
    static void test_getSObjectLineItems()
    {
        Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        Opportunity testSObject = createTestObjects();
        utility.getSObjectLineItems('bad Id');
        utility.getSObjectLineItems(testSObject.Id);
        utility.getSObjectLineItems(testSObject.Id);
        utility.loadLineMap(new List<SObject>{new Account()}, new Map<Id, SObject>());
        Test.stopTest();
    }

    @isTest
    static void test_getSObjectContacts()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getSObjectContacts(testSObject.Id);
		utility.getSObjectContacts(null);
		Test.stopTest();
    }

    @isTest
    static void test_getAccountId()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getAccountId(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getCustomerNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getCustomerNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getName()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getName(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getQuoteNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getQuoteNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getOrderNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getOrderNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getContractNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getContractNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getInquiryNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getInquiryNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getCreditMemoNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getCreditMemoNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getDebitMemoNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getDebitMemoNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getReturnOrderNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getReturnOrderNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getStatus()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getStatus(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getOpportunityId()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getOpportunityId(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getPriceBookId()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
		utility.getPriceBookId(null);
		utility.getPriceBookId(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getProductId()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
		utility.getProductId(null);
		Test.stopTest();
    }

    @isTest
    static void test_getMaterial()
    {
    	Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
		utility.getMaterial(testSObject, null);
		Test.stopTest();
    }

    @isTest
    static void test_getItemNumber()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
		utility.getItemNumber(null);
		Test.stopTest();
    }
    
    @isTest
    static void test_validateSAPWithSfsObject()
    {
        Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
        utility.validateSAPWithSfsObject('', testSObject);
        Test.stopTest();
    }

    @isTest
    static void test_sObjectToSalesDocMapping()
    {
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        Opportunity testSObject = createTestObjects();
        List<SObject> lineItems = new List<SObject>{createTestObjectLines()};
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();
        ensxtx_DS_SalesDocAppSettings appSettings = new ensxtx_DS_SalesDocAppSettings();
        appSettings.itemNumberIncrement = 10;
        appSettings.DefaultDocType = 'OR';
        appSettings.Header = new ensxtx_DS_SalesDocAppSettings.DocumentSetting();
        appSettings.Item = new ensxtx_DS_SalesDocAppSettings.DocumentSetting();

        ensxtx_DS_Document_Detail newSalesDocDetail = utility.sObjectToSalesDocMapping(testSObject, lineItems, salesDocDetail, appSettings);
        ensxtx_DS_Document_Detail.ITEMS item = salesDocDetail.ITEMS[0];
        testSObject.put('SAP_SaveSalesDocHeader__c', JSON.serialize(salesDocDetail));
        for (SObject lineItem : lineItems) {
            lineItem.put('SAP_SaveSalesDocItem__c', JSON.serialize(item));
        }
        newSalesDocDetail = utility.sObjectToSalesDocMapping(testSObject, lineItems, salesDocDetail, appSettings);
    }

    @isTest
    static void test_salesDocMappingToSObject()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();
        salesDocDetail.SalesDocument = 'SalesDocument';
        ensxtx_DS_SalesDocAppSettings appSettings = new ensxtx_DS_SalesDocAppSettings();
        appSettings.SAPDocType = 'Quote';
		utility.salesDocMappingToSObject(testSObject, salesDocDetail, appSettings);
        appSettings.SAPDocType = 'Order';
		utility.salesDocMappingToSObject(testSObject, salesDocDetail, appSettings);
        appSettings.SAPDocType = 'Contract';
		utility.salesDocMappingToSObject(testSObject, salesDocDetail, appSettings);
        appSettings.SAPDocType = 'Credit Memo';
		utility.salesDocMappingToSObject(testSObject, salesDocDetail, appSettings);
        appSettings.SAPDocType = 'Debit Memo';
		utility.salesDocMappingToSObject(testSObject, salesDocDetail, appSettings);
        appSettings.SAPDocType = 'Inquiry';
		utility.salesDocMappingToSObject(testSObject, salesDocDetail, appSettings);
        appSettings.SAPDocType = 'Return Order';
		utility.salesDocMappingToSObject(testSObject, salesDocDetail, appSettings);
		Test.stopTest();
    }

    @isTest
    static void test_salesDocLineItemMappingToSObject()
    {
    	Test.startTest();
        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        SObject testSObject = createTestObjects();
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();
        salesDocDetail.SalesDocument = 'SalesDocument';
        SObject lineItem = null;
		lineItem = utility.salesDocLineItemMappingToSObject(testSObject, new ensxtx_DS_Document_Detail.ITEMS(), new ensxtx_DS_SalesDocAppSettings(), new PricebookEntry(), null, lineItem);
		lineItem = utility.salesDocLineItemMappingToSObject(testSObject, new ensxtx_DS_Document_Detail.ITEMS(), new ensxtx_DS_SalesDocAppSettings(), new PricebookEntry(), null, lineItem);
		Test.stopTest();
    }

    private static Opportunity createTestObjects()
    {
        Id pricebookId = ensxtx_UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = ensxtx_TSTU_SFTestObject.createTestOpportunity();
        opp.Name = 'TEST OPP';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.Pricebook2Id = pricebookId;
        ensxtx_TSTU_SFTestObject.upsertWithRetry(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        ensxtx_TSTU_SFTestObject.upsertWithRetry(opp);

        Contact cont = ensxtx_TSTU_SFTestObject.createTestContact(null);
        ensxtx_TSTU_SFTestObject.upsertWithRetry(cont);

        OpportunityContactRole oppContRole = ensxtx_TSTU_SFTestObject.createTestOpportunityContactRole(opp, cont);
        oppContRole.OpportunityId = opp.Id;
        ensxtx_TSTU_SFTestObject.upsertWithRetry(oppContRole);

        ensxtx_UTIL_SFOpportunity utility = new ensxtx_UTIL_SFOpportunity();
        return (Opportunity) utility.getSObject(opp.Id);
    }

    private static SObject createTestObjectLines()
    {
        OpportunityLineItem oli = ensxtx_TSTU_SFTestObject.createTestOpportunityLineItem();
		oli.Quantity = 10;
		oli.UnitPrice = .95;
		oli.Description = 'test Desciption';
        oli.FLD_SAP_Item_Number__c = '000010';
        return oli;
    }
}