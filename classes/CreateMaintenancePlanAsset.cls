public class CreateMaintenancePlanAsset {
    @invocableMethod(label = 'Create Maintenance Plan' description = 'Create MaintenancePlan with Asset' Category = 'MaintenancePlan')
    public static void createMaintenancePlan(List<List<MaintenancePlan>> planList){        
        String serviceConId;
        String newServiceConId;
        System.debug('serviceConId'+serviceConId);
        List<MaintenancePlan> newPlanList = new List<MaintenancePlan>();
        if(planList.size() > 0 && planList[0] != NULL){
            newPlanList = planList[0];
            if(newPlanList.size() > 0){
                if(newPlanList[0].Parent_Service_Agreement__c != NULL){
                    serviceConId = newPlanList[0].Parent_Service_Agreement__c;
                }
                if(newPlanList[0].ServiceContractId != NULL){
                    newServiceConId = newPlanList[0].ServiceContractId; 
                }
            }
        }        
        Map<String,Id> mpIds = new Map<String,Id>();
        for(MaintenancePlan mp :newPlanList){
            mpIds.put(mp.Clone_From__c,mp.Id); 
            system.debug('@#$%'+mpIds);
        }
        
        List<MaintenanceAsset> insertList = new List<MaintenanceAsset>();
        Map<String,Id> lineIds = new Map<String,Id>();
        List<ContractLineItem> lineItemList = new List<ContractLineItem>();
        if(newServiceConId != NULL){
            lineItemList = [Select id, Clone_From__c from ContractLineItem where ServiceContractId =: newServiceConId];
        }
        for(ContractLineItem cli :lineItemList){
            lineIds.put(cli.Clone_From__c,cli.Id); 
        }
        Map<String,Id> entitleIds = new Map<String,Id>();
        List<Entitlement> entitleList = new  List<Entitlement>();
        if(newServiceConId != NULL){
            entitleList = [Select id, Clone_From__c from Entitlement where ServiceContractId =: newServiceConId];
        }
        for(Entitlement ent : entitleList){
            entitleIds.put(ent.Clone_From__c,ent.Id); 
        }
        List<MaintenanceAsset> mainList = new List<MaintenanceAsset>();
        if(serviceConId != NULL){
            mainList = [SELECT Id, MaintenancePlanId, AssetId, WorkTypeId,Text_Lookup_Entitlement__c,Text_Lookup_Line_Item__c,Text_Lookup_Maintenance_Plan__c, ContractLineItemId, SFS_Entitlement__c, SFS_External_Id__c, SFS_Service_Agreement_Line_Item__c, SFS_Service_Agreement__c, Clone_From__c FROM MaintenanceAsset WHERE SFS_Service_Agreement__c =: serviceConId];
        }
        for(MaintenanceAsset ma : mainList){
            ma.Clone_From__c = ma.Id;
            ma.id = NULL;
            ma.SFS_Service_Agreement__c = newServiceConId;            
            system.debug('Text_Lookup_Maintenance_Plan__c'+ma.Text_Lookup_Maintenance_Plan__c);
             system.debug('mpIds.get(ma.Text_Lookup_Maintenance_Plan__c'+mpIds.get(ma.Text_Lookup_Maintenance_Plan__c));
            ma.MaintenancePlanId = mpIds.get(ma.Text_Lookup_Maintenance_Plan__c);
            System.debug('lineIds.containsKey(ma.Text_Lookup_Line_Item__c)'+lineIds.get(ma.Text_Lookup_Line_Item__c));
            if(lineIds.containsKey(ma.Text_Lookup_Line_Item__c)){
                ma.ContractLineItemId = lineIds.get(ma.Text_Lookup_Line_Item__c);
            }
            if(lineIds.containsKey(ma.Text_Lookup_Entitlement__c)){           
                ma.SFS_Entitlement__c = entitleIds.get(ma.Text_Lookup_Entitlement__c); 
            }            
            ma.CreatedById = NULL;
            insertList.add(ma);
            System.debug('ma'+ma);
        } 
        if(insertList.size() > 0){
            System.debug('insertList'+insertList.size());
            Database.saveResult[] re = Database.insert(insertList);
             System.debug('re'+re);
        }
    }
}