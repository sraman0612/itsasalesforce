// TODO: Fix SBO MOCs and regen this file
@isTest
public class MOC_EnosixCustomer_Detail
{
	public class MockEnosixCustomerSuccess implements ensxsdk.EnosixFramework.DetailSBOGetMock, ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock, ensxsdk.EnosixFramework.DetailSBOInitMock
	{
		public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object obj)
		{
			SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();

			result.setSuccess(true);

			return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
		{
			SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();

			result.setSuccess(true);

			return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string commandName, ensxsdk.EnosixFramework.DetailObject obj)
		{
			SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();

			result.setSuccess(true);

			return result;
        }

		public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject obj)
		{
			SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();

			result.setSuccess(true);

			return result;
		}
	}

	public class MockEnosixCustomerFailure implements ensxsdk.EnosixFramework.DetailSBOGetMock, ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock, ensxsdk.EnosixFramework.DetailSBOInitMock
	{
		public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object obj)
		{
			SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();

			result.setSuccess(false);

			return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
		{
			SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();

			result.setSuccess(false);

			return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string commandName, ensxsdk.EnosixFramework.DetailObject obj)
		{
			SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();

			result.setSuccess(false);

			return result;
        }

		public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject obj)
		{
			SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();

			result.setSuccess(false);

			return result;
		}
	}
}