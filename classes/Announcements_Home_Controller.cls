public with sharing class Announcements_Home_Controller {

    @AuraEnabled
    public static List<Announcements__c> getAnnouncements() {
        return [SELECT Id, Name, Active__c, Distribution_Channels__c, Expire_Date__c, Subject__c, Message__c, Posted_Date__c, Related_post__c
                FROM Announcements__c
                WHERE Active__c = true AND Expire_Date__c > TODAY
                ORDER BY Posted_Date__c DESC, Id DESC];
    }
}