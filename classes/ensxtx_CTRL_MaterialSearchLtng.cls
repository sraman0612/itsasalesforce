public class ensxtx_CTRL_MaterialSearchLtng
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_CTRL_MaterialSearchLtng.class);

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response loadMaterialTypes (List<String> defaultMaterialTypes)
    {
        logger.enterAura('loadMaterialTypes', new Map<String, Object> {
            'defaultMaterialTypes' => defaultMaterialTypes
        });

        if (defaultMaterialTypes == null || defaultMaterialTypes.size() == 0)
        {
            defaultMaterialTypes = (List<String>)ensxtx_UTIL_AppSettings.getList(ensxtx_UTIL_AppSettings.EnterpriseApp,
                'DefaultMaterialTypes', String.class, new String[] { 'FERT' });
        }
        Set<String> defMaterialTypesSet = new Set<String>(defaultMaterialTypes);

        List<MaterialTypeResult> responseData = new List<MaterialTypeResult>();
        List<ensxtx_RFC_MM_GET_PROD_HIERARCHY.ET_MATERIAL_TYPE> matList = rfcSearchOptions.ET_MATERIAL_TYPE_List;
        Integer matTot = matList.size();
        for (Integer matCnt = 0 ; matCnt < matTot ; matCnt++)
        {
            ensxtx_RFC_MM_GET_PROD_HIERARCHY.ET_MATERIAL_TYPE materialType = matList[matCnt];
            if (defMaterialTypesSet.contains(materialType.MTART))
            {
                responseData.add(new MaterialTypeResult(materialType.MTART, materialType.MTBEZ));
            }
        }

        return ensxtx_UTIL_Aura.createResponse(responseData);      
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response loadProductHierarchies()
    {
        logger.enterAura('loadProductHierarchies', null);

        return ensxtx_UTIL_Aura.createResponse(rfcSearchOptions.ET_PROD_HIERARCHY_List);
    }

    public class MaterialTypeResult
    {
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public String label { get; set; }

        public MaterialTypeResult (String value, String label)
        {
            this.value = value;
            this.label = label;
        }
    }

    private static ensxtx_RFC_MM_GET_PROD_HIERARCHY.RESULT cache_rfcSearchOptions;
    private static ensxtx_RFC_MM_GET_PROD_HIERARCHY.RESULT rfcSearchOptions
    {
        get
        {
            if (null == cache_rfcSearchOptions)
            {
                ensxtx_RFC_MM_GET_PROD_HIERARCHY rfc = new ensxtx_RFC_MM_GET_PROD_HIERARCHY();
                // Only get 3 levels of product hierarchy
                rfc.Params.IV_LEVEL_NUMBER = '3';
                cache_rfcSearchOptions = rfc.execute();

                if (!cache_rfcSearchOptions.isSuccess())
                {
                    ensxtx_UTIL_PageMessages.addFrameworkMessages(cache_rfcSearchOptions.getMessages());
                }
            }
            return cache_rfcSearchOptions;
        }
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getMaterialGroups(String salesOrg, String distChannel, String division)
    {
        logger.enterAura('getMaterialGroups', new Map<String, Object> {
            'salesOrg' => salesOrg,
            'distChannel' => distChannel,
            'division' => division
        });

        ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.RESULT result;
        try
        {
            ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST rfc = new ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST();
            rfc.PARAMS.IV_VKORG = salesOrg;
            rfc.PARAMS.IV_VTWEG = distChannel;
            rfc.PARAMS.IV_SPART = division;
            result = rfc.execute();
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response searchMaterials(Map<String, Object> searchParams, Map<String, Object> pagingOptions)
    {
        logger.enterAura('searchMaterials', new Map<String, Object> {
            'searchParams' => searchParams,
            'pagingOptions' => pagingOptions
        });

        List<MaterialSearchResult> responseData = new List<MaterialSearchResult>();
        Object responsePagingOptions = null;

        try
        {
            ensxtx_SBO_EnosixMaterial_Search sbo = new ensxtx_SBO_EnosixMaterial_Search();
            ensxtx_SBO_EnosixMaterial_Search.EnosixMaterial_SC context = new ensxtx_SBO_EnosixMaterial_Search.EnosixMaterial_SC();

            String materialDescription = (String) searchParams.get('MaterialDescription');
            if (null == materialDescription) materialDescription = '';
            if (!materialDescription.startsWith('*')) materialDescription = '*' + materialDescription;
            if (!materialDescription.endsWith('*')) materialDescription += '*';

            context.SEARCHPARAMS.MaterialDescription = materialDescription;
            String materialNumber = (String) searchParams.get('MaterialNumber');
            if (String.isNotEmpty(materialNumber)) context.SEARCHPARAMS.MaterialNumberFrom = materialNumber.toUpperCase();
            context.SEARCHPARAMS.SalesOrganization = (String) searchParams.get('SalesOrganization');
            context.SEARCHPARAMS.DistributionChannel = (String) searchParams.get('DistributionChannel');
            context.SEARCHPARAMS.ProductHierarchy = (String) searchParams.get('ProductHierarchy');
            context.SEARCHPARAMS.MaterialGroup = (String) searchParams.get('MaterialGroup');
            context.SEARCHPARAMS.KitFlag = (Boolean) searchParams.get('KitFlag');

            Object materialTypeValuesObject = searchParams.get('MaterialTypeValues');
            if (materialTypeValuesObject != null && materialTypeValuesObject instanceof List<Object>)
            {
                List<Object> materialTypeValues = (List<Object>) materialTypeValuesObject;
                Integer matTot = materialTypeValues.size();
                if (matTot > 0)
                {
                    for (Integer matCnt = 0 ; matCnt < matTot ; matCnt++)
                    {
                        Object materialTypeValue = MaterialTypeValues[matCnt];
                        ensxtx_SBO_EnosixMaterial_Search.MATERIAL_TYPE type =
                                new ensxtx_SBO_EnosixMaterial_Search.MATERIAL_TYPE();
                        type.MaterialType = (String) materialTypeValue;
                        context.MATERIAL_TYPE.add(type);
                    }
                }
            }

            ensxtx_UTIL_Aura.setSearchContextPagingOptions(context, pagingOptions);
            sbo.search(context);

            List<ensxtx_SBO_EnosixMaterial_Search.SEARCHRESULT> resultList = context.result.getResults();
            Integer resultTot = resultList.size();
            for (Integer resultCnt = 0 ; resultCnt < resultTot ; resultCnt++) 
            {
                ensxtx_SBO_EnosixMaterial_Search.SEARCHRESULT result = resultList[resultCnt];
                MaterialSearchResult material = new MaterialSearchResult();
                material.material = result.Material;
                material.materialType = result.MaterialType;
                material.materialDescription = result.MaterialDescription;
                material.productHierarchy = result.ProductHierarchy;
                material.productHierarchyDescription = result.ProductHierarchyDescription;
                material.quantity = 1;
                //material.unitOfMeasure = result.SalesUnit;
                // if (String.isEmpty(result.SalesUnit))
                // {
                //     material.unitOfMeasure = result.BaseUnitOfMeasure;
                // }
                material.unitOfMeasure = result.BaseUnitOfMeasure;
                material.scheduleDate = null;
                material.isSelected = false;
                material.isConfigurableMaterial = result.ConfigurableMaterial;
                material.plant = result.plant;
                material.bomUsage = result.BOMUsage;
                material.stock = result.Stock == true ? 'X' : '';
                material.available = result.Available;
                responseData.add(material);
            }   
            responsePagingOptions = context.pagingOptions;
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }

        return ensxtx_UTIL_Aura.createResponse(responseData, responsePagingOptions);      
    }

    public class MaterialSearchResult
    {
        @AuraEnabled public String material { get; set; }
        @AuraEnabled public String materialType { get; set; }
        @AuraEnabled public String materialDescription { get; set; }
        @AuraEnabled public String productHierarchy { get; set; }
        @AuraEnabled public String productHierarchyDescription { get; set; }
        @AuraEnabled public Decimal quantity { get; set; }
        @AuraEnabled public String unitOfMeasure { get; set; }
        @AuraEnabled public Date scheduleDate { get; set; }
        @AuraEnabled public Boolean isSelected { get; set; }
        @AuraEnabled public String isConfigurableMaterial { get; set; }
        @AuraEnabled public String plant { get; set; }
        @AuraEnabled public String bomUsage { get; set; }
        @AuraEnabled public String stock { get; set; }
        @AuraEnabled public Decimal available { get; set; }
    }
}