@isTest
public class TSTU_AllSyncSchedule
{
    @isTest
    public static void test_insertSchedule()
    {
        Set<Schedulable> syncSchedulables = new Set<Schedulable> {
            new UTIL_CustomerSyncSchedule(),
            new UTIL_ContactSyncSchedule(),
            new UTIL_MaterialSyncSchedule(),
            new UTIL_InvoiceSyncSchedule(),
            new UTIL_OrderSyncSchedule(),
            new UTIL_QuoteSyncSchedule()
        };

        Test.startTest();
        for (Schedulable syncInstance : syncSchedulables)
        {
            Datetime dt = Datetime.now().addMinutes(1);
            String timeForScheduler = dt.format('s m H d M \'?\' yyyy ');
            String schedId = System.Schedule(
                String.valueOf(syncInstance).split(':')[0] + 'RetryAfter' + timeForScheduler.trim(),
                timeForScheduler,
                syncInstance
            );
            // ID in API is 15 digits
            System.assertEquals(15, schedId.length());
        }

        Test.stopTest();
    }

    @isTest
    public static void test_insertSchedule2()
    {
        Set<Schedulable> syncSchedulables = new Set<Schedulable> {
            new UTIL_CustomerSyncSchedule(null),
            new UTIL_ContactSyncSchedule(null),
            new UTIL_MaterialSyncSchedule(null),
            new UTIL_InvoiceSyncSchedule(null),
            new UTIL_OrderSyncSchedule(null),
            new UTIL_QuoteSyncSchedule(null)
        };

        Test.startTest();
        for (Schedulable syncInstance : syncSchedulables)
        {
            Datetime dt = Datetime.now().addMinutes(1);
            String timeForScheduler = dt.format('s m H d M \'?\' yyyy ');
            String schedId = System.Schedule(
                String.valueOf(syncInstance).split(':')[0] + 'RetryAfter' + timeForScheduler.trim(),
                timeForScheduler,
                syncInstance
            );
            // ID in API is 15 digits
            System.assertEquals(15, schedId.length());
        }

        Test.stopTest();
    }
}