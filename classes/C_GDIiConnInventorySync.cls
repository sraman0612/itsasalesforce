public class C_GDIiConnInventorySync {
    
    static String gdiUserName;
    static String gdiPassword;
    static String baseURL;


    public static void iConnInventorySync(list<id> AstID){
        String response;

        iConn_Admin__c iconnAdmin = iConn_Admin__c.getOrgDefaults();
        gdiUserName = iconnAdmin.Username__c;
        gdiPassword = iconnAdmin.Password__c;
        baseURL = iconnAdmin.API_URL__c;

        
        try{
            
            List<Asset> ast = [SELECT id, name,IMEI__c FROM Asset WHERE id IN :AstID];
            List<Asset> updateAst = new List<Asset>();
            
            
            for(Asset a:ast){
                
                HTTPRequest r = new HTTPRequest();
                r.setEndpoint(baseURL + '/inventory/managedObjects?fragmentType=c8y_IsDevice&query=c8y_Mobile.imei%20eq%20'+a.IMEI__c);
                r.setMethod('GET');
                
                Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                r.setHeader('authorization', authorizationHeader);
                r.setHeader('Accept', '*/*');
                //r.setHeader('Accept','application/json-stream');
                r.setHeader('Content-Type','application/json');
                
                
                HTTP h = new HTTP();
                HTTPResponse resp = h.send(r);
                response = resp.getBody();
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                
                List<Object> managedObjects = (List<Object>)results.get('managedObjects');
                /*
                 Map<string, object> lstStats = (Map<string, object>)results.get('statistics');//pagination stats
                String lstNext = (String)results.get('next');
                String lstPrev = (String)results.get('prev');
                String lstSelf = (String)results.get('self');
                */
                if(managedObjects.size()>0){
                    Map<String, Object> customerAttributes = (Map<String, Object>)managedObjects[0];
                    // now loop through our customer attributes.            
                    Asset newAst = null;
                    newAst = new Asset();
                    newAst.id = a.id;
                    newAst.Cumulocity_ID__c = long.valueOf((String)customerAttributes.get('id'));
                    /* 
                    newAst.iConn_c8y_H_total__c = integer.valueof(customerAttributes.get('c8y_H_Total')!=null?customerAttributes.get('c8y_H_Total'):'0');
                    newAst.iConn_c8y_H_load__c = integer.valueof(customerAttributes.get('c8y_H_Load')!=null?customerAttributes.get('c8y_H_Load'):'0');
                    newAst.iConn_c8y_H_service__c = integer.valueof(customerAttributes.get('c8y_H_Service')!=null?customerAttributes.get('c8y_H_Service'):'0');
                    newAst.iConn_c8y_H_stal_1__c  = integer.valueof(customerAttributes.get('c8y_H_Stal_1')!=null?customerAttributes.get('c8y_H_Stal_1'):'0');
                    */
                    updateAst.add(newAst);
                }
               
            }
            update updateAst;
        }
        catch(Exception e){
            System.debug('Error: ==>'+e.getMessage());
        }
    }
  
}