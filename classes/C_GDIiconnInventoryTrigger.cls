public class C_GDIiconnInventoryTrigger {
    
    static String gdiUserName;
    static String gdiPassword;
    static String baseURL;

    //@future(callout=true)
    public static void iConnInventorySync(list<id> AstID){
        String response;

        iConn_Admin__c iconnAdmin = iConn_Admin__c.getOrgDefaults();
        gdiUserName = iconnAdmin.Username__c;
        gdiPassword = iconnAdmin.Password__c;
        baseURL = iconnAdmin.API_URL__c;
        
        try{
            
            List<Asset> ast = [SELECT id, name,IMEI__c FROM Asset WHERE id IN :AstID AND IMEI__c!=null];
            List<Asset> updateAst = new List<Asset>();
            
            
            for(Asset a:ast){
                
                HTTPRequest r = new HTTPRequest();
                r.setEndpoint(baseURL + 'inventory/managedObjects?fragmentType=c8y_IsDevice&query=c8y_Mobile.imei%20eq%20'+a.IMEI__c);
                r.setMethod('GET');
                
                Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                r.setHeader('authorization', authorizationHeader);
                r.setHeader('Accept', '*/*');
                //r.setHeader('Accept','application/json-stream');
                r.setHeader('Content-Type','application/json');
                
                
                HTTP h = new HTTP();
                HTTPResponse resp = h.send(r);
                response = resp.getBody();
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                
                List<Object> managedObjects = (List<Object>)results.get('managedObjects');
                
                if(managedObjects.size()>0){
                    Map<String, Object> customerAttributes = (Map<String, Object>)managedObjects[0];
                    // now loop through our customer attributes.            
                    Asset newAst = null;
                    newAst = new Asset();
                    newAst.id = a.id;
                    newAst.Cumulocity_ID__c = long.valueof((String)customerAttributes.get('id'));
                    updateAst.add(newAst);
                }
                
            }
            update updateAst;
        }
        catch(Exception e){
            System.debug('Error: ==>'+e.getMessage());
        }
    }
}