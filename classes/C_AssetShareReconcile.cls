global class C_AssetShareReconcile implements Database.Batchable<sObject>{ 
    global final String Query;
    
    global C_AssetShareReconcile(){
        
        Query='select id, AccountId, Parent_AccountID__c,Current_Servicer_ID__c, Current_Servicer__r.parentID, Current_Servicer_Parent_ID__c From Asset';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }   
    
    
    global void execute(Database.BatchableContext info, List<asset> scope){
        List<AssetShare> assetShareToCreate = new List<AssetShare>();
        
        for(Asset a : scope)
        {
            //Create sharing for all Single account users
            if(!string.isBlank(a.AccountId)){
                for(User u : [Select id,Partner_Account_ID__c from user 
                              where Partner_Account_ID__c = :string.valueOf(a.AccountId).substring(0,15)
                              and isActive = true])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
            
            //Create sharing for Global Account users
            if(!string.isBlank(a.Parent_AccountID__c)){
                for(User u : [Select id, Partner_Account_Parent_ID__c from user 
                              where toLabel(community_user_type__c) = 'Global Account' 
                              and Partner_Account_Parent_ID__c  = :string.valueOf(a.Parent_AccountID__c).substring(0,15)
                              and isActive = true])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
            
            //Create sharing for users who have the Current Servicer ID and their Parent Account ID
            if(!string.isBlank(a.Current_Servicer_ID__c)){
                for(User u : [Select id, Partner_Account_ID__c from user 
                              where Partner_Account_ID__c = :string.valueOf(a.Current_Servicer_ID__c ).substring(0,15)
                              and isActive = true])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
            
            if(!string.isBlank(a.Current_Servicer_Parent_ID__c)){
                for(User u : [Select id, Partner_Account_Parent_ID__c from user 
                              where toLabel(community_user_type__c) = 'Global Account' 
                              and Partner_Account_Parent_ID__c  = :string.valueOf(a.Current_Servicer_Parent_ID__c).substring(0,15)
                              and isActive = true])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
            
        }
        //Insert sharing after loop
        insert assetShareToCreate;
    }     
    global void finish(Database.BatchableContext info){     
    } 
}