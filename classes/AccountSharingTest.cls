@isTest
public class AccountSharingTest {
    @isTest
    public static void testUserAccountSharing() {
        createData('1');
        
        User u = [SELECT Id, AccountId, ProfileId from User where LastName = 'GlobalUser' LIMIT 1];
        
        Profile salesProfile = [SELECT Id FROM Profile where name = 'Partner Community - Sales Rep' Limit 1];

        Profile globalProfile = [SELECT Id FROM Profile where name =: AccountSharingHelper.STR_SALES_REP_ACCOUNT_USER_PROFILE_NAME Limit 1];

        u.ProfileId = salesProfile.Id;
        
        update u;
        
        Test.startTest();
        
        u.profileId = globalProfile.Id;
        update u;
        
        Test.stopTest();
    }
    
    @isTest
    public static void testAccountAccountSharing() {
        createData('2');
        
        User u = [SELECT Id from User where LastName = 'Wayne' LIMIT 1];
        
        Account repAccount = [SELECT Id from Account where Name = 'RepAccount'];
        Account portalAccount = [SELECT Id, Rep_Account2__c from Account where Name = 'PortalAccount'];
        
        System.runAs(u) {
            portalAccount.Rep_Account2__c = null;
            
            update portalAccount;
            
            Test.startTest();
            
            portalAccount.Rep_Account2__c = repAccount.Id;
            
            update portalAccount;
            
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testBatch() {
        createData('3');
        
        Test.startTest();
        
        DataBase.executeBatch(new AccountSharingBatch());
        
        Test.stopTest();
    }
    
    private static void createData(String unique) {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = 'testerson' + System.now().millisecond() + '@test' + unique + '.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        
        System.runAs ( portalAccountOwner1 ) {
            Account otherAccount = new Account(
                Name = 'otherAccount',
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(otherAccount);
            Account repAccount = new Account(
                Name = 'RepAccount',
                OwnerId = portalAccountOwner1.Id,
                Associated_Rep_Account__c = otherAccount.Id
            );
            Database.insert(repAccount);
            
            Account portalAccount = new Account(
                Name = 'PortalAccount',
                OwnerId = portalAccountOwner1.Id,
                Rep_Account2__c = repAccount.Id
            );
            Database.insert(portalAccount);

            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'GlobalUser',
                AccountId = repAccount.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
            Database.insert(contact1);
            
            Profile portalProfile = [SELECT Id FROM Profile where name =: AccountSharingHelper.STR_SALES_REP_ACCOUNT_USER_PROFILE_NAME Limit 1];
            User user1 = new User(
                Username = System.now().millisecond() + 'test@test' + unique + '.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'GlobalUser',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
        }
    }
}