/**
 * Created by OlhaHulenko on 12/10/2023.
 */

@IsTest
private class shouldIRunTest {
    @IsTest
    static void stopTriggerTest() {

        Test.startTest();
        shouldIRun.stopTrigger();
        Test.stopTest();

    }

    @IsTest
    static void canIRunTest() {
        Boolean isCanIRun = false;
        Test.startTest();
            isCanIRun = shouldIRun.canIRun();
        Test.stopTest();

        System.assertEquals(true,isCanIRun);

    }
}