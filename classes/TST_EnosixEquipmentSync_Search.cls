/// enosiX Inc. Generated Apex Model
/// Generated On: 8/8/2019 10:24:07 AM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class TST_EnosixEquipmentSync_Search
{

    public class MockSBO_EnosixEquipmentSync_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixEquipmentSync_Search.class, new MockSBO_EnosixEquipmentSync_Search());
        SBO_EnosixEquipmentSync_Search sbo = new SBO_EnosixEquipmentSync_Search();
        System.assertEquals(SBO_EnosixEquipmentSync_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC sc = new SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC();
        System.assertEquals(SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        SBO_EnosixEquipmentSync_Search.SEARCHPARAMS childObj = new SBO_EnosixEquipmentSync_Search.SEARCHPARAMS();
        System.assertEquals(SBO_EnosixEquipmentSync_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.DateFrom = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateFrom);

        childObj.DateTo = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateTo);

        childObj.InitialLoadFlag = true;
        System.assertEquals(true, childObj.InitialLoadFlag);


    }

    @isTest
    static void testEnosixEquipmentSync_SR()
    {
        SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR sr = new SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR();

        sr.registerReflectionForClass();

        System.assertEquals(SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        SBO_EnosixEquipmentSync_Search.SEARCHRESULT childObj = new SBO_EnosixEquipmentSync_Search.SEARCHRESULT();
        System.assertEquals(SBO_EnosixEquipmentSync_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixEquipmentSync_Search.SEARCHRESULT_COLLECTION childObjCollection = new SBO_EnosixEquipmentSync_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.EquipmentNumber = 'X';
        System.assertEquals('X', childObj.EquipmentNumber);

        childObj.EquipmentNumberDescription = 'X';
        System.assertEquals('X', childObj.EquipmentNumberDescription);

        childObj.Name = 'X';
        System.assertEquals('X', childObj.Name);

        childObj.Street = 'X';
        System.assertEquals('X', childObj.Street);

        childObj.City = 'X';
        System.assertEquals('X', childObj.City);

        childObj.Region = 'X';
        System.assertEquals('X', childObj.Region);

        childObj.Country = 'X';
        System.assertEquals('X', childObj.Country);

        childObj.PostalCode = 'X';
        System.assertEquals('X', childObj.PostalCode);

        childObj.TelephoneNumber = 'X';
        System.assertEquals('X', childObj.TelephoneNumber);

        childObj.TechnicalObjectStartupDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.TechnicalObjectStartupDate);

        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.SalesOrgDescription = 'X';
        System.assertEquals('X', childObj.SalesOrgDescription);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);

        childObj.DistributionChannelText = 'X';
        System.assertEquals('X', childObj.DistributionChannelText);

        childObj.Division = 'X';
        System.assertEquals('X', childObj.Division);

        childObj.DivisionDescription = 'X';
        System.assertEquals('X', childObj.DivisionDescription);

        childObj.Material = 'X';
        System.assertEquals('X', childObj.Material);

        childObj.MaterialDescription = 'X';
        System.assertEquals('X', childObj.MaterialDescription);

        childObj.Materialgroup4 = 'X';
        System.assertEquals('X', childObj.Materialgroup4);

        childObj.ProductHierarchy = 'X';
        System.assertEquals('X', childObj.ProductHierarchy);

        childObj.SerialNumber = 'X';
        System.assertEquals('X', childObj.SerialNumber);

        childObj.MasterWarrantyNumber = 'X';
        System.assertEquals('X', childObj.MasterWarrantyNumber);

        childObj.MasterWarrantyText = 'X';
        System.assertEquals('X', childObj.MasterWarrantyText);

        childObj.StroreNo = 'X';
        System.assertEquals('X', childObj.StroreNo);

        childObj.DeliveryDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DeliveryDate);

        childObj.UserStatus = 'X';
        System.assertEquals('X', childObj.UserStatus);

        childObj.UserStatusDescription = 'X';
        System.assertEquals('X', childObj.UserStatusDescription);

        childObj.SystemStatus1 = 'X';
        System.assertEquals('X', childObj.SystemStatus1);

        childObj.SystemStatus1Description = 'X';
        System.assertEquals('X', childObj.SystemStatus1Description);

        childObj.SystemStatus2 = 'X';
        System.assertEquals('X', childObj.SystemStatus2);

        childObj.SystemStatus2Description = 'X';
        System.assertEquals('X', childObj.SystemStatus2Description);

        childObj.SystemStatus3 = 'X';
        System.assertEquals('X', childObj.SystemStatus3);

        childObj.SystemStatus3Description = 'X';
        System.assertEquals('X', childObj.SystemStatus3Description);

        childObj.SystemStatus4 = 'X';
        System.assertEquals('X', childObj.SystemStatus4);

        childObj.SystemStatus4Description = 'X';
        System.assertEquals('X', childObj.SystemStatus4Description);

        childObj.SystemStatus5 = 'X';
        System.assertEquals('X', childObj.SystemStatus5);

        childObj.SystemStatus5Description = 'X';
        System.assertEquals('X', childObj.SystemStatus5Description);

        childObj.characteristic1 = 'X';
        System.assertEquals('X', childObj.characteristic1);

        childObj.characteristic1Val = 'X';
        System.assertEquals('X', childObj.characteristic1Val);

        childObj.ValText1 = 'X';
        System.assertEquals('X', childObj.ValText1);

        childObj.characteristic2 = 'X';
        System.assertEquals('X', childObj.characteristic2);

        childObj.characteristic2Val = 'X';
        System.assertEquals('X', childObj.characteristic2Val);

        childObj.ValText2 = 'X';
        System.assertEquals('X', childObj.ValText2);

        childObj.characteristic3 = 'X';
        System.assertEquals('X', childObj.characteristic3);

        childObj.characteristic3Val = 'X';
        System.assertEquals('X', childObj.characteristic3Val);

        childObj.ValText3 = 'X';
        System.assertEquals('X', childObj.ValText3);

        childObj.characteristic4 = 'X';
        System.assertEquals('X', childObj.characteristic4);

        childObj.characteristic4Val = 'X';
        System.assertEquals('X', childObj.characteristic4Val);

        childObj.ValText4 = 'X';
        System.assertEquals('X', childObj.ValText4);

        childObj.characteristic5 = 'X';
        System.assertEquals('X', childObj.characteristic5);

        childObj.characteristic5Val = 'X';
        System.assertEquals('X', childObj.characteristic5Val);

        childObj.ValText5 = 'X';
        System.assertEquals('X', childObj.ValText5);

        childObj.characteristic6 = 'X';
        System.assertEquals('X', childObj.characteristic6);

        childObj.characteristic6Val = 'X';
        System.assertEquals('X', childObj.characteristic6Val);

        childObj.ValText6 = 'X';
        System.assertEquals('X', childObj.ValText6);

        childObj.characteristic7 = 'X';
        System.assertEquals('X', childObj.characteristic7);

        childObj.characteristic7Val = 'X';
        System.assertEquals('X', childObj.characteristic7Val);

        childObj.ValText7 = 'X';
        System.assertEquals('X', childObj.ValText7);

        childObj.characteristic8 = 'X';
        System.assertEquals('X', childObj.characteristic8);

        childObj.characteristic8Val = 'X';
        System.assertEquals('X', childObj.characteristic8Val);

        childObj.ValText8 = 'X';
        System.assertEquals('X', childObj.ValText8);

        childObj.characteristic9 = 'X';
        System.assertEquals('X', childObj.characteristic9);

        childObj.characteristic9Val = 'X';
        System.assertEquals('X', childObj.characteristic9Val);

        childObj.ValText9 = 'X';
        System.assertEquals('X', childObj.ValText9);

        childObj.characteristic10 = 'X';
        System.assertEquals('X', childObj.characteristic10);

        childObj.characteristic10Val = 'X';
        System.assertEquals('X', childObj.characteristic10Val);

        childObj.ValText10 = 'X';
        System.assertEquals('X', childObj.ValText10);

        childObj.characteristic11 = 'X';
        System.assertEquals('X', childObj.characteristic11);

        childObj.characteristic11Val = 'X';
        System.assertEquals('X', childObj.characteristic11Val);

        childObj.ValText11 = 'X';
        System.assertEquals('X', childObj.ValText11);

        childObj.characteristic12 = 'X';
        System.assertEquals('X', childObj.characteristic12);

        childObj.characteristic12Val = 'X';
        System.assertEquals('X', childObj.characteristic12Val);

        childObj.ValText12 = 'X';
        System.assertEquals('X', childObj.ValText12);

        childObj.characteristic13 = 'X';
        System.assertEquals('X', childObj.characteristic13);

        childObj.characteristic13Val = 'X';
        System.assertEquals('X', childObj.characteristic13Val);

        childObj.ValText13 = 'X';
        System.assertEquals('X', childObj.ValText13);

        childObj.characteristic14 = 'X';
        System.assertEquals('X', childObj.characteristic14);

        childObj.characteristic14Val = 'X';
        System.assertEquals('X', childObj.characteristic14Val);

        childObj.ValText14 = 'X';
        System.assertEquals('X', childObj.ValText14);

        childObj.characteristic15 = 'X';
        System.assertEquals('X', childObj.characteristic15);

        childObj.characteristic15Val = 'X';
        System.assertEquals('X', childObj.characteristic15Val);

        childObj.ValText15 = 'X';
        System.assertEquals('X', childObj.ValText15);

        childObj.characteristic16 = 'X';
        System.assertEquals('X', childObj.characteristic16);

        childObj.characteristic16Val = 'X';
        System.assertEquals('X', childObj.characteristic16Val);

        childObj.ValText16 = 'X';
        System.assertEquals('X', childObj.ValText16);

        childObj.characteristic17 = 'X';
        System.assertEquals('X', childObj.characteristic17);

        childObj.characteristic17Val = 'X';
        System.assertEquals('X', childObj.characteristic17Val);

        childObj.ValText17 = 'X';
        System.assertEquals('X', childObj.ValText17);

        childObj.characteristic18 = 'X';
        System.assertEquals('X', childObj.characteristic18);

        childObj.characteristic18Val = 'X';
        System.assertEquals('X', childObj.characteristic18Val);

        childObj.ValText18 = 'X';
        System.assertEquals('X', childObj.ValText18);

        childObj.characteristic19 = 'X';
        System.assertEquals('X', childObj.characteristic19);

        childObj.characteristic19Val = 'X';
        System.assertEquals('X', childObj.characteristic19Val);

        childObj.ValText19 = 'X';
        System.assertEquals('X', childObj.ValText19);

        childObj.characteristic20 = 'X';
        System.assertEquals('X', childObj.characteristic20);

        childObj.characteristic20Val = 'X';
        System.assertEquals('X', childObj.characteristic20Val);

        childObj.ValText20 = 'X';
        System.assertEquals('X', childObj.ValText20);

        childObj.characteristic21 = 'X';
        System.assertEquals('X', childObj.characteristic21);

        childObj.characteristic21Val = 'X';
        System.assertEquals('X', childObj.characteristic21Val);

        childObj.ValText21 = 'X';
        System.assertEquals('X', childObj.ValText21);

        childObj.characteristic22 = 'X';
        System.assertEquals('X', childObj.characteristic22);

        childObj.characteristic22Val = 'X';
        System.assertEquals('X', childObj.characteristic22Val);

        childObj.ValText22 = 'X';
        System.assertEquals('X', childObj.ValText22);

        childObj.characteristic23 = 'X';
        System.assertEquals('X', childObj.characteristic23);

        childObj.characteristic23Val = 'X';
        System.assertEquals('X', childObj.characteristic23Val);

        childObj.ValText23 = 'X';
        System.assertEquals('X', childObj.ValText23);

        childObj.characteristic24 = 'X';
        System.assertEquals('X', childObj.characteristic24);

        childObj.characteristic24Val = 'X';
        System.assertEquals('X', childObj.characteristic24Val);

        childObj.ValText24 = 'X';
        System.assertEquals('X', childObj.ValText24);

        childObj.characteristic25 = 'X';
        System.assertEquals('X', childObj.characteristic25);

        childObj.characteristic25Val = 'X';
        System.assertEquals('X', childObj.characteristic25Val);

        childObj.ValText25 = 'X';
        System.assertEquals('X', childObj.ValText25);

        childObj.PartnerFunction = 'X';
        System.assertEquals('X', childObj.PartnerFunction);

        childObj.PartnerFunctionDescription = 'X';
        System.assertEquals('X', childObj.PartnerFunctionDescription);

        childObj.SoldToPartnerNumber = 'X';
        System.assertEquals('X', childObj.SoldToPartnerNumber);

        childObj.SoldToName = 'X';
        System.assertEquals('X', childObj.SoldToName);

        childObj.MODEL_NUMBER_C = 'X';
        System.assertEquals('X', childObj.MODEL_NUMBER_C);

        childObj.MODEL_C = 'X';
        System.assertEquals('X', childObj.MODEL_C);

        childObj.SERNR_MODNR = 'X';
        System.assertEquals('X', childObj.SERNR_MODNR);

        childObj.DateChanged = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateChanged);

        childObj.IMEISerialNumber = 'X';
        System.assertEquals('X', childObj.IMEISerialNumber);

        childObj.SalesOrderNumber = 'X';
        System.assertEquals('X', childObj.SalesOrderNumber);


    }

}