global class C_SerialNumAccUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {
    global Integer recordsProcessed = 0;
    global String query = ''; 
    
    global C_SerialNumAccUpdateBatch(String freq)
    {
        // D for daily, W for weekend
        if(freq=='W')
        {
            query = 'SELECT Id, Name, DChl__c, Account.Name, Account.Account_Number__c, ' +
                'Current_Servicer__r.Name, Current_Servicer__r.Account_Number__c ' +
                'FROM Asset where DChl__c != null ' +
                'AND (AccountId != null OR Current_Servicer__c != null)';
        }
        else if (freq=='D')
        {
            query = 'SELECT Id, Name, DChl__c, Account.Name, Account.Account_Number__c, ' +
                'Current_Servicer__r.Name, Current_Servicer__r.Account_Number__c ' +
                'FROM Asset where DChl__c != null AND Process_TSM_Batch__c = true ' +
                'AND (AccountId != null OR Current_Servicer__c != null)';
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {   
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Asset> scope){
        // map to lookup sales district code to user id
        Map<String,Id> mapCodeUser = new Map<String,Id>();
        for(User u : [select Id, Name, Sales_District_Code__c
                      from User
                      where Sales_District_Code__c!=null])
        {
            mapCodeUser.put(u.Sales_District_Code__c, u.Id);
        }
        
        // map to lookup sap-provided account number to sales district
        Map<String, String> mapAccnumSalesDistrict = new Map<String, String>();
        for(Account a : [select Id, Name, Account_Number__c, Sales_district__c
                         from Account
                         where Account_Number__c!=null
                         and Sales_district__c!=null])
        {
            String tempSdCode = (String)a.Sales_district__c;
            String sdCode = tempSdCode.substringBefore(' ');
            
            mapAccnumSalesDistrict.put(a.Account_Number__c, sdCode);
        }
        
        for(Asset s : scope)
        {
            if(s.Account!=null)
            {
                String pseudoSapAcctAccNum = (String)s.Account.Account_Number__c + ' ' + '-' + ' ' + s.DChl__c;
                String userCode = mapAccnumSalesDistrict.get(pseudoSapAcctAccNum);
                s.Serial_Number_Owner__c = mapCodeUser.get(userCode);
            }
            if(s.Current_Servicer__c!=null)
            {
                String pseudoSapCurrAccNum = (String)s.Current_Servicer__r.Account_Number__c + ' ' + '-' + ' ' + s.DChl__c;
                String userCode = mapAccnumSalesDistrict.get(pseudoSapCurrAccNum);
                s.Current_Servicer_Owner__c = mapCodeUser.get(userCode);
            }
            s.Process_TSM_Batch__c=false;
        }
        Database.update(scope,false);
    }    
    
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed.');
    }
}