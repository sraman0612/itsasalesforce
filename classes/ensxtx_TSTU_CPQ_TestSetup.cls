@isTest public with sharing class ensxtx_TSTU_CPQ_TestSetup {

    public static Account createAccount(String customerNumber) {
        Account account = ensxtx_TSTU_SFTestObject.createTestAccount();
        account.Name = 'Mock Account';
        account.AccountNumber = customerNumber;
        account.BillingCity = 'Toledo';
        account.BillingStreet = '1234 Test St';
        account.BillingState = 'Ohio';
        // account.BillingStateCode = 'OH';
        account.BillingPostalCode = '43613';
        account.BillingCountry = 'United States';
        // account.BillingCountryCode = 'US';
        account.put(ensxtx_UTIL_SFAccount.CustomerFieldName, customerNumber);
        ensxtx_TSTU_SFTestObject.upsertWithRetry(account);

        return account;
    }

    public static SBQQ__Quote__c createQuote() {
        SBQQ.TriggerControl.disable();
        SBQQ__Quote__c quote = ensxtx_TSTU_SFCPQQuote.createTestQuote();
        quote.SBQQ__Key__c = 'Mock Quote';
        quote.FLD_Ship_To_Number__c = 'ShipTo';
        ensxtx_TSTU_SFTestObject.upsertWithRetry(quote);
        
        Product2 product = createProduct2();
        
        SBQQ__QuoteLine__c quoteLine = ensxtx_TSTU_SFCPQQuote.createTestQuoteLine();
        quoteLine.SBQQ__Quote__c = quote.Id;
        quoteLine.SBQQ__Product__c = product.Id;
        quoteLine.SBQQ__Quantity__c = 1;
        quoteLine.SBQQ__ListPrice__c = 20.00;
        quoteLine.SBQQ__Description__c = 'TEST DESC';
        ensxtx_TSTU_SFTestObject.upsertWithRetry(quoteLine);

        return quote;
    }

    public static SBQQ__QuoteLine__c createQuoteLine() {
        SBQQ.TriggerControl.disable();
        SBQQ__Quote__c quote = ensxtx_TSTU_SFCPQQuote.createTestQuote();
        quote.SBQQ__Key__c = 'Mock Quote';
        ensxtx_TSTU_SFTestObject.upsertWithRetry(quote);
        
        Product2 product = createProduct2();
        
        SBQQ__QuoteLine__c quoteLine = ensxtx_TSTU_SFCPQQuote.createTestQuoteLine();
        quoteLine.SBQQ__Quote__c = quote.Id;
        quoteLine.SBQQ__Product__c = product.Id;
        quoteLine.SBQQ__Quantity__c = 1;
        quoteLine.SBQQ__ListPrice__c = 20.00;
        quoteLine.SBQQ__Description__c = 'TEST DESC';
        ensxtx_TSTU_SFTestObject.upsertWithRetry(quoteLine);
        ensxtx_TSTU_SFTestObject.upsertWithRetry(quote);

        return quoteLine;
    }

    public static List<SObject> createAccountQuoteLinked() {
        Account acct = ensxtx_TSTU_SFTestObject.createTestAccount();
        acct.put(ensxtx_UTIL_SFAccount.CustomerFieldName,'CustNum');
        ensxtx_TSTU_SFTestObject.upsertWithRetry(acct);

        Id pricebookId = ensxtx_UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = new Opportunity();
        opp.Name = 'TEST OP';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.AccountId = acct.Id;
        opp.Pricebook2Id = pricebookId;

        ensxtx_TSTU_SFTestObject.upsertWithRetry(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        ensxtx_TSTU_SFTestObject.upsertWithRetry(opp);
        
        SBQQ__Quote__c quote = ensxtx_TSTU_SFCPQQuote.createTestQuote();
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__Account__c = acct.Id;
        quote.FLD_Distribution_Channel__c = 'CM';
        quote.End_Customer_Account__c = acct.Id;
        quote.SBQQ__Primary__c = true;
        ensxtx_TSTU_SFTestObject.upsertWithRetry(quote);

        return new List<SObject> { acct, quote };
    }

    public static Product2 createProduct2() {
        Product2 product = ensxtx_TSTU_SFTestObject.createTestProduct2();
        ensxtx_TSTU_SFTestObject.upsertWithRetry(product);
        return product;
    }

    public static Address createAddress() {
        Account a = createAccount('TEST');
        a = [SELECT BillingAddress FROM Account WHERE Id = :a.Id];
        return (Address)a.get('BillingAddress');
    }

}