public class QuoteTriggerHandler extends CPQTriggerHandler {

    public QuoteTriggerHandler() {}

    /* context overrides */
    
    protected override void beforeUpdate() {

        manageProcessInputs(Trigger.new);

        // If Approval Status changes, change Record Id accordingly
        for (SBQQ__Quote__c quote : (List<SBQQ__Quote__c>) Trigger.new) {
            SBQQ__Quote__c oldQuote = (SBQQ__Quote__c) Trigger.oldMap.get(quote.Id);
            
            // Recalled or Rejected, update to Quote - Draft
            if ( quote.ApprovalStatus__c != oldQuote.ApprovalStatus__c && ( quote.ApprovalStatus__c == 'Rejected' || quote.ApprovalStatus__c == 'Recalled' ) )
            {
                try {
                    quote.RecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Quote - Draft').getRecordTypeId();
                    quote.SBQQ__Status__c = 'Draft';
                }
                catch (Exception e) {}
            }
            
             // Updated to Quote Validated with no Validation errors
            if ( quote.SBQQ__Status__c != oldQuote.SBQQ__Status__c && quote.SBQQ__Status__c == 'Quote Validated' )
            {
                try {
                    quote.RecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Quote - Validated').getRecordTypeId();
                }
                catch (Exception e) {}
            }
            
            // Updated to Draft from Quote Validated
            if ( oldQuote.SBQQ__Status__c == 'Quote Validated' && quote.SBQQ__Status__c == 'Draft' )
            {
                try {
                    quote.RecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Quote - Draft').getRecordTypeId();
                }
                catch (Exception e) {}
            }
                        
            // Submitted, update to Quote - In Review
            else if ( quote.ApprovalStatus__c != oldQuote.ApprovalStatus__c && quote.ApprovalStatus__c == 'Pending' )
            {
                try {
                    quote.RecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Quote - In Review').getRecordTypeId();
                    quote.SBQQ__Status__c = 'In Review';
                }
                catch (Exception e) {}
            }
            // Submitted, update to Quote - Approved
            else if ( quote.ApprovalStatus__c != oldQuote.ApprovalStatus__c && quote.ApprovalStatus__c == 'Approved' )
            {
                try {
                    quote.RecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Quote - Approved').getRecordTypeId();
                    quote.SBQQ__Status__c = 'Approved';
                    quote.SBQQ__WatermarkShown__c = false;
                }
                catch (Exception e) {}
            }
            
        }
    }
    
    /*
    
    protected override void beforeInsert() {

    }
    
    protected override void beforeDelete() {
    
    }
    */
    protected override void afterInsert() {

        manageProcessInputs(Trigger.new);
        // exportToFPX(Trigger.newMap.keySet());
    }
    
    protected override void afterUpdate() {

        manageProcessInputs(Trigger.new);
    }
    /*
    protected override void afterDelete() {
    
    }
    */
    
    /* // Removed per request of Nathan Holhaus on 8/2/21
    private static void exportToFPX(Set<Id> quoteIds) {
        // Call a queuable class to allow callouts from trigger
    	System.enqueueJob(new atg_QuoteExportQueueable(quoteIds));    
    }
	*/

    static void manageProcessInputs(List<SBQQ__Quote__c> triggerNew){

        List<SBQQ__ProcessInputValue__c> oldInputValues = new List<SBQQ__ProcessInputValue__c>([SELECT Id FROM SBQQ__ProcessInputValue__c WHERE SBQQ__QuoteId__c IN :triggerNew]);
        List<SBQQ__ProcessInputValue__c> newInputValues = new List<SBQQ__ProcessInputValue__c>();

        for (SBQQ__Quote__c quote : triggerNew) {


            newInputValues.add(new SBQQ__ProcessInputValue__c(
                SBQQ__ProcessInputID__c = Label.Distribution_Channel_Process_Input,
                SBQQ__QuoteId__c = quote.Id,
                SBQQ__Value__c = quote.Sales_Channel_userfriendly__c
            ));

            if (quote.Count_of_Compressors__c == 0) {

                newInputValues.add(new SBQQ__ProcessInputValue__c(
                    SBQQ__ProcessInputID__c = Label.Dryer_Process_Input,
                    SBQQ__QuoteId__c = quote.Id,
                    SBQQ__Value__c = Label.Machine_Definition
                ));
            }
            else if (quote.Count_of_Compressors__c > 0 && quote.Count_of_Dryers__c == 0 && !quote.Dismiss_Dryer_Alert__c) {

                newInputValues.add(new SBQQ__ProcessInputValue__c(
                    SBQQ__ProcessInputID__c = Label.Dryer_Process_Input,
                    SBQQ__QuoteId__c = quote.Id,
                    SBQQ__Value__c = Label.Dryer_Definition
                ));

                newInputValues.add(new SBQQ__ProcessInputValue__c(
                    SBQQ__ProcessInputID__c = Label.Dryer_Min_CFM_Process_Input,
                    SBQQ__QuoteId__c = quote.Id,
                    SBQQ__Value__c = String.valueOf(quote.Min_Dryer_Flow_guided_selling__c)
                ));

                newInputValues.add(new SBQQ__ProcessInputValue__c(
                    SBQQ__ProcessInputID__c = Label.Dryer_Max_CFM_Process_Input,
                    SBQQ__QuoteId__c = quote.Id,
                    SBQQ__Value__c = String.valueOf(quote.Max_Dryer_Flow_guided_selling__c)
                ));

                newInputValues.add(new SBQQ__ProcessInputValue__c(
                    SBQQ__ProcessInputID__c = Label.Dryer_Process_Input_Preferred,
                    SBQQ__QuoteId__c = quote.Id,
                    SBQQ__Value__c = Label.Dryer_Preferred_Value
                ));
            }

            else if (quote.Count_of_Compressors__c > 0 && quote.Count_of_Filters__c == 0 && !quote.Dismiss_Filter_Alert__c) {

                newInputValues.add(new SBQQ__ProcessInputValue__c(
                    SBQQ__ProcessInputID__c = Label.Dryer_Process_Input,
                    SBQQ__QuoteId__c = quote.Id,
                    SBQQ__Value__c = Label.Filter_Definition
                ));
            }
            else if (quote.Count_of_Compressors__c > 0 && quote.Count_of_Piping__c == 0 && !quote.Dismiss_Piping_Alert__c) {

                newInputValues.add(new SBQQ__ProcessInputValue__c(
                    SBQQ__ProcessInputID__c = Label.Dryer_Process_Input,
                    SBQQ__QuoteId__c = quote.Id,
                    SBQQ__Value__c = Label.Piping_Definition
                ));
            }
            else if (quote.Count_of_Compressors__c > 0 && quote.Count_of_OWSs__c == 0 && !quote.Dismiss_Oil_Water_Separator_Alert__c) {

                newInputValues.add(new SBQQ__ProcessInputValue__c(
                    SBQQ__ProcessInputID__c = Label.Dryer_Process_Input,
                    SBQQ__QuoteId__c = quote.id,
                    SBQQ__Value__c = Label.Oil_Water_Separator_Definition
                ));
            }
        }

        if(!newInputValues.isEmpty()){
            insert newInputValues;
        }
        
        if (!oldInputValues.isEmpty()) {
            delete oldInputValues;
        }
    }
}