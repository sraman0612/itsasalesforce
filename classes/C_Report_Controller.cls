public class C_Report_Controller {
    
    @auraEnabled
    public static List<Folder> getAvailableFolders(){
        return [SELECT Id, DeveloperName, Name FROM Folder WHERE Type = 'Report' LIMIT 20];
    }

    @auraEnabled
    public static List<reportWrap> getWrappers(){
        List<reportWrap> result = new List<reportWrap>();
        List<Folder> theFolders = [SELECT Id, DeveloperName, Name FROM Folder WHERE Type = 'Report' LIMIT 20];
        for(Folder f : theFolders){
            List<Report> reports = [SELECT Id, Name, Description FROM Report WHERE FolderName = :f.Name ORDER BY Name ASC LIMIT 20];
            result.add(new reportWrap(f,reports));
        }
        return result;
    }
    
    
    public class reportWrap{
        @auraEnabled
        public Folder theFolder {get; set;}
        @auraEnabled
        List<Report> theReports {get; set;}
        
        public reportWrap(Folder f, List<Report> r){
            this.theFolder = f;
            this.theReports = r;
        }
    }
    
}