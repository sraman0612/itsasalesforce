public class C_ServHistTrigHelperPartsCount
{
    public static void processPartCounts(List<Service_History__c> servs)
    {
        List<Service_History__c> servsToUpdate = new List<Service_History__c>();
        
        for(Service_History__c sh : servs)
        {
            Boolean updated = false;
            if(sh.Oil_Filter__c !=null && sh.Oil_Filter__c!='')
            {
                Service_History__c tmpSH = parseParts(sh, 'Oil_Filter__c', 'Lubricant_Filter_Installed__c', 'Lubricant_Filter_Left_For_Inventory__c' );
                sh.Lubricant_Filter_Installed__c = tmpSH.Lubricant_Filter_Installed__c;
                sh.Lubricant_Filter_Left_For_Inventory__c = tmpSH.Lubricant_Filter_Left_For_Inventory__c;
                updated = true;
            }
            else
            {
                sh.Lubricant_Filter_Installed__c='';
                sh.Lubricant_Filter_Left_For_Inventory__c='';
            }
            if(sh.Air_Filter__c !=null && sh.Air_Filter__c!='')
            {
                Service_History__c tmpSH = parseParts(sh, 'Air_Filter__c', 'Air_Filter_Installed__c', 'Air_Filter_Left_For_Inventory__c' );
                sh.Air_Filter_Installed__c = tmpSH.Air_Filter_Installed__c;
                sh.Air_Filter_Left_For_Inventory__c = tmpSH.Air_Filter_Left_For_Inventory__c;
                updated = true;
            }
            else
            {
                sh.Air_Filter_Installed__c='';
                sh.Air_Filter_Left_For_Inventory__c='';
            }
            if(sh.Separator__c !=null && sh.Separator__c!='')
            {
                Service_History__c tmpSH = parseParts(sh, 'Separator__c', 'Separator_Installed__c', 'Separator_Left_For_Inventory__c' );
                sh.Separator_Installed__c = tmpSH.Separator_Installed__c;
                sh.Separator_Left_For_Inventory__c = tmpSH.Separator_Left_For_Inventory__c;
                updated = true;
            }
            else
            {
                sh.Separator_Installed__c='';
                sh.Separator_Left_For_Inventory__c='';
            }
            if(sh.Cabinet_Filter__c !=null && sh.Cabinet_Filter__c!='')
            {
                Service_History__c tmpSH = parseParts(sh, 'Cabinet_Filter__c', 'Cabinet_Filter_Installed__c', 'Cabinet_Filter_Left_For_Inventory__c' );
                sh.Cabinet_Filter_Installed__c = tmpSH.Cabinet_Filter_Installed__c;
                sh.Cabinet_Filter_Left_For_Inventory__c = tmpSH.Cabinet_Filter_Left_For_Inventory__c;
                updated = true;
            }
            else
            {
                sh.Cabinet_Filter_Installed__c='';
                sh.Cabinet_Filter_Left_For_Inventory__c='';
            }
            if(sh.Control_Box_Filter__c !=null && sh.Control_Box_Filter__c!='')
            {
                Service_History__c tmpSH = parseParts(sh, 'Control_Box_Filter__c', 'Control_Box_Filter_Installed__c', 'Control_Box_Filter_Left_for_Inventory__c' );
                sh.Control_Box_Filter_Installed__c = tmpSH.Control_Box_Filter_Installed__c;
                sh.Control_Box_Filter_Left_for_Inventory__c = tmpSH.Control_Box_Filter_Left_for_Inventory__c;
                updated = true;
            }
            else
            {
                sh.Control_Box_Filter_Installed__c='';
                sh.Control_Box_Filter_Left_for_Inventory__c='';
            }
            if(updated)
            {
                servsToUpdate.add(sh);
            }
        }
    }
    
    public static Service_History__c parseParts(Service_History__c sh, String fieldName, String tField, String fField)
    {
        sh.put(tField,null);
        sh.put(fField,null);
        Boolean parsed = false;
        Integer tCount = 0;
        Integer fCount = 0;
        
        // keep track of counts
        Map<String,PartCount> serviceHCountsProcessed = new Map<String,PartCount>();
        
        String partsValue = (String)sh.get(fieldName);
        List<String> splitParts = partsValue.split(',\\s*');
        
        // loop thru each part T&F string to check for dups in the part field
        for(String strPart: splitParts)
        {
            if(!serviceHCountsProcessed.containsKey(strPart))
            {
                serviceHCountsProcessed.put(strPart, new PartCount(strPart));
            }
            else
            {
                PartCount shc = serviceHCountsProcessed.get(strPart);
                shc.partCount+=1;
                serviceHCountsProcessed.put(strPart, shc);
            }
        }
        
        for(PartCount pc : serviceHCountsProcessed.values())
        {
            
            if(pc.partName.contains(':T') || pc.partName.contains(':t'))
            {
                String tmpVal = pc.partName.substringBefore(':');
                if(sh.get(tField)!=null)
                {
                    String strTField = (String)sh.get(tField); // convert the current field val
                    if(strTField.substringBefore(' ')!=tmpVal)
                    {
                        String tempTField = sh.get(tField) + ' ' + tmpVal+' ('+pc.partCount+')';
                        sh.put(tField,tempTField);
                    }
                }
                else
                {
                    sh.put(tField,tmpVal+' ('+pc.partCount+')');
                }
                parsed = true;
            }
            if(pc.partName.contains(':F') || pc.partName.contains(':f'))
            {
                String tmpVal = pc.partName.substringBefore(':');
                if(sh.get(fField)!=null)
                {
                    String strFField = (String)sh.get(fField); // convert the current field val
                    if(strFField.substringBefore(' ')!=tmpVal)
                    {
                        String tempFField = sh.get(fField) + ' ' + tmpVal+' ('+pc.partCount+')';
                        sh.put(fField,tempFField);
                    }
                }
                else
                {
                    sh.put(fField,tmpVal+' ('+pc.partCount+')');
                }
                parsed = true;
            }
        }
        return sh;
    }
    
    private class PartCount
    {
        String partName;
        Integer partCount = 1;
        
        private PartCount(String partName)
        {
            this.partName = partName;
        }
    }
}