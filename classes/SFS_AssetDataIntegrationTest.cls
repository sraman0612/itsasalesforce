@IsTest
public class SFS_AssetDataIntegrationTest {
   @IsTest
    public static void HttpRESTMethodtest(){
      String interFaceDetail = 'RentalSelectionOracleDataTable';
      List<Asset> AssetList = SFS_TestDataFactory.createAssets( 1 , true);
      Set<Id> assetIds = new Set<Id>();
       for(Asset ast : AssetList){
        assetIds.add(ast.Id);
       }
       Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HttpRESTMethodCalloutMock());
            SFS_AssetDataIntegration.HttpRESTMethod(interFaceDetail,assetIds,'update');
       Test.stopTest();
   }
    public class HttpRESTMethodCalloutMock implements HttpCalloutMock{
        public HttpResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            res.setStatus('OK');
            res.setStatusCode(200);
            String str = '{"documents":{"items": [{"id":"320805046","Unique_ID": "02i3a00000D5ZReAAN","Asset_ID": "TS5350","Serial_Number": "TS5350","Rental_Type": "Compressor","Area": "West","Division":"Dallas Customer Center"}]}}';
            res.setBody(str);
            return res;
        }
    }
}