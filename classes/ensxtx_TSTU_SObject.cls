@isTest public with sharing class ensxtx_TSTU_SObject
{
     @isTest
    static void test_getSObjectNameFromId()
    {
        Case tstCase = ensxtx_TSTU_SFTestObject.createTestCase();
        ensxtx_TSTU_SFTestObject.upsertWithRetry(tstCase);

        Test.startTest();
        String sObjName = ensxtx_UTIL_SObject.getSObjectNameFromId(null);
        sObjName = ensxtx_UTIL_SObject.getSObjectNameFromId(tstCase.Id);
        Test.stopTest();

        System.assertEquals(sObjName, 'Case');

    }

    @isTest
    static void test_getSObjectById()
    {
        Account testAcct = ensxtx_TSTU_SFTestObject.createTestAccount();
        testAcct.put(ensxtx_UTIL_SFAccount.CustomerFieldName, 'TEST');
        ensxtx_TSTU_SFTestObject.upsertWithRetry(testAcct);

        List<String> fields = new List<String>();
        fields.add('Name');

        Test.startTest();

        SObject getObj = ensxtx_UTIL_SObject.getSObjectById(testAcct.Id, fields);
        
        Account getAcct = (Account)getObj;

        Test.stopTest();

        System.assertEquals(testAcct.Name,getAcct.Name);

    }


    @isTest
    static void test_isSobjectLinkedToCustomer()
    {
        Account testAcct = ensxtx_TSTU_SFTestObject.createTestAccount();
        ensxtx_TSTU_SFTestObject.upsertWithRetry(testAcct);

        Test.startTest();
        Boolean result1 = ensxtx_UTIL_SObject.isSObjectLinkedToCustomer(testAcct.Id);
        ensxtx_UTIL_SObject.setCustomerNumberOnSObject(testAcct.Id, 'customerNumber');
        ensxtx_UTIL_SObject.getValueFromSObjectField(null, null, null);
        ensxtx_UTIL_SObject.getValueFromSObjectField(testAcct, 'Bad Field', '');
        Test.stopTest();

        System.assertEquals(result1, false);
    }

    @isTest
    static void test_isSobjectLinkedToCustomer2()
    {
        Account testAcct = ensxtx_TSTU_SFTestObject.createTestAccount();
        testAcct.put(ensxtx_UTIL_SFAccount.CustomerFieldName, 'TEST');
        ensxtx_TSTU_SFTestObject.upsertWithRetry(testAcct);

        Test.startTest();
        Boolean result2 = ensxtx_UTIL_SObject.isSObjectLinkedToCustomer(testAcct.Id);
        Test.stopTest();

        System.assertEquals(result2, true);
    }

    @isTest
    static void test_booleanCalls()
    {
        Test.startTest();
        Id testId = Id.valueOf('001xa000003DIlo');

        Boolean custMapping = ensxtx_UTIL_SObject.doesSObjectHaveCustomerMapping(testId);
        Boolean userAccess = ensxtx_UTIL_SObject.canUserAccessSObjectCustomerNumber(testId);
        Boolean custNumber = ensxtx_UTIL_SObject.canUserSetSObjectCustomerNumber(testId);
        ensxtx_UTIL_SObject.getCustomerNumberFieldfromSObject(new Account());
        Boolean userAccessField = ensxtx_UTIL_SObject.canUserAccessSObjectField(testId, 'Name');
        Test.stopTest();

        System.Assert(custMapping == true);
        System.Assert(userAccess == true);
        System.Assert(custNumber == true);
        System.Assert(userAccessField == true);
    }
}