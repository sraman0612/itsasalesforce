@isTest
global class PaymentGatewayCalloutMockTest implements HttpCalloutMock {
    
    
    global HTTPResponse respond(HttpRequest req){
                // Below Code is used to setup the Body for the Request
        String 	bodyToSend = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-282871802"><wsu:Created>2022-09-13T05:56:05.291Z</wsu:Created></wsu:Timestamp></wsse:Security></soap:Header><soap:Body><c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.134"><c:merchantReferenceCode>CTS-56955</c:merchantReferenceCode><c:requestID>6630485645486895204009</c:requestID><c:decision>ACCEPT</c:decision><c:reasonCode>100</c:reasonCode><c:requestToken>Axj/7wSTZ3B5DMB1AQapABsY0aWZElvSbUadWCojkS5GccBURyJcjOPpBSoPMIz4ZNJMq6PSXrLhok2dweQzAdQEGqQAtwZ9</c:requestToken><c:purchaseTotals><c:currency>USD</c:currency></c:purchaseTotals><c:ccAuthReply><c:reasonCode>100</c:reasonCode><c:amount>6891.00</c:amount><c:authorizationCode>831000</c:authorizationCode><c:avsCode>Y</c:avsCode><c:avsCodeRaw>Y</c:avsCodeRaw><c:authorizedDateTime>2022-09-13T05:56:05Z</c:authorizedDateTime><c:processorResponse>00</c:processorResponse><c:reconciliationID>44YHI7R6QSUA</c:reconciliationID><c:authRecord>0110322000000E10000200000000000068910009130556051575933434594849375236515355413833313030303030000159004400223134573031363135303730333830323039344730363400103232415050524F56414C0006564943524120</c:authRecord><c:paymentNetworkTransactionID>016150703802094</c:paymentNetworkTransactionID></c:ccAuthReply><c:encryptedPayment><c:referenceID>1202209130556051012567478</c:referenceID></c:encryptedPayment><c:card><c:cardType>001</c:cardType></c:card></c:replyMessage></soap:Body></soap:Envelope>';
        String RESPONSE_HEADER_PARAM_NAME = 'Content-Type';
        String RESPONSE_HEADER_CONTENT_TYPE = 'application/xml';
        HttpResponse resp=new HttpResponse();
        resp.setHeader(RESPONSE_HEADER_PARAM_NAME, RESPONSE_HEADER_CONTENT_TYPE);
        resp.setBody(bodyToSend);
        resp.setStatusCode(200);
        return resp;
        
    }
    
}