global class C_CreateAssetShareBatch implements Database.Batchable<sObject> {
    global final String Query ;
    global final List<string> aIDs;
    
    global C_CreateAssetShareBatch(string q, List<string> acctIDs)
    {
        aIDs = acctIDs;
        if(q != null)
        	Query=q;
        else
            Query='Select id, parent_record_id__c from account where ID IN: aIDs';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
      return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        List<assetShare> assetShareToCreate = new List<assetShare>();
        //List<id> uId = new List<id>();
        for(Account account:scope)
        {
            system.debug(account.id);
            for(User U : [Select id,Partner_Account_ID__c,accountid from User where accountID = :account.id and toLabel(Community_User_Type__c)='Global Account'])
            {
                system.debug('user account id: '+u.Partner_Account_ID__c +' Account parent account id: '+account.parent_record_id__c);
                //uID.add(u.id);
                for(Asset a: [Select id from asset where parent_accountid__c = :account.parent_record_id__c or accountid =:u.Partner_Account_ID__c])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
        }
        system.debug(assetShareToCreate.size());
        insert assetShareToCreate;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        system.debug('Finished creating rights!!!');
    }

}