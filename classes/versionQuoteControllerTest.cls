/**
 * Created by OlhaHulenko on 05/10/2023.
 */

@IsTest
private class versionQuoteControllerTest {
    //@TestSetup
    public static void createTestData() {
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        RecordType contactRecordType = [SELECT Id, Name, SObjectType FROM RecordType WHERE Name ='IR Comp AIRD Contact' AND SObjectType = 'Contact'];
        List<Account> accList=SFS_TestDataFactory.createAccounts(2, false);
        accList[0].IRIT_Customer_Number__c='1211';
        accList[0].Type='Prospect';
        accList[1].IRIT_Customer_Number__c='1281';
        accList[1].RecordTypeId=accRecID;
        insert accList;
        accList[0].Bill_To_Account__c=accList[1].Id;
        update accList;
        Contact con = new Contact(Salutation = 'Ms.',LastName = 'test', FirstName='contact', Title = 'KS', Email = 'example@ir.com',RecordTypeId = contactRecordType.Id,
                Phone ='8890009', AccountId = accList[0].Id);

        insert con;

        Division__c div=new Division__c(Name='and',	EBS_System__c='MfgPRO',CurrencyIsoCode='USD');
        insert div;

        ServiceContract Serc=new ServiceContract(AccountId=accList[0].Id,SFS_Bill_To_Account__c=accList[0].Id,SFS_Type__c='Advanced Billing',SFS_Division__c=div.Id,SFS_Status__c='Draft',SFS_Agreement_Value__c=23,
                CurrencyIsoCode='USD',	SFS_Consumables_Ship_To__c=accList[0].Id,	SFS_Shipping_Instructions__c='the tagah',	SFS_Portable_Required__c='Yes',
                SFS_Invoice_Type__c='Prepaid',	SFS_Invoice_Format__c='Detail',Name='agree',StartDate=Date.newInstance(2021, 11, 5),
                EndDate=Date.newInstance(2021, 11, 30));

        insert Serc;

        List<OperatingHours> opHoursList=new List<OperatingHours>();
        OperatingHours opHours=new OperatingHours();
        opHours.Name = 'Test Operating Hours';
        opHours.TimeZone = UserInfo.getTimeZone().getID();
        opHoursList.add(opHours);
        insert opHoursList[0];

        DateTime cDT = System.Now();
        String dayOfWeek = cDT.format('EEEE');
        Time timeStart = Time.newInstance(Integer.valueOf('04'),Integer.valueOf('30'),0,0);
        Time timeEnd = Time.newInstance(Integer.valueOf('07'),Integer.valueOf('30'),0,0);
        List<TimeSlot> timeSloList=new List<TimeSlot>();
        TimeSlot timeslt=new TimeSlot();
        timeslt.StartTime=timeStart;
        timeslt.EndTime=timeEnd;
        timeslt.DayOfWeek=dayOfWeek;
        timeslt.OperatingHoursId = opHoursList[0].Id;
        timeSloList.add(timeslt);
        insert timeSloList;

        List<MaintenancePlan> planList = SFS_TestDataFactory.createMaintenancePlan(1,Serc.Id,false);
        insert planList;

        WorkOrder wo = new WorkOrder(AccountId = accList[0].Id,
                ContactId = con.Id,
                SFS_Work_Order_Type__c = 'Installation',
                Status = 'New',
                SFS_Severity__c = '4 - Low',
                Subject = 'test',
                SFS_Estimated_Work_Order_Value__c=100,
                ServiceContractId=Serc.Id,
                FSL__VisitingHours__c = opHoursList[0].Id,
                MaintenancePlanId=planList[0].Id,
                SuggestedMaintenanceDate=System.today()+5);
        insert wo;

        Id woId = [SELECT Id FROM WorkOrder LIMIT 1].Id;

        cafsl__Oracle_Quote__c oracleQuote = new cafsl__Oracle_Quote__c(Name='abc', Work_Order__c = woId, cafsl__Transaction_ID__c = '323184985');

        insert oracleQuote;

    }

    @IsTest
    public static void testHTTPCallout(){
        createTestData();
        String xmlString = '323133183';
        String interfaceDetail = 'OracleCPQVersionQuote';
        String interfaceLabel = 'CPQ Sales Version Quote';
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new cPQValuesResponseHTTPMock());
            versionQuoteController.HttpMethod(xmlString, interfaceDetail, interfaceLabel);
        Test.stopTest();
    }
    @IsTest
    public static void reDirectToQuoteRecordTest(){
        createTestData();
        List<cafsl__Oracle_Quote__c> testQuote = [SELECT Id FROM cafsl__Oracle_Quote__c];
        Test.startTest();
            PageReference pageRef = Page.VersionQuote;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', String.valueOf(testQuote[0].Id));
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(testQuote);
            System.debug('sc:'+sc);
            sc.setSelected(testQuote);
            versionQuoteController verCtrl = new versionQuoteController(sc);
            System.debug('verCtrl:'+verCtrl);
            System.assertEquals(null,verCtrl.reDirectToQuoteRecord());
        Test.stopTest();
    }
    @IsTest
    public static void reDirectToQuoteRecordsListTest(){
        createTestData();
        WorkOrder wo = [SELECT Id FROM WorkOrder LIMIT 1];
        cafsl__Oracle_Quote__c oracleQuote = new cafsl__Oracle_Quote__c(Name='abc1', Work_Order__c = wo.Id, cafsl__Transaction_ID__c = '323184987');
        insert oracleQuote;

        List<cafsl__Oracle_Quote__c> testQuote = [SELECT Id FROM cafsl__Oracle_Quote__c];
        Test.startTest();
        PageReference pageRef = Page.VersionQuote;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', String.valueOf(testQuote[0].Id));
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(testQuote);
        System.debug('sc:'+sc);
        sc.setSelected(testQuote);
        versionQuoteController verCtrl = new versionQuoteController(sc);
        System.debug('verCtrl:'+verCtrl);
        System.assertEquals(null,verCtrl.reDirectToQuoteRecord());
        Test.stopTest();
    }
//    @IsTest
//    public static void reDirectToQuoteRecordTestFail(){
//       //
//        //List<cafsl__Oracle_Quote__c> testQuote = [SELECT Id FROM cafsl__Oracle_Quote__c];
//        Test.startTest();
//        PageReference pageRef = Page.VersionQuote;
//        Test.setCurrentPage(pageRef);
//        pageRef.getParameters().put('id', null);
//        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(null);
//        System.debug('sc:'+sc);
//        sc.setSelected(null);
//        versionQuoteController verCtrl = new versionQuoteController(sc);
//        System.debug('verCtrl:'+verCtrl);
//        System.assertNotEquals(null,verCtrl.reDirectToQuoteRecord());
//        Test.stopTest();
//    }
    @IsTest
    public static void testHTTPResp(){
        createTestData();
        String responseKey = 'bs_id';

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new cPQValuesResponseHTTPMock());
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://example.com/example/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        //HttpResponse rese = "System.HttpResponse[Status=OK, StatusCode=200]";
        System.debug('res: '+ res);
        //Test.setMock(HttpCalloutMock.class, new cPQValuesResponseHTTPMock()));
        versionQuoteController.getCPQVersionResponse(res,responseKey);
        //versionQuoteController.getCPQVersionResponse(null,responseKey);
        //Map<String, Object> testRes =  new Map<String, Object> (versionQuoteController.getCPQValuesResponse(res,responseKey));
        Test.stopTest();

    }
    public class cPQValuesResponseHTTPMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            // Create a fake response.
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"documents":{"bs_id":"323184985"}}');
            res.setStatusCode(200);
            return res;

        }
    }
}