global class GDInsideSyncAcctToContactSched implements Schedulable{
    global void execute(SchedulableContext SC) {
        string current = string.valueof(date.valueof(system.today()));
        GDInsideSyncAcctToContact.sync(current);
   }

}