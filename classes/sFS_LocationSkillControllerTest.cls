@isTest
public class sFS_LocationSkillControllerTest {
    @isTest
     public static void getRecordsTest(){
         List<Division__C> divList=SFS_TestDataFactory.createDivisions(1, false);
         insert divList[0];
         List<Schema.Location> locationList=SFS_TestDataFactory.createLocations(1,divList[0].Id, true);
         
         SFS_Location_Skill_Requirement__c lSR =new SFS_Location_Skill_Requirement__c(Location__c=locationList[0].Id,Skill__c='Small Rotary');
         insert lSR;
         sFS_LocationSkillController.getRecords(locationList[0].Name,lSR.Skill__c);
         sFS_LocationSkillController.getSkill('Small Rotary');
     }

}