@isTest
private class iConn_Data_Creation_Batch_Test {

    @testSetup
    private static void setup(){
         Asset sn = new Asset(
            Name='Test_Asset',
			IMEI__c = '123456789',
            Cumulocity_Id__c = 987654321.0
        );
        insert sn;
    }
    
    private static testmethod void doTest(){
        Test.startTest();
			Database.executeBatch(new iConn_Data_Creation_Batch(), 15); 
        Test.stopTest();
    }
    
}