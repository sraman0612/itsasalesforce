@isTest
public class SFS_TimeSheetEntry_Test {
    @TestSetup
    static void makeData(){
        DateTime startTime = DateTime.now();
        DateTime endTime = startTime.addHours(1);

        User usr = SFS_TestDataFactory.createTestUser('SFS Service Technician', True);

        ServiceResource tech = new ServiceResource(Name = 'Test Tech', RelatedRecordId = usr.Id);
        insert tech;

        TimeSheet ts = new TimeSheet(StartDate = Date.today(), EndDate = Date.today(), ServiceResourceId = tech.Id);
        insert ts;

        TimeSheetEntry tse = new TimeSheetEntry(TimeSheetId = ts.Id, StartTime = startTime, EndTime = endTime);
        insert tse;
    }

    @isTest
    static void runTest() {
        TimeSheetEntry tse = [SELECT Id, StartTime, EndTime, Local_Punch_Date__c, Local_Punchout_Time__c FROM TimeSheetEntry LIMIT 1];
        System.assert(String.isNotBlank(tse.Local_Punch_Date__c), 'Local Punch Date is not null');
        System.assert(String.isNotBlank(tse.Local_Punchout_Time__c), 'Local Punchout Date is not null');
    }
}