/*=========================================================================================================
* @author Manimozhi B, Capgemini
* @date 10/5/2022
* @description: Scheduled Apex class for TimeSheet



Modification Log:
------------------------------------------------------------------------------------
Developer Mod Number Date Description
------------------------------------------------------------------------------------



===============================================================================================================================*/

public class SFS_TimeSheetNotification implements Database.Batchable<sObject>, Schedulable {
    public Database.QueryLocator start(Database.BatchableContext BC){
        List<RecordType> recordTypeId =[select id, DeveloperName from RecordType where DeveloperName ='SFS_Time_Sheet_Service' Limit 1];
        return Database.getQueryLocator([select id, ServiceResourceId from TimeSheet  where Status ='New' AND RecordTypeId =:recordTypeId]);
	}
    public void execute(Database.BatchableContext BC, list<TimeSheet> timeSheetList){
        List<CustomNotificationType> customType = [select id from CustomNotificationType where DeveloperName ='SFS_Weekly_Timesheets'];
        Set<String> managerId =new Set<String>();
         Set<String> serviceResId =new Set<String>();
        Messaging.CustomNotification notify = new Messaging.CustomNotification();
        for(TimeSheet TS :timeSheetList){
            serviceResId.add(TS.ServiceResourceId);
        }
        notify.setTitle('Weekly Timesheets');
        notify.setBody('Be sure all your Technicians have their Timesheets completed by Sunday.');
        notify.setNotificationTypeId(customType[0].id);
        notify.setTargetId(userInfo.getUserId());
        List<ServiceResource> managerIdList=[select id,RelatedRecordId,RelatedRecord.ManagerId from ServiceResource  where  Id IN:serviceResId];
        for(ServiceResource SR:managerIdList){
            if(SR.RelatedRecord.ManagerId!=null){
                managerId.add(SR.RelatedRecord.ManagerId);
            }            
            
        }
            notify.send(managerId);
    } 
    public void finish(Database.BatchableContext BC){
        
    }
    public void execute(SchedulableContext SC){
        SFS_TimeSheetNotification tsn =new SFS_TimeSheetNotification();
        Database.executeBatch(tsn);
    }
    

}