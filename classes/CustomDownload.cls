global class CustomDownload {

    webservice static string loadAttachment(ID att_id){
        String base64Value;
        String contentType;
        Attachment att = null;
        ContentDocument doc = null;
        ContentVersion ver = null;

        // Determine if the ID passed in belongs to the Attachment object (00P) or the ContentDocument object (069)
        String myIdPrefix = String.valueOf(att_id).substring(0,3);
        system.debug('myIdPrefix ==> '+myIdPrefix);
     
        if (myIdPrefix == '00P') {
            att = [Select Id, Body, ContentType From Attachment where id=:att_id];
        }

        if (att != null) {
            base64Value = EncodingUtil.base64Encode(att.Body);
            contentType = att.ContentType;
        }
        // Code to pull back a file instead of an attachment
        if (att == null && myIdPrefix == '069') {
            doc = [Select Id, ContentAssetId, FileType, FileExtension From ContentDocument where id=:att_id];
            ver = [SELECT VersionData FROM ContentVersion WHERE ContentDocumentId = :doc.Id AND IsLatest = true];
            if (doc != null && ver != null) {
                base64Value = EncodingUtil.base64Encode(ver.VersionData);
                contentType = doc.FileExtension;
            }
        }
        return '{"base64Value":"'+base64Value+'","contentType":"'+contentType+'"}';
    }
}