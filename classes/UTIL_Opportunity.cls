public without sharing class UTIL_Opportunity {

    public static void updateOwner (List<Id> oppIds) {

        Map<String, Opportunity> oppsToUpdateMap = new Map<String, Opportunity>();
        Boolean updateFlag = false;
        Set<Id> accountIds = new Set<Id>();

        for (Opportunity opp : [SELECT Id,AccountId,OwnerId,Sales_Channel__c FROM Opportunity WHERE Id IN :oppIds]) {
            oppsToUpdateMap.put(opp.AccountId+'-'+opp.Sales_Channel__c, opp);
            accountIds.add(opp.AccountId);
        }

        for (Account acc : [SELECT Id, DChannel_First_2__c, ParentId, TSM_of_DC_Account__c FROM Account WHERE Account_Type__c='Child' AND ParentId IN :accountIds]) {
            String key = acc.ParentId+'-'+acc.DChannel_First_2__c;
            if (oppsToUpdateMap.keySet().contains(key) && null != acc.TSM_of_DC_Account__c) {
                oppsToUpdateMap.get(key).OwnerId = acc.TSM_of_DC_Account__c;
                updateFlag = true;
            }
        }
        if (updateFlag) update oppsToUpdateMap.values();
    }
}