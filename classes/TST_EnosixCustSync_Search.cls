/// enosiX Inc. Generated Apex Model
/// Generated On: 9/11/2019 10:25:30 AM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class TST_EnosixCustSync_Search
{

    public class MockSBO_EnosixCustSync_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixCustSync_Search.class, new MockSBO_EnosixCustSync_Search());
        SBO_EnosixCustSync_Search sbo = new SBO_EnosixCustSync_Search();
        System.assertEquals(SBO_EnosixCustSync_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        SBO_EnosixCustSync_Search.EnosixCustSync_SC sc = new SBO_EnosixCustSync_Search.EnosixCustSync_SC();
        System.assertEquals(SBO_EnosixCustSync_Search.EnosixCustSync_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);
        System.assertNotEquals(null, sc.ACCOUNT_GROUPS);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        SBO_EnosixCustSync_Search.SEARCHPARAMS childObj = new SBO_EnosixCustSync_Search.SEARCHPARAMS();
        System.assertEquals(SBO_EnosixCustSync_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.DateFrom = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateFrom);

        childObj.InitialLoad = true;
        System.assertEquals(true, childObj.InitialLoad);


    }

    @isTest
    static void testACCOUNT_GROUPS()
    {
        SBO_EnosixCustSync_Search.ACCOUNT_GROUPS childObj = new SBO_EnosixCustSync_Search.ACCOUNT_GROUPS();
        System.assertEquals(SBO_EnosixCustSync_Search.ACCOUNT_GROUPS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixCustSync_Search.ACCOUNT_GROUPS_COLLECTION childObjCollection = new SBO_EnosixCustSync_Search.ACCOUNT_GROUPS_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.CustomerAccountGroup = 'X';
        System.assertEquals('X', childObj.CustomerAccountGroup);


    }

    @isTest
    static void testEnosixCustSync_SR()
    {
        SBO_EnosixCustSync_Search.EnosixCustSync_SR sr = new SBO_EnosixCustSync_Search.EnosixCustSync_SR();

        sr.registerReflectionForClass();

        System.assertEquals(SBO_EnosixCustSync_Search.EnosixCustSync_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        SBO_EnosixCustSync_Search.SEARCHRESULT childObj = new SBO_EnosixCustSync_Search.SEARCHRESULT();
        System.assertEquals(SBO_EnosixCustSync_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixCustSync_Search.SEARCHRESULT_COLLECTION childObjCollection = new SBO_EnosixCustSync_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.CentralDeletionIndicator = 'X';
        System.assertEquals('X', childObj.CentralDeletionIndicator);

        childObj.CustomerAccountGroup = 'X';
        System.assertEquals('X', childObj.CustomerAccountGroup);

        childObj.CustomerNumber = 'X';
        System.assertEquals('X', childObj.CustomerNumber);

        childObj.Name1 = 'X';
        System.assertEquals('X', childObj.Name1);

        childObj.Name2 = 'X';
        System.assertEquals('X', childObj.Name2);

        childObj.Name3 = 'X';
        System.assertEquals('X', childObj.Name3);

        childObj.Name4 = 'X';
        System.assertEquals('X', childObj.Name4);

        childObj.Street = 'X';
        System.assertEquals('X', childObj.Street);

        childObj.Street2 = 'X';
        System.assertEquals('X', childObj.Street2);

        childObj.Street3 = 'X';
        System.assertEquals('X', childObj.Street3);

        childObj.Street4 = 'X';
        System.assertEquals('X', childObj.Street4);

        childObj.Street5 = 'X';
        System.assertEquals('X', childObj.Street5);

        childObj.HouseNumber = 'X';
        System.assertEquals('X', childObj.HouseNumber);

        childObj.CityPostalCode = 'X';
        System.assertEquals('X', childObj.CityPostalCode);

        childObj.City = 'X';
        System.assertEquals('X', childObj.City);

        childObj.CountryKey = 'X';
        System.assertEquals('X', childObj.CountryKey);

        childObj.Region = 'X';
        System.assertEquals('X', childObj.Region);

        childObj.POBox = 'X';
        System.assertEquals('X', childObj.POBox);

        childObj.POBoxPostalCode = 'X';
        System.assertEquals('X', childObj.POBoxPostalCode);

        childObj.TelephoneNo = 'X';
        System.assertEquals('X', childObj.TelephoneNo);

        childObj.EMailAddress = 'X';
        System.assertEquals('X', childObj.EMailAddress);

        childObj.ChangeDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.ChangeDate);

        childObj.ChangeTime = Time.newInstance(1,1,1,0);
        System.assertEquals(Time.newInstance(1,1,1,0), childObj.ChangeTime);

        childObj.AddressNumber = 'X';
        System.assertEquals('X', childObj.AddressNumber);

        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);

        childObj.Division = 'X';
        System.assertEquals('X', childObj.Division);

        childObj.CentralOrderBlockForCustomer = 'X';
        System.assertEquals('X', childObj.CentralOrderBlockForCustomer);

        childObj.CustomerCassification = 'X';
        System.assertEquals('X', childObj.CustomerCassification);

        childObj.CityCode = 'X';
        System.assertEquals('X', childObj.CityCode);

        childObj.CountyCode = 'X';
        System.assertEquals('X', childObj.CountyCode);

        childObj.SortField = 'X';
        System.assertEquals('X', childObj.SortField);

        childObj.AccountAssignmentGroup = 'X';
        System.assertEquals('X', childObj.AccountAssignmentGroup);

        childObj.CustomerGroup1 = 'X';
        System.assertEquals('X', childObj.CustomerGroup1);

        childObj.CustomerGroup2 = 'X';
        System.assertEquals('X', childObj.CustomerGroup2);

        childObj.CustomerGroup3 = 'X';
        System.assertEquals('X', childObj.CustomerGroup3);

        childObj.CustomerGroup5 = 'X';
        System.assertEquals('X', childObj.CustomerGroup5);

        childObj.CustomerOrderBlockSalesArea = 'X';
        System.assertEquals('X', childObj.CustomerOrderBlockSalesArea);

        childObj.DeletionFlagForCustomerSalesLevel = 'X';
        System.assertEquals('X', childObj.DeletionFlagForCustomerSalesLevel);

        childObj.CustomerGroup = 'X';
        System.assertEquals('X', childObj.CustomerGroup);

        childObj.Incoterms1 = 'X';
        System.assertEquals('X', childObj.Incoterms1);

        childObj.PriceListType = 'X';
        System.assertEquals('X', childObj.PriceListType);

        childObj.SalesGroup = 'X';
        System.assertEquals('X', childObj.SalesGroup);

        childObj.TermsOfPaymentKey = 'X';
        System.assertEquals('X', childObj.TermsOfPaymentKey);

        childObj.SalesDistrict = 'X';
        System.assertEquals('X', childObj.SalesDistrict);

        childObj.SalesOffice = 'X';
        System.assertEquals('X', childObj.SalesOffice);

        childObj.CompanyCode = 'X';
        System.assertEquals('X', childObj.CompanyCode);

        childObj.PartnerFunction = 'X';
        System.assertEquals('X', childObj.PartnerFunction);

        childObj.Description = 'X';
        System.assertEquals('X', childObj.Description);

        childObj.Partner = 'X';
        System.assertEquals('X', childObj.Partner);

        childObj.PartnerName = 'X';
        System.assertEquals('X', childObj.PartnerName);

        childObj.AccountAsgnGrpText = 'X';
        System.assertEquals('X', childObj.AccountAsgnGrpText);

        childObj.SalesOrganizationDescription = 'X';
        System.assertEquals('X', childObj.SalesOrganizationDescription);

        childObj.DistributionChannelDescription = 'X';
        System.assertEquals('X', childObj.DistributionChannelDescription);

        childObj.DivisionDescription = 'X';
        System.assertEquals('X', childObj.DivisionDescription);

        childObj.CentralOrderBlockText = 'X';
        System.assertEquals('X', childObj.CentralOrderBlockText);

        childObj.SalesAreaOrderBlockText = 'X';
        System.assertEquals('X', childObj.SalesAreaOrderBlockText);

        childObj.CustomerGroupText = 'X';
        System.assertEquals('X', childObj.CustomerGroupText);

        childObj.CustomerGroup1Text = 'X';
        System.assertEquals('X', childObj.CustomerGroup1Text);

        childObj.CustomerGroup2Text = 'X';
        System.assertEquals('X', childObj.CustomerGroup2Text);

        childObj.CustomerGroup3Text = 'X';
        System.assertEquals('X', childObj.CustomerGroup3Text);

        childObj.CustomerGroup5Text = 'X';
        System.assertEquals('X', childObj.CustomerGroup5Text);

        childObj.AccountGroupName = 'X';
        System.assertEquals('X', childObj.AccountGroupName);

        childObj.SalesDistrictText = 'X';
        System.assertEquals('X', childObj.SalesDistrictText);

        childObj.SalesOfficeText = 'X';
        System.assertEquals('X', childObj.SalesOfficeText);

        childObj.ReqStockLevelCM = 'X';
        System.assertEquals('X', childObj.ReqStockLevelCM);

        childObj.ReqStockLevelSB = 'X';
        System.assertEquals('X', childObj.ReqStockLevelSB);

        childObj.ReqStockLevelDI = 'X';
        System.assertEquals('X', childObj.ReqStockLevelDI);

        childObj.ReqStockLevelGI = 'X';
        System.assertEquals('X', childObj.ReqStockLevelGI);

        childObj.ReqStockLevelGT = 'X';
        System.assertEquals('X', childObj.ReqStockLevelGT);

        childObj.ReqStockLevelDT = 'X';
        System.assertEquals('X', childObj.ReqStockLevelDT);

        childObj.ReqStockLevelMV = 'X';
        System.assertEquals('X', childObj.ReqStockLevelMV);

        childObj.ReqStockLevelDV = 'X';
        System.assertEquals('X', childObj.ReqStockLevelDV);

        childObj.ReqStockLevelFH = 'X';
        System.assertEquals('X', childObj.ReqStockLevelFH);

        childObj.ReqStockLevelWT = 'X';
        System.assertEquals('X', childObj.ReqStockLevelWT);

        childObj.ReqStockLevelER = 'X';
        System.assertEquals('X', childObj.ReqStockLevelER);


    }

}