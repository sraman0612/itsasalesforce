global class C_Batch_MVPL_Reassign implements Database.Batchable<sObject> {
    
    //assign latest
    
    global final String Query = 'SELECT Id, Name, Machine_Version_Parts_List__c, MPG4_Code__c, Model_Number__c From Asset WHERE Machine_Version_Parts_List__c IN (SELECT Id FROM Machine_Version_Parts_List__c WHERE End_Date__c != null)';
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Asset> scope){
		System.debug(scope[0]);
        //Model Number contains MVPL Config
        List<AggregateResult> agrs = [SELECT Configuration__c, MAX(Start_Date__c) FROM Machine_Version_Parts_List__c WHERE MPG4_Code_Lookup__c = :scope[0].MPG4_Code__c GROUP BY Configuration__c ORDER BY Configuration__c];
        //List<Machine_Version_Parts_List__c> mvpls = [SELECT Id, Configuration__c FROM Machine_Version_Parts_List__c WHERE MPG4_Code_Lookup__c = :scope[0].MPG4_Code__c GROUP BY MPG4_Code_Lookup__c, Configuration__c HAVING MAX(Start_Date__c) ORDER BY Start_Date__c DESC];  
        
        if(agrs.size() == 0){
            //if no records are found we don't want to change the MVPL currently assigned
            return;
        }
        
        boolean madeChange = false;
        
        for(AggregateResult agr : agrs){
            String config = (String) agr.get('Configuration__c');
            
            if(scope[0].Model_Number__c.containsIgnoreCase(config)){
                Date startDate = (Date) agr.get('expr0');
                //match, go grab Id
                Machine_Version_Parts_List__c mvpl = [SELECT Id FROM Machine_Version_Parts_List__c WHERE MPG4_Code_Lookup__c = :scope[0].MPG4_Code__c AND Configuration__c = :config AND Start_Date__c = :startDate];
                scope[0].Machine_Version_Parts_List__c = mvpl.Id;
                madeChange = true;
                break;
            }

        }
  
        if(madeChange){
        	update scope;
        }
        
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
    
}