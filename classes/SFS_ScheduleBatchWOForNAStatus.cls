/*=========================================================================================================
* @author Mahesh Raj, Capgemini
* @date 07/21/2023
* @description: This class is used to schedule the batch SFS_batchtorunWOwithSAStatusNA at 6AM and 6PM CDT everyday

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/
global class SFS_ScheduleBatchWOForNAStatus implements schedulable{
  global void execute(SchedulableContext sc){
      SFS_batchtorunWOwithSAStatusNA b = new SFS_batchtorunWOwithSAStatusNA(); 
      database.executebatch(b,1);
   }
 }