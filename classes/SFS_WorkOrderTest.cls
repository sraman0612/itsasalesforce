/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 06/10/2021
* @description: SFS_WorkOrderHandler Apex Handler.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001       22/09/2021  Apex Handler to call PolygonUtils Apex class to get ServiceTerritoryId
Bhargavi Nerella        M-002       22/11/2021  Test Method for createChargesForWOLI in WorkOrderHandler
============================================================================================================================================================*/
@isTest
public class SFS_WorkOrderTest {
    @isTest
    public static void CallPolygonUtilsClass(){
        RecordType rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='Site Account - NA Air' AND SObjectType = 'Account'];
        RecordType rtCon = [Select Id, Name, SObjectType FROM RecordType where Name ='Service' AND SObjectType = 'Contact'];
        
        Map<Id,WorkOrder> oldMap = new Map<id,WorkOrder>();
        Map<Id,WorkOrder> newMap = new Map<id,WorkOrder>();
        List<WorkOrder> newList = new List<WorkOrder>();
        
        Double latitude = 32.873605;
        Double longitude =	-96.971908;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Currency__c='USD';
        acct.RecordTypeId = rtAcc.Id; 
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.ShippingCity ='Montreat';
        acct.ShippingCountry ='United States';
        acct.ShippingGeocodeAccuracy ='Zip';
        acct.ShippingPostalCode='28757';
        acct.ShippingState='NC';
        acct.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct.IRIT_Customer_Number__c='1234';       
        
        insert acct;
        Double latitude1 = 32.873605;
        Double longitude2 =	-96.971908;
        List<Account> accList = new List<Account>();
        Account acct2 = new Account();
        
        acct2.name = 'test account2';
        acct.Currency__c='USD';
        acct2.RecordTypeId = rtAcc.Id; 
        acct2.ShippingLatitude =   latitude1;
        acct2.ShippingLongitude =   longitude2;
        acct2.ShippingCity ='Montreat';
        acct2.ShippingCountry ='United States';
        acct2.ShippingGeocodeAccuracy ='Zip';
        acct2.ShippingPostalCode='28757';
        acct2.ShippingState='NC';
        acct2.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct2.IRIT_Customer_Number__c='1234';        
        insert acct2;
        accList.add(acct2);      
        
        Contact con = new Contact(RecordTypeId = rtCon.Id, 
                                  Firstname = 'Test',
                                  LastName = 'Contact',
                                  Account = acct,
                                  title = 'tester',
                                  email = 'test@gmail.com',
                                  phone = '1234567890');
        insert con;
        OperatingHours op= new OperatingHours(Name='Normal',TimeZone='America/Chicago');
        insert op;
        
        ServiceTerritory territory=new ServiceTerritory(Name='abc',OperatingHoursId=op.Id,
                                                        SFS_Service_Report_Name__c='test service report',SFS_Service_Report_Phone__c='12364778',
                                                        IsActive=true);
        
        
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        List<Schema.Location> Loc= SFS_TestDataFactory.createLocations(1,divisionsList[0].Id, true);
        
        Test.startTest();
        List<workOrder> tw=SFS_TestDataFactory.createWorkOrder(1,acct.Id,Loc[0].id,divisionsList[0].Id,null,true);
        List<workOrder> tw2=SFS_TestDataFactory.createWorkOrder(1,acct2.Id,Loc[0].id,divisionsList[0].Id,null,true);
        
        OldMap.put(tw2[0].id,tw2[0]);
              
        tw2[0].subject = 'test1';
        tw2[0].AccountId=acct.Id;
        update tw2[0];        
        newMap.put(tw2[0].Id, tw2[0]);
        newList.add(tw2[0]);
        
        SFS_WorkOrderHandler.callPolygonUtilsForST(oldMap,newList);           
        Test.stopTest();
    }
    //Test Method for createChargesForWOLI in WorkOrderHandler
    @isTest
    public static void createChargesForWOLITest(){
        Double latitude = 48.7646475292616;
        Double longitude =-98.1987222787954;
        Double latitude1 = 48.7646475292616;
        Double longitude2 =-98.1987222787954;
        Map<Id,WorkOrder> oldMap = new Map<id,WorkOrder>();
        List<WorkOrder> newList = new List<WorkOrder>();
        
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[1].IRIT_Customer_Number__c='1234';
        accList[1].RecordTypeId=accRecID;
        insert accList[1];
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].CurrencyIsoCode='USD';
        accList[0].ShippingLatitude =   latitude;
        accList[0].ShippingLongitude =   longitude;
        accList[0].ShippingCity ='Montreat';
        accList[0].ShippingCountry ='United States';
        accList[0].ShippingGeocodeAccuracy ='Zip';
        accList[0].ShippingPostalCode='28757';
        accList[0].ShippingState='NC';
        accList[0].ShippingStreet='2342 Appalachian Way\nUnit 156';
        accList[0].IRIT_Customer_Number__c='1234';
        accList[0].Type = 'Prospect';
        insert accList[0];
        Contact con = SFS_TestDataFactory.getContact();
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        insert svcAgreementList;
        
        OperatingHours op= new OperatingHours(Name='Normal',TimeZone='America/Chicago');
        insert op;
        
        ServiceTerritory territory=new ServiceTerritory(Name='abc',OperatingHoursId=op.Id,
                                                        SFS_Service_Report_Name__c='test service report',SFS_Service_Report_Phone__c='12364778',
                                                        IsActive=true);
        insert territory; 
        
        List<Product2> productsList=SFS_TestDataFactory.createProduct(2,false);
        insert productsList;
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  productsList[0].Id, UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id =  productsList[0].Id, UnitPrice = 12000, IsActive = true, CurrencyIsoCode='USD');
        insert customPrice;
        
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, false);
        divisionsList[0].SFS_Margin__c=20;
        divisionsList[0].SFS_Expense_margin__c=20;
        insert divisionsList;
             
        List<Schema.Location> Loc= SFS_TestDataFactory.createLocations(1,divisionsList[0].Id, false);
        Loc[0].IsInventoryLocation = true;      
        insert Loc;
        
        Schema.Address vis = new Schema.Address();
        vis.street = '2321 Druid Oaks NE';
        vis.city = 'Atlanta';
        vis.state = 'GA';
        vis.country = 'USA';
        vis.postalcode = '30329';
        vis.ParentId = loc[0].Id;
        insert vis;
        
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,Loc[0].Id,divisionsList[0].Id,svcAgreementList[0].Id,false);       
        woList[0].CurrencyIsoCode='USD';
        woList[0].Shipping_Account__c=accList[0].Id;
        woList[0].SFS_PO_Number__c=null;
        insert woList;
        List<ID> ids= new List<ID>();
        ids.add(woList[0].id);
        
        Id p = [select id from profile where name= 'System Administrator'].id;
        User SampleUser = new User(alias = 'Sample1', email='SampleUser1@gmail.com',
                                   emailencodingkey='UTF-8', lastname='Sample User1', languagelocalekey='en_US',
                                   localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                   timezonesidkey='America/Los_Angeles', username='SampleUserSFS1@gmail.com'
                                  );
        insert SampleUser;
        
        ServiceResource Serres = new ServiceResource(Name='Abc',ResourceType='T',RelatedRecordId=SampleUser.id);
        insert Serres;
        cafsl__Oracle_Quote__c oracquote = new cafsl__Oracle_Quote__c(Name='abc');
        insert oracquote;
        
        cafsl__Oracle_Quote_Line_Item__c oracle = new   cafsl__Oracle_Quote_Line_Item__c(cafsl__Oracle_Quote__c=oracquote.Id);
        insert oracle;
        
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(2,woList[0].Id,workTypeList[0].Id,false);
        woliList[0].SFS_PO_Number__c=null;
        woliList[1].SFS_PO_Number__c=null;
        woliList[0].SFS_Standard_Labor_Rate__c=10;
        woliList[0].SFS_Round_Trip_Time_Hrs__c=10;
        woliList[0].Location = loc[0];
        insert woliList;
        
        List<ServiceAppointment> saList=SFS_TestDataFactory.createServiceAppointment(1,woliList[0].Id,true);
        
        List<CAP_IR_Charge__c> chargeList=SFS_TestDataFactory.createCharge(1,woList[0].id,false);
        chargeList[0].SFS_PO_Number__c=null;
        chargeList[0].SFS_Charge_Type__c ='Freight / S&H';       
        insert chargeList;
        
        List<ProductRequest> prList=SFS_TestDataFactory.createProductRequest(1,woList[0].Id,woliList[0].Id,false);
        prList[0].SFS_Priority__c='100';
        insert prList;
        
        
        List<ProductRequestLineItem> prliList=SFS_TestDataFactory.createPRLI(1,prList[0].Id,productsList[0].Id,false);
        prliList[0].SFS_Freight_Charge__c=200;
        prliList[0].status='Shipped';
        insert prliList;
        List<ProductItem> proItemList=SFS_TestDataFactory.createProductItem(1,Loc[0].id,productsList[0].id,true);       
        
        List<Expense> expenseList=SFS_TestDataFactory.createExpense(2,woList[0].Id,woliList[0].Id,false);
        expenseList[1].ExpenseType='Outside Services';
        expenseList[1].SFS_Category__c='EHS Supplies';      
        insert expenseList;
        
        List<CAP_IR_Labor__c> laborHourList= SFS_TestDataFactory.createLaborHours(1,null,woliList[0].Id,false);
        laborHourList[0].CAP_IR_Labor_Rate__c='Standard';
        laborHourList[0]. CAP_IR_Quantity__c=2;
        insert laborHourList;
        OldMap.put(woList[0].id,woList[0]);
        Test.startTest();
        woList[0].status = 'Completed';
        update woList[0];        
        newList.add(woList[0]);
        SFS_WorkOrderHandler.createChargesForWOLI(newList,OldMap);      
        Test.stopTest();
    }
    
    
    //Test Method for createChargesForCompletedWOLI in WorkOrderHandler
    @isTest
    public static void createChargesForCompletedWOLITest(){
        Map<Id,WorkOrder> oldMap = new Map<id,WorkOrder>();
        List<WorkOrder> newList = new List<WorkOrder>();
        String accRecID2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[1].IRIT_Customer_Number__c='1234';
        accList[1].RecordTypeId=accRecID2;
        insert accList[1];
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].IRIT_Customer_Number__c='1234';
        accList[0].Type = 'Prospect';
        insert accList[0];
        Contact con = SFS_TestDataFactory.getContact();
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        insert svcAgreementList;
        
        OperatingHours op= new OperatingHours(Name='Normal',TimeZone='America/Chicago');
        insert op;
        
        ServiceTerritory territory=new ServiceTerritory(Name='abc',OperatingHoursId=op.Id,
                                                        SFS_Service_Report_Name__c='test service report',SFS_Service_Report_Phone__c='12364778',
                                                        IsActive=true);
        insert territory; 
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        List<Schema.Location> Loc= SFS_TestDataFactory.createLocations(1,divisionsList[0].Id, false);
        Loc[0].IsInventoryLocation = true;      
        insert Loc;
        
        
        List<Product2> prodList = SFS_TestDataFactory.createProduct(1, true);
        prodList[0].productCode = '00250506';
        update prodList[0];
        
        Id pb = Test.getStandardPricebookId();       
        
        List<PricebookEntry> pbe = SFS_TestDataFactory.createPricebookEntry(1, false);       
        pbe[0].Pricebook2Id = pb;
        insert pbe[0];
        
        Pricebook2 servicePricebook = new Pricebook2();
        servicePricebook.IsActive = true;
        servicePricebook.Name = 'Service Pricebook Test';                         
        insert servicePricebook;
        id pricebookx = servicePricebook.id;
        
        Product2 pro = new Product2(Name = 'testProduct');
        insert pro;
        
        PricebookEntry entry = new PricebookEntry();
        entry.Pricebook2Id = pricebookx;
        entry.Product2Id = pro.Id;
        entry.UnitPrice = 2.00;
        entry.IsActive = true;
        
        
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(2,accList[0].Id,Loc[0].id,divisionsList[0].Id,null,false);
        woList[0].SFS_Bill_to_Account__c=accList[1].Id;
        woList[0].SFS_PO_Number__c='123';
        woList[1].SFS_PO_Number__c='123';
        woList[0].Pricebook2Id=pricebookx;
        woList[0].SFS_PO_Expiration_Date__c=System.today()+1;
        insert woList;
        List<CAP_IR_Charge__c> chargeList=SFS_TestDataFactory.createCharge(1,woList[0].id,false);        
        chargeList[0].SFS_PO_Number__c='123';
        chargeList[0].SFS_Charge_Type__c ='Freight / S&H';       
        insert chargeList;
        
        Id p = [select id from profile where name= 'System Administrator'].id;
        User SampleUser = new User(alias = 'Sample1', email='SampleUser1@gmail.com',
                                   emailencodingkey='UTF-8', lastname='Sample User1', languagelocalekey='en_US',
                                   localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                   timezonesidkey='America/Los_Angeles', username='SampleUserSFS1@gmail.com'
                                  );
        insert SampleUser;
        
        List<ID> ids= new List<ID>();
        Set<ID> ids1= new Set<ID>();
        ids.add(woList[0].id);
        ids1.add(woList[0].id);
        ServiceResource Serres = new ServiceResource(Name='Abc',ResourceType='T',RelatedRecordId=SampleUser.id);
        insert Serres;
        
        cafsl__Oracle_Quote__c oracquote = new cafsl__Oracle_Quote__c(Name='abc');
        insert oracquote;
        
        cafsl__Oracle_Quote_Line_Item__c oracle = new   cafsl__Oracle_Quote_Line_Item__c(cafsl__Oracle_Quote__c=oracquote.Id);
        insert oracle;
        
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        woList[0].SFS_Bill_to_Account__c=accList[1].Id;
        update woList;
        
        
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(2,woList[0].Id,workTypeList[0].Id,false);
        woliList[0].Status='Completed';
        woliList[1].Status='Completed';
        woliList[0].SFS_PO_Number__c='123';
        woliList[1].SFS_PO_Number__c='123';
        woliList[0].SFS_Standard_Labor_Rate__c=10;
        woliList[0].SFS_Round_Trip_Time_Hrs__c=10;
        woliList[0].Location = loc[0];
        woliList[0].SFS_Coolant_Changed__c='No';
        woliList[0].SFS_Asset_Run_Hours__c=2;
        insert woliList;
        
        woList[0].status = 'Open';
        update woList;
        
        List<ProductRequest> prList=SFS_TestDataFactory.createProductRequest(1,woList[0].Id,woliList[0].Id,false);
        prList[0].SFS_Priority__c='100';
        insert prList;
      
        List<Expense> expenseList=SFS_TestDataFactory.createExpense(3,null,woliList[0].Id,false);
        expenseList[0].ExpenseType='Outside Services';
        expenseList[0].SFS_Category__c='EHS Supplies';
        expenseList[2].ExpenseType='Outside Services';
        expenseList[2].SFS_Category__c='EHS Supplies';
        expenseList[1].WorkOrderId=woList[0].Id;
        expenseList[1].SFS_Work_Order_Line_Item__c=null;
        expenseList[1].ExpenseType='Purchased Expense';
        expenseList[1].SFS_Transaction_Status__c='Open';
        insert expenseList;
        
        OldMap.put(woList[0].id,woList[0]);
        Test.startTest();            
        
        
        SFS_WorkOrderHandler.createChargesForCompletedWOLI(ids);
        SFS_WorkOrderHandler.updateChargeAmount(ids1);
        try{
            woList[0].Status = 'Completed';
            update woList;
            newList.add(woList[0]);
            SFS_WorkOrderHandler.checkPurchaseExpense(OldMap,newList);
        }
        catch(Exception e){
            System.assert(e.getMessage().contains('WO/WOLI workflow error - Purchased expenses must be receipted'));
        }
        Test.stopTest();
    }
    
    
    @isTest
    public static void sendServiceReportAttachmentToMultipleRecTest(){
        List<Account> acc = SFS_TestDataFactory.createAccounts(1, False);
        String accRecID3 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
        List<Account> bill2 = SFS_TestDataFactory.createAccounts(1, False);
        bill2[0].Currency__c = 'USD';
        bill2[0].IRIT_Customer_Number__c='1234';
        bill2[0].RecordTypeId=accRecID3;
        insert bill2;
        acc[0].Bill_To_Account__c = bill2[0].Id;
        acc[0].Type = 'Prospect';
        acc[0].IRIT_Customer_Number__c='1234';
        insert acc;
        
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, True);
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1, div[0].Id, True);
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acc[0].Id, false);
        sc[0].SFS_Rental_Location1__c = loc[0].Id;
        insert sc;
        Id p = [select id from profile where name= 'System Administrator'].id;
        User SampleUser = new User(alias = 'Sample1', email='SampleUser1@gmail.com',
                                   emailencodingkey='UTF-8', lastname='Sample User1', languagelocalekey='en_US',
                                   localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                   timezonesidkey='America/Los_Angeles', username='SampleUserSFS1@gmail.com'
                                  );
        insert SampleUser;
        User testUser2 = SFS_TestDataFactory.createTestUser('System Administrator',true);
        testUser2.ManagerId = SampleUser.Id;
        testUser2.Division = div[0].Id;
        testUser2.Street = '2342 Appalachian Way\nUnit 156';
        testUser2.City = 'Montreat';
        testUser2.State = 'NC';
        testUser2.PostalCode = '28757';
        testUser2.Country = 'United States';
        testUser2.Phone = '8989898989';     
        update testUser2;
        
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(2, acc[0].Id, loc[0].Id, div[0].Id, sc[0].Id, true);
        wo[0].ServiceReportTemplateId = '0SL3a000000blKMGAY';
        update wo[0];
        
        Contact con=new Contact();
        con=SFS_TestDataFactory.getContact();
        SFS_Work_Order_Contacts__c wOContacts =new SFS_Work_Order_Contacts__c();
        wOContacts.SFS_Work_Order__c=wo[0].id;
        wOContacts.SFS_Contact__c=con.id;
        Insert wOContacts;
        
        ContentVersion contentVersion = new ContentVersion(
            Title          = 'a picture',
            PathOnClient   = 'Pic.jpg',
            VersionData    = Blob.valueOf('Test Content'),
            IsMajorVersion = true);
        insert contentVersion;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = wo[0].Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.ShareType = 'V';
        cdl.Visibility = 'AllUsers';
        insert cdl;
        
        
        ServiceReport sr = new ServiceReport();
        List<ServiceReport> srlist = new List<ServiceReport>();
        sr.ParentId = wo[0].Id;      
        sr.DocumentBody = Blob.valueOf('Test Content') ; 
        sr.DocumentContentType ='application/pdf';
        sr.DocumentName='Test';
        srlist.add(sr);
        Test.startTest();
        try{
            Insert srlist;
            
            system.runAs(testUser2){               
                SFS_WorkOrderHandler.sendServiceReportAttachmentToMultipleRec(wo);            
            }
        }
         catch(Exception e){
            System.assert(e.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY, SFS_ServiceReportTrigger: execution of AfterInsert'));          
        }
        Test.stopTest();
    }
    
     @isTest
    public static void updateWarrantyJobCodeTest(){
         Map<Id,WorkOrder> oldWOMap = new Map<id,WorkOrder>();
        List<WorkOrder> newListCode = new List<WorkOrder>();
         List<Account> acc = SFS_TestDataFactory.createAccounts(1, False);
        String accRecID3 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
        List<Account> bill2 = SFS_TestDataFactory.createAccounts(1, False);
        bill2[0].Currency__c = 'USD';
        bill2[0].IRIT_Customer_Number__c='1234';
        bill2[0].RecordTypeId=accRecID3;
        insert bill2;
        acc[0].Bill_To_Account__c = bill2[0].Id;
        acc[0].Type = 'Prospect';
        acc[0].IRIT_Customer_Number__c='1234';
        insert acc;
        
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, True);
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1, div[0].Id, True);
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(2, acc[0].Id, loc[0].Id, div[0].Id, null, false);
        wo[0].SFS_Warranty_Job_Code__c= 'Test1'+';'+'Test2';
        insert wo[0];
        OldWOMap.put(wo[0].id,wo[0]);
        Test.startTest();
        wo[0].SFS_Warranty_Job_Code__c= 'Test1WO'+';'+'Test2WO';
        update wo[0];
        newListCode.add(wo[0]);
        SFS_WorkOrderHandler.updateWarrantyJobCode(OldWOMap,newListCode);
        Test.stopTest();
    }
    
}