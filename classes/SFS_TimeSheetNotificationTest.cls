@isTest
public class SFS_TimeSheetNotificationTest {
    static testMethod void testMethod1() 
    {
        List<TimeSheet> lstTimesheet= new List<TimeSheet>();
        User usr = SFS_TestDataFactory.createTestUser('SFS Service Technician', True);
        User usr1 = SFS_TestDataFactory.createTestUser('SFS Service', False);
        usr1.Username = 'managersfs@testuser.com';
        insert usr1;
        usr.ManagerId = usr1.Id;
        update usr;
        ServiceResource sr = new ServiceResource();
        sr.RelatedRecordId = usr.Id;
        sr.Name = 'test';
        sr.ResourceType = 'T';
        insert sr;        
        
        for(Integer i=0 ;i <200;i++)
        {
            TimeSheet ts = new TimeSheet();
            ts.StartDate = System.today();
            ts.EndDate = System.today();
            ts.ServiceResourceId = sr.Id;
            ts.Status = 'New';
            lstTimesheet.add(ts);
        }             
        
        insert lstTimesheet;
        Test.startTest();
        
        SFS_TimeSheetNotification obj = new SFS_TimeSheetNotification();
        String jobId = System.scheduleBatch(obj, 'timesheetjob', 2);
		obj.execute(null); 
        
        Test.stopTest();
    }
}