public class SFS_EntitlementHandlerBatch implements Database.Batchable<sObject> {


    // FINAL STRING iQuery = 'SELECT Id, StartDate, EndDate, AssetId, ServiceContractId, SFS_Agreement_Type__c, SFS_PackageCARE_PM_only_PCPM__c, CAP_IR_Status__c FROM Entitlement WHERE (StartDate = TODAY OR EndDate = YESTERDAY) AND SFS_Coverage_Type__c = \'Service Contract\' AND AssetId != NULL AND ServiceContractId != NULL';
    FINAL STRING iQuery = 'SELECT Id, StartDate, EndDate, AssetId, CAP_IR_Status__c, ServiceContractId, SFS_Agreement_Type__c, SFS_PackageCARE_PM_only_PCPM__c FROM Entitlement'
                            + ' WHERE SFS_Coverage_Type__c = \'Service Contract\' AND AssetId != NULL AND ServiceContractId != NULL'
                            + ' AND ('
                                + '(StartDate <= TODAY AND EndDate > TODAY AND CAP_IR_Status__c NOT IN (\'Expired\', \'Inactive\') AND Processed_Start__c = false)'
                                + ' OR (EndDate < TODAY AND (CAP_IR_Status__c = NULL OR CAP_IR_Status__c IN (\'Active\')) AND Processed_End__c = false))';
    Final string fQuery;

    // End: ((CAP_IR_Status__c = 'Active' && EndDate < TODAY) || (CAP_IR_Status__c = 'Expired')) && Processed_End__c = FALSE
    // Start: CAP_IR_Status__c = Active && StartDate <= TODAY && Processed_Start__c = FALSE

    public SFS_EntitlementHandlerBatch(){
        fQuery = iQuery;
    }

    public SFS_EntitlementHandlerBatch(String cQuery){
        if(String.isNotBlank(cQuery)){
            fQuery = cQuery;
        }
        else{
            fQuery = iQuery;
        }
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(fQuery);
    }

    public void execute(Database.BatchableContext bc, List<Entitlement> entitlements) {
        System.debug('Working on ' + entitlements.size() + ' entitlements');
        List<Entitlement> toUpdate = new List<Entitlement>();
        // If entitlement ended yesterday
        Entitlement[] ended = getEnded(entitlements);
        if (ended.size() > 0) {
            Asset[] assets = new List<Asset>();
            for (Entitlement ent : ended) {
                Asset asset = new Asset(Id = ent.AssetId);
                asset = SFS_EntitlementHandler.clearAssetFlags(asset);
                assets.add(asset);
                ent.Processed_End__c = true;
                toUpdate.add(ent);
            }
            update assets;
        }

        // If entitlement started today
        Entitlement[] started = getStarted(entitlements);
        if (started.size() > 0) {
            SFS_EntitlementHandler.updateAssetEntitlementDetails(started);
        }
        for (Entitlement ent : started) {
            ent.Processed_Start__c = true;
            toUpdate.add(ent);
        }

        if(toUpdate.size() > 0){
            update toUpdate;
        }
    }

    public void finish(Database.BatchableContext bc) {
        System.debug('Finished Processing');
    }

    private static List<Entitlement> getStarted(List<Entitlement> entitlements) {
        List<Entitlement> started = new List<Entitlement>();
        for (Entitlement ent : entitlements) {
            if (ent.StartDate <= System.today() && ent.EndDate > System.today()) { // && ent.CAP_IR_Status__c != 'Inactive' && ent.CAP_IR_Status__c != 'Expired'
                started.add(ent);
            }
        }
        System.debug(started);
        return started;
    }

    private static List<Entitlement> getEnded(List<Entitlement> entitlements) {
        List<Entitlement> ended = new List<Entitlement>();
        for (Entitlement ent : entitlements) {
            if (ent.EndDate < System.today()) { // && (String.isBlank(ent.CAP_IR_Status__c) || ent.CAP_IR_Status__c == 'Active')
                ended.add(ent);
            }
        }
        System.debug(ended);
        return ended;
    }
}