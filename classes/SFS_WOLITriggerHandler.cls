/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 19/10/2021
* @description: Work Order Line Item Apex handler
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001      SIF-69     19/10/2021      Work Order Line Item Apex handler
============================================================================================================*/
public class SFS_WOLITriggerHandler {
    /**********************************************************************************************************
Purpose	: SIF-69 -- Checks all child Service Appointments are Complete, Cancelled or Cannot Complete before a WOLI can be
changed to Completed or Closed.
Parameters: Trigger.New and Trigger.Newmap from SFS_WOLITrigger
************************************************************************************************************/
    Public static void checkStatus(List<WorkOrderLineitem> lineItems,Map<Id,WorkOrderLineItem> newMap){
        try{
            Boolean error=false;
            Set<Id> lineItemIds=new Set<Id>();
            for(WorkOrderLineitem woli:lineItems){
                lineItemIds.add(woli.Id);
            }
            Map<Id,List<ServiceAppointment>> svcAppointmentMap=new Map<Id,List<ServiceAppointment>>();
            List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
            if(lineItemIds.size()>0){
                woliList=[Select Id,(Select Id,AppointmentNumber,Status from ServiceAppointments)
                          from WorkOrderLineItem where Id IN:lineItemIds];
            }
            If(woliList.size()>0){
                For(WorkOrderLineItem record:woliList){
                    svcAppointmentMap.put(record.Id,record.ServiceAppointments);
                }
            }
            for(WorkOrderLineitem woli:lineItems){
                String newStatus=newMap.get(woli.Id).Status;
      			Id ProfileId = userInfo.getProfileId();
                string ProfileName=[SELECT Id, Name From Profile where ID=:ProfileId].Name;
    			boolean isFieldServiceOptimization = ProfileName!= null && ProfileName == 'Field Service Optimization';
                If(newStatus=='Completed' || newStatus=='Closed')
                {
                    For(ServiceAppointment app:svcAppointmentMap.get(woli.Id)){
                        If(app.Status=='Completed' || app.Status=='Cannot Complete' || app.Status=='Canceled' || app.Status=='Not Required'){
                            error=false;
                        }
                        else{
                            error=true;
                            break;
                        }
                    }
                }
                if(!isFieldServiceOptimization && error==true){
                    woli.addError('All Service Appointment’s must be completed before Work Order Line Item Status is moved to completed or Closed.');
                }
            }
        }
        catch(Exception e){}
    }

 /**********************************************************************************************************
Purpose	: SIF-138-- whenever WOLI created with Location OR Location on the WOLI updated, all skills should be
created under the skill requirement related list based on the location.
Parameters: Trigger.New and Trigger.oldmap from SFS_WOLITrigger
************************************************************************************************************/


    Public static void createSkillReq(List<WorkOrderLineitem> lineItems,Map<Id,WorkOrderLineItem> oldMap){
        Set<Id> locationIds=new Set<Id>();
        Set<Id> woliIds=new Set<Id>();
        for(WorkOrderLineitem woliRec:lineItems){
            locationIds.add(woliRec.LocationId);
            woliIds.add(woliRec.Id);
        }
        //Get Skill Requirement records for the workorderlineitems
        Map<Id,List<SkillRequirement>> woliToSkillReq=new Map<Id,List<SkillRequirement>>();
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        if(woliIds.size()>0){

            woliList=[Select Id,(Select Id,RelatedRecordId,SkillId from SkillRequirements)
                      from WorkOrderLineItem where Id IN :woliIds];

        }
        for(WorkOrderLineItem woli:woliList){
            woliToSkillReq.put(woli.Id,woli.SkillRequirements);
        }
        // Get Location Skill requirement with the WOLI's Location
        Map<Id,List<SFS_Location_Skill_Requirement__c>> woliToLocSkillReq=new Map<Id,List<SFS_Location_Skill_Requirement__c>>();
        List<Schema.Location> locList= new List<Schema.Location>();
        if(locationIds.size()>0){
            locList=[Select Id,(Select Id, Location__c,Skill_Id__c from Location_Skill_Requirements__r )
                     from Location where Id IN: locationIds];
        }
        if(locList.size()>0){
            for(Schema.Location locationRec:LocList){
                woliToLocSkillReq.put(locationRec.Id,locationRec.Location_Skill_Requirements__r);
            }
        }
        //this list holds the skill requirement that needs to be inserted
        List<SkillRequirement> newRec=new List<SkillRequirement>();
        for(WorkOrderLineitem woliRec:lineItems){
            WorkOrderLineItem oldRecord= new WorkOrderLineItem();
            if(oldMap!=null){
                oldRecord = oldMap.get(woliRec.Id);
            }
            if(woliRec.LocationId!= null){
            if((oldMap==null && woliRec.LocationId!= null) || (oldMap.size() > 0 && oldRecord.LocationId != null && oldRecord.LocationId!=woliRec.LocationId) || (oldMap.size() > 0 && oldRecord.LocationId == null && oldRecord.LocationId!=woliRec.LocationId)){
                List<SkillRequirement> getSRRec = woliToSkillReq.get(woliRec.Id);
                List<SFS_Location_Skill_Requirement__c> getLSRRec=woliToLocSkillReq.get(woliRec.LocationId);
                if(getLSRRec!=null){
                    for(SFS_Location_Skill_Requirement__c locationSkillRec: getLSRRec){
                        Boolean skillExists=false;
                        for(SkillRequirement skillReqRec: getSRRec){
                            //checks all the location skills with already available skills(WOLI's skill(Skill Requirements)) to remove the duplicates
                            if(locationSkillRec.Skill_Id__c == skillReqRec.SkillId ){
                                skillExists=true;
                            }
                        }
                        //if skillExists = true then the skill is already there no need to insert the skills
                        //if false insert the skills
                        if(skillExists!=true){
                            SkillRequirement newSkillReq =new SkillRequirement();
                            newSkillReq.SkillId=locationSkillRec.Skill_Id__c;
                            newSkillReq.SFS_Skill_From_Location__c =true;
                            newSkillReq.RelatedRecordId =woliRec.Id;
                            system.debug('skillid'+locationSkillRec.Skill_Id__c);
                            system.debug('woliid'+woliRec.Id);
                            system.debug('rec'+newSkillReq);
                            newRec.add(newSkillReq);

                        }
                    }
                }


            }
            }
        }
        system.debug('125--'+newRec);
        insert newRec;
    }

  /**********************************************************************************************************
Purpose	: SIF-138-- On updation of Location On WOLI, delete the old skill requirements under that WOLI
Parameters: Trigger.New and Trigger.oldmap from SFS_WOLITrigger
************************************************************************************************************/
    public static void deleteOldLocSkill(List<WorkOrderLineItem> newList,Map<Id,WorkOrderLineItem> oldMap){


        List<SkillRequirement> skillReqToBeDeleted = new List<SkillRequirement>();
       // Set<Id> locationIds=new Set<Id>();
        Map<Id,WorkOrderLineItem> woliList = new Map<Id,WorkOrderLineItem>();
        List<SkillRequirement> skillList = new List<SkillRequirement>();

        for(WorkOrderLineItem woli : newList)  {
            if(woli.Id != null){
                woliList.put(woli.Id, woli);
               // locationIds.add(woli.LocationId);
            }

        }
        skillList = [Select Id, SkillId, SFS_Skill_From_Location__c, RelatedRecordId FROM SkillRequirement WHERE RelatedRecordId IN:woliList.keySet()];
            for(WorkOrderLineitem woli : newList){
                WorkOrderLineItem oldRecord= new WorkOrderLineItem();
                if(oldMap!=null){
                    oldRecord = oldMap.get(woli.Id);
                    system.debug('===='+oldRecord);
                }
                for(SkillRequirement skill:skillList){// skillList holds the old skillrequirements on WOLI
                if((oldMap.size() > 0 && oldRecord.LocationId != null && oldRecord.LocationId!=woli.LocationId)){
                    if(skill.SFS_Skill_From_Location__c== true){ // SFS_Skill_From_Location__c== true it means the skills are created by the locations only
                        skillReqToBeDeleted.add(skill);
                    }
                }
            }
            if(!skillReqToBeDeleted.isEmpty()){
                system.debug('hi'+skillReqToBeDeleted);
                delete skillReqToBeDeleted;
            }
        }
    }
/*************************************************************************************************************
Purpose : SIF-48 Get the Asset Parts Required records based on the WOLI's Maintenance Work Rule's Maintenance Asset
1. If the Work Type work scope contains Asset Part Required work scope, Part Required record will be created
2. The Asset Part Required records with Delete After Use=true will be created as part required records under WOLI and will be deleted under Asset
3. If Product Required exist for same Product in work type and Asset Part Required, deleting the Part Required in woli that
is created from work type.
****************************************************************************************************************/
    Public static void createPartsRequired(List<WorkOrderLineItem> woliList){
        System.debug('@@@CreatePartsRequired');
        try{
            Set<Id> woliIds=new Set<Id>();
            for(WorkOrderLineItem woli:woliList){
                woliIds.add(woli.Id);
            }
            system.debug('191--'+woliIds);
            String PM='Preventive Maintenance';

            String queryString='select Id,LineItemNumber,AssetId,WorkOrder.WorkOrderNumber,WorkOrder.SFS_CPQ_Transaction_Id__c,WorkType.SFS_Work_Scope1__c,MaintenanceWorkRuleId,MaintenanceWorkRule.ParentMaintenanceRecord.AssetId,';
            queryString+='(Select Id,Product2Id,Product2.SFS_Superceded__c,Product2.Status__c,QuantityRequired from ProductsRequired) from WorkOrderLineItem where Id IN:woliIds and (MaintenanceWorkRuleId!=null or WorkOrder.SFS_Work_Order_Type__c =:PM) ';

            system.debug('201--'+queryString);
            List<WorkOrderLineItem> woliDetails = Database.query(queryString);
            system.debug('202--'+woliDetails);
            Set<Id> assetIds=new Set<Id>();
            map<Id,List<ProductRequired>> productRequiredWoliMap=new map<Id,List<ProductRequired>>();
            for(WorkOrderLineItem woli:woliDetails){
                if(!test.isRunningTest() && woli.MaintenanceWorkRuleId!=null){
                    assetIds.add(woli.MaintenanceWorkRule.ParentMaintenanceRecord.AssetId);
                }else{
                    assetIds.add(woli.AssetId);
                }
                productRequiredWoliMap.put(woli.Id,woli.ProductsRequired);
            }
            System.debug('213--'+assetIds);
            List<Asset> assetList=[Select Id,Name,(Select Id,Product__c,Product__r.name,SFS_Quantity__c,SFS_Delete_After_Use__c,
                                                   SFS_0k__c,SFS_1k__c,SFS_2k__c,SFS_4k__c,SFS_6k__c,SFS_8k__c,SFS_12k__c,SFS_16k__c,SFS_24k__c from CAP_IR_Asset_Product_Required__r)
                                   from Asset where Id IN : assetIds];
            system.debug('217--'+assetlist);
            Map<Id,List<CAP_IR_Asset_Part_Required__c>> partsRequiredMap=new Map<Id,List<CAP_IR_Asset_Part_Required__c>>();
            for(Asset record:assetList){
                partsRequiredMap.put(record.Id,record.CAP_IR_Asset_Product_Required__r);
            }
            system.debug('222--'+partsRequiredMap);
            List<ProductRequired> productRequiredList=new List<ProductRequired>();
            Set<Id> deletionIds=new Set<Id>();
            List<ProductRequired> existingPartsfromWT=new List<ProductRequired>();
            System.debug('198--'+woliDetails);
            for(WorkOrderLineItem woli:woliDetails){
                List<CAP_IR_Asset_Part_Required__c> partsRequired = new List<CAP_IR_Asset_Part_Required__c>();
                if(!test.isRunningTest() && woli.MaintenanceWorkRuleId!=null){
                    partsRequired=partsRequiredMap.get(woli.MaintenanceWorkRule.ParentMaintenanceRecord.AssetId);
                }else{
                    partsRequired=partsRequiredMap.get(woli.AssetId);
                }
                Map<Id,List<CAP_IR_Asset_Part_Required__c>> productToPartRequiredMap=new Map<Id,List<CAP_IR_Asset_Part_Required__c>>();
                if(partsRequired.size()>0){
                    for(CAP_IR_Asset_Part_Required__c record:partsRequired){
                        if(productToPartRequiredMap.size()==0 || (!productToPartRequiredMap.containsKey(record.Product__c))){
                             productToPartRequiredMap.put(record.Product__c,new List<CAP_IR_Asset_Part_Required__c>{record});
                        }else{
                            List<CAP_IR_Asset_Part_Required__c> partsList=productToPartRequiredMap.get(record.Product__c);
                            partsList.add(record);
                            productToPartRequiredMap.put(record.Product__c,partsList);
                        }
                    }
                }
                if(productToPartRequiredMap.keyset().size()>0){
                    System.debug('Found Products Required' + productToPartRequiredMap);
                    for(Id productId:productToPartRequiredMap.keyset()){
                        List<CAP_IR_Asset_Part_Required__c> partsRequiredList=productToPartRequiredMap.get(productId);
                        System.debug(productId);
                        system.debug(partsRequiredList);
                        Boolean checkDeleteafterUse=false;
                        List<ProductRequired> partRequiredWoli=productRequiredWoliMap.get(woli.Id);
                        if(partsRequiredList.size()>1){
                            System.debug('If two or more Asset Part '+partsRequiredList);
                            for(CAP_IR_Asset_Part_Required__c record:partsRequiredList){
                                System.debug('Condition check '+record.SFS_Delete_After_Use__c);
                                if(record.SFS_Delete_After_Use__c==true){
                                    checkDeleteafterUse=true;
                                    if(record.SFS_Quantity__c!=null && record.SFS_Quantity__c>0){
                                        ProductRequired pr=new ProductRequired(ParentRecordId=woli.Id,
                                                                               Product2Id=record.Product__c,
                                                                               QuantityRequired=record.SFS_Quantity__c);
                                        productRequiredList.add(pr);
                                    }
                                    deletionIds.add(record.Id);
                                }
                            }
                        }
                        if(checkDeleteafterUse==false){
                            String workScope=woli.WorkType.SFS_Work_Scope1__c;
                            Boolean createPartRequired=false;
                            for(ProductRequired existingRec:partRequiredWoli){
                                if(existingRec.Product2Id==partsRequiredList[0].Product__c){
                                    existingPartsfromWT.add(existingRec);
                                }
                            }
                            for(CAP_IR_Asset_Part_Required__c record:partsRequiredList){
                                if(workScope=='0' && record.SFS_0k__c==true){
                                	createPartRequired=true;
                            	}else if(workScope=='1' && record.SFS_1k__c==true){
                            	    createPartRequired=true;
                            	}else if(workScope=='2' && record.SFS_2k__c==true){
                                	createPartRequired=true;
                            	}else if(workScope=='4' && record.SFS_4k__c==true){
                                	createPartRequired=true;
                            	}else if(workScope=='6' && record.SFS_6k__c==true){
                                	createPartRequired=true;
                            	}else if(workScope=='8' && record.SFS_8k__c==true){
                                	createPartRequired=true;
                            	}else if(workScope=='12' && record.SFS_12k__c==true){
                                	createPartRequired=true;
                            	}else if(workScope=='16' && record.SFS_16k__c==true){
                                	createPartRequired=true;
                            	}else if(workScope=='24' && record.SFS_24k__c==true){
                                	createPartRequired=true;
                            	}else if(workScope=='48' && record.SFS_48k__c==true){
                                	createPartRequired=true;
                            	}
                            }

                            Set<Id> productIds = new Set<Id>();
                            productIds.add(partsRequiredList[0].Product__c);
                            List<Product2> products = [SELECT Id, Name, SFS_Superceded__c FROM Product2 WHERE Id IN :productIds and SFS_Superceded__c = false];

                            if(createPartRequired==true && partsRequiredList[0].SFS_Quantity__c!=null && partsRequiredList[0].SFS_Quantity__c>0 && products.size()>0 && woli.WorkOrder.SFS_CPQ_Transaction_Id__c==null){
                                ProductRequired pr=new ProductRequired(ParentRecordId=woli.Id,
                                                                       Product2Id=partsRequiredList[0].Product__c,
                                                                       QuantityRequired=partsRequiredList[0].SFS_Quantity__c);
                                productRequiredList.add(pr);
                            }
                        }
                    }
                }
            }
            system.debug('241--'+productRequiredList);
            system.debug('242--'+deletionIds);
            delete existingPartsfromWT;
            insert productRequiredList;
            if(deletionIds.size()>0){//added the condition for SOQL error
                List<CAP_IR_Asset_Part_Required__c> deletionList=[Select Id from CAP_IR_Asset_Part_Required__c where Id IN:deletionIds];
                delete deletionList;
            }
        }Catch(Exception e){
            System.debug('@@@ EXCEPTION at WOLI Create Parts Required Method:   ' + e);
        }
    }
/*Purpose : SIF-35 when a WOLI is created from Maintenance Plan
1. If an Open Product Request doesn't exist under woli, create a part request and part request line items
for the parts required under the woli
2. If an Open Part Request exist under WOLI, Create Part Request Line Items for the Parts Required under WOLI*/
    Public static void createParts(List<WorkOrderLineItem> recordList){
        try{
            Id prRecordTypeId = Schema.SObjectType.ProductRequest.getRecordTypeInfosByDeveloperName().get('Service').getRecordTypeId();
            Id prliRecordTypeId = Schema.SObjectType.ProductRequestLineItem.getRecordTypeInfosByDeveloperName().get('Service').getRecordTypeId();
            Set<Id> woliIds=new Set<Id>();
            Set<Id> workTypeIds=new Set<Id>();
            Set<Id> workOrderIds=new Set<Id>();
            for(WorkOrderLineItem woli:recordList){
                if(!Test.isRunningTest() && woli.MaintenanceWorkRuleId != null && woli.WorkTypeId!=null){
                    woliIds.add(woli.Id);
                    workTypeIds.add(woli.WorkTypeId);

                }else{
                    if(woli.WorkTypeId!=null){
                        woliIds.add(woli.Id);
                        workTypeIds.add(woli.WorkTypeId);
                    }
                }
            }
            String PM='Preventive Maintenance';
            Map<Id,List<ProductRequest>> woliToPR=new Map<Id,List<ProductRequest>>();
            Map<Id,List<ProductRequired>> partRequiredMap=new Map<Id,List<ProductRequired>>();
            List<WorkOrderLineItem> woliList=[Select Id,MaintenanceWorkRuleId,WorkTypeId,WorkOrder.AccountId,WorkOrder.Contact.Phone,WorkOrder.ContactId,WorkOrder.SFS_Division__c,AssetId,WorkOrder.ServiceContract.SFS_Shipping_Instructions__c,
                                              WorkOrder.ServiceContract.SFS_Shipping_Method__c,WorkOrder.ServiceContract.SFS_Division__c,WorkOrder.Account.ShippingCity,
                                              WorkOrder.Account.ShippingStreet,WorkOrder.Account.ShippingState,WorkOrder.Account.ShippingCountry,WorkOrder.Account.ShippingPostalCode,WorkOrder.Contact.FirstName,WorkOrder.Contact.LastName,
                                              WorkOrder.MaintenancePlan.StartDate,WorkOrder.ServiceTerritory.SFS_Requested_Ship_Date_Offset__c,MaintenancePlanId,
                                              (Select Id,Status from ProductRequests where Status='Open'),
                                              (Select Id,Product2Id,Product2.SFS_Superceded__c,Product2.Status__c,QuantityRequired from ProductsRequired
                                                    where Product2.SFS_Superceded__c=false and Product2.Status__c!='OBSOLETE')
                                              from WorkOrderLineItem where Id IN : woliIds and (MaintenanceWorkRuleId!=null or WorkOrder.SFS_Work_Order_Type__c =:PM)];
            if(woliList.size()>0){
                for(WorkOrderLineItem woli:woliList){
                    if(woli.ProductRequests.size()>0){
                        woliToPR.put(woli.Id,woli.ProductRequests);
                    }
                    partRequiredMap.put(woli.Id, woli.ProductsRequired);
                }
            }
            List<ProductRequest> newPrList = new List<ProductRequest>();
            List<ProductRequestLineItem> prliList=new List<ProductRequestLineItem>();
            String strStatus,strOpen;
            if(woliList.size()>0){
                for(WorkOrderLineItem woli:woliList){
                    String strMP=woli.MaintenancePlanId;

                    List<ProductRequest> prList = new List<ProductRequest>();
                    if(woliToPR.containskey(woli.Id)){
                        prList = woliToPR.get(woli.Id);
                    }
                    if(prList.size()==0){
                        Integer offset;
                        Date requestedShipDate;
                        String contactName;
                        Boolean shipDate=false;
                        contactName= woli.WorkOrder.Contact.FirstName+' '+woli.WorkOrder.Contact.LastName;
                        if(woli.WorkOrder.ServiceTerritory.SFS_Requested_Ship_Date_Offset__c != null){
                            offset=(1) * Integer.valueOf(woli.WorkOrder.ServiceTerritory.SFS_Requested_Ship_Date_Offset__c);
                            requestedShipDate=System.today().addDays(offset);
                            shipDate=true;
                        }
                        if(shipDate==false){
                            requestedShipDate=System.today();
                        }
                        ProductRequest partRequest=new ProductRequest(AccountId=woli.WorkOrder.AccountId,
                                                                      SFS_Consumables_Ship_To_Account__c = woli.WorkOrder.AccountId,
                                                                      RecordTypeId=prRecordTypeId,
                                                                      SFS_Freight_Terms__c='Prepaid',
                                                                      SFS_No_Partials__c=true,
                                                                      SFS_Priority__c='250',
                                                                      SFS_Shipping_Instructions__c=woli.WorkOrder.ServiceContract.SFS_Shipping_Instructions__c,
                                                                      SFS_Shipping_Method__c=woli.WorkOrder.ServiceContract.SFS_Shipping_Method__c,
                                                                      SFS_Division__c=woli.WorkOrder.SFS_Division__c,
                                                                      Status='Open',
                                                                      SFS_Contact_Name__c=contactName,
                                                                      SFS_Contact_Phone__c=woli.WorkOrder.Contact.Phone,
                                                                      WorkOrderId=woli.WorkOrderId,
                                                                      WorkOrderLineItemId=woli.Id,
                                                                      ShipToStreet=woli.WorkOrder.Account.ShippingStreet,
                                                                      ShipToCity=woli.WorkOrder.Account.ShippingCity,
                                                                      ShipToState=woli.WorkOrder.Account.ShippingState,
                                                                      ShipToCountry=woli.WorkOrder.Account.ShippingCountry,
                                                                      ShipToPostalCode=woli.WorkOrder.Account.ShippingPostalCode,
                                                                      SFS_Requested_Ship_Date__c=requestedShipDate);
                        newPrList.add(partRequest);
                        System.debug('Debug PR Normal '+partRequest);
                    }
                    List<ProductRequired> partRequiredList= new List<ProductRequired>();
                    if(partRequiredMap.containsKey(woli.Id)){
                        partRequiredList=partRequiredMap.get(woli.Id);
                    }
                    system.debug('355--'+partRequiredList);
                    system.debug('Product Required--> '+partRequiredList);
                    for(ProductRequired pr : partRequiredList){
                        ProductRequestLineItem prli=new ProductRequestLineItem(WorkOrderId=woli.WorkOrderId,
                                                                              WorkOrderLineItemId=woli.Id,
                                                                              SFS_Asset__c=woli.AssetId,
                                                                              RecordTypeId=prliRecordTypeId,
                                                                              QuantityRequested=pr.QuantityRequired,
                                                                              Product2Id=pr.Product2Id,
                                                                              Status='Open');
                        prliList.add(prli);
                    }
                }
            }
            Database.SaveResult[] resultList = Database.insert(newPrList);
            Set<Id> prIds = new Set<Id>();
            for(Database.SaveResult result : resultList){
                prIds.add(result.getId());
            }
            List<ProductRequest> insertedPrList=[Select Id,WorkOrderLineItemId from ProductRequest where WorkOrderLineItemId IN : woliIds and (Status='Open' or Status='Open')];
            for(ProductRequest pr : insertedPrList){
                for(ProductRequestLineItem prli : prliList){
                    if(pr.WorkOrderLineItemId == prli.WorkOrderLineItemId){
                        prli.ParentId=pr.Id;
                    }
                }
            }
            insert prliList;
            System.debug('PRLI List --> '+prliList);
        }Catch(Exception e){
            System.debug('@@@Exception PRLI Creation in WOLIHandler ' + e);
        }
    }

  /*Purpose : SIF-3899 Validate that all Purchased Expense lines are receipted in Oracle before allowing WO to be completed
When there is any Purchased expesne record present on WO with the Transaction status not closed, it will not allow us to close the WOLI
*/
public static void checkPurchaseExpense(Map<Id,WorkOrderLineItem> woliOldMap,List<WorkOrderLineItem> woliNew){

        try{
            system.debug('woliNew**'+woliNew);
            list<Id> newWoIds = new list<Id>();
            for(WorkOrderLineItem woli :woliNew){
                newWoIds.add(woli.WorkOrderId);
            }
            system.debug('newWoIds**'+newWoIds);
            system.debug('woliOldMap**'+woliOldMap);
            List<Expense> expList = [Select Id,SFS_Transaction_Status__c,WorkOrderId from Expense where (ExpenseType = 'Purchased Expense' OR ExpenseType = 'Purchased Labor') AND WorkOrderId IN: newWoIds];
            system.debug('expList'+expList);
            Map<Id,List<String>> woExpMap = new  Map<Id,List<String>>();
            for(Expense exp:expList){
                List<String> tranStatusList = woExpMap.get(exp.WorkOrderId);
                if(tranStatusList!=null){
                    tranStatusList.add(exp.SFS_Transaction_Status__c);
                    woExpMap.put(exp.WorkOrderId,tranStatusList);
                }else{
                    List<String> newTranStatusList = new List<String>();
                    newTranStatusList.add(exp.SFS_Transaction_Status__c);
                    woExpMap.put(exp.WorkOrderId,newTranStatusList);
                }


            }
            system.debug('woExpMap'+woExpMap);
            if(!woExpMap.isEmpty()){

                for(WorkOrderLineItem woli:woliNew){
                    WorkOrderLineItem oldWoli = woliOldMap.get(woli.id);
                    List<String> expListData = woExpMap.get(woli.WorkOrderId);
                    if(oldWoli.Status!='Closed' && woli.Status=='Closed' &&
                       (expListData.contains('OPEN') || expListData.contains('PENDING') || expListData.contains('APPROVED') || expListData.contains('REJECTED'))){

                        woli.addError('WO/WOLI workflow error - Purchased expenses must be receipted');
                    }
                }
            }
        }
        catch(Exception e){
            system.debug('@@@exception'+e.getMessage());
        }
    }

    /*Purpose : SIF-4916 Do not allow user to Cancel the WOLI if -
	Any of the Service Appointments on WOLI has status other than Canceled
	Any of the Part Request on WOLI has status other than Canceled
	There is any Labor Cost Associated with WOLI
	There is any Expense Cost Associated with WOLI
	There is a consumed parts on the WOLI */

    public static void checkWOLICancellation(List<WorkOrderLineItem> newWorkOrderLineItems, Map<Id, WorkOrderLineItem> oldWorkOrderLineItemsMap) {
try {

            for (WorkOrderLineItem woli : newWorkOrderLineItems) {
                List<ServiceAppointment> serviceAppointments = [SELECT Id,Status FROM ServiceAppointment WHERE CTS_Work_Order_Line_Item__c = :woli.Id];
                List<ProductRequest> partRequests = [SELECT Id,Status FROM ProductRequest WHERE WorkOrderLineItemId = :woli.Id];
                List<Expense> expenses = [SELECT Id FROM Expense WHERE SFS_Work_Order_Line_Item__c = :woli.Id];
                List<CAP_IR_Labor__c> labors = [SELECT Id FROM CAP_IR_Labor__c WHERE CAP_IR_Work_Order_Line_Item__c = :woli.Id];
                List<ProductConsumed> partConsumed = [SELECT Id FROM ProductConsumed WHERE WorkOrderLineItemId = :woli.Id];

                for (ServiceAppointment sa : serviceAppointments) {
                    if (sa.Status != 'Canceled') {
                        woli.addError('Please ensure that all service appointments and Part Requests are in canceled status, also ensure that WOLI does not contain any labor, Expenses or part consumed before canceling the WOLI');
                    }}
                for (ProductRequest pr : partRequests) {
                    if (pr.Status != 'Canceled') {
                        woli.addError('Please ensure that all service appointments and Part Requests are in canceled status, also ensure that WOLI does not contain any labor, Expenses or part consumed before canceling the WOLI');
                    }}
                if (!expenses.isEmpty() || !labors.isEmpty() || !partConsumed.isEmpty() ) {
                    woli.addError('Please ensure that all service appointments and Part Requests are in canceled status, also ensure that WOLI does not contain any labor, Expenses or part consumed before canceling the WOLI');
                }

            }
        } catch (Exception ex) {
            System.debug('An exception occurred: ' + ex.getMessage());
        }
    }

    //SIF-4934
    public static void checkWOEntitlementMatches(List<WorkOrderLineItem> wolis){
        Set<Id> woIds = new Set<Id>();
        for(WorkOrderLineItem woli : wolis){
            woIds.add(woli.WorkOrderId);
        }

        Map<Id,WorkOrder> wos = new Map<Id,WorkOrder>([SELECT Id, ServiceContractId, Status FROM WorkOrder WHERE Id IN :woIds]);

        Set<Id> etIds = new Set<Id>();
        for(WorkOrderLineItem woli : wolis){
            etIds.add(woli.SFS_Entitlement__c);
        }

        Map<Id,Entitlement> ets = new Map<Id,Entitlement>([SELECT Id, ServiceContractId FROM Entitlement WHERE Id IN :etIds]);

        for(WorkOrderLineItem woli : wolis){
            WorkOrder wo = wos.get(woli.WorkOrderId);


            if(wo != null && woli.SFS_Entitlement__c == null && wo.ServiceContractId != null){
                //woli.addError('Error: Cannot Process Work Order Line Item with different Entitlements on Assets. Please create one Work Order per Entitlement.');
            }

            if(woli.SFS_Entitlement__c != null){
                if(ets.get(woli.SFS_Entitlement__c).ServiceContractId != wo.ServiceContractId && (wo.Status == 'New' || wo.Status == 'Open' || wo.Status == 'Completed')){
                    //woli.addError('Error2: Cannot Process Work Order Line Item with different Entitlements on Assets. Please create one Work Order per Entitlement.');
                }
            }
        }

    }
}