@IsTest
public class TSTC_SAP_WarrantyClaimStatus {

    @IsTest
    public static void test() {
        Warranty_Claim__c wc = new Warranty_Claim__c(SAP_Status__c = '', SAP_Messages__c = '');
        
        insert wc;
        
        CTRL_SAP_WarrantyClaimStatus.getStatus(wc.Id);
        CTRL_SAP_WarrantyClaimStatus.getStatus(null);
    }
}