/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 07/12/2021
* @description: Test Class for Product Request Line Item Apex handler
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001      SIF-33     07/12/2021     Test coverage for restrictDeletion method in SFS_PRLIHandler and SFS_ProductrequestLineItemTrigger
============================================================================================================*/
@isTest
private class SFS_PRLIHandlerTest {
//Test class for coverage of restrictDeletion method in SFS_PRLIHandler and SFS_ProductrequestLineItemTrigger
@isTest
    private Static void testRestrictDeletion(){
        RecordType prliType = [Select Id, Name, SObjectType FROM RecordType where Name ='Service' 
                                    AND SObjectType = 'ProductRequestLineitem'];
        RecordType productType = [Select Id, Name, SObjectType FROM RecordType where 
                                     SObjectType = 'Product2' Limit 1];
        Product2 product1=new Product2(Name='Testt',Recordtypeid=productType.Id,IsActive=true,SFS_Oracle_Orderable__c='Y',SFS_External_Id__c='123');
        insert product1;
        ProductRequest record=new ProductRequest(SFS_Freight_Plan_Number__c='123');
        insert record;
        ProductRequestLineitem item=new ProductRequestLineitem(ParentId=record.Id,QuantityRequested=4,RecordTypeId=prliType.Id,
                                                               status='Submitted',Product2Id=product1.Id);
        insert item;
       List<ProductRequestLineitem> prliList=new List<ProductRequestLineitem>();
        prliList.add(item);
        Test.startTest();
        try{
            SFS_PRLIHandler.restrictDeletion(prliList);
            delete item;
        }Catch(Exception e){
            system.assert(e.getMessage().contains('Product Request Line Item cannot be deleted once submitted'));
        }
        Test.stopTest();
    }
    @isTest
    private Static void testRestrictDeletionOpenStatus(){
        RecordType prliType = [Select Id, Name, SObjectType FROM RecordType where Name ='Service' 
                                    AND SObjectType = 'ProductRequestLineitem'];
        RecordType productType = [Select Id, Name, SObjectType FROM RecordType where 
                                     SObjectType = 'Product2' Limit 1];
        Product2 product1=new Product2(Name='Testt',Recordtypeid=productType.Id,IsActive=true,SFS_Oracle_Orderable__c='Y',SFS_External_Id__c='123');
        insert product1;
        ProductRequest record=new ProductRequest(SFS_Freight_Plan_Number__c='123');
        insert record;
        ProductRequestLineitem item=new ProductRequestLineitem(ParentId=record.Id,QuantityRequested=4,RecordTypeId=prliType.Id,
                                                               status='null',Product2Id=product1.Id);
        insert item;
       List<ProductRequestLineitem> prliList=new List<ProductRequestLineitem>();
        prliList.add(item);
        Test.startTest();  
            SFS_PRLIHandler.restrictDeletion(prliList);
            delete item;
            Test.stopTest();     
}
    @isTest
    private Static void testRestrictDeletionOpenStatus1(){
        RecordType prliType = [Select Id, Name, SObjectType FROM RecordType where Name ='Service' 
                                    AND SObjectType = 'ProductRequestLineitem'];
        RecordType productType = [Select Id, Name, SObjectType FROM RecordType where 
                                     SObjectType = 'Product2' Limit 1];
        Product2 product1=new Product2(Name='Testt',Recordtypeid=productType.Id,IsActive=true,SFS_Oracle_Orderable__c='Y',SFS_External_Id__c='123');
        insert product1;
        ProductRequest record=new ProductRequest(SFS_Freight_Plan_Number__c='123');
        insert record;
        ProductRequestLineitem item=new ProductRequestLineitem(ParentId=record.Id,QuantityRequested=4,RecordTypeId=prliType.Id,
                                                               status='Open',Product2Id=product1.Id);
        insert item;
       List<ProductRequestLineitem> prliList=new List<ProductRequestLineitem>();
        prliList.add(item);
        Test.startTest();  
            SFS_PRLIHandler.restrictDeletion(prliList);
            delete item;
            Test.stopTest();     
}
}