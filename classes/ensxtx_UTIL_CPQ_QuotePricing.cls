public with sharing class ensxtx_UTIL_CPQ_QuotePricing 
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_UTIL_CPQ_QuotePricing.class);

    // getSBOForensxtx_ENSX_Quote
    //
    // Performs the SAP pricing simulation
    public static ensxtx_SBO_EnosixPricing_Detail.EnosixOpportunityPricing getSBOForensxtx_ENSX_Quote(
        ensxtx_ENSX_Quote quote, Map<Integer, ensxtx_ENSX_QuoteLineMapping> preCalculateState, Map<Id, Product2> productMap)
    {
        logger.enterAura('getSBOForensxtx_ENSX_Quote', new Map<String, Object> {
            'quote' => quote
        });
        ensxtx_SBO_EnosixPricing_Detail.EnosixOpportunityPricing oppPricingDetail = new ensxtx_SBO_EnosixPricing_Detail.EnosixOpportunityPricing();
        try
        {
            string soldToParty = String.isNotBlank(quote.soldToParty) ? quote.soldToParty : ensxtx_UTIL_VCPricing.defaultCustomerNumber;
            if (String.isBlank(soldToParty))
            {
                throw new ensxtx_ENSX_Exceptions.SimulationException(System.Label.LBL_Account_Sold_To_Blank);
            }
            oppPricingDetail.SoldToParty = soldToParty;

            oppPricingDetail.SALES.SalesDocumentType = String.isNotEmpty(quote.salesDocType) ? quote.salesDocType : ensxtx_UTIL_VCPricing.defaultSalesDocType;
            oppPricingDetail.SALES.SalesOrganization = String.isNotEmpty(quote.SalesOrg) ? quote.SalesOrg : ensxtx_UTIL_VCPricing.defaultSalesOrg;
            oppPricingDetail.SALES.DistributionChannel = String.isNotEmpty(quote.salesDistChannel) ? quote.salesDistChannel : ensxtx_UTIL_VCPricing.defaultDistributionChannel;
            oppPricingDetail.SALES.Division = String.isNotEmpty(quote.salesDivision) ? quote.salesDivision : ensxtx_UTIL_VCPricing.defaultDivision;

            ensxtx_SBO_EnosixPricing_Detail.PARTNERS soldToPartner = new ensxtx_SBO_EnosixPricing_Detail.PARTNERS();
            soldToPartner.PartnerFunction = ensxtx_UTIL_Customer.SOLD_TO_PARTNER_CODE;
            soldToPartner.CustomerNumber = soldToParty;

            oppPricingDetail.PARTNERS.add(soldToPartner);

            if (String.isNotBlank(quote.shipToParty))
            {
                ensxtx_SBO_EnosixPricing_Detail.PARTNERS shipTo = new ensxtx_SBO_EnosixPricing_Detail.PARTNERS();
                shipTo.PartnerFunction = ensxtx_UTIL_Customer.SHIP_TO_PARTNER_CODE;
                shipTo.CustomerNumber = quote.shipToParty;
                oppPricingDetail.PARTNERS.add(shipTo);
            }

            Integer qlTot = quote.LinkedQuoteLines.size();
            for (Integer qlCnt = 0 ; qlCnt < qlTot ; qlCnt++)
            {
                ensxtx_ENSX_QuoteLine line = quote.LinkedQuoteLines[qlCnt];
                Product2 lineProduct = productMap.get(line.Product);
                if (!line.IsProductFeature && ensxtx_UTIL_SFProduct.isProductLinkedToMaterial(lineProduct))
                {
                    ensxtx_SBO_EnosixPricing_Detail.ITEMS itm = new ensxtx_SBO_EnosixPricing_Detail.ITEMS();
                    itm.ItemNumber = String.valueOf(preCalculateState.get(line.LineItem).SAPLineItem);
                    itm.Material = ensxtx_UTIL_SFProduct.getMaterialNumberFromProduct(lineProduct);
                    itm.Plant = line.ItemConfiguration.plant;
                    itm.OrderQuantity = line.Quantity;
                    itm.Materialgroup1 = ensxtx_UTIL_VCPricing.defaultMaterialgroup1;
                    itm.Materialgroup2 = ensxtx_UTIL_VCPricing.defaultMaterialgroup2;
                    oppPricingDetail.Items.add(itm);
                    ensxtx_ENSX_CPQ_QuoteCalculationService.applyQuoteLineRequestMappingRules(line, itm, oppPricingDetail);

                    // Variant Configuration
                    if (null != line.ItemConfiguration && null != line.ItemConfiguration.selectedCharacteristics && !line.ItemConfiguration.selectedCharacteristics.isEmpty())
                    {
                        Integer scTot = line.ItemConfiguration.selectedCharacteristics.size();
                        for (Integer scCnt = 0 ; scCnt < scTot ; scCnt++)
                        {
                            ensxtx_ENSX_Characteristic c = line.ItemConfiguration.selectedCharacteristics[scCnt];
                            if (c.UserModified != null && c.UserModified && String.isNotEmpty(c.CharacteristicID))
                            {
                                ensxtx_SBO_EnosixPricing_Detail.ITEMS_CONFIG cfg = new ensxtx_SBO_EnosixPricing_Detail.ITEMS_CONFIG();
                                cfg.ItemNumber = itm.ItemNumber;
                                cfg.CharacteristicID = c.CharacteristicID;
                                cfg.CharacteristicName = c.CharacteristicName;
                                cfg.CharacteristicValue = c.CharacteristicValue;
                                oppPricingDetail.ITEMS_CONFIG.add(cfg);
                            }
                        }
                    }
                }
            }

            ensxtx_ENSX_CPQ_QuoteCalculationService.applyQuoteRequestMappingRules(quote, oppPricingDetail);

            oppPricingDetail = ensxtx_UTIL_VCPricing.simulatePricing(oppPricingDetail);
            
        } catch (Exception ex) {
            logger.log(LoggingLevel.ERROR, 'There was an exception thrown when attempting to getSBOForensxtx_ENSX_Quote ', ex);
            throw ex;
        } finally { 
            logger.exit();
        }

        return oppPricingDetail;
    }
}