public class C_WarrantyInformation_Controller {

    @AuraEnabled
    public static Asset queryAsset(Id assetId){
        return [SELECT Id, Name, Warranty_Type__c, Warranty_Start_Date__c, Warranty_End_Date__c FROM Asset WHERE Id =: assetId];
    
    }
}