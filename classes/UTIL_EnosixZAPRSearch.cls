global with sharing class UTIL_EnosixZAPRSearch {

    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_ContactList.class);

    @TestVisible
    private static final String UNASSIGNED_USER_FNAME = 'LeadAssignment';
    @TestVisible
    private static final String UNASSIGNED_USER_LNAME = 'DummyUser';

    public static void updateOwner(Id leadId) {
        if (Test.isRunningTest()) {
            updateOwnerImpl(leadId);
        } else {
            updateOwnerFuture(leadId);
        }
    }

    @future(callout=true)
    public static void updateOwnerFuture(Id leadId) {
        updateOwnerImpl(leadId);
    }

    public static void updateOwnerImpl(Id leadId) {

        Lead lead = [SELECT Id, OwnerId, Address, City, State, PostalCode, Country, ProductCategory__c, FLD_Customer_Numbers__c,
                    FLD_Sales_Organization__c, FLD_Region__c, FLD_Country__c, FLD_County__c FROM Lead WHERE Id =: leadId LIMIT 1];

        switch on lead.ProductCategory__c {
            when 'Blower' {
                lead.FLD_Distribution_Channel__c = 'SB';
            }
            when 'ChampionUS' {
                lead.FLD_Distribution_Channel__c = 'CP';
            }
            when 'BlowerMDK' {
                lead.FLD_Distribution_Channel__c = 'MD';
            }
            when 'TransportMDK' {
                lead.FLD_Distribution_Channel__c = 'MT';
            }
            when 'VacuumMDK' {
                lead.FLD_Distribution_Channel__c = 'KV';
            }
            when 'Compressor' {
                lead.FLD_Distribution_Channel__c = 'CM';
            }
            when 'DVSystems' {
                lead.FLD_Distribution_Channel__c = 'DS';
                lead.FLD_Sales_Organization__c = '5340';
            }
            when 'High Pressure' {
                lead.FLD_Distribution_Channel__c = 'CM';
            }
            when 'LeROI' {
                lead.FLD_Distribution_Channel__c = 'CY';
            }
            when 'MPPumps' {
                lead.FLD_Distribution_Channel__c = 'SP';
            }
            when 'Oberdorfer' {
                lead.FLD_Distribution_Channel__c = 'SP';
            }
            when 'OFCompressor' {
                lead.FLD_Distribution_Channel__c = 'CM';
            }
            when 'Transport' {
                lead.FLD_Distribution_Channel__c = null;
            }
            when 'Vacuum' {
                lead.FLD_Distribution_Channel__c = 'ER';
            }
            when else {
                lead.FLD_Distribution_Channel__c = null;
            }
        }

        //Set "FLD_" field values from the existing lead address.  They won't be visible to the end user
        String tmpCountry = lead.Address.getCountry();
        if (tmpCountry == 'United States') tmpCountry = 'US';
        lead.FLD_Country__c = tmpCountry;
        lead.FLD_Region__c = lead.Address.getState();
        if (lead.FLD_Sales_Organization__c == null) lead.FLD_Sales_Organization__c = 'GDMI';
        
        String salesOrg = lead.FLD_Sales_Organization__c;
        String distributionChannel = lead.FLD_Distribution_Channel__c;
        String country = lead.FLD_Country__c;
        String region = lead.FLD_Region__c;
        String county = '';
        //Always look up the county in case the zipcode has changed, only for US and CA
        if(tmpCountry == 'US' || tmpCountry == 'CA'){
            county = getCounty(lead.Address);
            lead.FLD_County__c = county;
        }
        //lead.OwnerId = getDummyUser().Id;

        List<String> customerNumbers = new List<String>();
        //List of DCs that need to be used for the ZAPR search to get account numbers for the mobile (transport) product category
        List<String> mobileDCs = new List<String>{'DT','DV','FH','GT','MV','WT'};
        String repAccount = '';
        String repSalesDistrict = '';
        String repSalesDistrictName = '';

        if (lead.ProductCategory__c != null && lead.ProductCategory__c != 'Transport') {
            SBO_EnosixZAPR_Search.EnosixZAPR_SR searchResult = searchZAPR(salesOrg, distributionChannel, country, region, county);

            if (searchResult != null) {
                for (SBO_EnosixZAPR_Search.SEARCHRESULT result : searchResult.getResults()) {
                    customerNumbers.add(result.CustomerNumber);
                    if (result.AppSegCode != null) {
                        repAccount = result.CustomerNumber;
                        repSalesDistrict = result.SalesDistrict;
                        repSalesDistrictName = result.SalesDistrictName;
                    }
                    try{
                    User thisTSM = getUserByFullName(result.SalesDistrictName);

                    if (thisTSM != null) {
                        lead.OwnerId = thisTSM.Id;
                    }
                    }
                    catch(exception e){}
                }
            }
        }
        else if (lead.ProductCategory__c != null && lead.ProductCategory__c == 'Transport') {
            //Using queueable apex to ensure the callouts are made for all DCs in the mobileDCs list above
            SBO_EnosixZAPR_Search.EnosixZAPR_SR searchResult = searchZAPR(salesOrg, mobileDCs[0], country, region, county);

            if (searchResult != null) {
                for (SBO_EnosixZAPR_Search.SEARCHRESULT result : searchResult.getResults()) {
                    customerNumbers.add(result.CustomerNumber);
                    //The TSM for the "Transport" product category should be the same across all impacted DCs
                    User thisTSM = getUserByFullName(result.SalesDistrictName);

                    if (thisTSM != null) {
                        lead.OwnerId = thisTSM.Id;
                    }
                }
            }
        }

        system.debug('customerNumbers ===> '+customerNumbers);
        lead.FLD_Customer_Numbers__c = String.join(customerNumbers, ',');
        update lead;
        //Call queueable job to enable ability to make additional callouts for all impacted DCs
        if (lead.ProductCategory__c == 'Transport' && !Test.isRunningTest()) System.enqueueJob(new AddDCaccounts(lead,salesOrg,mobileDCs[1],country,region,county));
        //Call queueable job to enable ability to make additional callout for all related distributors
        if (lead.FLD_Distribution_Channel__c == 'CP' && !Test.isRunningTest()) System.enqueueJob(new AddDistributors(lead,salesOrg,distributionChannel,country,region,county,repAccount,repSalesDistrict,repSalesDistrictName));
    }

    private static String getCounty(Address leadAddress) {
        if (leadAddress == null) {
            return null;
        }

        String country = leadAddress.getCountry();
        String postalCode = leadAddress.getPostalCode();

        if (country == 'United States') {
            if (postalCode.length() > 5) {
                postalCode = postalCode.substring(5);
            }
        }

        List<County_Zip_Codes__c> results = [SELECT Id, County__c from County_Zip_Codes__c where name = : postalCode];

        if (results == null || results.size() == 0) {
            return null;
        }

        return results[0].County__c;
    }

    public static SBO_EnosixZAPR_Search.EnosixZAPR_SR searchZAPR(String salesOrg, String distributionChannel, String country, String region, String county) {
        return searchZAPR(salesOrg, distributionChannel, country, region, county, true);
    }

    public static SBO_EnosixZAPR_Search.EnosixZAPR_SR searchZAPR(String salesOrg, String distributionChannel, String country, String region, String county, boolean addAppSegCode) {

        logger.enterAura('searchZAPR', new Map<String, Object> {
            'salesOrg' => salesOrg,
            'distributionChannel' => distributionChannel,
            'country' => country,
            'region' => region,
            'county' => county,
            'addAppSegCode' => addAppSegCode
        });

        SBO_EnosixZAPR_Search.EnosixZAPR_SC searchContext = new SBO_EnosixZAPR_Search.EnosixZAPR_SC();

        searchContext.SEARCHPARAMS.SalesOrganization = salesOrg;
        searchContext.SEARCHPARAMS.DistributionChannel = distributionChannel;
        searchContext.SEARCHPARAMS.Country = country;
        searchContext.SEARCHPARAMS.Region = region;
        searchContext.SEARCHPARAMS.County = county;

        // Added parameter to retrieve rep account when the Distribution Channel is CP
        if (distributionChannel == 'CP' && addAppSegCode) {
            searchContext.SEARCHPARAMS.AppSegCode = 'ZCO2';
        }

        SBO_EnosixZAPR_Search sbo = new SBO_EnosixZAPR_Search();

        try {
            searchContext = sbo.search(searchContext);
        } catch (Exception e) {
            System.debug('We have an error: ' + e.getMessage());
        } finally {
            logger.exit();
        }

        return searchContext.result;
    }

    private static User getUserByFullName(String fullName) {
        if (fullName == null) {
            return null;
        }

        String firstName = null;
        String lastName = null;
        if (fullName.contains(',')) {
            List<String> nameList = fullName.split(',');
            lastName = nameList[0];
            firstName = nameList[1];
        } else {
            List<String> nameList = fullName.split(' ');
            firstName = nameList[0];
            lastName = nameList[1];
        }

        if (firstName != null) {
            firstName = firstName.trim();
        }

        if (lastName != null) {
            lastName = lastName.trim();
        }

        List<User> users = [Select Id from User where FirstName = : firstName and LastName =: lastName];

        if (users == null || users.isEmpty()) {
            return null;
        }

        return users[0];
    }

    @TestVisible
    private static User getDummyUser() {
        User retVal = null;

        List<User> users = [Select Id from User where FirstName =: UNASSIGNED_USER_FNAME and LastName =: UNASSIGNED_USER_LNAME];

        if (users == null || users.isEmpty()) {
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' LIMIT 1];
            retVal = new User(  Alias = 'standt',
                                Email = UNASSIGNED_USER_FNAME + '.' + UNASSIGNED_USER_LNAME + '@gdi.gdi',
                                EmailEncodingKey = 'UTF-8',
                                FirstName = UNASSIGNED_USER_FNAME,
                                LastName = UNASSIGNED_USER_LNAME,
                                LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US',
                                ProfileId = p.Id,
                                ManagerId = getManagerId().Id,
                                TimeZoneSidKey = 'America/Los_Angeles',
                                UserName = UNASSIGNED_USER_FNAME + '.' + UNASSIGNED_USER_LNAME + '@' + Math.random() + 'testorg.com');

            insert retVal;
        } else {
            retVal = users[0];
        }

        return retVal;
    }

    @TestVisible
    private static User getManagerId() {
        return [Select Id from User where FirstName = 'Nathan' and LastName = 'Holthaus' LIMIT 1];
    }

    @TestVisible
    private static User createTestUser(String firstName, String lastName) {
        User retVal = null;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' LIMIT 1];
        retVal = new User(  Alias = 'standt',
                            Email = firstName + '.' + lastName + '@gdi.gdi',
                            EmailEncodingKey = 'UTF-8',
                            FirstName = firstName,
                            LastName = lastName,
                            LanguageLocaleKey = 'en_US',
                            LocaleSidKey = 'en_US',
                            ManagerId = getManagerId().Id,
                            ProfileId = p.Id,
                            TimeZoneSidKey = 'America/Los_Angeles',
                            UserName = firstName + '.' + lastName + '@' + Math.random() + 'testorg.com');

        try {
            insert retVal;
        } catch (Exception e) {
            system.debug('createTestUser Error ====> '+e.getMessage());
        }

        return retVal;
    }
}