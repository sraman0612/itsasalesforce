public with sharing class CTRL_CPQ_Partners
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_CPQ_Partners.class);

    @AuraEnabled
    public static UTIL_Aura.Response getPartnerList(String customerNumber, 
                                                        String partnerFunction, 
                                                        String salesOrg,
                                                        String salesDistribution,
                                                        String salesDivision
                                                    )
    {   
        List <SBO_SFCIPartner_Search.SEARCHRESULT> responseData = null;
        logger.enterAura('getPartnerList', new Map<String, Object> {
            'customerNumber' => customerNumber,
            'partnerFunction' => partnerFunction,
            'salesOrg' => salesOrg,
            'salesDistribution' => salesDistribution,
            'salesDivision' => salesDivision
        });

        SBO_SFCIPartner_Search sbo = new SBO_SFCIPartner_Search();
        SBO_SFCIPartner_Search.SFCIPartner_SC sc = new SBO_SFCIPartner_Search.SFCIPartner_SC();
        try
        {  
            if (partnerFunction == null)
            {
                UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.PartnerFunction_Not_Found);
            }
            else
            {
                if (customerNumber == null)
                {
                   UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.CustomerNumber_Not_Found);
                }
                else
                {
                    SBO_EnosixCustomer_Detail.EnosixCustomer cpqCustomer = UTIL_Customer.getCustomerByNumber(customerNumber);
                    if (cpqCustomer == null)
                    {
                        UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.CPQ_Quote_ErrorNotLinked);
                    }
                    else
                    {
                        sc.SEARCHPARAMS.CustomerNumber = customerNumber;
                        sc.SEARCHPARAMS.PartnerFunction = partnerFunction;
                        sc.SEARCHPARAMS.SalesOrganization = salesOrg;
                        sc.SEARCHPARAMS.Division = salesDivision;
                        sc.SEARCHPARAMS.DistributionChannel = salesDistribution;
                        sbo.search(sc);
                        responseData = sc.result.getResults();
                        
                        if(responseData.size() == 0)
                        {
                            UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.CPQ_Quote_ErrorNoPartners);
                        }
                    }
                }
            }
            
        }
        catch(Exception ex)
        {
            UTIL_PageMessages.addExceptionMessage(ex);
        }
        finally
        {
            logger.exit();
        }
        return UTIL_Aura.createResponse(responseData);
    }
}