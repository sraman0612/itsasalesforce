@isTest
public with sharing class TSTU_WarrantyClaimExpensesSyncBatch
{
    static final string TEST_JSON =
        '{"UTIL_WarrantyClaimExpensesSyncBatch.Logging": true}';

    public class MockSyncSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        private boolean success = true;

        public void setSuccess(boolean success)
        {
            this.success = success;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext)
        {
            if (this.throwException)
            {
                throw new UTIL_SyncHelper.SyncException('');
            }

            SBO_EnosixSOSync_Search.EnosixSOSync_SR searchResult =
                new SBO_EnosixSOSync_Search.EnosixSOSync_SR();

            SBO_EnosixSOSync_Search.SEARCHRESULT result1 =
                new SBO_EnosixSOSync_Search.SEARCHRESULT();

            result1.SalesDocument = 'SalesDocument';
            result1.ItemNumber = 'ItemNumber';

            searchResult.SearchResults.add(result1);

            SBO_EnosixSOSync_Search.SEARCHRESULT result2 =
                new SBO_EnosixSOSync_Search.SEARCHRESULT();

            result2.SalesDocument = 'SalesDocument';
            result2.ItemNumber = 'ItemNumber2';
    
            searchResult.SearchResults.add(result2);

            searchResult.setSuccess(this.success);
            searchContext.baseResult = searchResult;
            return searchContext;
        }
    }

    public static testMethod void test_AssetSync()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSOSync_Search.class, new MockSyncSearch());

        UTIL_AppSettings.resourceJson = TEST_JSON;
        createExistingObject();
        Test.startTest();
        UTIL_WarrantyClaimExpensesSyncBatch controller = new UTIL_WarrantyClaimExpensesSyncBatch();
        UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
        lastSync.retryCnt = 1;
        controller.setBatchParam(lastSync);
        Database.executeBatch(controller);
        Test.stopTest();
    }

    public static testMethod void test_AssetSyncFailure()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSOSync_Search.class, mockSyncSearch);
        mockSyncSearch.setSuccess(false);

        createExistingObject();
        Test.startTest();
        UTIL_WarrantyClaimExpensesSyncBatch controller = new UTIL_WarrantyClaimExpensesSyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
        }
        catch (Exception e) {}
        Test.stopTest();
    }

    public static testMethod void test_AssetSyncException()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSOSync_Search.class, mockSyncSearch);
        mockSyncSearch.setThrowException(true);

        Test.startTest();
        UTIL_WarrantyClaimExpensesSyncBatch controller = new UTIL_WarrantyClaimExpensesSyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
            controller.start(null);
        }
        catch (Exception e) {}
        Test.stopTest();
    }

    public static testMethod void test_AssetSyncSchedule()
    {
        UTIL_WarrantyClaimExpensesSyncSchedule schedule = new UTIL_WarrantyClaimExpensesSyncSchedule();
        schedule.setBatchParam(null);
        
        schedule.execute(null);
    }

    private static void createExistingObject()
    {
        UTIL_AppSettings.resourceJson = TEST_JSON;

        Warranty_Claim__c warrantyClaim = new Warranty_Claim__c();
        warrantyClaim.SAP_Sales_Order_Number__c = 'SalesDocument';
        insert warrantyClaim;

        Warranty_Claim_Expenses__c warrantyClaimExpenses = new Warranty_Claim_Expenses__c();
        warrantyClaimExpenses.Warranty_Claim__c = warrantyClaim.Id;
        warrantyClaimExpenses.SAP_Item_Number__c = 'ItemNumber';
        insert warrantyClaimExpenses;

        OBJ_SAP_Sync__c sapSync = new OBJ_SAP_Sync__c();
        sapSync.Name = 'UTIL_WarrantyClaimExpensesSyncSchedule';
        sapSync.FLD_Sync_DateTime__c = System.today().addDays(-1);
        sapSync.FLD_Page_Number__c = 0;
        upsert sapSync;
    }
}