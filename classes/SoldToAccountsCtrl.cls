public class SoldToAccountsCtrl {

    @AuraEnabled
    public static List<Account> fetchAccts(Id oppId){
        List<Account> Accnts = new List<Account>();
        system.debug('@@@oppId---->'+oppId);
        Id ctsBillToRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        Set<String> validPricelists = new Set<String>{'ITS_PRIMARY','ITS_CAN_PRIMARY'};
            Set<String> billToNumbers = new Set<String>();
        Set<string> setOfShipToCustomer = new Set<string>();
        Set<Id> setOfOppIds = new Set<Id>();
        List<Opportunity> lstOpp = new List<Opportunity>();
        if(oppId != null){

                Opportunity opp = [Select id,CTS_BillToAccount__c from Opportunity where Id = :oppId];
          
		system.debug('opp BiiToAcc-> ' + opp.CTS_BillToAccount__c);  
            Accnts =[SELECT Id,Name, Type, Oracle_Number__c ,ShippingStreet,ShippingCity,ShippingState, ShippingPostalCode 
                         FROM account WHERE Bill_To_Account__c =: opp.CTS_BillToAccount__c AND type='Sold To'];
        }
        system.debug('Accnts-> ' + Accnts.size());
        return Accnts;
    }

    @AuraEnabled
    public static string getRecordTypeName(Id OppId){
        string recType='';
        recType = [SELECT ID, Recordtype.DeveloperName FROM opportunity where Id=:OppId LIMIT 1].Recordtype.DeveloperName;
        return recType;
    }    
    
  
    @AuraEnabled
    public static string setSoldToAccount(Id OppId, Id AccId){
	system.debug('DEBUG-> ' + OppId + ' == ' + AccId);
        string exceptionMsg = '';
        if(oppId != null && AccId != null){
            opportunity opp = New Opportunity();
            opp.Id = oppId;
            opp.Sold_To_ID__c = AccId;
            opp.Sold_To_Account__c = AccId;
            try{
                update opp;}
            catch(exception e){
                exceptionMsg = e.getMessage();
            }
        }
   return exceptionMsg;
    }
}