public class CreateAlertFromEvent {
    
    public static void addAlert(iConn_Alert__e alert){
        Map<String,Object> dataString =  (Map<String,Object>)JSON.deserializeUntyped(alert.Alert_String__c);
        
        if(dataString.get('realtimeAction') == 'CREATE') {
            System.debug('realtimeAction is CREATE, creating...');
            Alert__c newAl = New Alert__c();
            Map<String, Object> inData = (Map<String, Object>)dataString.get('data');
            Map<String, Object> alSrc = (Map<String, Object>)inData.get('source');
            Long cumulocityId = long.valueOf(String.valueof(alSrc.get('id')));
            
            // Select relevant Asset by its Cumulocity ID
            List<Asset> asset = [SELECT Id, Name, IMEI__c, Cumulocity_ID__c, Current_Servicer_Owner__c, Current_Servicer__c FROM Asset WHERE Cumulocity_ID__c = :cumulocityId];
            List<Asset> defaultAsset = [SELECT Id, Name FROM Asset WHERE Name='iConnMachine' LIMIT 1];
            System.debug('Matching Asset found with Cumulocity ID: ' + asset);
            
            if(asset.size() > 0){ 
                // Matching Asset found!
                System.debug('Creating Alert on found Asset...');
                
                newAl.Alert_ID__c = String.valueof(inData.get('id'));
                newAl.Alert_Message__c = String.valueof(inData.get('text'));
                newAl.Severity__c = String.valueof(inData.get('severity'));
                newAl.Servicer__c = asset[0].Current_Servicer__c;
                newAl.Serial_Number__c = asset[0].Id;
                newAl.IMEI__c = asset[0].IMEI__c;
                newAl.Source_ID__c  = long.valueof(String.valueof(alSrc.get('id')));// put Cumulocity ID 
                newAl.Status__c = String.valueof(inData.get('status'));
                newAl.No_Match_Found__c = false;
            } else {
                // No matching Asset found!
                System.debug('No Asset with that Cumulocity ID was found! Using defaultAsset Serial Number...');
                
                newAl.Alert_ID__c = String.valueof(inData.get('id'));
                newAl.Alert_Message__c = String.valueof(inData.get('text'));
                newAl.Severity__c = String.valueof(inData.get('severity'));
                newAl.Serial_Number__c = defaultAsset[0].Id;
                newAl.Source_ID__c  = long.valueof(String.valueof(alSrc.get('id')));// put Cumulocity ID 
                newAl.Status__c = String.valueof(inData.get('status'));
                newAl.No_Match_Found__c = true;
            }
            
            upsert newAl Alert_ID__c;
            newAl.Code_Block_Status__c = true;
            upsert newAl Alert_ID__c;
        }
        
        if(dataString.get('realtimeAction') == 'UPDATE') {
            System.debug('realtimeAction is UPDATE, updating...');
            Map<String, Object> inData = (Map<String, Object>)dataString.get('data');
            String alertId = String.valueOf(inData.get('id'));
            
            System.debug('Searching for Alert with Alert_Id__c of: ' + alertId);
            List<Alert__c> currAlert = [SELECT Id, Status__c , Alert_ID__c FROM Alert__c WHERE Alert_ID__c = :alertId];
            
            if(currAlert.size() > 0) {
                System.debug('Alert found!');
                System.debug(currAlert);
                System.debug('Updating...');
                
            	currAlert[0].Status__c = String.valueOf(inData.get('status'));
                
                Database.update(currAlert[0]);
            } else {
                System.debug('No Alerts with that Alert_Id__c found. Creating new Alert.');
                
                Alert__c newAl = New Alert__c();
                Map<String, Object> alSrc = (Map<String, Object>)inData.get('source');
                Long cumulocityId = long.valueOf(String.valueof(alSrc.get('id')));
                
                // Select relevant Asset by its Cumulocity ID
                List<Asset> asset = [SELECT Id, Name, IMEI__c, Cumulocity_ID__c, Current_Servicer_Owner__c, Current_Servicer__c FROM Asset WHERE Cumulocity_ID__c = :cumulocityId];
                List<Asset> defaultAsset = [SELECT Id, Name FROM Asset WHERE Name='iConnMachine' LIMIT 1];
                System.debug('Matching Asset found with Cumulocity ID: ' + asset);
                
                if(asset.size() > 0){ 
                    // Matching Asset found!
                    System.debug('Creating Alert on found Asset...');
                    
                    newAl.Alert_ID__c = String.valueof(inData.get('id'));
                    newAl.Alert_Message__c = String.valueof(inData.get('text'));
                    newAl.Severity__c = String.valueof(inData.get('severity'));
                    newAl.Servicer__c = asset[0].Current_Servicer__c;
                    newAl.Serial_Number__c = asset[0].Id;
                    newAl.IMEI__c = asset[0].IMEI__c;
                    newAl.Source_ID__c  = long.valueof(String.valueof(alSrc.get('id')));// put Cumulocity ID 
                    newAl.Status__c = String.valueof(inData.get('status'));
                    newAl.No_Match_Found__c = false;
                } else {
                    // No matching Asset found!
                    System.debug('No Asset with that Cumulocity ID was found! Using defaultAsset Serial Number...');
                    
                    newAl.Alert_ID__c = String.valueof(inData.get('id'));
                    newAl.Alert_Message__c = String.valueof(inData.get('text'));
                    newAl.Severity__c = String.valueof(inData.get('severity'));
                    newAl.Serial_Number__c = defaultAsset[0].Id;
                    newAl.Source_ID__c  = long.valueof(String.valueof(alSrc.get('id')));// put Cumulocity ID 
                    newAl.Status__c = String.valueof(inData.get('status'));
                    newAl.No_Match_Found__c = true;
                }
                
                upsert newAl Alert_ID__c;
                newAl.Code_Block_Status__c = true;
                upsert newAl Alert_ID__c;
            }
        }
    }
    
}