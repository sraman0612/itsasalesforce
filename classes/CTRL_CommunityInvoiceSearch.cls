public with sharing class CTRL_CommunityInvoiceSearch
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_CommunityInvoiceSearch.class);

    @AuraEnabled
    public static UTIL_Aura.Response getResultList(String AcctId) {

        List<SBO_EnosixInvoice_Search.SEARCHRESULT> responseData = new List<SBO_EnosixInvoice_Search.SEARCHRESULT>();
        SBO_EnosixInvoice_Search.EnosixInvoice_SC searchContext = new SBO_EnosixInvoice_Search.EnosixInvoice_SC();

        SBO_EnosixInvoice_Search sbo = new SBO_EnosixInvoice_Search();
        sbo.search(searchContext);
        SBO_EnosixInvoice_Search.EnosixInvoice_SR result = searchContext.result;
        responseData = result.getResults();

        return UTIL_Aura.createResponse(responseData);
    }

    @AuraEnabled
    public static UTIL_Aura.Response reprintInvoice(String SalesDocument){
        logger.enterAura('reprintInvoice', new Map < String, Object > {
            'SalesDocument' => SalesDocument
        });

        Object responseData = null;

        try
        {
            SBO_EnosixSalesDocOutput_Search searchSBO = new SBO_EnosixSalesDocOutput_Search();
            SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC context = UTIL_EnosixOutput_Search.getInvoiceSearchContext(SalesDocument);

            context = searchSBO.Search(context);
            responseData = context.result.SearchResults.get(0);

            if (null == responseData)
            {
                UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, 'No Results were found');
            }

            UTIL_PageMessages.addFrameworkMessages(context.getMessages());

        } catch (Exception ex) {
            UTIL_PageMessages.addExceptionMessage(ex);
        } finally {
            logger.exit();
        }

        return UTIL_Aura.createResponse(responseData);
    }
}