@istest
public class SFS_setInvoiceSubmittedQueueableTest {
    @istest public static void SFS_setInvoiceSubmittedQueueableTest() {    
        
        RecordType rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'];
        
        
        Double latitude = 48.7646475292616;
        Double longitude =-98.1987222787954;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Currency__c='USD';
        acct.RecordTypeId = rtAcc.Id; 
        acct.IRIT_Payment_Terms__c='NET 30';
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.ShippingCity ='Montreat';
        acct.ShippingCountry ='United States';
        acct.ShippingGeocodeAccuracy ='Zip';
        acct.ShippingPostalCode='28757';
        acct.ShippingState='NC';
        acct.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct.IRIT_Customer_Number__c='1234';
        insert acct;
      
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(2, null ,null, null, null, true);
        wo[0].Status='Open';       
        wo[0].SFS_Bill_to_Account__c=acct.Id;      
        wo[0].SFS_Requested_Payment_Terms__c='NET 30';
       
        Update wo;
        List<WorkOrderLineItem> woli = SFS_TestDataFactory.createWorkOrderLineItem(1,wo[0].Id,null,true);
        woli[0].Status='Closed';
        Update woli;
       
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,false);
      
        Id invRecType =[select id from RecordType where DeveloperName ='SFS_WO_Invoice'].Id;
        //get invoice
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(2,false);
        inv[0].SFS_Work_Order__c = wo[0].Id;       
        inv[0].SFS_Status__c='Pending Submission';
        inv[0].RecordTypeId=invRecType;           
        insert inv;
        
        charge[0].SFS_Invoice__c = inv[0].id;
        charge[0].CAP_IR_Amount__c=500;
        charge[0]. SFS_Charge_Type__c='Ordered Item'; 
        insert charge;
        Test.startTest();
        SFS_Invoice_submit_batch obj = new SFS_Invoice_submit_batch();
        String jobId = System.scheduleBatch(obj, 'SFSSetInvoiceSubmittedTest', 2);
		obj.execute(null); 
        Test.stopTest();
    }
    
}