public class SFS_InvLIFlowController {
    public SFS_InvLIFlowController(ApexPages.StandardSetController controller) {
        
    } 
    public Flow.Interview.SFS_Flow_InvLI_Credit_Screen myFlow { get; set; }
    
    public String getmyID() {
        if (myFlow==null)  return ''; else return myFlow.invoiceRecordTypeIdNew;
    }
    
    public String getmyOldID() {
        if (myFlow==null)  return ''; else return myFlow.parentInvoiceId;
    }
    
    public PageReference getOID(){ 
        String id = getmyID();
        PageReference p ; p = id != null ?  new PageReference('/lightning/r/Invoice__c/'+id+'/view'):new PageReference('/lightning/o/Invoice__c/list?filterName=Recent'); p.setRedirect(true);return p;   
    }
    
    public pagereference backMethod(){
        String id = getmyOldID();
        PageReference p ; p = id != null ?  new PageReference('/lightning/r/Invoice__c/'+id+'/view'):new PageReference('/lightning/o/Invoice__c/list?filterName=Recent'); p.setRedirect(true);return p;
       
    }
}