@isTest
public with sharing class TSTU_ObjectLookups
{
    public class MOC_EnosixCustomer_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();
            result.setSuccess(false);
            return result;
        }
    }

    public class MOC_EnosixMaterial_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            SBO_EnosixMaterial_Detail.EnosixMaterial result = new SBO_EnosixMaterial_Detail.EnosixMaterial();
            result.setSuccess(false);
            return result;
        }
    }

    private static Case createTestCase()
    {
        Case testCase = new Case();
        upsert testCase;
        return testCase;
    }

    private static Opportunity createTestOpp()
    {
        Account testAccount = TSTU_SFAccount.createTestAccount();
        testAccount.Name = 'test';
        upsert testAccount;

        Opportunity testOpportunity = TSTU_SFOpportunity.createTestOpportunity();
        testOpportunity.Name = 'test';
        testOpportunity.StageName = 'test';
        testOpportunity.CloseDate = DateTime.Now().Date();
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.ENSX_EDM__OrderNumber__c = 'SAPOppO';
        testOpportunity.ENSX_EDM__Quote_Number__c = 'SAPOppQ';

        PriceBook2 testPriceBook = new PriceBook2();
        testPriceBook.Name = 'Test';
        upsert testPriceBook;

        testOpportunity.Pricebook2Id = testPriceBook.Id;
        upsert testOpportunity;
        return testOpportunity;
    }

    @isTest
    public static void test_getCaseById()
    {
        Case testCase = createTestCase();

        Test.startTest();
        Case result = UTIL_ObjectLookups.getCaseById('bad Id');
        result = UTIL_ObjectLookups.getCaseById(testCase.Id);
        Test.stopTest();
    }

    @isTest
    public static void test_getCustomerForCase()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixCustomer_Detail.class, new MOC_EnosixCustomer_Detail());

        Account testAccount = TSTU_SFAccount.createTestAccount();
        testAccount.Name = 'test';
        upsert testAccount;

        Case testCase = createTestCase();
        testCase.AccountId = testAccount.Id;

        Test.startTest();
        SBO_EnosixCustomer_Detail.EnosixCustomer cust = UTIL_ObjectLookups.GetCustomerForCase(testCase);

        testAccount.put(UTIL_SFAccount.CustomerFieldName,'test');
        upsert testAccount;
        cust = UTIL_ObjectLookups.GetCustomerForCase(testCase);
        Test.stopTest();
    }

    @isTest
    public static void test_getContactById()
    {
        Opportunity testOpportunity = createTestOpp();

        Test.startTest();
        Contact contact = UTIL_ObjectLookups.getContactById(testOpportunity.Id);
        Test.stopTest();
    }
}