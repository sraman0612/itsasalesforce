public with sharing class SFS_BatchCreatePricebookEntry implements Database.Batchable<sObject>{
	public ID stdPriceBookId;
    public ID stdServicePriceBookId;
    public ID HibonProdRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Product2' AND DeveloperName = 'Hibon'].id;
    public ID NaAirProdRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Product2' AND DeveloperName = 'NA_Air'].id;
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'SELECT Id, Name, ProductCode, IsActive, CurrencyIsoCode, ExternalID FROM Product2 WHERE IsActive = TRUE AND (RecordTypeId = \''+ HibonProdRecordTypeId + '\' OR (RecordTypeId = \''+ NaAirProdRecordTypeId + '\' AND ProductCode <> NULL))';        
        
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        if(Test.isRunningTest()){
            stdPriceBookId = Test.getStandardPricebookId();
            system.debug('@@@stdPriceBookId'+stdPriceBookId);
            stdServicePriceBookId = [SELECT Id FROM Pricebook2 WHERE Name = 'Service Price Book'].id;
            system.debug('@@@stdServicePriceBookId'+stdServicePriceBookId);
          }else{
            stdPriceBookId = Label.Default_Pricebook;
        }            stdServicePriceBookId = Label.Default_Service_Pricebook;

        
        //Get a list of all active currency types in the Org
        List<CurrencyType> fullCurrencyList = [SELECT Id, IsoCode, ConversionRate, DecimalPlaces, IsActive FROM CurrencyType WHERE IsActive = TRUE];
        
        
        //Get a list of every Material and Labor product that exists
        List<Product2> fullProductList = (List<Product2>)scope;
        Set<Id> setOfProductIds = (new Map<Id,SObject>(fullProductList)).keySet();
        
        
        //Get a list of all of the PriceBookEntries (PBE), and create a set that has the Product Code of the PBE and the Currency ISO Code
        //We will use this list to see if the necessary PBE already exists, and if not we will create it.
        List<PriceBookEntry> fullPricebookEntryList = [SELECT Id, Name, Pricebook2Id, Product2Id, ProductCode, CurrencyIsoCode, UnitPrice
                                                       , IsActive, UseStandardPrice
                                                       FROM PricebookEntry WHERE Product2.Id IN :setOfProductIds];
        Set<String> PBESet = new Set<String>();
        for(PriceBookEntry pbe : fullPricebookEntryList){
            PBESet.add(pbe.ProductCode + pbe.CurrencyIsoCode + pbe.Pricebook2Id);
            
        }
        
        //This list will hold any new PBEs that need to be created
        List<PriceBookEntry> newStdEntries = new List<PriceBookEntry>();
        List<PriceBookEntry> newServiceEntries = new List<PriceBookEntry>();
      
        
        
        //Loop through every product, and for each product loop through every currency, to make sure a PBE has been created for each product in each currency
        for(Product2 prod : fullProductList){
            for(CurrencyType ct : fullCurrencyList){
                String standardPBElookup = prod.ProductCode + ct.IsoCode + stdPriceBookId;
                String servicePBElookup = prod.ProductCode + ct.IsoCode + stdServicePriceBookId;
                
                //Check to see if the Product Code / Currency ISO Code combination already exists as a PBE
                //If not, then add to the newEntries PBE List
                if(!PBESet.contains(standardPBElookup)){
                    //System.debug('Inside the if statement for standard set ' + prod +', ' + ct);
                    PricebookEntry stdPBE = new PricebookEntry(Pricebook2Id = stdPriceBookId
                                                               ,Product2Id = prod.Id
                                                               ,CurrencyIsoCode = ct.IsoCode
                                                               ,UnitPrice = 0
                                                               ,isActive = prod.IsActive
                                                               ,UseStandardPrice = false);
                    //,Pricebook_Entry_External_Id__c = prod.ProductCode + ct.IsoCode);
                    //System.debug(stdPBE);
                    newStdEntries.add(stdPBE);
                }
                if(!PBESet.contains(servicePBElookup)){
                    //System.debug('Inside the if statement for service set ' + prod +', ' + ct);
                    PricebookEntry servicePBE = new PricebookEntry(Pricebook2Id = stdServicePriceBookId
                                                                   ,Product2Id = prod.Id
                                                                   ,CurrencyIsoCode = ct.IsoCode
                                                                   ,UnitPrice = 0
                                                                   ,isActive = prod.IsActive
                                                                   ,UseStandardPrice = false);
                    //,Pricebook_Entry_External_Id__c = prod.ProductCode + ct.IsoCode);
                    //System.debug(servicePBE);
                    newServiceEntries.add(servicePBE);
                }
            }
        }
        
        if(newStdEntries.size() > 0){
            System.debug('Inserting standard entries ' + newStdEntries.size());
            insert newStdEntries;
        }
        
        if(newServiceEntries.size() > 0 && !Test.isRunningTest()){
            System.debug('Inserting service entries ' + newServiceEntries.size());
            insert newServiceEntries;
        }
    }
    
    public void finish(Database.BatchableContext BC){
    }
}