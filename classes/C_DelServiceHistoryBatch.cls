global class C_DelServiceHistoryBatch implements Database.Batchable<sObject> {
    
    global final String query;
    public final String soql1 = 'Select Name From Account Where Id IN :XXX';
    List<Service_History__Share> delShare = new List<Service_History__Share>();
    
    global C_DelServiceHistoryBatch(List<string> uID,List<string> uID2,List<string> uID3)
    {
        String newStr = '';
        String newStr2 = '';
        String newStr3 = '';
        for(String str : uID)
        {
            newStr += '\'' + str.trim() + '\',';
            newStr = newStr.lastIndexOf(',') > 0 ? newStr.substring(0,newStr.lastIndexOf(',')) + ',' : newStr ;
        }
        
        for(String str : uID2)
        {
            newStr2 += '\'' + str.trim() + '\',';
            newStr2 = newStr.lastIndexOf(',') > 0 ? newStr.substring(0,newStr.lastIndexOf(',')) + ',' : newStr ;
        }
        
        for(String str : uID3)
        {
            newStr3 += '\'' + str.trim() + '\',';
            newStr3 = newStr3.lastIndexOf(',') > 0 ? newStr3.substring(0,newStr3.lastIndexOf(',')) + ',' : newStr3 ;
        }
        system.debug(newStr+' '+newStr2+' '+newStr3+' '+uID+' '+uID2+' '+uID3);
        if(!string.isBlank(newStr))
        	newStr = newStr.Substring(0,newStr.length()-1);
        else
            newStr = '\'\'';
        if(!string.isBlank(newStr2))
        	newStr2 = newStr2.Substring(0,newStr2.length()-1);
        else
            newStr2 = '\'\'';
        if(!string.isBlank(newStr3))
        	newStr3 = newStr3.Substring(0,newStr3.length()-1); 
        else
            newStr3 = '\'\'';
        query = 'Select id from Service_History__Share where userorgroupid IN ('+newStr+') OR userorgroupid IN ('+newStr2+') OR userorgroupid IN ('+newStr3+')';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug(query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Service_History__Share> scope){
        for(Service_History__Share sh : scope)
        {
            delShare.add(sh);
        }
        Database.delete(delShare);
    }
    
    global void finish(Database.BatchableContext BC){
        
    }  
}