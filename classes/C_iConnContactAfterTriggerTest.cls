@isTest
public class C_iConnContactAfterTriggerTest {
    @TestSetup static void setup()
    {
        UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'SFDC_System_Administrator' LIMIT 1];
        Profile profile_1 = [SELECT Id FROM Profile WHERE Name = 'Partner Community User' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId FROM User WHERE Profile.Name = 'System Administrator' AND UserRoleId = :userRole_1.Id LIMIT 1];
        User user_1;
        System.runAs(admin) {
        Account ap1 = new account(name='test3');
        insert ap1;
        
        Account ap2 = new account(name='test4');
        insert ap2;
        
        Account a = new account(name='test',ParentId = ap1.id);
        insert a;
        
         Account a1 = new account(name='test1', ParentId = ap2.id);
        insert a1;
        
        contact con = new contact(firstname='j', lastname='b', email='jbtest1@test.com', accountid =a.id);
        insert con;
        
        contact con1 = new contact(firstname='j2', lastname='b2', email='jbtest2@test.com', accountid =a1.id);
        insert con1;
       
            
            user_1 = new User( 
                Email = 'test@ir.com',
                ProfileId = profile_1.Id, 
                UserName = 'test123@ir.com', 
                Alias = 'Test',
                TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey = 'ISO-8859-1',
                LocaleSidKey = 'en_US', 
                LanguageLocaleKey = 'en_US',
                ContactId = con1.Id,
                PortalRole = 'Manager',
                FirstName = 'Firstname',
                LastName = 'Lastname'
            );
            insert user_1;
        }
        Test.startTest();
        System.runAs(user_1) {
        Account ap1 = new account(name='test3');
        insert ap1;    
        Account a = new account(name='test',ParentId=ap1.id);
        insert a;
        Asset serial = new Asset();
        serial.SerialNumber = '1234567';
        serial.Name = 'test';
        serial.AccountId=a.id;
        insert serial;
        
        Asset serial2 = new Asset();
        serial2.SerialNumber = '12345678';
        serial2.Name = 'test';
        serial2.AccountId=a.id;
        insert serial2;
        
        Serial_Number_Contact__c sc = new Serial_Number_Contact__c();
		sc.First_Name__c ='j';
        sc.Last_Name__c = 'b';
        sc.Relationship__c = 'End User';
        sc.Email_Address__c  = 'jbtest1@test.com';
        sc.Serial_Number__c  = serial.id;
        
		insert sc;
        
		Serial_Number_Contact__c sc1 = new Serial_Number_Contact__c();
		sc1.First_Name__c ='j';
        sc1.Last_Name__c = 't';
        sc1.Relationship__c = 'Distributor';
        sc1.Email_Address__c  = 'jbtest2@test.com';
        sc1.Serial_Number__c  = serial2.id;

		insert sc1;   
            
        }
        Test.stopTest();
    }
    @isTest static void test()
    {
        
        Asset sn = [SELECT Id, Current_Servicer__c FROM Asset WHERE SerialNumber = '1234567'];
        
        Account acct = [SELECT Id FROM Account WHERE Name = 'test1'];
        
        sn.Current_Servicer__c = acct.Id;
        
        update sn;
        
        
    }
}