@isTest
public with sharing class ensxtx_TSTU_RFC
{
    public class MOC_ensxtx_RFC_SD_GET_DOC_TYPE_VALUES implements ensxsdk.EnosixFramework.RFCMock
    {
        private boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        public ensxsdk.EnosixFramework.FunctionObject executeFunction()
        {
            ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.RESULT result = new ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.RESULT();
            ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT sditm = new ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT();
            sditm.DocumentType = 'SD';
            sditm.BEZEI = 'Standard';
            result.ET_OUTPUT_List.add(sditm);
            for (integer mocCnt = 0; mocCnt < 20; mocCnt++)
            {
                ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT out = new ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT();
                out.DocumentType = 'tst' + mocCnt;
                out.BEZEI = 'tst' + mocCnt;
                result.ET_OUTPUT_List.add(out);
            }
            result.setSuccess(this.success);
            return result;
        }
    }

    @isTest
    public static void test_getDocTypeMaster()
    {
        MOC_ensxtx_RFC_SD_GET_DOC_TYPE_VALUES mocRfc = new MOC_ensxtx_RFC_SD_GET_DOC_TYPE_VALUES();
        ensxsdk.EnosixFramework.setMock(ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.class, mocRfc);
        Test.startTest();

        ensxtx_UTIL_RFC.getDocTypeMaster();
        mocRfc.setSuccess(false);
        ensxtx_UTIL_RFC.getDocTypeMaster();

        Test.stopTest();
    }
}