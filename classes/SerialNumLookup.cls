@RestResource(urlMapping='/serialNum')
global class SerialNumLookup {
    @httpPost
    global static void doPost(String serialNum) {
        RestContext.response.addHeader('Content-Type', 'application/json');
        try {
        Asset a = [SELECT AccountId,Account_Number__c,active_iconn__c,Aftercooler__c,Air_Filter__c,Case__c,Change_Date__c,Comments__c,Competitive_Unit__c,ContactId,Contract_Number__c,
                       Contract_Start_Date__c,Contract_Type__c,CreatedById,CreatedDate,Currently_Under_Warranty__c,Date_of_Adoption__c,DChl__c,DelDate__c,Description,Description_of_Technical_Object__c,
                       Distributor_Name__c,Drive_Motor__c,Equipment_Type__c,Extended_Wear__c,Flange_Size__c,H_P__c,Id,iConn_c8y_H_total__c,iConn_c8y_H_load__c,iConn_Measurement_Last_Change__c,IMEI__c,Inactive__c,InstallDate,IsCompetitorProduct,
                       IsDeleted,Item__c,LastModifiedDate,Machine_Speed__c,Manufacturer__c,
                       Material_Description__c,MG_4__c,Model_Number__c,Model_Type_Other__c,Model_Type__c,Model__c,MPG4_Code__c,Name,Oil_Cooler_Fitted__c,
                       Oil_Filter__c,Oper_From__c,Owner_Address_Line_1__c,Owner_City__c,Owner_Country__c,Owner_Name__c,Owner_State__c,Owner_Zip_Code__c,
                       Packing_Box__c,ParentId,Parent_Account__c,Previous_Actual_Type_of_Maintenance__c,Price,Product2Id,Product_Hierarchy__c,PurchaseDate,
                       Quantity,RecordTypeId,RootAssetId,Sales_Order__c,SCRAPERS__c,SerialNumber,Serial_Number_18_Digit_ID__c,Serial_Number_Model_Number__c,
                       Ship_Date__c,Sold_to__c,SOrg__c,Special_Modifications__c,Start_up_Date__c,Status,Store_Number__c,Store__c,SystemModstamp,Type_of_Scheduled_Maintenance__c,
                       UsageEndDate,Warranty_Type__c,Warranty_No__c,X1ST_STG_VLVS__c,X3rd_Stg_Suct_Valve__c,Year_of_Maintenance__c, CHAR1__C, CHAR_TEXT1__C, CHAR2__C, CHAR_TEXT2__C, CHAR3__C, CHAR_TEXT3__C, 
                       CHAR4__C, CHAR_TEXT4__C, CHAR5__C, CHAR_TEXT5__C, CHAR6__C, CHAR_TEXT6__C, CHAR7__C, CHAR_TEXT7__C, CHAR8__C, CHAR_TEXT8__C, CHAR9__C, CHAR_TEXT9__C, CHAR10__C, CHAR_TEXT10__C, 
                       CHAR11__C, CHAR_TEXT11__C, CHAR12__C, CHAR_TEXT12__C, CHAR13__C, CHAR_TEXT13__C, CHAR14__C, CHAR_TEXT14__C, CHAR15__C, CHAR_TEXT15__C, CHAR16__C, CHAR_TEXT16__C, 
                       CHAR17__C, CHAR_TEXT17__C, CHAR18__C, CHAR_TEXT18__C, CHAR19__C, CHAR_TEXT19__C, CHAR20__C, CHAR_TEXT20__C, CHAR21__C, CHAR_TEXT21__C, CHAR22__C, CHAR_TEXT22__C, CHAR23__C, CHAR_TEXT23__C, 
                       CHAR24__C, CHAR_TEXT24__C, CHAR25__C, CHAR_TEXT25__C, Machine_Version_Parts_List__r.Air_Filter_1__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Air_Filter_2__r.Part_Number__c, Machine_Version_Parts_List__r.Air_Filter_3__r.Part_Number__c, Machine_Version_Parts_List__r.Air_Filter_4__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Air_Filter_Quantity_Expected__c, Machine_Version_Parts_List__r.Air_Filter_Unique_Products_Expected__c,
                       Machine_Version_Parts_List__r.Cabinet_Filter_Quantity_Expected__c, Machine_Version_Parts_List__r.Cabinet_Filter_Unique_Products_Expected__c,
                       Machine_Version_Parts_List__r.Control_Box_Filter_Quantity_Expected__c, Machine_Version_Parts_List__r.Control_Box_Filter_Unique_Expected__c,
                       Machine_Version_Parts_List__r.Maintenance_Kit_Quantity_Expected__c, Machine_Version_Parts_List__r.Maintenance_Kit_Unique_Products_Expected__c,
                       Machine_Version_Parts_List__r.Oil_Filter_Quantity_Expected__c, Machine_Version_Parts_List__r.Oil_Filter_Unique_Products_Expected__c,
                       Machine_Version_Parts_List__r.Separator_Quantity_Expected__c, Machine_Version_Parts_List__r.Separator_Unique_Products_Expected__c, Machine_Version_Parts_List__r.Lubricant_Quantity__c,
                       Machine_Version_Parts_List__r.Cabinet_Filter_1__r.Part_Number__c, Machine_Version_Parts_List__r.Cabinet_Filter_2__r.Part_Number__c, Machine_Version_Parts_List__r.Cabinet_Filter_3__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Control_Box_Filter_1__r.Part_Number__c, Machine_Version_Parts_List__r.Control_Box_Filter_2__r.Part_Number__c, Machine_Version_Parts_List__r.Control_Box_Filter_3__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Lubricant_1__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_2__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_3__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_4__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Lubricant_5__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_6__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_7__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_8__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Maintenance_Kit_1__r.Part_Number__c, Machine_Version_Parts_List__r.Maintenance_Kit_2__r.Part_Number__c, Machine_Version_Parts_List__r.Maintenance_Kit_3__r.Part_Number__c, 
                       Machine_Version_Parts_List__r.Oil_Filter_1__r.Part_Number__c, Machine_Version_Parts_List__r.Oil_Filter_2__r.Part_Number__c, Machine_Version_Parts_List__r.Oil_Filter_3__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Oil_Filter_4__r.Part_Number__c, Machine_Version_Parts_List__r.Separator_1__r.Part_Number__c, Machine_Version_Parts_List__r.Separator_2__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Separator_3__r.Part_Number__c, Machine_Version_Parts_List__r.Separator_4__r.Part_Number__c FROM Asset WHERE Name = :serialNum and DChl__c ='CM' and Inactive__c != TRUE and (NOT MPG4_Code__r.Level_1__c like '%Parts%') and (NOT MPG4_Code__r.Level_1__c like '%Bare%') and (NOT MPG4_Code__r.Level_1__c like '%Reman%') limit 1];
                        
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(a));
        } catch (Exception e) {
           
            JSONGenerator gen = JSON.createGenerator(false);
            gen.writeStartObject();
            gen.writeNumberField('statusCode', 404);
            gen.writeStringField('error', 'There was an error with the request.');
            gen.writeEndObject();
            RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
            
        }
    }
    
}