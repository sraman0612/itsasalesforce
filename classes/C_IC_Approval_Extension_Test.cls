@isTest
private class C_IC_Approval_Extension_Test {

    @testSetup
    static void setupData(){
    
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Contact cont1 = new Contact();
        cont1.AccountId=acct.id;
        cont1.firstName = 'Can';
        cont1.lastName = ' Pango';
        cont1.email = 'service-gdi@canpango.com';
        cont1.Relationship__c = 'Customer';
        insert cont1;
        
        Case cse1 = new Case();
        cse1.Account = acct;
        cse1.Contact = cont1;
        cse1.ContactId = cont1.Id;
        cse1.Type = 'CSE1';
        cse1.Subject = 'Testing Case';

        
        // need to associate an org-wide email address to this case for testing
        OrgWideEmailAddress owea = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        System.debug(owea);
        cse1.Org_Wide_Email_Address_ID__c = ' ' + owea.Id;
        
        insert cse1;
        
        System.debug(cse1);
        
        Email_Template_Admin__c eta = new Email_Template_Admin__c();
        eta.Name = 'Test Email_Template_Admin__c';
        insert eta;

          
        Attachment att = new Attachment();
        att.Name = 'Attachment Test';
        att.ParentId = cse1.Id;
        att.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att.ContentType = 'image/jpg';
        insert att;
        
        
        EmailMessage em = new EmailMessage();
            em.Incoming = true;
            em.FromName = 'Canpango';
            em.ToAddress = 'salesforce@gardnerdenver.com';
            em.TextBody = 'Body';
            em.Subject = 'Subject';
            em.MessageDate = System.today();
            em.ParentId = cse1.Id;
            em.HtmlBody = '<p>Body</p>';
            em.FromAddress = 'service-gdi@canpango.com';
            em.CcAddress = 'service-gdi@canpango.com';
            em.BccAddress = 'service-gdi@canpango.com';
        insert em;
    }
   
    static testmethod void testC_IC_Approval_Extension_Controller(){
        Case c = [SELECT Id,IsClosed,Assigned_From_Address_Picklist__c,Inquiry_Call_Type_List__c  FROM Case];
        c.Inquiry_Call_Type_List__c ='test';
        c.Assigned_From_Address_Picklist__c='gdcomp.cs.qcy@gardnerdenver.com';
        c.Inquiry_Type__c = 'Adding to Order';
        c.status='Assigned';
        c.Brand__c='Champion - CS';
        update c;
       
        Test.setCurrentPageReference(new PageReference('C_IC_Approval'));       
        System.currentPageReference().getParameters().put('CaseId', c.Id);        

        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        
        C_IC_Approval_Extension cae = new C_IC_Approval_Extension(sc);
        cae.approve();
        cae.decline();
        System.assertEquals('Closed', c.Status);
       
    }        
}