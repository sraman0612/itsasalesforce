/*
* Test cases for price book utility class
*/
@isTest
public class TSTU_Pricebook
{
    @isTest
    public static void test_getStandardPricebook()
    {
        Test.startTest();
        Id pb = UTIL_Pricebook.getStandardPriceBookId();
        Test.stopTest();
    }

    @isTest
    public static void test_getEntriesForPricebook()
    {
        Pricebook2 priceBook = writePricebookTestRecords();

        Test.startTest();
        Map<Id, PricebookEntry> result = UTIL_Pricebook.getEntriesForPricebook(priceBook);
        Account testAcct = TSTU_SFAccount.createTestAccount();
        insert testAcct;
        UTIL_Pricebook.getEntriesForPricebookById(testAcct.Id, null);
        Id pricebookId = createTestObjects();
        Set<String> materialNumbers = new Set<String>();
        materialNumbers.add('materialNumber');
        UTIL_Pricebook.getEntriesForPricebookById(pricebookId, materialNumbers);
        Map<Id, PricebookEntry> pbeMap = UTIL_Pricebook.getProductIdToEntryMapForMaterials(materialNumbers, pricebookId, '');
        UTIL_Pricebook.getMaterialToProductIdMapFromPricebookEntryMap(pbeMap);
        Test.stopTest();

        system.assertNotEquals(null, result);
    }

    @isTest
    public static void test_getEntriesForPricebook2()
    {
        Pricebook2 priceBook = writePricebookTestRecords();

        Set<String> materialNumbers = new Set<String>();
        materialNumbers.add('test');

        Test.startTest();
        Map<Id, PricebookEntry> result = UTIL_Pricebook.getEntriesForPricebook(priceBook, materialNumbers);
        Test.stopTest();

        system.assertNotEquals(null, result);
    }

    static Pricebook2 writePricebookTestRecords()
    {
        Product2 product = new Product2();
        product.Name = 'test';
        product.put(UTIL_SFProduct.MaterialFieldName, 'test');
        upsert product;
        PricebookEntry standardPBE = new PricebookEntry();
        standardPBE.PriceBook2Id = UTIL_Pricebook.getStandardPriceBookId();
        standardPBE.Product2Id = product.Id;
        standardPBE.UnitPrice = 100;
        upsert standardPBE;
        Pricebook2 priceBook = new Pricebook2();
        priceBook.Name = 'test';
        upsert priceBook;
        PricebookEntry priceBookEntry = new PricebookEntry();
        priceBookEntry.PriceBook2Id = priceBook.Id;
        priceBookEntry.Product2Id = product.Id;
        priceBookEntry.UnitPrice = 100;
        priceBookEntry.isActive = true;
        upsert pricebookentry;
        return priceBook;
    }

    private static Id createTestObjects()
    {
        Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();

        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        newProd.put(UTIL_SFProduct.MaterialFieldName,'materialNumber');
        insert newProd;

        PriceBookEntry standardPbe = new PriceBookEntry();
        standardPbe.UnitPrice = 100;
        standardPbe.Pricebook2Id = pricebookId;
        standardPbe.Product2Id = newProd.Id;
        standardPbe.UseStandardPrice = false;
        standardPbe.IsActive = true;
        insert standardPbe;

        Product2 newProd2 = new Product2(Name = 'test product', family = 'test family');
        insert newProd2;

        PriceBookEntry standardPbe2 = new PriceBookEntry();
        standardPbe2.UnitPrice = 100;
        standardPbe2.Pricebook2Id = pricebookId;
        standardPbe2.Product2Id = newProd2.Id;
        standardPbe2.UseStandardPrice = false;
        standardPbe2.IsActive = true;
        insert standardPbe2;

        Product2 newProd3 = new Product2(Name = 'test product', family = 'test family');
        insert newProd3;

        PriceBookEntry standardPbe3 = new PriceBookEntry();
        standardPbe3.UnitPrice = 100;
        standardPbe3.Pricebook2Id = pricebookId;
        standardPbe3.Product2Id = newProd3.Id;
        standardPbe3.UseStandardPrice = false;
        standardPbe3.IsActive = true;
        insert standardPbe3;

        return pricebookId;
    }
}