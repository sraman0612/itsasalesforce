@isTest
public class ensxtx_TSTU_Document_Detail
{
    private static final String sboDetailType = 'SalesDocument';

    @isTest
    public static void test_simulateCommand()
    {
        String simulateCommand = ensxtx_UTIL_Document_Detail.getSimulateCommand(sboDetailType);
    }

    @isTest
    public static void test_referenceCommand()
    {
        String referenceCommand = ensxtx_UTIL_Document_Detail.getReferenceDocumentCommand(sboDetailType);
    }

    @isTest
    public static void test_removeAllConditions()
    {
        ensxsdk.EnosixFramework.DetailObject docDetail = ensxtx_UTIL_Document_Detail.removeAllConditions(
            new ensxtx_SBO_EnosixSalesDocument_Detail.EnosixSalesDocument(), sboDetailType);
    }

    @isTest
    public static void test_updateTextFields()
    {
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();

        ensxsdk.EnosixFramework.DetailObject docDetail = ensxtx_UTIL_Document_Detail.updateTextFields(
            new ensxtx_SBO_EnosixSalesDocument_Detail.EnosixSalesDocument(), salesDocDetail, sboDetailType);
    }

    @isTest
    public static void test_buildSBOReference()
    {
        ensxsdk.EnosixFramework.DetailObject docDetail = ensxtx_UTIL_Document_Detail.buildSBOForReference('111', 'Test', sboDetailType);
    }

    @isTest
    public static void test_convertToSBO()
    {
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();
        ensxtx_DS_SalesDocAppSettings salesDocAppSettings = new ensxtx_DS_SalesDocAppSettings();
        salesDocAppSettings.SBODetailType = sboDetailType;

        ensxsdk.EnosixFramework.DetailObject salesDocSBO = ensxtx_UTIL_Document_Detail.convertToSBO(null, salesDocDetail, false, salesDocAppSettings);
    }

    @isTest
    public static void test_convertToObject()
    {
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();
        ensxtx_DS_SalesDocAppSettings salesDocAppSettings = new ensxtx_DS_SalesDocAppSettings();
        salesDocAppSettings.SBODetailType = sboDetailType;

        ensxtx_UTIL_Document_Detail.convertToObject(new ensxtx_SBO_EnosixSalesDocument_Detail.EnosixSalesDocument(), salesDocDetail, false, salesDocAppSettings);
    }

    @isTest
    public static void test_getPartnerFromDocumentDetail()
    {
        List<ensxtx_DS_Document_Detail.PARTNERS> partnerList = new List<ensxtx_DS_Document_Detail.PARTNERS>();

        ensxtx_UTIL_Document_Detail.getPartnerFromDocumentDetail(partnerList, 'partnerFunction', true);
        ensxtx_UTIL_Document_Detail.getPartnerFromDocumentDetail(partnerList, 'partnerFunction', true);
    }
}