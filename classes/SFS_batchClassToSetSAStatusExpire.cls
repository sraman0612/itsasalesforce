/*=========================================================================================================
* @author Yadav, Arati
* @date 07/10/2021
* @description:This is used to update the Service Agreement status expired

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/
public class SFS_batchClassToSetSAStatusExpire implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
    public ID sAgreeRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'ServiceContract' AND DeveloperName = 'SFS_Advanced_Billing'].id;
    public ID rentalAgreeRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'ServiceContract' AND DeveloperName = 'SFS_Rental'].id;
    public List<ServiceContract> SAToUpdate = new List<ServiceContract>();
    public Database.QueryLocator start(database.BatchableContext bc) {
        String Query = 'Select id,EndDate,SFS_Status__c from ServiceContract where ((SFS_Status__c = \'APPROVED\' OR SFS_Status__c = \'Pending\' OR SFS_Status__c = \'Draft\' ) AND (RecordType.DeveloperName = \'SFS_Advanced_Billing\' OR RecordType.DeveloperName = \'SFS_Contracting\' OR RecordType.DeveloperName = \'SFS_PackageCARE\' OR RecordType.DeveloperName = \'SFS_PartsCARE\' OR RecordType.DeveloperName = \'SFS_PlannedCARE\' OR RecordType.DeveloperName = \'SFS_Preventive_Maintenance\' OR RecordType.DeveloperName = \'SFS_Rental\') AND EndDate < TODAY) OR SFS_Status__c = \'Pending Closed\'';
        system.debug('Query********');
        return Database.getQueryLocator(Query);
        
    }
    
    public void execute(Database.BatchableContext bc, List<ServiceContract> scope){
        system.debug('Query********'+scope);
        Try{     
            for(ServiceContract SA : scope){
                
                if(SA.EndDate < system.today() || SA.SFS_Status__c == 'Pending Closed'){
                    SA.SFS_Status__c ='Expired';
                    system.debug('in IF');
                    SAToUpdate.add(SA); 
                }

                
                if(!SAToUpdate.isEmpty()){
                    Update SAToUpdate;
                    // System.enqueueJob(new SFS_setSAExpiredQueueable(SAToUpdate));
                } 
            }
        }
        Catch(Exception e){
        }       
    }
    
    public void finish(Database.BatchableContext bc){ 
        
        SFS_EntitlementHandlerBatch b = new SFS_EntitlementHandlerBatch(); 
        database.executebatch(b,1);        
    }
    
}