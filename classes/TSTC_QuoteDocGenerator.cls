@IsTest
public class TSTC_QuoteDocGenerator {
	@IsTest
    public static void testGenerateDocument() {
        SBQQ__Quote__c quote = TSTC_EnosixMachineDocLookup.createQuote();
        CTRL_QuoteDocGenerator.generateQuoteDocument('test', 'test', quote.id, null, 'test', 'MS Word');
    }
    @IsTest
    public static void testJobStatus() {
        CTRL_QuoteDocGenerator.getJobStatus(null);
        CTRL_QuoteDocGenerator.getJobStatus('"123456789012345678"');
    }
    @IsTest
    public static void testGetQuoteDocument() {
        SBQQ__Quote__c quote = TSTC_EnosixMachineDocLookup.createQuote();
        String quoteDocumentName = 'test';
        CTRL_QuoteDocGenerator.getQuoteDocument(quoteDocumentName, quote.Id);
    }
    @IsTest
    public static void testGetScreenData() {
        SBQQ__Quote__c quote = TSTC_EnosixMachineDocLookup.createQuote();
        CTRL_QuoteDocGenerator.getScreenData(quote.Id);
    }
}