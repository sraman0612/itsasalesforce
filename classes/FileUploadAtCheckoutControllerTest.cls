@IsTest
private class FileUploadAtCheckoutControllerTest {

    @IsTest
    static void getCartTotalAmountTest() {
        Account account = new Account(Name = 'Test');
        insert account;
        WebStore webStore = new WebStore(Name='TestWebStore');
        insert webStore;
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id);
        insert cart;

        Test.startTest();
        Decimal amount = FileUploadAtCheckoutController.getCartTotalAmount(cart.Id);
        Test.stopTest();

        System.assertEquals(amount, 0);

    }

    @IsTest
    static void uploadFileTest() {
        Test.startTest();
        FileUploadAtCheckoutController.uploadFile('test script', 'Test', null);
        Test.stopTest();

        List<ContentVersion> contentVersions = [ SELECT ContentDocumentId FROM ContentVersion];
        System.assertEquals(contentVersions.size() > 0, true);

    }

    @IsTest
    static void createContentLinkTest() {
        Account account = new Account(Name = 'Test');
        insert account;
        ContentVersion contentVersion = FileUploadAtCheckoutController.createContentVersion('test script', 'Test');
        Test.startTest();
        FileUploadAtCheckoutController.createContentLink(contentVersion.Id, account.Id);
        Test.stopTest();
        ContentDocumentLink cdl = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :account.Id];
        List<ContentVersion> contentVersions = [ SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        System.assertEquals(cdl.ContentDocumentId, contentVersions[0].ContentDocumentId);

    }

    @IsTest
    static void updateCartTest() {
        Account account = new Account(Name = 'Test');
        insert account;
        ContentVersion contentVersion = FileUploadAtCheckoutController.createContentVersion('test script', 'Test');
        WebStore webStore = new WebStore(Name='TestWebStore');
        insert webStore;
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id);
        insert cart;
        Test.startTest();
        FileUploadAtCheckoutController.updateCart(cart.Id, contentVersion.Id);
        Test.stopTest();
        WebCart cart2 = [SELECT Id, Content_Document_Id__c FROM WebCart WHERE Id = :cart.Id];
        System.assertEquals(cart2.Content_Document_Id__c, contentVersion.Id);

    }
}