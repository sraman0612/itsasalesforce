public with sharing class CaseQuickClose {
    
    @AuraEnabled
    public static Case getCase(Id caseId){
		//make your own SOQL here from your desired object where you want to place your lightning action
        return [SELECT CaseNumber, Id, Status, Description, Re_opened__c FROM Case WHERE Id = :caseId ];
    }
 
    @AuraEnabled
    public static Case updateCase(Case currCase){
        //you can make your own update here.
        currCase.Re_opened__c = false;
        currCase.Status = 'Closed';
        upsert currCase;
        return currCase;
    }

}