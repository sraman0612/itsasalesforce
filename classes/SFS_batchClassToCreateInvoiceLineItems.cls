/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 14/10/2021
* @description: This class is used to create the automatic Invoice Line items for the service agreement line items
* @Story Number: SIF-23

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Srikanth P        M-001       23-11-2022   Change in Recurring adjustment logic
Srikanth P        M-002       07-04-2023   Change in Service Charge Amount Calculation
============================================================================================================================================================*/

public class SFS_batchClassToCreateInvoiceLineItems implements Database.Batchable<Sobject>{
    public final String Query;
    public final Set<Id> sIds;
    public Id AgreementInvoiceRecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Agreement Invoice').getRecordTypeId();
    public Id CAREInvoiceRecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('SFS_CARE_Invoice').getRecordTypeId();
    public Id agreementChargeRecordType = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByName().get('Agreement Charge').getRecordTypeId();
    public Id careChargeRecordType = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByName().get('CARE Charge').getRecordTypeId();
    CAP_IR_Charge__c charge;
     public  SFS_batchClassToCreateInvoiceLineItems(Set<Id> scIds){
          sIds = scIds;
          Query = 'Select Id,RecordTypeId,RecordType.DeveloperName,SFS_Status__c,SFS_Service_Agreement__c,SFS_Billing_Period_Start_Date__c,SFS_Service_Agreement__r.SFS_Agreement_Length__c,SFS_Service_Agreement__r.SFS_Renewal_Escalator_Start_Date__c,SFS_Service_Agreement__r.SFS_Recurring_Adjustment__c,SFS_Service_Agreement__r.StartDate,SFS_Service_Agreement__r.EndDate,SFS_Invoice4Multiples__c,CurrencyIsoCode,SFS_Service_Agreement__r.AccountId,SFS_Service_Agreement__r.RecordType.DeveloperName,SFS_Service_Agreement__r.ShippingHandling,SFS_Service_Agreement_1__c,SFS_Service_Agreement_1__r.Service_Charge_Frequency__c,SFS_Service_Agreement__r.SFS_Status__c,SFS_Service_Agreement_1__r.StartDate,SFS_Service_Agreement_1__r.SFS_Renewal_Escalator_Start_Date__c,SFS_Service_Agreement_1__r.SFS_Recurring_Adjustment__c,SFS_Service_Agreement_1__r.SFS_Invoice_Frequency__c from Invoice__c Where ((SFS_Service_Agreement__c IN:sIds) OR (SFS_Service_Agreement_1__c IN:sIds)) AND  SFS_Status__c =\'Created\' Order By Id ASC' ;
            
    }
    public Database.QueryLocator start(database.BatchableContext bc) {
       return Database.getQueryLocator(Query);
      
    }
    
    public void execute(Database.BatchableContext bc, List<Invoice__c> scope){
      try{
     	    List<CAP_IR_Charge__c> chargeList = new List<CAP_IR_Charge__c>();
            set<Id> agreementIdList = new set<Id>();
           	for(Invoice__c inv : scope){
                if(inv.SFS_Service_Agreement__c!=null){
            	   agreementIdList.add(inv.SFS_Service_Agreement__c);
                } 
                else if (inv.SFS_Service_Agreement_1__c!=null && !agreementIdList.Contains(inv.SFS_Service_Agreement_1__c)){
                      agreementIdList.add(inv.SFS_Service_Agreement_1__c);
                }
            }
            Integer firstInvoice=1;
            Map<Id,ContractLineItem> cliMap	= new Map<Id,ContractLineItem>([select Id,UnitPrice, ServiceContractId,ServiceContract.SFS_Status__c,ServiceContract.AccountId,product2.Name from ContractLineItem where ServiceContractId IN: agreementIdList Order By CreatedDate]);
            map<Double,Double> tempPreviousAmountMap = new map<Double,Double>();
            map<Integer,Double> tempPreServiceAmtMap = new map<Integer,Double>();
            Double periodChange =0;
            //Boolean periodCheck = false;
           for(Invoice__c inv : scope){
                Double recurringPercentage =0;
                Double period =0;
                Boolean periodCheck = false;
                if((inv.SFS_Service_Agreement__c!=null && inv.SFS_Billing_Period_Start_Date__c>=inv.SFS_Service_Agreement__r.SFS_Renewal_Escalator_Start_Date__c) || (inv.SFS_Service_Agreement_1__c!=null && inv.SFS_Billing_Period_Start_Date__c>=inv.SFS_Service_Agreement_1__r.SFS_Renewal_Escalator_Start_Date__c)){
                   period = (inv.SFS_Service_Agreement__c!=null)?(inv.SFS_Service_Agreement__r.SFS_Renewal_Escalator_Start_Date__c.daysBetween(inv.SFS_Billing_Period_Start_Date__c)/365)+1:(inv.SFS_Service_Agreement_1__r.SFS_Renewal_Escalator_Start_Date__c.daysBetween(inv.SFS_Billing_Period_Start_Date__c)/365)+1;                  
                    if(period>0){
                        recurringPercentage =(inv.SFS_Service_Agreement__c!=null)?(inv.SFS_Service_Agreement__r.SFS_Recurring_Adjustment__c/100):(inv.SFS_Service_Agreement_1__r.SFS_Recurring_Adjustment__c/100);
                      }
                }
               if(period>periodChange){
                   periodCheck = true;
               }
             
            if(inv.RecordTypeId == AgreementInvoiceRecordTypeId){  
                   Integer count =0;
                   for(ContractLineItem cli : cliMap.values()){
                        count++;                       
                        if(inv.SFS_Service_Agreement__c==cli.ServiceContractId){
                            if(cli.UnitPrice >0 && cli.ServiceContract.SFS_Status__c =='APPROVED'){
                                double invAmount = period<1?cli.UnitPrice :(tempPreviousAmountMap.get(count)!=null && periodCheck)?tempPreviousAmountMap.get(count)+tempPreviousAmountMap.get(count)*recurringPercentage:tempPreviousAmountMap.get(count)!=null && periodCheck==false?tempPreviousAmountMap.get(count):cli.UnitPrice;
                               // Sets amount based on recurring adjustment
                           	   charge = new CAP_IR_Charge__c();
                               charge.CAP_IR_Type__c ='Recurring';
                               charge.CAP_IR_Account__c=cli.ServiceContract.AccountId;
                               charge.CAP_IR_Service_Contract__c= cli.ServiceContractId;
                               charge.CAP_IR_Contract_Line_Item__c= cli.Id;
                               if(inv.SFS_Invoice4Multiples__c == true){
                                   charge.CAP_IR_Amount__c = 0.00;//SIF-651--adds invoice line item amount //When Rental Agreement is active, we create weekly Invoices, but every 4th week is 0$.
                                  }
                                   else{
                                         charge.CAP_IR_Amount__c = invAmount;
                                       }
                               charge.CAP_IR_Date__c = inv.SFS_Billing_Period_Start_Date__c;
                               charge.SFS_Invoice__c = inv.Id;
                               charge.CAP_IR_Status__c='New';
                               charge.CAP_IR_Description__c=cli.Product2.Name;
                               charge.CurrencyIsoCode=inv.CurrencyIsoCode;
                               charge.RecordTypeId = agreementChargeRecordType;
                               chargeList.add(charge);
                               tempPreviousAmountMap.put(count,invAmount);
                                System.debug('tempPreviousAmountMap.get(count):'+tempPreviousAmountMap.get(count)+' periodCheck:'+periodCheck+'total:'+tempPreviousAmountMap.get(count)+tempPreviousAmountMap.get(count)*recurringPercentage+' perc:'+recurringPercentage);
                             }
                          }
                             
                      }                          
                 }
                // Logic to create service charges              
                if(inv.RecordTypeID == CAREInvoiceRecordTypeId){
                    Double totAmount =0;Integer serCount = 0;
                    Double chargeAmount =0;
                    String frequency = inv.SFS_Service_Agreement_1__r.Service_Charge_Frequency__c;
                    String invFrequency = inv.SFS_Service_Agreement_1__r.SFS_Invoice_Frequency__c;
                    Double Amount=0.0;
                     for(ContractLineItem cli : cliMap.values()){
                         serCount++;
                         if(inv.SFS_Service_Agreement_1__c==cli.ServiceContractId && cli.UnitPrice >0 && cli.ServiceContract.SFS_Status__c =='APPROVED'){
                            double invAmount = period<1?cli.UnitPrice :(tempPreServiceAmtMap.get(serCount)!=null && periodCheck)?tempPreServiceAmtMap.get(serCount)+tempPreServiceAmtMap.get(serCount)*recurringPercentage:tempPreviousAmountMap.get(serCount)!=null && periodCheck==false?tempPreServiceAmtMap.get(serCount):cli.UnitPrice;
                            //double invAmount = period<1?cli.UnitPrice :(tempPreviousAmountMap.get(serCount)!=null && periodCheck)?tempPreviousAmountMap.get(serCount)+tempPreviousAmountMap.get(serCount)*recurringPercentage:tempPreviousAmountMap.get(serCount)!=null && periodCheck==false?tempPreviousAmountMap.get(serCount):cli.UnitPrice;

                             tempPreServiceAmtMap.put(serCount,invAmount);
                            Amount+=invAmount;
                             System.debug('tempPreServiceAmtMap.get(serCount):'+tempPreServiceAmtMap.get(serCount)+' periodCheck:'+periodCheck+'total:'+tempPreServiceAmtMap.get(serCount)+tempPreServiceAmtMap.get(serCount)*recurringPercentage+' perc:'+recurringPercentage);
                            totAmount+= (invFrequency=='Monthly')?(invAmount*12):(invFrequency=='Quarterly')?(invAmount*4):(invFrequency=='3x per Year')?(invAmount*3):(invFrequency=='Semi Annually')?(invAmount*2):invAmount;                                                
                         }      
                     
                           chargeAmount = (frequency=='Monthly')?(totAmount/12):(frequency=='Quarterly')?totAmount/4:(frequency=='3x per Year')?totAmount/3:(frequency=='Semi Annually')?totAmount/2:totAmount;
                     }
                    CAP_IR_Charge__c c = new CAP_IR_Charge__c(); 
                    c.RecordTypeId = careChargeRecordType;
                    c.CAP_IR_Type__c ='Recurring';
                    c.SFS_Service_Agreement__c = inv.SFS_Service_Agreement_1__c;
                    c.CAP_IR_Date__c = inv.SFS_Billing_Period_Start_Date__c;
                    c.CAP_IR_Amount__c = chargeAmount;
                    c.SFS_Invoice__c = inv.Id;
                    //c.CAP_IR_Contract_Line_Item__c= cli.Id;
                    c.CAP_IR_Contract_Line_Item__c= charge.CAP_IR_Contract_Line_Item__c;
                    c.CAP_IR_Status__c='New';
                    c.CurrencyIsoCode=inv.CurrencyIsoCode;
                    chargeList.add(c);  
                         
               }
               periodChange = period;
                if(firstInvoice==1 && inv.SFS_Service_Agreement__r.RecordType.DeveloperName=='SFS_Rental' && inv.SFS_Service_Agreement__r.ShippingHandling>0 && inv.SFS_Service_Agreement__r.SFS_Status__c =='APPROVED'){
                    firstInvoice++;
                           CAP_IR_Charge__c charge = new CAP_IR_Charge__c();
                           charge.CAP_IR_Type__c ='Recurring';
                           charge.CAP_IR_Account__c=inv.SFS_Service_Agreement__r.AccountId;
                           charge.CAP_IR_Service_Contract__c= inv.SFS_Service_Agreement__c;
                           charge.CAP_IR_Amount__c = inv.SFS_Service_Agreement__r.ShippingHandling;
                           charge.CAP_IR_Date__c = inv.SFS_Billing_Period_Start_Date__c;
                           charge.SFS_Invoice__c = inv.Id;
                           charge.CAP_IR_Status__c='New';
                           charge.CurrencyIsoCode=inv.CurrencyIsoCode;
                           chargeList.add(charge); 
                }                
            }
            
            if(!chargeList.isEmpty()){
               Database.insert(chargeList,true);               
              }
	     }	
          catch(Exception e){
               system.debug('@@@exception'+e.getMessage() + e.getLineNumber());
          } 		  
    }
   
    public void finish(Database.BatchableContext bc){
       }
    
 }