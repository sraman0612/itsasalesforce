public with sharing class ConcessionRequest_ctr {

    public Concession_Request__c record {get;private set;}
    private String recordId;
    public Boolean isInitError {get;private set;}
    public Boolean submitDisabled {get;private set;}
    public Boolean recallDisabled {get;private set;}
    public Boolean saveDisabled {get;private set;}
    public List<Concession_Request__History> historyList {get; private set;}

    public ConcessionRequest_ctr(){

        recordId = Apexpages.currentPage().getParameters().get('Id');
        
        System.debug('Constructor RecordId: '+recordId);

        isInitError = !(recordId.length() > 14);
        
        if(!isInitError){
            
            Integer recordCount = [Select count() from Concession_Request__c where Quote_Line__c = :recordId];
            
            if(recordCount == 0){
                
                submitDisabled = false;
                recallDisabled = true;                
            }
            else{
                
                record = [
                    SELECT  Competitor__c,
                            Competitor_Product_Family__c,
                            Competitor_Product_Model__c,
                            Competitor_Bid_Price__c,
                            Requested_Multiplier__c,
                            Quote_Line__r.Standard_Multiplier__c,
                            Primary_Reason__c,
                            Standard_Dnet__c,
                            Standard_Multiplier__c,
                            Standard_Multiplier1__c,
                            Standard_D_Net__c,
                            Quote_Line__r.Standard_Discount__c,
                            Quote_Line__r.Discount1__c,
                            Quote_Line__r.Approved_Multiplier__c,
                            Quote_Line__r.SBQQ__Product__c,
                            Quote_Line__r.SBQQ__Product__r.Name,
                            Quote_Line__r.SBQQ__Quote__c,
                            ApprovalStatus__c,
                            Quote_Line__r.Dist_Margin_Percent__c,
                            //TSM_Manager__c,
                            //TSM_Manager_created__c,
                            Quote_Line__r.SBQQ__Quote__r.Concession_Rollup__c,
                            Distribution_Channel2__c,
                            Distribution_Channel__c,
                            Internal_Reason__c,
                            Competitor_Comments_Other__c,
                            Requested_Discount__c,
                            Use_Standard_Discount__c,
                            Use_Standard_Discount2__c,
                            Comments__c,
                            Quote_Line__r.Approved_Discount__c,
                            Product2__c,
                            Quote_Line__r.Standard_Multiplier1__c,
                            Use_Standard_Discount_Checkbox__c,
                            Distributor_Profit_Margin__c,
                            Standard_Discount2__c,
                            User__r.ManagerId,
                            Emergency__c,
                    New_Proposed_Disti_Margin_Percent__c,
                    New_Proposed_Disti_Sell_Price__c
                    
                    FROM    Concession_Request__c
                    WHERE   Quote_Line__c = :recordId
                ];
                
                historyList = [
                    Select ParentId, 
                    OldValue, 
                    NewValue, 
                    Field, 
                    CreatedById, 
                    CreatedDate 
                    from Concession_Request__History 
                    where parentId = :record.ID 
                    and Field in ('ApprovalStatus__c','Requested_Multiplier__c')
                ];
                
                if(record.ApprovalStatus__c == null || record.ApprovalStatus__c == '' || 
                   record.ApprovalStatus__c == 'Draft' || record.ApprovalStatus__c == 'Recalled'){
                    
                    submitDisabled = false;
                    recallDisabled = true;
                }
                
                if(record.ApprovalStatus__c != null &&  record.ApprovalStatus__c == 'Pending'){
                    
                    submitDisabled = true;
                    recallDisabled = false;
                }
                
                if(record.ApprovalStatus__c != null && record.ApprovalStatus__c == 'Approved'){
                    
                    submitDisabled = true;
                    recallDisabled = false;
                }
                
                if(record.ApprovalStatus__c == 'Approved' && record.ApprovalStatus__c == 'Approved'){
                    
                    submitDisabled = true;
                    recallDisabled = false;
                    saveDisabled = true; 
                }
                
                if(record.ApprovalStatus__c != null && record.ApprovalStatus__c == 'Rejected'){
                    
                    submitDisabled = false;
                    recallDisabled = true;
                }
                
                if(record.Distribution_Channel2__c == null ){
                   Apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR, 'Your quote must have a Sales Channel before using the concession request')); 
                }                    
            }
            
        }
        else{            
            Apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR, 'You must do a quick save before trying to submit a new Quote Line for approval'));
        }
    }
    
    public void createRecord(){
        
        if(recordID != null && recordId != 'undefined'){
            
            Integer recordCount = [Select count() from Concession_Request__c where Quote_Line__c = :recordId];
            
            System.debug('RecordId - '+recordId);
            
            if( recordCount == 0){
               SBQQ__QuoteLine__c ql = [
                   select  Id, 
                           Distribution_Channel__c,
                           Discount1__c,
                           Standard_Multiplier1__c,
                           Disti_Margin_Percent__c,
                           SBQQ__Product__r.Name,
                           Standard_Dnet__c,
                           Standard_Multiplier__c,
                           Standard_Discount__c,
                           Use_Standard_Discount__c
                           
            
                   from    SBQQ__QuoteLine__c 
                   where   Id = :recordId
               ];                
                
                Concession_Request__c record = New Concession_Request__c();
                record.Quote_Line__c = recordID;
                record.ApprovalStatus__c = 'Draft';
                record.product2__c = ql.SBQQ__Product__r.Name;
                record.Standard_Multiplier__c  = ql.Standard_Multiplier1__c;                
                record.Standard_Discount2__c = ql.Discount1__c;
                system.debug('Standard_Discount' + ql.Discount1__c);
                system.debug('Standard_Discount CR' + record.Standard_Discount2__c);
                record.Distribution_Channel2__c = ql.Distribution_Channel__c;
                record.Standard_D_Net__c = ql.Standard_Dnet__c;                  
                record.Use_Standard_Discount_Checkbox__c = ql.Use_Standard_Discount__c;
                record.Distributor_Profit_Margin__c = ql.Disti_Margin_Percent__c;
                //record.TSM_Manager__c = TSM_Manager_created__c;
                
                Boolean result = record.Distribution_Channel2__c.contains('Elmo') || 
                                record.Distribution_Channel2__c.contains('Mobile') || 
                                record.Distribution_Channel2__c.contains('Blower');

                IF (result){
                    record.Use_Standard_Discount2__c = TRUE;
                }
                else {
                    record.Use_Standard_Discount2__c = FALSE;
                }
                IF (record.emergency__c){
                    
                    system.debug('Emergency ='+ record.emergency__c );
                    IF(record.Use_Standard_Discount_Checkbox__c){    
                        system.debug('Use_Standard_Discount__c'+ record.Use_Standard_Discount__c);
                        record.Standard_Discount2__c = record.Standard_Discount2__c * .8;
                    }
                    else{
                         record.Standard_Multiplier__c = record.Standard_Multiplier__c *.8;    
                    }
                }
            else{
                system.debug('Not an emergency');
                    IF(record.Use_Standard_Discount_Checkbox__c){   
                        record.Standard_Discount2__c = ql.Discount1__c;
                    }
                else{
                    record.Standard_Multiplier__c = ql.Standard_Multiplier1__c;
                }
            }
                insert record;
                this.record = record;
            }            
        }        
    }
    
    public void doSave(){   

        try{
            
            update record;
            SBQQ__QuoteLine__c ql = [select Id,Standard_Multiplier1__c,Standard_Discount__c, Discount1__c from SBQQ__QuoteLine__c where Id = :recordId];

                IF (record.emergency__c){
                    
                    system.debug('Emergency ='+ record.emergency__c );
                    IF(record.Use_Standard_Discount_Checkbox__c){    
                        system.debug('Use_Standard_Discount__c'+ record.Use_Standard_Discount__c);
                        record.Standard_Discount2__c = record.Standard_Discount2__c * .8;
                    }
                    else{
                         record.Standard_Multiplier__c = record.Standard_Multiplier__c *.8;    
                    }
                }
            else{
                system.debug('Not an emergency');
                    IF(record.Use_Standard_Discount_Checkbox__c){   
                        record.Standard_Discount2__c = ql.Discount1__c;
                    }
                else{
                    record.Standard_Multiplier__c = ql.Standard_Multiplier1__c;
                }
            }
            
            update record;
        }
        catch(System.DMlException e){
            Apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR, ' ' + e.getDmlMessage(0)));
        }        
    }
    public void doSubmit(){   
        
        try{
            
            update record;
            
            System.debug('Requested Multiplier =' + record.Requested_Multiplier__c);
            record.User__c = UserInfo.getUserId();
            
            System.debug('Submitted User =' + record.User__c);
            //System.debug('TSM Manager =' + record.TSM_Manager__c);
            //
            record.Submitted_Date__c = system.today();
            record.ApprovalStatus__c = 'Pending';
            update record;
            
            SBAA.ApprovalAPI.submit(record.Id, SBAA__Approval__c.Concession_Request__c);
            Apexpages.addMessage(new ApexPages.message(Apexpages.Severity.INFO, 'Submitted for Approval'));
            //System.debug('L3 Approval =' + record.Requires_L3_Approval__c);
            
            System.debug('Pending Concession =' + record.Quote_Line__r.SBQQ__Quote__r.Concession_Rollup__c);
            submitDisabled = true;
            recallDisabled = false;
            System.debug('Submitted record');            
        }
        catch(Exception e){
            Apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR, ' ' + e.getMessage()));
            Apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR, ' ' + e.getStackTraceString()));
            System.debug('Submit for approval failed');
        }        
    }
    public void doRecall(){        
        
        SBAA.ApprovalAPI.recall(record.Id, SBAA__Approval__c.Concession_Request__c);
        record.ApprovalStatus__c = 'Recalled';
        
        submitDisabled = false;
        recallDisabled = true;
        System.debug('Recalled record');        
    }
    
}