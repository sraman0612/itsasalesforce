@RestResource(urlMapping='/paymentGatewayCalloutClassCPQAPI1/*')
global class PaymentGatewayCalloutClass {
    
    @HttpPost
    global static String calloutToPaymentGateway(String firstName, String woRecordId, String lastName, String last4Digits, String bluefin, String expirationDate, String quoteCurrency, String grandTotal){
        Cyber_Source_Credit_Card__mdt  endpoint = [SELECT Id,Password__c,Soap_End_Point_URL__c, Token__c , Username__c
                                                   FROM Cyber_Source_Credit_Card__mdt 
                                                   WHERE Label = 'CyberSource'];
        
        system.debug('endpoint.Password__c+endpoint.Token__c@@@------>'+endpoint.Password__c+endpoint.Token__c);
        system.debug('endpoint.Username__c@@@------>'+endpoint.Username__c);
        system.debug('@@@endpointMerchantId@@@------>'+endpoint.Username__c);
        system.debug('@@@merchantRefCode@@@------>'+woRecordId);
        system.debug('@@@custFirstName@@@------>'+firstName);
        system.debug('@@@custLastName@@@------>'+lastName);
        system.debug('@@@custQuoteCurrency@@@------>'+quoteCurrency);
        system.debug('@@@GrandTotalAmount@@@------>'+grandTotal);
        system.debug('@@@bluefin@@@------>'+bluefin);
        
        
        // Below Code is used to setup the Body for the Request
        String 	bodyToSend = System.Label.PaymentGatewayCalloutClassCPQAPI1 + System.Label.PaymentGatewayCalloutClassCPQAPI2 ;
        bodyToSend = bodyToSend.replaceAll('@@@endpointPwdPlusToken@@@', endpoint.Password__c+endpoint.Token__c);
        bodyToSend = bodyToSend.replaceAll('@@@endpointUserName@@@', endpoint.Username__c);
        bodyToSend = bodyToSend.replaceAll('@@@endpointMerchantId@@@', endpoint.Username__c);
        bodyToSend = bodyToSend.replaceAll('@@@merchantRefCode@@@', woRecordId);
        bodyToSend = bodyToSend.replaceAll('@@@custFirstName@@@', firstName);
        bodyToSend = bodyToSend.replaceAll('@@@custLastName@@@', lastName);
        bodyToSend = bodyToSend.replaceAll('@@@custQuoteCurrency@@@', quoteCurrency);
        bodyToSend = bodyToSend.replaceAll('@@@GrandTotalAmount@@@', grandTotal);
        bodyToSend = bodyToSend.replaceAll('@@@bluefin@@@', bluefin);
        
        
        // Token For Request 1 Responses
        string resp1;
        String resp1_requestID;
        string resp1_decision;
        string resp1_requestToken;
        string resp1_authorizationCode;
        string resp1_authorizedDateTime;
        string resp1_reconciliationID;
        string resp1_paymentNetworkTransactionID;
        string resp1_referenceID;
        string resp1_cardType;
        string resp1_authRecord;
        string resp1_reasonCode;
        
        // Token For Request 2 Responses
        string resp2;
        String resp2_subscriptionID;
        string resp2_decision;
        string resp2_reasonCode;
        string resp2_requestToken;
        
        // Token For Request 3 Responses
        String resp3;
        String resp3_requestID;
        String resp3_decision;
        String resp3_requestToken;
        String resp3_reasonCode;
        String resp3_missingField;
        String resp3_CreatedDate;
        String resp3_cardAccountNumber;
        String resp3_cardExpirationMonth;
        String resp3_cardExpirationYear;
        String resp3_cardStartMonth;
        String resp3_cardStartYear;
        String resp3_currency;
        String resp3_paymentMethod;
        String resp3_paymentsRemaining;
        String resp3_status;
        String resp3_totalPayments;
        
        
        // request 1 Callout -- START
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint.Soap_End_Point_URL__c);
        request.setMethod('POST');
        request.setHeader('Content-Type','application/xml');
        request.setTimeout(120000);
        request.setBody(bodyToSend);
        HttpResponse response = http.send(request);
        
        system.debug('response--->'+response);
        
        
        resp1 = string.valueOf(response.getBody());
        system.debug('response1--->'+resp1);
        
        resp1_requestID = returnStringBetweenTagsFromSoapResponse(resp1,'<c:requestID>', '</c:requestID>');
        resp1_decision = returnStringBetweenTagsFromSoapResponse(resp1,'<c:decision>', '</c:decision>');
        resp1_requestToken = returnStringBetweenTagsFromSoapResponse(resp1,'<c:requestToken>', '</c:requestToken>');
        resp1_authorizationCode = returnStringBetweenTagsFromSoapResponse(resp1,'<c:authorizationCode>', '</c:authorizationCode>');      
        resp1_authorizedDateTime = returnStringBetweenTagsFromSoapResponse(resp1,'<c:authorizedDateTime>', '</c:authorizedDateTime>');      
        resp1_reconciliationID = returnStringBetweenTagsFromSoapResponse(resp1,'<c:reconciliationID>', '</c:reconciliationID>');   
        resp1_paymentNetworkTransactionID = returnStringBetweenTagsFromSoapResponse(resp1,'<c:paymentNetworkTransactionID>', '</c:paymentNetworkTransactionID>');   
        resp1_referenceID = returnStringBetweenTagsFromSoapResponse(resp1,'<c:referenceID>', '</c:referenceID>');   
        resp1_cardType = returnStringBetweenTagsFromSoapResponse(resp1,'<c:cardType>', '</c:cardType>'); 
        resp1_authRecord = returnStringBetweenTagsFromSoapResponse(resp1,'<c:authRecord>', '</c:authRecord>'); 
       	resp1_reasonCode  = returnStringBetweenTagsFromSoapResponse(resp1,'<c:reasonCode>', '</c:reasonCode>'); 
        // request 1 Callout -- END
        
        // request 2 Callout -- START
        if(resp1_requestID != '' && resp1_decision != 'REJECT' && resp1_decision == 'ACCEPT'){
            String 	bodyToSendForResponse2 ='<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><wsse:UsernameToken><wsse:Username>'+endpoint.Username__c+'</wsse:Username>';
            bodyToSendForResponse2 +=  '<wsse:Password>'+endpoint.Password__c+endpoint.Token__c+'</wsse:Password>';
            bodyToSendForResponse2 +=  '</wsse:UsernameToken></wsse:Security></SOAP-ENV:Header><SOAP-ENV:Body><requestMessage xmlns="urn:schemas-cybersource-com:transaction-data-1.134"><merchantID>'+endpoint.Username__c+'</merchantID><merchantReferenceCode>'+woRecordId+'</merchantReferenceCode><recurringSubscriptionInfo><frequency>on-demand</frequency></recurringSubscriptionInfo>';
            bodyToSendForResponse2 += '<paySubscriptionCreateService run="true"><paymentRequestID>'+resp1_requestID+'</paymentRequestID></paySubscriptionCreateService></requestMessage></SOAP-ENV:Body></SOAP-ENV:Envelope>';
            Http http2 = new Http();
            HttpRequest request2 = new HttpRequest();
            request2.setEndpoint(endpoint.Soap_End_Point_URL__c);
            request2.setMethod('POST');
            request2.setHeader('Content-Type','application/xml');
            request2.setTimeout(120000);
            request2.setBody(bodyToSendForResponse2);
            HttpResponse response2 = http2.send(request2);
            system.debug('response2--->'+response2);
            resp2 = string.valueOf(response2.getBody());
            resp2_subscriptionID = returnStringBetweenTagsFromSoapResponse(resp2,'<c:subscriptionID>', '</c:subscriptionID>');
            resp2_decision = returnStringBetweenTagsFromSoapResponse(resp1,'<c:decision>', '</c:decision>');
            resp2_requestToken = returnStringBetweenTagsFromSoapResponse(resp1,'<c:requestToken>', '</c:requestToken>');
            resp2_reasonCode = returnStringBetweenTagsFromSoapResponse(resp1,'<c:reasonCode>', '</c:reasonCode>');
            // request 2 Callout -- END
            
            // request 3 Callout -- START
            if(resp2_subscriptionID != '' && resp2_decision == 'ACCEPT'){
                String 	bodyToSendForResponse3 ='<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><wsse:UsernameToken><wsse:Username>'+endpoint.Username__c+'</wsse:Username>';
                bodyToSendForResponse3 +=  '<wsse:Password>'+endpoint.Password__c+endpoint.Token__c+'</wsse:Password>';
                bodyToSendForResponse3 +=  '</wsse:UsernameToken></wsse:Security></SOAP-ENV:Header><SOAP-ENV:Body><requestMessage xmlns="urn:schemas-cybersource-com:transaction-data-1.134"><merchantID>'+endpoint.Username__c+'</merchantID><merchantReferenceCode>'+woRecordId+'</merchantReferenceCode>';
                bodyToSendForResponse3 += '<recurringSubscriptionInfo><subscriptionID>'+resp2_subscriptionID+'</subscriptionID></recurringSubscriptionInfo><paySubscriptionRetrieveService run="true"></paySubscriptionRetrieveService></requestMessage></SOAP-ENV:Body></SOAP-ENV:Envelope>';
                system.debug(bodyToSendForResponse3);
                Http http3 = new Http();
                HttpRequest request3 = new HttpRequest();
                request3.setEndpoint(endpoint.Soap_End_Point_URL__c);
                request3.setMethod('POST');
                request3.setHeader('Content-Type','application/xml');
                request3.setTimeout(120000);
                request3.setBody(bodyToSendForResponse3);
                HttpResponse response3 = http3.send(request3);
                system.debug('response3--->'+response3);
                resp3 = string.valueOf(response3.getBody());
                resp3_requestID = returnStringBetweenTagsFromSoapResponse(resp3,'<c:requestID>', '</c:requestID>');
                resp3_decision = returnStringBetweenTagsFromSoapResponse(resp3,'<c:decision>', '</c:decision>');
                resp3_requestToken = returnStringBetweenTagsFromSoapResponse(resp3,'<c:requestToken>', '</c:requestToken>');
                resp3_reasonCode = returnStringBetweenTagsFromSoapResponse(resp3,'<c:reasonCode>', '</c:reasonCode>');
                resp3_missingField = returnStringBetweenTagsFromSoapResponse(resp3,'<c:missingField>', '</c:missingField>');
                resp3_CreatedDate = returnStringBetweenTagsFromSoapResponse(resp3,'<wsu:Created>', '</wsu:Created>');
                resp3_cardAccountNumber = returnStringBetweenTagsFromSoapResponse(resp3,'<c:cardAccountNumber>', '</c:cardAccountNumber>');
                resp3_cardExpirationMonth = returnStringBetweenTagsFromSoapResponse(resp3,'<c:cardExpirationMonth>', '</c:cardExpirationMonth>');
                resp3_cardExpirationYear = returnStringBetweenTagsFromSoapResponse(resp3,'<c:cardExpirationYear>', '</c:cardExpirationYear>');
                resp3_cardStartMonth = returnStringBetweenTagsFromSoapResponse(resp3,'<c:cardStartMonth>', '</c:cardStartMonth>');
                resp3_cardStartYear = returnStringBetweenTagsFromSoapResponse(resp3,'<c:cardStartYear>', '</c:cardStartYear>');
                resp3_currency = returnStringBetweenTagsFromSoapResponse(resp3,'<c:currency>', '</c:currency>');
                resp3_paymentMethod = returnStringBetweenTagsFromSoapResponse(resp3,'<c:paymentMethod>', '</c:paymentMethod>');
                resp3_paymentsRemaining = returnStringBetweenTagsFromSoapResponse(resp3,'<c:paymentsRemaining>', '</c:paymentsRemaining>');
                resp3_status = returnStringBetweenTagsFromSoapResponse(resp3,'<c:status>', '</c:status>');
                resp3_totalPayments = returnStringBetweenTagsFromSoapResponse(resp3,'<c:totalPayments>', '</c:totalPayments>');
                
                system.debug('resp1_requestID-->'+resp1_requestID);
                system.debug('resp1_decision-->'+resp1_decision);
                system.debug('resp1_requestToken-->'+resp1_requestToken);
                system.debug('resp1_authorizationCode-->'+resp1_authorizationCode);
                system.debug('resp1_authorizedDateTime-->'+resp1_authorizedDateTime);
                system.debug('resp1_reconciliationID-->'+resp1_reconciliationID);
                system.debug('resp1_paymentNetworkTransactionID-->'+resp1_paymentNetworkTransactionID);
                system.debug('resp1_referenceID-->'+resp1_referenceID);
                system.debug('resp1_cardType-->'+resp1_cardType);
                system.debug('resp1_authRecord-->'+resp1_authRecord);
                system.debug('resp2_subscriptionID-->'+resp2_subscriptionID);
                system.debug('resp2_decision-->'+resp2_decision);
                system.debug('resp3_requestID-->'+resp3_requestID);
                system.debug('resp3_decision-->'+resp3_decision);
                system.debug('resp3_reasonCode-->'+resp3_reasonCode);
                system.debug('resp3_missingField-->'+resp3_missingField);
                system.debug('resp3_CreatedDate-->'+resp3_CreatedDate);
                system.debug('resp3_cardAccountNumber-->'+resp3_cardAccountNumber);
                system.debug('resp3_cardExpirationMonth-->'+resp3_cardExpirationMonth);
                system.debug('resp3_cardExpirationYear-->'+resp3_cardExpirationYear);
                system.debug('resp3_cardStartMonth-->'+resp3_cardStartMonth);
                system.debug('resp3_cardStartYear-->'+resp3_cardStartYear);
                system.debug('resp3_currency-->'+resp3_currency);
                system.debug('resp3_paymentMethod-->'+resp3_paymentMethod);
                system.debug('resp3_paymentsRemaining-->'+resp3_paymentsRemaining);
                system.debug('resp3_status-->'+resp3_status);
                system.debug('resp3_totalPayments-->'+resp3_totalPayments);
                // DECISION
                // request 3 Callout -- END
                
                //All below Code is for returning back the result with additional parameter from response
                if(resp3_decision != 'REJECT' && resp3_decision != 'ERROR' && resp3_decision != ''){
                    
                    return 'PASS'+'~~~'+resp1_requestID+'~~'+resp1_decision+'~~'+resp1_requestToken+'~~'+resp1_authorizationCode+'~~'+resp1_authorizedDateTime+'~~'+resp1_reconciliationID+'~~'+resp1_paymentNetworkTransactionID+'~~'+resp1_referenceID+'~~'+resp1_cardType+'~~'+resp1_authRecord+'~~'+resp2_subscriptionID+'~~'+resp2_decision+'~~'+'~~'+resp3_requestID+'~~'+resp3_decision+'~~'+resp3_reasonCode+'~~'+resp3_missingField+'~~'+resp3_CreatedDate+'~~'+resp3_cardAccountNumber+'~~'+resp3_cardExpirationMonth+'~~'+resp3_cardExpirationYear+'~~'+resp3_cardStartMonth+'~~'+resp3_cardStartYear+'~~'+resp3_currency+'~~'+resp3_paymentMethod+'~~'+resp3_paymentsRemaining+'~~'+resp3_status+'~~'+resp3_totalPayments;
                }else if(resp3_reasonCode == '150' && resp3_decision == 'ERROR') {
                    // record would be created here with all the fields information
                    return 'Unable to decrypt encrypted_payment_data for provider'+'~~'+resp3_reasonCode+'~~'+resp3_decision;
                }else{
                    return 'Unable to decrypt encrypted_payment_data for provider'+'~~'+resp3_reasonCode+'~~'+resp3_decision;
                }
            }else{
                // Record would be created here with all the fields information
                
                return 'Unable to decrypt encrypted_payment_data for provider'+'~~'+resp2_reasonCode+'~~'+resp2_decision;
            }
        }else{
            // record would be created here with all the fields information
            return 'Unable to decrypt encrypted_payment_data for provider'+'~~'+resp3_reasonCode+'~~'+resp1_decision;
        }
    }
    
    // This below code is use to parse the content between the starting and end tags from the response
    // param1: it would contain response in string format
    // param2: it contains the starting tag
    // param3: it contains the end tag
    public static string returnStringBetweenTagsFromSoapResponse(String totalResponse, String StartingTag, String EndTag){
        String finalValueBetweentags = totalResponse.substringBetween(StartingTag, EndTag);
        System.debug(finalValueBetweentags);
        return finalValueBetweentags;
    }
    
    
}