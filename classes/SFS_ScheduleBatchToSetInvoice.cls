/*=========================================================================================================
* @author Yadav, Arati
* @date 07/10/2021
* @description:This is used to update the Rental Invoice status to 'Submitted
* @Story Number: SIF-3937

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/

global class SFS_ScheduleBatchToSetInvoice implements schedulable {
    
   global void execute(SchedulableContext sc) {
     SFS_batchClassToSetInvoiceRentalStatus b = new SFS_batchClassToSetInvoiceRentalStatus(); 
     database.executebatch(b,1);
       
   }
}