/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 15/03/2022
* @description: SFS_ATPInterfaceOutboundIntegration
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001      15/03/2022  SFS_ATPInterfaceOutboundIntegration from SFDC to Oracle
============================================================================================================================================================*/
//Get PRLI Ids from related list via VF page

public class SFS_ATPInterfaceOutboundIntegration {
    public static SFS_ATP_Check_Interface__mdt[] atpMAP = [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                           FROM SFS_ATP_Check_Interface__mdt where SFS_Node_Order__c !=null Order By SFS_Node_Order__c asc];
    public static String xmlString = '';
    public static Map<Double, String> atpXmlMap = new Map<Double, String>();
    public static List<ProductRequestLineItem> prliUpdateList = new List<ProductRequestLineItem>();
    public static SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT SFS_Endpoint_URL__c, SFS_Username__c, SFS_Password__c
                                                               FROM SFS_SFS_Integration_Endpoint__mdt WHERE Label =: 'XXONT1360'];
    
    @InvocableMethod(label='SFS_ATPInterfaceOutboundIntegration1' description='ATP Check from SFDC to Oracle' category='ProductRequestLineItem')
    public static void Run(List<List<Id>> prliIds){
        //Main Method
        List<Id> prliList = new List<Id>();
        for(Integer i=0;i < prliIds.size();i++){
            prliList = prliIds.get(i);
        }
        SFS_ATPInterfaceBatch objbatch = new SFS_ATPInterfaceBatch(prliList);
        Database.executeBatch(objbatch,1);
    }
    public static void xmlStructure(Map<String,Object> productRequestLineItemFields,ProductRequestLineItem prli,Map<String, String> divMap){
        System.debug('@@@@@@@@productRequestLineItemFields'+productRequestLineItemFields);
        System.debug('@@@@@@@@prli'+prli);
        Map<String, Object> AtpFieldValues = new Map<String, Object>();
        try{
            for(String field : productRequestLineItemFields.keyset()){
                AtpFieldValues.put(field.toLowerCase(), prli.get(field)); 
            }
            List<Double> nodeList = new List<Double>();
            for(SFS_ATP_Check_Interface__mdt m: atpMAP){
                //Some data will need to be hardcoded. We will check for fields that do not need to be hardcoded first.
                nodeList.add(m.SFS_Node_Order__c);
                if(m.SFS_HardCoded_Flag__c == false ){
                    String sfField = m.SFS_Salesforce_Field__c;
                    if(AtpFieldValues.containsKey(sfField)){
                        double nodeOrder = m.SFS_Node_Order__c;
                        if(AtpFieldValues.get(sffield) != null){
                            //Making sure sffield != null. If not replace with values in SA map, else add the m.SFS_XML_Full_Name value to the map.
                            String xmlfullName = m.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(AtpFieldValues.get(sffield));
                            if(sffield == 'sfs_external_id__c'){
                                String externalId = String.ValueOf(AtpFieldValues.get(sffield)); 
                                replacement = externalId+system.now();
                            }
                            else if(sffield == 'quantityrequested'){
                                Integer quReq = Integer.valueOf(AtpFieldValues.get(sffield));
                                replacement = String.ValueOf(quReq);
                            }
                            String newpa = XMLFullName.replace(sffield,replacement); 
                            atpXmlMap.put(nodeOrder,newpa);
                        }  
                        else if(AtpFieldValues.get(sffield) == null){
                            atpXmlMap.put(m.SFS_Node_Order__c, m.SFS_XML_Full_Name__c);
                        }                   
                    }
                    else if(!AtpFieldValues.ContainsKey(sffield)){
                        String empty = '';
                        String replacement = m.SFS_XML_Full_Name__c.replace(sffield, empty);
                        atpXmlMap.put(m.SFS_Node_Order__c, replacement);
                    }
                }
                else if (m.SFS_HardCoded_Flag__c == true){                    
                    atpXmlMap.put(m.SFS_Node_Order__c, m.SFS_XML_Full_Name__c);
                }  
            }
            nodeList.Sort();
            List<String> finalXML = new List<String>();
            for(Double n : nodeList){
                finalXML.add(atpXmlMap.get(n));         
            }            
            for(String s : finalXML){
                xmlString = xmlString + s;
            }
            String interfaceLabel = 'ATP Check from SFDC to Oracle';
            system.debug('@@@@@@@@@xmlString'+xmlString);
            HttpMethod(xmlString,interfaceLabel,prli.Id,divMap);
        }
        catch(SObjectException e){
            system.debug('@@@exception'+e.getMessage());
        }
    }
    public static void HttpMethod(String xmlString, String interfaceLabel,String prliId,Map<String, String> divMap){
        try{
            system.debug('insideHttpCallout'+endpoint.SFS_Endpoint_URL__c);
            system.debug('divMap'+divMap);
            //Http Request
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint.SFS_Endpoint_URL__c);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'text/xml');
            request.setTimeout(120000);
            request.setBody(xmlString);
            System.debug('Payload: ' + xmlString);
            //Get Http Response
            HttpResponse response = http.send(request);
            system.debug('@@@response'+response);
            System.debug('STATUS: ' + response.getStatusCode());
            System.debug('Error Message: ' +response.getBody());
            String statusFlag; String whse;String avlDate; String resMessage;           
            Dom.Document doc = response.getBodyDocument();
            system.debug('doc'+doc);
            //Retrieve the root element for this document.
            Dom.XMLNode envelope = doc.getRootElement();
            system.debug('envelope'+envelope);
            for(DOM.XmlNode node : envelope.getChildElements())
            { 
                system.debug('node.getName()'+node.getName());
                if(node.getName() == 'Body'){
                    system.debug('node.getName()'+node.getName());
                    for(Dom.XmlNode ChildNode: node.getChildElements()) {
                        system.debug('ChildNode.getName()'+ChildNode.getName());
                        if(ChildNode.getName() == 'ATPResponse'){
                            for(Dom.XmlNode CDArea: ChildNode.getChildElements()){
                                if(CDArea.getName() == 'ControlArea'){
                                    system.debug('CDArea'+CDArea);
                                    statusFlag = CDArea.getChildElement('Status',null).getText();
                                    resMessage = CDArea.getChildElement('Messsage',null).getText();
                                    system.debug('statusFlag'+statusFlag);
                                    system.debug('resMessage'+resMessage);
                                }
                                if(CDArea.getName() == 'DataArea'){
                                    for(Dom.XmlNode GItem : CDArea.getChildElements()){
                                        if(GItem.getName() == 'GItem'){
                                            for(Dom.XmlNode Item : GItem.getChildElements()){
                                                if(Item.getName() == 'Item'){
                                                    whse = Item.getChildElement('BestWHSE', null).getText();
                                                }
                                                for(Dom.XmlNode GAvailability : Item.getChildElements()){
                                                    if(GAvailability.getName() == 'GAvailability'){
                                                        for(Dom.XmlNode Availability : GAvailability.getChildElements()){
                                                            if(Availability.getName() == 'Availability'){
                                                                for(Dom.XmlNode AvailabieDate : Availability.getChildElements()){
                                                                  if(AvailabieDate.getName() =='AvailableDate'){
                                    								avlDate = AvailabieDate.getText();
                                								 }    
                                                                }   
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } 
                }   
            } 
            if(whse!=null && avlDate!=null){
                parseXMLSuccess(whse,prliId,'',avlDate,divMap);
            }
            else if(response.getStatusCode() == 504){
                parseXMLError(doc,prliId);
            }
            else{
                parseXMLErrHandler(prliId,resMessage);
            }
           if(prliUpdateList.size() > 0){
                update prliUpdateList;
                system.debug('@prliUpdateList'+prliUpdateList); 
            }
        }
        catch(SObjectException e){
            system.debug('@@@exception'+e.getMessage());
        }
    }  
    public static void parseXMLSuccess(String whse,Id prliId,String resMessage,String avlDate,Map<String, String> divMap){
        try{
            system.debug('divMap'+divMap);
            ProductRequestLineItem prliUpdate = new ProductRequestLineItem();            
            prliUpdate.SFS_ATP_Integration_Status__c = 'APPROVED';
            prliUpdate.SFS_ATP_Integration_Response__c = resMessage;
            Date availDate = ParsedDate(avlDate);
            prliUpdate.SFS_Available_Date__c = Date.valueOf(availDate);
            system.debug('whse'+whse);
            system.debug('divMap'+divMap);
            Id sourceDiv = divMap.get(whse);
            system.debug('sourceDiv'+sourceDiv);
            prliUpdate.SFS_Source_Location__c = sourceDiv;
            prliUpdate.Id = prliId;
            prliUpdateList.add(prliUpdate);
        }
        catch(SObjectException e){
            system.debug('@@@exception'+e.getMessage());
        }
    }
    public static void parseXMLError(Dom.Document doc,Id prliId){
        try{
            String title;
            ProductRequestLineItem prliUpdate = new ProductRequestLineItem();
            //Retrieve the root element for this document.
            Dom.XMLNode html = doc.getRootElement();
            for(DOM.XmlNode head : html.getChildElements()){
                if(head.getName() == 'head'){
                    title = head.getChildElement('title', null).getText();
                }
            }
            prliUpdate.SFS_ATP_Integration_Status__c = 'ERROR';
            prliUpdate.SFS_ATP_Integration_Response__c = title;
            prliUpdate.Id = prliId;
            prliUpdateList.add(prliUpdate);
            system.debug('title'+title);
            system.debug('prliUpdateList'+prliUpdateList);
        }
        catch(SObjectException e){
            system.debug('@@@exception'+e.getMessage());
        }
    }
    public static void parseXMLErrHandler(Id prliId,String resMessage){
        try{
            ProductRequestLineItem prliUpdate = new ProductRequestLineItem();
            prliUpdate.SFS_ATP_Integration_Status__c = 'ERROR';
            prliUpdate.SFS_ATP_Integration_Response__c = resMessage;
            prliUpdate.Id = (Id)prliId;
            prliUpdateList.add(prliUpdate);
            system.debug('resMessage'+resMessage);
            system.debug('prliUpdateList'+prliUpdateList);
        }
        catch(SObjectException e){
            system.debug('@@@exception'+e.getMessage());
        }
    }
    public static Date ParsedDate (String myDate) {
        Integer myMonth = 0;
        String plMonth = myDate.substring(3,6);
        if(plMonth == 'Jan'){
            myMonth = 01;   
        }else if(plMonth == 'Feb'){
            myMonth = 02;   
        }else if(plMonth == 'Mar'){
            myMonth = 03;   
        }else if(plMonth == 'Apr'){
            myMonth = 04;   
        }else if(plMonth == 'May'){
            myMonth = 05;       
        }else if(plMonth == 'Jun'){            
            myMonth = 06;   
        }else if(plMonth == 'Jul'){
            myMonth = 07;   
        }else if(plMonth == 'Aug'){
            myMonth = 08;   
        }else if(plMonth == 'Sep'){
            myMonth = 09;   
        }else if(plMonth == 'Oct'){
            myMonth = 10;  
        }else if(plMonth == 'Nov'){
            myMonth = 11;  
        }else{
            myMonth = 12;
        }
        
        String myDay = myDate.substring(0,2);
        String myYear = myDate.substring(7,11);
        Date myavlDate = Date.newInstance(Integer.valueOf(myYear),myMonth,Integer.valueOf(myDay));
        system.debug('@@@@date'+myavlDate);
        return myavlDate;
    }
}