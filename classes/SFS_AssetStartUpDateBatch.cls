global class SFS_AssetStartUpDateBatch implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts {
    
    global List<id> selectedList;
    
    public SFS_AssetStartUpDateBatch(List<id> selectedList) {
        this.selectedList = selectedList;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT id,AccountId,sfs_source_warehouse__c,sfs_oracle_ship_date__c,Name,SFS_Part_Number__c,SFS_OrganizationCode__c,IRIT_Last_Hours_on_Asset__c,ManufactureDate,Start_up_Date__c,ship_date__c,DelDate__c,Sales_Order__c,Invoice_Number__c,Invoice_Date__c,SFS_Divsion_DealerNumber__c,SFS_CustomerNumber__c,Account_Name__c,SFS_ShippingStreet__c,SFS_ShippingCity__c,SFS_ShippingCountry__c,SFS_ShippingState__c,SFS_ShippingPostalCode__c from asset where Id IN:selectedList';
         return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC, List<Asset> scope) {
         system.debug('@@@@@@@@@@scope'+scope);
         SFS_Country_Code_Mapping__mdt[] countryList;
        List<Id> accountIds=new List<Id>();
        Map<string,string> countryMap=new Map<string,string>();  
        for(Asset asset : scope){
            accountIds.add(asset.AccountId);
        }
         Map<Id,Account> accountMap=new Map<Id,Account>([SELECT Id,Name,IRIT_Customer_Number__c,ShippingStreet,ShippingCity,ShippingState,ShippingCountry,ShippingPostalCode 
                                          From Account where Id IN: accountIds]);
         countryList=[SELECT id,Label,SFS_Locale__c from SFS_Country_Code_Mapping__mdt];
         for(SFS_Country_Code_Mapping__mdt cou:countryList){
            countryMap.put(cou.Label,cou.SFS_Locale__c);
        }     
        Map<String,Object> assetFields = new Map <String, Object>();
        for(Asset asset : scope){
            system.debug('@@dealerNumber'+asset.SFS_Divsion_DealerNumber__c);
            assetFields = asset.getPopulatedFieldsAsMap();
            system.debug('@@assetFields'+assetFields);
            SFS_AssetStartUpDateOutboundIntegration.xmlStructure(assetFields,asset,countryMap,accountMap);
        }   
           
    }   
    
    global void finish(Database.BatchableContext BC) {
        //Database.executeBatch(new SFS_AssetStartUpDateBatch(selectedList),1);
    }
}