public class ensxtx_UTIL_Document_Detail
{
    // removeAllConditions
    //
    // Removing all the conditions that is in the SBO
    // Primarily use for update transaction
    public static ensxsdk.EnosixFramework.DetailObject removeAllConditions(
        ensxsdk.EnosixFramework.DetailObject sboObject, String sboDetailType)
    {
        Object detailInstance = getDetailInstace(sboDetailType);
        ensxsdk.EnosixFramework.DetailObject detailObject;

        if (detailInstance instanceof ensxtx_I_Document_Detail) {
            detailObject = ((ensxtx_I_Document_Detail) detailInstance).removeAllConditions(sboObject);
        }
        return detailObject;
    }

    // updateTextFields()
    //
    // Update the Text fields, (Header and Item)
    // Primarily use for update transaction
    public static ensxsdk.EnosixFramework.DetailObject updateTextFields(
        ensxsdk.EnosixFramework.DetailObject sboObject, ensxtx_DS_Document_Detail docObj, String sboDetailType)
    {
        Object detailInstance = getDetailInstace(sboDetailType);
        ensxsdk.EnosixFramework.DetailObject detailObject;

        if (detailInstance instanceof ensxtx_I_Document_Detail) {
            detailObject = ((ensxtx_I_Document_Detail) detailInstance).updateTextFields(sboObject, docObj);
        }
        return detailObject;
    }

    // buildSBOForReference()
    //
    // Returns the SBO detail object
    // Use before calling the the command to clone document
    public static ensxsdk.EnosixFramework.DetailObject buildSBOForReference(
        String salesDocNumber, String salesDocType, String sboDetailType)
    {
        Object detailInstance = getDetailInstace(sboDetailType);
        ensxsdk.EnosixFramework.DetailObject detailObject;

        if (detailInstance instanceof ensxtx_I_Document_Detail) {
            detailObject = ((ensxtx_I_Document_Detail) detailInstance).buildSBOForReference(salesDocNumber, salesDocType);
        }
        return detailObject;
    }

    // getMatAvailabilityAndPriceScale()
    //
    // Map the Material Availability and Price Scales
    // That was returned from another command
    // public static ensxsdk.EnosixFramework.DetailObject getMatAvailabilityAndPriceScale(ensxsdk.EnosixFramework.DetailObject detailObj,
    //     ensxsdk.EnosixFramework.DetailObject materilAvailabilityObj, ensxsdk.EnosixFramework.DetailObject priceScaleObj, String sboDetailType)
    // {
    //     Object detailInstance = getDetailInstace(sboDetailType);
    //     ensxsdk.EnosixFramework.DetailObject detailObject;

    //     if (detailInstance instanceof ensxtx_I_Document_Detail) {
    //         detailObject = ((ensxtx_I_Document_Detail) detailInstance).getMatAvailabilityAndPriceScale(detailObj, materilAvailabilityObj, priceScaleObj);
    //     }
    //     return detailObject;
    // }

    // convertToSBO
    //
    // Convert DS Object to SBO Detail object
    // Use to build SBO Object for simulation and creation
    public static ensxsdk.EnosixFramework.DetailObject convertToSBO(ensxsdk.EnosixFramework.DetailObject sboObject,
        ensxtx_DS_Document_Detail docObj, Boolean isUpdate, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        Object detailInstance = getDetailInstace(appSettings.SBODetailType);
        ensxsdk.EnosixFramework.DetailObject detailObject;

        if (detailInstance instanceof ensxtx_I_Document_Detail) {
            detailObject = ((ensxtx_I_Document_Detail) detailInstance).convertToSBO(sboObject, docObj, isUpdate, appSettings);
        }
        return detailObject;
    }

    // convertToObject()
    //
    // Convert SBO Detail object to DS Object
    public static void convertToObject(ensxsdk.EnosixFramework.DetailObject sboObject,
        ensxtx_DS_Document_Detail docObj, Boolean isFromGetDetail, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        Object detailInstance = getDetailInstace(appSettings.SBODetailType);

        if (detailInstance instanceof ensxtx_I_Document_Detail) {
            ((ensxtx_I_Document_Detail) detailInstance).convertToObject(sboObject, docObj, isFromGetDetail, appSettings);
        }
    }

    // getSimulateCommand()
    //
    // Returns the simulate command of the Detail SBO
    public static String getSimulateCommand(String sboDetailType)
    {
        Object detailInstance = getDetailInstace(sboDetailType);
        String simulateCommand = '';

        if (detailInstance instanceof ensxtx_I_Document_Detail) {
            simulateCommand = ((ensxtx_I_Document_Detail) detailInstance).getSimulateCommand();
        }
        return simulateCommand;
    }

    // getReferenceDocumentCommand()
    //
    // Returns the reference document command of the detail SBO
    // The command is use for cloning a document
    public static String getReferenceDocumentCommand(String sboDetailType)
    {
        Object detailInstance = getDetailInstace(sboDetailType);
        String referenceDocumentCommand = '';

        if (detailInstance instanceof ensxtx_I_Document_Detail) {
            referenceDocumentCommand = ((ensxtx_I_Document_Detail) detailInstance).getReferenceDocumentCommand();
        }
        return referenceDocumentCommand;
    }

    // getMaterialAvailabilityCommand()
    //
    // Returns the material availability command of the detail SBO
    // public static String getMaterialAvailabilityCommand(String sboDetailType)
    // {
    //     Object detailInstance = getDetailInstace(sboDetailType);
    //     String referenceDocumentCommand = '';

    //     if (detailInstance instanceof ensxtx_I_Document_Detail) {
    //         referenceDocumentCommand = ((ensxtx_I_Document_Detail) detailInstance).getMaterialAvailabilityCommand();
    //     }
    //     return referenceDocumentCommand;
    // }

    // getPriceScaleCommand()
    //
    // Returns the price scale command of the detail SBO
    // public static String getPriceScaleCommand(String sboDetailType)
    // {
    //     Object detailInstance = getDetailInstace(sboDetailType);
    //     String referenceDocumentCommand = '';

    //     if (detailInstance instanceof ensxtx_I_Document_Detail) {
    //         referenceDocumentCommand = ((ensxtx_I_Document_Detail) detailInstance).getPriceScaleCommand();
    //     }
    //     return referenceDocumentCommand;
    // }

    // getDetailInstace()
    //
    // Returns the instance of the detail adapter
    private static Object getDetailInstace(String sboDetailType)
    {
        String className = 'ensxtx_UTIL_' + sboDetailType + '_Detail';
        System.Type detailType = Type.forName(className);
        return detailType.newInstance();
    }
    
    public static ensxtx_DS_Document_Detail.PARTNERS getPartnerFromDocumentDetail(List<ensxtx_DS_Document_Detail.PARTNERS> partnerList, string partnerFunction, boolean create)
    {
        ensxtx_DS_Document_Detail.PARTNERS result = null;
        if (partnerList != null)
        {
            Integer partnerTot = partnerList.size();
            for (Integer partnerCnt = 0 ; partnerCnt < partnerTot ; partnerCnt++)
            {
                ensxtx_DS_Document_Detail.PARTNERS partner = partnerList[partnerCnt];
                if (partnerFunction == partner.PartnerFunction)
                {
                    result = partner;
                    break;
                }
            }
        }

        if (null == result && create)
        {
            result = new ensxtx_DS_Document_Detail.PARTNERS();
            result.PartnerFunction = partnerFunction;
            if (partnerList != null) partnerList.add(result);
        }

        return result;
    }
}