/// enosiX Inc. Generated Apex Model
/// Generated On: 6/5/2019 2:20:19 PM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class TST_EnosixVCSync_Search
{

    public class MockSBO_EnosixVCSync_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixVCSync_Search.class, new MockSBO_EnosixVCSync_Search());
        SBO_EnosixVCSync_Search sbo = new SBO_EnosixVCSync_Search();
        System.assertEquals(SBO_EnosixVCSync_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        SBO_EnosixVCSync_Search.EnosixVCSync_SC sc = new SBO_EnosixVCSync_Search.EnosixVCSync_SC();
        System.assertEquals(SBO_EnosixVCSync_Search.EnosixVCSync_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        SBO_EnosixVCSync_Search.SEARCHPARAMS childObj = new SBO_EnosixVCSync_Search.SEARCHPARAMS();
        System.assertEquals(SBO_EnosixVCSync_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.Material = 'X';
        System.assertEquals('X', childObj.Material);

        childObj.Plant = 'X';
        System.assertEquals('X', childObj.Plant);


    }

    @isTest
    static void testEnosixVCSync_SR()
    {
        SBO_EnosixVCSync_Search.EnosixVCSync_SR sr = new SBO_EnosixVCSync_Search.EnosixVCSync_SR();

        sr.registerReflectionForClass();

        System.assertEquals(SBO_EnosixVCSync_Search.EnosixVCSync_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        SBO_EnosixVCSync_Search.SEARCHRESULT childObj = new SBO_EnosixVCSync_Search.SEARCHRESULT();
        System.assertEquals(SBO_EnosixVCSync_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixVCSync_Search.SEARCHRESULT_COLLECTION childObjCollection = new SBO_EnosixVCSync_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.Material = 'X';
        System.assertEquals('X', childObj.Material);

        childObj.Plant = 'X';
        System.assertEquals('X', childObj.Plant);

        childObj.CharacteristicName = 'X';
        System.assertEquals('X', childObj.CharacteristicName);

        childObj.CharacteristicDescription = 'X';
        System.assertEquals('X', childObj.CharacteristicDescription);

        childObj.CharacteristicValue = 'X';
        System.assertEquals('X', childObj.CharacteristicValue);

        childObj.CharacteristicValueDescription = 'X';
        System.assertEquals('X', childObj.CharacteristicValueDescription);


    }

}