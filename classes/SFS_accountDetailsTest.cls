@isTest
public class SFS_accountDetailsTest {
    @isTest
    public static void searchAccountTest(){
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> billToAccList=SFS_TestDataFactory.createAccounts(1,false);
        billToAccList[0].AccountSource='Web';
        billToAccList[0].IRIT_Customer_Number__c='1234';
        billToAccList[0].RecordTypeId=accRecID;
        insert billToAccList[0];
        
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        
        acclist[0].IRIT_Customer_Number__c='1234';
        acclist[0].Oracle_Number__c='1234';
        acclist[0].Bill_To_Account__c=billToAccList[0].id;
        acclist[1].IRIT_Customer_Number__c='1234';
        acclist[1].Oracle_Number__c='1234';
        acclist[1].Bill_To_Account__c=billToAccList[0].id;
        acclist[0].Type ='Ship To';
        acclist[1].Type ='Prospect';
      
        insert accList;
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(2,accList[0].Id,false);
        svcAgreementList[0].SFS_Status__c='APPROVED';
        svcAgreementList[1].SFS_Status__c='Draft';
        Insert svcAgreementList;
        List<ServiceContract> svcAgreementList1=SFS_TestDataFactory.createServiceAgreement(2,accList[1].Id,false);
         svcAgreementList1[0].SFS_Status__c='APPROVED';
        svcAgreementList1[1].SFS_Status__c='Draft';
        Insert svcAgreementList1;
        
         List<Division__C> divList=SFS_TestDataFactory.createDivisions(1, false);
         insert divList[0];
         List<Schema.Location> locationList=SFS_TestDataFactory.createLocations(1,divList[0].Id, true);
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,locationList[0].Id,divList[0].Id,null,false);
        insert woList;
         List<WorkOrder> woList1=SFS_TestDataFactory.createWorkOrder(1,accList[1].Id,locationList[0].Id,divList[0].Id,null,false);
        insert woList1;
        Customer_Relationship__c cr=new Customer_Relationship__c(Shipping_Customer__c='1234',Name='1234');
        insert cr;
        Test.startTest();  
        SFS_accountDetails.searchAccount('test','ServiceContract','1234','Ship To','1234');
        SFS_accountDetails.searchAccount('test','ServiceContract','1234','Prospect','1234');
        SFS_accountDetails.searchAccount('test','ServiceContract','1234',null,'1234');
        SFS_accountDetails.searchAccount('test','WorkOrder','1234','Ship To','1234');
        SFS_accountDetails.searchAccount('test','WorkOrder','1234','Prospect','1234');
        SFS_accountDetails.getAccount('Ship To','ServiceContract','1234','1234');
        SFS_accountDetails.getAccount('Prospect','ServiceContract','1234','1234');
        SFS_accountDetails.getAccount(null,'ServiceContract','1234','1234');
        SFS_accountDetails.getAccount('Ship To','WorkOrder','1234','1234');
        SFS_accountDetails.getAccount('Prospect','WorkOrder','1234','1234');
      
        Test.stopTest();
    }

}