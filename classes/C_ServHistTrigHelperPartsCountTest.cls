@isTest
public class C_ServHistTrigHelperPartsCountTest 
{
    @isTest
    public static void runSetup()
    {
        Asset sn = new Asset(
            Name='DM006400'
        );
        insert sn;
        
        Service_History__c sh1 = new Service_History__c
            (
                Oil_Filter__c='ZS1088421:T, ZS1088422:T, ZS1088421:F',
                Air_Filter__c='2118315:T, 2118315:F, 2118314:F',
                Separator__c='A10533574:T, A10533574:F, A10533566:F, A10533566:F',
                Cabinet_Filter__c='300USC784:T',
                Control_Box_Filter__c='338USC6032:T',
                Serial_Number__c=sn.Id,
                Date_of_Service__c=System.today(),
                Current_Hours__c=100
            );
        insert sh1;
        
        Service_History__c sh2 = new Service_History__c
            (
                Id=sh1.Id,
                Oil_Filter__c='ZS1088421:T, ZS1088421:F',
                Air_Filter__c='A11207674:T, A11207674:F',
                Separator__c='A10533574:T, A10533574:F, A10533566:F	',
                Cabinet_Filter__c='300USC784:T',
                Control_Box_Filter__c='338USC6032:T',
                Serial_Number__c=sn.Id,
                Date_of_Service__c=System.today(),
                Current_Hours__c=100
            );
        update sh2;
    }
}