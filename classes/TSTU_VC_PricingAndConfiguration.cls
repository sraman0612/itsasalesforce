@isTest
public class TSTU_VC_PricingAndConfiguration
{
    public class MOC_EnosixVC_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock,
            ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock,
            ensxsdk.EnosixFramework.DetailSBOInitMock
    {
        public boolean success = true;
        public boolean throwException = false;

        public void setSuccess(boolean success)
        {
            this.success = success;
        }

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixVC_Detail.EnosixVC result = new SBO_EnosixVC_Detail.EnosixVC();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixVC_Detail.EnosixVC result = (SBO_EnosixVC_Detail.EnosixVC) obj;
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string command, ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixVC_Detail.EnosixVC result = (SBO_EnosixVC_Detail.EnosixVC) obj;
            result.setSuccess(success);
            return result;
        }

        public  ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject  initialState)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixVC_Detail.EnosixVC result = new SBO_EnosixVC_Detail.EnosixVC();
            result.setSuccess(success);
            return result;
        }
    }

    @isTest
    public static void test_logger()
    {
        ensxsdk.Logger logger = UTIL_VC_PricingAndConfiguration.logger;
    }

    @isTest
    public static void test_getProductIdForSAPMaterialNumber()
    {
        Product2 product = new Product2();
        product.Name = 'Name';
        product.ProductCode = 'sapMaterialNumber';
        product.ENSX_EDM__Material__c = 'sapMaterialNumber';
        upsert product;
        String prodId = UTIL_VC_PricingAndConfiguration.getProductIdForSAPMaterialNumber('sapMaterialNumber');
    }

    @isTest
    public static void test_getSAPMaterialNumberFromProductId()
    {
        Product2 product = new Product2();
        product.Name = 'Name';
        product.ProductCode = 'sapMaterialNumber';
        product.ENSX_EDM__Material__c = 'sapMaterialNumber';
        upsert product;
        String prodId = UTIL_VC_PricingAndConfiguration.getSAPMaterialNumberFromProductId(product.Id);
    }

    @isTest
    public static void test_stripLeadingZeros()
    {
        System.assertEquals('421', UTIL_VC_PricingAndConfiguration.stripLeadingZeros('00421'));
        System.assertEquals('0', UTIL_VC_PricingAndConfiguration.stripLeadingZeros('0'));
    }

    @isTest
    public static void test_getInitialConfigFromMaterialAndPricing()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixVC_Detail.class, mocEnosixVCDetail);
        ENSX_VCConfiguration serializableCfg = UTIL_VC_PricingAndConfiguration.getInitialConfigFromMaterialAndPricing('material', new ENSX_VCPricingConfiguration());
    }

    @isTest
    public static void test_getInitialConfigFromMaterialAndPricingAndCustomConfig()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixVC_Detail.class, mocEnosixVCDetail);
        try
        {
            DS_VCMaterialConfiguration config =
                UTIL_VC_PricingAndConfiguration.getInitialConfigFromMaterialAndPricingAndCustomConfig('material', new ENSX_VCPricingConfiguration());
        }
        catch(Exception e)
        {}
        mocEnosixVCDetail.setThrowException(true);
        try
        {
            DS_VCMaterialConfiguration config = 
                UTIL_VC_PricingAndConfiguration.getInitialConfigFromMaterialAndPricingAndCustomConfig('material', new ENSX_VCPricingConfiguration());
        }
        catch(Exception e)
        {}
    }

    @isTest
    public static void test_applyPricingConfigurationToSBO()
    {
        SBO_EnosixVC_Detail.EnosixVC sboConfig = UTIL_VC_PricingAndConfiguration.applyPricingConfigurationToSBO(new SBO_EnosixVC_Detail.EnosixVC (), new ENSX_VCPricingConfiguration());
    }
    
    @isTest
    public static void test_getInitializedConfigSBOModelFromBOM()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixVC_Detail.class, mocEnosixVCDetail);
        mocEnosixVCDetail.setSuccess(false);
        List<DS_VCCharacteristicValues> BOM = new List<DS_VCCharacteristicValues>();
        DS_VCCharacteristicValues VCValue = new DS_VCCharacteristicValues();
        VCValue.UserModified = false;
        BOM.add(VCValue);
        UTIL_VC_PricingAndConfiguration.getInitializedConfigSBOModelFromBOM('', null, BOM);
    }
}