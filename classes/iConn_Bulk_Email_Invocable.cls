public class iConn_Bulk_Email_Invocable {
    
    @InvocableMethod(label='Send iConn Bulk Alert Email' description = 'Sends bulkified email to end user and/or current servicer associated to this alerts Serial Number' category='Email')
    public static void sendiConnBulkEmail(List<iConn_Bulk_Email_Wrapper> wrappers) {
        EmailTemplate contactET = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'iConn_Alarm_VisualForce'];
        EmailTemplate distET = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'iConn_Alarm_Dist_VisualForce'];
        
        OrgWideEmailAddress oea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'iconn.notification.qcy@gardnerdenver.com']; 
        
        
        List<Messaging.SingleEmailMessage> listOfEmails = new List<Messaging.SingleEmailMessage>();
        
        
        if(wrappers[0].iConnContactIds != null) {
            for(String contactId : wrappers[0].iConnContactIds) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(contactET.Id);
                mail.setWhatId(wrappers[0].alert.Id);
                mail.setSaveAsActivity(true);
                mail.setOrgWideEmailAddressId(oea.Id);
                mail.setTargetObjectId(contactId);
                if(wrappers[0].bccAddresses != null) {
                    mail.setBccAddresses(wrappers[0].bccAddresses);
                }
                listOfEmails.add(mail);
            }
       } 
        
        if(wrappers[0].servicerContactId != null) {
            for(String distId : wrappers[0].servicerContactId) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(distET.Id);
                mail.setWhatId(wrappers[0].alert.Id);
                mail.setSaveAsActivity(true);
                mail.setOrgWideEmailAddressId(oea.Id);
                mail.setTargetObjectId(distId);
                listOfEmails.add(mail);
            }
         }
        
        
        if(!Test.isRunningTest()) {
            messaging.sendEmail(listOfEmails);
        }
        
        //fade into oblivion
        return;
        
    }
    
    public class iConn_Bulk_Email_Wrapper {
        @InvocableVariable(label='Alert' description='' required=true)
        public Alert__c alert; 
        
        @InvocableVariable(label='Contact Ids' description='Recepient Id - Either Serial_Number_Contact__r.Contact__c Or Contact.Id' required=false)
        public String[] iConnContactIds;
        
        @InvocableVariable(label='Servicer Contact Id' description ='Servicer Contact Id')
        public String[] servicerContactId;
        
        @InvocableVariable(label='BCC Addresses' description='Addresses to BCC the alert' required=false)
        public List<String> bccAddresses;
        
        
    }
}