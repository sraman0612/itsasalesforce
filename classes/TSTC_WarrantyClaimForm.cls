@IsTest
public class TSTC_WarrantyClaimForm {
    public class MockSBO_EnosixServiceNotification_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock 
    {
        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) { 
            return new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification(); 
        }
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { return obj; }
    }

    public class MockSBO_EnosixSO_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) { return obj; }
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { return obj; }
    }

    public class MockSBO_EnosixServNotifUpdate_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { return obj; }
    }
    
    @IsTest
    public static void testGetSalesOrderDetail() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Detail.class, new MockSBO_EnosixSO_Detail());

        CTRL_WarrantyClaimForm.getSalesOrderDetail('X');
    }
    
    @IsTest
    public static void testVerifyServiceNotification() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotification_Detail.class, new MockSBO_EnosixServiceNotification_Detail());
        Account account = new Account(Name = 'name', BillingCountry = 'United States');
        UTIL_SFAccount.setAccountCustomerNumber(account, 'test');
        insert account;
        
        Product2 product = new Product2();
        product.Name = 'X';
        product.productCode = 'XYZ';
        product.FLD_Distribution_Channel__c = 'CM';
        
        insert product;

        Asset asset = new Asset();
        asset.Name = '8888';
        asset.put(UTIL_AssetSyncBatch.SFSyncKeyField,'8888-9999');
        asset.Product2Id = product.Id;
        insert asset;

        CTRL_WarrantyClaimForm.OBJ_WarrantyClaim obj = new CTRL_WarrantyClaimForm.OBJ_WarrantyClaim();
        
        obj.accountId = account.Id;
        obj.serialNumberId = asset.Id;
        obj.failedPartNumber = 'XYZ';
        obj.failedSerialNumber = '8888';
        obj.salesOrg = 'X';
        obj.distributionChannel = 'CM';
        obj.dateOfMalfunction = System.today();
        obj.locationNameLine1 = 'X';
        obj.locationNameLine2 = 'X';
        obj.locationStreet = 'X';
        obj.locationCity = 'X';
        obj.locationPostalCode = 'X';
        obj.locationRegion = 'X';
        obj.locationCountry = 'X';
        obj.locationCounty = 'X';
        obj.locationPhone = 'X';
        obj.locationFax = 'X';
        obj.dateOfMalfunction = System.today();
        obj.serviceDate = System.today();

        CTRL_WarrantyClaimForm.createServiceNotificationImpl(JSON.serialize(obj));
        obj.serialNumberId = null;
        CTRL_WarrantyClaimForm.createServiceNotificationImpl(JSON.serialize(obj));
    }

    @IsTest
    public static void testSaveServiceNotification() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotification_Detail.class, new MockSBO_EnosixServiceNotification_Detail());

        CTRL_WarrantyClaimForm.saveServiceNotification(new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification());
    }

    @IsTest
    public static void testSimulateSalesOrder() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Detail.class, new MockSBO_EnosixSO_Detail());

        Account account = new Account(Name = 'name', BillingCountry = 'United States');
        UTIL_SFAccount.setAccountCustomerNumber(account, 'test');
        insert account;

        Product2 product = new Product2();
        product.Name = 'X';
        product.productCode = 'XYZ';
        product.FLD_Distribution_Channel__c = 'CM';
        
        insert product;

        Asset asset = new Asset();
        asset.Name = '8888';
        asset.put(UTIL_AssetSyncBatch.SFSyncKeyField,'8888-9999');
        asset.Product2Id = product.Id;
        insert asset;

        CTRL_WarrantyClaimForm.OBJ_WarrantyClaim obj = new CTRL_WarrantyClaimForm.OBJ_WarrantyClaim();
        
        obj.accountId = account.Id;
        obj.partsClaim = false;
        obj.serialNumberId = asset.Id;
        obj.salesOrg = 'X';
        obj.distributionChannel = 'CM';
        obj.sapServiceNotificationNumber = 'X';
        obj.customerReferenceNumber = 'X';
        obj.serviceTechnicianNumber = 'X';
        obj.hoursInService = 'X';
        obj.modelNumber = 'X';
        obj.serialNumber = 'X';
        obj.failedPartNumber = 'XYZ';
        obj.failedSerialNumber = '8888';
        obj.failedHoursInService = 1234;
        obj.failedStartupDate = System.today();
        obj.laborHours = 1;
        obj.travelHours = 1;
        obj.travelMiles = 1;
        obj.freight = 11.0;
        obj.localMaterial = 10.0;
        obj.otherExpenses = 12.0;
        obj.replacementPartNumber = 'X';
        obj.replacementSerialNumber = 'X';
        obj.replacementGDInvoiceNumber = 'X';
        obj.replacementGDInvoiceItemNumber = 'X';
        obj.locationNameLine1 = 'X';
        obj.locationNameLine2 = 'X';
        obj.locationStreet = 'X';
        obj.locationCity = 'X';
        obj.locationPostalCode = 'X';
        obj.locationRegion = 'X';
        obj.locationCountry = 'X';
        obj.locationCounty = 'X';
        obj.locationPhone = 'X';
        obj.locationFax = 'X';
        obj.itemJSON = '"[{\"PartNo\":\"1234\",\"GDInvoiceNumber\":\"1234\",\"GDInvoiceItemNumber\":\"1234\"}]"';
        obj.claimText = 'X';
        obj.dateOfMalfunction = System.today();
        obj.serviceDate = System.today();

        CTRL_WarrantyClaimForm.simulateSalesOrder(JSON.serialize(obj), true);
        obj.serialNumberId = null;
        obj.customerReferenceNumber = '';
        CTRL_WarrantyClaimForm.simulateSalesOrder(JSON.serialize(obj), false);
    }

    @IsTest
    public static void testUpdateServiceNotification() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServNotifUpdate_Detail.class, new MockSBO_EnosixServNotifUpdate_Detail());
        
        CTRL_WarrantyClaimForm.updateServiceNotification('X');
    }

    @IsTest
    public static void testMyAddFrameworkMessages() {
        CTRL_WarrantyClaimForm.myAddFrameworkMessages(new List<ensxsdk.EnosixFramework.Message>{
            new ensxsdk.EnosixFramework.Message(ensxsdk.EnosixFramework.MessageType.ERROR,'TEST1'),
            new ensxsdk.EnosixFramework.Message(ensxsdk.EnosixFramework.MessageType.ERROR,'TEST2')
        });
        
        SBO_EnosixSO_Detail.EnosixSO salesOrder = new SBO_EnosixSO_Detail.EnosixSO();
            
        
        CTRL_WarrantyClaimForm.myAddFrameworkMessages(salesOrder, new List<ensxsdk.EnosixFramework.Message>{
            new ensxsdk.EnosixFramework.Message(ensxsdk.EnosixFramework.MessageType.ERROR,'TEST1'),
            new ensxsdk.EnosixFramework.Message(ensxsdk.EnosixFramework.MessageType.ERROR,'TEST2')
        });
    }
}