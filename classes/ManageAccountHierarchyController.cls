public with sharing class ManageAccountHierarchyController {
	
	List<Account> bastardAccountList {get;set;}
	public String accountSearch{get;set;}
	
	public ManageAccountHierarchyController() {
		bastardAccountList = fetchAccounts(null);
	}
/*	
	@AuraEnabled
    public static List<Account> getBastardAccounts() {
    	System.debug( 'getBastardAccount' );
    	List<Account> lA = [SELECT Id, Name, ParentId
				FROM Account
				WHERE ParentId = null
				LIMIT 50];				// to make it snappy.
		System.debug( 'This many: ' + lA.size() );
		
		return lA;
	}
	*/
	@AuraEnabled
	public static List <Account> fetchAccounts(String searchKeyWord) {
		String searchKey = '%' + searchKeyWord + '%';
		String query = 'SELECT Id, Name, Phone, BillingStreet, BillingCity, BillingState FROM Account WHERE ParentId = null';
		if( null != searchKeyWord ) {
			query += ' AND Name LIKE \'' + searchKey + '\'';
		}
		query += ' LIMIT 500';
		System.debug( 'Query: ' + query );
		List <Account> accounts = (List<Account>)Database.query( query );
		return accounts;
	}
}