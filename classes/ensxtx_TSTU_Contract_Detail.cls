@isTest
public class ensxtx_TSTU_Contract_Detail
{
    @isTest
    public static void test_getCommand()
    {
        ensxtx_UTIL_Contract_Detail documentDetail = new ensxtx_UTIL_Contract_Detail();

        String simulateCommand = documentDetail.getSimulateCommand();
        String referenceCommand = documentDetail.getReferenceDocumentCommand();
    }

    @isTest
    public static void test_buildSBOReference()
    {
        ensxtx_UTIL_Contract_Detail documentDetail = new ensxtx_UTIL_Contract_Detail();
        ensxsdk.EnosixFramework.DetailObject docDetail = documentDetail.buildSBOForReference(null, null);
    }

    @isTest
    public static void test_convertToSBO()
    {
        ensxtx_DS_Document_Detail salesDocDetail = ensxtx_TSTD_Document_Detail.buildSalesDocDetail();
        ensxtx_UTIL_Contract_Detail documentDetail = new ensxtx_UTIL_Contract_Detail();
        ensxtx_DS_SalesDocAppSettings salesDocAppSettings = ensxtx_TSTD_SalesDocAppSettings.buildAppSettings();

        salesDocAppSettings.SBODetailType = 'Contract';

        ensxtx_SBO_EnosixContract_Detail.EnosixContract salesDocSBO = 
            (ensxtx_SBO_EnosixContract_Detail.EnosixContract) documentDetail.convertToSBO(null, salesDocDetail, false, salesDocAppSettings);

        for (ensxtx_SBO_EnosixContract_Detail.PARTNERS partner : salesDocSBO.PARTNERS.getAsList()) {
            if (String.isEmpty(partner.ItemNumber)) {
                partner.ItemNumber = '0';
            }
        }
        for (ensxtx_SBO_EnosixContract_Detail.CONTRACT contract : salesDocSBO.CONTRACT.getAsList()) {
            if (String.isEmpty(contract.ItemNumber)) {
                contract.ItemNumber = '0';
            }
        }
        salesDocSBO = (ensxtx_SBO_EnosixContract_Detail.EnosixContract) documentDetail.convertToSBO(salesDocSBO, salesDocDetail, true, salesDocAppSettings);
        salesDocSBO.CONTRACT.clear();
        salesDocSBO.PARTNERS.clear();
        salesDocSBO.TEXTS.clear();
        salesDocSBO = (ensxtx_SBO_EnosixContract_Detail.EnosixContract) documentDetail.convertToSBO(salesDocSBO, salesDocDetail, true, salesDocAppSettings);
        salesDocSBO = (ensxtx_SBO_EnosixContract_Detail.EnosixContract) documentDetail.updateTextFields(salesDocSBO, salesDocDetail);

        ensxtx_DS_Document_Detail clonedSalesDocHeader = salesDocDetail.cloneHeader();
        salesDocSBO = (ensxtx_SBO_EnosixContract_Detail.EnosixContract) documentDetail.removeAllConditions(salesDocSBO);
    }

    @isTest
    public static void test_convertToObject()
    {
        ensxtx_DS_Document_Detail salesDocDetail = ensxtx_TSTD_Document_Detail.buildSalesDocDetail();
        ensxtx_UTIL_Contract_Detail documentDetail = new ensxtx_UTIL_Contract_Detail();
        ensxtx_DS_SalesDocAppSettings salesDocAppSettings = ensxtx_TSTD_SalesDocAppSettings.buildAppSettings();

        salesDocAppSettings.SBODetailType = 'Contract';

        ensxtx_SBO_EnosixContract_Detail.EnosixContract salesDocSBO = 
            (ensxtx_SBO_EnosixContract_Detail.EnosixContract) documentDetail.convertToSBO(null, salesDocDetail, false, salesDocAppSettings);

        ensxtx_SBO_EnosixContract_Detail.INCOMPLETION_LOG newLog1 = new ensxtx_SBO_EnosixContract_Detail.INCOMPLETION_LOG();
        newLog1.Description = 'Configuration';
        newLog1.ItemNumber = '000010';
        salesDocSBO.INCOMPLETION_LOG.add(newLog1);
        ensxtx_SBO_EnosixContract_Detail.INCOMPLETION_LOG newLog2 = new ensxtx_SBO_EnosixContract_Detail.INCOMPLETION_LOG();
        newLog2.Description = 'Configuration';
        newLog2.ItemNumber = '000000';
        salesDocSBO.INCOMPLETION_LOG.add(newLog2);
        ensxtx_SBO_EnosixContract_Detail.BILLING_PLAN_DATES billPlanDate = new ensxtx_SBO_EnosixContract_Detail.BILLING_PLAN_DATES();
        billPlanDate.ItemNumber = '000010';
        billPlanDate.DeadlineSettlementDate = Date.today();
        billPlanDate.Usage = 'Usage';
        salesDocSBO.BILLING_PLAN_DATES.add(billPlanDate);
        for (ensxtx_SBO_EnosixContract_Detail.CONTRACT contract : salesDocSBO.CONTRACT.getAsList()) {
            if (String.isEmpty(contract.ItemNumber)) {
                contract.ItemNumber = '0';
            }
        }

        documentDetail.convertToObject(salesDocSBO, salesDocDetail, false, salesDocAppSettings);
        salesDocDetail = new ensxtx_DS_Document_Detail();
        documentDetail.convertToObject(salesDocSBO, salesDocDetail, true, salesDocAppSettings);
    }
}