@IsTest
public class TSTC_EnosixMachineDocLookup {
    @IsTest
    public static void testDoSearch() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixDocument_Search.class, new MockSBO_EnosixDocument_Search(true));
        
        SBQQ__Quote__c quote = createQuote();
        SBQQ__QuoteDocument__c qd = createQuoteDocument(quote.Id);
        CTRL_EnosixMachineDocLookup.doSearch(null, null);
        String newQuoteId = quote.Id;
        newQuoteId = newQuoteId.substring(0, newQuoteId.length()-1) + '0';
        CTRL_EnosixMachineDocLookup.doSearch(null, newQuoteId);
        CTRL_EnosixMachineDocLookup.doSearch(null, quote.Id);
    }
    @IsTest
    public static void testGetScreenData() {
        SBQQ__Quote__c quote = createQuote();
        SBQQ__QuoteDocument__c qd = createQuoteDocument(quote.Id);
        CTRL_EnosixMachineDocLookup.getScreenData(null);
        CTRL_EnosixMachineDocLookup.getScreenData(quote.Id);
        CTRL_EnosixMachineDocLookup.getRelatedEmailAddresses(quote.Id);
    }
    @IsTest
    public static void testGetToken() {
        CTRL_ENosixMachineDocLookup.getToken();
    }
    @IsTest
    public static void testDoDownload() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixDocument_Detail.class, new MockSBO_EnosixDocument_Detail(true, false));
        
        SBQQ__Quote__c quote = createQuote();
        
        SBQQ__QuoteDocument__c qd = createQuoteDocument(quote.Id);
        
        CTRL_EnosixMachineDocLookup.doDownload('12345', '12345', null, 'test');
        CTRL_EnosixMachineDocLookup.doDownload('12345', quote.Id, CTRL_EnosixMachineDocLookup.QUOTE_DOCUMENT_SOURCE, 'test');
        CTRL_EnosixMachineDocLookup.doDownload('12345', '12345', CTRL_EnosixMachineDocLookup.SAP_DOCUMENT_SOURCE, 'test');
        CTRL_EnosixMachineDocLookup.doDownload('12345', '12345', CTRL_EnosixMachineDocLookup.GDINSIDE_DOCUMENT_SOURCE, 'test');
        CTRL_EnosixMachineDocLookup.doDownload('12345', '12345', CTRL_EnosixMachineDocLookup.TCO_DOCUMENT_SOURCE, 'test');
        
        CTRL_EnosixMachineDocLookup.doDownloadQuoteDocument(qd.SBQQ__DocumentId__c, 'test.doc');
    }
    @IsTest
    public static void testSAPDoDownloadSuccessWithAttachment() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixDocument_Detail.class, new MockSBO_EnosixDocument_Detail(true, true));
        
        CTRL_EnosixMachineDocLookup.doDownload('12345', '12345', CTRL_EnosixMachineDocLookup.SAP_DOCUMENT_SOURCE, 'test');
    }
    @IsTest
    public static void testSAPDoDownloadFail() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixDocument_Detail.class, new MockSBO_EnosixDocument_Detail(false, true));
        
        CTRL_EnosixMachineDocLookup.doDownload('12345', '12345', CTRL_EnosixMachineDocLookup.SAP_DOCUMENT_SOURCE, 'test');
    }
    @IsTest
    public static void testDoSendEmail() {
        SBQQ__Quote__c quote = createQuote();
        
        SBQQ__QuoteDocument__c qd = createQuoteDocument(quote.Id);
        
        List<String> emailAddresses = new List<String>{'test@test.test'};
        List<String> selectedRows = new List<String>{'0,' + qd.Id + ',testg.doc', '2,abcdeh,testh.doc'};
            
        CTRL_EnosixMachineDocLookup.doSendEmail(quote.Id, emailAddresses, emailAddresses, false, 'test', 'test', selectedRows);
    }
    @IsTest
    public static void testGetFileSize() {
        
        CTRL_EnosixMachineDocLookup.getAttachmentSize(new List<String>{createAttachment().Id});
    }
    @IsTest
    public static void testMisc() {
        CTRL_EnosixMachineDocLookup.SEARCH_RESULTS results = new CTRL_EnosixMachineDocLookup.SEARCH_RESULTS();
        results.token = new CTRL_EnosixMachineDocLookup.TOKEN();
        results.results = new List<CTRL_EnosixMachineDocLookup.DOCUMENT_ROW>();
        
        CTRL_EnosixMachineDocLookup.DOCUMENT_ROW row = new CTRL_EnosixMachineDocLookup.DOCUMENT_ROW();
        results.results.add(row);
        
        row.Id = 'test';
        row.Type = 'test';
        row.FileName = 'test.doc';
        row.Title = 'test';
        row.Description = 'test';
        results.token.token_type = 'test';
        results.token.expires_in = 1;
        results.token.access_token = 'test';
        results.token.refresh_token = 'test';
        
        CTRL_EnosixMachineDocLookup.getGDInsideFilterCriteria('abcde');
    }
    
    public static SBQQ__Quote__c createQuote() {
		Account acct = new Account();
        acct.Name = 'Test Account';
        acct.Child_DCs__c = 'XX,YY,';
        acct.Account_Number__c='1234';
        acct.DC__c = 'CP - XXXXXXX';

        insert acct;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware', ProductCode = 'ABCDE');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT> selectedRows = new List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT>();
        
        SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT sr = new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();
        
        selectedRows.add(sr);
        
        sr.Material = 'ABCDE';
        sr.MaterialDescription = 'Test';
        sr.MaterialFamily = 'Test';
        sr.Stock = 1;
        sr.Scheduled = 1;
        sr.UOM = 'EA';
        sr.SalesOrganization = 'GDMI';
        sr.DistributionChannel = 'CP';
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'email@gmail.com',
            Phone = '9743800309'); 
        insert c; 
        
        UTIL_Aura.Response response = CTRL_EnosixInventorySearch.doCreateQuote(acct.Id, 'CP', selectedRows);
        String quoteUrl = (String)response.data;
        String quoteId = quoteUrl.substring(quoteUrl.length()-18);
        
        SBQQ__Quote__c quote = [SELECT Id, Comp_PDF_UID__c, SBQQ__PrimaryContact__c from SBQQ__Quote__c where Id = : quoteId LIMIT 1];
        
        quote.Comp_PDF_UID__c = '12345';
        quote.SBQQ__PrimaryContact__c = c.Id;
        
        update quote;
        
        return quote;
	}
    
    public static SBQQ__QuoteDocument__c createQuoteDocument(String quoteId) {
        Attachment att = createAttachment();
        
        Document document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'my_document';
        document.IsPublic = true;
        document.Name = 'My Document';
        //document.FolderId = [select id from folder where name = 'Company Dashboards' LIMIT 1].id;
		document.AuthorId = UserInfo.getUserId();
		document.FolderId = UserInfo.getUserId();
        
        insert document;        
        
        SBQQ__QuoteDocument__c retVal = new SBQQ__QuoteDocument__c();
        retVal.SBQQ__Quote__c = quoteId;
        retVal.SBQQ__AttachmentId__c = att.Id;
        retVal.SBQQ__DocumentId__c = document.Id;
        retVal.SBQQ__OutputFormat__c = 'MS Word';
        
        insert retVal;
        
        return retVal;
    }
    
    public static Attachment createAttachment() {
        Case cse = new Case();
        cse.Subject = 'test';

        insert cse;
        
        Attachment attach = new Attachment();   	
        
    	attach.Name = 'test';
    	Blob bodyBlob = Blob.valueOf('test');
    	attach.body = bodyBlob;
        attach.parentId = cse.id;
        
        insert attach;

        return attach;
    }
    
    public static String getGDInsideResponse() {
        CTRL_EnosixMachineDocLookup.GDINSIDE_ASSET_RESPONSE response = new CTRL_EnosixMachineDocLookup.GDINSIDE_ASSET_RESPONSE();
        
        response.current_page = 1;
        response.last_page = 1;
        response.next_page_url = 'http://test.test.test/test.test';
        response.path = 'abcde';
        response.per_page = 10;
        response.prev_page_url = 'http://test.test.test/test.test';
        response.total = 1;
        
        CTRL_EnosixMachineDocLookup.GDINSIDE_ASSET_ROW row = new CTRL_EnosixMachineDocLookup.GDINSIDE_ASSET_ROW();
        
        response.data = new List<CTRL_EnosixMachineDocLookup.GDINSIDE_ASSET_ROW>();
        response.data.add(row);
        
		row.file_size = 123456;
		row.asset_file = 'test.doc';
        row.alert_restock_qty = 1;
		row.archive = 1;
		row.author = 'test';
		row.available_for_sales_force = 1;
		row.contact_map = 1;
		row.created_at = 'test';
		row.custom_download_link = 'test';
		row.date_published = System.today();
		row.description = 'test';
		row.document_number = 'test';
		row.download_api_link = 'test';
		row.public_download_url = 'test';
		row.expiration_date = System.today();
		row.fast_facts = 1;
		row.featured = 1;
		row.featured_expiration = 'test';
		row.hardcopy_orderable = 1;
		row.id = 1;
		row.keywords = 'test';
		row.last_modfified_by = 1;
		row.live = 1;
		row.live_date = System.today();
		row.max_order_qty = 1;
		row.memo = 1;
		row.minimum_quantity = 1;
		row.non_emailable = 1;
		row.non_printable = 1;
		row.not_downloadable = 1;
		row.notes = 'test';
		row.pending_approval = 1;
		row.qty_available = 1;
		row.qty_on_hand = 1;
		row.robuschi = 1;
		row.salesForceAttributes = new CTRL_EnosixMachineDocLookup.SALESFORCE_ATTRIBUTES();
        row.salesForceAttributes.Horsepower_range = new List<String>();
        row.salesForceAttributes.pressure = 'test';
        row.salesForceAttributes.MG4 = new List<String>();
        row.salesForceAttributes.product_category = new List<String>();
		row.thumbnail = 'test';
		row.thumbnail_download_api_link = 'test';
		row.title = 'test';
		row.updated_at = 'test';
		row.updated_thumbnail = 1;
		row.video_embed = 'test';
		row.vimeo_id = 'test';

        
        return JSON.serialize(response);
    }
    
    public class ThisException extends Exception {}

    public class MockSBO_EnosixDocument_Search implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        Boolean isSuccess;

        public MockSBO_EnosixDocument_Search(Boolean success) {
            isSuccess = success;
        }

		public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
		{
            if (!isSuccess) {
                throw new ThisException();
            }

            SBO_EnosixDocument_Search.EnosixDocument_SC searchContext = (SBO_EnosixDocument_Search.EnosixDocument_SC)sc;
            SBO_EnosixDocument_Search.EnosixDocument_SR searchResult = new SBO_EnosixDocument_Search.EnosixDocument_SR();

            if (isSuccess)
            {
                SBO_EnosixDocument_Search.SEARCHRESULT result = new SBO_EnosixDocument_Search.SEARCHRESULT();

                result.StartDate = System.today().addYears(-2);
                result.EndDate = System.today().addYears(2);
                result.EnosixObjKey = 'abc123';
                result.DocumentNumber = '1';
                result.DocumentVersion = '1';
                result.GdDocumentType = 'X';
                result.DocumentType = 'X';
                result.DocumentPart = 'X';
                result.DocumentDescription = 'X';

                searchResult.SearchResults.add(result);
            }

            searchResult.setSuccess(isSuccess);
            searchContext.baseResult = searchResult;

			return searchContext;
        }
    }

    public class MockSBO_EnosixDocument_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock
    {
        Boolean isSuccess;
        Boolean populateAttachment;

        public MockSBO_EnosixDocument_Detail(Boolean success, Boolean attachment) {
            isSuccess = success;
            populateAttachment = attachment;
        }

		public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object obj)
		{
            if (!isSuccess) {
                throw new ThisException();
            }

			SBO_EnosixDocument_Detail.EnosixDocument result = new SBO_EnosixDocument_Detail.EnosixDocument();

			result.setSuccess(isSuccess);
            
            if (populateAttachment) {
                SBO_EnosixDocument_Detail.ATTACHMENTS att = new SBO_EnosixDocument_Detail.ATTACHMENTS();
                result.ATTACHMENTS.add(att);
                att.FileIndex = 1;
                att.FileType = 'X';
                att.FileTypeDescription = 'X';
                att.FileName = 'X';
                att.HTMLContentType = 'X';
                att.GetFile = 'X';
                att.FileB64String = 'X';
            }

			return result;
        }
    }
}