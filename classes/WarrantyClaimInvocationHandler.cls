global without sharing class WarrantyClaimInvocationHandler {
    @InvocableMethod(label='Check Serial Number and Account Sharing for User' description='')
    public static void updateSharing(List<FlowInput> input) {
        if (input == null || input.size() != 1) {
            return;
        }
        
        if (input[0].serialNumberId != null) {
            processSerialNumber(UserInfo.getUserId(), input[0].serialNumberId);
        }
        
        if (input[0].accountId != null) {
            processAccount(UserInfo.getUserId(), input[0].accountId);
        }
    }
    
    private static void processSerialNumber(String userId, String recordId) {
        if (!hasAccess(userId, recordId)) {
            try {
                AssetShare share = new AssetShare();
                share.assetAccessLevel='Edit';
                share.AssetId=recordId;
                share.RowCause='Manual';
                share.UserOrGroupId=userId;
                
                insert share;
            } catch (Exception e) {}
        }
        
        Asset asset = [Select Id, AccountId, End_User_Account__c from Asset where Id = : recordId LIMIT 1];
        
        if (asset.AccountId != null) {
            processAccount(userId, asset.AccountId);
        }
        
        if (asset.End_User_Account__c != null) {
            processAccount(userId, asset.End_User_Account__c);
        }
    }
    
    private static void processAccount(String userId, String recordId) {
        if (!hasAccess(userId, recordId)) {
            try {
                insert AccountSharingHelper.createAccountShareForUser(recordId, userId);
            } catch (Exception e) {}
        }
    }
    
    private static Boolean hasAccess(String userId, String recordId) {
        List<UserRecordAccess> recordAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = : userId AND RecordId = : recordId];
        
        if (recordAccess != null && recordAccess.size() == 1) {
            return recordAccess[0].HasEditAccess;
        }
        
        return false;
    }
    
    public class FlowInput {
        @InvocableVariable
        public String serialNumberId;
        
        @InvocableVariable
        public String accountId;
    }
    
}