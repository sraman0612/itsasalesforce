global class C_Scheduled_Warranty_Checker implements Schedulable {
    List<Asset> updateAss = new List<asset>();
    global void execute(SchedulableContext SC) {
        for(Asset a : [SELECT Id, Air_Filter_Next_Expected_Date__c, Oil_Filter_Next_Expected_Date__c, 
                       Lubricant_Next_Expected_Date__c, Cabinet_Filter_Next_Expected_Date__c, Separator_Next_Expected_Date__c, Control_Box_Filter_Next_Expected_Date__c FROM Asset]) {
            checkWarranty(a);
        }
        update updateAss;
    }
    
    public void checkWarranty(Asset a) {
        // Check serial numbers to see which dates are PAST due for compliance
        
        Date td = Date.today();
        System.debug('scheduled asset: ' + a);
        if (td.daysBetween(a.Air_Filter_Next_Expected_Date__c) > -119) {
            a.Compliant_Air_Filter__c = false;
        }
        
        if (td.daysBetween(a.Oil_Filter_Next_Expected_Date__c) > -119) {
            a.Compliant_Oil_Filter__c = false;
        }
        
        if (td.daysBetween(a.Cabinet_Filter_Next_Expected_Date__c) > -119) {
            a.Compliant_Cabinet_Filter__c = false;
        }
        
        if (td.daysBetween(a.Control_Box_Filter_Next_Expected_Date__c) > -119) {
            a.Compliant_Control_Box_Filter__c = false;
        }
        
        if (td.daysBetween(a.Separator_Next_Expected_Date__c) > -119) {
            a.Compliant_Separator__c = false;
        }
        
        if (td.daysBetween(a.Lubricant_Next_Expected_Date__c) > -119) {
            a.Compliant_Lubricant__c = false;
        }
        
        updateAss.add(a);
    }
}