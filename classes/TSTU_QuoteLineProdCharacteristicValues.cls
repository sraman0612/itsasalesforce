@IsTest
private class TSTU_QuoteLineProdCharacteristicValues {

    @testSetup
    static void setup() {
        HP_KW_Mapping_Table__c hpkwt = new HP_KW_Mapping_Table__c();
        hpkwt.HP__c = '100';
        hpkwt.Max_Flow__c = 560.0;
        hpkwt.Min_Flow__c = 320.0;
        hpkwt.HP_KW__c = '100HP (75kW)';
        insert hpkwt;

        Account acct = TSTU_SFAccount.createTestAccount();
        acct.Name = 'Test Acct 1';
        acct.BillingStreet = '6 NORFOLK AVENUE';
        acct.BillingCity = 'SOUTH EASTON';
        acct.BillingState = 'MA';
        acct.BillingPostalCode = '02375';
        acct.BillingCountry = 'US';
        acct.ShippingStreet = '6 NORFOLK AVENUE';
        acct.ShippingCity = 'SOUTH EASTON';
        acct.ShippingState = 'MA';
        acct.ShippingPostalCode = '02375';
        acct.ShippingCountry = 'US';
        acct.put(UTIL_SFAccount.CustomerFieldName,'CustNum');
    	insert acct;

        Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'Test Opp';
        opp.StageName ='IsWon';
        opp.CloseDate = system.today();
        opp.AccountId = acct.Id;
        opp.ENSX_EDM__Quote_Number__c = '12345';
        opp.Pricebook2Id = pricebookId;
        TSTU_SFOpportunity.upsertOpportunity(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];

        Product2 prod1 = new Product2();
        prod1.Name = 'test1';
        prod1.put(UTIL_SFProduct.MaterialFieldName, 'test1');
        prod1.MG1__c = 'M';
        upsert prod1;

        Product2 prod2 = new Product2();
        prod2.Name = 'test2';
        prod2.put(UTIL_SFProduct.MaterialFieldName, 'test2');
        prod2.MG1__c = 'M';
        prod2.Description = 'RPC DRYER, 75-550 SCFM';
        upsert prod2;

        PriceBookEntry pbe1 = new PriceBookEntry();
        pbe1.UnitPrice = 100;
		pbe1.Pricebook2Id = UTIL_Pricebook.getStandardPriceBookId();
		pbe1.Product2Id = prod1.Id;
		pbe1.UseStandardPrice = false;
		pbe1.IsActive = true;
		insert pbe1;

        PriceBookEntry pbe2 = new PriceBookEntry();
        pbe2.UnitPrice = 200;
		pbe2.Pricebook2Id = UTIL_Pricebook.getStandardPriceBookId();
		pbe2.Product2Id = prod2.Id;
		pbe2.UseStandardPrice = false;
		pbe2.IsActive = true;
		insert pbe2;

        SBQQ__Quote__c quote1 = TSTU_SFCPQQuote.createTestQuote();
        quote1.SBQQ__Account__c = acct.Id;
        quote1.SBQQ__Opportunity2__c = opp.Id;
        quote1.SBQQ__PriceBook__c = pricebookId;
        quote1.SAP_Configuration__c =
            '{"soldToParty":"soldToParty",' +
            '"salesOrg":"salesOrg",' +
            '"distChannel":"distChannel",' +
            '"division":"division"}';
        upsert quote1;
        quote1 = [SELECT Id, SBQQ__Opportunity2__c, SBQQ__Account__c, FLD_SAP_Quote_Number__c, SAP_Configuration__c FROM SBQQ__Quote__c WHERE Id = :quote1.Id];
        quote1.SBQQ__PriceBook__c = pricebookId;
        upsert quote1;

        String sapConfigJSON = '{"plant":"","OrderQuantity":1,"selectedCharacteristics":['+
            				'{"CharacteristicID":"0000016191","CharacteristicName":"SHP1","CharacteristicDescription":"Horsepower","CharacteristicValue":"100","CharacteristicValueDescription":"100 HORSEPOWER","UserModified":true},'+
            				'{"CharacteristicID":"0000016683","CharacteristicName":"SPRESSURE1","CharacteristicDescription":"Pressure","CharacteristicValue":"125","CharacteristicValueDescription":"125 PSIG","UserModified":true},'+
            				'{"CharacteristicID":"0000016202","CharacteristicName":"SVOLT1","CharacteristicDescription":"VOLT","CharacteristicValue":"380-3-50","CharacteristicValueDescription":"380 VOLT, 3PH, 50HZ","UserModified":true},'+
            				'{"CharacteristicID":"0000016190","CharacteristicName":"SMOTSTARTVOLT1","CharacteristicDescription":"MOTOR START","CharacteristicValue":"WYE","CharacteristicValueDescription":"WYE-DELTA REDUCED VOLT START","UserModified":false},'+
            				'{"CharacteristicID":"0000000853","CharacteristicName":"SCONTROLBOX","CharacteristicDescription":"CONTROL BOX","CharacteristicValue":"N4_WITH","CharacteristicValueDescription":"UL4 WITH CURRENT MONITOR","UserModified":false},'+
            				'{"CharacteristicID":"0000018171","CharacteristicName":"SSTARTER1","CharacteristicDescription":"STARTER SUPPLIED","CharacteristicValue":"YES","CharacteristicValueDescription":"WITH STARTER","UserModified":true},'+
            				'{"CharacteristicID":"0000000873","CharacteristicName":"SCONTROL","CharacteristicDescription":"CONTROL","CharacteristicValue":"ASC-WITH","CharacteristicValueDescription":"AIR SMART CONTROL WITH SEQ","UserModified":true},'+
            				'{"CharacteristicID":"0000016854","CharacteristicName":"SMOTOR1","CharacteristicDescription":"DRIVE MOTOR","CharacteristicValue":"ODP","CharacteristicValueDescription":"ODP MOTOR (50HZ ONLY)","UserModified":true},'+
            				'{"CharacteristicID":"0000016637","CharacteristicName":"SCOOLING1","CharacteristicDescription":"COOLING TYPE","CharacteristicValue":"AIRSTD","CharacteristicValueDescription":"AIR CLD WITH AFT/CLR","UserModified":true},'+
            				'{"CharacteristicID":"0000000869","CharacteristicName":"SENCLOSURE","CharacteristicDescription":"ENCLOSURE","CharacteristicValue":"NONE","CharacteristicValueDescription":"NO ENCLOSURE","UserModified":true},'+
            				'{"CharacteristicID":"0000018170","CharacteristicName":"SLUBRICANT1","CharacteristicDescription":"LUBRICANT","CharacteristicValue":"2000","CharacteristicValueDescription":"AEON 2000","UserModified":true},'+
            				'{"CharacteristicID":"0000007889","CharacteristicName":"SLANGUAGE","CharacteristicDescription":"LANGUAGE","CharacteristicValue":"ENGLISH","CharacteristicValueDescription":"AMERICAN ENGLISH (STD)","UserModified":false},'+
            				'{"CharacteristicID":"0000024027","CharacteristicName":"SMOISTSEP","CharacteristicDescription":"Moisture Separator","CharacteristicValue":"N","CharacteristicValueDescription":"No","UserModified":true},'+
            				'{"CharacteristicID":"0000000882","CharacteristicName":"SOUTLINE","CharacteristicDescription":"OUTLINE","CharacteristicValue":"303EBQ804","CharacteristicValueDescription":"303EBQ804","UserModified":false},'+
            				'{"CharacteristicID":"0000000872","CharacteristicName":"SWIRING","CharacteristicDescription":"WIRING DIAGRAM","CharacteristicValue":"328EAQ546","CharacteristicValueDescription":"328EAQ546","UserModified":false},'+
            				'{"CharacteristicID":"0000002934","CharacteristicName":"SSHIPPACKET","CharacteristicDescription":"SHIPPING PACKET","CharacteristicValue":"314EAQ6003","CharacteristicValueDescription":"314EAQ6003","UserModified":false},'+
                            '{"CharacteristicID":"0000002934","CharacteristicName":"SMOUNTEDDRYER","CharacteristicDescription":"Mounted Dryer","CharacteristicValue":"333EAQ4444","CharacteristicValueDescription":"Water Cooled Mounted Dryer","UserModified":false},'+
            				'{"CharacteristicID":"0000004348","CharacteristicName":"SCOLMODOUTLINE","CharacteristicDescription":"COOLING MODULE OUTLINE","CharacteristicValue":"NONE","CharacteristicValueDescription":"NONE","UserModified":false}]}';

        SBQQ__QuoteLine__c qln1 = new SBQQ__QuoteLine__c();
        qln1.SBQQ__Quote__c = quote1.Id;
        qln1.SBQQ__PricebookEntryId__c = pbe1.id;
        qln1.SBQQ__Product__c = prod1.Id;
        qln1.SAP_Configuration__c = sapConfigJSON;
        insert qln1;
    }

    static testmethod void testUpdateBehavior() {

        UTIL_QuoteLineProdCharacteristicValues.SAP_Product_Configuration prodClass = new UTIL_QuoteLineProdCharacteristicValues.SAP_Product_Configuration();
        prodClass.plant = 'Test Plant';
        prodClass.OrderQuantity = 2;

        UTIL_QuoteLineProdCharacteristicValues.Characteristic_Values cvClass = new UTIL_QuoteLineProdCharacteristicValues.Characteristic_Values();
        cvClass.CharacteristicID = '12345';
        cvClass.CharacteristicName = 'Name';
        cvClass.CharacteristicValue = 'Value';
        cvClass.UserModified = true;
    }
}