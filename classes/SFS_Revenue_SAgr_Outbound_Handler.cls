/*=========================================================================================================
* @author : Srikanth P, Capgemini
* @date : 20/4/2022
* @description: Billing Events (Service Agreement Revenue) integration
 Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Ryan Reddish	M-001		23/09/2022		Added logic to populate LineItemNumber at AgreementItemId tag. Code is not working. Additional logic added to Charge Query, line 99->101,line 116, line 134->136	
============================================================================================================================================================*/
public class SFS_Revenue_SAgr_Outbound_Handler {
    @invocableMethod(label = 'Outbound Invoice to Oracle' description = 'Send to CPI' Category = 'Invoice')
    public static void Run(List<Id> invoiceId){
        //Main Method
        String xmlString = '';
        //Map<Double, String> invoiceMap = new Map<Double, String>();
        ProcessData(invoiceId,xmlString);
        system.debug('@@invoiceId'+invoiceId);
    }
    public static void ProcessData (List<Id>invoiceId, String xmlString ){
       List<Invoice__c> invList = new  List<Invoice__c>();  
       SFS_Service_Agr_Revenue_XML_Structure__mdt[] revenueMetaMap = [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c,SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Charge_Type__c,SFS_Hardcoded_Flag__c 
                                                                      FROM SFS_Service_Agr_Revenue_XML_Structure__mdt Order By SFS_Node_Order__c asc]; 
              
        invList = [SELECT Id,Name,RecordTypeId,RecordType.DeveloperName,SFS_Related_Invoice__c, SFS_Related_Invoice__r.SFS_Service_Agreement_Line_Item__c,SFS_External_Id__c,CreatedBy.Name,CurrencyIsoCode,SFS_Invoice_Amount__c,SFS_Oracle_Invoice_Type__c,SFS_Canceled__c,SFS_Service_Agreement__c,
                         SFS_Service_Agreement__r.ContractNumber,SFS_Service_Agreement__r.SFS_External_Id__c,SFS_Service_Agreement__r.SFS_Type__c,
                         SFS_Service_Agreement_1__c,SFS_Service_Agreement_1__r.ContractNumber,SFS_Service_Agreement_1__r.SFS_External_Id__c,SfsAGRorSiebelAGR__c, SFS_Service_Agreement_1__r.SFS_Type__c,
                         SFS_Invoice_Format__c,SFS_Credit_Memo_Reason__c,Invoice_Date__c,SFS_Approval_Flag__c,SFS_Invoice_Description__c,SFS_Invoice_Print_Code__c,
                         SFS_Invoice_Status__c,SFS_No_Billing_Calculator__c,SFS_Billing_Period_Start_Date__c,SFS_Billing_Period_End_Date__c,SFS_Invoice_Type__c,SFS_Authorization_Number__c,
                         SFS_Expiration_Month__c,SFS_Expiration_Year__c,SFS_Selected_Credit_Card__c,SFS_SubscriptionID__c,SFS_Request_Token__c,SFS_Oracle_CreditCard_Type__c,SFS_Authorization_Date__c
                         FROM Invoice__c WHERE Id IN:InvoiceId]; //ApprovalFlag,TransactionId,
        

        List<CAP_IR_Charge__c> chargeList = [SELECT Name,CurrencyIsoCode,SFS_Sell_Price__c,CAP_IR_Description__c,SFS_Integration_Status__c,SFS_PO_Number__c,CAP_IR_Type__c,SFS_Expense_Type__c,SFS_Line_Number__c,SFS_Oracle_Charge_Type__c,
                                            CAP_IR_Service_Contract__c,CAP_IR_Service_Contract__r.ContractNumber,SFS_Service_Agreement__c,SFS_Service_Agreement__r.ContractNumber,CAP_IR_Amount__c,CTS_Quanity__c,SFS_Invoice__c,SFS_Invoice__r.Name,SFS_Invoice__r.Invoice_Date__c,
                                            SFS_Expense__c,SFS_Expense__r.ExpenseNumber,CAP_IR_Contract_Line_Item__r.LineItemNumber ,CAP_IR_Labor__c,CAP_IR_Labor__r.Name,SFS_Charge_Type__c,SFS_Product__c,SFS_Product__r.ProductCode,
                                            SFS_Related_Charge__r.SFS_Charge_Type__c,CAP_IR_Product_Consumed__r.ProductConsumedNumber,CAP_IR_Agreement_Line_New__c,CAP_IR_Contract_Line_Item__r.SFS_External_Id__c,
                                            SFS_Related_Charge__c   
                                            FROM CAP_IR_Charge__c WHERE SFS_Invoice__c IN:invoiceId AND (SFS_Service_Agreement__c!=null OR CAP_IR_Service_Contract__c!=null)  Order by SFS_Invoice__c ]; 
        //ContractLineItem relatedCLIId = new ContractLineItem();
        Id serviceInvId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('SFS_CARE_Invoice').getRecordTypeId();
        //Map Queries, then populate using getPopulatedFieldAsMap()
        MAP<String,Object> finalInvoiceFieldsMap = new MAP<String,Object>();
        Map<Id, List<CAP_IR_Charge__c>> chargeToInvoiceMap = new Map<Id,List<CAP_IR_Charge__c>>();
        Id invCheck;
        List<CAP_IR_Charge__c> chrgList =new list<CAP_IR_Charge__c>();
        for(CAP_IR_Charge__c chrg:chargeList){
            if(invCheck!=chrg.SFS_Invoice__c && chrgList.size()>0 ){
                chargeToInvoiceMap.put(invCheck,chrgList);
                chrgList.clear();
            }
            invCheck=chrg.SFS_Invoice__c;
            chrgList.add(chrg);
            
        }
        if(chrgList.size()>0)
                chargeToInvoiceMap.put(invCheck,chrgList);
        
        Map<String, Object> invMap = new Map<String, Object>();
        for(Invoice__c inv : invList){
        invMap = inv.getPopulatedFieldsAsMap();
        System.debug('@@invMap'+invMap);
        
        
        Map<String,Object> scFields = new Map<String,Object>();
        if(inv.SFS_Service_Agreement__c!=Null){
            ServiceContract sc = (ServiceContract)invMap.get('SFS_Service_Agreement__r');
            scFields = sc.getPopulatedFieldsAsMap(); 
        }
        else if (inv.SFS_Service_Agreement_1__c!=null){
            ServiceContract scService = (ServiceContract)invMap.get('SFS_Service_Agreement_1__r');
            scFields = scService.getPopulatedFieldsAsMap();
        }
        
        Map<String,Object> wUserFields = new Map<String,Object>();
        try{
        User wuser = (User)invMap.get('CreatedBy');
        wUserFields = wuser.getPopulatedFieldsAsMap();
        }catch(exception e){}
        
        
        SFS_Revenue_SAgr_Outbound_Handler revenue = new SFS_Revenue_SAgr_Outbound_Handler();
        List<String> reXMLList = new List<String>(); 
        String chargeType = '';
        for(String field : invMap.keyset()){
            if(!field.contains('SFS_Service_Agreement__r') && invMap.get(field)!=null && !field.contains('SFS_Service_Agreement_1__r')) {
               String iritNoBilling =  inv.RecordTypeId==serviceInvId && field =='SFS_No_Billing_Calculator__c'? 'Y':'N';
               if(field =='SFS_No_Billing_Calculator__c'){
                    finalInvoiceFieldsMap.put(field,iritNoBilling); 
                  }
                  else { 
                         finalInvoiceFieldsMap.put(field,invMap.get(field));
                      }
                
               }            
          } 
        
        
        for(String field : scFields.keyset()){
                 string fieldstring = 'SFS_Service_Agreement__r'+'-'+field;
                 finalInvoiceFieldsMap.put(fieldstring, scFields.get(field));
        } 
        
        for(String field : wUserFields.keyset()){
            string fieldstring = 'CreatedBy'+'-'+field;
            finalInvoiceFieldsMap.put(fieldstring, wUserFields.get(field));
        }
      
        system.debug('****finalInvoiceFieldsMap'+finalInvoiceFieldsMap);
       Map<Integer,Map<String,Object>> lineItemsMap = new Map<Integer,Map<String,Object>>();
           System.debug('@@inv.id: '+inv.id);
            System.debug('@@chargeToInvoiceMap: '+chargeToInvoiceMap);
            System.debug('@@chargeToInvoiceMap.get(inv.id): '+chargeToInvoiceMap.get(inv.id));
            if(chargeToInvoiceMap.get(inv.id)!=null){
        for(CAP_IR_Charge__c c:chargeToInvoiceMap.get(inv.id)){
            Map<String,Object> chargFields = new Map <String, Object>();
            chargFields = c.getPopulatedFieldsAsMap();
            
            Map<String,Object> cInvoiceFields = new Map<String,Object>();
            Invoice__c cinv = (Invoice__c)chargFields.get('SFS_Invoice__r');
            cInvoiceFields = cinv.getPopulatedFieldsAsMap();
            System.debug('cInvoiceFields: '+CInvoiceFields);
             System.debug('@@c: '+c.id+ ':'+c.CAP_IR_Service_Contract__c);
            Map<String,Object> cAgreeFields = new Map<String,Object>();
            String serviceAgreememt = c.CAP_IR_Service_Contract__c!=null? 'CAP_IR_Service_Contract__r':'SFS_Service_Agreement__r';
            ServiceContract cAgree = (ServiceContract)chargFields.get(serviceAgreememt);
            cAgreeFields = cAgree.getPopulatedFieldsAsMap();
            
            Map<String,Object> cChargeFields = new Map<String,Object>();
            if(c.CAP_IR_Contract_Line_Item__c!=null){
                 ContractLineItem cli = (ContractLineItem)chargFields.get('CAP_IR_Contract_Line_Item__r');
                 cChargeFields = cli.getPopulatedFieldsAsMap();
                
            }
                        
            Map<String,Object> cExpenseFields = new Map<String,Object>();
            if(c.SFS_Expense__c!=null){
                Expense cEx = (Expense)chargFields.get('SFS_Expense__r');
                cExpenseFields = cEx.getPopulatedFieldsAsMap();
              }
            
            Map<String,Object> cprodFields = new Map<String,Object>();
            
             
            Map<String,Object> finalChargeFields = new Map<String,Object>();
            for(String field : chargFields.keyset()){
                if(!field.contains('SFS_Invoice__r') && !field.contains('CAP_IR_Service_Contract__r') && !field.contains('SFS_Service_Agreement__r') &&!field.contains('SFS_Expense__r') && !field.contains('CAP_IR_Labor__r') && !field.contains('CAP_IR_Product_Consumed__r') && !field.contains('CAP_IR_Contract_Line_Item__r') && field!='SFS_Oracle_Charge_Type__c'){
                 //  if(field =='ActiveId' && (c.CAP_IR_Type__c=='Time' || c.CAP_IR_Type__c=='Expense' || chargFields.get(field)=='Part')) {
                       finalChargeFields.put(field,chargFields.get(field));                                        
                }
                
                if(field=='SFS_Oracle_Charge_Type__c' && serviceAgreememt=='CAP_IR_Service_Contract__r' && Inv.SFS_Service_Agreement__r.SFS_Type__c!='Rental' && Inv.SFS_Service_Agreement__r.SFS_Type__c!='PackageCARE'){
                    
                    finalChargeFields.put(field,'Milestone'); 
                }
                else{
                    finalChargeFields.put(field,chargFields.get(field));
                }
            }
            for(String field: invMap.keyset()){
                if(field.contains('SFS_Oracle_Invoice_Date__c')){
                    finalChargeFields.put('SFS_Oracle_Invoice_Date__c',invMap.get(field));
                }
            }
            for(String field : cInvoiceFields.keyset()){
                 string fieldstring = 'SFS_Invoice__r'+'-'+field;
                 finalChargeFields.put(fieldstring, cInvoiceFields.get(field));
                System.debug('KEY: ' + fieldString + ', Value: ' + cInvoiceFields.get(field));
              }
            for(String field : cAgreeFields.keyset()){
                 string fieldstring = 'CAP_IR_Service_Contract__r'+'-'+field;
                 finalChargeFields.put(fieldstring, cAgreeFields.get(field));
              }
             if(c.CAP_IR_Contract_Line_Item__c!=null){
                  for(String field : cChargeFields.keyset()){
                      String fieldString = 'ContractLineItem-' + field;
                       finalChargeFields.put(fieldString,cChargeFields.get(field));                      
                       if(c.CAP_IR_Type__c=='Time' || c.CAP_IR_Type__c=='Expense' ||  c.CAP_IR_Type__c =='Part Movement') {
                          finalChargeFields.put('ActiveId',cChargeFields.get(field)); 
                       }  
                      else{
                           finalChargeFields.put('ActiveId',''); 
                          }                       
               }
            }
            if(c.SFS_Expense__c!=null){
                 for(String field : cExpenseFields.keyset()){
                       string fieldstring = 'SFS_Expense__r'+'-'+field;
                       finalChargeFields.put(fieldstring, cExpenseFields.get(field));
              }
            }    
            if(c.SFS_Product__c!=null){
                product2 cprod = (product2)chargFields.get('SFS_Product__r');
                cprodFields = cprod.getPopulatedFieldsAsMap();
                for(String field : cprodFields.keyset()){
                    string fieldstring = 'SFS_Product__r'+'-'+field;
                    finalChargeFields.put(fieldstring, cprodFields.get(field));
                 }
              }
            lineItemsMap.put(chargeList.indexOf(c),finalChargeFields);        
        }
     }
   
        xmlString='';
        // Revenue XML build logic
        reXMLList = revenue.buildXmlStructure(revenueMetaMap,finalInvoiceFieldsMap,'Invoice','Header');
        List<String> chargeXMLList = new List<String>();
        //Charges XML build logic
           for(Integer i=0;i<lineItemsMap.Size();i++){
                  Map<String,Object> lineItemsFieldsMap = (Map<String, Object>)lineItemsMap.get(i); 
                  system.debug('@chargeXMLList'+chargeXMLList);
               if(lineItemsFieldsMap!=null){
                  chargeXMLList = revenue.buildXmlStructure(revenueMetaMap,lineItemsFieldsMap,'Charge','Charge'); 
               }
                  system.debug('@reXMLList'+reXMLList);
                  reXMLList.addAll(chargeXMLList);
          }  
        
        SFS_Service_Agr_Revenue_XML_Structure__mdt re = SFS_Service_Agr_Revenue_XML_Structure__mdt.getInstance('SFS_End_Tag');
        reXMLList.add(re.SFS_XML_Full_Name__c);
            
        System.debug('Final reXMLList:  ' + reXMLList);    
        for(String s : reXMLList){
            xmlString = xmlString + s;
            System.debug('XML:  ' + s);
        }
        System.debug('***XMLString**** ' + xmlString);
        
       
        
       // xmlString = '<EBS_PROJECT_BILLING_REQUEST><CONTROL_AREA><PARTNER_CODE>SFS-NA</PARTNER_CODE><EXTERNAL_MESSAGE_ID>234567</EXTERNAL_MESSAGE_ID></CONTROL_AREA><DATA_AREA><G_PA_BILLING_EVENT><PA_BILLING_EVENT><P_PM_EVENT_REFERENCE>1-15022022222</P_PM_EVENT_REFERENCE><P_TASK_NUMBER>AGL-00000002</P_TASK_NUMBER><P_EVENT_NUMBER></P_EVENT_NUMBER><P_EVENT_TYPE>Labor</P_EVENT_TYPE><P_AGREEMENT_NUMBER>AGR-33000254</P_AGREEMENT_NUMBER><P_AGREEMENT_TYPE></P_AGREEMENT_TYPE><P_CUSTOMER_NUMBER></P_CUSTOMER_NUMBER><P_DESCRIPTION>AgreementLineCharges</P_DESCRIPTION><P_BILL_HOLD_FLAG>N</P_BILL_HOLD_FLAG><P_INVOICE_DATE>01-FEB-2022</P_INVOICE_DATE><P_INVOICE_START>01-FEB-2022</P_INVOICE_START><P_INVOICE_END></P_INVOICE_END><P_COMPLETION_DATE>01-FEB-2022</P_COMPLETION_DATE><P_DESC_FLEX_NAME></P_DESC_FLEX_NAME><P_ATTRIBUTE_CATEGORY></P_ATTRIBUTE_CATEGORY><P_ATTRIBUTE1></P_ATTRIBUTE1><P_ATTRIBUTE2></P_ATTRIBUTE2><P_ATTRIBUTE3></P_ATTRIBUTE3><P_ATTRIBUTE4></P_ATTRIBUTE4><P_ATTRIBUTE5></P_ATTRIBUTE5><P_ATTRIBUTE6></P_ATTRIBUTE6><P_ATTRIBUTE7></P_ATTRIBUTE7><P_ATTRIBUTE8></P_ATTRIBUTE8><P_ATTRIBUTE9></P_ATTRIBUTE9><P_ATTRIBUTE10></P_ATTRIBUTE10><P_PROJECT_NUMBER>AGR-33000254</P_PROJECT_NUMBER><P_ORGANIZATION_NAME>NOC</P_ORGANIZATION_NAME><P_INVENTORY_ORG_NAME></P_INVENTORY_ORG_NAME><P_INVENTORY_ITEM_ID></P_INVENTORY_ITEM_ID><P_QUANTITY_BILLED>1</P_QUANTITY_BILLED><P_UOM_CODE>EA</P_UOM_CODE><P_UNIT_PRICE>12000</P_UNIT_PRICE><P_REFERENCE1>Thankyouforyourbusiness.</P_REFERENCE1><P_REFERENCE2>10055</P_REFERENCE2><P_REFERENCE3>0</P_REFERENCE3><P_REFERENCE4>NP</P_REFERENCE4><P_REFERENCE5>1-333446666</P_REFERENCE5><P_REFERENCE6>Receivable</P_REFERENCE6><P_REFERENCE7>N</P_REFERENCE7><P_REFERENCE8></P_REFERENCE8><P_REFERENCE9>ServiceCharge-Base</P_REFERENCE9><P_REFERENCE10>67580</P_REFERENCE10><P_BILL_TRANS_CURRENCY_CODE>USD</P_BILL_TRANS_CURRENCY_CODE><P_BILL_TRANS_BILL_AMOUNT>12000</P_BILL_TRANS_BILL_AMOUNT><P_BILL_TRANS_REV_AMOUNT>0</P_BILL_TRANS_REV_AMOUNT><P_PROJECT_RATE_TYPE></P_PROJECT_RATE_TYPE><P_PROJECT_RATE_DATE></P_PROJECT_RATE_DATE><P_PROJECT_EXCHANGE_RATE></P_PROJECT_EXCHANGE_RATE><P_PROJFUNC_RATE_TYPE></P_PROJFUNC_RATE_TYPE><P_PROJFUNC_RATE_DATE></P_PROJFUNC_RATE_DATE><P_PROJFUNC_EXCHANGE_RATE></P_PROJFUNC_EXCHANGE_RATE><P_FUNDING_RATE_TYPE></P_FUNDING_RATE_TYPE><P_FUNDING_RATE_DATE></P_FUNDING_RATE_DATE><P_FUNDING_EXCHANGE_RATE></P_FUNDING_EXCHANGE_RATE><P_ADJUSTING_REVENUE_FLAG></P_ADJUSTING_REVENUE_FLAG><P_EVENT_ID></P_EVENT_ID><P_DELIVERABLE_ID></P_DELIVERABLE_ID><P_ACTION_ID></P_ACTION_ID><P_CONTEXT></P_CONTEXT><P_RECORD_VERSION_NUMBER></P_RECORD_VERSION_NUMBER><P_BILL_GROUP></P_BILL_GROUP><P_REVENUE_HOLD_FLAG>N</P_REVENUE_HOLD_FLAG><P_HEADER_NOTES>CheckifthisreallyisaHeaderNote</P_HEADER_NOTES><P_SHIP_TO_CUSTOMER></P_SHIP_TO_CUSTOMER><P_SHIP_TO_ADDRESS_ID></P_SHIP_TO_ADDRESS_ID><P_SHIP_TO_ADDRESS_1></P_SHIP_TO_ADDRESS_1><P_SHIP_TO_ADDRESS_2></P_SHIP_TO_ADDRESS_2><P_SHIP_TO_ADDRESS_3></P_SHIP_TO_ADDRESS_3><P_SHIP_TO_ADDRESS_4></P_SHIP_TO_ADDRESS_4><P_CITY></P_CITY><P_STATE></P_STATE><P_COUNTY></P_COUNTY><P_PROVINCE></P_PROVINCE><P_POSTAL_CODE></P_POSTAL_CODE><P_COUNTRY></P_COUNTRY><P_EXT_ATTRIBUTE1></P_EXT_ATTRIBUTE1><P_EXT_ATTRIBUTE2></P_EXT_ATTRIBUTE2><P_EXT_ATTRIBUTE3></P_EXT_ATTRIBUTE3><P_EXT_ATTRIBUTE4></P_EXT_ATTRIBUTE4><P_EXT_ATTRIBUTE5></P_EXT_ATTRIBUTE5><P_EXT_ATTRIBUTE6></P_EXT_ATTRIBUTE6><P_EXT_ATTRIBUTE7></P_EXT_ATTRIBUTE7><P_EXT_ATTRIBUTE8></P_EXT_ATTRIBUTE8><P_EXT_ATTRIBUTE9></P_EXT_ATTRIBUTE9><P_EXT_ATTRIBUTE10></P_EXT_ATTRIBUTE10><P_EXT_ATTRIBUTE11></P_EXT_ATTRIBUTE11><P_EXT_ATTRIBUTE12></P_EXT_ATTRIBUTE12><P_EXT_ATTRIBUTE13></P_EXT_ATTRIBUTE13><P_EXT_ATTRIBUTE14></P_EXT_ATTRIBUTE14><P_EXT_ATTRIBUTE15></P_EXT_ATTRIBUTE15><G_SRC_ATTRIBUTES><SRC_ATTRIBUTE><NAME></NAME><VALUE></VALUE></SRC_ATTRIBUTE></G_SRC_ATTRIBUTES></PA_BILLING_EVENT></G_PA_BILLING_EVENT></DATA_AREA></EBS_PROJECT_BILLING_REQUEST>';
        String IdString = String.ValueOf(InvoiceId[0]);
        String interfaceDetail = 'INT-155';
        String InterfaceLabel = 'SARevenue|'+IdString;
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail,InterfaceLabel);
      }       
    }
    
    public List<String> buildXmlStructure(SFS_Service_Agr_Revenue_XML_Structure__mdt[] revenueMetaMap,Map<String,Object>fieldsMap,String ObjectName,String ChargeType){
       List<String> xmlStructure = new List<String>();
       for(SFS_Service_Agr_Revenue_XML_Structure__mdt m: revenueMetaMap){
           System.debug('Map Meta: ' + m.SFS_XML_Full_Name__c);
              if(m.SFS_Salesforce_Object__c==ObjectName || m.SFS_Charge_Type__c==ChargeType){  
                  if(m.SFS_HardCoded_Flag__c == false){
                      String sfField = m.SFS_Salesforce_Field__c;
                      system.debug('sfField'+sfField);
                 
                      If(sfField=='Invoice_Date__c'){
                         String xmlfullName = m.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(System.today());
                          	replacement = replacement.escapeXML();
                            String newpa = XMLFullName.replace(sffield,replacement);  
                            xmlStructure.add(newpa);
                    }
                      //else if(fieldsMap!=null){
                       else if(fieldsMap.containsKey(sfField)){
                         System.debug('@@sfField'+fieldsMap.containsKey(sfField));
                         System.debug('@@sffield'+fieldsMap.get(sffield));
                          if(fieldsMap.get(sffield) != null){
                              System.debug('@@m.SFS_XML_Full_Name__c'+m.SFS_XML_Full_Name__c);
                               String xmlfullName = m.SFS_XML_Full_Name__c;
                               String replacement = String.ValueOf(fieldsMap.get(sffield));
                               replacement = replacement.escapeXML();
                               String newpa = XMLFullName.replace(sffield,replacement);  
                               xmlStructure.add(newpa);
                             }  
                             else if(fieldsMap.get(sffield) == null){
                                     xmlStructure.add(m.SFS_XML_Full_Name__c);
                                     }    
                                }
                     // }
                      //else if(fieldsMap!=null){
                              else if(!fieldsMap.ContainsKey(sffield)){
                                       String empty = '';
                                       String replacement = m.SFS_XML_Full_Name__c.replace(sffield,empty);
                                       xmlStructure.add(replacement);
                                    } 
                     // }
                               }
                  
                  
                             else if(m.SFS_HardCoded_Flag__c == true){
                                     xmlStructure.add(m.SFS_XML_Full_Name__c);
                                    }
                  
                       } 
           		System.debug('@@@XML::: ' + m.SFS_XML_Full_Name__c);
              }         
        return xmlStructure;      
    }
     
    
    Public static void getResponse(HttpResponse res, String interfaceLabel){
       List<String> InvoiceId = interfaceLabel.split('\\|');
        Id InvoiceIDRes = Id.ValueOf(InvoiceId[1]);
        
        Invoice__c invoice = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM Invoice__c WHERE Id =: InvoiceIDRes];
        List<SFS_Invoice_Line_Item__c> INVline = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM SFS_Invoice_Line_Item__c WHERE SFS_Invoice__c =: InvoiceIDRes];
        List<CAP_IR_Charge__c> Charge1 = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM CAP_IR_Charge__c WHERE SFS_Invoice__c =: InvoiceIDRes];
        
         If(res.getStatusCode() != 200){
            if(res.getStatusCode() == 0 || res.getBody() == 'Read timed out/nnull')
            {
               invoice.SFS_Integration_Status__c = 'Submit-Timed Out';
            invoice.SFS_Integration_Response__c = res.getBody();
            for(SFS_Invoice_Line_Item__c c: INVline){
                c.SFS_Integration_Status__c = 'Timed Out';
            	c.SFS_Integration_Response__c =  res.getBody();
            }
                for(CAP_IR_Charge__c ch: Charge1){
                ch.SFS_Integration_Status__c = 'Timed Out';
            	ch.SFS_Integration_Response__c =  res.getBody();
            }  
            }   
            else{
            invoice.SFS_Integration_Status__c = 'Submit-Failed';
            invoice.SFS_Integration_Response__c = res.getBody();
            for(SFS_Invoice_Line_Item__c c: INVline){
                c.SFS_Integration_Status__c = 'Failed';
            	c.SFS_Integration_Response__c =  res.getBody();
            }
                for(CAP_IR_Charge__c ch: Charge1){
                ch.SFS_Integration_Status__c = 'Timed Out';
            	ch.SFS_Integration_Response__c =  res.getBody();
            }  
            }
        }
        else{
            invoice.SFS_Integration_Status__c = 'Submitted';
            invoice.SFS_Integration_Response__c = res.getBody();
            for(SFS_Invoice_Line_Item__c c: INVline){
                c.SFS_Integration_Status__c = 'Submitted';
                invoice.SFS_Integration_Response__c = res.getBody();
            }
            for(CAP_IR_Charge__c ch: Charge1){
                ch.SFS_Integration_Status__c = 'Timed Out';
            	ch.SFS_Integration_Response__c =  res.getBody();
            }  
        }
        
        update invoice;
        update INVline;
       
     
    }
    
}