@isTest
public with sharing class AssignLeadTest {

    static final String accNumber = '12345678';

    @testSetup
    static void createData(){

        Account acct = new Account();
        acct.name = 'test account';
        acct.AccountNumber = accNumber;
        insert acct;

        acct = [select AccountNumber from Account where Id = :acct.Id];

        System.assert(acct.AccountNumber == accNumber, 'Something is changing the account\'s AccountNumber field');

        Lead theLead = new Lead(
            LastName = 'Picard',
            FirstName = 'Jean-Luke',
            Email = 'Captain@NCC1701D.net',
            Company = 'United Federation of Planets',
            Street = '123 Street St.',
            City = 'Palm Coast',
            PostalCode = '32164',
            State = 'Florida',
            Country = 'US'
        );
        insert theLead;

        theLead.FLD_Customer_Numbers__c = accNumber;
        update theLead;

        theLead = [select FLD_Customer_Numbers__c from Lead where Id = :theLead.Id];
        System.assert(theLead.FLD_Customer_Numbers__c == accNumber, 'Something is changing the lead FLD_Customer_Numbers__c field');
    }

    @isTest
    static void getOptions(){

        System.Test.startTest();
            List<Account> accOptions = AssignLead.fetchOptions([select Id from Lead][0].Id);
        System.Test.stopTest();

        System.assertNotEquals(0, accOptions.size(), 'Account not found');
        System.assertEquals(accNumber, accOptions[0].AccountNumber, 'Incorrect account returned');
    }

    @isTest
    static void assignLead(){

        Lead theLead = [select Id from Lead][0];

        System.Test.startTest();
            List<Account> accOptions = AssignLead.fetchOptions(theLead.Id);
        try{
            AssignLead.assignLead(accOptions[0].Id, theLead.Id);
        }
        catch(exception e){
            
        }
        System.Test.stopTest();

        theLead = [select Assigned_Distributor__c from Lead where Id = :theLead.Id];

        System.assertNotEquals(0, accOptions.size(), 'Account not found');
        System.assertEquals(accNumber, accOptions[0].AccountNumber, 'Incorrect account returned');
    }


}//lead opportunity to quote management