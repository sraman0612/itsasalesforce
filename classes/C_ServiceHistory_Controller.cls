public class C_ServiceHistory_Controller {
    @AuraEnabled
    public static boolean getIsReadOnly(){
        Profile pro = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        return pro.Name == 'Partner Community User - Read Only';
    }
    
    @AuraEnabled
    public static Asset queryAsset(Id assetId){
        return [SELECT Id, Name, Contract_Type__c, Contract_Start_Date__c, Contract_End_Date__c, Is_Covered__c, Current_Lubricant__c FROM Asset WHERE Id =: assetId];
    }
    
    @AuraEnabled
    public static List<Service_History__c> queryServiceHistory(Id assetId){
        return [SELECT Id, Name, Date_Of_Service__c, Hours_At_Service__c, Air_Filter__c, Oil_Filter__c, Lubricant__c, Lubricant_Used__c, Service_Notes__c, Current_Hours__c, Separator__c FROM Service_History__c WHERE Serial_Number__c =: assetId ORDER BY Date_Of_Service__c DESC NULLS LAST];
    }
    
    @AuraEnabled
    public static boolean saveAsset(Asset a){
        try {
            update a;
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
}