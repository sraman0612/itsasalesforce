@isTest
public class SFS_EntitlementHandlerBatch_Test {
    @TestSetup
    static void makeData() {
        List<Account> accnts = SFS_TestDataFactory.createAccounts(2, true);
        List<Asset> assets = SFS_TestDataFactory.createAssets(2, true);

        List<ServiceContract> cons = SFS_TestDataFactory.createServiceAgreement(2, accnts[0].Id, true);
        System.debug(cons);
        List<ContractLineItem> conLIs = SFS_TestDataFactory.createServiceAgreementLineItem(2, cons[0].Id, false);
        System.debug(conLIs);

        List<Entitlement> ents = SFS_TestDataFactory.createEntitlement(2, accnts[0].Id, assets[0].Id, cons[0].Id, conLIs[0].Id, false);
        for (Integer i = 0; i < accnts.size(); i++) {
            cons[i].AccountId = accnts[i].Id;
            conLIs[i].ServiceContractId = cons[i].Id;
            ents[i].AccountId = accnts[i].Id;
            ents[i].AssetId = assets[i].Id;
            ents[i].ContractLineItemId = conLIs[i].Id;
        }

        // Ended
        ents[0].StartDate = Date.today() - 7;
        ents[0].EndDate = Date.today() - 1;

        // Started
        ents[1].StartDate = Date.today();
        ents[1].EndDate = Date.today() + 7;

        update cons;
        insert conLIs;
        insert ents;
    }

    @isTest
    static void testBatch() {
        List<Entitlement> entsBefore = [SELECT Id, Processed_Start__c, Processed_End__c FROM Entitlement ORDER BY StartDate ASC];
        System.assertEquals(false, entsBefore[0].Processed_End__c, 'Should not be ended');
        System.assertEquals(false, entsBefore[1].Processed_Start__c, 'Should not be started');

        Test.startTest();
            Database.executeBatch(new SFS_EntitlementHandlerBatch(), 10);
        Test.stopTest();

        List<Entitlement> entsAfter = [SELECT Id, Processed_Start__c, Processed_End__c FROM Entitlement ORDER BY StartDate ASC];
        System.assertEquals(true, entsAfter[0].Processed_End__c, 'Should be ended');
        System.assertEquals(true, entsAfter[1].Processed_Start__c, 'Should be started');
    }
}