public class UTIL_ClaimAttachment {
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_ClaimAttachment.class);
    
    public static void handleAttachmentInsert(List<ContentDocumentLink> attachments) {
        processAttachmentsImpl(attachments);
    }
    
    public static void processAttachments(Id claimId) {
        if (claimId.getSObjectType() == Warranty_Claim__c.sObjectType || claimId.getSObjectType() == Machine_Startup_Form__c.sObjectType) {
            List<ContentDocumentLink> documentLinks = [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE IsDeleted = false AND LinkedEntityId = : claimId];

            if (!documentLinks.isEmpty()) {
                processAttachmentsImpl(documentLinks);
            }
        }
    }

    private static void processAttachmentsImpl(List<ContentDocumentLink> attachments) {
        Map<Id,String> sapServiceNotificationNumberMap = UTIL_ClaimAttachment.getServiceNotificationNumbers(attachments);

        List<SAP_ATTACHMENT> attachmentList = new List<SAP_ATTACHMENT>();

        for (ContentDocumentLink attachment : attachments) {
            String serviceNotificationNumber = sapServiceNotificationNumberMap.get(attachment.Id);

            if (String.isNotBlank(serviceNotificationNumber)) {
                SAP_ATTACHMENT cur = new SAP_ATTACHMENT();
                attachmentList.add(cur);
                cur.contentDocumentLinkId = attachment.Id;
                cur.serviceNotificationNumber = serviceNotificationNumber;
            }
        }

        if (!attachmentList.isEmpty()) {
            if (!Test.isRunningTest()) { System.enqueueJob(new LoadSAPServiceNotification(attachmentList)); } else { new LoadSAPServiceNotification(attachmentList).execute(null); }
        }
    }

    private static Map<Id,String> getServiceNotificationNumbers(List<ContentDocumentLink> attachments) {
        Map<Id,Id> warrantyClaimAttachmentMap = new Map<Id,Id>();
        Map<Id,Id> startupClaimAttachmentMap = new Map<Id,Id>();

        for (ContentDocumentLink attachment : attachments) {
            if (attachment.LinkedEntityId.getSObjectType() == Warranty_Claim__c.sObjectType) {
                warrantyClaimAttachmentMap.put(attachment.LinkedEntityId, attachment.Id);
            } else if (attachment.LinkedEntityId.getSObjectType() == Machine_Startup_Form__c.sObjectType) {
                startupClaimAttachmentMap.put(attachment.LinkedEntityId, attachment.Id);
            }
        }

        Map<Id,String> retVal = new Map<Id,String>();

        if (!warrantyClaimAttachmentMap.isEmpty()) {
            List<Warranty_Claim__c> warrantyClaims = [SELECT Id, SAP_Service_Notification_Number__c FROM Warranty_Claim__c where Id in : warrantyClaimAttachmentMap.keySet()];

            for (Warranty_Claim__c warrantyClaim : warrantyClaims) {
                retVal.put(warrantyClaimAttachmentMap.get(warrantyClaim.Id), warrantyClaim.SAP_Service_Notification_Number__c);
            }
        }

        if (!startupClaimAttachmentMap.isEmpty()) {
            List<Machine_Startup_Form__c> startupClaims = [SELECT Id, SAP_Service_Notification_Number__c FROM Machine_Startup_Form__c where Id in : startupClaimAttachmentMap.keySet()];

            for (Machine_Startup_Form__c startupClaim : startupClaims) {
                retVal.put(startupClaimAttachmentMap.get(startupClaim.Id), startupClaim.SAP_Service_Notification_Number__c);
            }
        }

        return retVal;
    }

    public class SAP_ATTACHMENT {
        public String serviceNotificationNumber;
        public Id contentDocumentLinkId;
    }

    public static Boolean shouldSendNotification(Id entityId) {
        try {
            List<Sobject> sobjectList = Database.query('SELECT Id, Notify_Warranty_Admin_Upon_Upload__c from ' + entityId.getSObjectType().getDescribe().getName() + ' where Id = \'' + entityId + '\'');
            return (Boolean)sobjectList.get(0).get('Notify_Warranty_Admin_Upon_Upload__c');
        } catch (Exception e) {
        }

        return false;
    }
    
    public class ZipExploder implements Queueable, Database.AllowsCallouts {
        public List<SAP_ATTACHMENT> attachments;
        public Id contentDocumentLinkId;

        public ZipExploder(List<SAP_ATTACHMENT> attachments, Id contentDocumentLinkId) {
            this.attachments = attachments;
            this.contentDocumentLinkId = contentDocumentLinkId;
        }

        public void execute(QueueableContext context) {
            ContentDocumentLink attachment = [SELECT Id, LinkedEntityId, ContentDocument.LatestPublishedVersion.VersionData from ContentDocumentLink where Id = : this.contentDocumentLinkId LIMIT 1];

            Zippex zippex = new Zippex(attachment.ContentDocument.LatestPublishedVersion.VersionData);

            Set<String> fileNames = zippex.getFileNames();

            List<ContentVersion> contentVersions = new List<ContentVersion>();
    
            for (String fileName : fileNames) {
                System.debug('fileName : ' + fileName);
                Map<String,String> fileInfo = zippex.getFileInfo(fileName);
    
                System.debug('fileInfo : ' + fileInfo);
                if (fileInfo.get('fileSize') == null || fileInfo.get('fileSize') == '0') {
                    continue;
                }
    
                ContentVersion contentVersion = new ContentVersion();
                contentVersion.VersionData = zippex.getFile(fileName);
                contentVersion.Title = fileName.substring(0, fileName.indexOf('.'));
                contentVersion.ContentLocation = 's';
                contentVersion.PathOnClient = fileName;
    
                contentVersions.add(contentVersion);
            }

            insert contentVersions;

            List<Id> contentVersionIds = new List<Id>();

            for (ContentVersion contentVersion : contentVersions) {
                contentVersionIds.add(contentVersion.Id);
            }

            List<ContentDocument> contentDocuments = [SELECT Id from ContentDocument where LatestPublishedVersionId in : contentVersionIds];

            List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();

            for (ContentDocument contentDocument : contentDocuments) {    
                ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
                contentDocumentLink.LinkedEntityId = attachment.LinkedEntityId;
                contentDocumentLink.ContentDocumentId = contentDocument.Id;
    
                contentDocumentLinks.add(contentDocumentLink);
            }

            insert contentDocumentLinks;

            delete attachment;

            if (!this.attachments.isEmpty()) {
                if (!Test.isRunningTest()) { System.enqueueJob(new LoadSAPServiceNotification(this.attachments)); } else { new LoadSAPServiceNotification(this.attachments).execute(null); }
            }
        }
    }

    public class LoadSAPServiceNotification implements Queueable, Database.AllowsCallouts {
        public List<SAP_ATTACHMENT> attachments;
        
        public LoadSAPServiceNotification(List<SAP_ATTACHMENT> attachments) {
            this.attachments = attachments;
        }

        public void execute(QueueableContext context) {
            logger.enterAura('execute', new Map<String, Object> {
                'attachments' => this.attachments
            });

            if (this.attachments.isEmpty()) {
                return;
            }

            SAP_ATTACHMENT attachment = this.attachments.remove(0);

            SBO_EnosixServiceNotification_Detail sbo = new SBO_EnosixServiceNotification_Detail();
            System.debug('attachment.contentDocumentLinkId=' + attachment.contentDocumentLinkId);
            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification notification = sbo.getDetail(attachment.serviceNotificationNumber);
            ContentDocumentLink attachmentLink = null;
            try {
 	           attachmentLink = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE Id = :attachment.contentDocumentLinkId LIMIT 1];
            } catch (Exception e) {
                return;
            }
            
            ContentVersion contentVersion = [SELECT Title, ContentSize, FileType, FileExtension, VersionData FROM ContentVersion WHERE ContentDocumentId = :attachmentLink.ContentDocumentId AND IsLatest = true LIMIT 1];

            if (contentVersion.FileType == 'ZIP') {
                if (!Test.isRunningTest()) { System.enqueueJob(new ZipExploder(this.attachments, attachmentLink.Id)); } else { new ZipExploder(this.attachments, attachmentLink.Id).execute(null); }
            } else {
                SBO_EnosixServiceNotification_Detail.ATTACHMENTS sboAttachment = new SBO_EnosixServiceNotification_Detail.ATTACHMENTS();
            
                sboAttachment.ContentData = EncodingUtil.base64Encode(contentVersion.VersionData);
                sboAttachment.ContentLength = String.valueOf(contentVersion.ContentSize);
                sboAttachment.ContentName = contentVersion.Title;
                sboAttachment.ContentFName = contentVersion.Title + '.' + contentVersion.FileExtension;
                sboAttachment.FieldContents =  contentVersion.FileType;
                notification.ATTACHMENTS.add(sboAttachment);
    
                SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = sbo.save(notification);
    
                System.debug('result.isSuccess()=' + result.isSuccess());
                if (!result.isSuccess()) {
                    List<ensxsdk.EnosixFramework.Message> mList = result.getMessages();
                    Integer mTot = mList.size();
                    for (Integer mCnt = 0 ; mCnt < mTot ; mCnt++) {
                        ensxsdk.EnosixFramework.Message m = mList[mCnt]; 
                        System.debug('message type=' + m.Type + ' text='+m.Text);
                    }
                }

                String[] toAddresses = new String[] {
                    (String)UTIL_AppSettings.getValue('UTIL_ClaimAttachment.WarrantyClaimQueueEmail')
                };
    
                if (UTIL_ClaimAttachment.shouldSendNotification(attachmentLink.LinkedEntityId)) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
                    mail.setToAddresses(toAddresses);
                    mail.setSubject('File Uploaded to Claim ' + notification.NotificationNumber);
                    mail.setPlainTextBody('The file "' + contentVersion.Title + '.' + contentVersion.FileExtension + '" was uploaded to claim ' + notification.NotificationNumber);
                    
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }

                if (!this.attachments.isEmpty()) {
                    if (!Test.isRunningTest()) { System.enqueueJob(new LoadSAPServiceNotification(this.attachments)); } else { new LoadSAPServiceNotification(this.attachments).execute(null); }
                }
            }
        }
    }
}