public class UTIL_MachineStartupForm
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_MachineStartupForm.class);

    public static void createServiceRequests(List<Id> formIds) {
        for (Id curFormId : formIds) {
            sendRequest(curFormId);
        }
    }

    private static void sendRequest(Id formId) {
        if (Test.isRunningTest()) {
            sendRequestImpl(formId);
        } else {
            sendRequestFuture(formId);
        }
    }

    @future(callout=true)
    private static void sendRequestFuture(Id formId) {
        sendRequestImpl(formId);
    }

    private static void sendRequestImpl(Id formId) {
        SBO_EnosixServiceNotification_Detail.EnosixServiceNotification serviceNotification = new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification();

        serviceNotification = UTIL_ServiceNotification.populateWithMachineStartupForm(serviceNotification, formId);

        serviceNotification = CTRL_EnosixServiceNotificationDetail.verifyServiceNotification(serviceNotification);

        if (!UTIL_MachineStartupForm.hasErrors(serviceNotification.getMessages())) {
            UTIL_MachineStartupForm.setMessage(formId, null, UTIL_MachineStartupForm.generateMessages(serviceNotification.getMessages()), null, null);
            
            if (!Test.isRunningTest()) { System.enqueueJob(new SaveServiceNotification(formId, serviceNotification)); } else { new SaveServiceNotification(formId, serviceNotification).execute(null); }
            
        } else {
            setMessage(formId, 'ERROR', UTIL_MachineStartupForm.generateMessages(serviceNotification.getMessages()), null, null);
        }
    }

    public static boolean hasErrors(List<ensxsdk.EnosixFramework.Message> messages) {
        if (messages == null) {
            return false;
        }
        
        for (ensxsdk.EnosixFramework.Message m: messages) {
            if (m.Type == ensxsdk.EnosixFramework.MessageType.Error) {
                return true;
            }
        }

        return false;
    }

    public static String generateMessages(List<ensxsdk.EnosixFramework.Message> messages) {
        if (messages == null) {
            return null;
        }
        
        String messagesStr = '';

        for (ensxsdk.EnosixFramework.Message m: messages) {
            if (m.Type == ensxsdk.EnosixFramework.MessageType.Warning) {
                messagesStr += ' Warning: ';
                messagesStr += m.Text;
                messagesStr += '\n';
            } else if (m.Type == ensxsdk.EnosixFramework.MessageType.Error) {
                messagesStr += ' ERROR: ';
                messagesStr += m.Text;
                messagesStr += '\n';
            } else if (m.Type == ensxsdk.EnosixFramework.MessageType.Success) {
                messagesStr += ' Success: ';
                messagesStr += m.Text;
                messagesStr += '\n';
            }

        }

        return messagesStr;
    }

    public static String getServiceNotificationId(List<ensxsdk.EnosixFramework.Message> messages) {
        if (messages == null) {
            return null;
        }
        
        for (ensxsdk.EnosixFramework.Message m: messages) {
            if (m.Type == ensxsdk.EnosixFramework.MessageType.Success) {
                return m.Text.substring(21, 30);
            }
        }

        return null;
    }

    public static String getSalesOrderNumber(List<ensxsdk.EnosixFramework.Message> messages) {
        if (messages == null) {
            return null;
        }
        
        for (ensxsdk.EnosixFramework.Message m: messages) {
            if (m.Type == ensxsdk.EnosixFramework.MessageType.Success) {
                if (m.Text.startsWith('WarrantyInternet')) {
                    return m.Text.substring(21, 30);
                }
            }
        }

        return null;
    }

    public static void setMessage(Id formId, String status, String errorMessage, String serviceNotificationId, String salesOrderNumber) {
        Machine_Startup_Form__c startupForm = [SELECT Id, Name, SAP_Status__c, SAP_Messages__c, SAP_Service_Notification_Number__c, SAP_Sales_Order_Number__c from Machine_Startup_Form__c where Id =: formId LIMIT 1];

        startupForm.SAP_Status__c = status;

        if (serviceNotificationId != null && startupForm.Name != serviceNOtificationId) {
            startupForm.Name = serviceNotificationId;
        }

        if (errorMessage != null) {
            if (startupForm.SAP_Messages__c == null) {
                startupForm.SAP_Messages__c = errorMessage;
            } else {
                startupForm.SAP_Messages__c += errorMessage;
            }
        }

        startupForm.SAP_Service_Notification_Number__c = serviceNotificationId;

        startupForm.SAP_Sales_Order_Number__c = salesOrderNumber;

        update startupForm;
    }

    public class SaveServiceNotification implements Queueable, Database.AllowsCallouts {
        private String formId;
        private SBO_EnosixServiceNotification_Detail.EnosixServiceNotification serviceNotification;

        public SaveServiceNotification(String formId, SBO_EnosixServiceNotification_Detail.EnosixServiceNotification serviceNotification) {
            this.formId = formId;
            this.serviceNotification = serviceNotification;
        }

        public void execute(QueueableContext context) {
            serviceNotification = CTRL_EnosixServiceNotificationDetail.createServiceNotificationImpl(serviceNotification);
            List<SBO_EnosixServiceNotification_Detail.PARTNERS> partners = serviceNotification.PARTNERS.getAsList();

            if (!UTIL_MachineStartupForm.hasErrors(serviceNotification.getMessages())) {
                String serviceNotificationId = UTIL_MachineStartupForm.getServiceNotificationId(serviceNotification.getMessages());
                UTIL_MachineStartupForm.setMessage(formId, null, UTIL_MachineStartupForm.generateMessages(serviceNotification.getMessages()), serviceNotificationId, null);
	            if (!Test.isRunningTest()) { System.enqueueJob(new CreateSalesOrder(formId, serviceNotificationId, partners)); } else { new CreateSalesOrder(formId, serviceNotificationId, partners).execute(null); }
            } else {
                UTIL_MachineStartupForm.setMessage(formId, 'ERROR', UTIL_MachineStartupForm.generateMessages(serviceNotification.getMessages()), null, null);
            }
        }
    }

    public class CreateSalesOrder implements Queueable, Database.AllowsCallouts {
        private String formId;
        private String serviceNotificationId;
        private List<SBO_EnosixServiceNotification_Detail.PARTNERS> partners;

        public CreateSalesOrder(String formId, String serviceNotificationId, List<SBO_EnosixServiceNotification_Detail.PARTNERS> partners) {
            this.formId = formId;
            this.serviceNotificationId = serviceNotificationId;
            this.partners = partners;
        }

        public void execute(QueueableContext context) {
            System.debug('starting CreateSalesOrder');
            SBO_EnosixSO_Detail.EnosixSO salesOrder = new SBO_EnosixSO_Detail.EnosixSO();

            salesOrder = UTIL_ServiceNotification.populateSalesOrderForStartupForm(salesOrder, formId, serviceNotificationId, partners);

            salesOrder = CTRL_EnosixServiceNotificationDetail.simulateOrder(salesOrder);
            
            if (!UTIL_MachineStartupForm.hasErrors(salesOrder.getMessages())) {
                UTIL_MachineStartupForm.setMessage(formId, null, UTIL_MachineStartupForm.generateMessages(salesOrder.getMessages()), serviceNotificationId, null);
                String claimText = UTIL_ServiceNotification.generateClaimText(formId);

                if (String.isNotBlank(claimText)) {
                    SBO_EnosixSO_Detail.TEXTS note1 = new SBO_EnosixSO_Detail.TEXTS();
                    note1.TextID = 'ZINT';
                    note1.TextLanguage = 'E';
                    note1.TextIDDescription = 'Internal X-Note';
                    note1.Text = claimText;
                    salesOrder.TEXTS.add(note1);
                }

                if (!Test.isRunningTest()) { System.enqueueJob(new SaveSalesOrder(formId, serviceNotificationId, salesOrder)); } else { new SaveSalesOrder(formId, serviceNotificationId, salesOrder).execute(null); }
            } else {
                UTIL_MachineStartupForm.setMessage(formId, 'ERROR', UTIL_MachineStartupForm.generateMessages(salesOrder.getMessages()), serviceNotificationId, null);
            }
        }
    }

    public class SaveSalesOrder implements Queueable, Database.AllowsCallouts {
        private String formId;
        private String serviceNotificationId;
        private SBO_EnosixSO_Detail.EnosixSO salesOrder;

        public SaveSalesOrder(String formId, String serviceNotificationId, SBO_EnosixSO_Detail.EnosixSO salesOrder) {
            this.formId = formId;
            this.serviceNotificationId = serviceNotificationId;
            this.salesOrder = salesOrder;
        }

        public void execute(QueueableContext context) {
            System.debug('starting SaveSalesOrder');

            salesOrder = CTRL_EnosixServiceNotificationDetail.saveSalesOrder(salesOrder);

            String salesOrderNumber = UTIL_MachineStartupForm.getSalesOrderNumber(salesOrder.getMessages());

            if (!UTIL_MachineStartupForm.hasErrors(salesOrder.getMessages())) {
                UTIL_MachineStartupForm.setMessage(formId, null, UTIL_MachineStartupForm.generateMessages(salesOrder.getMessages()), serviceNotificationId, salesOrderNumber);
	            if (!Test.isRunningTest()) { System.enqueueJob(new UpdateServiceNotification(formId, serviceNotificationId, salesOrderNumber)); } else { new UpdateServiceNotification(formId, serviceNotificationId, salesOrderNumber).execute(null); }
            } else {
                UTIL_MachineStartupForm.setMessage(formId, 'ERROR', UTIL_MachineStartupForm.generateMessages(salesOrder.getMessages()), serviceNotificationId, salesOrderNumber);
            }
        }
    }

    public class UpdateServiceNotification implements Queueable, Database.AllowsCallouts {
        private String formId;
        private String serviceNotificationId;
        private String salesOrderNumber;

        public UpdateServiceNotification(String formId, String serviceNotificationId, String salesOrderNumber) {
            this.formId = formId;
            this.serviceNotificationId = serviceNotificationId;
            this.salesOrderNumber = salesOrderNumber;
        }

        public void execute(QueueableContext context) {
            SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate snUpdate = new SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate();
            snUpdate.SalesDocument = salesOrderNumber;
            snUpdate = CTRL_EnosixServiceNotificationDetail.updateServiceNotification(snUpdate);

            if (!UTIL_MachineStartupForm.hasErrors(snUpdate.getMessages())) {
                UTIL_MachineStartupForm.setMessage(formId, 'SUCCESS', UTIL_MachineStartupForm.generateMessages(snUpdate.getMessages()), serviceNotificationId, salesOrderNumber);
                UTIL_ClaimAttachment.processAttachments(formId);
            } else {
                UTIL_MachineStartupForm.setMessage(formId, 'ERROR', UTIL_MachineStartupForm.generateMessages(snUpdate.getMessages()), serviceNotificationId, salesOrderNumber);
            }
        }
    }
}