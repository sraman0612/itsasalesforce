public with sharing class C_OilSample_SH {
    public C_OilSample_SH(Oil_Sample__c os) {
        Service_History__c[] sh = null;
        Asset sn = null;
        Account acc;
        Decimal shHours = null;
        String snStripped = null;
        Warranty_Compliance__c dateRange = Warranty_Compliance__c.getOrgDefaults();
        
        try {
            // Format serial num if it is incorrectly formatted
            if (os.Serial_Number_Text__c.containsWhitespace()) {
                snStripped = os.Serial_Number_Text__c.substringBefore(' ');
            } else {
                snStripped = os.Serial_Number_Text__c;
            }
            
            try {
                // If cannot find sn, orphan them 
                sn = [SELECT Id, Name, SerialNumber, Current_Servicer__c, Oil_Sample_Next_Expected_Date__c, Hours_Oil_Sample__c FROM Asset WHERE Name = :snStripped];
                System.debug('sn: ' + sn);
                os.Serial_Number_Lookup__c = sn.Id;
                Decimal dr = os.Date_Range__c;
                
                // Allow for customization in date range on request, if field is blank use defaults from custom settings
                if (os.Date_Range__c == null) {
                    dr = dateRange.Oil_Sample_Date_Range__c;
                }
                
                // Finding SH in date range
                sh = [SELECT Id, Name, Date_of_Service__c, Account__c, Current_Hours__c FROM Service_History__c WHERE Serial_Number__r.Name = :snStripped];
                for (Service_History__c s : sh) {
                    if (os.Collect_Date__c.daysBetween(s.Date_of_Service__c) <= dr && os.Collect_Date__c.daysBetween(s.Date_of_Service__c) >= -dr) {
                        System.debug('Found Service History rec. Dates in range. Date of service: ' + s.Date_of_Service__c + '; Oil collection date: ' + os.Collect_Date__c);
                        os.Service_History__c = s.Id;
                        System.debug('Current Hours: ' + s.Current_Hours__c);
                        shHours = s.Current_Hours__c;
                        //os.Distributor__c = s.Account__c;
                        System.debug('SH Id: ' + s.Id);
                        System.debug('SH account: ' + s.Account__c);
                        break;
                    }
                }
                
                // Tie lube sample to account
                try {
                    acc = [SELECT Id FROM Account WHERE Lube_Sample_Account_Name__c = :os.Collecting_Distributor__c];
                    os.Distributor__c = acc.Id;
                } catch (Exception z) {
                    acc = [SELECT Id FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
                    os.Distributor__c = acc.Id;
                }
            } catch (Exception e) {
                // If invalid SN (or doesn't exist)
                System.debug('Error: ' + e.getMessage());
                // Tie lube sample to account
                try {
                    acc = [SELECT Id FROM Account WHERE Lube_Sample_Account_Name__c = :os.Collecting_Distributor__c];
                    os.Distributor__c = acc.Id;
                } catch (Exception z) {
                    acc = [SELECT Id FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
                    os.Distributor__c = acc.Id;
                }
            }
            
            // If no service history was attached, use the account from the SN
            if (os.Distributor__c == null && sn.Current_Servicer__c != null) {
                os.Distributor__c = sn.Current_Servicer__c;
                System.debug('SN account: ' + sn.Current_Servicer__c);
            } else if (sn == null) {
                System.debug('Error: Current_Servicer__c cannot be null or invalid.');
            }
            
            // Assign a default distributor if none have been found?
            if (os.Distributor__c == null && sn != null && sh != null) {
                os.Distributor__c = sh[0].Account__c;
            }
            
            // Name will now be report number, null if none attached
            if (os.Report_Number__c != null) {
                os.Name = os.Report_Number__c;   
            } else {
                os.Name = 'Report Number Required';
            }
            
            System.debug('New record data: ' + os + ' ' + sn + ' ' + sh);
            upsert os;
        } catch(Exception e) {
            System.debug('error: ' + e.getMessage());
        }
    }
}