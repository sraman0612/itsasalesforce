global with sharing class CTRL_enosiXExternalConfiguratorPlugin
{
    private static String defaultCustomerNumber = (String)UTIL_AppSettings.getValue('CPQSimulation.CustomerNumber', '');
    private static String defaultSalesOrg = (String)UTIL_AppSettings.getValue('CPQSimulation.SalesOrg', '');
    private static String defaultDistributionChannel = (String)UTIL_AppSettings.getValue('CPQSimulation.DistributionChannel', '');
    private static String defaultDivision = (String)UTIL_AppSettings.getValue('CPQSimulation.Division', '');
    private static String defaultItemPlant = (String)UTIL_AppSettings.getValue('CPQSimulation.ItemPlant', '');

    @RemoteAction
    global static String getInfo(Map<String,Object> params) {
        String quoteId = (String)params.get('quoteId');
        String accountId = (String)params.get('accountId');
        String productId = (String)params.get('productId');

        String accountSoldTo;
        if (String.isNotEmpty(accountId)) {
            Account acct = UTIL_SFAccount.getAccountById(accountId);
            accountSoldTo = UTIL_SFAccount.getCustomerNumberFromAccount(acct);
        }
        if (String.isEmpty(accountSoldTo)) accountSoldTo = defaultCustomerNumber;

        String productMaterial;
        if (String.isNotEmpty(productId)) {
            Product2 pdt = UTIL_SFProduct.getProductById(productId);
            productMaterial = UTIL_SFProduct.getMaterialNumberFromProduct(pdt);
        }

        String quoteSAPConfiguration;
        if (String.isNotEmpty(quoteId)) {
            List<SBQQ__Quote__c> quotes = [SELECT SAP_Configuration__c FROM SBQQ__Quote__c WHERE Id = :quoteId];
            if (quotes.size() == 1) {
                quoteSAPConfiguration = quotes[0].SAP_Configuration__c;
            }
        }
        if (String.isEmpty(quoteSAPConfiguration))
        {
            Map<String, String> obj = new Map<String, String>();
            obj.put('salesOrg', defaultSalesOrg);
            obj.put('distChannel', defaultDistributionChannel);
            obj.put('division', defaultDivision);
            obj.put('soldToParty', String.isBlank(accountSoldTo) ? defaultCustomerNumber : accountSoldTo);
            quoteSAPConfiguration = JSON.serialize(obj);
        }

        Map<String, String> itemObj = new Map<String, String>();
        itemObj.put('plant', defaultItemPlant);
        String defaultItem = JSON.serialize(itemObj);

        return JSON.serialize(new Map<String,String> {
            'accountSoldTo' => accountSoldTo,
            'productMaterial' => productMaterial,
            'quoteSAPConfiguration' => quoteSAPConfiguration,
            'defaultItem' => defaultItem
        });
    }

    @RemoteAction
    global static void updateInfo(Map<String,Object> params) {
        String quoteId = (String)params.get('quoteId');
        String quoteSAPConfiguration = (String)params.get('quoteSAPConfiguration');
        if (String.isNotBlank(quoteId)) {
            List<SBQQ__Quote__c> quotes = [SELECT Id, SAP_Configuration__c  FROM SBQQ__Quote__c WHERE Id = :quoteId];
            if (quotes.size() == 1) {
                SBQQ__Quote__c quote = quotes[0];
                if (quoteSAPConfiguration != quote.SAP_Configuration__c) {
                    Boolean reenableCPQTriggers = Test.isRunningTest() || SBQQ.TriggerControl.isEnabled();
                    SBQQ.TriggerControl.disable();
                    try {
                        quote.SAP_Configuration__c = quoteSAPConfiguration;
                        update quote;
                    } finally {
                        if (reenableCPQTriggers) {
                            SBQQ.TriggerControl.enable();
                        }
                    }
                }
            }
        }
    }

    // declareDependencies is never called - it's purpose is for the compiler to raise the red flag when custom fields are missing
    @testVisible
    private void declareDependencies() {
        if(!Test.isRunningTest()) return;
        Object o = [SELECT SAP_Configuration__c FROM SBQQ__QuoteLine__c LIMIT 1];
    }
}