public class C_ChildAssetShare_Batch implements Database.batchable<sObject>{

    private static final Profile globalProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Global Account User'];
    private static final Profile singleProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community - Single Account User'];
    private static String query = 'SELECT Id, ProfileId, Partner_Account_ID__c, Partner_Account_Parent_ID__c FROM User WHERE IsActive = TRUE AND ProfileId IN (\'\'{0}\'\',\'\'{1}\'\')';
    
    
    public final String query_template_global = 'SELECT Id, Parent_Asset__c FROM Child_Asset__c WHERE Current_Servicer__r.ParentId = \'\'{0}\'\''; 
    public final String query_template_single = 'SELECT Id, Parent_Asset__c FROM Child_Asset__c WHERE Current_Servicer__c = \'\'{0}\'\''; 
    
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String formattedQuery = String.format(query, new List<String>{globalProfile.Id,singleProfile.Id});
        System.debug(formattedQuery);
        if(Test.isRunningTest()){
            formattedQuery += ' AND LastName = \'last-community\' ';
        }
        return Database.getQueryLocator(formattedQuery);      
    }
    
    public void execute(Database.BatchableContext info, List<User> scope){
        List<AssetShare> shareRecords = new List<AssetShare>();
        for(User u : scope){
            
            Boolean isGlobal = (u.ProfileId == globalProfile.Id);

			String innerQuery;
            
            if(isGlobal){
                innerQuery = String.format(query_template_global, new List<Object>{u.Partner_Account_Parent_ID__c});
            }
            else{
                innerQuery = String.format(query_template_single, new List<Object>{u.Partner_Account_ID__c});
            }

			List<Child_Asset__c> theAssets = Database.query(innerQuery);

            if(theAssets != null && theAssets.size() > 0){
                
        
                for(Child_Asset__c row : theAssets){
                    
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId = row.Parent_Asset__c;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.Id;
                    
                    shareRecords.add(share);
                }
                
            }            
            
        }
        
        if(shareRecords.size() > 0 ){
            insert shareRecords;
        }
        
    }     
    
    public void finish(Database.BatchableContext info){     

    } 
    
}