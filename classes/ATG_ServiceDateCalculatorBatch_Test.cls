@isTest
public with sharing class ATG_ServiceDateCalculatorBatch_Test {
    @TestSetup
    static void makeData(){
        Date today = System.today();
        Id currentUser = UserInfo.getUserId();
        Id distributorRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        //ContactId
        
        // User userObj = [SELECT Id, ContactId FROM User WHERE Id = :currentUser LIMIT 1];

        // System.debug('userobj: '+userObj);
        // Contact testContact = [SELECT Id FROM Contact WHERE Id =:userObj.ContactId LIMIT 1];
        Id profId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1][0].Id;
        User testUser = new User(FirstName = 'Testy', 
                                LastName = 'McTesterson',
                                Username = 'thisisatest@test.test',
                                Email = 'test@test.com.test',
                                Alias = 'alias',
                                TimeZoneSidKey = 'America/Chicago',
                                LocaleSidKey = 'en_US',
                                EmailEncodingKey = 'ISO-8859-1',
                                ProfileId = profId,
                                LanguageLocaleKey = 'en_US');
        insert testUser;
        List<Service_Contract__c> serviceContractsToInsert = new List<Service_Contract__c>();
        Contact testContact = new Contact(FirstName = 'Wile', 
                                          LastName = 'Coyote',
                                          RecordTypeId = distributorRecordTypeId, 
                                          Email = 'WileECoyote@Acme.com',
                                          OwnerId = testUser.Id);
        insert testContact;
        testUser.ContactId = testContact.Id;
        //update testUser;
        Account testAccount = new Account(Name = 'Acme inc', 
                                          Service_Notification_Contact__c = testContact.Id,
                                          TSM_of_DC_Account__c = currentUser);
        insert testAccount;
        
        List<Asset> serialNumbersToInsert = new List<Asset>();
        serialNumbersToInsert.add(new Asset(Name = 'SR-0000',
                                    AccountId = testAccount.Id,
                                    Expected_Next_Service_Date__c = today.addDays(3),
                                    Machine_Status__c = 'Active - Primary Unit',
                                    Inactive__c=False,
                                    IMEI__c='1234',    
                                    Hours__c = 1500.0,
                                    Current_Servicer__c = testAccount.Id));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0001',
                                   AccountId = testAccount.Id,
                                   Expected_Next_Service_Date__c = today.addDays(20),
                                   Machine_Status__c = 'Active - Primary Unit',
                                   iConn_c8y_H_total__c = 10000,
                                   iConn_c8y_H_service__c = 20000,
                                            Hours__c = 1500.0,
                                   Last_Logged_Service_Visit__c = today.addDays(-30),
                                   Current_Servicer__c = testAccount.Id));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0002',
                                   AccountId = testAccount.Id,
                                   Expected_Next_Service_Date__c = today.addDays(-7),
                                   Machine_Status__c = 'Active - Primary Unit',
                                   iConn_c8y_H_total__c = 10000,
                                   iConn_c8y_H_service__c = 20000,
                                            Hours__c = 1500.0,
                                   Last_Logged_Service_Visit__c = today.addDays(-30),
                                   Current_Servicer__c = testAccount.Id));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0003',
                                    AccountId = testAccount.Id,
                                    Expected_Next_Service_Date__c = today.addDays(3),
                                    Machine_Status__c = 'Customer Servicing - with OEM Parts',
                                    Hours__c = 1500.0,
                                    Last_Logged_Service_Visit__c = today.addDays(-30),
                                    Current_Servicer__c = testAccount.Id));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0004',
                                   AccountId = testAccount.Id,
                                   Expected_Next_Service_Date__c = today.addDays(20),
                                   Machine_Status__c = 'Customer Servicing - with OEM Parts',
                                   Current_Servicer__c = testAccount.Id));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0005',
                                   AccountId = testAccount.Id,
                                   Expected_Next_Service_Date__c = today.addDays(-7),
                                   Machine_Status__c = 'Customer Servicing - with OEM Parts',
                                   Current_Servicer__c = testAccount.Id));
        insert serialNumbersToInsert;
        //Create the service contracts
        //Id currentUser = UserInfo.getUserId();
        /*
        for(Asset serialNumber: serialNumbersToInsert){
            Service_Contract__c serviceContract = new Service_Contract__c();
            serviceContract.OwnerId = currentUser;
            serviceContract.Account__c = testAccount.Id;
            serviceContract.Service_Interval__c = '3';
            serviceContract.Serial_Number__c = serialNumber.Id;
            serviceContractsToInsert.add(serviceContract);
            
        }
        insert serviceContractsToInsert;
        */
        // System.debug('ServContracts: '+serviceContractsToInsert[0]);
    }
    @isTest
    public static void serviceDateCalculatorBatchTest(){
        Test.startTest();
        ATG_ServiceDateCalculatorBatch batch = new ATG_ServiceDateCalculatorBatch();
        Database.executeBatch(batch);

        //Retrieve Serial Numbers and confirm Calculated Service Date field is populated
        Test.stopTest();
    }
}