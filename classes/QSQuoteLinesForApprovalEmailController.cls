public class QSQuoteLinesForApprovalEmailController {

    // Code Zero
    // Used with QSQuoteLinesForApprovalEmail Component and CPQ Advanced Approvals VisualForce email templates
    // Displays all applicable Quote Lines 

    public String Quote;
    public String getQuote()
    {
        return quote;
    }
    public void setQuote(String q)
    {
        Quote = q;
        getQuoteLinesForEmail();
    }
    
    public List <SBQQ__QuoteLine__c> QuoteLines;

    public void getQuoteLinesForEmail() {
        
        System.debug('Quote ID: ' + quote);
        try
        {
            QuoteLines = [select SBQQ__ProductName__c, SBQQ__Quantity__c, SBQQ__Discount__c,
                SBQQ__TotalDiscountAmount__c, SBQQ__NetTotal__c
                from SBQQ__QuoteLine__c
                where SBQQ__Quote__c =: quote];
        }
        catch (Exception e) {} //included to prevent errors when viewing email template in Setup menu
    }
    
    public List <SBQQ__QuoteLine__c> getQuoteLines() {
        return quotelines;
    }

}