@isTest
public class SFS_ScheduleBatchToGetLaborRateTest {
   @isTest
    public static void SFS_ScheduleBatchToGetLaborRateTest()
    {
       String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
        RecordType contactRecordType = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp AIRD Contact' AND SObjectType = 'Contact'];
        List<Account> accList=SFS_TestDataFactory.createAccounts(2, false); 
        accList[0].IRIT_Customer_Number__c='1211'; 
        accList[0].Type='Prospect'; 
        accList[1].IRIT_Customer_Number__c='1281';
        accList[1].RecordTypeId=accRecID;
        insert accList;
        accList[0].Bill_To_Account__c=accList[1].Id;
        update accList;
        Contact con = new Contact(Salutation = 'Ms.',LastName = 'test', FirstName='contact', Title = 'KS', Email = 'example@ir.com',RecordTypeId = contactRecordType.Id,
                                   Phone ='8890009', AccountId = accList[0].Id);
        
        insert con;
        
        Division__c div=new Division__c(Name='and',	EBS_System__c='MfgPRO',CurrencyIsoCode='USD');
        insert div;
       
        ServiceContract Serc=new ServiceContract(AccountId=accList[0].Id,SFS_Bill_To_Account__c=accList[0].id,SFS_Type__c='Advanced Billing',SFS_Division__c=div.Id,SFS_Status__c='Draft',SFS_Agreement_Value__c=23,
                                           CurrencyIsoCode='USD',	SFS_Consumables_Ship_To__c=accList[0].Id,	SFS_Shipping_Instructions__c='the tagah',	SFS_Portable_Required__c='Yes',
                                           	SFS_Invoice_Type__c='Prepaid',	SFS_Invoice_Format__c='Detail',Name='agree',StartDate=Date.newInstance(2021, 11, 5),
                                           	EndDate=Date.newInstance(2021, 11, 30));
        
        insert Serc;
        
        List<OperatingHours> opHoursList=new List<OperatingHours>();
        OperatingHours opHours=new OperatingHours();
        opHours.Name = 'Test Operating Hours';
        opHours.TimeZone = UserInfo.getTimeZone().getID();
        opHoursList.add(opHours);
        insert opHoursList[0];
        
        DateTime cDT = system.Now();
		String dayOfWeek = cDT.format('EEEE'); 
        Time timeStart = Time.newInstance(Integer.valueOf('04'),Integer.valueOf('30'),0,0);
        Time timeEnd = Time.newInstance(Integer.valueOf('07'),Integer.valueOf('30'),0,0);
        List<TimeSlot> timeSloList=new List<TimeSlot>();
        TimeSlot timeslt=new TimeSlot();
        timeslt.StartTime=timeStart;
        timeslt.EndTime=timeEnd;
        timeslt.DayOfWeek=dayOfWeek;
        timeslt.OperatingHoursId = opHoursList[0].Id;
        timeSloList.add(timeslt);
        insert timeSloList;
        
        List<MaintenancePlan> planList = SFS_TestDataFactory.createMaintenancePlan(1,Serc.Id,false);
        insert planList;
        
        List<WorkOrder> woList=new List<WorkOrder>();
        WorkOrder wo = new WorkOrder(AccountId = accList[0].Id, 
                                    ContactId = con.Id,
                                    SFS_Work_Order_Type__c = 'Installation',
                                    Status = 'New',
                                    SFS_Severity__c = '4 - Low',
                                    subject = 'test',
                                    SFS_Estimated_Work_Order_Value__c=100,
                                    ServiceContractId=Serc.Id,
                                    FSL__VisitingHours__c = opHoursList[0].Id,
                                    MaintenancePlanId=planList[0].Id,
                                    SuggestedMaintenanceDate=System.today()+5,
                                    SFS_CPQ_Transaction_Id__c='320046870'
                                    );
        insert wo;
        woList.add(wo);
        
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId=woList[0].Id,
                                                       SFS_Quote_Number__c='1236589',
                                                       Status = 'New',
                                                       MaintenancePlanId=woList[0].MaintenancePlanId,
                                                       SuggestedMaintenanceDate=System.today()+5,
                                                       SFS_Alpha_WOLI__c=true,SFS_Overtime_Labor_Rate__c=Null
                                                      );
        insert woli;
        woliList.add(woli);
        Test.startTest();
        String jobId = System.schedule('Test Batch Job', '0 0 0 15 3 ?', new SFS_ScheduleBatchToGetLaborRate());
        Test.stopTest();
    }
}