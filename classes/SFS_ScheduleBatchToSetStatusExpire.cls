/*=========================================================================================================
* @author Yadav, Arati
* @date 07/10/2021
* @description:This is used to update the Service Agreement status expired

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/


global class SFS_ScheduleBatchToSetStatusExpire implements schedulable{
 global void execute(SchedulableContext sc) {
     SFS_batchClassToSetSAStatusExpire b = new SFS_batchClassToSetSAStatusExpire(); 
     database.executebatch(b,1);
       
   }
}