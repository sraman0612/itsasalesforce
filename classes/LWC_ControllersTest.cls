@isTest
public class LWC_ControllersTest {

    static testMethod void testCaseAccept() {
        Case testCase1 = new Case();
        testCase1.Status = 'New';
        testCase1.Store_Number__c  = 'M994';
        testCase1.Subject = 'Test';
        insert testCase1;

        Case queryCase = CaseAccept.getCase(testCase1.Id);
        Case updtCase = CaseAccept.updateCase(testCase1);
    }

    static testMethod void testCaseAddToCalendar() {
        Case testCase2 = new Case();
        testCase2.Status = 'New';
        testCase2.Store_Number__c  = 'M994';
        testCase2.Subject = 'Test';
        insert testCase2;

        Case queryCase = CaseAddToCalendar.getCase(testCase2.Id);
    }

    static testMethod void testCaseNewActionItem() {
        Case testCase3 = new Case();
        testCase3.Status = 'New';
        testCase3.Store_Number__c  = 'M994';
        testCase3.Subject = 'Test';
        insert testCase3;

        Case queryCase = CaseNewActionItem.getCase(testCase3.Id);
        String recType = CaseNewActionItem.getCaseRecType('Customer Care');
    }

    static testMethod void testCaseDispatchPaperworkPDF() {
        Case testCase4 = new Case();
        testCase4.Status = 'New';
        testCase4.Store_Number__c  = 'M994';
        testCase4.Subject = 'Test';
        insert testCase4;

        Case queryCase = CaseDispatchPaperworkPDF.getCase(testCase4.Id);
        Case updtCase = CaseDispatchPaperworkPDF.updateCase(testCase4);
    }

    static testMethod void testCaseNewFromContact() {
        Account testAccount1 = new Account();
        testAccount1.Name = 'Test Account';
        Insert testAccount1;

        Contact testContact1 = new Contact();
        testContact1.FirstName = 'John';
        testContact1.LastName  = 'Doe';
        testContact1.Email = 'jdoe@test.com';
        testContact1.AccountId = testAccount1.Id;
        insert testContact1;

        Contact queryContact = CaseNewFromContact.getContact(testContact1.Id);
        User queryUser = CaseNewFromContact.getUserData();
    }

    static testMethod void testCaseNewSiteVisit() {
        Case testCase5 = new Case();
        testCase5.Status = 'New';
        testCase5.Store_Number__c  = 'M994';
        testCase5.Subject = 'Test';
        insert testCase5;

        Case queryCase = CaseNewSiteVisit.getCase(testCase5.Id);
    }

    static testMethod void testCaseQuickClose() {
        Account testAccount2 = new Account();
        testAccount2.Name = 'Test Account 2';
        Insert testAccount2;

        Contact testContact2 = new Contact();
        testContact2.FirstName = 'Jane';
        testContact2.LastName  = 'Doe';
        testContact2.Email = 'jane.doe@test.com';
        testContact2.AccountId = testAccount2.Id;
        insert testContact2;

        Case testCase6 = new Case();
        testCase6.Assigned_From_Address_Picklist__c = 'belliss.service.qcy@gardnerdenver.com';
        testCase6.Brand__c = 'Belliss - CS';
        testCase6.Subject = 'Quick Close Test';
        testCase6.Status = 'Open';
        testCase6.Description  = 'Quick Close Test';
        testCase6.Inquiry_Call_Type_List__c = 'Product Support';
        testCase6.Re_opened__c = false;
        testCase6.Work_Completed_Date__c = system.Today();
        testCase6.AccountId = testAccount2.Id;
        testCase6.ContactId = testContact2.Id;
        insert testCase6;

        Case queryCase = CaseQuickClose.getCase(testCase6.Id);
        Case updtCase = CaseQuickClose.updateCase(testCase6);
    }

    static testMethod void testEmailNewActionItem() {
        Case testCase3 = new Case();
        testCase3.Status = 'New';
        testCase3.Store_Number__c  = 'M994';
        testCase3.Subject = 'Test';
        insert testCase3;

        EmailMessage eMsg = new EmailMessage();
        eMsg.ParentId = testCase3.Id;
        eMsg.RelatedToId = testCase3.Id;
        eMsg.FromAddress = 'joe.black@gardnerdenver.com';
        eMsg.FromName = 'Joe Black';
        eMsg.Subject = 'Case Email Test';
        eMsg.TextBody = 'Hi There, Hope you are having a great day!';
        eMsg.ToAddress = 'tina.turner@gardnerdenver.com';
        insert eMsg;

        Case queryCase = EmailNewActionItem.getCase(eMsg.Id);
        String recType = EmailNewActionItem.getCaseRecType('Customer Care');
    }
}