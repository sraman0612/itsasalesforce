@isTest
public class CXUtilsTest {

    @isTest
    static void testDateTimeForTimezone() {
        DateTime dt = CXUtils.dateTimeForTimezone(2023, 08, 03, 01, 16, 53, 'America/New_York');
        System.assert(dt.hourGmt() == 05, 'GMT hour should be 5');
    }


    @isTest
    static void testGetMonthNumber() {
        Integer monthNum = CXUtils.getMonthNum('December');
        System.assert(monthNum == 12, 'December should be the 12th month');
    }
}