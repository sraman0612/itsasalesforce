public class SFS_getChargesforCredit {
     @invocableMethod(label = 'charges for Credit' description = 'get charges by selected invli types' Category = 'Invoice')
    public static List<ReturnVariables> credit( List<InputVariables> inputVariables){
        //get input 
       List<SFS_Invoice_Line_Item__c> passedInvLi = inputVariables.get(0).Invli;
        //define output
        List<CAP_IR_Charge__c> chrgList = new List<CAP_IR_Charge__c>();
        
        List<string> invliTypes =new List<string>();
        for(SFS_Invoice_Line_Item__c inLi:passedInvLi){
            invliTypes.add(inli.SFS_Type__c);
        }
        
        chrgList=[select id,SFS_charge_details_for_credit__c, SFS_Sell_Price__c from CAP_IR_Charge__c where SFS_Invoice__c =:passedInvLi[0].SFS_Invoice__c and SFS_Charge_Type__c IN:invliTypes ];
        List<ReturnVariables> returnVarsList =new List<ReturnVariables>();
        ReturnVariables returnVars = new ReturnVariables();
        returnVars.returnCharge =chrgList;
        returnVarsList.add(returnVars);
        return returnVarsList;
        
    }
    
    public class InputVariables{
        @InvocableVariable
        public List<SFS_Invoice_Line_Item__c> Invli;
    }
    public class ReturnVariables{
        @InvocableVariable
        public List<CAP_IR_Charge__c> returnCharge;
    }
}