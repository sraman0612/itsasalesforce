/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 10/01/2022
* @description: This class is used to delete the Maintenance Plan and Maintenance asset records based on the record Ids passed from Service Agreement, 
                Service Agreement Line Items and Entitlement flows
* @Story Number: SIF-52

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/
public class SFS_deleteMaintenancePlanAndAsset {
    
    @InvocableMethod(label = 'Get Service Agreement')
    public static void deleteMaintenancePlanAndAsset(List<Id> scIds){
        
        String SObjType='';
        String[] WOGenerationStatus = new String[] {'Unsuccessful','NotStarted'};
        List<String> MPIds = new List<String>();
        for(Id ids:scIds){
           SObjType=ids.getSobjectType().getDescribe().getName(); 
        }        
		
        if(SObjType=='ServiceContract'){
            List<MaintenancePlan> mPlanList = [Select Id from MaintenancePlan where ServiceContractId IN:scIds AND WorkOrderGenerationStatus IN :WOGenerationStatus]; 

        	if(!mPlanList.isEmpty()){
            	database.delete(mPlanList,false); // deletes Maintenance Plan Records
         	}   
        } else if(SObjType=='ContractLineItem'){
	  		List<MaintenanceAsset> mAssetList= [Select MaintenancePlanId,Id from MaintenanceAsset where SFS_Service_Agreement_Line_Item__c IN:scIds];
      		for(MaintenanceAsset ma : mAssetList){
        		MPIds.add(ma.MaintenancePlanId);
      		}	
        	if(!mAssetList.isEmpty()){
            	database.delete(mAssetList,false); //deletes Maintenance Asset Records               
         	}  
            
            List<MaintenanceAsset> mPlanCheckList = [Select Id from MaintenanceAsset where MaintenancePlanId IN:MPIds]; 
           	if(mPlanCheckList.isEmpty()){  
                	database.delete(MPIds,false); // deletes Maintenance Plan Records
            }
            
        } else if(SObjType=='Entitlement'){
            List<MaintenanceAsset> mAssetEList= [Select MaintenancePlanId,Id from MaintenanceAsset where SFS_Entitlement__c IN:scIds];
      		for(MaintenanceAsset ma : mAssetEList){
        		MPIds.add(ma.MaintenancePlanId);
      		}	
        	if(!mAssetEList.isEmpty()){
            	database.delete(mAssetEList,false); //deletes Maintenance Asset Records
         	}   
            
            List<MaintenanceAsset> mPlanCheckEList = [Select Id from MaintenanceAsset where MaintenancePlanId IN:MPIds]; 
           	if(mPlanCheckEList.isEmpty()){
                	database.delete(MPIds,false); // deletes Maintenance Plan Records
            }
        }

    }
   
}