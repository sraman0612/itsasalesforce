/// enosiX Inc. Generated Apex Model
/// Generated On: 10/9/2019 10:07:34 AM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixZAPR_Search extends ensxsdk.EnosixFramework.SearchSBO 
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixZAPR_Search_Meta', new Type[] {
            SBO_EnosixZAPR_Search.EnosixZAPR_SC.class
            , SBO_EnosixZAPR_Search.EnosixZAPR_SR.class
            , SBO_EnosixZAPR_Search.SEARCHRESULT.class
            , SBO_EnosixZAPR_Search.SEARCHPARAMS.class
            , SBO_EnosixZAPR_Search.SEARCHRESULT.class
            } 
        );
    }

    public SBO_EnosixZAPR_Search() 
    {
        super('EnosixZAPR', SBO_EnosixZAPR_Search.EnosixZAPR_SC.class, SBO_EnosixZAPR_Search.EnosixZAPR_SR.class);
    }
    
    public override Type getType() { return SBO_EnosixZAPR_Search.class; }

    public EnosixZAPR_SC search(EnosixZAPR_SC sc) 
    {
        return (EnosixZAPR_SC)super.executeSearch(sc);
    }

    public EnosixZAPR_SC initialize(EnosixZAPR_SC sc) 
    {
        return (EnosixZAPR_SC)super.executeInitialize(sc);
    }

    public class EnosixZAPR_SC extends ensxsdk.EnosixFramework.SearchContext 
    { 		
        public EnosixZAPR_SC() 
        {		
            super(new Map<string,type>		
                {		
                    'SEARCHPARAMS' => SBO_EnosixZAPR_Search.SEARCHPARAMS.class		
                });		
        }

        public override Type getType() { return SBO_EnosixZAPR_Search.EnosixZAPR_SC.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixZAPR_Search.registerReflectionInfo();
        }

        public EnosixZAPR_SR result { get { return (EnosixZAPR_SR)baseResult; } }


        @AuraEnabled public SBO_EnosixZAPR_Search.SEARCHPARAMS SEARCHPARAMS 
        {
            get
            {
                return (SBO_EnosixZAPR_Search.SEARCHPARAMS)this.getStruct(SBO_EnosixZAPR_Search.SEARCHPARAMS.class);
            }
        }
        
        }

    public class EnosixZAPR_SR extends ensxsdk.EnosixFramework.SearchResult 
    {
        public EnosixZAPR_SR() 
        {
            super(new Map<string,type>{'SEARCHRESULT' => SBO_EnosixZAPR_Search.SEARCHRESULT.class } );
        }
        
        public ensxsdk.EnosixFramework.FrameworkCollection SearchResults
        {
            get { return super.getCollection(SBO_EnosixZAPR_Search.SEARCHRESULT.class); }
        }
        
        public List<SEARCHRESULT> getResults() 
        {
            List<SEARCHRESULT> results = new List<SEARCHRESULT>();
            SearchResults.copyTo(results);
            return results;
        }

        public override Type getType() { return SBO_EnosixZAPR_Search.EnosixZAPR_SR.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixZAPR_Search.registerReflectionInfo();
        }
    }

    public class SEARCHPARAMS extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixZAPR_Search.SEARCHPARAMS.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixZAPR_Search.registerReflectionInfo();
        }
        @AuraEnabled public String SalesOrganization
        { 
            get { return this.getString ('VKORG'); } 
            set { this.Set (value, 'VKORG'); }
        }

        @AuraEnabled public String DistributionChannel
        { 
            get { return this.getString ('VTWEG'); } 
            set { this.Set (value, 'VTWEG'); }
        }

        @AuraEnabled public String Country
        { 
            get { return this.getString ('LAND1'); } 
            set { this.Set (value, 'LAND1'); }
        }

        @AuraEnabled public String Region
        { 
            get { return this.getString ('REGIO'); } 
            set { this.Set (value, 'REGIO'); }
        }

        @AuraEnabled public String County
        { 
            get { return this.getString ('COUNC'); } 
            set { this.Set (value, 'COUNC'); }
        }

        @AuraEnabled public String AppSegCode
        { 
            get { return this.getString ('APPSEG'); } 
            set { this.Set (value, 'APPSEG'); }
        }

    }

    public class SEARCHRESULT extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixZAPR_Search.SEARCHRESULT.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixZAPR_Search.registerReflectionInfo();
        }
        @AuraEnabled public String SalesOrganization
        { 
            get { return this.getString ('VKORG'); } 
            set { this.Set (value, 'VKORG'); }
        }

        @AuraEnabled public String DistributionChannel
        { 
            get { return this.getString ('VTWEG'); } 
            set { this.Set (value, 'VTWEG'); }
        }

        @AuraEnabled public String Country
        { 
            get { return this.getString ('LAND1'); } 
            set { this.Set (value, 'LAND1'); }
        }

        @AuraEnabled public String Region
        { 
            get { return this.getString ('REGIO'); } 
            set { this.Set (value, 'REGIO'); }
        }

        @AuraEnabled public String County
        { 
            get { return this.getString ('COUNC'); } 
            set { this.Set (value, 'COUNC'); }
        }

        @AuraEnabled public String AppSegCode
        { 
            get { return this.getString ('APPSEG'); } 
            set { this.Set (value, 'APPSEG'); }
        }

        @AuraEnabled public String AppSegCodeDesc
        { 
            get { return this.getString ('APPSEGDESC'); } 
            set { this.Set (value, 'APPSEGDESC'); }
        }

        @AuraEnabled public String Sequence
        { 
            get { return this.getString ('SEQUM'); } 
            set { this.Set (value, 'SEQUM'); }
        }

        @AuraEnabled public String CustomerNumber
        { 
            get { return this.getString ('KUNNR'); } 
            set { this.Set (value, 'KUNNR'); }
        }

        @AuraEnabled public String SalesDistrict
        { 
            get { return this.getString ('BZIRK'); } 
            set { this.Set (value, 'BZIRK'); }
        }

        @AuraEnabled public String SalesDistrictName
        { 
            get { return this.getString ('BZTXT'); } 
            set { this.Set (value, 'BZTXT'); }
        }

    }

    public class SEARCHRESULT_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public SEARCHRESULT_COLLECTION()
        {
            super('SEARCHRESULT', SBO_EnosixZAPR_Search.SEARCHRESULT.class, null);
        }

        public List<SBO_EnosixZAPR_Search.SEARCHRESULT> getAsList()
        {
            return (List<SBO_EnosixZAPR_Search.SEARCHRESULT>)this.buildList(List<SBO_EnosixZAPR_Search.SEARCHRESULT>.class);
        }
    }


}