@isTest
public class ConcessionRequest_ctr_Test {

    @testSetup
    private static void createTestData(){
        
        Product2 prod = new Product2(
            Name = 'Laptop X200',
            Family = 'Hardware',
            ProductCode = 'ABCDE'
        );
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 10000,
            IsActive = true
        );
        insert standardPrice;

        String distributionChannel = 'CM';
        Map<String,Schema.RecordTypeInfo> oppRecordTypesByName = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName();
        Map<String,Schema.RecordTypeInfo> quoteRecordTypesByName = SBQQ__Quote__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName();
        Id salesRecordTypeId = oppRecordTypesByName.get('Sales').getRecordTypeId();
        Id quoteDraftRecordTypeId = quoteRecordTypesByName.get('Quote_Draft').getRecordTypeId();
        
        Account acct = new Account();
        acct.Name = 'Test Account';
        acct.Child_DCs__c = 'XX,YY,';
         acct.Account_Number__c='1234';  
        acct.DC__c = 'CP - XXXXXXX';

        insert acct;
        
        Opportunity opp = new Opportunity();

        opp.Name = 'test';
        opp.StageName = 'Stage 1 - Qualified Lead';
        opp.CloseDate = System.today().addMonths(1);
        opp.AccountId = acct.Id;
        opp.End_User_Account__c = acct.Id;
        opp.SBQQ__QuotePricebookId__c = Test.getStandardPricebookId();
        opp.RecordTypeId = salesRecordTypeId;
        opp.Sales_Channel__c = distributionChannel;

        insert opp; 

        SBQQ__Quote__c quote = new SBQQ__Quote__c();

        quote.SBQQ__Account__c = acct.Id;
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__Primary__c = true;
        quote.RecordTypeId = quoteDraftRecordTypeId;
        quote.End_Customer_Account__c = acct.Id;
        quote.Distribution_Channel__c = distributionChannel;
        quote.Count_of_Compressors__c=2;
        quote.Count_of_Filters__c=0;
        quote.Dismiss_Filter_Alert__c=False;
        quote.SAP_Configuration__c = '{"soldToParty":"185150","division":"00","distChannel":"CM","salesOrg":"GDMI"}';

        insert quote;
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__Quote__c = quote.Id;
        quoteLine.SBQQ__Product__c = prod.Id;
        quoteLine.SBQQ__Quantity__c = 1;
        insert quoteLine;
        
        SBQQ__QuoteProcess__c qp=new SBQQ__QuoteProcess__c();
		qp.name='Test';
		insert qp;
		
		
		SBQQ__ProcessInput__c pi=new SBQQ__ProcessInput__c();
		pi.SBQQ__QuoteProcess__c=qp.id;
		insert pi;
		
		SBQQ__ProcessInputValue__c processInput=new SBQQ__ProcessInputValue__c();
        processInput.SBQQ__ProcessInputID__c = pi.id; 
        processInput.SBQQ__QuoteId__c = quote.Id;
        processInput.SBQQ__Value__c ='Piping';
		insert processInput;
        
        //insert new Concession_Request__c(Quote_Line__c=quoteLine.Id);
        // debugging => Id tmp = String.valueOf([select Discount1__c from SBQQ__QuoteLine__c][0].Discount1__c);
    }
    
    @isTest
    private static void ConcessionRequest_ctr_1(){
        
        PageReference concessionPage = Page.ConcessionRequest;
        concessionPage.getParameters().put('Id', 'invalid_id');
        
        System.Test.setCurrentPage(concessionPage);
        
        Boolean hasErrorsBefore = ApexPages.hasMessages(ApexPages.severity.ERROR);
        System.assertEquals(false, hasErrorsBefore);
            
        System.Test.startTest();
            ConcessionRequest_ctr controller = new ConcessionRequest_ctr();
        System.Test.stopTest();
        
        Boolean hasErrorsAfter = ApexPages.hasMessages(ApexPages.severity.ERROR);
        System.assertEquals(true, hasErrorsAfter, 'User was not notified');
    }
    @isTest
    private static void concession_record_created(){
        
        SBQQ__QuoteLine__c record = [
            SELECT  Standard_Multiplier1__c 
            FROM    SBQQ__QuoteLine__c 
            LIMIT   1
        ];
        
        PageReference concessionPage = Page.ConcessionRequest;
        concessionPage.getParameters().put('Id', record.Id);
        
        List<Concession_Request__c> existingRequests = new List<Concession_Request__c>([
            SELECT  Id
            FROM    Concession_Request__c
        ]);
        
        System.assert(existingRequests.isEmpty());
        
        System.Test.setCurrentPage(concessionPage);
        System.Test.startTest();
            
            ConcessionRequest_ctr controller = new ConcessionRequest_ctr();
            controller.createRecord();
            
        System.Test.stopTest();
        
        System.assert(controller.isInitError == false);
        
        List<Concession_Request__c> requests = new List<Concession_Request__c>([
            SELECT  Id
            FROM    Concession_Request__c
        ]);
        
        System.assert(requests.size() == 1);
    }
    @isTest
    private static void concession_records_created(){
        
        SBQQ__QuoteLine__c record = [
            SELECT  Standard_Multiplier1__c 
            FROM    SBQQ__QuoteLine__c 
            LIMIT   1
        ];
        
        PageReference concessionPage = Page.ConcessionRequest;
        concessionPage.getParameters().put('Id', record.Id);
        
        List<Concession_Request__c> existingRequests = new List<Concession_Request__c>([
            SELECT  Id
            FROM    Concession_Request__c
        ]);
        
        System.assert(existingRequests.isEmpty());
        
        System.Test.setCurrentPage(concessionPage);
        System.Test.startTest();
            
            ConcessionRequest_ctr controller = new ConcessionRequest_ctr();
            controller.createRecord();  
        
            // constructor when existing records exist
            controller = new ConcessionRequest_ctr();
            
        System.Test.stopTest();
        
        System.assert(controller.isInitError == false);
        
        List<Concession_Request__c> requests = new List<Concession_Request__c>([
            SELECT  Id
            FROM    Concession_Request__c
        ]);
        
        System.assert(requests.size() == 1);
    }
    @isTest
    private static void concession_record_saved(){
        
        SBQQ__QuoteLine__c record = [
            SELECT  Standard_Multiplier1__c 
            FROM    SBQQ__QuoteLine__c 
            LIMIT   1
        ];
        
        PageReference concessionPage = Page.ConcessionRequest;
        concessionPage.getParameters().put('Id', record.Id);
        
        List<Concession_Request__c> existingRequests = new List<Concession_Request__c>([
            SELECT  Id
            FROM    Concession_Request__c
        ]);
        
        System.assert(existingRequests.isEmpty());
        
        System.Test.setCurrentPage(concessionPage);
        System.Test.startTest();
            
            ConcessionRequest_ctr controller = new ConcessionRequest_ctr();
            controller.createRecord();
            controller.record.emergency__c = false;
            controller.doSave();
            
        System.Test.stopTest();
        
        System.assertEquals( 
            record.Standard_Multiplier1__c ,
            controller.record.Standard_Multiplier__c,
            'concession request\'s Standard_Multiplier__c field not set to the quoteline\'s Standard_Multiplier1__c field.'
        );
    }
    @isTest
    private static void concession_record_submitted(){
        
        SBQQ__QuoteLine__c record = [
            SELECT  Standard_Multiplier1__c 
            FROM    SBQQ__QuoteLine__c 
            LIMIT   1
        ];
        
        PageReference concessionPage = Page.ConcessionRequest;
        concessionPage.getParameters().put('Id', record.Id);
        
        List<Concession_Request__c> existingRequests = new List<Concession_Request__c>([
            SELECT  Id
            FROM    Concession_Request__c
        ]);
        
        System.assert(existingRequests.isEmpty());
        
        System.Test.setCurrentPage(concessionPage);
        System.Test.startTest();
            
            ConcessionRequest_ctr controller = new ConcessionRequest_ctr();
            controller.createRecord();
            controller.record.emergency__c = false;
            controller.doSubmit();
            
        System.Test.stopTest();
    }
    @isTest
    private static void concession_record_recalled(){
        
        SBQQ__QuoteLine__c record = [
            SELECT  Standard_Multiplier1__c 
            FROM    SBQQ__QuoteLine__c 
            LIMIT   1
        ];
        
        PageReference concessionPage = Page.ConcessionRequest;
        concessionPage.getParameters().put('Id', record.Id);
        
        List<Concession_Request__c> existingRequests = new List<Concession_Request__c>([
            SELECT  Id
            FROM    Concession_Request__c
        ]);
        
        System.assert(existingRequests.isEmpty());
        
        System.Test.setCurrentPage(concessionPage);
        System.Test.startTest();
            
            ConcessionRequest_ctr controller = new ConcessionRequest_ctr();
            controller.createRecord();
            controller.record.emergency__c = false;
            controller.doRecall();
            
        System.Test.stopTest();
    }
}