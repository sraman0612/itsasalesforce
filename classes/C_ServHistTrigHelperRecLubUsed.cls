public class C_ServHistTrigHelperRecLubUsed 
{
    public static void servRollupAfterInsUpd(Map<Id,Service_History__c> incomingServs)
    {
        Set<Id> serialIds = new Set<Id>();
        serialIds = getSerialIds(incomingServs.values());
        
        if(serialIds!=null)
        {
            // get all the records in scope
            List<Asset> serials = new List<Asset>();
            List<Asset> serialsToUpdate = new List<Asset>();
            List<String> tmpParts = new List<String>();
            List<String> lstParts = new List<String>();
            Map<String,Id> mapPartToId = new Map<String,Id>(); // part lookup cache
            
            for(Asset a : [select Id,Name,(select Id,Name,CreatedDate,Lubricant__c,Serial_Number__c
                                           from Service_History_del__r
                                           order by Name desc
                                           limit 100)
                           from Asset
                           where Id in: serialIds]) 
            {
                for(Service_History__c s : a.Service_History_del__r)
                {
                    if(s.Lubricant__c!=null && s.Lubricant__c!='') 
                    {
                        // load part to cache
                        tmpParts.addAll(splitPartField((String)s.Lubricant__c));
                        // load serials to update at the end
                        serials.add(a);
                        break;
                    }
                }
            }
            
            // remove :T & :F from parts strings
            for(String s : tmpParts)
            {
                String tempAsset = s.substringBefore(':');
                lstParts.add(tempAsset);
            }
            
            // get the part records to cache
            List<Part__c> lstPartRecs = new List<Part__c>();
            lstPartRecs = [select Id, Name
                           from Part__c
                           where Name in : lstParts];
            
            // create the lookup cache for parts
            for(Part__c p : lstPartRecs)
            {
                mapPartToId.put(p.Name, p.Id);
            }
            
            if(serials.size()>0)
            {
                for(Asset a : serials)
                {
                    
                    
                    for(Service_History__c s : a.Service_History_del__r)
                    {
                        if(s.Lubricant__c!=null && s.Lubricant__c!='') 
                        {
                            serialsToUpdate.add(getLastLubricant(a, s, mapPartToId));
                            break;
                        }
                    }
                }
                
                Database.update(serialsToUpdate,false);
            }
        }
    }
    
    // separate the part fields by part
    public static List<String> splitPartField(String partsValue)
    {
        List<String> splitParts = partsValue.split(',\\s*');
        return splitParts;
    }
    
    public static void servRollupBeforeDel(Map<Id,Service_History__c> incomingServs)
    {
        System.debug('size ** '+incomingServs.values());
        Set<Id> serialIds = new Set<Id>();
        serialIds = getSerialIds(incomingServs.values());
        
        if(serialIds!=null)
        {
            // get all the records in scope
            List<Asset> serials = new List<Asset>();
            List<Asset> serialsToUpdate = new List<Asset>();
            List<String> tmpParts = new List<String>();
            List<String> lstParts = new List<String>();
            Map<String,Id> mapPartToId = new Map<String,Id>(); // part lookup cache
            
            for(Asset a : [select Id,Name,(select Id,Name,CreatedDate,Lubricant__c,Serial_Number__c
                                           from Service_History_del__r
                                           where Id not in: incomingServs.keySet()
                                           order by Name desc
                                           limit 100)
                           from Asset
                           where Id in: serialIds]) 
            {
                if(a.Service_History_del__r.size()>0)
                {
                    for(Service_History__c s : a.Service_History_del__r)
                    {
                        if(s.Lubricant__c!=null && s.Lubricant__c!='') 
                        {
                            // load part to cache
                            tmpParts.addAll(splitPartField((String)s.Lubricant__c));
                            // load serials to update at the end
                            serials.add(a);
                            break;
                        }
                    }
                }
                else
                {
                    serialsToUpdate.add(new Asset(Id=a.Id,Last_Lubricant_Used__c=null));
                }
                
            }
            
            // remove :T & :F from parts strings
            for(String s : tmpParts)
            {
                String tempAsset = s.substringBefore(':');
                lstParts.add(tempAsset);
            }
            
            // get the part records to cache
            List<Part__c> lstPartRecs = new List<Part__c>();
            lstPartRecs = [select Id, Name
                           from Part__c
                           where Name in : lstParts];
            
            // create the lookup cache for parts
            for(Part__c p : lstPartRecs)
            {
                mapPartToId.put(p.Name, p.Id);
            }
            
            
            
            
            for(Asset a : serials)
            {
                if(a.Service_History_del__r.size()>0)
                {
                    for(Service_History__c s : a.Service_History_del__r)
                    {
                        if(s.Lubricant__c!=null && s.Lubricant__c!='') 
                        {
                            serialsToUpdate.add(getLastLubricant(a, s, mapPartToId));
                            break;
                        }
                    }
                }
                
            }
            Database.update(serialsToUpdate,false);
            
        }
    }
    
    public static Asset getLastLubricant(Asset asst, Service_History__c serv, Map<String,Id> mapPartToId)
    {
        Asset a = new Asset(Id=asst.Id);
        
        List<String> splitParts = splitPartField((String)serv.Lubricant__c);
        
        for(Integer i=splitParts.size()-1;i>=0;i--)
        {
            if(splitParts[i].substringAfterLast(':')=='T')
            {
                String tempAsset = splitParts[i].substringBefore(':');
                a.Last_Lubricant_Used__c = mapPartToId.get(tempAsset);
                break; // if last part is found, break out of the loop
            }
        }
        return a;
    }
    
    public static Set<Id> getSerialIds(List<Service_History__c> lstServs)
    {
        Set<Id> serialIds = new Set<Id>();
        for(Service_History__c c : lstServs)
        {
            if(c.Serial_Number__c!=null)
            {
                serialIds.add(c.Serial_Number__c);
            }
        }
        return serialIds;
    }
    
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
}