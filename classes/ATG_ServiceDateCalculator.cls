public with sharing class ATG_ServiceDateCalculator {
    public ATG_ServiceDateCalculator() {
        
    }
    /*
The date derived from the Distributor’s service interval (if one exists). 
The formula should be Last Service Date + Serial Number > Service Interval (in days). This will be calculated at the Serial Number level. 
Stamp this date in a field on the Service Number.

This will live on the service contract, make sure we grab the most recent(active or based on date ranges)
create a new field for the service interval
*/
    /*
public static Date getDistributorServiceIntervalDate(Asset serialNumber, Service_Contract__c serviceContract){
//Assumptions: serviceContract contains the most recent service contract related to the serial number
//If there is a service interval on the service contract calculate the next service interval date
System.debug('ATG_ServiceDateCalculator:::getDistributorServiceIntervalDate: '+serialNumber);
if(serviceContract.Service_Interval__c != null){
if(serialNumber.Last_Logged_Service_Visit__c != null){
Integer serviceIntervalDays = (Integer)(365/Integer.valueOf(serviceContract.Service_Interval__c));
Date serviceIntervalDate = serialNumber.Last_Logged_Service_Visit__c.addDays(serviceIntervalDays);  //.addMonths(Integer.valueof(serviceContract.Service_Interval__c));
serialNumber.Service_Interval_Date__c = serviceIntervalDate;
return serviceIntervalDate;

}else{
//If there isnt a last logged service date, but there is a service interval
Integer serviceIntervalDays = (Integer)(365/Integer.valueOf(serviceContract.Service_Interval__c));
Date serviceIntervalDate = serviceContract.Contract_Start_Date__c.addDays(serviceIntervalDays);  //.addMonths(Integer.valueof(serviceContract.Service_Interval__c));
return serviceIntervalDate;
}
}else{
return null;
}
}
*/
    
    /*
An estimated date based on iConn data burn down rate. 
The formula should be CEIL(iConn Total Run Hours / (Today’s Date – Last Service Date)) = Daily Run Hours
FLOOR(iConn Hours To Service / Daily Run Hours) = Estimated Remaining Days
Last Service Date + Estimated Remaining Days. 
This should be run at the Serial Number level, as the iConn data abstracts away the part level calculations. 
Stamp this date in a field on the Service Number.
*/
    public static Date getIConnBurnDownDate(Asset serialNumber){
        System.debug('ATG_ServiceDateCalculator:::getIconnBurnDownDate: '+serialNumber);
        
        if(serialNumber.Last_Logged_Service_Visit__c != null && 
           serialNumber.iConn_c8y_H_total__c != null && 
           serialNumber.iConn_c8y_H_service__c != null){
               
               Date lastServiceDate = serialNumber.Last_Logged_Service_Visit__c;
               //Calculate the difference in hours between Current iConn and Last Service Visit
               Decimal totalRunHours = serialNumber.iConn_c8y_H_total__c - serialNumber.Hours__c;
               Decimal iConnHoursToService = serialNumber.iConn_c8y_H_service__c;
               Integer daysSinceLastService = lastServiceDate.daysBetween(Date.today());
               
               if(daysSinceLastService == 0){
                   return lastServiceDate.addDays(180);
               }
               
               Integer dailyRunHours = (Integer)(totalRunHours/daysSinceLastService).round(System.RoundingMode.CEILING);
               if(dailyRunHours > 0){
                   Date today = System.today();
                   Integer estimatedRemainingDays = (Integer)(iConnHoursToService/dailyRunHours).round(System.RoundingMode.CEILING);
                   Date calculatedNextServiceDate = today.addDays(estimatedRemainingDays);
                   
                   /* if(dailyRunHours == 1){
                       calculatedNextServiceDate = lastServiceDate.addDays(180);
                   } */
                   
                   return calculatedNextServiceDate;
               }
               else{
                   return Date.newInstance(1,1,1900);
               }
               
           }
        else{
            return Date.newInstance(1,1,1901);
        }
        
    }
    
    public static Date getAverageHoursBurnDownDate(Asset serialNumber){
        System.debug('ATG_ServiceDateCalculator:::getAverageHoursBurnDownDate: '+serialNumber);
        
        
        Date lastServiceDate;
        
        if(serialNumber.Last_Logged_Service_Visit__c != null){
            lastServiceDate = serialNumber.Last_Logged_Service_Visit__c;
        }
        else{
            if(serialNumber.Start_up_Date__c != null){
            	lastServiceDate = serialNumber.Start_up_Date__c;
            }
            else{
                return Date.newInstance(1,1,1902);
            }
        }
            
        //Calculate the difference in hours between Current iConn and Last Service Visit
        
        Decimal totalRunHours = serialNumber.Hours__c;
        Integer dailyRunHours;
        
        if(test.isRunningTest()){
            dailyRunHours = 8;
        }
        else{
            dailyRunHours = (Integer)(serialNumber.Avg_Yearly_Run_Hours_use__c/365).round(System.RoundingMode.CEILING);
        }
        
        if(dailyRunHours == 0){
            //uh oh
            return Date.newInstance(1,1,1903);
        }
        
        Integer daysForService = (Integer) (2000.0 / dailyRunHours).round(System.RoundingMode.CEILING);
        
        if(dailyRunHours > 0){
            Date calculatedNextServiceDate = lastServiceDate.addDays(daysForService);
            if(dailyRunHours == 1){
                lastServiceDate.addDays(180);
            }
            return calculatedNextServiceDate;
        }
        else{
            return Date.newInstance(1,1,1904);
        }
        
    }
    
    public static Map<Id,Service_Contract__c> mapServiceContractToSerialNumber(List<Service_Contract__c> serviceContracts){
        //Iterate through the service contracts and map them to their Serial Number (mapping only the latest Service Contract)
        Map<Id,Service_Contract__c> serviceContractMap = new Map<Id,Service_Contract__C>();
        for(Service_Contract__c serviceContract : serviceContracts){
            if(serviceContract.Serial_Number__c != null){
                //Serial Number already in map, need to compare the contract end dates
                if(serviceContractMap.containsKey(serviceContract.Serial_Number__c)){
                    //Get the service contract already in the map
                    Service_Contract__c tempServiceContract = serviceContractMap.get(serviceContract.Serial_Number__c);
                    //Compare the end dates
                    if(serviceContract.Contract_End_Date__c > tempServiceContract.Contract_End_Date__c){
                        serviceContractMap.put(serviceContract.Serial_Number__c,serviceContract);
                    }
                }else{
                    serviceContractMap.put(serviceContract.Serial_Number__c,serviceContract);
                }
            }
        }
        return serviceContractMap;
    }
    
    public static ReasonDueTuple calculateSoonestDate(Asset serialNumber){
        List<Date> serviceDates = new List<Date>();
        //Service interval date - Service_Interval_Date__c
        //iconn burn down date - iConn_Burn_Down_Service_Date__c
        //average daily hours - Average_Hours_Service_Date__c
        //warranty date - Expected_Next_Service_Date__c
        System.debug('ATG_ServiceDateCalculator:::CalculateSoonestDate: '+serialNumber);
        
        Map<Date, String> reasonDueMapping = new Map<Date,String>();
        
        
        if(serialNumber.Average_Hours_Service_Date__c != null){
            serviceDates.add(serialNumber.Average_Hours_Service_Date__c);
            reasonDueMapping.put(serialNumber.Average_Hours_Service_Date__c, 'Average Daily Run Hours');
        } 
        
        if(serialNumber.Expected_Next_Service_Date__c != null){
            serviceDates.add(serialNumber.Expected_Next_Service_Date__c);
            reasonDueMapping.put(serialNumber.Expected_Next_Service_Date__c, 'Warranty - 6 Month Service Requirement');
        } 
        
        if(serialNumber.iConn_Burn_Down_Service_Date__c != null){
            serviceDates.add(serialNumber.iConn_Burn_Down_Service_Date__c);
            reasonDueMapping.put(serialNumber.iConn_Burn_Down_Service_Date__c, 'iConn Hours to Service');
        }     
        /*
        if(serialNumber.Service_Interval_Date__c != null){
        serviceDates.add(serialNumber.Service_Interval_Date__c);
        reasonDueMapping.put(serialNumber.Expected_Next_Service_Date__c, 'Service Agreement Interval');
        } 
        */
        if(!serviceDates.isEmpty()){
            serviceDates.sort();
            if(serviceDates[0] < Date.Today()){
                return new ReasonDueTuple(Date.Today(), 'Warranty - 6 Month Service Requirement');
            }else{
                return new ReasonDueTuple(serviceDates[0], reasonDueMapping.get(serviceDates[0]));
            }
        }else{
            return null;
        }
        
    }
    
    public class ReasonDueTuple{
        public Date dueDate{get; private set;}
        public String reasonDue {get; private set;}
        
        public ReasonDueTuple(Date d, String r){
            this.dueDate = d;
            this.reasonDue = r;
        }
    }
}