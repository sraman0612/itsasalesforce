public with sharing class ProductLookupController {
    @AuraEnabled
    public static void appendLineItems(String quoteId, List<String> productIds, List<Integer> quantities) {
        Integer lineNumber = 1;
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__Number__c from SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = : quoteId order by SBQQ__Number__c desc];
        if (!quoteLines.isEmpty()) {
            lineNumber = Integer.valueOf(quoteLines.get(0).SBQQ__Number__c) + 1;
        }

        List<SBQQ__QuoteLine__c> toInsert = new List<SBQQ__QuoteLine__c>();

        for (Integer i = 0; i < productIds.size(); i++) {
            SBQQ__QuoteLine__c cur = new SBQQ__QuoteLine__c();
            toInsert.add(cur);

            cur.SBQQ__Quote__c = quoteId;
            cur.SBQQ__Product__c = productIds[i];
            cur.SBQQ__Quantity__c = quantities[i];
            cur.SBQQ__Number__c = lineNumber;
            lineNumber++;
        }

        if (toInsert.size() > 0) {
            insert toInsert;
        }
    }
}