@isTest
global class C_GDIiConnInventorySyncTest{
    
  @isTest
    static void posTest(){
        
        list<id> AstID = new list<id>();
        Asset newAst1 = new Asset(name='testing11',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c='359804083674640');
        // Asset newAst2 = new Asset(name='testing22',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c='');
        insert newAst1;
        AstID.add(newAst1.id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new C_GDIHttpMockTest());
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://example.com/example/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        C_GDIiConnInventorySync.iConnInventorySync(AstID);
        C_GDIiConnInventorySyncSchedule sh1 = new C_GDIiConnInventorySyncSchedule();
		String sch = '0 0 23 * * ?'; system.schedule('Test iConnInventorySync', sch, sh1); 
        Test.stopTest();

    }
   
     @isTest
    static void negTest(){
        
        list<id> AstID = new list<id>();
        Asset newAst1 = new Asset(name='testing11',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c='359804083674640');
        // Asset newAst2 = new Asset(name='testing22',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c='');
        insert newAst1;
        AstID.add(newAst1.id);
        Test.startTest();    
        C_GDIiConnInventorySync.iConnInventorySync(AstID);
        C_GDIiConnInventorySyncSchedule sh1 = new C_GDIiConnInventorySyncSchedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test iConnInventorySync', sch, sh1);
        Test.stopTest();
    }
    
    
}