public class ensxtx_CTRL_MaterialBOM
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_CTRL_MaterialBOM.class);

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getMaterialBOM(Map<String, Object> searchParams, Map<String, Object> pagingOptions)
    {
        logger.enterAura('getMaterialBOM', new Map<String, Object> {
            'searchParams' => searchParams,
            'pagingOptions' => pagingOptions
        });

        String results = '';
        Object responsePagingOptions = null;

        try
        {
            ensxtx_SBO_EnosixMaterialBOM_Search sbo = new ensxtx_SBO_EnosixMaterialBOM_Search();
            ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SC context = 
                new ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SC();

            context.SEARCHPARAMS.Material = (String) searchParams.get('material');
            context.SEARCHPARAMS.Plant = (String) searchParams.get('plant');
            context.SEARCHPARAMS.QuantityForKit = 1;
            context.SEARCHPARAMS.SalesOrganization = (String) searchParams.get('salesOrganization');
            context.SEARCHPARAMS.DistributionChannel = (String) searchParams.get('distributionChannel');

            ensxtx_UTIL_Aura.setSearchContextPagingOptions(context, pagingOptions);
            sbo.search(context);

            List<ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT> searchResults = context.result.getResults();
            if (searchResults.size() > 0) results = JSON.serialize(searchResults);
            responsePagingOptions = context.pagingOptions;
            ensxtx_UTIL_PageMessages.addFrameworkMessages(context.result.getMessages());
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }

        return ensxtx_UTIL_Aura.createResponse(results, responsePagingOptions);
    }
}