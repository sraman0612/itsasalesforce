public with sharing class serialNumberConfigController {

    @auraEnabled
    public static List<configTuple> getValues(String recordId){
        List<String> preFormatted = new List<String>();
        List<configTuple> result = new List<configTuple>();
        List<configTuple> preorder = new  List<configTuple>{null,null,null,null};
        Asset a = [SELECT char1__c, char2__c, char3__c, char4__c, char5__c, char6__c, char7__c, char8__c, char9__c, 
                   char10__c, char11__c, char12__c, char13__c, char14__c, char15__c, char16__c, char17__c, char18__c, char19__c, 
                   char20__c, char21__c, char22__c, char23__c, char24__c, char25__c,
                   char_Text1__c, char_Text2__c, char_Text3__c, char_Text4__c, char_Text5__c, char_Text6__c, 
                   char_Text7__c, char_Text8__c, char_Text9__c, char_Text10__c, char_Text11__c, char_Text12__c, 
                   char_Text13__c, char_Text14__c, char_Text15__c, char_Text16__c, char_Text17__c, char_Text18__c, 
                   char_Text19__c, char_Text20__c, char_Text21__c, char_Text22__c, char_Text23__c, char_Text24__c, 
                   char_Text25__c FROM Asset WHERE Id =: recordId
                  ];
        
        for(integer i = 1; i < 26; i++){
            String label = (String) a.get('char'+i.format()+'__c');
            if(String.isNotEmpty(label) && !label.containsIgnoreCase('Variants') && !label.containsIgnoreCase('Power/Stages')){
                
                if(label.containsIgnoreCase('hp') || label.containsIgnoreCase('horsepower') || label.containsIgnoreCase('kw') || label.containsIgnoreCase('kilowatt')){
                    preorder[0] = new configTuple(label, (String) a.get('char_Text'+i.format()+'__c'));
                }
                else if(label.equalsIgnoreCase('pressure')){
                    preorder[1] = new configTuple(label, (String) a.get('char_Text'+i.format()+'__c'));
                }
                else if(label.containsIgnoreCase('volt')){
                    preorder[2] = new configTuple(label, (String) a.get('char_Text'+i.format()+'__c'));
                }
                else if(label.containsIgnoreCase('phase')){
                    preorder[3] = new configTuple(label, (String) a.get('char_Text'+i.format()+'__c'));
                }
                else{
                    preFormatted.add(label + '|' + (String) a.get('char_Text'+i.format()+'__c'));
                	//result.add(new configTuple(label, (String) a.get('char_Text'+i.format()+'__c')));
                }
            }          
        }
        
        preFormatted.sort();
        
        for(integer i = 0; i < 4; i++){
            if(preorder[i] != null){
                result.add(preorder[i]);
            }
        }
        
        for(String s : preFormatted){
            String[] sTemp = s.split('[|]');
            result.add(new configTuple(sTemp[0],sTemp[1]));
        }
        
        return result;
    }
    
    
    public class configTuple{
        @auraEnabled
        public String Label {get; set;}
        @auraEnabled
        public String Val {get; set;}
        
        public configTuple(String l, String v){
            this.Label = l;
            this.Val = v;
        }
        
    }
}