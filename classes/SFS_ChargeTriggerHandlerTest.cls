@istest
public class SFS_ChargeTriggerHandlerTest {
    
    @istest public static void updateChargeCountTest() {    
        
        RecordType rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='Site Account - NA Air' AND SObjectType = 'Account'];
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        Double latitude = 48.7646475292616;
        Double longitude =-98.1987222787954;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Currency__c='USD';
        acct.RecordTypeId = accRecID; 
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.ShippingCity ='Montreat';
        acct.ShippingCountry ='United States';
        acct.ShippingGeocodeAccuracy ='Zip';
        acct.ShippingPostalCode='28757';
        acct.ShippingState='NC';
        acct.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct.IRIT_Customer_Number__c='1234';
        insert acct;
        Double latitude1 = 48.7646475292616;
        Double longitude2 =-98.1987222787954;
        
        Account acct2 = new Account();
        acct2.name = 'test account2';
        acct.Currency__c='USD';
        acct2.RecordTypeId = rtAcc.Id; 
        acct2.Bill_To_Account__c =acct.id;
        acct2.ShippingLatitude =   latitude1;
        acct2.ShippingLongitude =   longitude2;
        acct2.ShippingCity ='Montreat';
        acct2.ShippingCountry ='United States';
        acct2.ShippingGeocodeAccuracy ='Zip';
        acct2.ShippingPostalCode='28757';
        acct2.ShippingState='NC';
        acct2.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct2.IRIT_Customer_Number__c='1234';
        insert acct2;
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, true);
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct2.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct2.Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,false);
        
        Map<Id,CAP_IR_Charge__c> oldMap = new Map<id,CAP_IR_Charge__c>();
        Map<Id,CAP_IR_Charge__c> oldMap1 = new Map<id,CAP_IR_Charge__c>();
        String WOinvoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('WO Invoice').getRecordTypeId();
        //get invoice
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(2,false);
        inv[0].SFS_Work_Order__c = wo[0].Id;
        inv[0].RecordTypeId=WOinvoiceRecordTyeId;
        inv[1].RecordTypeId=WOinvoiceRecordTyeId;
        inv[0].SFS_Charge_Count__c = 0;        
        inv[1].SFS_Work_Order__c = wo[0].Id;
        inv[1].SFS_Charge_Count__c = 0;       
        insert inv;
        
        charge[0].SFS_Invoice__c = inv[0].id;
        charge[0].CAP_IR_Amount__c=500;
        insert charge;
        List<CAP_IR_Charge__c> charge2 = SFS_TestDataFactory.createCharge(1,wo[0].Id,false);
        charge2[0].CAP_IR_Amount__c=500;
        charge2[0].SFS_Invoice__c=inv[1].id;
        insert charge2;
        
        SFS_ChargeTriggerHandler.updateChargeCount(charge,null);
    }
    static testmethod void rollUpAmountToWorkOrderTest(){
        RecordType rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='Site Account - NA Air' AND SObjectType = 'Account'];
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        
        Double latitude = 48.7646475292616;
        Double longitude =-98.1987222787954;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Currency__c='USD';
        acct.RecordTypeId = accRecID; 
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.ShippingCity ='Montreat';
        acct.ShippingCountry ='United States';
        acct.ShippingGeocodeAccuracy ='Zip';
        acct.ShippingPostalCode='28757';
        acct.ShippingState='NC';
        acct.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct.IRIT_Customer_Number__c='1234';
        insert acct;
        Double latitude1 = 48.7646475292616;
        Double longitude2 =-98.1987222787954;
        
        Account acct2 = new Account(); 
        acct2.name = 'test account2';
        acct.Currency__c='USD';
        acct2.RecordTypeId = rtAcc.Id; 
        acct2.Bill_To_Account__c =acct.id;
        acct2.ShippingLatitude =   latitude1;
        acct2.ShippingLongitude =   longitude2;
        acct2.ShippingCity ='Montreat';
        acct2.ShippingCountry ='United States';
        acct2.ShippingGeocodeAccuracy ='Zip';
        acct2.ShippingPostalCode='28757';
        acct2.ShippingState='NC';
        acct2.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct2.IRIT_Customer_Number__c='1234';
        insert acct2;
        
        List<WorkOrder> woList = SFS_TestDataFactory.createWorkOrder(1,acct2.Id,null, null, null,true);
        List<CAP_IR_Charge__c> chargeList = SFS_TestDataFactory.createCharge(1, woList[0].Id,false);
        insert chargeList;
        Test.startTest();
        chargeList[0].SFS_Sell_Price__c=1500;
        update chargeList;
        chargeList[0].CAP_IR_Work_Order__c=null;
        update chargeList;
        //delete chargeList;
        Test.stopTest();
    }
    
    
    @istest public static void invoiceAmountRollUpSummaryTest() {  
        RecordType rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='Site Account - NA Air' AND SObjectType = 'Account'];
        RecordType rtInv = [Select Id, Name, SObjectType FROM RecordType where DeveloperName ='SFS_Agreement_Charge' AND SObjectType = 'CAP_IR_Charge__c'];
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        Double latitude = 48.7646475292616;
        Double longitude =-98.1987222787954;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Currency__c='USD';
        acct.RecordTypeId =accRecID; 
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.ShippingCity ='Montreat';
        acct.ShippingCountry ='United States';
        acct.ShippingGeocodeAccuracy ='Zip';
        acct.ShippingPostalCode='28757';
        acct.ShippingState='NC';
        acct.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct.IRIT_Customer_Number__c='1234';
        insert acct;
        Double latitude1 = 48.7646475292616;
        Double longitude2 =-98.1987222787954;
        
        Account acct2 = new Account();
        acct2.name = 'test account2';
        acct.Currency__c='USD';
        acct2.RecordTypeId = rtAcc.Id; 
        acct2.Bill_To_Account__c =acct.id;
        acct2.ShippingLatitude =   latitude1;
        acct2.ShippingLongitude =   longitude2;
        acct2.ShippingCity ='Montreat';
        acct2.ShippingCountry ='United States';
        acct2.ShippingGeocodeAccuracy ='Zip';
        acct2.ShippingPostalCode='28757';
        acct2.ShippingState='NC';
        acct2.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct2.IRIT_Customer_Number__c='1234';
        insert acct2;
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, true);
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct2.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct2.Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,false);
        charge[0].RecordTypeId=rtInv.id;
        insert charge;
        List<CAP_IR_Charge__c> charge2 = SFS_TestDataFactory.createCharge(1,wo[0].Id,false);
        charge2[0].RecordTypeId=rtInv.id;
        charge2[0].CAP_IR_Amount__c=500;
        insert charge2;
        String SAinvoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Agreement Invoice').getRecordTypeId();
        String WOinvoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('WO Invoice').getRecordTypeId();
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(2,false);
        inv[0].SFS_Work_Order__c = wo[0].Id;
        inv[0].RecordTypeId=WOinvoiceRecordTyeId;
        inv[1].RecordTypeId=SAinvoiceRecordTyeId;
        inv[1].SFS_Service_Agreement__c = sc[0].Id;
        inv[0].SFS_Billing_Period_Start_Date__c = date.Today();
        inv[1].SFS_Billing_Period_Start_Date__c = date.Today();
        inv[0].SFS_Billing_Period_End_Date__c = date.Today().addDays(30);
        inv[1].SFS_Billing_Period_End_Date__c = date.Today().addDays(30);
        insert inv;
        
        charge[0].SFS_Invoice__c=inv[1].Id;
        update charge;
        Map<Id,CAP_IR_Charge__c> invLineMap = new Map<id,CAP_IR_Charge__c>();
        Map<Id,CAP_IR_Charge__c> oldMap = new Map<id,CAP_IR_Charge__c>();
        oldMap.put(charge2[0].id,charge2[0]);
        
        SFS_ChargeTriggerHandler.invoiceAmountRollUpSummary(charge, invLineMap);
        SFS_ChargeTriggerHandler.invoiceAmountRollUpSummary(charge, oldMap);
        delete charge;     
    }
    
    @istest public static void assignChargeAmountTest() {  
        RecordType rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='Site Account - NA Air' AND SObjectType = 'Account'];
        RecordType rtInv = [Select Id, Name, SObjectType FROM RecordType where DeveloperName ='SFS_WO_Charge' AND SObjectType = 'CAP_IR_Charge__c'];
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        Double latitude = 48.7646475292616;
        Double longitude =-98.1987222787954;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Currency__c='USD';
        acct.RecordTypeId = accRecID; 
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.ShippingCity ='Montreat';
        acct.ShippingCountry ='United States';
        acct.ShippingGeocodeAccuracy ='Zip';
        acct.ShippingPostalCode='28757';
        acct.ShippingState='NC';
        acct.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct.IRIT_Customer_Number__c='1234';
        insert acct;
        Double latitude1 = 48.7646475292616;
        Double longitude2 =-98.1987222787954;
        
        Account acct2 = new Account();        
        acct2.name = 'test account2';
        acct.Currency__c='USD';
        acct2.RecordTypeId = rtAcc.Id; 
        acct2.Bill_To_Account__c =acct.id;
        acct2.ShippingLatitude =   latitude1;
        acct2.ShippingLongitude =   longitude2;
        acct2.ShippingCity ='Montreat';
        acct2.ShippingCountry ='United States';
        acct2.ShippingGeocodeAccuracy ='Zip';
        acct2.ShippingPostalCode='28757';
        acct2.ShippingState='NC';
        acct2.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct2.IRIT_Customer_Number__c='1234';
        insert acct2;
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, false);
        div[0].SFS_Expense_margin__c=20;
        insert div;
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct2.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct2.Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,false);
        charge[0].RecordTypeId=rtInv.id;
        charge[0].SFS_Charge_Type__c='Time';
        insert charge;  
        
        List<CAP_IR_Charge__c> charge2 = SFS_TestDataFactory.createCharge(1,wo[0].Id,false);
        charge2[0].RecordTypeId=rtInv.id;
        charge2[0].CAP_IR_Amount__c=500;
        insert charge2;
        
        
        String WOinvoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('WO Invoice').getRecordTypeId();
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(2,false);
        inv[0].RecordTypeId=WOinvoiceRecordTyeId;
        inv[1].RecordTypeId=WOinvoiceRecordTyeId;
        inv[0].SFS_Work_Order__c = wo[0].Id;
        inv[0].SFS_Charge_Count__c=0;
        inv[1].SFS_Work_Order__c = wo[0].Id;
        inv[0].SFS_Charge_Count__c=0;
        insert inv;
        
        charge[0].SFS_Invoice__c=inv[0].id;
        charge[0].SFS_Sell_Price__c=200;
        charge[0].CAP_IR_Amount__c=800;
        update charge;
        
        
        charge2[0].SFS_Invoice__c=inv[1].id;
        charge2[0].SFS_Sell_Price__c=400;
        update charge2;
        
        Map<Id,CAP_IR_Charge__c> invLineMap = new Map<id,CAP_IR_Charge__c>();
        Map<Id,CAP_IR_Charge__c> oldMap = new Map<id,CAP_IR_Charge__c>();
        oldMap.put(charge2[0].id,charge2[0]);
        Test.startTest();  
        SFS_ChargeTriggerHandler.assignChargeAmount(charge, invLineMap);
        SFS_ChargeTriggerHandler.assignChargeAmount(charge, oldMap);
        SFS_ChargeTriggerHandler.assignSellPrice(charge, invLineMap);
        SFS_ChargeTriggerHandler.assignSellPrice(charge, oldMap);
        Test.stopTest();       
    }
 @isTest
    static void testAfterInsert() {
       
        Invoice__c invoice = new Invoice__c(
            SFS_Invoice_Type__c = 'Prepaid',
            SFS_Invoice_Description__c = 'Test'
        );
        insert invoice;
        
        SFS_Invoice_Line_Item__c lineItem = new SFS_Invoice_Line_Item__c(
            SFS_Invoice__c = invoice.Id
        );
        insert lineItem;

        List<CAP_IR_Charge__c> testCharges = new List<CAP_IR_Charge__c>();
        for (Integer i = 0; i < 5; i++) {
            CAP_IR_Charge__c charge = new CAP_IR_Charge__c(
                CAP_IR_Amount__c = 110,
                CAP_IR_Date__c = System.Today(),
                CAP_IR_Status__c = 'New',
                CAP_IR_Description__c = 'Test' + i,
                Invoice_Line_Item__c = lineItem.Id
            );
            testCharges.add(charge);
        }
        Test.startTest();
        insert testCharges;
        Test.stopTest();

        
       // SFS_Invoice_Line_Item__c updatedLineItem = [SELECT Id, Number_of_Charges__c FROM SFS_Invoice_Line_Item__c WHERE Id = :lineItem.Id];
      //  Integer expectedChargeCount = testCharges.size();
        //System.assertEquals(expectedChargeCount, updatedLineItem.Number_of_Charges__c, 'Number_of_Charges__c field on SFS_Invoice_Line_Item__c should match the count of related CAP_IR_Charge__c records after insert.');
    }

    @isTest
    static void testAfterUpdate() {
       
        Invoice__c invoice = new Invoice__c(
            SFS_Invoice_Type__c = 'Prepaid',
            SFS_Invoice_Description__c = 'Test'
        );
        insert invoice;
        
        SFS_Invoice_Line_Item__c lineItem = new SFS_Invoice_Line_Item__c(
            SFS_Invoice__c = invoice.Id
        );
        insert lineItem;

        List<CAP_IR_Charge__c> testCharges = new List<CAP_IR_Charge__c>();
        for (Integer i = 0; i < 5; i++) {
            CAP_IR_Charge__c charge = new CAP_IR_Charge__c(
                CAP_IR_Amount__c = 110,
                CAP_IR_Date__c = System.Today(),
                CAP_IR_Status__c = 'New',
                CAP_IR_Description__c = 'Test' + i,
                Invoice_Line_Item__c = lineItem.Id
            );
            testCharges.add(charge);
        }
        insert testCharges;
        testCharges[0].CAP_IR_Amount__c = 200;

        Test.startTest();
        update testCharges;
        Test.stopTest();

       
     //   SFS_Invoice_Line_Item__c updatedLineItem = [SELECT Id, Number_of_Charges__c FROM SFS_Invoice_Line_Item__c WHERE Id = :lineItem.Id];
      //  Integer expectedChargeCount = testCharges.size();
       // System.assertEquals(expectedChargeCount, updatedLineItem.Number_of_Charges__c, 'Number_of_Charges__c field on SFS_Invoice_Line_Item__c should match the count of related CAP_IR_Charge__c records after update.');
    }
  
    @isTest
    static void testAfterDelete() {
       
        Invoice__c invoice = new Invoice__c(
            SFS_Invoice_Type__c = 'Prepaid',
            SFS_Invoice_Description__c = 'Test'
        );
        insert invoice;
        SFS_Invoice_Line_Item__c lineItem = new SFS_Invoice_Line_Item__c(
            SFS_Invoice__c = invoice.Id
        );
        insert lineItem;

        List<CAP_IR_Charge__c> testCharges = new List<CAP_IR_Charge__c>();
        for (Integer i = 0; i < 5; i++) {
            CAP_IR_Charge__c charge = new CAP_IR_Charge__c(
                CAP_IR_Amount__c = 110,
                CAP_IR_Date__c = System.Today(),
                CAP_IR_Status__c = 'New',
                CAP_IR_Description__c = 'Test' + i,
                Invoice_Line_Item__c = lineItem.Id
            );
            testCharges.add(charge);
        }
        insert testCharges;

        Test.startTest();
        delete testCharges;
        Test.stopTest();

     //   SFS_Invoice_Line_Item__c updatedLineItem = [SELECT Id, Number_of_Charges__c FROM SFS_Invoice_Line_Item__c WHERE Id = :lineItem.Id];
       // System.assertEquals(0, updatedLineItem.Number_of_Charges__c, 'Number_of_Charges__c field on SFS_Invoice_Line_Item__c should be reset to 0 after delete.');
    }

    @isTest
    static void testAfterUndelete() {
        // Create test data for CAP_IR_Charge__c records
        Invoice__c invoice = new Invoice__c(
            SFS_Invoice_Type__c = 'Prepaid',
            SFS_Invoice_Description__c = 'Test'
        );
        insert invoice;
     
        SFS_Invoice_Line_Item__c lineItem = new SFS_Invoice_Line_Item__c(
            SFS_Invoice__c = invoice.Id
        );
        insert lineItem;

        List<CAP_IR_Charge__c> testCharges = new List<CAP_IR_Charge__c>();
        for (Integer i = 0; i < 5; i++) {
            CAP_IR_Charge__c charge = new CAP_IR_Charge__c(
                CAP_IR_Amount__c = 110,
                CAP_IR_Date__c = System.Today(),
                CAP_IR_Status__c = 'New',
                CAP_IR_Description__c = 'Test' + i,
                Invoice_Line_Item__c = lineItem.Id
            );
            testCharges.add(charge);
        }
        insert testCharges;

        delete testCharges;
      
        Test.startTest();
        Database.undelete(testCharges);
        Test.stopTest();
      //  SFS_Invoice_Line_Item__c updatedLineItem = [SELECT Id, Number_of_Charges__c FROM SFS_Invoice_Line_Item__c WHERE Id = :lineItem.Id];
       // Integer expectedChargeCount = testCharges.size();
     //   System.assertEquals(expectedChargeCount, updatedLineItem.Number_of_Charges__c, 'Number_of_Charges__c field on SFS_Invoice_Line_Item__c should match the count of related CAP_IR_Charge__c records after undelete.');
    }

    
}