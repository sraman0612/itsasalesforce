@isTest
public with sharing class DistributorWarehouseSearchControllerTest {

    @TestSetup static void createData(){ 

        Account distributor = new Account(
            Name = 'Test Account'
        );
        insert distributor;

        Asset machine = new Asset(
            AccountId = distributor.Id,
            Name = 'Asset',
            Dchl__c = 'CM',
            Hide_in_Distributor_Warehouse__c = false
        );
        insert machine;

        Asset_Share__c assetShare = new Asset_Share__c(
            Serial_Number__c = machine.Id
        );
        insert assetShare;
    }

    @isTest static void globalUser_init(){
        User profile =[SELECT ProfileId FROM  User WHERE Profile_Name__c=:'Customer Service' LIMIT 1];
        User distributor = [
            SELECT  Id
            FROM    User
            WHERE   ProfileId = :profile.ProfileId
                AND IsActive = TRUE
        ][0];

        Account acc = [select Id from Account limit 1];

        System.runAs(distributor){

            System.Test.startTest();
                DistributorWarehouseSearchController.ViewData viewState = DistributorWarehouseSearchController.doInit(acc.Id,acc.Id,'DI');
            System.Test.stopTest();

            System.assertEquals(acc.Id, viewState.activeAccountId, 'Incorrect account Id after initilization');
        }
    }

    @isTest static void init(){


        Account acc = [select Id from Account limit 1];

        System.Test.startTest();
            DistributorWarehouseSearchController.ViewData viewState = DistributorWarehouseSearchController.doInit(acc.Id,acc.Id,'DI');
        System.Test.stopTest();

        System.assertEquals(acc.Id, viewState.activeAccountId, 'Incorrect account Id after initilization');
    }

    @isTest static void removeFromDW(){

        List<Asset> assetList = new List<Asset>();

        for(Asset machine : [select Id from Asset where Hide_in_Distributor_Warehouse__c = TRUE]){

            machine.Hide_in_Distributor_Warehouse__c = False;
            assetList.add(machine);
        }

        update assetList;

        for(Asset machine : [select Id from Asset where Hide_in_Distributor_Warehouse__c = True]){
            System.assert(false, 'records matching the query should not exist');
        }

        List<Asset> assets = new List<Asset>([select Id from Asset]);
        List<Id> machineIds = new List<Id>();

        for(Asset machine : [select Id from Asset where Hide_in_Distributor_Warehouse__c = False]){
            machineIds.add(machine.Id);
        }

        System.assertNotEquals(0, machineIds.size());

        System.Test.startTest();
            DistributorWarehouseSearchController.removeFromDW(machineIds);
        System.Test.stopTest();

        machineIds = new List<Id>();

        for(Asset machine : [select Id from Asset where Hide_in_Distributor_Warehouse__c = True]){
            machineIds.add(machine.Id);
        }

        System.assert(machineIds.size() > 0, 'Machines not removed from Distributor Warehouse');
    }

    @isTest static void search(){

        System.Test.startTest();

            DistributorWarehouseSearchController.SearchResult result =
                DistributorWarehouseSearchController.search(
                    10,
                    1,
                    'modelNo',
                    'desc',
                    5,
                    5,
                    5,
                    'DI'
                );

        System.Test.stopTest();

        System.assertNotEquals(NULL, result.resultItems);

    }

    @isTest static void dataStructures(){

        DistributorWarehouseSearchController.AccountOption option =
            new DistributorWarehouseSearchController.AccountOption('ID','LABEL',FALSE);

        System.assertEquals('ID', option.value);
        System.assertEquals('LABEL', option.label);
        System.assertEquals(FALSE, option.disabled);

        String jsonAssets = JSON.serialize([select Id from Asset]);

        List<Asset> assets = (List<Asset>)JSON.deserialize(jsonAssets, List<Asset>.class);

        for(Asset tmp : assets){
            System.assertEquals(NULL, tmp.DelDate__c);
        }

        DistributorWarehouseSearchController.MyInventory inventory =
            new DistributorWarehouseSearchController.MyInventory(assets);

        System.assertNotEquals(NULL, inventory.inventoryItems);
        System.assertEquals(1, inventory.inventoryItems.size());
        System.assertEquals('action:close', inventory.inventoryItems[0].iconName);

        for(Asset tmp : assets){
            tmp.DelDate__c = System.today();
        }

        inventory = new DistributorWarehouseSearchController.MyInventory(assets);

        System.assertNotEquals(NULL, inventory.inventoryItems);
        System.assertEquals(1, inventory.inventoryItems.size());
        System.assertEquals('action:approval', inventory.inventoryItems[0].iconName);

        String assetShareSTR = JSON.serialize([select Id from Asset_Share__c]);

        List<Asset_Share__c> assetShares = (List<Asset_Share__c>)JSON.deserialize(assetShareSTR, List<Asset_Share__c>.class);

        DistributorWarehouseSearchController.SearchResult searchResults =
            new DistributorWarehouseSearchController.SearchResult(assetShares, 100, 25);

        System.assertEquals(100, searchResults.totalRowCount);
        System.assertEquals(4, searchResults.totalPageCount);
    }
}