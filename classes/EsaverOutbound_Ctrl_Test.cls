@isTest
public class EsaverOutbound_Ctrl_Test {

    @TestSetup
    static void createData() {

        Account acc = new Account(
            Name = 'TEST ACCOUNT',
            AccountNumber = '12345678',
            Account_Number__c='1234'
        );

        insert acc;

        Id pricebookId = Test.getStandardPricebookId();

        Opportunity opp = new Opportunity(
            AccountId = acc.Id,
            Name = 'oppName',
            Amount = 213230,
            CloseDate = Date.today(),
            StageName = 'Stage 1 - Qualified Lead',
            Pricebook2Id = pricebookId,
            LeadSource = 'Email',
            Probability = 25,
            TotalOpportunityQuantity = 27
        );

        insert opp;

        insert new SBQQ__Quote__c(
            SBQQ__Account__c = acc.Id,
            End_Customer_Account__c = acc.Id,
            SBQQ__Distributor__c = acc.Id,
            SBQQ__Opportunity2__c = opp.Id,
            Distribution_Channel__c = 'CM'
        );
    }

    @isTest
    static void getAccessToken() {

        PageReference testPage = Page.EsaverOutbound;

        System.Test.setCurrentPage(testPage);
        EsaverOutbound_Ctrl_Mock calloutMock = new EsaverOutbound_Ctrl_Mock(
            EsaverOutbound_Ctrl_Mock.ESAVER_REDIRECT_STATUS_MODE.SUCCESS
        );

        System.Test.setMock(HttpCalloutMock.class, calloutMock);
        System.Test.startTest();
            EsaverOutbound_Ctrl controller = new EsaverOutbound_Ctrl();
            String token = controller.getAccessToken();
        System.Test.stopTest();
    }


    @isTest
    static void successRedirect() {

        PageReference testPage = Page.EsaverOutbound;
        testPage.getParameters().put('Id', [SELECT Id FROM SBQQ__Quote__c][0].Id);

        System.Test.setCurrentPage(testPage);
        EsaverOutbound_Ctrl_Mock calloutMock = new EsaverOutbound_Ctrl_Mock(
            EsaverOutbound_Ctrl_Mock.ESAVER_REDIRECT_STATUS_MODE.SUCCESS
        );

        System.Test.setMock(HttpCalloutMock.class, calloutMock);
        System.Test.startTest();
            EsaverOutbound_Ctrl controller = new EsaverOutbound_Ctrl();
            PageReference esaverPage = controller.doRedirect();
        System.Test.stopTest();
    }

    @IsTest
    static void missingInfo(){

        Id quoteId = [SELECT Id FROM SBQQ__Quote__c][0].Id;

        update new SBQQ__Quote__c(
            Id = quoteId,
            End_Customer_Account__c = null,
            SBQQ__Distributor__c = null
        );

        PageReference testPage = Page.EsaverOutbound;
        testPage.getParameters().put('Id', quoteId);

        System.Test.setCurrentPage(testPage);
        EsaverOutbound_Ctrl_Mock calloutMock = new EsaverOutbound_Ctrl_Mock(
            EsaverOutbound_Ctrl_Mock.ESAVER_REDIRECT_STATUS_MODE.SUCCESS
        );

        System.Test.setMock(HttpCalloutMock.class, calloutMock);
        System.Test.startTest();
            EsaverOutbound_Ctrl controller = new EsaverOutbound_Ctrl();
            PageReference esaverPage = controller.doRedirect();
        System.Test.stopTest();
    }

    @isTest
    static void redirectBadRequest() {

        PageReference testPage = Page.EsaverOutbound;
        testPage.getParameters().put('Id', [SELECT Id FROM SBQQ__Quote__c][0].Id);

        System.Test.setCurrentPage(testPage);
        EsaverOutbound_Ctrl_Mock calloutMock = new EsaverOutbound_Ctrl_Mock(
            EsaverOutbound_Ctrl_Mock.ESAVER_REDIRECT_STATUS_MODE.BAD_REQUEST
        );

        System.Test.setMock(HttpCalloutMock.class, calloutMock);

        System.Test.startTest();
            EsaverOutbound_Ctrl controller = new EsaverOutbound_Ctrl();
            PageReference esaverPage = controller.doRedirect();
        System.Test.stopTest();
    }


    @isTest
    static void redirectCaughtServerError() {

        PageReference testPage = Page.EsaverOutbound;
        testPage.getParameters().put('Id', [SELECT Id FROM SBQQ__Quote__c][0].Id);

        System.Test.setCurrentPage(testPage);

        EsaverOutbound_Ctrl_Mock calloutMock = new EsaverOutbound_Ctrl_Mock(
            EsaverOutbound_Ctrl_Mock.ESAVER_REDIRECT_STATUS_MODE.ESAVER_SERVER_ERROR_CAUGHT
        );

        System.Test.setMock(HttpCalloutMock.class, calloutMock);

        System.Test.startTest();
            EsaverOutbound_Ctrl controller = new EsaverOutbound_Ctrl();
            PageReference esaverPage = controller.doRedirect();
        System.Test.stopTest();
    }
}