/*=========================================================================================================
* @author : Ponneri Naresh ,Capgemini
* @date : 21/9/2022
* @description: Credit Card Authorization Cybersource
==============================================================================================================*/
global class SFS_CreditCardAuthorizationCybersource {
    global class creditCrad{
        @InvocableVariable
        public ID invoiceId;
        @InvocableVariable
        public string parenrWoId;
        @InvocableVariable
        public string parenrSgId;
    }
    @invocableMethod(label ='Credit Card Authorization Cybersource' description = 'Send to CPI' Category = 'Invoice')
    global static void Run(List<creditCrad> creditcradDetails){
        system.debug('----------------'+creditcradDetails[0].invoiceId);
        List<String> recordIds=new List<String>();
        try{
            Invoice__c inv=[Select id,SFS_Invoice_Amount__c,SFS_Total_InvLI_Amount__c,SFS_Credit_Card_Authorization_Status__c,
                            SFS_Authorization_Number__c,SFS_Authorization_Date__c from Invoice__c where id=:creditcradDetails[0].invoiceId];
            system.debug('------Invoice----'+inv);
            Id creditCardId;
            String parentId; String parentObject;
            if(creditcradDetails[0].parenrSgId!=null && creditcradDetails[0].parenrSgId!=''){
                ServiceContract SC=[select id,SFS_External_Id__c,SFS_Account_Credit_Card__r.id from ServiceContract Where id=:creditcradDetails[0].parenrSgId];
                system.debug('-----ServiceContract----'+sc);
                parentId=SC.SFS_External_Id__c;
                parentObject='ServiceAgreement';
                if(SC!=null)
                    creditCardId=SC.SFS_Account_Credit_Card__r.id;
                system.debug('-----Creditcard----'+creditCardId);
            }
            if(creditcradDetails[0].parenrWoId!=null && creditcradDetails[0].parenrWoId!=''){
                WorkOrder WO=[select id,SFS_External_Id__c,SFS_Account_Credit_Card__r.id from WorkOrder Where id=:creditcradDetails[0].parenrWoId];
                parentId=WO.SFS_External_Id__c;
                parentObject='WorkOrder';
                if(WO!=null)
                    creditCardId=WO.SFS_Account_Credit_Card__r.id;
            }
            System.debug('-----CreditCardId-----'+creditCardId);
            System.debug('-----ParentId-----'+parentId);
            recordIds.add(creditCardId);
            recordIds.add(parentId);
            recordIds.add(inv.id);
            IF(creditCardId!=NULL){
                SFS_CreditCardAuthorizationCybersource.Call_API(recordIds,parentObject);
            }
        }
        catch(exception e){
              System.debug('Error Message'+e.getMessage());
        }
    }
    
    @future(callout=true)
    public static void Call_API(List<String> recordIds,string ParentObject){
        String response;
        Invoice__c inv = new Invoice__c ();
        
        try{
            inv=[Select id,SFS_Invoice_Amount__c,SFS_Total_InvLI_Amount__c,SFS_Credit_Card_Authorization_Status__c,SFS_SubscriptionID__c,SFS_Request_Token__c,
                        SFS_Authorization_Number__c,SFS_Authorization_Error_Message__c,SFS_Authorization_Date__c,SFS_Expiration_Month__c,SFS_Expiration_Year__c from Invoice__c where id=:recordIds[2]];
            double invAmount = (ParentObject=='ServiceAgreement')?inv.SFS_Invoice_Amount__c:inv.SFS_Total_InvLI_Amount__c;

            Account_Credit_Card__c aCreditCrad=[Select id,CurrencyIsoCode,Account__c,SFS_Active__c,Bluefin_Key__c,Expiration_Date__c,Last_4_Digits__c,
                                            Fusion_OBM_Routing_Code__c,First_Name__c,Name, Last_Name__c,CG_Org_Channel__c,RecordTypeName__c,Siebel_ID__c 
                                            from Account_Credit_Card__c where id=:recordIds[0]];
            System.debug('-----AccountCreditCrad--------'+aCreditCrad);
       	     response=PaymentGatewayCalloutClass.calloutToPaymentGateway(aCreditCrad.Name,recordIds[1],aCreditCrad.Last_Name__c,aCreditCrad.Last_4_Digits__c,aCreditCrad.Bluefin_Key__c,
                                                                          aCreditCrad.Expiration_Date__c,aCreditCrad.CurrencyIsoCode,string.valueOf(invAmount));
        
            
         System.debug('-----Response------'+response);
        List<String> responseData = response.split('~~');
            
        inv.SFS_Credit_Card_Authorization_Status__c=responseData[2];
        inv.SFS_Authorization_Number__c=responseData[4];
        inv.SFS_Authorization_Date__c=Date.valueOf(responseData[5]);
        inv.SFS_Authorization_Error_Message__c=responseData[16];
        inv.SFS_Authorization_Submitted_By__c = UserInfo.getUserId();
        inv.SFS_Expiration_Month__c = responseData[20];
        inv.SFS_Expiration_Year__c = responseData[21];    
        inv.SFS_SubscriptionID__c = responseData[11];
        inv.SFS_Request_Token__c =  responseData[14];   
        inv.SFS_Charge_Count__c=0;
        Update inv;
        system.debug('-----Updated Invoice Is-------'+responseData[15]);
        }
        Catch(Exception e){
            if(test.isRunningTest()){
                response = '6643663644406561604012~~ACCEPT~~Axj/7wSTaCdatPL+M4GsABsZEjMmE1xMjWnMiEojkXtW3oBURyL2rb1pA6cQI34ZNJMq6PSXrLhok2gnWrTy/jOBrAAA0hkB~~831000~~2022-09-28T11:59:24Z~~DF20M8LFZ9HB~~16150703802094~~1202209281159241013372781~~1~~0110322000000E1000020000000000000100000928115924252440444632304D384C465A3948423833313030303030000159004400223134573031363135303730333830323039344730363400103232415050524F56414C0006564943524120~~9909000366737875~~ACCEPT~~6643663654046562304012~~ACCEPT~~100~~null~~2022-09-28T11:59:25.474Z~~412493XXXXXX9990~~12~~2022~~6~~94~~USD~~credit card~~0~~CURRENT~~0~~';
            }
             inv=[Select id,SFS_Invoice_Amount__c,SFS_Total_InvLI_Amount__c,SFS_Authorization_Error_Message__c,SFS_Credit_Card_Authorization_Status__c,
                        SFS_Authorization_Number__c,SFS_Authorization_Date__c from Invoice__c where id=:recordIds[2]];
            List<String> responseData = new List<String>();
            if(response!=null) responseData = response.split('~~');
             inv.SFS_Authorization_Error_Message__c = '';
            For(String s : responseData){
               // System.debug('CC Response: ' + s + '@@COUNT: ' + count);
               
                inv.SFS_Authorization_Error_Message__c = inv.SFS_Authorization_Error_Message__c + ' ' + s;
            }
            inv.SFS_Authorization_Error_Message__c = inv.SFS_Authorization_Error_Message__c + '\nTime of Submission: '+ String.ValueOf(DateTime.now());
            inv.SFS_Authorization_Error_Message__c = inv.SFS_Authorization_Error_Message__c.replace('null', ' ');
            inv.SFS_Credit_Card_Authorization_Status__c = 'Error';
            inv.SFS_Authorization_Date__c = null;
            inv.SFS_Authorization_Number__c = '';
            inv.SFS_Authorization_Submitted_By__c = UserInfo.getUserId();
            update inv;
            System.debug('Error Message'+e.getMessage());
            
        }
    }
}