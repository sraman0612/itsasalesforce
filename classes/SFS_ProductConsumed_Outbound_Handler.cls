/*=========================================================================================================
* @author : Ryan Reddish, Capgemini
* @date : 25/02/2022
* @description: XXINV1951 Parts Used Transformation class.
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
Ryan Reddish		M001					05/02/2022		Refactored and Bulkified code.
Ryan Reddish 		M002					06/20/2022		Added Synchronous error capture.
Ryan Reddish		M003					11/01/2022		Added logic to escape special characters before adding to XML.
Jamecia Moore		M004					12/03/2022		Added the field SFS_Project_Number__c to the query
Srikant P           M005                    04/04/2023      Product Consumed Ack Logic
============================================================================================================*/
public class SFS_ProductConsumed_Outbound_Handler {
	 @invocableMethod(label = 'Outbound ProductConsumed' description = 'Send to CPI' Category = 'ProductConsumed')
    public static void Run(List<Id> woliId){
        //Main Method
        String xmlString = '';
        Map<Double,String> pcXmlMap = new Map<Double,String>();
        Map<Double,String> CLIXMLMap = new Map<Double,String>();
        //system.debug('@newWorkOrderId'+newWorkOrderId);        
        ProcessData(woliId, pcXmlMap,xmlString);
    }
    public static void ProcessData(List<Id> woliId, Map<Double, String> pcXmlMap, String xmlString){
      
        SFS_Parts_Used_XML_Structure__mdt[] partsUsedStruct = [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                         FROM SFS_Parts_Used_XML_Structure__Mdt Order By SFS_Node_Order__c asc]; 
        
        
        SFS_Parts_Used_XML_Structure__mdt st = SFS_Parts_Used_XML_Structure__mdt.getInstance('Reference');
        SFS_Parts_Used_XML_Structure__mdt et = SFS_Parts_Used_XML_Structure__mdt.getInstance('Close_Tags');
        String IdList = '';
        String startTag = st.SFS_XML_Full_Name__c;
        String endTag = et.SFS_XML_Full_Name__c;
        List<ProductConsumed> prod = new List<ProductConsumed>();
        try{
        prod = [SELECT Id, SFS_Project_Number__c, QuantityConsumed,WorkOrderId,ProductConsumedNumber,sfs_serial_number__c,sfs_oracle_created_date__c,
                                       product2.ProductCode,SFS_Division_Org_Code__c,SFS_Integration_Status__c, SFS_Integration_Response__c,SFS_Locator_Code__c,SFS_Subinventory_Code__c, 
                                       WorkOrder.WorkOrderNumber,WorkOrder.AssetId,WorkOrder.SFS_External_Id__c FROM ProductConsumed WHERE WorkOrderLineItemId =: woliId OR Id =: woliId];// LocationId,SFS_Part_Number__c SFS_Transaction_Type__c ProductTransferNumber Name
         }catch(System.Exception e){
            System.debug('Exception sjg' + e.getMessage());
            for(ProductConsumed p : prod){
                p.SFS_Integration_Status__c = 'Error';
                p.SFS_Integration_Response__c = e.getMessage();
            }
            update prod;
        }
         System.debug('Prod Size: ' + prod.size());
        List<String> FinalXML = new List<String>();
        List<String> productConsXmlList = new List<String>();
        SFS_ProductConsumed_Outbound_Handler pcOut = new SFS_ProductCOnsumed_Outbound_Handler();
        Map<Integer,Map<String,Object>> pConsMap = new Map<Integer,Map<String,Object>>();
            System.debug('sjg map' + pConsMap);
        for(ProductConsumed p: prod){
            Map<String, Object> InitialMap = new Map<String, Object>();
            initialMap = p.getPopulatedFieldsAsMap();
            System.debug('Product Consumed Initial Map: ' + InitialMap);
           	Map<String,Object> workOrderMap = new Map<String, Object>();
            WorkOrder w = (WorkOrder)InitialMap.get('WorkOrder');
            workOrderMap = w.getPopulatedFieldsAsMap();
            
            Map<String, Object> ProductMap = new Map<String, Object>();
            Product2 p2 = (Product2)InitialMap.get('Product2');
            ProductMap = p2.getPopulatedFieldsAsMap();

            Map<String, Object> finalMap = new Map<String, Object>();
            for(String field: initialMap.keySet()){
               System.debug('MAP 1: ' + field);
                if(!field.contains('WorkOrder') && !field.contains('Product2') && !field.contains('ProductItem')){
                    FinalMap.put(field, p.get(field));
                    // System.debug('MAP 1: ' + field);
                }
                
            }
            for(String field : workOrderMap.keyset()){
                try{
                    if(field == 'SFS_External_Id__c'){
                        FinalMap.put('WorkOrder-WorkOrderNumber', w.get(field));
                        System.debug('TRUE WO');
                    }else{             
                        FinalMap.put(field.toLowerCase(), w.get(field));
                    }
                    
                }catch(System.Exception x){
                    
                }
                //FinalMap.put('WorkOrder-' + field,w.get(field));
            }
            for(String field : productMap.keyset()){
                FinalMap.put('Product-'+ field,p2.get(field));
            }
            pConsMap.put(prod.indexOf(p), FinalMap);
            System.debug('COUNT: ' + prod.indexOf(p));
            productConsXmlList = pcOut.buildXML(partsUsedStruct, FinalMap);
            System.debug('After Build: ' + productConsXMLList);
            FinalXML.addAll(productConsXmlList);
        }
        
        xmlString = StartTag;
        for(string x : FinalXml){
           xmlString = xmlString + x; 
            System.debug('XML Payload: ' + x);
        }
        xmlString = xmlString + endTag;
        
       
        for(ProductConsumed x: prod){
            idList = idList + '|' + x.Id;
            System.debug('ID: ' + idList);
            System.debug('x: ' + x);
        }
            
       
        System.debug('ID Out: ' + idList);
      	String Status = '';
        String ErrorMessage = '';
        
        //Condition to callout to the normal HTTP Method if a test isn't running.
       	//Else callout to the Test method. 
       	//The Class is SFS_Outbound_Integration_Callout.
        
        //Interface Detail will come from here: https://gdi--dprj01.lightning.force.com/lightning/setup/CustomMetadata/page?address=%2Fm16%3Fsetupid%3DCustomMetadata%26appLayout%3Dsetup%26tour%3D%26sfdcIFrameOrigin%3Dhttps%253A%252F%252Fgdi--dprj01.lightning.force.com%26sfdcIFrameHost%3Dweb%26nonce%3D3a2e73348c5fd624a2d856de6ec886ef8737c4b101af9f28e920bdb2e1a80477%26ltn_app_id%3D06m1h0000002BK8AAM%26clc%3D1
		string recordIds = '';
        List<string> IdStringList = new List<string>();
        for(id i : woliId){
            IdStringList.add(string.valueof(i));
        }
        for(string i : IdStringList){
            recordIds = recordIds + '|' + i;
        }
        string interfaceDetail = 'XXPA1951_PC';
        String interfaceLabel = 'Parts Used product Consumed ' + recordIds;
        
        If(!test.isRunningTest()){
           SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel);
        }else{
          SFS_Outbound_Integration_Callout.HttpCalloutTest('Return Order',recordIds,xmlString, 'XXPA1951_PC', interfaceLabel);
        }
        
        
        
        //HttpSendStore(xmlString, ErrorMessage, Status, IdList);
       
        
        
    }
    
    public List<String> buildXML(SFS_Parts_Used_XML_Structure__mdt[] partsUsedStruct, Map<String, Object>ProductConsumedFieldMap){
        
        List<String> xmlList = new List<String>();
        String startTag = '';
        String endtag = '';
        
        for(SFS_Parts_Used_XML_Structure__mdt xmlStructure : partsUsedStruct){
            //check for hardcoded flag. If false, replace data.
            if(xmlStructure.SFS_XML_Object__c != 'End' && xmlStructure.SFS_XML_Object__c != 'Start'){
                if(xmlStructure.SFS_Hardcoded_Flag__c == false){
                    String sfField = xmlStructure.SFS_Salesforce_Field__c;
                    if(ProductConsumedFieldMap.containsKey(sfField)){
                        double nodeOrder = xmlStructure.SFS_Node_Order__c;
                        //check if value from query is null
                        if(ProductConsumedFieldMap.get(sfField) != null){
                            String xmlFullName = xmlStructure.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(ProductConsumedFieldMap.get(sfField));
                            replacement = replacement.escapeXml();
                            String newLine='';
                            if(sfField!='SFS_Serial_Number__c'){
                                newLine = XMLFullName.replace(sffield,replacement);
                                xmlList.add(newLine);
                            }//Added as part of SIF-4642 - To include serialized parts in the payload
                            else if(sfField=='SFS_Serial_Number__c'){
                                List<String> serialNumbersList = replacement.split('\\|');
                                for(String serialNo:serialNumbersList){
                                    newLine = XMLFullName.replace(sffield,serialNo); 
                                    xmlList.add(newLine);
                                }
                            }
                        } 
                        else if(ProductConsumedFieldMap.get(sfField) == null){
                            xmlList.add(xmlStructure.SFS_XML_Full_Name__c);
                            //convertedValues.put(xmlStructure.SFS_Node_Order__c, xmlStructure.SFS_XML_Full_Name__c);
                        }
                    }
                    //if SCFieldMap does not contain sffield, the field value is null. Replace null value with just tags.
                    else if(!ProductConsumedFieldMap.ContainsKey(sfField)){
                        String empty = '';String replacement ='';
                        if(sfField ==null) replacement = xmlStructure.SFS_XML_Full_Name__c;
                        else replacement = xmlStructure.SFS_XML_Full_Name__c.replace(sfField, empty);                       
                        System.debug('replacement' + replacement);
                        xmlList.add(replacement);
                    }
                }
                else if(xmlStructure.SFS_Hardcoded_Flag__c == true){
                    xmlList.add(xmlStructure.SFS_XML_Full_Name__c);
                }
            
            
           
        }
        }
       
    	return xmlList;
    }
    
    
    public static void IntegrationResponse(HttpResponse res, String interfaceDetail){
        double status = res.getStatusCode();
        List<string> response1 = new List<string>();
        response1 = interfaceDetail.split('\\|');
      
        List<ProductConsumed> prodConList = [SELECT SFS_Integration_Response__c, SFS_Integration_Status__c FROM ProductConsumed WHERE id = : response1];
        
        system.debug('body'+res.getBody());
        Map<String,String> refCodeMap = new  Map<String,String>();
        Map<String,String> refMsgMap = new  Map<String,String>();
        dom.Document resDoc = res.getBodyDocument();
        Dom.XMLNode envelope = resDoc.getRootElement();
        for(DOM.XmlNode node : envelope.getChildElements())
            {
               if(node.getName() == 'Response'){
                   for(Dom.XmlNode ChildNode: node.getChildElements()){
                        if(ChildNode.getName() == 'Status'){
                            for(Dom.XmlNode detailNode: ChildNode.getChildElements()){
                                if(detailNode.getName() == 'StatusText'){
                                      for(Dom.XmlNode detailNodeChild: detailNode.getChildElements()){
                                          if(detailNodeChild.getName() == 'Status'){
                                               for(Dom.XmlNode detailNodeChild1: detailNodeChild.getChildElements()){
                                                    if(detailNodeChild1.getName() == 'G_TRANSACTIONS'){
                                                        for(Dom.XmlNode detailNodeChild2: detailNodeChild1.getChildElements()){
                                                            if(detailNodeChild2.getName() == 'MTL_TRANSACTIONS_INTERFACE'){
                                                                 String pcNum; String code; String errMsg;
                                                                 for(Dom.XmlNode detailNodeChild3: detailNodeChild2.getChildElements()){
                                                                                                     
                                                                     if(detailNodeChild3.getName() == 'UNIQUE_REFERENCE_NO' && detailNodeChild3.getText()!=null){
                                                                          pcNum = detailNodeChild3.getText();
                                
                                                                     } 
                                                                     if(detailNodeChild3.getName() == 'code' && detailNodeChild3.getText()!=null){
                                                                          code = detailNodeChild3.getText();
                                                                     }  
                                                                     if(detailNodeChild3.getName() == 'errorMessage' && detailNodeChild3.getText()!=null){
                                                                          errMsg = detailNodeChild3.getText();
                                                                      } 
                                                                     else{
                                                                           errMsg ='';
                                                                        }   
                                                                     
                                                                 } 
                                                                 refCodeMap.put(pcNum,code);
                                                                 refMsgMap.put(pcNum,errMsg);
                                                                 system.debug('refCodeMap'+refCodeMap);
                                                            }  
                                                        }  
                                                    }    
                                                 }    
                                          }    
                                      }
                                }   
                            }
                        }    
                   
                }
            }  
        }
      List<ProductConsumed> pcUpdate = new List<ProductConsumed>();
      List<ProductConsumed> pcList = [Select Id, ProductConsumedNumber, SFS_Integration_Status__c, SFS_Integration_Response__c from ProductConsumed where ProductConsumedNumber IN :refCodeMap.keySet()];
      for(ProductConsumed pc: pcList) {
            pc.SFS_Integration_Status__c = (refCodeMap.get(pc.ProductConsumedNumber)=='SUCCESS')?'APPROVED':'ERROR';
            pc.SFS_Integration_Response__c = refMsgMap.get(pc.ProductConsumedNumber);
            pcUpdate.add(pc);
        } 
      if(!pcUpdate.isEmpty()){
          update pcUpdate; 
        } 
    }
}