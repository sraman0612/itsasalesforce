public class SFS_ServiceAppointment_Queueable implements Queueable{
	public List<Id> parentIdList ; 
    public List<Id> saIdList;
    public SFS_ServiceAppointment_Queueable(List<Id> parentIdList,List<Id> saIdList){
        this.parentIdList = parentIdList ;  
        this.saIdList = saIdList;
    }
    public void execute(QueueableContext context) { 
        Set<Id> woIds=new Set<Id>();
        DateTime StartDate;
        Double Duration=0.00;
        Decimal ActualDur;
        Set<Id> woliId=new Set<Id>();
        Set<Id> AssetIds=new Set<Id>();
        Set<Id> wolisId=new Set<Id>();
        List<Id> SaIds = new List<Id>();
        Boolean Completed=false;
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        List<CAP_IR_Labor__c> LhourList=new List<CAP_IR_Labor__c>();
        Map<Id,Double> saMap=new Map<Id,Double>();
        Map<Id,Datetime> saDate=new Map<Id,Datetime>();
        Map<Id,Double> saActualDur=new Map<Id,Double>();
        Map<Id,Double> saDurCompl=new Map<Id,Double>();
        
        List<WorkOrderLineItem> woliLists=[select id,WorkOrderId,AssetId from WorkOrderLineItem where Id IN:parentIdList and SFS_Alpha_WOLI__c =true and WorkOrder.MaintenancePlanId !='' AND MaintenancePlanId!=''];
        List<WorkOrderLineItem> woliListManual=[select id,WorkOrderId,AssetId from WorkOrderLineItem where Id IN:parentIdList];
        //SIF-4926 added condition for canceled status 
        List<ServiceAppointment> checkAllSA=[select id,status from ServiceAppointment where Status !='Completed' And Status!='Cannot Complete'And Status !='Canceled' and ParentRecordId IN:parentIdList];
        if(woliLists.size()>0){
        for(WorkOrderLineItem woli:woliLists){
            woIds.add(woli.WorkOrderId);
            woliId.add(woli.Id);          
        }
            List<WorkOrderLineItem> wolis=[select id,WorkOrderId,AssetId from WorkOrderLineItem where WorkOrderId IN:woIds and MaintenancePlanId !='' ];   
            for(WorkOrderLineItem woli:wolis){
                wolisId.add(woli.Id); 
                AssetIds.add(woli.AssetId);
            }
        Double durUpdate,ActualDurUpdate,days,hours,DurCompl,durUpdate1;
        if(woIds.size()>0){
        List<ServiceAppointment> saList=[select id,Status,SFS_Labor_Start__c,Duration,ParentRecordId,ActualDuration,SFS_Original_Duration__c,ActualStartTime,ActualEndTime,Work_Order__c
                                   from ServiceAppointment where ParentRecordId IN:wolisId AND (Status = 'Not Required' OR Id =:saIdList) ];
        
        for(ServiceAppointment saDuration:saList){
            if(woliId.contains(saDuration.ParentRecordId)){
                StartDate=saDuration.SFS_Labor_Start__c;
                saDate.put(saDuration.Work_Order__c,StartDate);
                ActualDurUpdate=saDuration.ActualDuration;
                saActualDur.put(saDuration.Work_Order__c,ActualDurUpdate);
                DurCompl=saDuration.Duration;                
				saDurCompl.put(saDuration.Work_Order__c,DurCompl);
                if(saDuration.Status=='Completed' || saDuration.Status=='Cannot Complete')
                	Completed=true;
            }
			        
        }                       
            
        for(ServiceAppointment saDuration:saList){
            
            durUpdate1=saActualDur.get(saDuration.Work_Order__c);
            hours=durUpdate1/60;           
        	
            Decimal durCheck=saDuration.SFS_Original_Duration__c/saDurCompl.get(saDuration.Work_Order__c);
        	Duration=durCheck*hours; 
            saMap.put(saDuration.ParentRecordId,Duration);
            
            }
        List<WorkOrderLineItem> woliDetails=[select id,Status,WorkOrderId,AssetId from WorkOrderLineItem where ID IN:wolisId];
        
        for(WorkOrderLineItem woli:woliDetails){
            if(woli.Status!='Completed'){
               woli.Status='Completed'; 
               woliList.add(woli);
            }
            
            durUpdate=saMap.get(woli.Id);
             
            CAP_IR_Labor__c Lhour=new CAP_IR_Labor__c();
            if(Test.isRunningTest())
                Lhour.CAP_IR_Start_Date__c=System.now();
            else
            	Lhour.CAP_IR_Start_Date__c=saDate.get(woli.WorkOrderId);
			Lhour.CAP_IR_End_Date__c =System.now(); 
            Lhour.CAP_IR_Labor_Type__c='Onsite'; 
            Lhour.CAP_IR_Labor_Rate__c='Standard';
            Lhour.CAP_IR_Work_Order__c=woli.WorkOrderId;
            Lhour.CAP_IR_Work_Order_Line_Item__c=woli.Id;
            Lhour.CAP_IR_Quantity__c=durUpdate;
            LhourList.add(Lhour);
        } 
            
       	if(LhourList.size()>0)
        	insert LhourList;
        } 
        }
        else if(woliListManual.size()>0){
            for(WorkOrderLineItem woli:woliListManual){
            	woIds.add(woli.WorkOrderId);
            	woliId.add(woli.Id);
            	AssetIds.add(woli.AssetId);
                woli.Status='Completed';
                Completed=true;
                woliList.add(woli);
        	}
        }
        if(woliList.size()>0 && Completed==true && checkAllSA.size()==0)
        	update woliList;
        if(AssetIds.size()>0){
            List<Asset> assetList=[Select id,SFS_WOLI_Last_Completed_Date__c from Asset where Id IN:AssetIds];
   			List<Asset> assetUpdate=new List<Asset>();
            for(Asset ass:assetList){
                String strAssetId=ass.Id;
                if(!String.isEmpty(strAssetId)){
                    Date AssDate=Date.today();
         			ass.SFS_WOLI_Last_Completed_Date__c=AssDate;
                    assetUpdate.add(ass);
         		}
            }
            update assetUpdate;
        }
    }
}