@isTest
public with sharing class B2BCartControllerTest {

    @testSetup
    static void setup() {
        UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' limit 1];
        User usr = CTS_TestUtility.createUser(false);
        usr.UserRoleId = r1.Id;
        insert usr;
        System.runAs(usr){
            Account account = CTS_TestUtility.createAccount('TestAccount', false);
            account.OwnerId = usr.Id;
            insert account;
            Contact con = CTS_TestUtility.createContact('lastnameContact', 'firstnameContact', 'test@test.com', account.Id, true);
            WebStore webStore = new WebStore(Name='TestWebStore');
            insert webStore;
            List<Product2> prdList = new List<Product2>();
            Product2 pr1 = new Product2(Name = 'prd1');
            prdList.add(pr1);
            Product2 pr2 = new Product2(Name = 'prd2');
            prdList.add(pr2);
            insert prdList;
            CTS_IOT_Frame_Type__c cif = new CTS_IOT_Frame_Type__c(name = 'frameType',CTS_IOT_Type__c = 'Compressor');
            insert cif;
            Cross_Sell_Recommendations__c csr = new Cross_Sell_Recommendations__c(Recommended_ProductId__c = prdList[0].Id, Recommended_Model_ParentId__c = cif.Id);
            insert csr;
        }
    }

    @isTest
    static void resolveCommunityIdToWebstoreIdTest() {
        String webStoreId = [Select Id from WebStore limit 1].Id;
        String productId = [Select Id from product2 limit 1].Id;
        String effectiveAccountId = [Select Id from Account limit 1].Id;
        User user = [SELECT Id FROM User LIMIT 1];
        ConnectApi.CartItem cartItem = RecommendedProducts.addToCart(webStoreId,productId,'5',effectiveAccountId);

        Test.startTest();
        ConnectApi.CartItemCollection cartItemCollection = B2BCartController.getCartItems(effectiveAccountId, user.Id);
        Assert.isNull(cartItemCollection);
        Test.stopTest();
    }

    @isTest
    static void checkBuyerPermissionTest() {
        User adminUser = CTS_TestUtility.createUser(true);
        System.runAs(adminUser) {
            Test.startTest();
            Boolean hasPermission = B2BCartController.checkBuyerPermission(adminUser.Id, '');
            Assert.isFalse(hasPermission);
            Test.stopTest();
        }
    }

    @isTest
    static void checkBuyerPermissionWithFrameTypeTest() {
        String frameTypeId = [Select Recommended_Model_ParentId__c from Cross_Sell_Recommendations__c LIMIT 1 ].Recommended_Model_ParentId__c;
        UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' LIMIT 1];
        User user = [SELECT Id FROM User WHERE UserRoleId = :r1.Id LIMIT 1];
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'B2B_Buyer_Additional_Access'];
        PermissionSetAssignment psa = new PermissionSetAssignment
                (AssigneeId = user.Id, PermissionSetId = ps.Id);
        insert psa;
        System.runAs(user) {
            Test.startTest();
            Boolean hasPermission = B2BCartController.checkBuyerPermission(user.Id, frameTypeId);
            Assert.isTrue(hasPermission);
            Test.stopTest();
        }
    }

}