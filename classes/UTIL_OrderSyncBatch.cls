/*
Paste the below code into the Anonymous Apex Window
To know how many total records are there to sync
========== START CODE ==========
new ensxsdk.Logger(null);
List<String> docTypes = (List<String>)UTIL_AppSettings.getList(
    'UTIL_OrderSyncBatch.DocTypes', String.class, new List<String>{});
String transactionGroup = '0';

SBO_EnosixSalesDocSync_Search.EnosixSalesDocSync_SC searchContext = new SBO_EnosixSalesDocSync_Search.EnosixSalesDocSync_SC();
searchContext.pagingOptions.pageSize = 1;
searchContext.pagingOptions.pageNumber = 1;
searchContext.SEARCHPARAMS.TransactionGroup = transactionGroup;
searchContext.SEARCHPARAMS.InitialLoad = true;
if (docTypes.size() > 0)
{    
    for (String docType : docTypes)
    {
        SBO_EnosixSalesDocSync_Search.DOC_TYPE newDocType = new SBO_EnosixSalesDocSync_Search.DOC_TYPE();
        newDocType.SalesDocumentType = docType;
        searchContext.DOC_TYPE.add(newDocType);
    }
}
SBO_EnosixSalesDocSync_Search sbo = new SBO_EnosixSalesDocSync_Search();
sbo.search(searchContext);
System.debug(searchContext.result.isSuccess());
System.debug(searchContext.pagingOptions.totalRecords);
========== END CODE ==========
*/
public with sharing class UTIL_OrderSyncBatch
    extends UTIL_SalesDocSyncBatch
    implements Database.Batchable<Object>,
    Database.AllowsCallouts,
    Database.Stateful,
    I_ParameterizedSync
{
    public UTIL_OrderSyncBatch()
    {
        super('0', 'OBJ_Order__c', 'UTIL_OrderSyncSchedule', 'UTIL_OrderSyncBatch');
    }

    /* Database.Batchable methods start(), execute(), and finish() */
    // start()
    public List<Object> start(Database.BatchableContext context)
    {
        return super.startBatch(context);
    }

    // execute()
    public void execute(
        Database.BatchableContext context,
        List<Object> searchResults)
    {
        super.executeBatch(context, searchResults);
    }

    // finish()
    public void finish(Database.BatchableContext context)
    {
        super.finishBatch(context);
    }
}