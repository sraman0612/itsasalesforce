@isTest
public class C_ServHistTrigHelperRecLubUsedTest 
{
    @isTest
    public static void runInsUpdTest()
    {
        Asset sn1 = new Asset(
            Name='DM006400'
        ); insert sn1;
        
        Asset sn2 = new Asset(
            Name='DM007400'
        ); insert sn2;
        
        Part__c p1 = new Part__c(
            Name='ZS1088422'
        ); insert p1;
        
        Part__c p2 = new Part__c(
            Name='ZS1088421'
        ); insert p2;
        
        
        
        Test.startTest();
        Service_History__c sh1 = new Service_History__c
            (
                Lubricant__c='ZS1088425:T, ZS1088426:T, ZS1088427:F',
                Air_Filter__c='2118315:T, 2118315:F, 2118314:F',
                Separator__c='A10533574:T, A10533574:F, A10533566:F, A10533566:F',
                Cabinet_Filter__c='300USC784:T',
                Control_Box_Filter__c='338USC6032:T',
                Serial_Number__c=sn1.Id,
                Date_of_Service__c=System.today(),
                Current_Hours__c=100
            );
        
        Service_History__c sh2 = new Service_History__c
            (
                Lubricant__c='ZS1088422:T, ZS1088422:F',
                Air_Filter__c='A11207674:T, A11207674:F',
                Separator__c='A10533574:T, A10533574:F, A10533566:F	',
                Cabinet_Filter__c='300USC784:T',
                Control_Box_Filter__c='338USC6032:T',
                Serial_Number__c=sn1.Id,
                Date_of_Service__c=System.today(),
                Current_Hours__c=100
            );
        
        Service_History__c sh3 = new Service_History__c
            (
                Air_Filter__c='A11207674:T, A11207674:F',
                Separator__c='A10533574:T, A10533574:F, A10533566:F	',
                Cabinet_Filter__c='300USC784:T',
                Control_Box_Filter__c='338USC6032:T',
                Serial_Number__c=sn1.Id,
                Date_of_Service__c=System.today(),
                Current_Hours__c=100
            );
        
        Service_History__c sh4 = new Service_History__c
            (
                Lubricant__c='ZS1088499:T, ZS1088421:T, ZS1088427:F',
                Air_Filter__c='2118315:T, 2118315:F, 2118314:F',
                Separator__c='A10533574:T, A10533574:F, A10533566:F, A10533566:F',
                Cabinet_Filter__c='300USC784:T',
                Control_Box_Filter__c='338USC6032:T',
                Serial_Number__c=sn2.Id,
                Date_of_Service__c=System.today(),
                Current_Hours__c=100
            );
        List<Service_History__c> lstServ = new List<Service_History__c>{sh1};
         insert lstServ;
        Test.stopTest();
        
        
        //System.assertEquals('ZS1088421', sn.Last_Lubricant_Used__c);
        
    }
    
    @isTest
    public static void runDelTest()
    {
        Asset sn1 = new Asset(
            Name='DM006400'
        ); insert sn1;
        
        Part__c p1 = new Part__c(
            Name='ZS1088426'
        ); insert p1;
        
        Part__c p2 = new Part__c(
            Name='ZS1088425'
        ); insert p2;
        
        Service_History__c sh1 = new Service_History__c
            (
                Lubricant__c='ZS1088425:T, ZS1088426:T, ZS1088427:F',
                Air_Filter__c='2118315:T, 2118315:F, 2118314:F',
                Separator__c='A10533574:T, A10533574:F, A10533566:F, A10533566:F',
                Cabinet_Filter__c='300USC784:T',
                Control_Box_Filter__c='338USC6032:T',
                Serial_Number__c=sn1.Id,
                Date_of_Service__c=System.today(),
                Current_Hours__c=100
            );
        insert sh1;
         
        Service_History__c sh2 = new Service_History__c
            (
                Lubricant__c='ZS1088425:T, ZS1088426:T, ZS1088427:F',
                Air_Filter__c='2118315:T, 2118315:F, 2118314:F',
                Separator__c='A10533574:T, A10533574:F, A10533566:F, A10533566:F',
                Cabinet_Filter__c='300USC784:T',
                Control_Box_Filter__c='338USC6032:T',
                Serial_Number__c=sn1.Id,
                Date_of_Service__c=System.today(),
                Current_Hours__c=100
            );
        insert sh2;
        
        Test.startTest();
        Service_History__c sh98 = [select id from Service_History__c limit 1];
        delete sh98;
        Service_History__c sh99 = [select id from Service_History__c limit 1];
        delete sh99;
        
        Test.stopTest();
        Asset a = [select id,name,Last_Lubricant_Used__c from Asset limit 1];
        
    }
}