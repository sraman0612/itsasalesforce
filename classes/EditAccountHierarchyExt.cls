public with sharing class EditAccountHierarchyExt {

	public Account theAccount {get;set;}
	public ApexPages.StandardController theController {get;set;}

	
	public EditAccountHierarchyExt( ApexPages.StandardController controller) {
		theController = controller;
		theAccount = [SELECT Id, Name, ParentId FROM Account WHERE Id = :((Account)theController.getRecord()).Id];
	}
	
	@AuraEnabled
	public static List<Account> getAccounts() {
		return [SELECT Id, ParentId, Name FROM Account WHERE ParentId = null limit 50];
	}
	
	public PageReference saveRecord() {
		// Update the account and go back to the 'Manage' page.
		theAccount = (Account)theController.getRecord();
		update theAccount;
		return new PageReference( '/lightning/n/Manage_Account_Hierarchy' );
	}
	
	public PageReference cancelEdit() {
		// Just go back to the 'Manage' page.
		return new PageReference( '/lightning/n/Manage_Account_Hierarchy' );
	}
}