@IsTest
public class TSTU_MachineStartupForm {
    public class MockRFC_Z_ENSX_GET_TAX_JURISDICTION implements ensxsdk.EnosixFramework.RFCMock
    {
        public ensxsdk.EnosixFramework.FunctionObject executeFunction() 
        {
            return null;
        }
    }


    public class MockSBO_EnosixServiceNotification_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) { 
            return new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification(); 
        }
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { 

            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification sn = (SBO_EnosixServiceNotification_Detail.EnosixServiceNotification)obj;
            
            return sn; 
        }
    }

    public class MockSBO_EnosixSO_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) { return obj; }
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { return obj; }
    }

    public class MockSBO_EnosixServNotifUpdate_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { return obj; }
    }

    @IsTest
    public static void test_sapMessages() {
        List<ensxsdk.EnosixFramework.Message> messages = (List<ensxsdk.EnosixFramework.Message>)JSON.deserialize('[]', List<ensxsdk.EnosixFramework.Message>.class);

        UTIL_MachineStartupForm.hasErrors(messages);
        UTIL_MachineStartupForm.generateMessages(messages);
        UTIL_MachineStartupForm.getServiceNotificationId(messages);
        UTIL_MachineStartupForm.getSalesOrderNumber(messages);
    }
    
    
    @IsTest
    public static void test() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServNotifUpdate_Detail.class, new MockSBO_EnosixServNotifUpdate_Detail());
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Detail.class, new MockSBO_EnosixSO_Detail());
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotification_Detail.class, new MockSBO_EnosixServiceNotification_Detail());
        ensxsdk.EnosixFramework.setMock(RFC_Z_ENSX_GET_TAX_JURISDICTION.class, new MockRFC_Z_ENSX_GET_TAX_JURISDICTION());

        Account account = new Account(Name = 'name', BillingCountry = 'United States');
        UTIL_SFAccount.setAccountCustomerNumber(account, 'test');
        insert account;

        Asset asset = new Asset();
        asset.Name = '8888';
        asset.put(UTIL_AssetSyncBatch.SFSyncKeyField,'8888-9999');
        asset.AccountId = account.Id;
        insert asset;
        
        Machine_Startup_Form__c machineStartupForm = populateStartupForm();

        machineStartupForm.Serial_Number__c = asset.Id;
        machineStartupForm.SAP_Status__c = 'READY';
        machineStartupForm.Account__c = account.Id;
        machineStartupForm.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'GD_Compressor_Startup_Form' AND sObjectType = 'Machine_Startup_Form__c'].Id;
        insert machineStartupForm;
    }
    
    public static Machine_Startup_Form__c populateStartupForm() {
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Machine_Startup_Form__c machineStartupForm = new Machine_Startup_Form__c();
        machineStartupForm.X1st_2nd_condensate_drains_plumbed_sep__c = false;
        machineStartupForm.X8_Jog_Main_Motor__c = true;
        machineStartupForm.Advisory_Information__c = 'Advisory Information';
        machineStartupForm.Airend_High_Pressure_Serial_Number__c = 'Airend High Pressure Serial No';
        machineStartupForm.Airend_Serial_Number__c = 'Airend Serial Number - Package Nameplate';
        machineStartupForm.All_connections_checked_for_tightness_an__c = false;
        machineStartupForm.All_pipe_fittings_and_connections_are_ch__c = false;
        machineStartupForm.Oil_level_is_adequate__c = false;
        machineStartupForm.Ampere_1__c = 'Amps - VFD (1) Nameplate';
        machineStartupForm.Ampere_2__c = 'Amps - Main Motor (2) Nameplate';
        machineStartupForm.Ampere_3__c = 'Amps - VFD (2) Nameplate';
        machineStartupForm.Ampere_4__c = 'Amps - Main Motor (1) Nameplate';
        machineStartupForm.Ampere_5__c = 'Amps - VFD (3) Nameplate';
        machineStartupForm.Ample_space_is_provided_around_the_packa__c = false;
        machineStartupForm.IL1_On__c = 'I L1 (on)';
        machineStartupForm.IL2_On__c = 'I L2 (on)';
        machineStartupForm.IL3_On__c = 'I L3 (on)';
        machineStartupForm.Auto_Pressure__c = 'Auto pressure';
        machineStartupForm.Auto_Restart_ON_or_OFF__c = 'ON';
        machineStartupForm.Capture_IMEI_number_from_iConn_modem__c = 'Capture the IMEI number from iConn modem:';
        machineStartupForm.Champion_Line_Reactor_Model_No_if_such__c = null;
        machineStartupForm.Champion_Premium_Warranty_Registration__c = null;
        machineStartupForm.Claim_Data_Reviewed_and_Confirmed__c = false;
        machineStartupForm.Claim_Email_Contact__c = null;
        machineStartupForm.Closed_loop_coolant_reservoir_check__c = false;
        machineStartupForm.Comments_on_customer_water_system__c = 'Comments on customer water system:';
        machineStartupForm.Compressor_checked_for_leaks__c = false;
        machineStartupForm.Connect_the_wire_to_the_modem__c = false;
        machineStartupForm.Contact__c = null;
        machineStartupForm.Contact_Email__c = null;
        machineStartupForm.Contact_Name__c = null;
        machineStartupForm.Controller_Software_Version__c = null;
        machineStartupForm.Customer_Ref_Number__c = null;
        machineStartupForm.Customer_Signee_Name__c = null;
        machineStartupForm.Date__c = Date.newInstance(2019, 10, 24);
        machineStartupForm.Describe_application_and_installation_co__c = 'Describe application and installation conditions:';
        machineStartupForm.Describe_Operating_Environment__c = 'Describe Operating Environment';
        machineStartupForm.Designed_Pressure__c = null;
        machineStartupForm.Differential_press__c = 'Differential press';
        machineStartupForm.Discharge_temp__c = 'Discharge temp';
        machineStartupForm.Disconnect_lockout_and_tagout_power_sup__c = false;
        machineStartupForm.Distributor__c = acct.id;
        machineStartupForm.Ensure_customer_has_access_to_the_manual__c = false;
        machineStartupForm.NOTES__c = 'Notes';
        machineStartupForm.Exchange_any_two_phases_of_the_incoming__c = true;
        machineStartupForm.Form_Signed__c = false;
        machineStartupForm.GD_Line_Reactor_Model_Number__c = 'GD line reactor model no - Customer Supplied';
        machineStartupForm.GD_Purchase_Order_Number__c = null;
        machineStartupForm.Grounding_code_used_for_installation__c = 'grounding code - Customer Supplied';
        machineStartupForm.Hertz_1__c = 'Hz - Package Nameplate';
        machineStartupForm.Hertz_2__c = 'Hz - Main Motor (1) Nameplate';
        machineStartupForm.Hertz_3__c = 'Hz - Main Motor (2) Nameplate';
        machineStartupForm.Hold_down_bolts_cap_screws_are_properl__c = true;
        machineStartupForm.Identify_Target_Pressure__c = 'Identify target pressure';
        machineStartupForm.If_equipped_with_cooling_duct_work_is_i__c = 'No';
        machineStartupForm.If_the_customer_does_not_wish_to_activat__c = true;
        machineStartupForm.Incoming_Wire_Size__c = 'Incoming Wire Size - Customer Supplied';
        machineStartupForm.Incoming_wiring_properly_sized_installed__c = true;
        machineStartupForm.Inlet_air_temp_not_go_below_36F_2C__c = false;
        machineStartupForm.Inlet_temp__c = 'Inlet temp';
        machineStartupForm.Inlet_water_temperature_between_60_90_F__c = false;
        machineStartupForm.Input_f_Volts_H_1__c = 'Input - VFD (1) Nameplate';
        machineStartupForm.Input_f_Volts_H_2__c = 'Input - VFD (2) Nameplate';
        machineStartupForm.Input_f_Volts_H_3__c = 'Input - VFD (3) Nameplate';
        machineStartupForm.Installed_Ancillary_Equipment__c = null;
        machineStartupForm.Interstage_press__c = 'Interstage press - VST Compressor only';
        machineStartupForm.Interstage_Press_1__c = null;
        machineStartupForm.Interstage_Press_2__c = null;
        machineStartupForm.Interstage_temp__c = 'Interstage temp - VST Compressor only';
        machineStartupForm.Is_plant_pressure_stable__c = 'Yes';
        machineStartupForm.kW_HP_1__c = 'kW/HP - Main Motor (1) Nameplate';
        machineStartupForm.kW_HP_2__c = 'kw/HP - Main Motor (2) Nameplate';
        machineStartupForm.Length_of_Incoming_Wire__c = 'Length of incoming wire - Customer Supplied';
        machineStartupForm.Load_percentage__c = 'Load percentage';
        machineStartupForm.Load_pressure__c = null;
        machineStartupForm.Manual_Items_Reviewed__c = null;
        machineStartupForm.Model_No_1__c = 'Model - Package Nameplate';
        machineStartupForm.Model_No_2__c = 'Model No - Main Motor (1) Nameplate';
        machineStartupForm.Model_No_3__c = 'Model Number - Main Motor (2) Nameplate';
        machineStartupForm.Motor_1_current__c = 'Motor 1 current';
        machineStartupForm.Motor_1_freq__c = 'Motor 1 freq';
        machineStartupForm.Motor_1_Manufacturer__c = null;
        machineStartupForm.Motor_1_power__c = 'Motor 1 power';
        machineStartupForm.Motor_1_speed__c = 'Motor 1 speed';
        machineStartupForm.Motor_2_current__c = 'Motor 2 current';
        machineStartupForm.Motor_2_freq__c = 'Motor 2 freq';
        machineStartupForm.Motor_2_Manufacturer__c = null;
        machineStartupForm.Motor_2_power__c = 'Motor 2 power';
        machineStartupForm.Motor_2_speed__c = 'Motor 2 speed';
        machineStartupForm.Motor_3_current__c = 'Motor 3 current';
        machineStartupForm.Motor_3_freq__c = 'Motor 3 freq';
        machineStartupForm.Motor_3_power__c = 'Motor 3 power';
        machineStartupForm.Motor_3_speed__c = 'Motor 3 speed';
        machineStartupForm.Motor_Rating_1__c = 'Motor Rating - VFD (1) Nameplate';
        machineStartupForm.Motor_Rating_2__c = 'Motor Rating - VFD (2) Nameplate';
        machineStartupForm.Motor_Rating_3__c = 'Motor Rating - VFD (3) Nameplate';
        machineStartupForm.Oil_Free_Warranty_Registration__c = null;
        machineStartupForm.Oil_press_manifold__c = 'Oil press (manifold) - VST Compressor only';
        machineStartupForm.Operating_hours__c = 'Operating hours';
        machineStartupForm.Overall_appearance_and_condition_is_good__c = true;
        machineStartupForm.Owner_Contact__c = null;
        machineStartupForm.Owner_Contact_Email__c = 'chris.phillips@codezeroconsulting.com';
        machineStartupForm.Owner_Contact_Name__c = 'Customer (Compressor Owner) Contact:';
        machineStartupForm.Owner_Contact_Phone__c = 'Phone';
        machineStartupForm.Package_inlet_water_temp__c = 'Package inlet water temp';
        machineStartupForm.Package_outlet_water_temp__c = 'Package outlet water temp';
        machineStartupForm.Piping_sized_right_for_adequater_flow_ra__c = true;
        machineStartupForm.Plant_press__c = 'Plant press';
        machineStartupForm.Plant_temp__c = 'Plant temp';
        machineStartupForm.Platinum_Extended_Warranty_Registration__c = 'I am enrolling my Gardner Denver compressor in the no charge 10-Year Platinum Extended Warranty.';
        machineStartupForm.Pressure_relief_valve_s_are_provided_wh__c = true;
        machineStartupForm.Proper_operating_procedures__c = true;
        machineStartupForm.Pull_the_antenna_out_of_its_box_attach__c = true;
        machineStartupForm.Recommended_routine_maintenance__c = true;
        machineStartupForm.Remarks__c = 'Remarks:';
        machineStartupForm.Reservoir_press__c = 'Reservoir press';
        machineStartupForm.Review_Air_Filter__c = false;
        machineStartupForm.Review_Compressor_Lubrication_Separati__c = false;
        machineStartupForm.Review_Controls_and_Instrumentation__c = true;
        machineStartupForm.Review_Foreword__c = false;
        machineStartupForm.Review_Heat_Exchangers_Oil_Air__c = true;
        machineStartupForm.Review_Inlet_Control_Valve__c = true;
        machineStartupForm.Review_Installation__c = true;
        machineStartupForm.Review_Maintenance_Schedule__c = true;
        machineStartupForm.Review_Minimum_Pressure_Valve__c = false;
        machineStartupForm.Review_Motor_Lubrication__c = false;
        machineStartupForm.Review_Pressure_Relief_Valve__c = false;
        machineStartupForm.Review_Shaft_Coupling__c = true;
        machineStartupForm.Review_Starting_and_Operating_Procedures__c = false;
        machineStartupForm.Review_Troubleshooting__c = false;
        machineStartupForm.Review_Ventilation_Fans__c = true;
        machineStartupForm.Room_ventilation_adequate_for_compressor__c = false;
        machineStartupForm.Safety_Instructions__c = false;
        machineStartupForm.Separator_press__c = 'Separator press';
        machineStartupForm.Separator_temp__c = 'Separator temp';
        machineStartupForm.I_D_No_1__c = 'I.D. No - Main Motor (1) Nameplate';
        machineStartupForm.I_D_No_2__c = 'I.D. Number - Main Motor (2) Nameplate';
        machineStartupForm.Serial_No_1__c = 'Serial Number - Package Nameplate';
        machineStartupForm.Serial_No_2__c = 'Serial No. - VFD (1) Nameplate';
        machineStartupForm.Serial_No_VFD_2__c = null;
        machineStartupForm.Serial_No_VFD_3__c = null;
        machineStartupForm.Set_vacuum_generator_to_5mbar__c = false;
        machineStartupForm.Signature_Capture__c = null;
        machineStartupForm.Choose_a_Startup_Form_Type_below__c = null;
        machineStartupForm.Startup_Claim_Submitted_By__c = null;
        machineStartupForm.Startup_Performed_By_User_DONT_MIGRATE__c = null;
        machineStartupForm.Startup_Performed_By__c = 'Chris Phillips';
        machineStartupForm.Startup_Performed_By_Number__c = null;
        machineStartupForm.Submitted_to_SAP__c = false;
        machineStartupForm.Submitted_to_SAP_Date_Time__c = null;
        machineStartupForm.Supply_Voltage__c = 'Supply Voltage - Package Nameplate';
        machineStartupForm.Surface_makes_100_contact_with_bottom_o__c = false;
        machineStartupForm.Surface_supports_package_weight__c = true;
        machineStartupForm.Target_pressure__c = 'Target Pressure';
        machineStartupForm.TOTAL_MOTOR_CURRENT__c = 'TOTAL MOTOR CURRENT';
        machineStartupForm.TOTAL_MOTOR_POWER__c = 'TOTAL MOTOR POWER';
        machineStartupForm.Transformer_wired_and_fused_for_supply__c = false;
        machineStartupForm.Type_of_Business__c = 'Type of Business';
        machineStartupForm.Unit_installed_away_from_dirt__c = false;
        machineStartupForm.Unit_started_and_performing_well__c = 'Yes';
        machineStartupForm.Unload_pressure__c = 'Unload Pressure';
        machineStartupForm.Updated_Software_to_Latest_Version__c = false;
        machineStartupForm.Vacuum_generator_line_moved_to_dry__c = false;
        machineStartupForm.Verify_that_a_set_of_manuals_and_diagram__c = false;
        machineStartupForm.Cat_No__c = 'Cat No - VFD (1) Nameplate';
        machineStartupForm.Cat_No_2__c = null;
        machineStartupForm.Cat_No_3__c = null;
        machineStartupForm.Volts_1__c = 'Volts - Main Motor (1) Nameplate';
        machineStartupForm.Volts_2__c = 'Volts - Main Motor (2) Nameplate';
        machineStartupForm.V1_dc_main_motor_1__c = 'V1 dc (main motor 1)';
        machineStartupForm.V1_freq_main_motor_1__c = 'V1 freq (main motor 1)';
        machineStartupForm.V1_temp_vfd_sink__c = 'V1 temp (vfd sink)1';
        machineStartupForm.V1_temp_vfd_sink_2__c = 'V1 temp (vfd sink)2';
        machineStartupForm.V1_version_firmware__c = null;
        machineStartupForm.V2_dc_main_motor_2__c = 'V2 dc (main motor 2)';
        machineStartupForm.V2_freq_main_motor_2__c = 'V2 freq (main motor 2)';
        machineStartupForm.V2_temp_vfd_sink__c = 'V2 temp (vfd sink)1';
        machineStartupForm.V2_temp_vfd_sink_2__c = 'V2 temp (vfd sink)2';
        machineStartupForm.V3_dc_main_motor_3__c = 'V3 dc (main motor 3)';
        machineStartupForm.V3_freq_main_motor_3__c = 'V3 freq (main motor 3)';
        machineStartupForm.V3_temp_vfd_sink__c = 'V3 temp (vfd sink)1';
        machineStartupForm.V3_temp_vfd_sink_2__c = 'V3 temp (vfd sink)2';
        machineStartupForm.VL1_L2_Off__c = 'V L1-L2 (off)';
        machineStartupForm.VL1_L2_On__c = 'V L1-L2 (on)';
        machineStartupForm.VL2_L3_Off__c = 'V L2-L3 (off)';
        machineStartupForm.VL2_L3_On__c = 'V L2-L3 (on)';
        machineStartupForm.VL3_L1_Off__c = 'V L3-L1 (off)';
        machineStartupForm.VL3_L1_On__c = 'V L3-L1 (on)';
        machineStartupForm.Water_pressure__c = 'Water pressure';
        machineStartupForm.Water_PSIG_between_40_75_PSIG_at_full_ra__c = false;
        machineStartupForm.Water_Quality_Checked_and_Acceptable__c = false;
        machineStartupForm.Water_shutoff_valve_installed__c = true;
        machineStartupForm.Will_ambient_cond_get_below_freezing__c = 'Yes';
        machineStartupForm.Wire_Size_Electrical_Code_Followed__c = false;
        machineStartupForm.Wired_According_to_NEC__c = false;
        
        return machineStartupForm;
    }
}