@isTest//(SeeAllData=True)
private class C_CommunityControllers_MasterTests {
    
    @testSetup
    private static void setupData(){
        Account acct = new Account(Name = 'Test Account');
        insert acct;
        
		
        Contact c = new Contact(LastNAme = 'Test', phone='555-555-5555', accountID=acct.id);
        insert c;
        
        
        Asset a = new Asset(
            AccountId = acct.Id,
            Name='TestAsset',
            Warranty_End_Date__c=System.today().addDays(7),
            char1__c='Variants condition',
            char2__c='NEMA',
            char3__c='Pump Model',
            char4__c='Approximate Weight (lbs)',
            char5__c='Tank Drain',
            char6__c='California Code',
            char7__c='Voltage',
            char8__c='Motor Enclosure',
            char9__c='Motor Efficiency',
            char10__c='Control Type',
            char11__c='Motor Option',
            char12__c='Starter Option',
            char13__c='Vibration Isolators',
            char14__c='Control Voltage',
            char15__c='Net weight of item',
            char16__c='Branding Specification',
            char17__c='Phase',
            char18__c='Tank Option',
            char19__c='Tank Type',
            char20__c='Massachusetts Code Fittings',
            char21__c='High Temperature Switch',
            char22__c='Motor RPM',
            char23__c='Aftercooler Option',
            char24__c='Intake Isolation Valve',
            char25__c='Distribution channel',
            char_Text1__c='60_2HP_1PH',
            char_Text2__c='1',
            char_Text3__c='2MTOII',
            char_Text4__c='390',
            char_Text5__c='Standard Tank Drain',
            char_Text6__c='No',
            char_Text7__c='230',
            char_Text8__c='Open Drip Proof',
            char_Text9__c='Standard',
            char_Text10__c='Start / Stop',
            char_Text11__c='Included',
            char_Text12__c='Not Included',
            char_Text13__c='No',
            char_Text14__c='230',
            char_Text15__c='390.000',
            char_Text16__c='Gardner Denver Finish',
            char_Text17__c='Single',
            char_Text18__c='(1) 60 gallon tank',
            char_Text19__c='Standard',
            char_Text20__c='No',
            char_Text21__c='No',
            char_Text22__c='1800',
            char_Text23__c='No Aftercooler',
            char_Text24__c='No',
            char_Text25__c='CM'
            //Current_Servicer__c =acct.id
        );
        
        insert a;
    }
    
    private static testmethod void testSerialNumberConfig(){
        Asset a = [SELECT Id FROM Asset WHERE Name = 'TestAsset'];
        List<serialNumberConfigController.configTuple> vals = serialNumberConfigController.getValues(a.Id);
    }
    
    private static testmethod void testC_WarrantyInfo(){
        Asset a = [SELECT Id FROM Asset WHERE Name = 'TestAsset'];
        Asset res = C_WarrantyInformation_Controller.queryAsset(a.Id);
    }
    
    private static testmethod void testC_ServiceHistory(){
        Asset a = [SELECT Id FROM Asset WHERE Name = 'TestAsset'];
        boolean res_bool = C_ServiceHistory_Controller.getIsReadOnly();
        Asset res_asset = C_ServiceHistory_Controller.queryAsset(a.Id);
        List<Service_History__c> res_history = C_ServiceHistory_Controller.queryServiceHistory(a.Id);
        boolean res_bool2 = C_ServiceHistory_Controller.saveAsset(a);
    }
    
    private static testmethod void testC_Report(){
        
        Test.startTest();
        List<Folder> folders = C_Report_Controller.getAvailableFolders();
        List<C_Report_Controller.reportWrap> res = C_Report_Controller.getWrappers();
        C_Report_Controller.reportWrap cp=new C_Report_Controller.reportWrap (null,null);
        Test.stopTest();
    }
}