@isTest
private class TSTC_EnosixInventorySearch
{
    public class ThisException extends Exception {}

    public class MockSBO_EnosixMaterialInventorySync_Search implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        Boolean isSuccess;

        public MockSBO_EnosixMaterialInventorySync_Search(Boolean success) {
            isSuccess = success;
        }

		public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
		{
            if (!isSuccess) {
                throw new ThisException();
            }

            SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC searchContext = (SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC)sc;
            SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR searchResult =
                new SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR();

            if (isSuccess)
            {
                SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT result = new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();

                result.DistributionChannel = 'X';
                result.Material = 'X';
                result.MaterialDescription = 'X';
                result.MaterialFamily = 'X';
                result.SalesOrganization = 'X';
                result.Scheduled = 0;
                result.Stock = 0;
                result.UOM = 'X';

                searchResult.SearchResults.add(result);
            }

            searchResult.setSuccess(isSuccess);
            searchContext.baseResult = searchResult;

			return searchContext;
        }
    }

    @isTest
    public static void testSearchSAPSuccess() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMaterialInventorySync_Search.class, new MockSBO_EnosixMaterialInventorySync_Search(true));

        CTRL_EnosixInventorySearch.doSearchSAP('X', 'X');
        CTRL_EnosixInventorySearch.doSearchSAP(null, null);
    }
    @isTest
    public static void testSearchLocalSuccess() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMaterialInventorySync_Search.class, new MockSBO_EnosixMaterialInventorySync_Search(true));

        Product2 prod = new Product2(Name = 'Laptop X200', FLD_Distribution_Channel__c = 'X', isActive = true, SAP_Stock__c = 15,
                                     Family = 'Hardware', ProductCode = 'ABCDE');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        CTRL_EnosixInventorySearch.doSearchLocal('X', 'X');
        CTRL_EnosixInventorySearch.doSearchLocal(null, null);
    }
    @isTest
    public static void testSearchSuccess() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMaterialInventorySync_Search.class, new MockSBO_EnosixMaterialInventorySync_Search(true));

        CTRL_EnosixInventorySearch.doSearch('X', 'X');
        CTRL_EnosixInventorySearch.doSearch(null, null);
    }
    @isTest
    public static void testSearchFailure() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMaterialInventorySync_Search.class, new MockSBO_EnosixMaterialInventorySync_Search(false));

        CTRL_EnosixInventorySearch.doSearch('X', 'X');
    }
    @isTest
    public static void testCommunityUserInfo1() {
        CTRL_EnosixInventorySearch.getCommunityUserInfo(null);
    }
    @isTest
    public static void testCommunityUserInfo2() {
        
        Account acct = new Account();
        acct.Name = 'Test Account';
        insert acct;
        
        CTRL_EnosixInventorySearch.getCommunityUserInfo(acct.Id);
    }
    @isTest
    public static void testCommunityUserInfo3() {
        
        Account acct = new Account();
        acct.Name = 'Test Account';
        acct.Child_DCs__c = 'XX,YY,';
        acct.DC__c = 'XX - XXXXXXX';
        insert acct;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'Test Opportunity';
        opp.StageName = 'Prospecting';
        opp.CloseDate = System.today();
        
        insert opp;
        
        CTRL_EnosixInventorySearch.getCommunityUserInfo(opp.Id);
    }
    @isTest
    public static void testCreateQuote() {
        
        Account acct = new Account();
        acct.Name = 'Test Account';
        acct.Child_DCs__c = 'XX,YY,';
        acct.Account_Number__c='1234';
        acct.DC__c = 'CP - XXXXXXX';

        insert acct;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware', ProductCode = 'ABCDE');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT> selectedRows = new List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT>();
        
        SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT sr = new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();
        
        selectedRows.add(sr);
        
        sr.Material = 'ABCDE';
        sr.MaterialDescription = 'Test';
        sr.MaterialFamily = 'Test';
        sr.Stock = 1;
        sr.Scheduled = 1;
        sr.UOM = 'EA';
        sr.SalesOrganization = 'GDMI';
        sr.DistributionChannel = 'CP';
        
        CTRL_EnosixInventorySearch.doCreateQuote(acct.Id, 'CP', selectedRows);
    }
    @isTest
    public static void testCreateQuoteFailure() {
        CTRL_EnosixInventorySearch.doCreateQuote(null, 'CP', null);
    }
    @isTest
    public static void testMisc() {
        CTRL_EnosixInventorySearch.LABEL_VALUE_PAIR tester = new CTRL_EnosixInventorySearch.LABEL_VALUE_PAIR('test');
        
        tester = new CTRL_EnosixInventorySearch.LABEL_VALUE_PAIR('test', 'test');
    }
}