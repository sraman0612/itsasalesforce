/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 22/11/2021
* @description: Apex Handler for SFS_AssetTrigger
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number               Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001    SIF-175     22/11/2021      checkChildRecords method
============================================================================================================*/
public class SFS_AssetTriggerHandler {
    public static boolean deleteAssetInCPQ = false;
    
    Public static void checkChildRecords(List<Asset> assetList){
        try{
            List<Entitlement> entitlementList=[Select Id,AssetId from Entitlement where AssetId=:assetList[0].Id];
            List<Case> caseList=[Select Id from Case where AssetId=:assetList[0].Id];
            List<WorkOrder> woList=[Select Id,AssetId from WorkOrder where AssetId=:assetList[0].Id];
            List<WorkOrderLineitem> woliList=[Select Id,AssetId from WorkOrderLineitem where AssetId=:assetList[0].Id];
            List<CTS_Fluid_Sample__c> fluidSampleList=[Select Id,Asset__c from CTS_Fluid_Sample__c where Asset__c=:assetList[0].Id];
            List<CAP_IR_Reading__c> readingList=[Select Id,CAP_IR_Asset__c from CAP_IR_Reading__c where CAP_IR_Asset__c=:assetList[0].Id];
            List<ContentDocumentLink> filesList=[Select Id,LinkedEntityId from ContentDocumentLink where LinkedEntityId=:assetList[0].Id];
            
            If(assetList[0].IRIT_RMS_Flag__c==true){
                assetList[0].addError('You cannot delete a Connected Asset.');
            }
            If(entitlementList.size()>0 || caseList.size()>0 || woList.size()>0 
               || woliList.size()>0 || readingList.size()>0 || filesList.size()>0){
                   assetList[0].addError('All child records must be deleted before deleting Asset.');
               }
        }Catch(Exception e){}
    }
    
    //*****method to restrict upadate of asset name*****/
    
    public static void Assetnamechanged(List<Asset> AssetList,Map<Id,Asset> oldMap){
        Profile svcProf =[SELECT Id from Profile where Name ='SFS Service Technician'];
        User usr;
        if(test.isRunningTest()) {
            usr =[select Id,Profile.Name from User where ProfileId =:svcProf.Id Limit 1 ];
        }else{
            usr = [select Id,Profile.Name from User where Id = :userInfo.getUserId()];
        }
        for(Asset Asst : AssetList){
            Asset oldRecord=oldMap.get(Asst.Id);
            Id RentalRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('SFS_Asset_Rental').getRecordTypeId();
            If(Asst.CG_Org_Channel__c ==System.Label.CG_IR_Trigger && Asst.Name!=oldRecord.Name && usr.Profile.Name.toLowerCase()=='sfs service technician'){
                Asst.addError('Serial Number cannot be edited');
            }
            else if(Asst.CG_Org_Channel__c ==System.Label.CG_IR_Trigger && Asst.LocationId!=oldRecord.LocationId && usr.Profile.Name.toLowerCase()=='sfs service technician' && Asst.RecordTypeId!=RentalRecordTypeId){
                Asst.addError('Location cannot be edited');
            }
        }
    }
    
    //*****method to sync Rental Asset fields back to Oracle CPQ*****/
    //Created By -- Parag Bhatt
    //Date: 01/08/2022
    public static void syncRentalAssetFieldsOnOracleDB(List<Asset> newList, String DMLAction, Map<Id,Asset> oldMap){
       // if(newList == null){
         //   return;
        //}
        Set<Id> astIds = New Set<Id>();
		system.debug('newList-> ' + newList.size());

        Id RentalRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('SFS_Asset_Rental').getRecordTypeId();
        List<Asset> lstOfAssetRecordsToConvert = new List<Asset>();
        //START--Below changes made after updates from capgemini team by parag bhatt - 16/05/2023
        Set<string> setOfAssetRecords = new Set<string>{'Id','CPQ_Record_Id__c','Asset_product_name__c','SFS_Oracle_Frame_Type__c','Name','Rental_Type__c','Area__c','Division__c','Account_Name__c','SFS_ShippingCountry__c','District__c',
            'Primary_Account_Asset__c','SFS_ShippingStreet__c','SFS_ShippingCity__c','SFS_ShippingState__c','SFS_ShippingPostalCode__c','SFS_Account_Division__c','Status','IRIT_Frame_Type__c','SFS_Serial_Number__c','Manufacturer__c',
            'SFS_Rental_Division__c','CPQ_Part_Number__c','Rental_Part_Number__c','Skid_Mount__c','Weight__c','Quick_Connects__c','HP__c','Product_Line__c','Dryer_Type__c','Compressor1_Type__c','Model_Name__c','ProductDescription','Parent_Frame__c','SFS_Part_Number__c',
            'CTS_Voltage__c','PSI__c','CTS_CFM__c','AMPS__c','Air_Dis_Inch_NPT__c','Cable_Wire_Size__c','Hose_Size_Inches__c','Length_in__c','Width_in__c','Outdoor_Mod__c','IR_Rental_Contact_Email__c','IR_Rental_Con_Phone__c','IR_Rental_Contact__c','Operating_Status__c',
            'Monthly_Rate__c','Weekly_Rate__c','No_Of_Cable_Kits__c','No_Of_Hose_Kits__c','Height_in__c','Dimensions__c'};
      
       
        Boolean goodToProccessrecord = false;
        Asset astToTrigger;
        if(checkRecursive.runOnce()){
        for(Asset ast: newList){
            for(String astField : setOfAssetRecords){
                if(ast.recordtypeId == RentalRecordTypeId && (oldMap == null || (oldMap != null && (oldMap.get(ast.Id).get(astField) != ast.get(astField))) || ast.recordtypeId != oldMap.get(ast.Id).recordtypeId)) {
                    goodToProccessrecord = true;
                    astToTrigger = ast;
                    astIds.add(ast.Id);
                }
               
               // system.debug('recTyp-> ' + ast.recordtypeId + ' == ' + oldMap.get(ast.Id).recordtypeId);
                if(ast.recordtypeId != RentalRecordTypeId &&  (oldMap!=null && oldMap.get(ast.Id).recordtypeId == RentalRecordTypeId)) {
                    goodToProccessrecord = true;
                    astToTrigger = ast;
                    astIds.add(ast.Id);
                    DMLAction = 'delete';
                    system.debug('Marking as true-> ' + DMLAction); 
                }
            } 
            if(goodToProccessrecord){
                lstOfAssetRecordsToConvert.add(astToTrigger);
                string lstOfAssetData = JSON.serialize(lstOfAssetRecordsToConvert);
                  SFS_AssetDataIntegration.HttpRESTMethod('RentalSelectionOracleDataTable', astIds, DMLAction); //Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
               HttpMethod('RentalSelectionOracleDataTable', lstOfAssetData, DMLAction);
            	}
            }    
        }
    }
    
   @Future(callout=true)////Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
    public static void HttpMethod(String interfaceDetail, String stringOfAssetRecords, String DMLAction){
        
        //Http Method to send xml and recieve success or failure code.
        SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT Label,SFS_Endpoint_URL__c, SFS_Username__c, SFS_Password__c,SFS_ContentType__c
                                                      FROM SFS_SFS_Integration_Endpoint__mdt
                                                      WHERE Label =: interfaceDetail];
        
        //Encoding credentials for Authorization Header
        Blob headervalue = Blob.valueOf(endpoint.SFS_Username__c + ':' + endpoint.SFS_Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        String endpointUrl = '';
        List<Asset> lstOfAssetRecordToUpdate = (List<Asset>)JSON.deserialize(stringOfAssetRecords, List<Asset>.class);
        
        //Null checks for Division and Branch By Sarath 
        
        List<Asset> AsslistToUpdate = new List<asset>();
        //Create Asset JSON Data
        for(Asset ast: lstOfAssetRecordToUpdate){
            DMLAction = String.isNotBlank(ast.CPQ_Record_Id__c) ? 'update': 'create';
         
            string modelName = ast.Model_Name__c;
            String primaryAssetValue = 'No';
            if(modelName != null){
            if(modelName.contains('"')){
                system.debug('reached here @@@#####');
                modelName = modelName.replaceAll('"','\'\'');
            }}
            system.debug('modelName@@--->'+modelName);
            if(ast.Primary_Account_Asset__c){
                primaryAssetValue = 'Yes';
            }
            
            string productLine = ast.Product_Line__c;
            
            string rentalPartNumberNew = (productLine != null ? 'RENTAL'+productLine.substring(0, 3) : null);
            
            system.debug('deleteAssetInCPQ-> ' + deleteAssetInCPQ);
            
            String modlNo = ast.Model_Name__c==null?'':ast.Model_Name__c.replace('"', '\\"');
            decimal amps = ast.AMPS__c==null?0:ast.AMPS__c;
          //  DMLAction = 'create';
            String jsonResponse=        '{'+
                '  "documents": {'+
                '    "items": ['+
                '      {'+
                '        "id":"'+ast.CPQ_Record_Id__c+'",'+
                '        "Unique_ID": "'+ast.id+'",'+
                '        "Asset_ID": "'+ast.Name+'",'+
                '        "Serial_Number": "'+ast.SFS_Serial_Number__c+'",'+
                '        "Rental_Type": "'+ast.Rental_Type__c+'",'+
                '        "Area": "'+ast.Area__c +'",'+
                '        "Division": "'+ast.SFS_Account_Division__c +'",'+
                '        "Rental_Division": "'+ast.SFS_Rental_Division__r.Name+'",'+
                '        "Corp": "'+ast.Account_Name__c +'",'+ 
                '        "Account": "'+ast.Account_Name__c +'",'+ 
                '        "Country": "'+ast.SFS_ShippingCountry__c +'",'+ 
                '        "District": "'+ast.District__c +'",'+ 
                // '        "Branch": "'+ast.Branch__c +'",'+ 
                // '        "Service_Location": "'+ast.Service_Location__c +'",'+
                '        "Primary": "'+primaryAssetValue +'",'+
                '        "Street": "'+ast.SFS_ShippingStreet__c +'",'+ 
                '        "City": "'+ast.SFS_ShippingCity__c +'",'+ 
                '        "State": "'+ast.SFS_ShippingState__c +'",'+ 
                '        "Zip": "'+ast.SFS_ShippingPostalCode__c +'",'+ 
                // '        "Ship_To_ID": "'+ast.Ship_To_ID__c  +'",'+ 
                '        "Status": "'+ast.Operating_Status__c+'",'+ 
                // '        "Site_Use_ID": "'+ast.GES_Site__c+'",'+ 
                '        "Manufacturer": "'+ast.Manufacturer__c+'",'+ 
                //'        "Model": "'+modelName+'",'+
                '        "Product": "'+ast.Asset_product_name__c+'",'+ 
                '        "Product_Description": "'+ast.ProductDescription+'",'+ 
                '        "Parent_Frame": "'+ast.Parent_Frame__c+'",'+     
                '        "Frame_Type": "'+ast.IRIT_Frame_Type__c+'",'+ 
                // '        "Product_Type": "'+ast.Level_3__c+'",'+ 
                '        "Compressor_Type": "'+ast.Compressor1_Type__c+'",'+ 
                '        "Dryer_Type": "'+ast.Dryer_Type__c+'",'+ 
                '        "Product_Line": "'+ast.Product_Line__c+'",'+ 
                '        "SFS_Part_Number": "'+ast.SFS_Part_Number__c+'",'+ 
                '        "Part_Number": "'+ast.CPQ_Part_Number__c+'",'+ 
                '        "Part_Number_New": "'+rentalPartNumberNew+'",'+  
                '        "HP": '+ast.HP__c+','+ 
                '        "Voltage": '+ast.CTS_Voltage__c+','+ 
                '        "PSI": '+ast.PSI__c+','+ 
                '        "CFM": '+ast.CTS_CFM__c+','+ 
                //'        "AMPS": '+ast.AMPS__c+','+
                '        "Air_Dis_Inch_NPT": '+ast.Air_Dis_Inch_NPT__c+','+                 
                '        "Cable_Wire_Size": "'+ast.Cable_Wire_Size__c+'",'+                 
                '        "Hose_Size_Inches": '+ast.Hose_Size_Inches__c+','+     
                '        "Length": '+ast.Length_in__c+','+ 
                '        "Width": '+ast.Width_in__c+','+ 
                '        "Height": '+ast.Height_in__c+','+ 
                '        "No_Of_Hose_Kits": "'+ast.No_Of_Hose_Kits__c+'",'+                 
                '        "No_Of_Cable_Kits": "'+ast.No_Of_Cable_Kits__c+'",'+    
                '        "Weekly_Rate": '+ast.Weekly_Rate__c+','+ 
                '        "Monthly_Rate": '+ast.Monthly_Rate__c+','+ 
                '        "IR_Rental_Contact": "'+ast.IR_Rental_Contact__c+'",'+                 
                '        "IR_Rental_Con_Phone": "'+ast.IR_Rental_Con_Phone__c+'",'+  
                '        "IR_Rental_Con_Email": "'+ast.IR_Rental_Contact_Email__c+'",'+
                '        "Outdoor_Mod": "'+ast.Outdoor_Mod__c+'",'+  
                '        "Quick_Connects": "'+ast.Quick_Connects__c+'",'+  
                // '        "CableKits": "'+ast.CableKits__c+'",'+ 
                '        "Dimensions": "'+ast.Dimensions__c+'",'+
                '        "Weight": '+ast.Weight__c+','+
                '        "Skid_Mount":"'+ast.Skid_Mount__c+'",'+
                '        "Active":"Y",'+
                '        "Model":"'+modlNo+'",'+
                '        "AMPS":"'+amps+'",'+
                '		 "Shipping_Latitude":"'+ ast.Account.ShippingLatitude +'",'+
                '        "Shipping_Longitude":"'+ ast.Account.ShippingLongitude +'",'+
                '        "sync_action": "'+DMLAction+'"'+
                '      }'+
                '    ]'+
                '  }'+
                '}';
           AssetCPQFieldMapJsonClass obj = AssetCPQFieldMapJsonClass.parse(jsonResponse);//Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
           String JSONString = JSON.serialize(obj);//Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
            //String JSONString = 'Dummy';
            system.debug('@@JSONString@@--->'+JSONString);
            JSONString = JSONString.replaceAll('sync_action', '_sync_action');
            JSONString = JSONString.replaceAll('"null"', '" "');
            JSONString = JSONString.replaceAll('@@', ' ');
            JSONString = JSONString.replaceAll('##', '/');
            
            system.debug('@@JSONString After Change@@--->'+JSONString);
            
            //Http Request
            if(endpoint.Label =='RentalSelectionOracleDataTable'){
                endpointUrl = endpoint.SFS_Endpoint_URL__C;
                system.debug('---->endpointUrl'+endpointUrl);
                system.debug('Username---->'+endpoint.SFS_Username__c+'password--->'+endpoint.SFS_Password__c+'header'+ authorizationHeader + 'headerValue-->'+headervalue);                           
            }
            

            system.debug('call--> ' + DMLAction);
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpointUrl);
            request.setMethod('POST');
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('Content-Type',endpoint.SFS_ContentType__c);
            request.setBody(JSON.serialize(JSON.deserializeUntyped(JSONString)));
            request.setTimeout(120000);
            
            //Get Http Response
            If(!Test.isRunningTest()){
                system.debug('request--->'+request);
                HttpResponse response = http.send(request);
                System.debug('STATUS: ' + response.getStatusCode());
                System.debug('Response Body: ' +response.getBody());
                map<String,object> result =(map<String,object>)JSON.deserializeUntyped(response.getBody());
                System.debug('=================result==========='+result);
                String CPQRecordId;
                String AssetId;
                Map<String, Object> getDocuments = (Map<String, Object>)result.get('documents');
                 Map<String,object> ItemMap = new  Map<String,object>();
                Map<String , object>ItemsMap  = new Map<String , object>();
                Map<String , String> CPQAssetIdMap = new Map<String , String>();
                if(getDocuments !=null){
                List<Object> ItemList = (List<Object>)getDocuments.get('items');
                 System.debug('=================ItemList==========='+ItemList);
                    For(Object obj1 : ItemList){
                        ItemMap = (Map<String , Object>)obj1 ;
                        System.debug('obj1==='+ItemMap);

                    }
                }
                if(ItemMap !=null && !ItemMap.isEmpty()){
                System.debug('=================ItemMap==========='+ItemMap);
                CPQRecordId = String.valueOf(ItemMap.get('id'));
                AssetId= String.valueOf(ItemMap.get('Unique_ID'));
                System.debug('CPQRecordId==='+CPQRecordId);
                System.debug('AssetId==='+AssetId);
                    if(ast.CPQ_Record_Id__c == null || string.isEmpty(ast.CPQ_Record_Id__c)){
                 ast.CPQ_Record_Id__c = CPQRecordId;
                   AsslistToUpdate.add(ast) ;
                    }
                }
            }  
            
        }
        if(AsslistToUpdate !=null && AsslistToUpdate.size()>0){
           shouldIRun.stopTrigger();//Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
            update AsslistToUpdate;//Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
        }
    } 
    
    public static void createAssetRecords(List<Asset> AssetList){
        Id AssIds;
        Set<Id> AssFTypeIds=new Set<Id>();
        Id WorkTypeIds,Prodt;
        Boolean k1=false;
        Boolean k2=false;
        Boolean k4=false;
        Boolean k6=false;
        Boolean k8=false;
        Boolean k12=false;
        Boolean k16=false;
        Boolean k24=false;
        Boolean k48=false;
        Decimal Qunatity;
        String RecordTypeName;
        Map<Id,List<ProductRequired>> productRequiredMap=new Map<Id,List<ProductRequired>>();
        List<WorkType> workTypesList = new List<WorkType>();
        List<ProductRequired> partRequiredList = new List<ProductRequired>();
        if(AssetList.size()>0){
            for(Asset ast:AssetList){
                AssIds=ast.Id;
                AssFTypeIds.add(ast.CTS_Frame_Type__c);
            }
            if(AssFTypeIds.size()>0){
                workTypesList=[Select Id,Name,CTS_Frame_Type__c,SFS_Work_Scope1__c, (Select Id,ProductRequiredNumber,ParentRecordType,ParentRecordId,Product2Id,
                                                                                     Product2.Name,Product2.Part_Number__c,Product2.isActive,Product2.SFS_Oracle_Orderable__c, Product2.Description,
                                                                                     QuantityRequired,SFS_0k__c, SFS_1k__c, SFS_2k__c, SFS_4k__c, SFS_6k__c, SFS_8k__c, SFS_12k__c, SFS_16k__c, SFS_24k__c, 
                                                                                     SFS_48k__c,lastModifiedDate from ProductsRequired where Product2.SFS_Oracle_Orderable__c ='Y' and Product2.IsActive =true and QuantityRequired>0) from WorkType where CTS_Frame_Type__c=:AssFTypeIds ];
            }
            if(workTypesList.size()>0){
                List<CAP_IR_Asset_Part_Required__c> createdList=new List<CAP_IR_Asset_Part_Required__c>();
                for(WorkType wtList:workTypesList){
                    for(ProductRequired record:wtList.ProductsRequired){
                        CAP_IR_Asset_Part_Required__c newRecord=new CAP_IR_Asset_Part_Required__c();
                        newRecord.Product__c = record.Product2Id;
                        newRecord.SFS_Quantity__c = record.QuantityRequired;
                        RecordTypeName= record.ParentRecordType;
                        newRecord.SFS_1k__c = record.SFS_1k__c;
                        newRecord.SFS_2k__c = record.SFS_2k__c;
                        newRecord.SFS_4k__c = record.SFS_4k__c;
                        newRecord.SFS_6k__c = record.SFS_6k__c;
                        newRecord.SFS_8k__c = record.SFS_8k__c;
                        newRecord.SFS_12k__c = record.SFS_12k__c;
                        newRecord.SFS_16k__c = record.SFS_16k__c;
                        newRecord.SFS_24k__c = record.SFS_24k__c;
                        newRecord.SFS_48k__c = record.SFS_48k__c;
                        newRecord.Asset__c=AssIds;                        
                        if(RecordTypeName=='WorkType' || RecordTypeName=='Work Type'){
                            createdList.add(newRecord);
                        }
                    } 
                }
                if(createdList.size()>0){
                    insert createdList;      
                }
            }                           
        } 
}
    
}