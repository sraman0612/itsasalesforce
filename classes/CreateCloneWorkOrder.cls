public class CreateCloneWorkOrder {
    @invocableMethod(label = 'Update Work Order' description = 'Update Work Order' Category = 'WorkOrder')
    public static void updateWorkOrder(List<List<WorkOrder>> woList){
        String serviceConId;
        List<WorkOrder> newWoList = new List<WorkOrder>();
        if(woList.size() > 0 && woList[0] != NULL){
            newWoList = woList[0];            
        }
        Map<String,Id> mapWo = new Map<String,Id>();
         Map<String,Id> mapEnt = new Map<String,Id>();
        for(WorkOrder wo: newWoList){
            System.debug('(wo)'+wo.EntitlementId);
            mapWo.put(wo.EntitlementId, wo.Id);            
        }
        List<Entitlement> entList = [Select id, Clone_From__c from Entitlement Where Clone_From__c IN : mapEnt.keySet()];
        for(Entitlement e : entList){
            System.debug('(entList)'+entList);
            System.debug('(e12!@#)'+e);
            mapEnt.put(e.Clone_From__c, e.Id);
        }
         for(WorkOrder w: newWoList){
             System.debug('W+_+_'+w);
             if(mapEnt.containsKey(w.EntitlementId)){
                 w.EntitlementId = mapEnt.get(w.EntitlementId);
             }
        }
        if(newWoList.size() > 0){
           UPDATE newWoList;
        }        
    }
}