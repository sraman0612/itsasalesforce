/*
Paste the below code into the Anonymous Apex Window
To know how many total records are there to sync
========== START CODE ==========
new ensxsdk.Logger(null);
List<String> materialTypes = (List<String>)UTIL_AppSettings.getList('UTIL_MaterialSyncBatch.MaterialTypes', String.class, new List<String>{});
List<String> materialGroups = (List<String>)UTIL_AppSettings.getList('UTIL_MaterialSyncBatch.MaterialGroups', String.class, new List<String>{});
List<String> materialDivisions = (List<String>)UTIL_AppSettings.getList('UTIL_MaterialSyncBatch.Divisions', String.class, new List<String>{});

SBO_EnosixMatSync_Search.EnosixMatSync_SC searchContext = new SBO_EnosixMatSync_Search.EnosixMatSync_SC();
searchContext.pagingOptions.pageSize = 1;
searchContext.pagingOptions.pageNumber = 1;
if (materialTypes.size() > 0)
{
for (String matType : materialTypes)
{
SBO_EnosixMatSync_Search.MATERIAL_TYPE newMatType = new SBO_EnosixMatSync_Search.MATERIAL_TYPE();
newMatType.MaterialType = matType;
searchContext.MATERIAL_TYPE.add(newMatType);
}
}
if (materialGroups.size() > 0)
{
for (String matGroup : materialGroups)
{
SBO_EnosixMatSync_Search.MATERIAL_GROUP newMatGroup = new SBO_EnosixMatSync_Search.MATERIAL_GROUP();
newMatGroup.MaterialGroup = matGroup;
searchContext.MATERIAL_GROUP.add(newMatGroup);
}
}
if (materialDivisions.size() > 0)
{
for (String division : materialDivisions)
{
SBO_EnosixMatSync_Search.DIVISION newDivision = new SBO_EnosixMatSync_Search.DIVISION();
newDivision.Division = division;
searchContext.DIVISION.add(newDivision);
}
}
SBO_EnosixMatSync_Search sbo = new SBO_EnosixMatSync_Search();
sbo.search(searchContext);
System.debug(searchContext.result.isSuccess());
System.debug(searchContext.pagingOptions.totalRecords);
========== END CODE ==========
*/
public with sharing class UTIL_MaterialSyncBatch
implements Database.Batchable<Object>,
Database.AllowsCallouts,
Database.Stateful,
I_ParameterizedSync
{
    @TestVisible
    private static String SFSyncKeyField = 'SAP_Sync_Field__c';
    @TestVisible
    private static String BatchClassName = 'UTIL_MaterialSyncBatch';
    private static String ScheduleClassName = 'UTIL_MaterialSyncSchedule';
    private static Map<String, Schema.SObjectType> globalObjects;
    private static String cpqObject = 'sbqq__quote__c';
    private static Map<String, MPG4_Code__c> MPG4_Codes_Map = new Map<String, MPG4_Code__c>();
    private static Set<String> oemProducts = new Set<String>();
    
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_MaterialSyncBatch.class);
    public void logCallouts(String location)
    {
        if ((Boolean)UTIL_AppSettings.getValue(BatchClassName + '.Logging', false))
        {
            logger.enterVfpConstructor(location, null);
        }
    }
    
    // jobInfo contains debug information persisted across contexts since start(),
    // execute(), and finish() all run in separate contexts with separate debug logs
    private List<String> jobInfo = new List<String>();
    
    // In this case, we will store the largest change date/time as the param
    private UTIL_SyncHelper.LastSync fromLastSync = new UTIL_SyncHelper.LastSync();
    private static String ObjectType = 'Product2';
    private static String SAP_SalesOrg = (string) UTIL_AppSettings.getValue(BatchClassName + '.SalesOrg');
    private static List<String> List_SAP_DChainBlocks = (List<String>) UTIL_AppSettings.getList(BatchClassName + '.DChainBlocks',String.class);
    private static List<String> List_Excluded_SAP_DistChannels = (List<String>) UTIL_AppSettings.getList(BatchClassName + '.ExcludedDistributionChannels',String.class);
    private static Set<String> SAP_Excluded_DistChannels = new Set<String>();
    private Boolean initialSync = true;
    
    /* I_ParameterizedSync methods - setBatchParam() */
    public void setBatchParam(Object value)
    {
        this.fromLastSync = (UTIL_SyncHelper.LastSync) value;
    }
    /* end I_ParameterizedSync methods */
    
    // Sync Filter collections
    // Get the filters from the AppSettings
    private List<String> distributionChannels
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                BatchClassName + '.DistributionChannels', String.class, new List<String>{});
        }
    }
    private List<String> materialTypes
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                BatchClassName + '.MaterialTypes', String.class, new List<String>{});
        }
    }
    private List<String> materialGroups
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                BatchClassName + '.MaterialGroups', String.class, new List<String>{});
        }
    }
    private List<String> materialDivisions
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                BatchClassName + '.Divisions', String.class, new List<String>{});
        }
    }
    private Boolean createStandardPricebookEntries
    {
        get {
            return (Boolean)UTIL_AppSettings.getValue(
                BatchClassName + '.CreateStandardPricebookEntries');
        }
    }
    private Boolean createOEMPricebookEntries
    {
        get {
            return (Boolean)UTIL_AppSettings.getValue(
                BatchClassName + '.CreateOEMPricebookEntries');
        }
    }
    // End Sync filter collections
    
    /* Database.Batchable methods start(), execute(), and finish() */
    // start()
    //
    // Calls SBO and returns search results of update materials
    public List<Object> start(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.start');
        System.debug(context.getJobId() + ' started');
        
        SBO_EnosixMatSync_Search sbo = new SBO_EnosixMatSync_Search();
        SBO_EnosixMatSync_Search.EnosixMatSync_SC searchContext = new SBO_EnosixMatSync_Search.EnosixMatSync_SC();
        
        this.fromLastSync = UTIL_SyncHelper.getLastSyncFromTable(
            ScheduleClassName,
            this.fromLastSync);
        
        this.fromLastSync.pageNumber = this.fromLastSync.pageNumber + 1;
        
        if (this.fromLastSync.retryCnt == -1)
        {
            UTIL_SyncHelper.resetPage(this.fromLastSync, (Integer) UTIL_AppSettings.getValue(
                BatchClassName + '.SAPPageSize',
                9999));
        }
        if (this.fromLastSync.lastSyncDate != null)
        {
            searchContext.SEARCHPARAMS.DateFrom = this.fromLastSync.lastSyncDate;
            initialSync = false;
        }
        if (SAP_SalesOrg != null)
        {
            searchContext.SEARCHPARAMS.SalesOrganization = SAP_SalesOrg;
        }
        
        searchContext.pagingOptions.pageSize = this.fromLastSync.pageSize;
        searchContext.pagingOptions.pageNumber = this.fromLastSync.pageNumber;
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());
        System.debug('fromLastSync:' + this.fromLastSync.toString());
        
        if (this.distributionChannels.size() > 0)
        {
            for (String distributionChannel : this.distributionChannels)
            {
                SBO_EnosixMatSync_Search.DISTRIBUTION_CHANNEL newDistributionChannel = new SBO_EnosixMatSync_Search.DISTRIBUTION_CHANNEL();
                newDistributionChannel.DistributionChannel = distributionChannel;
                searchContext.DISTRIBUTION_CHANNEL.add(newDistributionChannel);
            }
        }
        if (this.materialTypes.size() > 0)
        {
            for (String matType : this.materialTypes)
            {
                SBO_EnosixMatSync_Search.MATERIAL_TYPE newMatType = new SBO_EnosixMatSync_Search.MATERIAL_TYPE();
                newMatType.MaterialType = matType;
                searchContext.MATERIAL_TYPE.add(newMatType);
            }
        }
        if (this.materialGroups.size() > 0)
        {
            for (String matGroup : this.materialGroups)
            {
                SBO_EnosixMatSync_Search.MATERIAL_GROUP newMatGroup = new SBO_EnosixMatSync_Search.MATERIAL_GROUP();
                newMatGroup.MaterialGroup = matGroup;
                searchContext.MATERIAL_GROUP.add(newMatGroup);
            }
        }
        if (this.materialDivisions.size() > 0)
        {
            for (String division : this.materialDivisions)
            {
                SBO_EnosixMatSync_Search.DIVISION newDivision = new SBO_EnosixMatSync_Search.DIVISION();
                newDivision.Division = division;
                searchContext.DIVISION.add(newDivision);
            }
        }
        
        // Execute the search
        SBO_EnosixMatSync_Search.EnosixMatSync_SR result;
        try
        {
            sbo.search(searchContext);
            result = searchContext.result;
        }
        catch (Exception ex)
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, ex, this.jobInfo);
        }
        
        // Write any response messages to the debug log
        String errorMessage = UTIL_SyncHelper.buildErrorMessage(BatchClassName, result.getMessages());
        
        if (!result.isSuccess())
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, new UTIL_SyncHelper.SyncException(errorMessage), this.jobInfo);
        }
        
        List<Object> searchResults = result.getResults();
        System.debug('Result size: ' + searchResults.size());
        
        // let finish() know to queue up another instance
        this.fromLastSync.isAnotherBatchNeeded = searchResults.size() > 0;
        this.fromLastSync.retryCnt = -1;
        
        this.jobInfo.add('searchResultsSize:' + searchResults.size());
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());
        
        // add distribution channels from the static resource file to the SAP_DistChannels <Set> for filtering
        for (String channel : List_Excluded_SAP_DistChannels) SAP_Excluded_DistChannels.add(channel);
        
        return searchResults;
    }
    
    // execute()
    //
    // Given the updated search results, does the work of updating the object table.
    public void execute(
        Database.BatchableContext context,
        List<Object> searchResults)
    {
        logCallouts(BatchClassName + '.execute');
        System.debug(context.getJobId() + ' executing');
        
        if (null == searchResults || 0 == searchResults.size()) return;
        if (null == globalObjects) globalObjects = Schema.getGlobalDescribe();
        Schema.SObjectType cpqObjectType = globalObjects.get(cpqObject);
        
        // Load map of MPG4 Codes to populate the related lookup field on the Asset object
        for (MPG4_Code__c mpg4Vals : [Select MPG4_Number__c,Id,Level_1__c,Level_2__c,Level_3__c From MPG4_Code__c]) {
            MPG4_Codes_Map.put(mpg4Vals.MPG4_Number__c, mpg4Vals);
        }
        
        List<SObject> errors = new List<SObject>();
        Set<String> configurableProductIds = new Set<String>();
        Map<String, Object> searchResultMap = createObjectKeyMap(searchResults);
        
        // First, update matching existing objects
        List<SObject> currentObjectList = UTIL_SyncHelper.getCurrentObjects(ObjectType, SFSyncKeyField, searchResultMap.keySet());
        List<SObject> updateObjectList = updateExistingObjects(searchResultMap, currentObjectList, errors, configurableProductIds);
        List<SObject> insertObjectList = createNewObjects(searchResultMap, currentObjectList, errors, configurableProductIds);
        Set<Id> savedIdSet = new Set<Id>();
        
        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Update', errors, savedIdSet, updateObjectList, BatchClassName, SFSyncKeyField);
        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Insert', errors, savedIdSet, insertObjectList, BatchClassName, SFSyncKeyField);
        
        if (null != cpqObjectType)
        {
            for (SObject obj : insertObjectList)
            {
                // Add the new Product Ids for Configurable Materials to the Set
                Product2 prod = (Product2) obj;
                if ((Boolean) prod.get('SBQQ__ExternallyConfigurable__c'))
                {
                    configurableProductIds.add(prod.Id);
                }
            }
        }
        
        if (savedIdSet.size() > 0) {
            List<PricebookEntry> insertPBEList = new List<PricebookEntry>();
            List<PricebookEntry> updatePBEList = new List<PricebookEntry>();
            
            // Standard Pricebook
            Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();
            List<PricebookEntry> pbEntryList = Database.Query(
                'SELECT Product2Id, Id, Name, UnitPrice, IsActive, Pricebook2Id FROM PricebookEntry ' +
                'WHERE Pricebook2Id =: pricebookId AND Product2Id IN :savedIdSet');
            Map<Id,PricebookEntry> savedPbeMap = new Map<Id,PricebookEntry>();
            for (PricebookEntry pbe : pbEntryList) {
                savedPbeMap.put(pbe.Product2Id, pbe);
            }
            for (Id savedId : savedIdSet) {
                PricebookEntry pbe = savedPbeMap.containsKey(savedId) ? savedPbeMap.get(savedId) : null;
                if (pbe == null || !pbe.isActive) {
                    if (pbe == null) pbe = new PricebookEntry();
                    pbe.isActive = true;
                    pbe.UnitPrice = 0;
                    pbe.UseStandardPrice = false;
                    if (pbe.Id == null) {
                        pbe.Pricebook2Id = pricebookId;
                        pbe.Product2Id = savedId;
                        insertPBEList.add(pbe);
                    } else {
                        updatePBEList.add(pbe);
                    }
                }
            }
            
            // OEM Pricebook
            if (createOEMPricebookEntries == true) {
                Id oemPriceBookId = [SELECT Id FROM Pricebook2 WHERE Name = 'OEM Pricebook' AND IsActive = true LIMIT 1].Id;
                
                system.debug('oemPriceBookId ===> '+oemPriceBookId);
                Map<Id,Product2> oemProdMap = new Map<Id,Product2>((Product2[])Database.query('SELECT Id, SAP_Sync_Field__c FROM Product2 WHERE SAP_Sync_Field__c IN :oemProducts'));
                Set<Id> oemKeys = new Set<Id>();
                oemKeys = oemProdMap.keyset();
                
                List<PricebookEntry> oemPbEntryList = Database.Query(
                    'SELECT Product2Id, Id, Name, UnitPrice, IsActive, Pricebook2Id FROM PricebookEntry ' +
                    'WHERE Pricebook2Id =: oemPriceBookId AND Product2Id IN :oemKeys');
                Map<Id,PricebookEntry> savedOemPbeMap = new Map<Id,PricebookEntry>();
                
                for (PricebookEntry pbe : oemPbEntryList) {
                    savedOemPbeMap.put(pbe.Product2Id, pbe);
                }
                
                for (String savedId : oemKeys) {
                    PricebookEntry pbe = savedOemPbeMap.containsKey(savedId) ? savedOemPbeMap.get(savedId) : null;
                    if (pbe == null || !pbe.isActive) {
                        if (pbe == null) pbe = new PricebookEntry();
                        pbe.isActive = true;
                        pbe.UnitPrice = 0;
                        pbe.UseStandardPrice = false;
                        if (pbe.Id == null) {
                            pbe.Pricebook2Id = oemPriceBookId;
                            pbe.Product2Id = savedId;
                            insertPBEList.add(pbe);
                        } else {
                            updatePBEList.add(pbe);
                        }
                    }
                }
            }
            
            UTIL_SyncHelper.insertUpdateResults('PriceBookEntry', 'Insert', errors, savedIdSet, insertPBEList, BatchClassName, null);
            UTIL_SyncHelper.insertUpdateResults('PriceBookEntry', 'Update', errors, savedIdSet, updatePBEList, BatchClassName, null);
            
            insertPBEList = new List<PricebookEntry>();
            updatePBEList = new List<PricebookEntry>();
            
            //Distribution Channel Pricebook
            List<Pricebook2> pbList = [SELECT Id, Name from Pricebook2 where Name in : distributionChannels];
            
            Map<String,Id> distributionChannelToPricebookIdMap = new Map<String,Id>();
            
            Set<Id> dcpbids = new Set<Id>();
            for (Pricebook2 pb : pbList) {
                distributionChannelToPricebookIdMap.put(pb.Name, pb.Id);
                dcpbids.add(pb.Id);
            }
            
            List<Product2> products = [SELECT Id, FLD_Distribution_Channel__c, SAP_Sync_Field__c from Product2 where Id in : savedIdSet];
            
            Map<Id,Id> productIdToPricebookIdMap = new Map<Id,Id>();
            
            for (Product2 product : products) {
                if (distributionChannelToPricebookIdMap.containsKey(product.FLD_Distribution_Channel__c)) {
                    productIdToPricebookIdMap.put(product.Id, distributionChannelToPricebookIdMap.get(product.FLD_Distribution_Channel__c));
                } else {
                    UTIL_SyncHelper.addLog(errors, 'ERROR', product.SAP_Sync_Field__c, Json.serialize(product), BatchClassName + ' ' + ObjectType + ' Pricebook not found for distribution channel ' + product.FLD_Distribution_Channel__c);
                }
            }
            
            List<PricebookEntry> dcPbEntryList = Database.Query(
                'SELECT Product2Id, Id, Name, UnitPrice, IsActive, Pricebook2Id FROM PricebookEntry ' +
                'WHERE Pricebook2Id IN : dcpbids AND Product2Id IN :savedIdSet');
            Map<Id,PricebookEntry> savedDcPbeMap = new Map<Id,PricebookEntry>();
            for (PricebookEntry pbe : dcPbEntryList) {
                savedDcPbeMap.put(pbe.Product2Id, pbe);
            }
            
            for (Id savedId : savedIdSet) {
                if (productIdToPricebookIdMap.containsKey(savedId)) {
                    PricebookEntry pbe = savedDcPbeMap.containsKey(savedId) ? savedDcPbeMap.get(savedId) : null;
                    if (pbe == null || !pbe.isActive) {
                        if (pbe == null) pbe = new PricebookEntry();
                        pbe.isActive = true;
                        pbe.UnitPrice = 0;
                        pbe.UseStandardPrice = false;
                        if (pbe.Id == null) {
                            pbe.Pricebook2Id = productIdToPricebookIdMap.get(savedId);
                            pbe.Product2Id = savedId;
                            insertPBEList.add(pbe);
                        } else {
                            updatePBEList.add(pbe);
                        }
                    }
                }
            }
            
            UTIL_SyncHelper.insertUpdateResults('PriceBookEntry', 'Insert', errors, savedIdSet, insertPBEList, BatchClassName, null);
            UTIL_SyncHelper.insertUpdateResults('PriceBookEntry', 'Update', errors, savedIdSet, updatePBEList, BatchClassName, null);
        }
        
        if (null != cpqObjectType && configurableProductIds.size() > 0)
        {
            // Insert a Product feature for Products that are configurable
            // Insert only if there are no product features for the configurable material
            List<SObject> currentProductFeatures = Database.query(
                'SELECT Id, SBQQ__ConfiguredSKU__c FROM SBQQ__ProductFeature__c WHERE SBQQ__ConfiguredSKU__c IN :configurableProductIds');
            
            Set<String> currentProductFeaturesProductId = new Set<String>();
            for (SObject feature : currentProductFeatures)
            {
                currentProductFeaturesProductId.add((String) feature.get('SBQQ__ConfiguredSKU__c'));
            }
            
            List<SObject> newProductFeatures = new List<SObject>();
            for (String productId : configurableProductIds)
            {
                if (currentProductFeaturesProductId.contains(productId)) continue;
                else
                {
                    SObject newProductFeature = globalObjects.get('sbqq__productfeature__c').newSObject();
                    newProductFeature.put('Name', 'SAP BoM');
                    newProductFeature.put('SBQQ__Number__c', 1);
                    newProductFeature.put('SBQQ__MinOptionCount__c', 0);
                    newProductFeature.put('SBQQ__OptionSelectionMethod__c', 'Dynamic');
                    newProductFeature.put('SBQQ__ConfiguredSKU__c', productId);
                    newProductFeatures.add(newProductFeature);
                }
            }
            
            UTIL_SyncHelper.insertUpdateResults('SBQQ__ProductFeature__c', 'Insert', errors, savedIdSet, newProductFeatures, BatchClassName, null);
        }
        
        UTIL_SyncHelper.insertUpdateResults('Error', 'Insert', errors, savedIdSet, errors, BatchClassName, null);
    }
    
    // finish()
    //
    // queues up another batch when isAnotherBatchNeeded is true
    public void finish(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.finish');
        System.debug(context.getJobId() + ' finished');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);
        if (this.fromLastSync.retryCnt >= 0)
        {
            System.debug('Retry=' + this.fromLastSync.retryCnt + ' ' + System.Now());
        }
        
        UTIL_SyncHelper.launchAnotherBatchIfNeeded(
            this.fromLastSync.isAnotherBatchNeeded, ScheduleClassName, this.fromLastSync);
        
        if (!this.fromLastSync.isAnotherBatchNeeded) Database.executeBatch(new UTIL_MaterialVCSyncBatch(),50);
    }
    
    private SBO_EnosixMatSync_Search.SEARCHRESULT getSboResult(Object searchResult)
    {
        return (SBO_EnosixMatSync_Search.SEARCHRESULT) searchResult;
    }
    
    // createObjectKeyMap()
    //
    // create map of product key / search result.
    private Map<String, Object> createObjectKeyMap(
        List<Object> searchResults)
    {
        Map<String, Object> result =
            new Map<String, Object>();
        
        for (Object searchResult : searchResults)
        {
            String key = getSboKey(searchResult);
            if (String.isNotEmpty(key))
            {
                result.put(key, searchResult);
            }
        }
        
        return result;
    }
    
    private List<SObject> updateExistingObjects(
        Map<String, Object> searchResultMap,
        List<SObject> currentObjectList,
        List<SObject> errors,
        Set<String> configurableProductIds)
    {
        List<SObject> updateObjectList = new List<SObject>();
        
        for (SObject currentObject : currentObjectList)
        {
            String syncKey = (String) currentObject.get(SFSyncKeyField);
            Object searchResult = searchResultMap.get(syncKey);
            
            // Updates fields and adds to objectList list for later commit
            syncObject(currentObject, searchResult, errors, updateObjectList, configurableProductIds);
        }
        
        System.debug('Existing Object Size: ' + updateObjectList.size());
        
        return updateObjectList;
    }
    
    private List<SObject> createNewObjects(
        Map<String, Object> searchResultMap,
        List<SObject> currentObjectList,
        List<SObject> errors,
        Set<String> configurableProductIds)
    {
        Set<String> existingObjectSyncKeys = new Set<String>();
        
        for (sObject currentObject : currentObjectList)
        {
            String syncKey = (String) currentObject.get(SFSyncKeyField);
            existingObjectSyncKeys.add(syncKey);
        }
        
        List<SObject> newObjectList = new List<SObject>();
        
        for (Object searchResult : searchResultMap.values())
        {
            if (!existingObjectSyncKeys.contains(getSboKey(searchResult)))
            {
                syncObject(null, searchResult, errors, newObjectList, configurableProductIds);
            }
        }
        
        System.debug('New Object Size: ' + newObjectList.size());
        
        return newObjectList;
    }
    
    private void syncObject(
        SObject currentObject,
        Object searchResult,
        List<SObject> errors,
        List<SObject> objectList,
        Set<String> configurableProductIds)
    {
        SBO_EnosixMatSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        if (sboResult.DeleteIndicator == 'X' || (initialSync == true && List_SAP_DChainBlocks.contains(sboResult.DistChainStatus) == true))
        {
            return;
        }
        // Code to filter by "Distribution Channel" when / if we are able to pull in product data that will contain values for the DC
        // This is not happening now because a material item can exist across multiple DCs so the search results are unpredictable
        // unless the SEARCHPARAMS are set for a specific "SalesOrganization" and "DistributionChannel" to limit the response
        if (String.isNotBlank(sboResult.DistributionChannel) && !SAP_Excluded_DistChannels.contains(sboResult.DistributionChannel))
        {
            if (currentObject == null)
            {
                currentObject = new Product2();
            }
        }
        else {
            return;
        }
        
        Product2 prod = (Product2) currentObject;
        String key = getSboKey(searchResult);
        prod.put(SFSyncKeyField,key);
        prod.isActive = sboResult.DeleteIndicator != 'X';
        prod.Name = sboResult.Material;
        if (String.isEmpty(prod.Name)) prod.Name = key;
        prod.FLD_Configurable_Material_Parent__c = sboResult.ConfigurableMaterial;
        prod.FLD_Configurable_Material__c = sboResult.ConfigurableMaterialFlag == 'X';
        prod.SBQQ__HidePriceInSearchResults__c = true;
        
        // This is for CPQ
        if (null != globalObjects.get(cpqObject))
        {
            prod.put('SBQQ__ExternallyConfigurable__c', prod.FLD_Configurable_Material__c);
            if (prod.FLD_Configurable_Material__c)
            {
                if (prod.Id != null) configurableProductIds.add(prod.Id);
                prod.put('SBQQ__ConfigurationType__c', 'Required');
                prod.put('SBQQ__ConfigurationEvent__c', 'Always');
            }
            else
            {
                prod.put('SBQQ__ConfigurationType__c', '');
                prod.put('SBQQ__ConfigurationEvent__c', '');
            }
        }
        
        // add product to OEM pricebook if needed
        if (sboResult.ProductAttribute6 == 'X') {
            oemProducts.add(key);
        }
        
        if (null != MPG4_Codes_Map.get(sboResult.Materialgroup4))
        {
            prod.put('MPG4_Code__c',MPG4_Codes_Map.get(sboResult.Materialgroup4).Id);
        }
        // Set existing product to inactive if it contains one of the status values included in the List_SAP_DChainBlocks
        if (initialSync == false && List_SAP_DChainBlocks.contains(sboResult.DistChainStatus) == true)
        {
            prod.isActive = false;
        }
        
        prod.ProductCode = sboResult.Material;
        prod.ENSX_EDM__Material__c = sboResult.Material;
        prod.FLD_Variant_Material__c = sboResult.MaterialVariant == 'X';
        prod.OEM_Product_Only__c = sboResult.ProductAttribute6 == 'X';
        prod.FLD_Material_Type__c = sboResult.MaterialType;
        prod.FLD_Material_Type_Description__c = sboResult.MaterialTypeDescription;
        prod.FLD_Division__c = sboResult.Division;
        prod.FLD_Division_Description__c = sboResult.DivisionDescription;
        prod.FLD_Material_Group__c = sboResult.MaterialGroup;
        prod.FLD_Material_Group_Description__c = sboResult.MaterialGroupDescription;
        prod.QuantityUnitOfMeasure = sboResult.BaseUnitOfMeasure;
        prod.FLD_Sales_Unit__c = sboResult.SalesUnit;
        prod.FLD_Sales_Organization__c = sboResult.SalesOrganization;
        prod.FLD_Distribution_Channel__c = sboResult.DistributionChannel;
        prod.FLD_Dist_Chain_Status__c = sboResult.DistChainStatus;
        prod.FLD_Dist_Chain_Status_Date__c = sboResult.DistChainStatusDate;
        prod.FLD_Weight_Unit__c = sboResult.WeightUnit;
        prod.MG1__c = sboResult.Materialgroup1;
        prod.MG3__c = sboResult.Materialgroup3;
        prod.MG4__c = sboResult.Materialgroup4;
        prod.Description = sboResult.MaterialSalesText != null ? sboResult.MaterialSalesText.replace('<(>&amp;<)>', '&').replace('&amp;', '&') : null; // Solves issue resulting from an & in the Description field
        prod.Product_Hierarchy__c = sboResult.ProductHierarchy;
        prod.Delivering_Plant__c = sboResult.DeliveringPlant;
        objectList.add(prod);
    }
    
    private String getSboKey(Object searchResult)
    {
        SBO_EnosixMatSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        return sboResult == null ? '' : sboResult.Material +' - '+sboResult.DistributionChannel;
    }
}