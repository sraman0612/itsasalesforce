@isTest public class ensxtx_TSTU_VC_Perf {

   @isTest static void test_RunConfiguration()
   {
      ensxtx_ENSX_VCPricingConfiguration ensxVcPricingConfguration = new ensxtx_ENSX_VCPricingConfiguration();
      Map<String, String> values = new Map<String, String>();
      values.put('CharacteristicName','CharacteristicName');
      values.put('CharacteristicDescription','CharacteristicDescription');
      Test.startTest();
      ensxtx_UTIL_VC_Perf.RunConfiguration(ensxVcPricingConfguration, values);
      ensxtx_UTIL_VC_Perf.CreateValueForCharAndValue(new ensxtx_DS_VCMaterialConfiguration(new ensxtx_SBO_EnosixVC_Detail.EnosixVC()), null, null);
      ensxtx_DS_VCMaterialConfiguration newConfig = new ensxtx_DS_VCMaterialConfiguration(new ensxtx_SBO_EnosixVC_Detail.EnosixVC());
      ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES selectedValue = new ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES();
      newConfig.indexedSelectedValues.put('key', new List<ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES>{new ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES()});
      ensxtx_UTIL_VC_Perf.AssertSelectedValueSet(newConfig, 'key', 'value');
      Test.stopTest();
   }
}