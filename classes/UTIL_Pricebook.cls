//THIS UTIL CLASS IS A CLONE FROM PDE WITH AN ADDED METHOD

public class UTIL_Pricebook
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_Pricebook.class);

	// getStandardPricebookId()
	// 
	// Return the Id of the standard pricebook
    static Id standardPriceBookId = null;
	public static Id getStandardPriceBookId()
	{
        if (standardPriceBookId == null)
        {
            try
            {
                standardPriceBookId = Test.isRunningTest() ? Test.getStandardPriceBookId() : [SELECT Id FROM Pricebook2 WHERE IsStandard = true LIMIT 1].Id;
            }
            catch (Exception e)
            {
                System.debug(e);
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'There was an issue retrieving the standard pricebook. ' + e.getMessage()));
            }
        }
		return standardPriceBookId;
	}

    // getEntriesForPricebook()
    //
    // Return a map of Product2Id and PricebookEntry
    public static Map<Id, PricebookEntry> getEntriesForPricebook(Pricebook2 priceBook)
    {
        Map<Id, PricebookEntry> result = new Map<Id, PricebookEntry>();

        if (null != priceBook)
        {
            List<PricebookEntry> items = [
                SELECT Id, Name, UnitPrice,IsActive,Product2Id,Pricebook2Id 
                FROM PricebookEntry 
                WHERE Pricebook2Id =: priceBook.Id];

            Integer itemTot = items.size();
            for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
            {
                PricebookEntry item = items[itemCnt];
                result.put(item.Product2Id, item);
            }
        }
        return result;
    }

    // getEntriesForPricebook()
    //
    // Return a map of Product2Id and PricebookEntry
    // From the given set of materialNumbers
    public static Map<Id, PricebookEntry> getEntriesForPricebook(Pricebook2 priceBook, Set<String> materialNumbers)
    {
        Map<Id, PricebookEntry> result = new Map<Id, PricebookEntry>();

        if (null != priceBook)
        {
            Id pricebookId = priceBook.Id;
            List<String> materialNumbersQuoted = new List<String>();

            List<string> mnList = new List<string>(materialNumbers);
            Integer mnTot = mnList.size();
            for (Integer mnCnt = 0 ; mnCnt < mnTot ; mnCnt++)
            {
                string materialNumber = mnList[mnCnt];
                materialNumbersQuoted.add('\'' +  String.escapeSingleQuotes(materialNumber) + '\'');
            }

            string materialNumberList = string.Join( materialNumbersQuoted,', ');

            List<PricebookEntry> items = Database.Query(
                'SELECT Product2Id, Id, Name, UnitPrice, IsActive, Pricebook2Id ' +
                'FROM PricebookEntry ' +
                'WHERE Pricebook2Id =: pricebookId and IsActive = true ' +
                'AND Product2.' + UTIL_SFProduct.MaterialFieldName + ' IN ( ' + materialNumberList + ' )'
            );

            Integer itemTot = items.size();
            for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
            {
                PricebookEntry item = items[itemCnt];
                result.put(item.Product2Id, item);
            }
        }

        return result;
    }

    public static Map<Id, PricebookEntry> getEntriesForPricebookById(Id pricebookId, Set<String> materialNumbers)
    {
        logger.enterAura('getEntriesForPricebookById', new Map<String, Object> {
            'pricebookId' => pricebookId,
            'materialNumbers' => materialNumbers
        });

        Map<Id, PricebookEntry> result = new Map<Id, PricebookEntry>();
        try
        {
            if (null != pricebookId)
            {
                List<PricebookEntry> items = new List<PricebookEntry>();

                items = Database.Query(
                    'SELECT Product2Id, Id, Product2.' + UTIL_SFProduct.MaterialFieldName + ', Product2.Description, UnitPrice, IsActive, Pricebook2Id ' +
                    'FROM PricebookEntry ' +
                    'WHERE Pricebook2Id =: pricebookId ' +
                    'and IsActive = true ' +
                    'AND Product2.' + UTIL_SFProduct.MaterialFieldName + ' IN :materialNumbers'
                );

                Integer itemTot = items.size();
                for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
                {
                    PricebookEntry item = items[itemCnt];
                    result.put(item.Product2Id, item);
                }
            }
        } catch (Exception ex) {
            logger.log(LoggingLevel.ERROR, 'There was an exception thrown when attempting to get price book entries by material numbers', ex);
        } finally { 
            logger.exit();
        }

        return result;
    }

    // getProductIdToEntryMapForMaterials()
    //
    // Returns a map of Product2.Id => PricebookEntries for the given material numbers
    //
    // Compatible Pricebook Entries are in the Standard Price Book, are active, and do NOT use the
    // Standard Price (since the price comes from SAP and therefore it is always overridden at the
    // Opportunity Line level).
    public static Map<Id, PricebookEntry> getProductIdToEntryMapForMaterials(
        Set<String> materialNumbers, Id priceBookId, String currencyIsoCode)
    {
        Map<Id, PricebookEntry> result = new Map<Id, PricebookEntry>();

        if (null != priceBookId && null != materialNumbers && materialNumbers.size() > 0)
        {
            String materialField = UTIL_SFProduct.MaterialFieldName;
            List<PricebookEntry> items = Database.Query(
                'SELECT Product2Id, Product2.' + materialField +', Id ' +
                'FROM PricebookEntry ' +
                'WHERE Pricebook2Id = :priceBookId ' +
                'AND IsActive = true ' +
                'AND UseStandardPrice = false ' +
                'AND Product2.' + materialField + ' IN :materialNumbers'
            );

            Integer itemTot = items.size();
            for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
            {
                PricebookEntry item = items[itemCnt];
                result.put(item.Product2Id, item);
            }
        }

        return result;
    }

    // getMaterialToProductIdMapFromPricebookEntryMap()
    //
    // Returns a map of Material Number => Product2.Id
    public static Map<String, Id> getMaterialToProductIdMapFromPricebookEntryMap(
        Map<Id, PricebookEntry> productToPricebookEntryMap)
    {
        Map<String, Id> result = new Map<String, Id>();

        List<Id> keyList = new List<Id>(productToPricebookEntryMap.keySet());
        Integer keyTot = keyList.size();
        for (Integer keyCnt = 0 ; keyCnt < keyTot ; keyCnt++)
        {
            Id key = keyList[keyCnt];
            PricebookEntry item = productToPricebookEntryMap.get(key);
            String materialNumber = String.valueOf(item.Product2.get(UTIL_SFProduct.MaterialFieldName));
            result.put(materialNumber, key);
        }

        return result;
    }
}