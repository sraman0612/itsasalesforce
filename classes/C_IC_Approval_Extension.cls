public class C_IC_Approval_Extension {

    private Case theCase;
    public boolean isClosed { get; private set;}
    public List<Action_Item_Comment__c> comments {get; private set;}

    public C_IC_Approval_Extension(ApexPages.StandardController controller) {
        this.theCase = (Case) controller.getRecord();
        this.isClosed = this.theCase.isClosed;
        this.comments = [SELECT Id, Name, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate, Comment__c FROM Action_Item_Comment__c WHERE Case__c = :this.theCase.Id ORDER BY CreatedDate DESC];
    }

    public void approve(){
        this.theCase.Status = 'Closed';
        update this.theCase;
    }

    public void decline(){
    
    }

}