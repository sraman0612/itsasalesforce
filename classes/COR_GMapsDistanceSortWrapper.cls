global class COR_GMapsDistanceSortWrapper implements Comparable {
	
	public COR_GMapsDistanceFinder.GMapsDistance distanceMetric;
	
	public COR_GMapsDistanceSortWrapper (COR_GMapsDistanceFinder.GMapsDistance distanceMetric) {
		this.distanceMetric = distanceMetric;
	}
	
	
	global Integer compareTo (Object compareTo) {
		// Cast argument to OpportunityWrapper
        COR_GMapsDistanceSortWrapper compareToObject = (COR_GMapsDistanceSortWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (distanceMetric.distanceNumber > compareToObject.distanceMetric.distanceNumber) {
            // Set return value to a positive value.
            returnValue = 1;
        } 
        else if (distanceMetric.distanceNumber < compareToObject.distanceMetric.distanceNumber) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        else {
        	if (distanceMetric.durationNumber > compareToObject.distanceMetric.durationNumber) {
	            // Set return value to a positive value.
	            returnValue = 1;
	        } 
	        else if (distanceMetric.durationNumber < compareToObject.distanceMetric.durationNumber) {
	            // Set return value to a negative value.
	            returnValue = -1;
	        }
	        else {
	        	return 0;
	        }
        }
        
        return returnValue;   
	}
}