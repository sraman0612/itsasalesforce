/*=========================================================================================================
* @author Sean Gibbon, Capgemini
* @date 8/12/2022
* @description: Test class for SFS_ProductConsumed_Outbound_Handler.

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

===========================================================================================================*/
@IsTest 
public class SFS_Product_Consumed_Outbound_Test  {
    @isTest
    static void call_Product_Consumed_Test(){
       // httpResponse res = new httpResponse();
        	//res.setStatusCode(200);
        	//res.setBody('<Response><Status><StatusText><Status><G_TRANSACTIONS><MTL_TRANSACTIONS_INTERFACE><UNIQUE_REFERENCE_NO>123</UNIQUE_REFERENCE_NO><code>ABC</code><errorMessage>Error message 1</errorMessage></MTL_TRANSACTIONS_INTERFACE></G_TRANSACTIONS></Status></StatusText></Status></Response>');
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, true);
        //<?xml version="1.0" encoding="UTF-8"?>
       	/*Account billTo = SFS_TestDataFactory.getAccount();
        update billTo;
       	Account acc = SFS_TestDataFactory.getAccount();
        acc.Bill_To_Account__c = billTo.id;
        acc.Type = 'Prospect';
        update acc;*/
        
        /* List<Account> acc = SFS_TestDataFactory.createAccounts(2, false);
        acc[0].AccountSource='Web';
        acc[0].IRIT_Customer_Number__c='1234';
        acc[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acc[0].IRIT_Payment_Terms__c='BANKCARD';
        insert acc[0]; 
        
        acc[1].Bill_To_Account__c = acc[0].Id;
        acc[1].AccountSource='Web';
        acc[1].name = 'test account';
        acc[1].CurrencyIsoCode = 'USD';
        acc[1].IRIT_Customer_Number__c='1234';
        acc[1].Type = 'Prospect';
        insert acc[1]; */
        
        List<Account> acc=SFS_TestDataFactory.createAccounts(2,false);
        acc[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acc[0].IRIT_Payment_Terms__c='BANKCARD';
        insert acc[0];
        acc[1].Bill_To_Account__c=acc[1].Id;
        acc[1].Type='Prospect';
        acc[1].ShippingPostalCode='28759';
        acc[1].ShippingCity ='Montreat2';
        insert acc[1];
        
        List<Schema.Location> locList = SFS_TestDataFactory.createLocations(1, div[0].id, false);
        locList[0].CurrencyIsoCode = 'USD';
        locList[0].isInventoryLocation = true;
        insert locList[0];
      	
        System.debug('LOCATION Test Class: ' + loclist[0]);
        List<Product2> prodList = SFS_TestDataFactory.createProduct(1, true);
        prodList[0].productCode = '00250506';
       	update prodList[0];
        
        Id pb = Test.getStandardPricebookId();
        System.debug('StandardPB ' + pb);
		        
        List<PricebookEntry> pbe = SFS_TestDataFactory.createPricebookEntry(1, false);
        System.debug('PRODUCT 2: ' + pbe[0]);
        pbe[0].Pricebook2Id = pb;
        insert pbe[0];
       	
 //       Test.startTest();
        Pricebook2 servicePricebook = new Pricebook2();
        servicePricebook.IsActive = true;
        servicePricebook.Name = 'Service Pricebook Test';
            //[SELECT Id, isActive FROM Pricebook2 WHERE Name = 'Service Price Book'];
		System.debug('Pricebook Query: ' + servicePricebook);        
        insert servicePricebook;
        id pricebookx = servicePricebook.id;
        
       
       System.debug('ServPB ' + pricebookx);
        Product2 pro = new Product2(Name = 'testProduct');
        insert pro;
        //Inserting PricebookEntry

        	PricebookEntry entry = new PricebookEntry();
        	entry.Pricebook2Id = pricebookx;
        	entry.Product2Id = pro.Id;
        	entry.UnitPrice = 2.00;
        	entry.IsActive = true;
        	//entry.UseStandardPrice = true;
//        System.assertEquals(true, pricebookx != null); 
        
//        Test.stopTest();
        WorkType wType = new WorkType();
        wType.Name = 'TestWorkTypePC';
        wType.EstimatedDuration = 10.0;
        insert wType;
     
        
        //System.debug('Product Test Class'+ prod);
        
        List<WorkOrder> woList = SFS_TestDataFactory.createWorkOrder(1, acc[1].id, locList[0].id, div[0].id, null, false);
        //woList[0].Pricebook = pbe[0].Product2;
        woList[0].Pricebook2Id = pricebookx;
        insert woList[0];
        woList[0].Status = 'Complete';
        update woList[0];
        
        update LocList[0];
        
        System.debug('Work Order Test Class: ' + woList[0]);
        List<WorkOrderLineItem> woliList = SFS_TestDataFactory.createWorkOrderLineItem(1, woList[0].id, wType.Id, true);
        list<ProductItem> prodItmList = SFS_TestDataFactory.createProductItem(1, locList[0].id, pro.Id, true);
        ProductItem proditm = new ProductItem();
        
        
        test.startTest();
        
        ProductConsumed prdc = new ProductConsumed();
        prdc.QuantityConsumed = 1;
        prdc.PricebookEntryId = entry.id;
        //prdc.Product2Id = prodList[0].id;
        prdc.SFS_Integration_Status__c = 'APPROVED';
        prdc.SFS_Integration_Response__c = 'This is a test of Integration Response';
        prdc.ProductItemId = prodItmList[0].id;
		prdc.WorkOrderId = woList[0].id;
        insert prdc;
        
        


		
		List<id> prodIdList = new List<id>();
        prodIdList.add(prdc.id);
        String interfaceLabel = 'Product Consumed!' + String.valueOf(prodIdList);
        SFS_ProductConsumed_Outbound_Handler.run(prodIdList);
        
        try{
            // HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
             //Test.setMock(HttpCalloutMock.class, res);
        
             //HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
            // SFS_ProductConsumed_Outbound_Handler.run(woliList[0].Id);
        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);
         res.setBody('<envelope>' +
                          '<Response>' +
                              '<Status>' +
                                  '<StatusText>' +
                                      '<Status>' +
                                          '<G_TRANSACTIONS>' +
                                              '<MTL_TRANSACTIONS_INTERFACE>' +
                                                  '<UNIQUE_REFERENCE_NO>12345</UNIQUE_REFERENCE_NO>' +
                                                  '<code>SUCCESS</code>' +
                                                  '<errorMessage></errorMessage>' +
                                              '</MTL_TRANSACTIONS_INTERFACE>' +
                                          '</G_TRANSACTIONS>' +
                                      '</Status>' +
                                  '</StatusText>' +
                              '</Status>' +
                           '</Response>' +
                     '</envelope>'  );

             SFS_ProductConsumed_Outbound_Handler.IntegrationResponse(res, interfaceLabel);
            
             system.assert(true, res.getStatusCode() == 200);
             
            //system.assertEquals(200, res.getStatusCode());
            
           
        }catch(System.Exception e){
            
        }
        
        
       
        test.stopTest();
    }
    
    
    @isTest
    static void call_Product_Consumed_Test1(){
       // httpResponse res = new httpResponse();
        	//res.setStatusCode(200);
        	//res.setBody('<Response><Status><StatusText><Status><G_TRANSACTIONS><MTL_TRANSACTIONS_INTERFACE><UNIQUE_REFERENCE_NO>123</UNIQUE_REFERENCE_NO><code>ABC</code><errorMessage>Error message 1</errorMessage></MTL_TRANSACTIONS_INTERFACE></G_TRANSACTIONS></Status></StatusText></Status></Response>');
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, true);
        //<?xml version="1.0" encoding="UTF-8"?>
       	/*Account billTo = SFS_TestDataFactory.getAccount();
        update billTo;
       	Account acc = SFS_TestDataFactory.getAccount();
        acc.Bill_To_Account__c = billTo.id;
        acc.Type = 'Prospect';
        update acc;*/
        
       /*  List<Account> acc = SFS_TestDataFactory.createAccounts(2, false);
        acc[0].AccountSource='Web';
        acc[0].IRIT_Customer_Number__c='1234';
        acc[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acc[0].IRIT_Payment_Terms__c='BANKCARD';
        insert acc[0]; 
        
        acc[1].Bill_To_Account__c = acc[0].Id;
        acc[1].AccountSource='Web';
        acc[1].name = 'test account';
        acc[1].CurrencyIsoCode = 'USD';
        acc[1].IRIT_Customer_Number__c='1234';
        acc[1].Type = 'Prospect';
        insert acc[1]; */
        
        List<Account> acc=SFS_TestDataFactory.createAccounts(2,false);
        acc[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acc[0].IRIT_Payment_Terms__c='BANKCARD';
        insert acc[0];
        acc[1].Bill_To_Account__c=acc[1].Id;
        acc[1].Type='Prospect';
        acc[1].ShippingPostalCode='28759';
        acc[1].ShippingCity ='Montreat2';
        insert acc[1];
        
        List<Schema.Location> locList = SFS_TestDataFactory.createLocations(1, div[0].id, false);
        locList[0].CurrencyIsoCode = 'USD';
        locList[0].isInventoryLocation = true;
        insert locList[0];
      	
        System.debug('LOCATION Test Class: ' + loclist[0]);
        List<Product2> prodList = SFS_TestDataFactory.createProduct(1, true);
        prodList[0].productCode = '00250506';
       	update prodList[0];
        
        Id pb = Test.getStandardPricebookId();
        System.debug('StandardPB ' + pb);
		        
        List<PricebookEntry> pbe = SFS_TestDataFactory.createPricebookEntry(1, false);
        System.debug('PRODUCT 2: ' + pbe[0]);
        pbe[0].Pricebook2Id = pb;
        insert pbe[0];
       	
 //       Test.startTest();
        Pricebook2 servicePricebook = new Pricebook2();
        servicePricebook.IsActive = true;
        servicePricebook.Name = 'Service Pricebook Test';
            //[SELECT Id, isActive FROM Pricebook2 WHERE Name = 'Service Price Book'];
		System.debug('Pricebook Query: ' + servicePricebook);        
        insert servicePricebook;
        id pricebookx = servicePricebook.id;
        
       
       System.debug('ServPB ' + pricebookx);
        Product2 pro = new Product2(Name = 'testProduct');
        insert pro;
        //Inserting PricebookEntry

        	PricebookEntry entry = new PricebookEntry();
        	entry.Pricebook2Id = pricebookx;
        	entry.Product2Id = pro.Id;
        	entry.UnitPrice = 2.00;
        	entry.IsActive = true;
        	//entry.UseStandardPrice = true;
//        System.assertEquals(true, pricebookx != null); 
        
//        Test.stopTest();
        WorkType wType = new WorkType();
        wType.Name = 'TestWorkTypePC';
        wType.EstimatedDuration = 10.0;
        insert wType;
     
        
        //System.debug('Product Test Class'+ prod);
        
        List<WorkOrder> woList = SFS_TestDataFactory.createWorkOrder(1, acc[1].id, locList[0].id, div[0].id, null, false);
        //woList[0].Pricebook = pbe[0].Product2;
        woList[0].Pricebook2Id = pricebookx;
        insert woList[0];
        woList[0].Status = 'Complete';
        update woList[0];
        
        update LocList[0];
        
        System.debug('Work Order Test Class: ' + woList[0]);
        List<WorkOrderLineItem> woliList = SFS_TestDataFactory.createWorkOrderLineItem(1, woList[0].id, wType.Id, true);
        list<ProductItem> prodItmList = SFS_TestDataFactory.createProductItem(1, locList[0].id, pro.Id, true);
        ProductItem proditm = new ProductItem();
        
        
        test.startTest();
        
        ProductConsumed prdc = new ProductConsumed();
        prdc.QuantityConsumed = 1;
        prdc.PricebookEntryId = entry.id;
        //prdc.Product2Id = prodList[0].id;
        prdc.SFS_Integration_Status__c = 'APPROVED';
        prdc.SFS_Integration_Response__c = 'This is a test of Integration Response';
        prdc.ProductItemId = prodItmList[0].id;
		prdc.WorkOrderId = woList[0].id;
        insert prdc;
        
        prdc.SFS_Status__c = 'Canceled';
        prdc.SFS_Integration_Status__c = 'ERROR';
        update prdc;
        


		
		List <id> prodIdList = new List<id>();
        prodIdList.add(prdc.id);
        String interfaceLabel = 'Product Consumed!' + String.valueOf(prodIdList);
        SFS_ProductConsumed_Outbound_Handler.run(prodIdList);
        
        try{
             //HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
             //Test.setMock(HttpCalloutMock.class, res);
        
             //HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
            // SFS_ProductConsumed_Outbound_Handler.run(woliList[0].Id);
                    HttpResponse res = new HttpResponse();
        res.setStatusCode(200);
         res.setBody('<envelope>' +
                          '<Response>' +
                              '<Status>' +
                                  '<StatusText>' +
                                      '<Status>' +
                                          '<G_TRANSACTIONS>' +
                                              '<MTL_TRANSACTIONS_INTERFACE>' +
                                                  '<UNIQUE_REFERENCE_NO>12345</UNIQUE_REFERENCE_NO>' +
                                                  '<code>SUCCESS</code>' +
                                                  '<errorMessage></errorMessage>' +
                                              '</MTL_TRANSACTIONS_INTERFACE>' +
                                          '</G_TRANSACTIONS>' +
                                      '</Status>' +
                                  '</StatusText>' +
                              '</Status>' +
                           '</Response>' +
                     '</envelope>'  );

             SFS_ProductConsumed_Outbound_Handler.IntegrationResponse(res, interfaceLabel);
            
             system.assert(true, res.getStatusCode() == 200);
             
            //system.assertEquals(200, res.getStatusCode());
            
           
        }catch(System.Exception e){
            
        }
        
        
       
        test.stopTest();
    }
    
    
        
   
}