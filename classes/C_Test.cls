/* TO BE DELETED LATER */
@IsTest
public class C_Test{
    @testSetup
    static void setupData(){
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Contact cont = new Contact();
        cont.firstName = 'Can';
        cont.lastName = ' Pango';
        cont.email = 'service-gdi@canpango.com';
        insert cont;
        
        Case cse = new Case();
        cse.Account = acct;
        cse.Contact = cont;
        cse.Subject = 'Test Case';
        insert cse;
        
        Attachment att1 = new Attachment();
        att1.Name = 'Attachment Test';
        att1.ParentId = cse.Id;
        att1.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att1.ContentType = 'image/jpg';
        insert att1; 
        
        Email_Field_Map__c efm = new Email_Field_Map__c();
        efm.Name = 'Test Email_Field_Map__c';
        insert efm;

        
    }

    static testMethod void testPositive() {       
    
        Profile pro = [select id from profile where name LIKE 'System Administrator'];
        User u= new User(Alias = 'standard', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = pro.Id,TimeZoneSidKey='America/Los_Angeles', UserName='testUserB@testorganise.com');
        insert u; 
                
        Case c = [SELECT Id,AccountId, ContactId FROM Case];
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Internal Action Item' and SObjectType=:'Case' Limit 1];
                        
        Case cse1 = new Case();
        cse1.AccountId = c.AccountId;
        cse1.ContactId = c.ContactId;
        cse1.Subject = 'Test Case';
        cse1.ParentId= c.id;
        cse1.Send_to_User__c = u.id;  
        cse1.RecordtypeId = rt.id;      
        insert cse1;

        Attachment att = new Attachment();
        att.Name = 'Attachment Test';
        att.ParentId = cse1.Id;
        att.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att.ContentType = 'image/jpg';
        insert att;        
        
        Action_Item_Comment__c ac = new Action_Item_Comment__c();
        ac.Case__c = cse1.id;
        ac.Comment__c = 'test';
        insert ac;
        
               
        C_Action_Item_Resolution_Extension.saveImage('test','VGVzdFN0cmluZw==');
        
        Test.setCurrentPageReference(new PageReference('C_Action_Item_Resolution_Extension'));       
        System.currentPageReference().getParameters().put('ParentId', cse1.Id);        
        System.currentPageReference().getParameters().put('OriginatingId', cse1.Id);
        System.currentPageReference().getParameters().put('AccountId', c.AccountId);
        System.currentPageReference().getParameters().put('CommentId', ac.id);
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(cse1);        
        C_Action_Item_Resolution_Extension  cae = new C_Action_Item_Resolution_Extension(sc);     
                                       
        //cae.theCase.ContactId = c.Id;
        cae.theCase.Send_To_User__c = u.Id;
        for(C_Action_Item_Resolution_Extension.attachmentWrapper aw : cae.attachmentsWrapper){
            system.debug(' *** hello ***');
            aw.Include = true;
        }                            
        cae.AIeditorContent = 'Text<br/>Text<badhtmltag>';
        cae.theCase.Subject = 'Testing Case';
        boolean hpa = cae.hasParentAttachments;
        cae.editorContent = 'Test Editor Content';
        
        cae.sendContent();
        cae.save();             
        cae.cancel();  
                                                             
    }
    

    static testMethod void testNegative() {       
    
        Profile pro = [select id from profile where name LIKE 'System Administrator'];
        User u= new User(Alias = 'standard', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = pro.Id,TimeZoneSidKey='America/Los_Angeles', UserName='testUserB@testorganise.com');
        insert u; 
                
        Case c = [SELECT Id,AccountId, ContactId FROM Case];
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Internal Action Item' and SObjectType=:'Case' Limit 1];
                        
        Case cse1 = new Case();
        cse1.AccountId = c.AccountId;
        cse1.ContactId = c.ContactId;
        cse1.Subject = 'Test Case';
        cse1.ParentId= c.id;
        cse1.Status = 'Closed';
        cse1.Send_to_User__c = u.id;  
        cse1.RecordtypeId = rt.id;      
        insert cse1;
                             
        Test.setCurrentPageReference(new PageReference('C_Action_Item_Resolution_Extension'));       
        System.currentPageReference().getParameters().put('ParentId', cse1.Id);        
        System.currentPageReference().getParameters().put('OriginatingId', cse1.Id);
        System.currentPageReference().getParameters().put('AccountId', c.AccountId);
                
        ApexPages.StandardController sc = new ApexPages.StandardController(cse1);        
        C_Action_Item_Resolution_Extension  cae = new C_Action_Item_Resolution_Extension(sc);     
                                       
        //cae.theCase.ContactId = c.Id;
        cae.theCase.Send_To_User__c = u.Id;
        for(C_Action_Item_Resolution_Extension.attachmentWrapper aw : cae.attachmentsWrapper){
            system.debug(' *** hello ***');
            aw.Include = true;
        }                            
        cae.AIeditorContent = 'Text<br/>Text<badhtmltag>';
        cae.theCase.Subject = 'Testing Case';
        boolean hpa = cae.hasParentAttachments;
        cae.editorContent = 'Test Editor Content';
        
        cae.sendContent();
        cae.save();             
        cae.cancel();                       
    
    }    
    
    static testMethod void testAttachments() { 
    
        Case c = [SELECT Id,AccountId, ContactId FROM Case];
        
        //Insert emailmessage for case
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@abc.org';
        email.Incoming = True;
        email.ToAddress= 'test@xyz.org';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.ParentId = c.Id; 
        insert email; 
        
        Attachment att2 = new Attachment();
        att2.Name = 'Attachment Test';
        att2.ParentId = email.Id;
        att2.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att2.ContentType = 'image/jpg';
        insert att2;             
    }
    
    /*
    static testMethod void testCases() {    
        Account a =[Select Id from account];     
        contact c = [Select id from contact];          
        
        //trigger test
        Recordtype rt = [select id,DeveloperName from Recordtype where sObjectType='Case' and DeveloperName = 'Managed_Care'];
        System.debug(' *** '+rt.DeveloperName);
        
        Case cse1 = new Case();
        cse1.SuppliedEmail ='test@test.com';
        cse1.Description ='test';
        cse1.Subject ='test';
        cse1.Recordtypeid=rt.id;
        cse1.AccountId = a.id;
        cse1.ContactId = c.id;
        
        test.StartTest();
            insert cse1; 
        test.StopTest();
     } */ 
}