/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 07/03/2022
* @description: SFS_CreateWOfromRentalAgreement
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001     SIF-124  07/03/2022  Automatically create Work Orders from Rental Agreement
============================================================================================================================================================*/
public class SFS_CreateWOfromRentalAgreement {
    private static Set<Id> frameIdSets = new Set<Id>();
    private static List<Entitlement> entList = new List<Entitlement>();
    private static List<CTS_IOT_Frame_Type__c> frameTypesWT = new List<CTS_IOT_Frame_Type__c>();
    private static Map<Id,List<WorkType>> mapWorkType = new Map<Id,List<WorkType>>(); 
    private static final String DEL_RECORDTYPEID = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Delivery').getRecordTypeId();
    private static final String PU_RECORDTYPEID = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Pick Up').getRecordTypeId();
    private static final String CI_RECORDTYPEID = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Check In').getRecordTypeId();
    private static final String WO_RECORDTYPEID = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
    private static final List<WorkOrder> woList = new List<WorkOrder>();
    private static final List<WorkOrderLineItem> woliList = new List<WorkOrderLineItem>();
    
    @InvocableMethod(label='SFS_CreateWOfromRentalAgreement' description='Automatically create Work Orders from Rental Agreement' category='WorkOrder')
    public static void Svc(List<Id>  svc) {
        try{
            List<Entitlement> entList = [SELECT Id,ServiceContractId,SFS_FrameTypeIds__c,AssetId,Asset.ContactId,Asset.AccountId,Asset.Account.Bill_To_Account__c 
                                         from Entitlement where ServiceContractId != null AND SFS_FrameTypeIds__c != null AND ServiceContractId IN: svc];
            if(!(entList.isEmpty())){
                for(Entitlement ent : entList){
                    system.debug('enter1');
                    frameIdSets.add(ent.SFS_FrameTypeIds__c);
                    system.debug('frameIdSets'+frameIdSets);
                }
                frameTypesWT = [SELECT Id, (SELECT SFS_Delivery__c,SFS_Pickup__c,SFS_Check_In__c from Work_Types__r where (SFS_Delivery__c = true OR SFS_Pickup__c = true OR SFS_Check_In__c = true)) from CTS_IOT_Frame_Type__c 
                                where Id IN: frameIdSets];
                system.debug('frameTypesWT'+frameTypesWT);
                for(CTS_IOT_Frame_Type__c wt : frameTypesWT){
                    mapWorkType.put(wt.Id,wt.Work_Types__r);
                }
            }
            
            system.debug('mapWorkType'+mapWorkType.values());
            WorkOrder woDel;
            WorkOrder woPC;          
            if(mapWorkType.values().size() > 0){
                system.debug('for');
                system.debug('entList.size()'+entList.size());
                for(Integer i=0; i<entList.size(); i++){
                    Integer cp=0;
                    Integer d=0;
                    Id Account = entList[i].Asset.AccountId;
                    Id Contact = entList[i].Asset.ContactId;
                    Id BilltoAcc = entList[i].Asset.Account.Bill_To_Account__c;
                    system.debug('enterloop'+entList[i].SFS_FrameTypeIds__c);   
                    for(WorkType wts : mapWorkType.get(entList[i].SFS_FrameTypeIds__c)){
                        if(wts != null){
                            system.debug('wts'+wts.Id);
                            if(wts.SFS_Delivery__c == true){
                                d=d+1;
                                if(d==1){
                                    system.debug('woDev'+wts.Id);
                                    woDel = new WorkOrder(AssetId=entList[i].AssetId,ServiceContractId=entList[i].ServiceContractId,
                                                          AccountId=Account!=null ? Account: null,ContactId=Contact!=null ? Contact: null, 
                                                          SFS_Type_Text__c = 'Delivery'+ i,SFS_Bill_to_Account__c=BilltoAcc!=null ? BilltoAcc: null);
                                    woList.add(woDel);
                                    system.debug('woDel'+woDel);
                                    d++;
                                }
                                createWOLI(woDel,DEL_RECORDTYPEID,wts);
                            }
                            else if(wts.SFS_Pickup__c == true || wts.SFS_Check_In__c == true){
                                cp=cp+1;
                                if(cp==1){
                                    system.debug('woPc'+wts.Id);
                                    woPC = new WorkOrder(AssetId=entList[i].AssetId,ServiceContractId=entList[i].ServiceContractId,
                                                         AccountId=Account!=null ? Account: null,ContactId=Contact!=null ? Contact: null,
                                                         SFS_Bill_to_Account__c=BilltoAcc!=null ? BilltoAcc: null,
                                                         SFS_Type_Text__c = 'PickupCheckIn' + i);
                                    woList.add(woPC);
                                    system.debug('woPC'+woPC);
                                    cp++;
                                }
                                if(wts.SFS_Pickup__c == true){
                                    createWOLI(woPC,PU_RECORDTYPEID,wts);
                                }
                                else if(wts.SFS_Check_In__c == true){
                                    createWOLI(woPC,CI_RECORDTYPEID,wts);
                                }
                            }
                        }
                    }
                }
            }
            insert woList;
            system.debug('woList'+woList);
            for(WorkOrder woLi : woList){
                for(WorkOrderLineItem woliLi : woliList){
                    if(woLi.AssetId == woliLi.AssetId){
                        if(woliLi.RecordTypeId == DEL_RECORDTYPEID && woLi.SFS_Type_Text__c == woliLi.SFS_Type_Text__c){
                            woliLi.WorkOrderId = woLi.Id;
                        }
                        else if((woliLi.RecordTypeId == PU_RECORDTYPEID || woliLi.RecordTypeId == CI_RECORDTYPEID) && woLi.SFS_Type_Text__c == woliLi.SFS_Type_Text__c){
                            woliLi.WorkOrderId = woLi.Id;
                        }
                    }
                }
            }
            system.debug('woliList'+woliList);
            insert woliList;
        }
        catch(Exception e){
            system.debug('@@@exception'+e.getMessage());
        }
    }
    public static void createWOLI(WorkOrder wo,Id recordId,WorkType wts){
        WorkOrderLineItem woli = new WorkOrderLineItem();
        woli.RecordTypeId=recordId;
        woli.WorkOrderId=wo.Id;
        woli.WorkTypeId =wts.Id;
        woli.AssetId = wo.AssetId;
        woli.SFS_Type_Text__c = wo.SFS_Type_Text__c;
        woliList.add(woli);
    }
}