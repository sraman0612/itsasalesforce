@isTest
public with sharing class TSTU_CustomerSyncBatchCleanup
{

    @isTest
    public static void test_CustomerSyncBatchCleanup()
    {
        createAccounts();
        test.startTest();
        Database.executeBatch(new UTIL_CustomerSyncBatchCleanup());
        test.stopTest();
    }

    private static void createAccounts()
    {
        List<Account> newAccounts = new List<Account>();

        Account Account1 = new Account();
        Account1.Name = 'SAP Test 1';
        Account1.Account_Number__c = '1000150';
        Account1.ENSX_EDM__SAP_Customer_Number__c = null;
        Account1.Customer_group__c = null;
        newAccounts.add(Account1);

        Account Account2 = new Account();
        Account2.Name = 'SAP Test 2';
        Account2.Account_Number__c = '1000150 - CP';
        Account2.ENSX_EDM__SAP_Customer_Number__c = '1000150';
        Account2.Customer_group__c = '12 - END USER 2';
        Account2.Rep_Account__c = '222444';
        newAccounts.add(Account2);

        Account Account3 = new Account();
        Account3.Name = 'SAP Test 3';
        Account3.Account_Number__c = '1000150 - GI';
        Account3.ENSX_EDM__SAP_Customer_Number__c = '1000150';
        Account3.Customer_group__c = 'U1 - END USER 1';
        Account3.Rep_Account__c = '111333';
        Account3.DC__c = 'GM';
        newAccounts.add(Account3);

        Account Account4 = new Account();
        Account4.Name = 'SAP Test 4';
        Account4.Account_Number__c = '1000150 - SB';
        Account4.ENSX_EDM__SAP_Customer_Number__c = '1000150';
        Account4.Customer_group__c = 'U1 - END USER 1';
        newAccounts.add(Account4);

        Account Account44 = new Account();
        Account44.Name = 'SAP Test 44';
        Account44.Account_Number__c = '1000750 - SB';
        Account44.ENSX_EDM__SAP_Customer_Number__c = '1000750';
        Account44.Customer_group__c = 'U1 - END USER 1';
        Account44.Rep_Account__c = '222444';
        newAccounts.add(Account44);

        Account Account5 = new Account();
        Account5.Name = 'REP Test 1';
        Account5.Account_Number__c = '111333';
        Account5.ENSX_EDM__SAP_Customer_Number__c = null;
        Account5.Customer_group__c = null;
        newAccounts.add(Account5);

        Account Account6 = new Account();
        Account6.Name = 'REP Test 2';
        Account6.Account_Number__c = '222444';
        Account6.ENSX_EDM__SAP_Customer_Number__c = null;
        Account5.Customer_group__c = null;
        newAccounts.add(Account6);

        insert newAccounts;
    }
}