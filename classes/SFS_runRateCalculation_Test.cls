@isTest
public class SFS_runRateCalculation_Test {
    Static testmethod void testCheckChildRecords(){
        //RecordType assetRecordType = [Select Id, Name, SObjectType FROM RecordType where SObjectType = 'Asset' limit 1];
        List<Asset> assetList=SFS_TestDataFactory.createAssets(2,false); 
        assetList[0].SFS_WOLI_Last_Completed_Date__c=system.today();
        insert assetList[0];
        assetList[1].SFS_WOLI_Last_Completed_Date__c=system.today();
        //assetList[1].AccountId=acct.Id;
        insert assetList[1];
       // assetList.add(asset1);
        CAP_IR_Reading__c reading=new CAP_IR_Reading__c(CAP_IR_Asset__c=assetList[0].Id,CurrencyIsoCode='USD');
        insert reading;
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
        Account acct1 = new Account();
        acct1.name = 'test account1';
        acct1.Currency__c='USD';
        acct1.AccountSource='Web';
        acct1.IRIT_Customer_Number__c='2311';
        acct1.IRIT_Payment_Terms__c='NET 30';
        acct1.RecordTypeId=accRecID;
        insert acct1;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Type='Prospect';
        acct.Bill_To_Account__c=acct1.id;
        acct.IRIT_Customer_Number__c='1234';
        acct.Currency__c='USD';
        acct.Account_Region__c ='Oracle 11i';
        acct.AccountSource='Web';
        acct.IRIT_Customer_Number__c='2331';
        acct.IRIT_Payment_Terms__c='NET 30';
        insert acct;
        
        List<Division__c> div = new List<Division__c> ();
        Division__c div1 = new Division__c( 
            Name = 'TestLoc',
            SFS_Org_Code__c = 'ABC',
            Division_Type__c='Customer Center' ,
            EBS_System__c='Oracle 11i');
        insert div1;
        div.add(div1);
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        sc[0].SFS_Bill_To_Account__c=acct1.id;
        sc[0].SFS_External_Id__c='1234';
        update sc[0];
        List<MaintenancePlan> mpList=SFS_TestDataFactory.createMaintenancePlan(1,sc[0].Id,false);
        insert mpList;
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct.Id ,loc[0].Id, div[0].Id, sc[0].Id, false);
        //wo[0].MaintenancePlanId=mpList[0].Id;
        //wo[0].SuggestedMaintenanceDate=system.Today().addYears(1);
        wo[0].SFS_Bill_To_Account__c=acct1.id;
        insert wo;
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId=wo[0].Id,
                                                       SFS_Quote_Number__c='1236589',assetId=assetList[0].id);
        woli.Status='Completed';
       	woli.SFS_Asset_Run_Hours__c =35000;
        woli.SFS_Coolant_Changed__c='Yes';
        woli.SFS_Completed_Date__c=system.today();
        woliList.add(woli);
        //insert woli;
        WorkOrderLineItem woli1 = new WorkOrderLineItem(WorkOrderId=wo[0].Id,
                                                       SFS_Quote_Number__c='1236789',assetId=assetList[0].id);
        woli1.Status='Completed';
       	woli1.SFS_Asset_Run_Hours__c =3000;
        woli1.SFS_Coolant_Changed__c='Yes';
        Date complDate=Date.today().addYears(-1);
        woli1.SFS_Completed_Date__c=complDate;
        woliList.add(woli1);
        //insert woli1;
        WorkOrderLineItem woli2 = new WorkOrderLineItem(WorkOrderId=wo[0].Id,
                                                       SFS_Quote_Number__c='1236789',assetId=assetList[0].id);
        woli2.Status='Completed';
       	woli2.SFS_Asset_Run_Hours__c =1000;
        woli2.SFS_Coolant_Changed__c='Yes';
        Date complDate1=Date.today().addDays(-1);
        woli2.SFS_Completed_Date__c=complDate1;
        woliList.add(woli2);
        //insert woli2;
        WorkOrderLineItem woli3 = new WorkOrderLineItem(WorkOrderId=wo[0].Id,
                                                       SFS_Quote_Number__c='1236787',assetId=assetList[0].id);
        woli3.Status='Completed';
       	woli3.SFS_Asset_Run_Hours__c =500;
        woli3.SFS_Coolant_Changed__c='Yes';
        Date complDate3=Date.today().addDays(-2);
        woli3.SFS_Completed_Date__c=complDate1;
        woliList.add(woli3);
        //insert woli3;
        insert woliList;
        Test.startTest();
        WorkOrderLineItem woli4 = new WorkOrderLineItem(WorkOrderId=wo[0].Id,
                                                       SFS_Quote_Number__c='1236781',assetId=assetList[1].id);
        woli4.Status='Completed';
       	woli4.SFS_Asset_Run_Hours__c =3000;
        woli4.SFS_Coolant_Changed__c='Yes';
        Date complDate32=Date.today().addYears(-2);
        woli4.SFS_Completed_Date__c=complDate32;
        insert woli4;
        WorkOrderLineItem woli5 = new WorkOrderLineItem(WorkOrderId=wo[0].Id,
                                                       SFS_Quote_Number__c='1236782',assetId=assetList[1].id);
        woli5.Status='Completed';
       	woli5.SFS_Asset_Run_Hours__c =1000;
        woli5.SFS_Coolant_Changed__c='Yes';
        Date complDate4=Date.today().addDays(-1);
        woli5.SFS_Completed_Date__c=complDate4;
        insert woli5;

        WorkOrderLineItem woli6 = new WorkOrderLineItem(WorkOrderId=wo[0].Id,
                                                       SFS_Quote_Number__c='1236783',assetId=assetList[1].id);
        woli6.Status='Completed';
       	woli6.SFS_Asset_Run_Hours__c =1000;
        woli6.SFS_Coolant_Changed__c='Yes';
        Date complDate5=Date.today().addDays(-100);
        woli6.SFS_Completed_Date__c=complDate5;
        insert woli6;
        SFS_BatchAssetRunTimeCalculationWOLI obj = new SFS_BatchAssetRunTimeCalculationWOLI();
        DataBase.executeBatch(obj); 
        SFS_runRateCalculation sh1 = new SFS_runRateCalculation();
        String sch = '0 0 2 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1);
        Test.stopTest();
    }
}