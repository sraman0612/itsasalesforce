@isTest
public with sharing class EsaverReturnCtr_Test {

    public static final String MOCK_URL = 'https://dev-gdiconnectedcustomer.cs40.force.com/gdiconnectedcustomer';

    @testSetup
    static void createData(){

        Account acc = new Account(
            Name = 'TEST ACCOUNT',
            AccountNumber = '12345678',
            Account_Number__c='1234'
        );

        insert acc;

        Id pricebookId = Test.getStandardPricebookId();

        Opportunity opp = new Opportunity(
            AccountId = acc.Id,
            Name = 'oppName',
            Amount = 213230,
            CloseDate = Date.today(),
            StageName = 'Stage 1 - Qualified Lead',
            Pricebook2Id = pricebookId,
            LeadSource = 'Email',
            Probability = 25,
            TotalOpportunityQuantity = 27
        );

        insert opp;

        insert new SBQQ__Quote__c(
            SBQQ__Account__c = acc.Id,
            End_Customer_Account__c = acc.Id,
            SBQQ__Distributor__c = acc.Id,
            SBQQ__Opportunity2__c = opp.Id,
            Distribution_Channel__c = 'CM'
        );
    }

    @isTest
    static void successfulRedirect(){

        String quoteId = [SELECT Id FROM SBQQ__Quote__c LIMIT 1][0].Id,
                pdfId = '123123123';

        PageReference testPage = Page.EsaverReturn;
        testPage.getParameters().put(EsaverReturnCtr.QUOTE_ID_PARAM, quoteId);
        testPage.getParameters().put(EsaverReturnCtr.PDF_ID_PARAM, pdfId);

        System.Test.setCurrentPage(testPage);
        System.Test.startTest();
            EsaverReturnCtr controller = new EsaverReturnCtr();
            PageReference redirectURL = controller.init();
        System.Test.stopTest();

        System.assertEquals(
            MOCK_URL + EsaverReturnCtr.SBQQ_EDITOR + quoteId,
            redirectURL.getUrl()
        );
    }

    @IsTest
    static void unsuccessfulRedirect(){

        String quoteId = 'invalid_id',
                pdfId = '123123123';

        PageReference testPage = Page.EsaverReturn;
        testPage.getParameters().put(EsaverReturnCtr.QUOTE_ID_PARAM, quoteId);
        testPage.getParameters().put(EsaverReturnCtr.PDF_ID_PARAM, pdfId);

        System.Test.setCurrentPage(testPage);
        System.Test.startTest();
            EsaverReturnCtr controller = new EsaverReturnCtr();
            PageReference redirectURL = controller.init();
        System.Test.stopTest();

        System.assertEquals(
            null,
            redirectURL
        );
    }
}