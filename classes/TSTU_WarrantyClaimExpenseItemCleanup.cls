@IsTest
public class TSTU_WarrantyClaimExpenseItemCleanup {

    @IsTest
    public static void test() {
        Warranty_Claim__c warrantyClaim = new Warranty_Claim__c();
        warrantyClaim.SAP_Sales_Order_Number__c = 'SalesDocument';
        insert warrantyClaim;

        Warranty_Claim_Expenses__c warrantyClaimExpenses = new Warranty_Claim_Expenses__c();
        warrantyClaimExpenses.Warranty_Claim__c = warrantyClaim.Id;
        warrantyClaimExpenses.SAP_Item_Number__c = '000020';
        insert warrantyClaimExpenses;

        Database.executeBatch(new UTIL_WarrantyClaimExpenseItemCleanup());
    }
}