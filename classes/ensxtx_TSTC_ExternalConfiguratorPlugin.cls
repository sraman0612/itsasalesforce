@isTest public class ensxtx_TSTC_ExternalConfiguratorPlugin
{
    @isTest static public void test_getInfo_updateInfo()
    {
        ensxtx_TSTU_CPQ_TestSetup.createAccountQuoteLinked();
        ensxtx_TSTU_CPQ_TestSetup.createProduct2();

        Id accountId = [SELECT Id FROM Account].Id;
        Id quoteId = [SELECT Id FROM SBQQ__Quote__c].Id;
        Id productId = [SELECT Id FROM Product2].Id;

        Map<String, Object> p = new Map<String, Object> { 
            'quoteId' => quoteId, 
            'accountId' => accountId, 
            'productId' => productId, 
            'quoteSAPConfiguration' => 'TEST',
            'quoteShipTo' => '{"shipToParty": "TEST"}' };

        Test.startTest();
        try{
        String info = ensxtx_CTRL_ExternalConfiguratorPlugin.getInfo(p);
        ensxtx_CTRL_ExternalConfiguratorPlugin.updateInfo(p);
        }
        catch(exception e){
            
        }
        Test.stopTest();

        //System.assert(info != null);
    }

    @isTest static public void test_emptyCalls()
    {
        Test.startTest();
        ensxtx_CTRL_ExternalConfiguratorPlugin item = new ensxtx_CTRL_ExternalConfiguratorPlugin();
        item.declareDependencies();
        Test.stopTest();
    }
}