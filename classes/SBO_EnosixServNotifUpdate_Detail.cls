/// enosiX Inc. Generated Apex Model
/// Generated On: 12/18/2019 9:43:17 AM
/// SAP Host: From REST Service On: https://gdi--UAT.my.salesforce.com
/// CID: From REST Service On: https://gdi--UAT.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixServNotifUpdate_Detail extends ensxsdk.EnosixFramework.DetailSBO
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixServNotifUpdate_Detail_Meta', new Type[] {
            SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate.class
            } 
        );
    }

    public SBO_EnosixServNotifUpdate_Detail()
    {
        super('EnosixServNotifUpdate', SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate.class);
    }

    public override Type getType() { return SBO_EnosixServNotifUpdate_Detail.class; }

    public EnosixServNotifUpdate initialize(EnosixServNotifUpdate obj)
    {
        return (EnosixServNotifUpdate)this.executeInitialize(obj);
    }
    
    public EnosixServNotifUpdate getDetail(object key)
    {
        return (EnosixServNotifUpdate)this.executeGetDetail(key);
    }
    
    public EnosixServNotifUpdate save(EnosixServNotifUpdate obj)
    {
        return (EnosixServNotifUpdate) this.executeSave(obj);
    }

    public EnosixServNotifUpdate command(string command, EnosixServNotifUpdate obj)
    {
        return (EnosixServNotifUpdate) this.executeCommand(command, obj);
    }
    
    public with sharing class EnosixServNotifUpdate extends ensxsdk.EnosixFramework.DetailObject
    {
        public EnosixServNotifUpdate()
        {
            super('HEADER_DATA', new Map<string,type>
                {
                    
                });	
        }

        public override Type getType() { return SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixServNotifUpdate_Detail.registerReflectionInfo();
        }

                @AuraEnabled public String SalesDocument
        { 
            get { return this.getString ('VBELN'); } 
            set { this.Set (value, 'VBELN'); }
        }

    }

    //Write child objects
    }