global class SFS_BatchCreatePricebookEntryScheduler implements Schedulable {
	public static String sched = '0 00 00 * * ?';  //Day and Time
    
    global void execute(SchedulableContext sc){      
            Database.executeBatch(new SFS_BatchCreatePricebookEntry());       
    }
}