@isTest
global class SFS_POReceipt_Integration_Test {
    public static testmethod void SFS_POReceipt_Integration_Test(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponsePOReceipt();

        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        Account billToAcc = SFS_TestDataFactory.getAccount();
        Update billToAcc;
        Account acc = SFS_TestDataFactory.getAccount();
        acc.Bill_To_Account__c=billToAcc.Id;
        acc.Type ='Prospect';
        Update acc;
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,false);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='00250506';
        update proList[0];
        List<ServiceContract> svc = SFS_TestDataFactory.createServiceAgreement(1, acc.Id,true);
        List<ContractLineItem> cliRec = SFS_TestDataFactory.createServiceAgreementLineItem(1, svc[0].Id,true);
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
        insert lc;
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acc.Id,loc[0].Id, div[0].Id,  svc[0].Id, true);
        WorkType wt = new WorkType();
           String wt_RECORDTYPEID = Schema.SObjectType.WorkType.getRecordTypeInfosByName().get('Service').getRecordTypeId();
           wt.Name = 'test';
           wt.SFS_Product__c = proList[0].Id;
           wt.RecordTypeId = wt_RECORDTYPEID;
           wt.EstimatedDuration = 12.00;
           wt.DurationType ='Hours';
           insert wt;

        List<WorkOrderLineItem> woliList =  SFS_TestDataFactory.createWorkOrderLineItem(1,WoList[0].Id,wt.Id,true);

        SFS_Shipping_Method__c sm = new SFS_Shipping_Method__c(Name = 'Test',SFS_Division__c=div[0].Id,SFS_Shipping_Method_Code__c='Test');
        insert sm;

        List<ProductRequest> prList =  SFS_TestDataFactory.createProductRequest(1,WoList[0].Id,woliList[0].Id,false);
        prList[0].DestinationLocationId=loc[0].Id;
        prList[0].AccountId =acc.Id;
        prList[0].SFS_Consumables_Ship_To_Account__c =acc.Id;
        prList[0].SFS_Shipping_Method__c=sm.Id;
        prList[0].WorkOrderlineitemId=woliList[0].Id;
        prList[0].SFS_Division__c =div[0].Id;
        insert prList[0];
        Test.startTest();
        List<Product2> productList = SFS_TestDataFactory.createProduct(1,true);
        List<ProductRequestLineItem> prliList =  SFS_TestDataFactory.createPRLI(1,prList[0].Id,productList[0].id,true);

        List<id> listids=new  List<id>();
        listids.add(woliList[0].Id);

        List<Shipment> shipmentList =  SFS_TestDataFactory.createShipment(1,prList[0].Id,true);
        List<ShipmentItem> shipmentItemList =  SFS_TestDataFactory.createShipmentItem(1,shipmentList[0].Id,prList[0].Id,prliList[0].Id,true);

        List<SFS_POReceipt_Integration.getpoReceipt> lb=new List<SFS_POReceipt_Integration.getpoReceipt>();
        SFS_POReceipt_Integration.getpoReceipt ss=new SFS_POReceipt_Integration.getpoReceipt();
        ss.woliId=woliList[0].Id;
        ss.saId=null;
        lb.add(ss);
        SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT SFS_Endpoint_URL__c,SFS_ContentType__c,SFS_Username__c, SFS_Password__c, SFS_RICE_ID__c, SFS_Interface_Destination__c
                                                                FROM SFS_SFS_Integration_Endpoint__mdt WHERE Label =:'XXINV2638'];
        Map<String,HttpResponse> responseXMLMap = new Map<String,HttpResponse>();
        responseXMLMap.put('test',res);
        SFS_POReceipt_Integration.Run(lb);
        SFS_POReceipt_Integration.ProcessData(prList[0],shipmentItemList,shipmentItemList.size());
        SFS_POReceipt_Integration.createIntegrationLog(responseXMLMap, 'test', endpoint);
        Test.stopTest();


    }

}