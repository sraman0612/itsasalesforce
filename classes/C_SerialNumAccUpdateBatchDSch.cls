// Daily batch
global class C_SerialNumAccUpdateBatchDSch implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Id batchId = Database.executeBatch(new C_SerialNumAccUpdateBatch('D'));
    }
}