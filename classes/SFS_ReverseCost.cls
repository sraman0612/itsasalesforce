/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 18/07/2022
* @description: Logic to cancel the Time and Expense charges, Invoice and Invoice line items at WOLI Level
* @Story Number: SIF-268

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
==============================================================================================================*/
public class SFS_ReverseCost {
    
    @InvocableMethod(label='Reverse Cost')
    public static void reverseCost(List<List<String>> ids){
        integer i;
        List<String> idsList = new List<String>();
        for(i=0;i<ids.size();i++){
            idsList.addAll(ids.get(i));
        }
        
        List<Expense> exList = [select Id,WorkOrderId,SFS_Work_Order_Line_Item__c,Amount,SFS_Reverse_Cost__c,SFS_Transaction_Type__c,SFS_Category__c,TransactionDate,
                                ExpenseType,SFS_Paid_With__c
                                from Expense where Id IN :idsList ];
        
        List<CAP_IR_Labor__c> labourList = [ Select Id, CAP_IR_Work_Order__c,CAP_IR_Work_Order_Line_Item__c,SFS_Labor_Rate_Formula__c,CAP_IR_Quantity__c,
                                             CAP_IR_Labor_Rate__c,CAP_IR_Start_Date__c,CAP_IR_End_Date__c,CAP_IR_Labor_Type__c 
                                             from CAP_IR_Labor__c where Id IN :idsList];
        Set<ID> woliIds = new Set<ID>();
        if(!labourList.isEmpty()){
             String woId = labourList[0].CAP_IR_Work_Order__c;
             List<CAP_IR_Labor__c> lhToUpdate = new List<CAP_IR_Labor__c>();
             List<CAP_IR_Labor__c> lhToInsert = new List<CAP_IR_Labor__c>();
             for(CAP_IR_Labor__c lh: labourList){
                     lh.SFS_Reverse_Cost__c = true;
                     lhToUpdate.add(lh);
                     lhToInsert.add(new CAP_IR_Labor__c(
                                           CAP_IR_Work_Order__c = lh.CAP_IR_Work_Order__c,
                                           CAP_IR_Work_Order_Line_Item__c = lh.CAP_IR_Work_Order_Line_Item__c,
                                           SFS_Transaction_Type__c = 'Reverse Transaction',
                                           CAP_IR_Quantity__c = lh.CAP_IR_Quantity__c,
                                           CAP_IR_Labor_Rate__c = lh.CAP_IR_Labor_Rate__c,
                                           CAP_IR_Start_Date__c = lh.CAP_IR_Start_Date__c,
                                           CAP_IR_End_Date__c = lh.CAP_IR_End_Date__c,
                                           CAP_IR_Labor_Type__c = lh.CAP_IR_Labor_Type__c
                                          ));      
                 if(!woliIds.contains(lh.CAP_IR_Work_Order_Line_Item__c)){
                       woliIds.add(lh.CAP_IR_Work_Order_Line_Item__c); 
                   }   
             }
             List<CAP_IR_Charge__c> chargeList = [Select Id,CAP_IR_Work_Order__c,CAP_IR_Work_Order_Line_Item__c,CAP_IR_Status__c,CAP_IR_Description__c 
                                                  from CAP_IR_Charge__c where SFS_Charge_Type__c = 'Time' and CAP_IR_Work_Order_Line_Item__c IN :woliIds];
           
             List<CAP_IR_Charge__c> cancelChargeList = new List<CAP_IR_Charge__c>();
             if(!chargeList.isEmpty()){
                  for(CAP_IR_Charge__c charge : chargeList){
                       if(charge.CAP_IR_Description__c !='Travel Time Labor'){
                             charge.CAP_IR_Status__c = 'Cancelled';
                             charge.CAP_IR_Work_Order__c =null;
                             charge.CAP_IR_Work_Order_Line_Item__c =null;    
                             cancelChargeList.add(charge);
                         }    
                      } 
             }
            if(!lhToUpdate.isEmpty()){
                update lhToUpdate;
             }
            if(!lhToInsert.isEmpty()){
                insert lhToInsert;
            }
            if(!cancelChargeList.isEmpty())
            {
                update cancelChargeList;
            }           
            cancelInvAndLines(woliIds);            
        }
           
      if(!exList.isEmpty()){
        String woId = exList[0].WorkOrderId;
        List<Expense> exToUpdate = new List<Expense>();
        List<Expense> exToInsert = new List<Expense>();
        List<String>  expenseType = new List<String>();
        for(Expense ex: exList){
             ex.SFS_Reverse_Cost__c = true;
             exToUpdate.add(ex);
             exToInsert.add(new Expense(
                                           WorkOrderId = ex.WorkOrderId,
                                           SFS_Work_Order_Line_Item__c = ex.SFS_Work_Order_Line_Item__c,
                                           Amount = ex.Amount,
                                           ExpenseType = ex.ExpenseType,
                                           SFS_Category__c = ex.SFS_Category__c,
                                           TransactionDate = ex.TransactionDate,
                                           SFS_Transaction_Type__c = 'Reverse Transaction',
                                           SFS_Paid_With__c = ex.SFS_Paid_With__c
                                          )); 
            if(!woliIds.contains(ex.SFS_Work_Order_Line_Item__c)){
                 woliIds.add(ex.SFS_Work_Order_Line_Item__c); 
              } 
            if(!expenseType.contains(ex.ExpenseType)){
                expenseType.add(ex.ExpenseType);
              }  
            
        }
       system.debug('@expenseType'+expenseType);
          
       List<CAP_IR_Charge__c> chargeToReverse = [Select Id,CAP_IR_Status__c,CAP_IR_Work_Order__c,CAP_IR_Work_Order_Line_Item__c,SFS_Expense_Type__c,SFS_Expense__c
                                                 from CAP_IR_Charge__c where SFS_Expense_Type__c IN:expenseType];
       List<CAP_IR_Charge__c> updateCharge = new List<CAP_IR_Charge__c>();
          
       for (CAP_IR_Charge__c exCharge:chargeToReverse){
              if((exCharge.SFS_Expense_Type__c =='Misc' || exCharge.SFS_Expense_Type__c =='Outside Services')){
                  if(idsList.contains(exCharge.SFS_Expense__c)){
                     exCharge.CAP_IR_Status__c = 'Cancelled';
                     exCharge.CAP_IR_Work_Order__c =null;
                     exCharge.CAP_IR_Work_Order_Line_Item__c =null;   
                     updateCharge.add(exCharge); 
                  }
              }
           else {
                  exCharge.CAP_IR_Status__c = 'Cancelled';
                  exCharge.CAP_IR_Work_Order__c =null;
                  exCharge.CAP_IR_Work_Order_Line_Item__c =null;   
                  updateCharge.add(exCharge); 
               }
           } 
          if(!exToUpdate.isEmpty()){
                update exToUpdate;
             }
          if(!exToInsert.isEmpty()){
                insert exToInsert;
            }  
   
          if(!updateCharge.isEmpty())
            {
                update updateCharge;
            }           
            cancelInvAndLines(woliIds);         
       }   
        
       if(!woliIds.isEmpty()) {
           List<workorderlineitem> woliToUpdate = new  List<workorderlineitem>();
           for(workorderlineitem woli : [select Id, SFS_Invoice_Generated__c from workorderlineitem where Id IN : woliIds]){
               woli.SFS_Invoice_Generated__c = false;
               woliToUpdate.add(woli);
           }
           
           update woliToUpdate;
        } 
        
        
    }    
   // Logic to cancel Invoice and Invoice Line Items 
   public static void cancelInvAndLines(Set<Id> woliIds){
           system.debug('@@woliIds'+woliIds); 
      List<SFS_Invoice_Line_Item__c> invlinesList = [select Id,SFS_Work_Order__c,SFS_Invoice__c,SFS_Invoice__r.Status__c
                                                              from SFS_Invoice_Line_Item__c where SFS_Work_Order_Line_Item__c IN :woliIds and SFS_Invoice__r.Status__c !='Submitted'];
      system.debug('@@invlinesList'+invlinesList); 
      List<Invoice__c> invToUpdate = new List<Invoice__c>(); 
      if(!invlinesList.isEmpty()){  
          Invoice__c inv = [Select Id,SFS_Work_Order__c,SFS_Status__c 
                            from Invoice__c Where Id =:invlinesList[0].SFS_Invoice__c and SFS_Status__c !='Submitted'];
          inv.SFS_Work_Order__c =null;
          inv.SFS_Status__c = 'Canceled';
          invToUpdate.add(inv);
          
          List<SFS_Invoice_Line_Item__c> linesToUpdate = new List<SFS_Invoice_Line_Item__c> ();
          for(SFS_Invoice_Line_Item__c lines :[Select Id from SFS_Invoice_Line_Item__c where SFS_Invoice__c=: inv.Id ]){
              lines.SFS_Work_Order__c =null;
              lines.SFS_Work_Order_Line_Item__c = null;
              linesToUpdate.add(lines);
             }
          if(!linesToUpdate.isEmpty()){
              update linesToUpdate;   
             }
          update invToUpdate;
        }    
  }
}