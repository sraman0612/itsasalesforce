public class SFS_accountDetails {
    
    @AuraEnabled (cacheable = true)
    public static List<Account> searchAccount(String accName,String  objectName,String customerNo, String accountType, String oracleNo ) {
        Id BillToRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        Set<String> country = new Set<String>{'USA','Canada'};
        string strAccName = '%'+ accName + '%';
        String Statuss;
        if(accountType==null){
            Statuss='APPROVED';
        }
        else{
            Statuss='Draft';
        }
        List<Account> accList =new List<Account>();
        if(objectName == 'ServiceContract'){
            if( Statuss=='APPROVED'){
                accList = [Select Id, Name, Type, IRIT_Customer_Number__c,ShippingStreet, ShippingCity,RecordTypeId, ShippingState, ShippingPostalCode,ShippingCountry from Account WHERE Name LIKE: strAccName AND IRIT_Customer_Number__c=:customerNo AND Type=:'Bill To' AND Status__c='Active' AND RecordTypeId=:BillToRTId AND ShippingCountry IN :country ];  
            }
            else if(Statuss=='Draft'){
                if(accountType =='Ship To'){
                    accList=callForShipToSearch(oracleNo,strAccName);
                }
                else if(accountType =='Prospect'){
                    accList = [Select Id, Name, Type, IRIT_Customer_Number__c,ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,ShippingCountry from Account WHERE Name LIKE: strAccName AND Type=:'Bill To' AND Status__c='Active'AND RecordTypeId=:BillToRTId AND ShippingCountry IN :country  Limit 25 ];
                }
            }
            
        }
        else if(objectName == 'WorkOrder'){
            if(accountType =='Ship To'){
                accList=callForShipToSearch(oracleNo,strAccName);
            }
            else if(accountType =='Prospect'){
                accList = [Select Id, Name, Type, IRIT_Customer_Number__c,ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,ShippingCountry from Account WHERE Name LIKE: strAccName AND Type=:'Bill To' AND Status__c='Active'AND RecordTypeId=:BillToRTId  AND ShippingCountry IN :country  Limit 25];
            }
            
        }
        
        return accList;
    }
    
    @AuraEnabled (cacheable = true)
    public static List<Account> getAccount(String accountType,String  objectName,String customerNo, String oracleNo ) {
        String Statuss;
        Set<String> country = new Set<String>{'USA','Canada'};
        List<Account> accList =new List<Account>();
        Id BillToRTIdOnAcc = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        if(accountType==null){
            Statuss='APPROVED';
        }
        else{
            Statuss='Draft';
        }
        if(objectName == 'ServiceContract'){
            if( Statuss=='APPROVED'){
                
                accList = [Select Id, Name, Type, IRIT_Customer_Number__c,ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,ShippingCountry from Account WHERE  Type=:'Bill To' AND IRIT_Customer_Number__c=:customerNo AND Status__c='Active'AND RecordTypeId=:BillToRTIdOnAcc AND ShippingCountry IN :country  Limit 25];  
            }
            else if(Statuss=='Draft'){
                if(accountType =='Ship To'){                
                    accList=callForShipToGet(oracleNo);
                }
                else if(accountType =='Prospect'){
                    accList = [Select Id, Name, Type, IRIT_Customer_Number__c,ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,ShippingCountry from Account WHERE  Type=:'Bill To' AND Status__c='Active' AND RecordTypeId=:BillToRTIdOnAcc AND ShippingCountry IN :country  Limit 25 ];
                }
            }
            
        }
        else if(objectName == 'WorkOrder'){
            if(accountType =='Ship To'){
                accList=callForShipToGet(oracleNo);
            }
            else if(accountType =='Prospect'){
                accList = [Select Id, Name, Type, IRIT_Customer_Number__c,ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,ShippingCountry from Account WHERE  Type=:'Bill To' AND Status__c='Active'AND RecordTypeId=:BillToRTIdOnAcc AND ShippingCountry IN :country  Limit 25 ];
            }
        }
        return accList;
    }
    
    public static List<Account> callForShipToGet( String oracleNo ) {    
        List<Account> accList =new List<Account>();
        Set<String> country = new Set<String>{'USA','Canada'};
        Id ctsBillToRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        Set<String> validPricelists = new Set<String>{'ITS_PRIMARY','ITS_CAN_PRIMARY'};
            Set<String> billToNumbers = new Set<String>(); 
        for (Customer_Relationship__c cr : [SELECT Name FROM Customer_Relationship__c WHERE Customer_Relationship__c.Shipping_Customer__c = :oracleNo]){
            billToNumbers.add(cr.Name);
        }
        
        if(oracleNo != null){
            accList =[SELECT Id,Name, Type, Oracle_Number__c ,IRIT_Customer_Number__c,ShippingStreet,ShippingCity,ShippingState, ShippingPostalCode,ShippingCountry 
                      FROM account 
                      WHERE 
                      Type='Bill To' and 
                      Status__c='Active' and 
                      RecordTypeId = :ctsBillToRTId and 
                      PriceList__c IN :validPricelists and 
                      Oracle_Number__c = :billToNumbers and
                      ShippingCountry IN :country 
                     ];
        }      
        return accList;      
    }
    public static List<Account> callForShipToSearch( String oracleNo, String strAccName ) {       
        List<Account> accList =new List<Account>();
        Set<String> country = new Set<String>{'USA','Canada'};
        Id ctsBillToRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        Set<String> validPricelists = new Set<String>{'ITS_PRIMARY','ITS_CAN_PRIMARY'};
            Set<String> billToNumbers = new Set<String>(); 
        for (Customer_Relationship__c cr : [SELECT Name FROM Customer_Relationship__c WHERE Customer_Relationship__c.Shipping_Customer__c = :oracleNo]){
            billToNumbers.add(cr.Name);
        }
        
        if(oracleNo != null){
            accList =[SELECT Id,Name, Type, Oracle_Number__c ,IRIT_Customer_Number__c,ShippingStreet,ShippingCity,ShippingState, ShippingPostalCode,ShippingCountry 
                      FROM account 
                      WHERE Name LIKE: strAccName and
                      Type='Bill To' and 
                      Status__c='Active' and 
                      RecordTypeId = :ctsBillToRTId and 
                      PriceList__c IN :validPricelists and 
                      Oracle_Number__c = :billToNumbers and
                      ShippingCountry IN :country 
                     ];
        }      
        return accList;      
    }
}