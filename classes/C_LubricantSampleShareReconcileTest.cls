@isTest
public class C_LubricantSampleShareReconcileTest {

    @testSetup static void setUp()
    {
        Account ap1 = new account(name='test3');
        insert ap1;
        
        Account ap2 = new account(name='test4');
        insert ap2;
        
        Account a = new account(name='test',ParentId = ap1.id);
        insert a;
        
        Account a1 = new account(name='test1', ParentId = ap2.id);
        insert a1;
        
        contact con = new contact(firstname='j', lastname='b', email='jbtest1@test.com', accountid =a.id);
        insert con;
        
        contact con1 = new contact(firstname='j2', lastname='b2', email='jbtest2@test.com', accountid =a.id);
        insert con1;
        
        contact con2 = new contact(firstname='j2', lastname='b2', email='jbtest3@test.com', accountid =a1.id);
        insert con2;
        
        contact con3 = new contact(firstname='j2', lastname='b2', email='jbtest4@test.com', accountid =a1.id);
        insert con3;
        
        
        
        User u = new User(
            ProfileId = [select id from profile where name='Partner Community User'].id,
            Firstname='First',
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Single Account'
        );
        User u2 = new User(
            ProfileId = [select id from profile where name='Partner Community User'].id,
            Firstname='First2',
            LastName = 'last2',
            Email = 'puser0001@amamama.com',
            Username = 'puser0001@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con1.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Global Account'
        );
        User u3 = new User(
            ProfileId = [select id from profile where name='Partner Community User'].id,
            Firstname='First3',
            LastName = 'last3',
            Email = 'puser0002@amamama.com',
            Username = 'puser0002@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            ContactId = con2.Id,
            community_user_type__c='Global Account'
        );
        User u4 = new User(
            ProfileId = [select id from profile where name='Partner Community User'].id,
            Firstname='First4',
            LastName = 'last4',
            Email = 'puser0003@amamama.com',
            Username = 'puser0030@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con3.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Global Account'
        );
        insert u;
        insert u2;
        insert u3;
        insert u4;
        
        Oil_Sample__c  os = new Oil_Sample__c();
        os.Distributor__c =a.id;
        insert os;
        
        Oil_Sample__c  os1 = new Oil_Sample__c();
        os1.Distributor__c =a1.id;
        insert os1;
        
        os1.Distributor__c =a.id;
        update os1;
        
        
    }
    
    @isTest static void testBatch()
    {
        C_LubricantSampleShareReconcile ls = new C_LubricantSampleShareReconcile();
        Id batchJobId = Database.executeBatch(ls);        
    }
}