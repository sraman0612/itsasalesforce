/*
 * Author   : Nocks Emmanuel Mulea 
 * Company  : Canpango LLC
 * Email    : emmanuel.mulea@canpango.com
 * 
 * 
 * 
 */


global class C_GDIiConnMeasurmentsServiceSchedule  implements schedulable{
    
    global void execute(SchedulableContext scheduleC){
        C_GDIiConnMeasurmentsServiceBatch btch = new C_GDIiConnMeasurmentsServiceBatch('');
         Database.executebatch(btch,25);
    }

}