@isTest
private class TSTC_EnosixOrderDetail
{
    public class ThisException extends Exception {}
    
    public class MockSBO_EnosixSO_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock {
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) {
            SBO_EnosixSO_Detail.EnosixSO result = new SBO_EnosixSO_Detail.EnosixSO();
            
            result.SalesDocument = 'X';
            result.SoldToParty = 'X';
            result.SoldToPartyText = 'X';
            result.CustomerPurchaseOrderNumber = 'X';
            result.CustomerPurchaseOrderDate = Date.valueOf('2020-12-31');
            result.NetOrderValue = 1.5;
            result.TaxAmount = 1.5;
            result.SalesDocumentCurrency = 'X';
            result.BillingPlan = 'X';
            result.ReferenceDocument = 'X';
            result.ReferenceDocumentCategory = 'X';
            result.SerialNumber = 'X';
            result.Material = 'X';
            result.NotificationNumber = 'X';
            result.EquipmentNumber = 'X';
            result.HoursInService = 'X';
            result.PersonnelNumber = 'X';
            result.InspectedDate = Date.valueOf('2020-12-31');
            
            return result;
        }
    }
    
    public class MockSBO_SFCIDelivery_Detail implements
        ensxsdk.EnosixFramework.DetailSBOGetMock
    {
        private Boolean success;
        
        public MockSBO_SFCIDelivery_Detail(Boolean success) {
            this.success = success;
        }
        
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { 
            if (!success) {
                throw new ThisException();
            }
	        SBO_SFCIDelivery_Detail.SFCIDelivery result = new SBO_SFCIDelivery_Detail.SFCIDelivery();

            result.registerReflectionForClass();
            
            result.Delivery = 'X';
            result.MeansOfTransportID = 'X';
            result.ShippingPoint = 'X';
            result.ShippingPointDescription = 'X';
            result.LoadingPoint = 'X';
            result.LoadingPointText = 'X';
            result.UnloadingPoint = 'X';
            result.Route = 'X';
            result.RouteText = 'X';
            result.BillofLading = 'X';
            result.CreateDate = Date.valueOf('2020-12-31');
            result.DeliveryDate = Date.valueOf('2020-12-31');
            result.PGIDate = Date.valueOf('2020-12-31');
            result.DeliveryType = 'X';
            result.DeliveryTypeText = 'X';
            result.SoldToParty = 'X';
            result.SoldToPartyText = 'X';
            result.ShipToParty = 'X';
            result.ShipToPartyText = 'X';
            result.ShippingConditions = 'X';
            result.ShipConditionText = 'X';
            result.DeliveryPriority = 'X';
            result.DeliveryPriorityText = 'X';
            result.NetOrderValue = 1.5;
            result.SalesDocumentCurrency = 'X';
            result.DeliveryBlock = 'X';
            result.DeliveryStatus = 'X';
            SBO_SFCIDelivery_Detail.PARTNERS partner = new SBO_SFCIDelivery_Detail.PARTNERS();
            result.PARTNERS.add(partner);
            partner.registerReflectionForClass();
            partner.PartnerFunction = 'CR';
            partner.PartnerFunctionName = 'Carrier';
            partner.CustomerNumber = 'X';
            partner.Vendor = 'X';
            partner.PersonnelNumber = 'X';
            partner.ContactPersonNumber = 'X';
            partner.PartnerName = 'X';
            partner.PartnerName2 = 'X';
            partner.HouseNumber = 'X';
            partner.Street = 'X';
            partner.City = 'X';
            partner.PostalCode = 'X';
            partner.Region = 'X';
            partner.RegionDescription = 'X';
            partner.Country = 'X';
            partner.CountryName = 'X';
            partner.TimeZone = 'X';
            partner.TimeZoneText = 'X';
            partner.TransportationZone = 'X';
            partner.TransportationZoneDescription = 'X';
            partner.POBox = 'X';
            partner.POBoxPostalCode = 'X';
            partner.CompanyPostalCode = 'X';
            partner.Language = 'X';
            partner.LanguageDesc = 'X';
            partner.TelephoneNumber = 'X';
            partner.TelephoneNumberExtension = 'X';
            partner.MobileNumber = 'X';
            partner.FaxNumber = 'X';
            partner.FaxNumberExtension = 'X';
            partner.EMailAddress = 'X';
            partner.DefaultCommunicationMethod = 'X';
            partner.DefaultCommunicationMethodDescription = 'X';
            partner.Extension1 = 'X';
            partner.Extension2 = 'X';
            partner.AddressNotes = 'X';
            
            return result; 
        }
    }

    public class MockSBO_EnosixSalesDocOutput_Search implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        Boolean isSuccess;
        
        public MockSBO_EnosixSalesDocOutput_Search(Boolean success) {
            isSuccess = success;
        }
        
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC searchContext = (SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC)sc;
            SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SR searchResult =
                new SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SR();
            
            if (isSuccess)
            {
                SBO_EnosixSalesDocOutput_Search.SEARCHRESULT result = new SBO_EnosixSalesDocOutput_Search.SEARCHRESULT();
        
                result.registerReflectionForClass();
        
                result.SalesDocument = 'X';
                result.ConditionType = 'X';
                result.ConditionTypeDescription = 'X';
                result.Language = 'X';
                result.PDFB64String = 'X';
                searchResult.SearchResults.add(result);
            } else {
                throw new ThisException();
            }
            
            searchResult.setSuccess(isSuccess);
            searchContext.baseResult = searchResult;
            
            return searchContext;
        }
    }

    public class MockSBO_EnosixSalesDocFlow_Search implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        Boolean isSuccess;
        
        public MockSBO_EnosixSalesDocFlow_Search(Boolean success) {
            isSuccess = success;
        }
        
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            SBO_EnosixSalesDocFlow_Search.EnosixSalesDocFlow_SC searchContext = (SBO_EnosixSalesDocFlow_Search.EnosixSalesDocFlow_SC)sc;
            SBO_EnosixSalesDocFlow_Search.EnosixSalesDocFlow_SR searchResult =
                new SBO_EnosixSalesDocFlow_Search.EnosixSalesDocFlow_SR();
            
            if (isSuccess)
            {
                SBO_EnosixSalesDocFlow_Search.SEARCHRESULT result = new SBO_EnosixSalesDocFlow_Search.SEARCHRESULT();
        
                result.registerReflectionForClass();
        
                result.SalesDocument = 'X';
                result.ItemNumber = 'X';
                result.DocumentCategory = 'X';
                result.DocumentCategoryText = 'X';
                result.CustomerNumber = 'X';
                result.CreateDate = Date.valueOf('2020-12-31');
                result.CreatedBy = 'X';
                result.DateChanged = Date.valueOf('2020-12-31');
                result.ChangedBy = 'X';
                result.X_PrecedingDocuments = true;
                result.X_SubsequentDocuments = true;
                result.DocumentStatus = 'X';
                searchResult.SearchResults.add(result);
            }
            
            searchResult.setSuccess(isSuccess);
            searchContext.baseResult = searchResult;
            
            return searchContext;
        }
    }
    
    @isTest
    public static void test1() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Detail.class, new MockSBO_EnosixSO_Detail());
        
        CTRL_EnosixOrderDetail.getOrderDetail('12345');
    }
    
    @isTest
    public static void test2s(){
        ensxsdk.EnosixFramework.setMock(SBO_SFCIDelivery_Detail.class, new MockSBO_SFCIDelivery_Detail(true));
    
		GDI_Delivery_Vendors__c vendor = new GDI_Delivery_Vendors__c();
        vendor.Name = 'test';
		vendor.FLD_Tracking_URL__c = 'X';
		vendor.FLD_Tracking_URL_Label__c = 'X';
		vendor.FLD_Vendor_Code__c = 'X';
        vendor.FLD_Vendor_Description__c = 'X';
        vendor.FLD_Vendor_Num__c = 'X';
        
        insert vendor;
        
        CTRL_EnosixOrderDetail.trackDelivery('12345');
    }
    
    @isTest
    public static void test2f(){
        ensxsdk.EnosixFramework.setMock(SBO_SFCIDelivery_Detail.class, new MockSBO_SFCIDelivery_Detail(false));
        
        CTRL_EnosixOrderDetail.trackDelivery('12345');
    }
    
    @isTest
    public static void test3s() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSalesDocFlow_Search.class, new MockSBO_EnosixSalesDocFlow_Search(true));
        
        CTRL_EnosixOrderDetail.getDocumentFlow('12345');
    }
    
    @isTest
    public static void test3f() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSalesDocFlow_Search.class, new MockSBO_EnosixSalesDocFlow_Search(false));
        
        CTRL_EnosixOrderDetail.getDocumentFlow('12345');
    }
    
    @isTest
    public static void test4s() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSalesDocOutput_Search.class, new MockSBO_EnosixSalesDocOutput_Search(true));
        
        CTRL_EnosixOrderDetail.downloadInvoice('12345');
    }
    
    @isTest
    public static void test4f() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSalesDocOutput_Search.class, new MockSBO_EnosixSalesDocOutput_Search(false));
        
        CTRL_EnosixOrderDetail.downloadInvoice('12345');
    }    
}