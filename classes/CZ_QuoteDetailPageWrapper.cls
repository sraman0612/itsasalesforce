public class CZ_QuoteDetailPageWrapper {
    
    public Machine_Data_Lookup__c machineDetails {get; set;}
    public String mainImage {get; set;}
    public String productCode {get; set;}
    public String productDescription {get; set;}
    public String technicalInfo1 {get; set;}
    public String technicalInfo2 {get; set;}
    public String technicalInfo3 {get; set;}
    public String technicalInfo4 {get; set;}
    public String technicalInfo5 {get; set;}
    public String technicalInfo6 {get; set;}
    public Lookup_Quote_Docs_Machines__c featureBenefits {get; set;}
    public List<Lookup_Quote_Docs_Distinct_Benefits__c> benefitList {get; set;}
    public Boolean hasBenefits {get
    								{ 
                                        if(this.benefitList == null){
                                            return false;
                                        }
                                        return !this.benefitList.isEmpty();
                                    }
                                
                               }


}