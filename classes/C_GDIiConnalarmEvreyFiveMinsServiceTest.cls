@isTest
global class C_GDIiConnalarmEvreyFiveMinsServiceTest {
    @isTest
    static void posAlert(){
        Asset newAst1 = new Asset(name='testing111',Cumulocity_ID__c=142327920,RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c='359804083674640');
        Asset newAst2 = new Asset(name='testing221',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c=Null);
        insert newAst1;
        insert newAst2;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new C_GDIHttpMockAlertTest());
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://example.com/example/test');
        req.setMethod('GET');
        Http h1 = new Http();
        HttpResponse res1 = h1.send(req);
        
        C_GDIiConnMeasurmentsService.alarmEvreyFiveMins();
        C_GDIiConnAlertServiceSchedule sh1 = new C_GDIiConnAlertServiceSchedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test C_GDIiConnAlertServiceSchedule', sch, sh1);
        Test.stopTest();
    }
    
    @isTest
    static void negAlert(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new C_GDIHttpMockAlertTest());
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://example.com/example/test');
        req.setMethod('POST');
        Http h1 = new Http();
        HttpResponse res1 = h1.send(req);
        C_GDIiConnMeasurmentsService.alarmEvreyFiveMins();
        C_GDIiConnAlertServiceSchedule sh1 = new C_GDIiConnAlertServiceSchedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test C_GDIiConnAlertServiceSchedule', sch, sh1);
        Test.stopTest();
    }
}