@isTest
public class ensxtx_TSTE_VCCharacteristic
{
    @isTest static void testGetControlForCharacteristic ()
    {
        Test.startTest();
        ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS characteristics = new ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS();
        String testString = ensxtx_ENSX_VCCharacteristic.getControlForCharacteristic(characteristics,0,false);
        System.assert(testString == 'ShortText');
        characteristics.DataType = 'Char';
        testString = ensxtx_ENSX_VCCharacteristic.getControlForCharacteristic(characteristics,1,true);
        System.assert(testString == 'RadioGroup');
        testString = ensxtx_ENSX_VCCharacteristic.getControlForCharacteristic(characteristics,1,false);
        System.assert(testString == 'InputPicklist');
        characteristics.DataType = 'Int';
        testString = ensxtx_ENSX_VCCharacteristic.getControlForCharacteristic(characteristics,0,false);
        System.assert(testString == 'Number');
        characteristics.DataType = 'NUM';
        testString = ensxtx_ENSX_VCCharacteristic.getControlForCharacteristic(characteristics,0,false);
        System.assert(testString == 'Number');
        characteristics.DataType = 'Date';
        testString = ensxtx_ENSX_VCCharacteristic.getControlForCharacteristic(characteristics,0,false);
        System.assert(testString == 'Date');
        Test.stopTest();
    }

    @isTest static void testGetValueDescriptionForValueId ()
    {
        Test.startTest();
        ensxtx_SBO_EnosixVC_Detail.EnosixVC enosixVC = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
        ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES allowedValue = new ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES();
        allowedValue.CharacteristicValue = 'CharacteristicValue';
        allowedValue.CharacteristicValueDescription = 'CharacteristicValueDescription';
        enosixVC.ALLOWEDVALUES.add(allowedValue);
        String testString = ensxtx_ENSX_VCCharacteristic.getValueDescriptionForValueId(enosixVC,'CharacteristicValue');
        System.assert(testString == 'CharacteristicValueDescription');
        Test.stopTest();
    }

    @isTest static void testGetCharacteristicDescriptionForValueId ()
    {
        Test.startTest();
        ensxtx_SBO_EnosixVC_Detail.EnosixVC enosixVC = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
        ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS characteristic = new ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS();
        characteristic.CharacteristicId = 'CharacteristicId';
        characteristic.CharacteristicDescription = 'CharacteristicDescription';
        enosixVC.CHARACTERISTICS.add(characteristic);
        String testString = ensxtx_ENSX_VCCharacteristic.getCharacteristicDescriptionForValueId(enosixVC,'CharacteristicId');
        System.assert(testString == 'CharacteristicDescription');
        Test.stopTest();
    }

    @isTest static void testGetCharacteristicForSBOModel ()
    {
        Test.startTest();
        ensxtx_SBO_EnosixVC_Detail.EnosixVC enosixVc = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
        ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES allowedValue = new ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES();
        allowedValue.CharacteristicID = 'CharacteristicID';
        allowedValue.CharacteristicValue = 'CharacteristicValue';
        allowedValue.CharacteristicValueDescription = 'CharacteristicValueDescription';
        ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS characteristic = new ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS();
        characteristic.CharacteristicID = 'CharacteristicID';
        characteristic.CharacteristicDescription = 'CharacteristicDescription';
        characteristic.CharacteristicName = 'CharacteristicName';
        enosixVc.ALLOWEDVALUES.add(allowedValue);
        ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES selectedValue = new ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES();
        selectedValue.CharacteristicID = 'CharacteristicID';
        selectedValue.CharacteristicValue = 'CharacteristicValue';
        enosixVc.SELECTEDVALUES.add(selectedValue);
        ensxtx_ENSX_VCCharacteristic testCharacteristic = ensxtx_ENSX_VCCharacteristic.getCharacteristicForSBOModel(enosixVc, characteristic);
        System.assert(testCharacteristic.CharacteristicName == 'CharacteristicName');
        Test.stopTest();
    }

    @isTest static void testGetSBOModelCharacteristicForCharacteristic ()
    {
        Test.startTest();
        ensxtx_ENSX_VCCharacteristic ensxVcCharacteristic = new ensxtx_ENSX_VCCharacteristic();
        ensxVcCharacteristic.CharacteristicName = 'CharacteristicName';
        ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS testCharacteristic = ensxtx_ENSX_VCCharacteristic.getSBOModelCharacteristicForCharacteristic(ensxVcCharacteristic);
        System.assert(testCharacteristic.CharacteristicName == 'CharacteristicName');
        Test.stopTest();
    }

    @isTest static void testGetSBOAllowedValuesFromPossibleValues ()
    {
        Test.startTest();
        ensxtx_ENSX_VCCharacteristic vcCharacteristic = new ensxtx_ENSX_VCCharacteristic();
        vcCharacteristic.PossibleValues = new List<ensxtx_ENSX_VCCharacteristicValues>();
        ensxtx_ENSX_VCCharacteristicValues characteristicValue = new ensxtx_ENSX_VCCharacteristicValues();
        characteristicValue.Value = 'Value';
        characteristicValue.CharacteristicId = 'CharacteristicId';
        vcCharacteristic.PossibleValues.add(characteristicValue);
        List<ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES> testList = ensxtx_ENSX_VCCharacteristic.getSBOAllowedValuesFromPossibleValues(vcCharacteristic);
        System.assert(testList[0].CharacteristicValue == 'Value');
        Test.stopTest();
    }
}