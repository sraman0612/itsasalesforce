@isTest

public class ensxtx_TSTC_VC_Characteristics_Detail
{
    public class MOC_EnosixVC_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock,
            ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock,
            ensxsdk.EnosixFramework.DetailSBOInitMock
    {
        public boolean success = true;
        public boolean throwException = false;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = (ensxtx_SBO_EnosixVC_Detail.EnosixVC) obj;
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string command, ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = (ensxtx_SBO_EnosixVC_Detail.EnosixVC) obj;
            ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS characteristic = new ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS();
            characteristic.CharacteristicName = 'CharacteristicName';
            characteristic.NotToBeDisplayed = '';
            result.CHARACTERISTICS.add(characteristic);
            result.setSuccess(success);
            return result;
        }

        public  ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject  initialState)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
            result.setSuccess(success);
            return result;
        }

        public  ensxsdk.EnosixFramework.DetailObject executeInitializeFromData(ensxsdk.EnosixFramework.DetailObject  initialState)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
            result.setSuccess(success);
            return result;
        }
    }


    public class MOC_EnosixQuote_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock,
            ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock,
            ensxsdk.EnosixFramework.DetailSBOInitMock
    {
        public boolean success = true;
        public boolean throwException = false;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            if (this.throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixQuote_Detail.EnosixQuote result = new ensxtx_SBO_EnosixQuote_Detail.EnosixQuote();
            ensxtx_SBO_EnosixQuote_Detail.ITEMS item = new ensxtx_SBO_EnosixQuote_Detail.ITEMS();
            item.ItemNumber = 'itemnumber';
            result.ITEMS.add(item);
            ensxtx_SBO_EnosixQuote_Detail.ITEMS_TEXT itemText1 = new ensxtx_SBO_EnosixQuote_Detail.ITEMS_TEXT();
            itemText1.ItemNumber = 'itemnumber';
            itemText1.TextID = 'Z991';
            itemText1.Text = 'Text';
            result.ITEMS_TEXT.add(itemText1);
            ensxtx_SBO_EnosixQuote_Detail.ITEMS_TEXT itemText2 = new ensxtx_SBO_EnosixQuote_Detail.ITEMS_TEXT();
            itemText2.ItemNumber = 'itemnumber';
            itemText2.TextID = 'Z992';
            itemText2.Text = 'Text';
            result.ITEMS_TEXT.add(itemText2);
            ensxtx_SBO_EnosixQuote_Detail.ITEMS_TEXT itemText3 = new ensxtx_SBO_EnosixQuote_Detail.ITEMS_TEXT();
            itemText3.ItemNumber = 'itemnumber';
            itemText3.TextID = 'Z993';
            itemText3.Text = 'Text';
            result.ITEMS_TEXT.add(itemText3);
            ensxtx_SBO_EnosixQuote_Detail.ITEMS_TEXT itemText4 = new ensxtx_SBO_EnosixQuote_Detail.ITEMS_TEXT();
            itemText4.ItemNumber = 'itemnumber';
            itemText4.TextID = 'Z994';
            itemText4.Text = 'Text';
            result.ITEMS_TEXT.add(itemText4);
            ensxtx_SBO_EnosixQuote_Detail.PARTNERS partner = new ensxtx_SBO_EnosixQuote_Detail.PARTNERS();
            partner.PartnerFunction = ensxtx_UTIL_Customer.SHIP_TO_PARTNER_CODE;
            partner.CustomerNumber = 'CustomerNumber';
            partner.PartnerName = 'PartnerName';
            partner.PartnerName2 = 'PartnerName2';
            partner.HouseNumber = 'HouseNumber';
            partner.Street = 'Street';
            partner.City = 'City';
            partner.Country = 'Country';
            result.PARTNERS.add(partner);
            ensxtx_SBO_EnosixQuote_Detail.ITEMS_SCHEDULE schedule = new ensxtx_SBO_EnosixQuote_Detail.ITEMS_SCHEDULE();
            schedule.ItemNumber = 'itemnumber';
            result.ITEMS_SCHEDULE.add(schedule);
            ensxtx_SBO_EnosixQuote_Detail.CONDITIONS condition = new ensxtx_SBO_EnosixQuote_Detail.CONDITIONS();
            condition.ConditionItemNumber = 'itemnumber';
            result.CONDITIONS.add(condition);
            ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG itemCondition = new ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG();
            itemCondition.ItemNumber = 'itemnumber';
            itemCondition.CharacteristicName = 'CharacteristicName';
            result.ITEMS_CONFIG.add(itemCondition);
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            ensxtx_SBO_EnosixQuote_Detail.EnosixQuote result = new ensxtx_SBO_EnosixQuote_Detail.EnosixQuote();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string command, ensxsdk.EnosixFramework.DetailObject obj)
        {
            ensxtx_SBO_EnosixQuote_Detail.EnosixQuote result = new ensxtx_SBO_EnosixQuote_Detail.EnosixQuote();
            result.setSuccess(success);
            return result;
        }

        public  ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject  initialState)
        {
            ensxtx_SBO_EnosixQuote_Detail.EnosixQuote result = new ensxtx_SBO_EnosixQuote_Detail.EnosixQuote();
            result.setSuccess(success);
            return result;
        }
    }


    @isTest
    public static void test_logger()
    {
        ensxsdk.Logger logger = ensxtx_UTIL_VC_PricingAndConfiguration.logger;
    }


    @isTest
    public static void testGetVC_Characteristics_Detail()
    {

        MOC_EnosixQuote_Detail mockQuoteDetail = new MOC_EnosixQuote_Detail();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixQuote_Detail.class,mockQuoteDetail);

        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixVC_Detail.class, mocEnosixVCDetail);

        Test.startTest();

        ensxtx_CTRL_VC_Characteristics_Detail.getVC_Characteristics_Detail(null,null,'itemnumber');

        Test.stopTest();
    }




    @isTest static void testProccessAndLogVCConfiguration ()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixVC_Detail.class, mocEnosixVCDetail);

        Test.startTest();
        ensxtx_DS_VCMaterialConfiguration config = ensxtx_CTRL_VC_Characteristics_Detail.proccessAndLogVCConfiguration(null,
            null);
        config = ensxtx_CTRL_VC_Characteristics_Detail.proccessAndLogVCConfiguration(new ensxtx_SBO_EnosixVC_Detail.EnosixVC(),
            new List<ensxtx_DS_VCCharacteristicValues>());
        Test.stopTest();
    }


    @isTest static void testConvertTo_CharacteristicsValues()
    {
        Test.startTest();

        List<ensxtx_DS_VCCharacteristicValues> testList = ensxtx_CTRL_VC_Characteristics_Detail.convertTo_CharacteristicValues(null);

        Test.stopTest();

    }

}