@isTest
public class ServiceHistory_RecordsTest {
    static Account testAcc;
    
    @TestSetup static void testSetup() {
        testAcc = new Account(name = 'Gardner Denver');
        insert testAcc;
    }
    
    @isTest static void testRESTPost()
    {
        Machine_Version_Parts_List__c mvpl = new Machine_Version_Parts_List__c(Oil_Filter_Quantity_Expected__c=1);
        insert mvpl;
        
        testAcc = [SELECT Id, Name FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
        Asset serial1 = new Asset(name = 'S483816-EDE99N', accountid = testAcc.Id,Serial_Number_Model_Number__c='S483816-EDE99N',Machine_Version_Parts_List__c=mvpl.id,
                                 Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0,Dchl__c='jhfdhakushkka');
        insert serial1;
        
        system.debug(serial1.id);
        
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Hour_Overage_Threshold_Percent__c = .10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        insert cs;
        //JSON string
        String JSONMsg = '{'+
            '"Service_History": {'+
                '"Date_of_Service__c" : "2018-01-30",'+
                '"Serial__c": "S483816-EDE99N",'+
                '"Current_Hours__c": 23650,'+
                '"Hours_Loaded__c": 0,'+
                '"Hours_Unloaded__c": 0,'+
                '"Gallons_of_Lubricant__c": 0,'+
                '"Separator__c": "202ECH6013:T",'+
                '"Oil_Filter__c": "2116110:T, 2116110:T, 2116110:F",'+
                '"Air_Filter__c": "2118315:T",'+
                '"Other_Parts__c": "",'+
                '"X10_Yr_Warranty_Kit__c" : "",'+
                '"Maintenance_Kit__c" : "",'+
                '"Technician_Email__c" : "nathan.holthaus@gardnerdenver.com",'+
                '"Customer_Feedback__c" : "This is only test feedback from a customer",'+
                '"Service_Notes__c" : "Here are some service notes",'+
                '"Oil_Sample_Taken__c": true,'+
                '"Error_Overrides__c": "Some generated errors are going here."'+
                '},'+
               '"attachments": [{"name":"Test", "ContentType":"image/png", "Body":"d29ybGQ="}],'+
               '"submissions":[]'+
        '}';
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/service-records/';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        ServiceHistory_Records.postRecord();
    }
    
    @isTest static void testRESTPostElse()
    {
         Machine_Version_Parts_List__c mvpl = new Machine_Version_Parts_List__c(Oil_Filter_Quantity_Expected__c=1);
        insert mvpl;
        testAcc = [SELECT Id, Name FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
        Asset serial1 = new Asset(name = 'S483816-EDE99N', accountid = testAcc.Id,Serial_Number_Model_Number__c='S483816-EDE99N',Machine_Version_Parts_List__c=mvpl.id,
                                 Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0,Dchl__c='jhfdhakushkka');
        insert serial1;
        
        system.debug(serial1.id);
        
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Hour_Overage_Threshold_Percent__c = .10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        insert cs;
        //JSON string
        String JSONMsg = '{'+
            '"Service_History": {'+
                '"Date_of_Service__c" : "2018-01-30",'+
                '"Serial__c": "S483816-EDE99N",'+
                '"Current_Hours__c": 23650,'+
                '"Hours_Loaded__c": 0,'+
                '"Hours_Unloaded__c": 0,'+
                '"Gallons_of_Lubricant__c": 0,'+
                '"Separator__c": "",'+
                '"Oil_Filter__c": "",'+
                '"Air_Filter__c": "",'+
                '"Other_Parts__c": "",'+
                '"X10_Yr_Warranty_Kit__c" : "",'+
                '"Maintenance_Kit__c" : "2118315:T",'+
                '"Technician_Email__c" : "nathan.holthaus@gardnerdenver.com",'+
                '"Customer_Feedback__c" : "This is only test feedback from a customer",'+
                '"Service_Notes__c" : "Here are some service notes",'+
                '"Oil_Sample_Taken__c": true,'+
                '"Error_Overrides__c": "Some generated errors are going here.",'+
                '"Cabinet_Filter__C":"2118315:T",'+
                '"Control_Box_Filter__c":"202ECH6013:T",'+
                '"Lubricant__C":"202ECH6013:T"'+
                '},'+
            '"attachments": [{"name":"Test", "ContentType":"PDF", "Body":"text string"}],'+
               '"submissions":[]'+
        '}';
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/service-records/';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        ServiceHistory_Records.postRecord();
    }
    
    @isTest static void testRESTPostCatch()
    {
        testAcc = [SELECT Id, Name FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
        Asset serial1 = new Asset(name = 'S483816smdskd', accountid = testAcc.Id,
                                 Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0,Dchl__c='jhfdhakushkka');
        insert serial1;
        
        system.debug(serial1.id);
        
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Hour_Overage_Threshold_Percent__c = .10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        insert cs;
        //JSON string
        String JSONMsg = '{'+
            '"Service_History": {'+
                '"Date_of_Service__c" : "2018-01-30",'+
                '"Serial__c": "S483816smdskd",'+
                '"Current_Hours__c": 23650,'+
                '"Hours_Loaded__c": 0,'+
                '"Hours_Unloaded__c": 0,'+
                '"Gallons_of_Lubricant__c": 0,'+
                '"Separator__c": "",'+
                '"Oil_Filter__c": "",'+
                '"Air_Filter__c": "",'+
                '"Other_Parts__c": "",'+
                '"X10_Yr_Warranty_Kit__c" : "",'+
                '"Maintenance_Kit__c" : "2118315:T",'+
                '"Technician_Email__c" : "nathan.holthaus@gardnerdenver.com",'+
                '"Customer_Feedback__c" : "This is only test feedback from a customer",'+
                '"Service_Notes__c" : "Here are some service notes",'+
                '"Oil_Sample_Taken__c": true,'+
                '"Error_Overrides__c": "Some generated errors are going here.",'+
                '"Cabinet_Filter__C":"2118315:T",'+
                '"Control_Box_Filter__c":"202ECH6013:T",'+
                '"Lubricant__C":"202ECH6013:T"'+
                '},'+
            '"attachments": [{"name":"Test", "ContentType":"PDF", "Body":"text string"}],'+
               '"submissions":[]'+
        '}';
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/service-records/';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        ServiceHistory_Records.postRecord();
    }
    
    @isTest static void testSerialNumLookUp()
    {
        Machine_Version_Parts_List__c mvpl = new Machine_Version_Parts_List__c(Oil_Filter_Quantity_Expected__c=1);
        insert mvpl;
        testAcc = [SELECT Id, Name FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
        System.debug(testAcc);
        System.debug(mvpl);
        Asset serial1 = new Asset(name = 'S483816-EDE99N', accountid = testAcc.Id,Serial_Number_Model_Number__c='S483816-EDE99N',Machine_Version_Parts_List__c=mvpl.id,
                                 Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0,Dchl__c='jhfdhakushkka');
        insert serial1;
        
        system.debug(serial1.id);
        
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Hour_Overage_Threshold_Percent__c = .10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        insert cs;
        //JSON string
        String JSONMsg = '{'+
            '"Service_History": {'+
                '"Date_of_Service__c" : "2018-01-30",'+
                '"Serial__c": "S483816-EDE99N",'+
                '"Current_Hours__c": 23650,'+
                '"Hours_Loaded__c": 0,'+
                '"Hours_Unloaded__c": 0,'+
                '"Gallons_of_Lubricant__c": 0,'+
                '"Separator__c": "202ECH6013:T",'+
                '"Oil_Filter__c": "2116110:T, 2116110:T, 2116110:F",'+
                '"Air_Filter__c": "2118315:T",'+
                '"Other_Parts__c": "",'+
                '"X10_Yr_Warranty_Kit__c" : "",'+
                '"Maintenance_Kit__c" : "",'+
                '"Technician_Email__c" : "nathan.holthaus@gardnerdenver.com",'+
                '"Customer_Feedback__c" : "This is only test feedback from a customer",'+
                '"Service_Notes__c" : "Here are some service notes",'+
                '"Oil_Sample_Taken__c": true,'+
                '"Error_Overrides__c": "Some generated errors are going here."'+
                '},'+
               '"attachments": [{"name":"Test", "ContentType":"image/png", "Body":"d29ybGQ="}],'+
               '"submissions":[]'+
        '}';
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/serialNum/';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        SerialNumLookup.doPost(serial1.name);
        serial1.DChl__c='CM';
        update serial1;
        SerialNumLookup.doPost(serial1.name);
    }
    
    @isTest static void testSerialNumLookUpV2()
    {
        Machine_Version_Parts_List__c mvpl = new Machine_Version_Parts_List__c(Oil_Filter_Quantity_Expected__c=1);
        insert mvpl;
        testAcc = [SELECT Id, Name FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
        System.debug(testAcc);
        System.debug(mvpl);
        Asset serial1 = new Asset(name = 'S483816-EDE99N', accountid = testAcc.Id,Serial_Number_Model_Number__c='S483816-EDE99N',Machine_Version_Parts_List__c=mvpl.id,
                                 Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0,Dchl__c='jhfdhakushkka');
        insert serial1;
        
        system.debug(serial1.id);
        
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Hour_Overage_Threshold_Percent__c = .10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        insert cs;
        //JSON string
        String JSONMsg = '{'+
            '"Service_History": {'+
                '"Date_of_Service__c" : "2018-01-30",'+
                '"Serial__c": "S483816-EDE99N",'+
                '"Current_Hours__c": 23650,'+
                '"Hours_Loaded__c": 0,'+
                '"Hours_Unloaded__c": 0,'+
                '"Gallons_of_Lubricant__c": 0,'+
                '"Separator__c": "202ECH6013:T",'+
                '"Oil_Filter__c": "2116110:T, 2116110:T, 2116110:F",'+
                '"Air_Filter__c": "2118315:T",'+
                '"Other_Parts__c": "",'+
                '"X10_Yr_Warranty_Kit__c" : "",'+
                '"Maintenance_Kit__c" : "",'+
                '"Technician_Email__c" : "nathan.holthaus@gardnerdenver.com",'+
                '"Customer_Feedback__c" : "This is only test feedback from a customer",'+
                '"Service_Notes__c" : "Here are some service notes",'+
                '"Oil_Sample_Taken__c": true,'+
                '"Error_Overrides__c": "Some generated errors are going here."'+
                '},'+
               '"attachments": [{"name":"Test", "ContentType":"image/png", "Body":"d29ybGQ="}],'+
               '"submissions":[]'+
        '}';
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/serialNumV2/';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        SerialNumLookupV2.doPost(serial1.name);
        serial1.DChl__c='CM';
        update serial1;
        SerialNumLookupV2.doPost(serial1.name);
    }

}