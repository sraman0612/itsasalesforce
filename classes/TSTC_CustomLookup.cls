@IsTest
public class TSTC_CustomLookup {

    @IsTest
    public static void test() {
        Account account = new Account(
            Name = 'otherAccount',
            AccountNumber = '12345',
            DC__c = 'CM'
        );
        
        insert account;
                
                
        CTRL_CustomLookupWithSharing.searchDB('Account', 'Name', 'AccountNumber', 'DC__c', false, 'CM', 'AccountNumber', false, '12345', 10, 'Name', 'otherAccount');
        CTRL_CustomLookupWithSharing.searchDB('Account', 'Name', 'AccountNumber', 'Name', true, 'test', 'AccountNumber', true, 'filter2_value', 10, 'Name', 'Fred');
        CTRL_CustomLookupWithSharing.searchDB('Account', 'Name', 'AccountNumber', 'Name', false, '', 'AccountNumber', false, 'filter2_value', 10, 'Name', 'Fred');
        CTRL_CustomLookupWithSharing.searchDB('Account', 'Name', 'AccountNumber', 'Name', false, '', 'Chosen_Service_Center__c', false, 'FALSE', 10, 'Name', 'Fred');
        CTRL_CustomLookupWithSharing.searchDB('Account', 'Name', 'AccountNumber', 'Name', false, '', 'Chosen_Service_Center__c', false, 'TRUE', 10, 'Name', 'Fred');
        CTRL_CustomLookupWithSharing.searchDB('Account', 'Name', 'AccountNumber', 'Name', false, '', 'Chosen_Service_Center__c', false, '', 10, 'Name', 'Fred');
        CTRL_CustomLookup.searchDB('Account', 'Name', 'AccountNumber', 'DC__c', false, 'CM', 'AccountNumber', false, '12345', 10, 'Name', 'otherAccount');
        CTRL_CustomLookup.searchDB('Account', 'Name', 'AccountNumber', 'Name', true, 'test', 'AccountNumber', true, 'filter2_value', 10, 'Name', 'Fred');
        CTRL_CustomLookup.searchDB('Account', 'Name', 'AccountNumber', 'Name', false, '', 'AccountNumber', false, 'filter2_value', 10, 'Name', 'Fred');
        CTRL_CustomLookup.searchDB('Account', 'Name', 'AccountNumber', 'Name', false, '', 'Chosen_Service_Center__c', false, 'FALSE', 10, 'Name', 'Fred');
        CTRL_CustomLookup.searchDB('Account', 'Name', 'AccountNumber', 'Name', false, '', 'Chosen_Service_Center__c', false, 'TRUE', 10, 'Name', 'Fred');
        CTRL_CustomLookup.searchDB('Account', 'Name', 'AccountNumber', 'Name', false, '', 'Chosen_Service_Center__c', false, '', 10, 'Name', 'Fred');
    }
}