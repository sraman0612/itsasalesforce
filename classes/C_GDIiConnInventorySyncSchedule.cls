global class C_GDIiConnInventorySyncSchedule implements schedulable{
    
    global void execute(SchedulableContext scheduleC){
        C_GDIiConnInventorySyncBatch btch = new C_GDIiConnInventorySyncBatch('');
        id batchprocessid = Database.executebatch(btch,100);
        system.debug('Process ID: ' + batchprocessid);
    }

}