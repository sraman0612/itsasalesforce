public without sharing class CTRL_CustomLookup {
    
    /**
* Returns JSON of list of ResultWrapper to Lex Components
* @objectName - Name of SObject
* @fld_API_Text - API name of field to display to user while searching
* @fld_API_Val - API name of field to be returned by Lookup COmponent
* @lim   - Total number of record to be returned
* @fld_API_Search - API name of field to be searched
* @searchText - text to be searched
* */
    @AuraEnabled(cacheable=true)
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val, String fld_API_filter, Boolean field_is_not_null, String filter_value, String fld2_API_filter, Boolean field2_is_not_null, String filter2_value, Integer lim, String fld_API_Search, String searchText) {
        
        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
        
        if (String.isNotBlank(fld_API_filter)) {
            if (field_is_not_null == true) {
                searchText += ' AND ' + fld_API_filter + ' != null';
            } else if (String.isNotBlank(filter_value)) {
                searchText += ' AND ' + fld_API_filter + ' = \'' + filter_value + '\'';
            } else {
                searchText += ' AND ' + fld_API_filter + ' = null';
            }
        }
        
        if (String.isNotBlank(fld2_API_filter)) {
            if (field2_is_not_null == true) {
                searchText += ' AND ' + fld2_API_filter + ' != null';
            } else if (String.isNotBlank(filter2_value)) {
                if (filter2_value.toUpperCase() == 'FALSE') {
	                searchText += ' AND ' + fld2_API_filter + ' = false';
                } else if (filter2_value.toUpperCase() == 'TRUE') {
	                searchText += ' AND ' + fld2_API_filter + ' = true';
                } else {
	                searchText += ' AND ' + fld2_API_filter + ' = \'' + filter2_value + '\'';
                }

            } else {
				searchText += ' AND ' + fld2_API_filter + ' = null';
            }            
        }
        
        if (lim == null) {
            lim = 4;
        }
        
        String query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
            ' FROM '+objectName+
            ' WHERE '+fld_API_Search+' LIKE '+searchText+ 
            ' LIMIT '+lim;
        System.debug('query=' + query);
        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        
        for(SObject s : sobjList){
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.text = String.valueOf(s.get(fld_API_Text)) ;
            obj.val = String.valueOf(s.get(fld_API_Val))  ;
            lstRet.add(obj);
        } 
        
        return JSON.serialize(lstRet);
    }
    
    public class ResultWrapper{
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
    }
}