public class iConn_Email_Controller {
    
    public String AlertMessage { get; private set; }
    public String AlertSeverity { get; private set; }
    
    public String CurrentServicerPhone { get; private set; }
    public String CurrentServicerName { get; private set; }
    
    public String SerialWarrantyEndDate { get; private set; }
    public String SerialWarrantyType { get; private set; }
    
    public Decimal MachineLoadHours { get; private set; }
    public Decimal MachineRunHours { get; private set; }
    
    public String MachineIdentifier { get; private set; }
    public String IMEI { get; private set; }
    
    public String SerialOwnerPhone { get; private set; }
    public String SerialOwnerCity { get; private set; }
    public String SerialOwnerName { get; private set; }

    public String SerialName { get; private set; }
    public String ModelNumber { get; private set; }
    
    public Boolean hasMVPL {get; private set;}
    
    public Boolean isDist {get; set;}
    
    public String ControlBoxFilter { get; private set; }
    public String CabinetFilter { get; private set; }
    public String Separator { get; private set; }
    public String LubricantFilter { get; private set; }
    public String AirFilter { get; private set; }  
    
    public List<String> AirFilters {get; private set;}
    public List<String> LubricantFilters {get; private set;}
    public List<String> Separators {get; private set;}
    public List<String> CabinetFilters {get; private set;}
    public List<String> ControlBoxFilters {get; private set;}

    public Contact endRecipient {get; 
                                 set{
                                     endRecipient = [SELECT Id, FirstName, Lastname, Email, AccountId 
                                                     FROM Contact 
                                                     WHERE Id = :value.Id];
                                 }}
    
    public Alert__c alert {get; 
                           set{
                               alert = [SELECT Id, Alert_ID__c, Alert_Message__c, IMEI__c, Serial_Number__c, Servicer__c, Code_Block_Status__c, Source_ID__c, Status__c,
                                        Severity__c, Machine_Total_Run_Hours__c, Machine_Load_Hours__c, Serial_Number_Name__c
                                        FROM Alert__c 
                                        WHERE Id = :value.Id];
                               doLogic();
                           }
                          }
    
    public void doLogic(){
        System.debug('iConn_Email_Controller - doLogic()');
        Asset serialNumber;
        Account currentServicer;
        Machine_Version_Parts_List__c mvpl;
        
        if(alert != null){
            
            this.AlertMessage = alert.Alert_Message__c;
            this.AlertSeverity = alert.Severity__c;
            
            serialNumber = [SELECT Id, Model_Number__c, Owner_Name_Community__c, Customer_City_Community__c, Owner_Phone_Community__c, Warranty_Type__c, Warranty_End_Date__c, Currently_Under_Warranty__c, Machine_Identifier__c, Machine_Version_Parts_List__c
                            FROM Asset
                            WHERE Id = :alert.Serial_Number__c];
            
            this.SerialWarrantyType = serialNumber.Warranty_Type__c;
            if(serialNumber.Warranty_End_Date__c != null){
            	this.SerialWarrantyEndDate = serialNumber.Warranty_End_Date__c.format();
            }
            this.SerialOwnerPhone = serialNumber.Owner_Phone_Community__c;
    		this.SerialOwnerCity = serialNumber.Customer_City_Community__c;
    		this.SerialOwnerName = serialNumber.Owner_Name_Community__c;
            
            
            currentServicer = [SELECT Id, iConn_Alert_Preference__c, iConn_Email__c, iConn_Mobile__c, Copy_On_iConn_Notifications__c, Phone, Name 
                               FROM Account
                               WHERE Id = :alert.Servicer__c];
            

            this.CurrentServicerPhone = currentServicer.Phone;
            this.CurrentServicerName = currentServicer.Name;
                
            if(serialNumber.Machine_Version_Parts_List__c != null){
                
                mvpl = [SELECT Id, Air_Filter_1__c, Air_Filter_2__c, Air_Filter_3__c, Air_Filter_4__c, Cabinet_Filter_1__c, Cabinet_Filter_2__c, Cabinet_Filter_3__c, Control_Box_Filter_1__c, Control_Box_Filter_2__c, Control_Box_Filter_3__c, Oil_Filter_2__c, Oil_Filter_1__c, Oil_Filter_3__c, Oil_Filter_4__c, Separator_1__c, Separator_2__c, Separator_3__c, Separator_4__c,
						Air_Filter_1__r.Name, Air_Filter_2__r.Name, Air_Filter_3__r.Name, Air_Filter_4__r.Name, Cabinet_Filter_1__r.Name, Cabinet_Filter_2__r.Name, Cabinet_Filter_3__r.Name, Control_Box_Filter_1__r.Name, Control_Box_Filter_2__r.Name, Control_Box_Filter_3__r.Name, Oil_Filter_2__r.Name, Oil_Filter_1__r.Name, Oil_Filter_3__r.Name, Oil_Filter_4__r.Name, Separator_1__r.Name, Separator_2__r.Name, Separator_3__r.Name, Separator_4__r.Name
						FROM Machine_Version_Parts_List__c
                        WHERE Id = :serialNumber.Machine_Version_Parts_List__c];
                
                if(mvpl != null){
                    this.hasMVPL = true;
                    this.AirFilter = mvpl.Air_Filter_1__c == null ? 'Not Applicable' : mvpl.Air_Filter_1__r.Name;
                    this.CabinetFilter = mvpl.Cabinet_Filter_1__c == null ? 'Not Applicable' : mvpl.Cabinet_Filter_1__r.Name;
                    this.ControlBoxFilter = mvpl.Control_Box_Filter_1__c == null ? 'Not Applicable' : mvpl.Control_Box_Filter_1__r.Name;
                    this.LubricantFilter = mvpl.Oil_Filter_1__c == null ? 'Not Applicable' : mvpl.Oil_Filter_1__r.Name;
                    this.Separator = mvpl.Separator_1__c == null ? 'Not Applicable' : mvpl.Separator_1__r.Name;
                    
             		this.AirFilters = new List<String>();       
                    if(mvpl.Air_Filter_1__c != null) this.AirFilters.add(mvpl.Air_Filter_1__r.Name);
                    if(mvpl.Air_Filter_2__c != null) this.AirFilters.add(mvpl.Air_Filter_2__r.Name);
                    if(mvpl.Air_Filter_3__c != null) this.AirFilters.add(mvpl.Air_Filter_3__r.Name);
                    if(mvpl.Air_Filter_4__c != null) this.AirFilters.add(mvpl.Air_Filter_4__r.Name);
                    if(this.AirFilters.size() == 0) this.AirFilters.add('Not Applicable');
                    
                    this.CabinetFilters = new List<String>();
                    if(mvpl.Cabinet_Filter_1__c != null) this.CabinetFilters.add(mvpl.Cabinet_Filter_1__r.Name);
                    if(mvpl.Cabinet_Filter_2__c != null) this.CabinetFilters.add(mvpl.Cabinet_Filter_2__r.Name);
                    if(mvpl.Cabinet_Filter_3__c != null) this.CabinetFilters.add(mvpl.Cabinet_Filter_3__r.Name);
                    if(this.CabinetFilters.size() == 0) this.CabinetFilters.add('Not Applicable');
                    
                    this.ControlBoxFilters = new List<String>();
                    if(mvpl.Control_Box_Filter_1__c != null) this.ControlBoxFilters.add(mvpl.Control_Box_Filter_1__r.Name);
                    if(mvpl.Control_Box_Filter_2__c != null) this.ControlBoxFilters.add(mvpl.Control_Box_Filter_2__r.Name);
                    if(mvpl.Control_Box_Filter_3__c != null) this.ControlBoxFilters.add(mvpl.Control_Box_Filter_3__r.Name);
                    if(this.ControlBoxFilters.size() == 0) this.ControlBoxFilters.add('Not Applicable');
                    
                    this.LubricantFilters = new List<String>();       
                    if(mvpl.Oil_Filter_1__c != null) this.LubricantFilters.add(mvpl.Oil_Filter_1__r.Name);
                    if(mvpl.Oil_Filter_2__c != null) this.LubricantFilters.add(mvpl.Oil_Filter_2__r.Name);
                    if(mvpl.Oil_Filter_3__c != null) this.LubricantFilters.add(mvpl.Oil_Filter_3__r.Name);
                    if(mvpl.Oil_Filter_4__c != null) this.LubricantFilters.add(mvpl.Oil_Filter_4__r.Name);
                    if(this.LubricantFilters.size() == 0) this.LubricantFilters.add('Not Applicable');
                    
                    this.Separators = new List<String>();       
                    if(mvpl.Separator_1__c != null) this.Separators.add(mvpl.Separator_1__r.Name);
                    if(mvpl.Separator_2__c != null) this.Separators.add(mvpl.Separator_2__r.Name);
                    if(mvpl.Separator_3__c != null) this.Separators.add(mvpl.Separator_3__r.Name);
                    if(mvpl.Separator_4__c != null) this.Separators.add(mvpl.Separator_4__r.Name);
                    if(this.Separators.size() == 0) this.Separators.add('Not Applicable');
                    
                }
            }
            
            this.ModelNumber = serialNumber.Model_Number__c;
            this.SerialName = alert.Serial_Number_Name__c;
            this.MachineLoadHours = alert.Machine_Load_Hours__c; 
            this.MachineRunHours = alert.Machine_Total_Run_Hours__c;
            this.MachineIdentifier = serialNumber.Machine_Identifier__c;
            this.IMEI = alert.IMEI__c;
            
        }
        
    }
    
}