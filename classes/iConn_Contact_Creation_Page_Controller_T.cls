@isTest
public with sharing class iConn_Contact_Creation_Page_Controller_T {
    @testSetup static void setup() {
        Account newParAccnt = new Account(Name = 'Mothership Burgers');
        insert newParAccnt;
        
        Account newAccnt = new Account(Name = 'Bob\'s Burgers', ParentId = newParAccnt.Id);
        insert newAccnt;
        
        Asset newAsset = new Asset(Name = 'testAsset123');
        insert newAsset;
        
        Contact newContact = new Contact(FirstName = 'Bob', LastName = 'Dillon', Email = 'BobDillon@bobsburgers.com', AccountId = newAccnt.Id);
        insert newContact;
    }
    @isTest public static void doTest() {
        Asset a = [SELECT Id FROM Asset WHERE Name = 'testAsset123' LIMIT 1];
        Contact c = [SELECT Id, Email, iConn_Mobile_Number__c FROM Contact WHERE Email = 'BobDillon@bobsburgers.com' LIMIT 1];
        
        c.Email = 'jk' + c.Email;
        c.iConn_Mobile_Number__c = '123-456-7890';
        iConn_Contact_Creation_Page_Controller.updateContact(c);
		
        Serial_Number_Contact__c con = new Serial_Number_Contact__c(Contact__c = c.Id, Serial_Number__c = a.Id, Email_Address__c = 'a@b.com');
        insert con;
        
        iConn_Contact_Creation_Page_Controller.paginationData resp = iConn_Contact_Creation_Page_Controller.getSerialNumbers(c.Id, 'Name', 'ASC', 0, 1);
        
        List<Serial_Number_Contact__c> conList = new List<Serial_Number_Contact__c>();
        conList.add(con);
        
        iConn_Contact_Creation_Page_Controller.insertiConnRecords(c.Id, conList);
    }
}