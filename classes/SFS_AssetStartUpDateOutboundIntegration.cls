/*=========================================================================================================
* @author Naresh Ponneri, Capgemini
* @date 01/04/2022
* @description: SFS_AssetStartUpDateOutboundIntegration(Warranty Claims Startup Date integration)
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/
public class SFS_AssetStartUpDateOutboundIntegration {
    
    public static SFS_Startup_Date_XML_Struct__mdt[] startUpMAP =[SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                                  FROM SFS_Startup_Date_XML_Struct__mdt where SFS_Node_Order__c !=null Order By SFS_Node_Order__c asc];
    public static SFS_Country_Code_Mapping__mdt[] countryList = [SELECT id,Label,SFS_Locale__c from SFS_Country_Code_Mapping__mdt];
    public static Map<string,string> countryMap=new Map<string,string>();    
    public static String xmlString = '';
    public static Map<Double, String> startUpXmlMap = new Map<Double, String>();
    public static List<ProductRequestLineItem> prliUpdateList = new List<ProductRequestLineItem>();
    public static SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT SFS_Endpoint_URL__c, SFS_Username__c, SFS_Password__c
                                                                FROM SFS_SFS_Integration_Endpoint__mdt WHERE Label =:'StartupDate'];
    public static Map<String, Id> divMap = new Map<String, Id>();
    
    @InvocableMethod(label='SFS_AssetStartUpDateOutboundIntegration' description='Asset StartUpDate Integration to Tavant' category='Asset')
    public static void Run(List<Id> astId){
        ProcessData(astId,startUpXmlMap,xmlString);
    }
    
    public static void ProcessData(List<Id> astId, Map<Double, String> startUpXmlMap, String xmlString){
        
        SFS_AssetStartUpDateBatch assetBatch=new SFS_AssetStartUpDateBatch(astId);
        Database.executeBatch(assetBatch,1);
    }
    
    public static void xmlStructure(Map<String,Object> assetFields,asset asset,Map<string,string> countryMap,Map<Id,Account> accountMap){
        
        Map<String, Object> startUpFieldValues = new Map<String, Object>();
       try{
            for(String field : assetFields.keyset()){
                startUpFieldValues.put(field.toLowerCase(), asset.get(field)); 
            }
            system.debug('@@@@'+assetFields);
            List<Double> nodeList = new List<Double>();
            //Replace fieldNames (value) in ScFieldValues with the XML Line and Salesforce Data.
            List<Double> newNodeList = new List<Double>();
            for(SFS_Startup_Date_XML_Struct__mdt xmlStructure : startUpMAP){
                newNodeList.add(xmlStructure.SFS_Node_Order__c);
                //check for hardcoded flag. If false, replace data.
                if(xmlStructure.SFS_Hardcoded_Flag__c == false){
                    String sfField = xmlStructure.SFS_Salesforce_Field__c;
                    if(startUpFieldValues.containsKey(sfField)){
                        double nodeOrder = xmlStructure.SFS_Node_Order__c;
                        //check if value from query is null
                        if(startUpFieldValues.get(sfField) != null){
                            system.debug('@@sfField'+sfField);
                            String xmlFullName = xmlStructure.SFS_XML_Full_Name__c;
                            String replacement;
                            if(sfField =='manufacturedate' || sfField =='start_up_date__c' || sfField =='sfs_shipment_date__c' || sfField =='sfs_oracle_ship_date__c' || sfField=='invoice_date__c' ||sfField=='ship_date__c'){
                                String dateString=String.valueOf(startUpFieldValues.get(sfField));   
                                replacement=dateString.removeEnd(' 00:00:00');
                            }
                            else{
                                replacement = String.ValueOf(startUpFieldValues.get(sfField));
                                replacement = replacement.escapeXML();
                                system.debug('@@replacement'+replacement);
                            }
                            String newLine = xmlFullName.replace(sfField, replacement);
                            system.debug('@@newLine'+newLine);
                            startUpXmlMap.put(nodeOrder, newLine);
                        }
                        else if(startUpFieldValues.get(sfField) == null){
                            startUpXmlMap.put(xmlStructure.SFS_Node_Order__c, xmlStructure.SFS_XML_Full_Name__c);
                        }
                    }
                    else if(sfField=='ShippingStreet'){
                        SFS_AssetStartUpDateOutboundIntegration.getFieldMap(String.ValueOf(accountMap.get(asset.AccountId).ShippingStreet),sfField,xmlStructure.SFS_Node_Order__c,xmlStructure.SFS_XML_Full_Name__c);
                    }
                    else if(sfField=='shippingcity'){
                        getFieldMap(String.ValueOf(accountMap.get(asset.AccountId).ShippingCity),sfField,xmlStructure.SFS_Node_Order__c,xmlStructure.SFS_XML_Full_Name__c);
                        
                    }
                    else if(sfField=='shippingstate'){
                        getFieldMap(String.ValueOf(accountMap.get(asset.AccountId).ShippingState),sfField,xmlStructure.SFS_Node_Order__c,xmlStructure.SFS_XML_Full_Name__c);
                        
                    }
                    else if(sfField=='shippingpostalcode'){
                        getFieldMap(String.ValueOf(accountMap.get(asset.AccountId).ShippingPostalCode),sfField,xmlStructure.SFS_Node_Order__c,xmlStructure.SFS_XML_Full_Name__c);
                        
                    }
                    else if(sfField=='shippingcountry'){
                        getFieldMap(String.ValueOf(accountMap.get(asset.AccountId).ShippingCountry),sfField,xmlStructure.SFS_Node_Order__c,xmlStructure.SFS_XML_Full_Name__c);
                        
                    }
                    else if(sfField=='locale'){
                        String xmlFullName = xmlStructure.SFS_XML_Full_Name__c;
                        if(accountMap.get(asset.AccountId).ShippingCountry!=null){
                            string country= String.ValueOf(accountMap.get(asset.AccountId).ShippingCountry);
                            String replacement = countryMap.get(country);
                            String newLine = xmlFullName.replace(sfField, replacement);
                            startUpXmlMap.put(xmlStructure.SFS_Node_Order__c, newLine);
                        }
                        else{
                            String empty = '';
                            String replacement = xmlStructure.SFS_XML_Full_Name__c.replace(sfField, empty);
                            replacement = replacement.escapeXML();
                            startUpXmlMap.put(xmlStructure.SFS_Node_Order__c, replacement);
                        }
                    }
                    else if(sfField=='irit_customer_number__c'){
                        getFieldMap(String.ValueOf(accountMap.get(asset.AccountId).IRIT_Customer_Number__c),sfField,xmlStructure.SFS_Node_Order__c,xmlStructure.SFS_XML_Full_Name__c);
                    }
                    //if SCFieldMap does not contain sffield, the field value is null. Replace null value with just tags.
                    else if(!startUpFieldValues.ContainsKey(sfField)){
                        String empty = '';
                        String replacement = xmlStructure.SFS_XML_Full_Name__c.replace(sfField, empty);
                        replacement = replacement.escapeXML();
                        startUpXmlMap.put(xmlStructure.SFS_Node_Order__c, replacement);
                    }
                }
                else if(xmlStructure.SFS_Hardcoded_Flag__c == true){
                    startUpXmlMap.put(xmlStructure.SFS_Node_Order__c, xmlStructure.SFS_XML_Full_Name__c);
                }
            }
            newNodeList.sort();
            List<String>finalXML = new List<String>();
            for(Double n : newNodeList){
                system.debug('@@@@Finalvalue'+startUpXmlMap.get(n));
                finalXML.add(startUpXmlMap.get(n));
            }
            for(String s : finalXML){
                xmlString = xmlString + s;
               
            }
            System.debug('---------FinalXML---------'+xmlString);
            String interfaceLabel = 'Asset Start up date to Tavant';
            HttpMethod(xmlString,interfaceLabel,asset.Id);
        }
        catch(SObjectException e){
            system.debug('@@@exception'+e.getMessage());
        }
    }
    
    //Http Method to send xml and recieve success or failure code.
    public static void HttpMethod(String xmlString, String interfaceLabel,String assetId){
        try{
            List<Asset> assetUpdateList = new List<Asset>();
            system.debug('insideHttpCallout'+endpoint.SFS_Endpoint_URL__c);
            //Http Request
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint.SFS_Endpoint_URL__c);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'text/xml');
            request.setTimeout(120000);
            request.setBody(xmlString);
            System.debug('Payload: ' + xmlString);
            
            //Get Http Response
            HttpResponse response = http.send(request);
            system.debug('@@@response'+response);
            System.debug('STATUS: ' + response.getStatusCode());
            System.debug('Error Message: ' +response.getBody());
            String statusFlag;String resMessage;String StatusMessage;
            //if(response.getStatusCode() == 200){
            Dom.Document doc = response.getBodyDocument();
            system.debug('@@@@@@@@@@@@doc'+doc);
            //Retrieve the root element for this document.
            Dom.XMLNode envelope = doc.getRootElement();
            for(DOM.XmlNode node : envelope.getChildElements())
            { 
                if(node.getName() =='Body') {
                    for(DOM.XmlNode trans: node.getChildElements()) {
                        if(trans.getName() =='SNTransactionResponse'){
                            for(DOM.XmlNode status: trans.getChildElements()) {
                                if(status.getName() =='Status'){
                                    system.debug('@@@@@@@@value'+status.getText());
                                    statusFlag = status.getText();
                                }
                                if(status.getName() =='Message'){
                                    system.debug('@@@@@@@@value'+status.getText());
                                    resMessage = status.getText();
                                }
                                if(statusFlag=='SUCCESS'){
                                    StatusMessage='APPROVED';
                                }
                                else{
                                    if(status.getName() =='ErrorCodes'){
                                        for(DOM.XmlNode errorCode: status.getChildElements()) {
                                            if(errorCode.getName() == 'EachErrorCode') {
                                                for(DOM.XmlNode error: errorCode.getChildElements()) {
                                                    if(error.getName() =='errorMessage'){
                                                        resMessage = error.getText(); 
                                                        SYSTEM.debug('@@@@@@@@@@@resMessage'+resMessage);
                                                    }
                                                }
                                            }
                                        }
                                         StatusMessage='ERROR';
                                    }
                                }
                            }
                        }
                    }
                }
            }
                Asset ase=new Asset();
                ase.Id = assetId;
                ase.SFS_StartUp_Integration_Status__c=StatusMessage;
                ase.SFS_Startup_Integration_Response__c=resMessage;
                assetUpdateList.add(ase);
            if(assetUpdateList.size()>0 && !assetUpdateList.isEmpty()){
                update assetUpdateList;
            }
        }
        catch(SObjectException e){
            system.debug('@@@exception'+e.getMessage());
        }
    }
    
    public static void getFieldMap(String fieldValue,String field,Decimal nodeOrder,String xmlFullName){
        if(fieldValue!=null && fieldValue!='' ){
            String replacement =fieldValue;
            replacement = replacement.escapeXML();
            String newLine = xmlFullName.replace(field, replacement);
            startUpXmlMap.put(nodeOrder, newLine);
        }
        else{
            String empty = '';
            String replacement =xmlFullName.replace(field, empty);
            startUpXmlMap.put(nodeOrder, replacement);
        }
    }
}