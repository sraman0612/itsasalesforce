public class C_InvoiceFinishLocationFlowController {

    public C_InvoiceFinishLocationFlowController(ApexPages.StandardController controller) {

    }

public flow.interview.Invoice_Record_Creation myflow {get;set;}

    public C_InvoiceFinishLocationFlowController() {
    }

    public String getendID() {

        if (myflow !=null) return myflow.VarRecordID;
        else return 'home/home.jsp';
        }

public PageReference navigateToNewRecord {
     get { PageReference prRef = new PageReference('/' + getendID());
 
         prRef.setRedirect(true);
         return prRef;
         }
     set { navigateToNewRecord = value; }
         }

}