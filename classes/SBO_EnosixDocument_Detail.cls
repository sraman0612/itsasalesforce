/// enosiX Inc. Generated Apex Model
/// Generated On: 8/27/2019 1:20:31 PM
/// SAP Host: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixDocument_Detail extends ensxsdk.EnosixFramework.DetailSBO
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixDocument_Detail_Meta', new Type[] {
            SBO_EnosixDocument_Detail.EnosixDocument.class
            , SBO_EnosixDocument_Detail.ATTACHMENTS.class
            } 
        );
    }

    public SBO_EnosixDocument_Detail()
    {
        super('EnosixDocument', SBO_EnosixDocument_Detail.EnosixDocument.class);
    }

    public override Type getType() { return SBO_EnosixDocument_Detail.class; }

    public EnosixDocument initialize(EnosixDocument obj)
    {
        return (EnosixDocument)this.executeInitialize(obj);
    }
    
    public EnosixDocument getDetail(object key)
    {
        return (EnosixDocument)this.executeGetDetail(key);
    }
    
    public EnosixDocument save(EnosixDocument obj)
    {
        return (EnosixDocument) this.executeSave(obj);
    }

    public EnosixDocument command(string command, EnosixDocument obj)
    {
        return (EnosixDocument) this.executeCommand(command, obj);
    }
    
    public with sharing class EnosixDocument extends ensxsdk.EnosixFramework.DetailObject
    {
        public EnosixDocument()
        {
            super('HEADER', new Map<string,type>
                {
                    'ATTACHMENTS' => SBO_EnosixDocument_Detail.ATTACHMENTS_COLLECTION.class
                });	
        }

        public override Type getType() { return SBO_EnosixDocument_Detail.EnosixDocument.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixDocument_Detail.registerReflectionInfo();
        }

                @AuraEnabled public String DocumentType
        { 
            get { return this.getString ('DOKAR'); } 
            set { this.Set (value, 'DOKAR'); }
        }

        @AuraEnabled public String DocumentTypeDescription
        { 
            get { return this.getString ('DARTXT'); } 
            set { this.Set (value, 'DARTXT'); }
        }

        @AuraEnabled public String DocumentNumber
        { 
            get { return this.getString ('DOKNR'); } 
            set { this.Set (value, 'DOKNR'); }
        }

        @AuraEnabled public String DocumentVersion
        { 
            get { return this.getString ('DOKVR'); } 
            set { this.Set (value, 'DOKVR'); }
        }

        @AuraEnabled public String DocumentPart
        { 
            get { return this.getString ('DOKTL'); } 
            set { this.Set (value, 'DOKTL'); }
        }

        @AuraEnabled public String DocumentDescription
        { 
            get { return this.getString ('DKTXT'); } 
            set { this.Set (value, 'DKTXT'); }
        }

        @AuraEnabled public String DocumentStatus
        { 
            get { return this.getString ('DOKST'); } 
            set { this.Set (value, 'DOKST'); }
        }

        @AuraEnabled public String DocumentStatusDescription
        { 
            get { return this.getString ('DOSTX'); } 
            set { this.Set (value, 'DOSTX'); }
        }

        @AuraEnabled public String CreatedBy
        { 
            get { return this.getString ('DWNAM'); } 
            set { this.Set (value, 'DWNAM'); }
        }

        @AuraEnabled public String Laboratory
        { 
            get { return this.getString ('LABOR'); } 
            set { this.Set (value, 'LABOR'); }
        }

        @AuraEnabled public String ChangeNumber
        { 
            get { return this.getString ('AENNR'); } 
            set { this.Set (value, 'AENNR'); }
        }

        @AuraEnabled public String AuthorizationGroup
        { 
            get { return this.getString ('BEGRU'); } 
            set { this.Set (value, 'BEGRU'); }
        }

        @AuraEnabled public ATTACHMENTS_COLLECTION ATTACHMENTS
        {
            get 
            { 
                return (ATTACHMENTS_COLLECTION)this.getCollection(SBO_EnosixDocument_Detail.ATTACHMENTS_COLLECTION.class); 
            }
        }

            }

    //Write child objects
        public class ATTACHMENTS extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixDocument_Detail.ATTACHMENTS.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixDocument_Detail.registerReflectionInfo();
        }

        public override List<string> getKeyFields()
        {
            List<string> keyFields = new List<string>{ 'EnosixObjKey','iindex' };
    
            return keyFields;
        }

                @AuraEnabled public Integer FileIndex
        { 
            get { return this.getInteger ('FILE_IDX'); } 
            set { this.Set (value, 'FILE_IDX'); }
        }

        @AuraEnabled public String FileType
        { 
            get { return this.getString ('DAPPL'); } 
            set { this.Set (value, 'DAPPL'); }
        }

        @AuraEnabled public String FileTypeDescription
        { 
            get { return this.getString ('CVTEXT'); } 
            set { this.Set (value, 'CVTEXT'); }
        }

        @AuraEnabled public String FileName
        { 
            get { return this.getString ('FILENAME'); } 
            set { this.Set (value, 'FILENAME'); }
        }

        @AuraEnabled public String HTMLContentType
        { 
            get { return this.getString ('MIMETYPE'); } 
            set { this.Set (value, 'MIMETYPE'); }
        }

        @AuraEnabled public String GetFile
        { 
            get { return this.getString ('GET_FILE'); } 
            set { this.Set (value, 'GET_FILE'); }
        }

        @AuraEnabled public String FileB64String
        { 
            get { return this.getString ('FILE_B64STR'); } 
            set { this.Set (value, 'FILE_B64STR'); }
        }

    }
    public class ATTACHMENTS_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public ATTACHMENTS_COLLECTION()
        {
            super('ATTACHMENTS', SBO_EnosixDocument_Detail.ATTACHMENTS.class, null);
        }

        @AuraEnabled public List<SBO_EnosixDocument_Detail.ATTACHMENTS> getAsList()
        {
            return (List<SBO_EnosixDocument_Detail.ATTACHMENTS>)this.buildList(List<SBO_EnosixDocument_Detail.ATTACHMENTS>.class);
        }
    }
}