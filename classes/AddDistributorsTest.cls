@isTest
private class AddDistributorsTest {

    class MockSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        Boolean isSuccess;
        public MockSearch(Boolean isSuccess)
        {
            this.isSuccess = isSuccess;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            SBO_EnosixZAPR_Search.EnosixZAPR_SC searchContext = (SBO_EnosixZAPR_Search.EnosixZAPR_SC)sc;
            SBO_EnosixZAPR_Search.EnosixZAPR_SR searchResult = new SBO_EnosixZAPR_Search.EnosixZAPR_SR();

            if (isSuccess)
            {
                SBO_EnosixZAPR_Search.SEARCHRESULT result = new SBO_EnosixZAPR_Search.SEARCHRESULT();
                result.SalesOrganization = searchContext.SEARCHPARAMS.SalesOrganization;
                result.DistributionChannel = searchContext.SEARCHPARAMS.DistributionChannel;
                result.Country = searchContext.SEARCHPARAMS.Country;
                result.Region = searchContext.SEARCHPARAMS.Region;
                result.County = searchContext.SEARCHPARAMS.County;
                result.CustomerNumber = '12345';
                result.Sequence = '1';
                result.SalesDistrict = '43';
                result.SalesDistrictName = 'McTester,Testy';

                searchResult.SearchResults.add(result);

                result = new SBO_EnosixZAPR_Search.SEARCHRESULT();
                result.SalesOrganization = searchContext.SEARCHPARAMS.SalesOrganization;
                result.DistributionChannel = searchContext.SEARCHPARAMS.DistributionChannel;
                result.Country = searchContext.SEARCHPARAMS.Country;
                result.Region = searchContext.SEARCHPARAMS.Region;
                result.County = searchContext.SEARCHPARAMS.County;
                result.CustomerNumber = '32355';
                result.Sequence = '2';
                result.SalesDistrict = '43';
                result.SalesDistrictName = 'Nasty McRudey';

                searchResult.SearchResults.add(result);
            }

            searchResult.setSuccess(isSuccess);
            searchContext.baseResult = searchResult;

			return searchContext;
        }
    }

    @testSetup
    static void setup() {
        List<Lead> leads = new List<Lead>();
        // add test leads
        //for (Integer i = 0; i < 50; i++) {
            leads.add(new Lead(
                FirstName = 'Test',
                LastName = 'Lead', // '+i,
                Company = 'TestCo',
                Country = 'US',
                State = 'IL',
                ProductCategory__c = 'Blower'
            ));
        //}
        insert leads;

        Account repAccountTest = new Account();
        repAccountTest.name = 'Rep Test';
        repAccountTest.Account_Number__c = '12345';
        insert repAccountTest;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' LIMIT 1];

        User repUserTest = new User();
        repUserTest.Alias = 'kwink99';
        repUserTest.Email = 'Kerry.Wink@gdi.gdi';
        repUserTest.EmailEncodingKey = 'UTF-8';
        repUserTest.FirstName = 'Kerry';
        repUserTest.LastName = 'Wink';
        repUserTest.LanguageLocaleKey = 'en_US';
        repUserTest.LocaleSidKey = 'en_US';
        repUserTest.ManagerId = UserInfo.getUserId();
        repUserTest.ProfileId = p.Id;
        repUserTest.TimeZoneSidKey = 'America/Los_Angeles';
        repUserTest.UserName = 'Kerry.Wink@' + Math.random() + 'testorg.com';
        insert repUserTest;
    }

    static testmethod void testQueueable1() {

        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSearch(true));

        // Set up test data to pass to queueable class
        Lead currLead = [SELECT Id,FLD_Sales_Organization__c,FLD_Customer_Numbers__c FROM Lead LIMIT 1];

        String salesOrg = 'GDMI';
        String dcVal = 'DV';
        String country = 'US';
        String region = 'NY';
        String county = null;
        String repAccount = '12345';
        String repSalesDistrict = '112';
        String repSalesDistrictName = 'Kerry Wink';
        // Create our Queueable instance
        AddDistributors adst = new AddDistributors(currLead, salesOrg, dcVal, country, region, county, repAccount, repSalesDistrict, repSalesDistrictName);
        // startTest/stopTest block to force async processes to run
        Test.startTest();
        System.enqueueJob(adst);
        Test.stopTest();
        // Validate the job ran. Check if the correct number of leads were created
        System.assertEquals(1, [SELECT count() FROM Lead WHERE FLD_Sales_Organization__c = :currLead.FLD_Sales_Organization__c]);
    }
}