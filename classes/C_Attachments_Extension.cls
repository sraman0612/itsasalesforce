public class C_Attachments_Extension {

    public String attchID {get; set;}
    public List<Attachment> theAttachments {get; private set;}
    public List<AttachmentWrapper> thewrappers { 
        get;
        private set;}
    public Case theCase {get; private set;}

    public C_Attachments_Extension(ApexPages.StandardController controller) {
        this.theCase = (Case) controller.getRecord();
        this.theAttachments = [SELECT Id, Name, Description, ContentType FROM Attachment WHERE Parent.Id = :this.theCase.Id ORDER BY CreatedDate ASC];
        this.thewrappers = new List<AttachmentWrapper>();
        for(Attachment att : theAttachments){
            thewrappers.add(new AttachmentWrapper(att));
        }
    }
    
    public void redo(){
        this.theAttachments = [SELECT Id, Name, Description, ContentType FROM Attachment WHERE Parent.Id = :this.theCase.Id ORDER BY CreatedDate ASC];
        this.thewrappers = new List<AttachmentWrapper>();
        for(Attachment att : theAttachments){
            thewrappers.add(new AttachmentWrapper(att));
        }
    }

    public class AttachmentWrapper{
        public Attachment attach {get; private set;}
        public boolean cBox {get; set;}
        
        public AttachmentWrapper(Attachment a){
            this.attach = a;
            this.cBox = false;
        }
    }
    
    public void cloneAttachments()
    {
        Set<Id> attIds = new Set<Id>();
        List<Attachment> toClone = new List<Attachment>();
        
        for(AttachmentWrapper aw : theWrappers)
        {
            if(aw.cBox){
                attIds.add(aw.attach.Id);
            }
        }
        
        for(Attachment a : [SELECT Id, Name, Description, ContentType, Body FROM Attachment WHERE id in: attIds ORDER BY CreatedDate ASC])
        {
            if(theCase.ParentId!=null)
            {
                Attachment aTemp = new Attachment();
                aTemp = a.clone();
                aTemp.ParentId = getTopLevelCase(theCase.Id);
                toClone.add(aTemp);
            }
        }
        database.insert(toClone,false);
    }

	public String getTopLevelCase( String caseId ){
        Boolean topLevelParent = false;
        while ( !topLevelParent ) {
            Case c = [ Select Id, ParentId From Case where Id =: caseId limit 1 ];
            if ( c.ParentID != null ) {
                caseId = c.ParentID;
            }
            else {
                topLevelParent = true;
            }
        }
        return caseId;
    }
    
    public void deleteAttachments(){
        List<Attachment> toDelete = new List<Attachment>();
        List<AttachmentWrapper> awTemp = new List<AttachmentWrapper>();
        for(AttachmentWrapper aw : theWrappers){
            if(aw.cBox){
                System.debug('To Delete: ' + aw.Attach.Id);
                toDelete.add(aw.attach);
            }
            else{
                awTemp.add(aw);
            }
        }
        if(toDelete.size() > 0){
            try{
                delete toDelete;
                theWrappers = awTemp;
            }
            catch(exception e){
                System.debug(e);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            }
        }
        //return new PageReference('/apex/C_Attachments');
    }

    public void createNewFiles_Stage () {
        createNewFiles(attchID);
    }

    public void createNewFiles(String attId) {
        //Get attachment
        Attachment att = [SELECT Id, OwnerId, Name, Body, ContentType, ParentId From Attachment WHERE Id = :attId];

        if (att.Id != null) {
            //Insert ContentVersion
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
            cVersion.PathOnClient = att.Name; //File name with extention
            cVersion.Origin = 'H'; //C-Content Origin. H-Chatter Origin.
            cVersion.OwnerId = att.OwnerId;
            cVersion.Title = att.Name; //Name of the file
            cVersion.VersionData = att.Body; //File content
            insert cVersion;

            Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cVersion.Id].ContentDocumentId;

            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocId; //Add ContentDocumentId
            cDocLink.LinkedEntityId = theCase.Id; //Add attachment parentId
            cDocLink.ShareType = 'I'; //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            cDocLink.Visibility = 'AllUsers'; //AllUsers, InternalUsers, SharedUsers
            insert cDocLink;
        }
        //Call method to refresh file lists used when reloading the VF page
        redo();
    }
}