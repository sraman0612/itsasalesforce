public with sharing class CTRL_CPQ_Sales_Org
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_CPQ_Sales_Org.class);

    private static String defaultCustomerNumber = (String)UTIL_AppSettings.getValue('CPQSimulation.CustomerNumber', '');
    private static Set<String> genericSalesOrgs = (Set<String>)UTIL_AppSettings.getSet('CPQSimulation.GenericCustomerSalesOrgs', String.class, new Set<String>{''});

    @AuraEnabled
    public static UTIL_Aura.Response getSalesAreaList(String customerNumber) 
    {   
        Object responseData = null;
        logger.enterAura('getSalesAreaList', new Map<String, Object> {
            'customerNumber' => customerNumber
        });
        try
        {   
            if(customerNumber == null)
            {
                UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.CustomerNumber_Not_Found);
            }
            else
            {
                SBO_EnosixCustomer_Detail.EnosixCustomer cpqCustomer = UTIL_Customer.getCustomerByNumber(customerNumber);
                if (cpqCustomer == null)
                {
                    UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.CPQ_Quote_ErrorNotLinked);
                }
                else
                {
                    if(cpqCustomer.SALES_DATA.size() == 0)
                    {
                        UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.CPQ_Quote_ErrorNoSalesOrgs);
                    }
                    if (cpqCustomer.CustomerNumber == defaultCustomerNumber)
                    {
                        for (SBO_EnosixCustomer_Detail.SALES_DATA sd : cpqCustomer.SALES_DATA.getAsList())
                        {
                            if (!genericSalesOrgs.contains(sd.SalesOrganization))
                            {
                                cpqCustomer.SALES_DATA.remove(sd);
                            }
                        }
                    }
                    responseData = cpqCustomer; 
                }
            }

        }
        catch(Exception ex)
        {
            UTIL_PageMessages.addExceptionMessage(ex);
        }
        finally
        {
            logger.exit();
        }

        return UTIL_Aura.createResponse(responseData);
    }
}