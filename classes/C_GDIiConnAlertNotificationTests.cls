@isTest
public class C_GDIiConnAlertNotificationTests {
    @isTest
    static void positiveTestAlertNotiy(){
        
        Asset a = new Asset(SerialNumber='622165533test',Name='iConnMachinetest');
        insert a;
        Alert__c al = new Alert__c(Alert_ID__c='622165533',Serial_Number__c=a.id);
        insert al;
        RealTimeAlert__c rta = new RealTimeAlert__c(clientid__c='test');
        insert rta;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new C_GDIHttpMockNotify());
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://example.com/example/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        system.enqueueJob(new C_GDIiConnAlertNotificationQueueable());
        Test.stopTest();
    }
    
    @isTest
    static void negativeTestAlertNotiy(){
        
        
        Asset a = new Asset(SerialNumber='622165533test',Name='iConnMachinetest');
        insert a;
        Alert__c al = new Alert__c(Alert_ID__c='622165533',Serial_Number__c=a.id);
        insert al;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new C_GDIHttpMockNotify());
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://example.com/example/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        system.enqueueJob(new C_GDIiConnAlertNotificationQueueable());
        Test.stopTest();  
    }    
     
    public class C_GDIHttpMockNotify implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            
            // Create a fake response.
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(' [{"data":{"realtimeAction":"UPDATE","data":{"count":2,"creationTime":"2018-12-11T00:09:13.135+01:00","time":"2018-12-13T15:33:25.402+01:00","firstOccurrenceTime":"2018-12-11T00:09:13.135+01:00","history":{"auditRecords":[],"self":"http://industrials.iconn.gardnerdenver.com/audit/auditRecords"},"id":"622165533","self":"http://industrials.iconn.gardnerdenver.com/alarm/alarms/622165533","severity":"MAJOR","source":{"id":"192171580","self":"http://industrials.iconn.gardnerdenver.com/inventory/managedObjects/192171580"},"status":"CLEAR","text":"No data received from device within required interval.","type":"c8y_UnavailabilityAlarm"}},"channel":"/alarms/192171580","id":"70055412"},{"advice":{"interval":30000,"timeout":5400000,"reconnect":"retry"},"channel":"/meta/connect","successful":true}]');
            res.setStatusCode(200);
            return res;
            
        }
    }
}