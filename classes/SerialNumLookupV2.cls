@RestResource(urlMapping='/serialNumV2')
global with sharing class SerialNumLookupV2 {
    @httpPost
    global static void doPost(String serialNum) {
        RestContext.response.addHeader('Content-Type', 'application/json');
        try {
        Asset a = [SELECT AccountId,Account_Number__c,active_iconn__c,Aftercooler__c,Air_Filter__c,Case__c,Change_Date__c,Comments__c,Competitive_Unit__c,ContactId,Contract_Number__c,
                       Contract_Start_Date__c,Contract_Type__c,CreatedById,CreatedDate,Currently_Under_Warranty__c,Date_of_Adoption__c,DChl__c,DelDate__c,Description,Description_of_Technical_Object__c,
                       Distributor_Name__c,Drive_Motor__c,Equipment_Type__c,Extended_Wear__c,Flange_Size__c,H_P__c,Id,iConn_c8y_H_total__c,iConn_c8y_H_load__c,iConn_Measurement_Last_Change__c,IMEI__c,Inactive__c,InstallDate,IsCompetitorProduct,
                       IsDeleted,Item__c,LastModifiedDate,Machine_Speed__c,Manufacturer__c,
                       Material_Description__c,MG_4__c,Model_Number__c,Model_Type_Other__c,Model_Type__c,Model__c,MPG4_Code__c,Name,Oil_Cooler_Fitted__c,
                       Oil_Filter__c,Oper_From__c,Owner_Address_Line_1__c,Owner_City__c,Owner_Country__c,Owner_Name__c,Owner_State__c,Owner_Zip_Code__c,
                       Packing_Box__c,ParentId,Parent_Account__c,Previous_Actual_Type_of_Maintenance__c,Price,Product2Id,Product_Hierarchy__c,PurchaseDate,
                       Quantity,RecordTypeId,RootAssetId,Sales_Order__c,SCRAPERS__c,SerialNumber,Serial_Number_18_Digit_ID__c,Serial_Number_Model_Number__c,
                       Ship_Date__c,Sold_to__c,SOrg__c,Special_Modifications__c,Start_up_Date__c,Status,Store_Number__c,Store__c,SystemModstamp,Type_of_Scheduled_Maintenance__c,
                       UsageEndDate,Warranty_Type__c,Warranty_No__c,X1ST_STG_VLVS__c,X3rd_Stg_Suct_Valve__c,Year_of_Maintenance__c, Warranty_Status__c, Warranty_End_Date__c,
                       Air_Filter_Compliance__c,
                        Cabinet_Filter_Compliance__c,
                        Compliance_Exception_Air_Filter__c,
                        Compliance_Exception_Cabinet_Filter__c,
                        Compliance_Exception_Control_Box__c,
                        Compliance_Exception_Lubricant__c,
                        Compliance_Exception_Oil_Filter__c,
                        Compliance_Exception_Separator__c,
                        Compliant_Air_Filter__c,
                        Compliant_Cabinet_Filter__c,
                        Compliant_Control_Box_Filter__c,
                        Compliant_Lubricant__c,
                        Compliant_Oil_Filter__c,    
                        Compliant_Separator__c,
                        Compliant_Time__c,
                        Control_Box_Filter_Compliance__c,
                        Lubricant_Compliance__c,
                        Oil_Filter_Compliance__c,
                        Separator_Compliance__c,
                        Warranty_Compliance_Level__c,
                       CHAR1__C, CHAR_TEXT1__C, CHAR2__C, CHAR_TEXT2__C, CHAR3__C, CHAR_TEXT3__C, 
                       CHAR4__C, CHAR_TEXT4__C, CHAR5__C, CHAR_TEXT5__C, CHAR6__C, CHAR_TEXT6__C, CHAR7__C, CHAR_TEXT7__C, CHAR8__C, CHAR_TEXT8__C, CHAR9__C, CHAR_TEXT9__C, CHAR10__C, CHAR_TEXT10__C, 
                       CHAR11__C, CHAR_TEXT11__C, CHAR12__C, CHAR_TEXT12__C, CHAR13__C, CHAR_TEXT13__C, CHAR14__C, CHAR_TEXT14__C, CHAR15__C, CHAR_TEXT15__C, CHAR16__C, CHAR_TEXT16__C, 
                       CHAR17__C, CHAR_TEXT17__C, CHAR18__C, CHAR_TEXT18__C, CHAR19__C, CHAR_TEXT19__C, CHAR20__C, CHAR_TEXT20__C, CHAR21__C, CHAR_TEXT21__C, CHAR22__C, CHAR_TEXT22__C, CHAR23__C, CHAR_TEXT23__C, 
                       CHAR24__C, CHAR_TEXT24__C, CHAR25__C, CHAR_TEXT25__C, Machine_Version_Parts_List__r.Air_Filter_1__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Air_Filter_2__r.Part_Number__c, Machine_Version_Parts_List__r.Air_Filter_3__r.Part_Number__c, Machine_Version_Parts_List__r.Air_Filter_4__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Air_Filter_Quantity_Expected__c, Machine_Version_Parts_List__r.Air_Filter_Unique_Products_Expected__c,
                       Machine_Version_Parts_List__r.Cabinet_Filter_Quantity_Expected__c, Machine_Version_Parts_List__r.Cabinet_Filter_Unique_Products_Expected__c,
                       Machine_Version_Parts_List__r.Control_Box_Filter_Quantity_Expected__c, Machine_Version_Parts_List__r.Control_Box_Filter_Unique_Expected__c,
                       Machine_Version_Parts_List__r.Maintenance_Kit_Quantity_Expected__c, Machine_Version_Parts_List__r.Maintenance_Kit_Unique_Products_Expected__c,
                       Machine_Version_Parts_List__r.Oil_Filter_Quantity_Expected__c, Machine_Version_Parts_List__r.Oil_Filter_Unique_Products_Expected__c,
                       Machine_Version_Parts_List__r.Separator_Quantity_Expected__c, Machine_Version_Parts_List__r.Separator_Unique_Products_Expected__c, Machine_Version_Parts_List__r.Lubricant_Quantity__c,
                       Machine_Version_Parts_List__r.Cabinet_Filter_1__r.Part_Number__c, Machine_Version_Parts_List__r.Cabinet_Filter_2__r.Part_Number__c, Machine_Version_Parts_List__r.Cabinet_Filter_3__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Control_Box_Filter_1__r.Part_Number__c, Machine_Version_Parts_List__r.Control_Box_Filter_2__r.Part_Number__c, Machine_Version_Parts_List__r.Control_Box_Filter_3__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Lubricant_1__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_2__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_3__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_4__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Lubricant_5__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_6__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_7__r.Part_Number__c, Machine_Version_Parts_List__r.Lubricant_8__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Maintenance_Kit_1__r.Part_Number__c, Machine_Version_Parts_List__r.Maintenance_Kit_2__r.Part_Number__c, Machine_Version_Parts_List__r.Maintenance_Kit_3__r.Part_Number__c, 
                       Machine_Version_Parts_List__r.Oil_Filter_1__r.Part_Number__c, Machine_Version_Parts_List__r.Oil_Filter_2__r.Part_Number__c, Machine_Version_Parts_List__r.Oil_Filter_3__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Oil_Filter_4__r.Part_Number__c, Machine_Version_Parts_List__r.Separator_1__r.Part_Number__c, Machine_Version_Parts_List__r.Separator_2__r.Part_Number__c,
                       Machine_Version_Parts_List__r.Separator_3__r.Part_Number__c, Machine_Version_Parts_List__r.Separator_4__r.Part_Number__c,         
                       Machine_Version_Parts_List__r.X10_YR_Warranty_Kit__c,  Machine_Version_Parts_List__r.X10_YR_Warranty_Kit_2__c,
                       Machine_Version_Parts_List__r.Maintenance_Kit_1__c,Machine_Version_Parts_List__r.Maintenance_Kit_2__c,Machine_Version_Parts_List__r.Maintenance_Kit_3__c,
                     (SELECT Id, Name, CreatedDate, LastModifiedDate, Hours_Lubricant__c, Date_of_Service__c, Hours_at_Service__c, Hours_of_Last_Service__c, I_Used_a_Kit__c, Lubricant_Used__c, Machine_Location__c, Material_Description__c, Air_Filter_Number__c, Oil_Sample_Taken__c, Owner_Address__c, Owner_Name__c, Air_Filter_Qty__c, Service_Notes__c, Warranty_Type__c, Air_Filter_Compliance__c, Air_Filter_Installed__c, Air_Filter_Left_For_Inventory__c, Air_Filter_Next_Expected_Date__c, Air_Filter__c, Cabinet_Filter_Compliance__c, Cabinet_Filter_Installed__c, Cabinet_Filter_Left_For_Inventory__c, Cabinet_Filter_Next_Expected_Date__c, Cabinet_Filter__c, Completed_Oil_Change__c, Compliance_Exception_Air_Filter__c, Compliance_Exception_Cabinet_Filter__c, Compliance_Exception_Control_Box__c, Compliance_Exception_Lubricant__c, Compliance_Exception_Oil_Filter__c, Compliance_Exception_Separator__c, Compliant_Air_Filter__c, Compliant_Cabinet_Filter__c, Compliant_Control_Box_Filter__c, Compliant_Lubricant__c, Compliant_Oil_Filter__c, Compliant_Separator__c, Compliant_Time__c, Control_Box_Filter_Compliance__c, Control_Box_Filter_Installed__c, Control_Box_Filter_Left_for_Inventory__c, Control_Box_Filter_Next_Expected_Date__c, Control_Box_Filter__c, Current_Hours__c, Currently_Under_Warranty__c, Customer_Feedback__c, Error_Overrides__c, Expected_Next_Service_Date__c, Gallons_of_Lubricant__c, Hours_Air_Filter__c, Hours_Cabinet_Filter__c, Hours_Control_Box_Filter__c, Hours_Loaded__c, Hours_Oil_Filter__c, Hours_Separator__c, Hours_Unloaded__c, Inventory_Air_Filter__c, Inventory_Cabinet_Filter__c, Inventory_Control_Box_Filter__c, Inventory_Lubricant__c, Inventory_Oil_Filter__c, Inventory_Separator__c, Last_Logged_Service_Visit__c, Lubricant_Compliance__c, Lubricant_Filter_Installed__c, Lubricant_Filter_Left_For_Inventory__c, Lubricant_Next_Expected_Date__c, Lubricant_Sample_Next_Expected_Date__c, Lubricant__c, Maintenance_Kit__c, Oil_Filter_Compliance__c, Oil_Filter_Next_Expected_Date__c, Oil_Filter__c, Other_Lubricant_Type__c, Other_Parts__c, Part_Tracking_Submissions__c, Previous_Hours__c, Repair__c, Separator_Compliance__c, Separator_Installed__c, Separator_Left_For_Inventory__c, Separator_Next_Expected_Date__c, Separator__c, Technician_Email__c, Warranty_Compliance_Level__c, X10_Yr_Warranty_Kit__c, X12_Mo_Visit__c, Cabinet_Filter_Qty__c, Control_Box_Filter_Number__c, Control_Box_Filter_Qty__c, Lubricant_Filter_Number__c, Lubricant_Filter_Qty__c, Lubricant_Part_Number__c, Separator_Number__c, Separator_Qty__c, Part_Validity__c, Record_Creation_Method__c, Original_Servicer__c, Original_Tech__c, Type_of_Service__c, Cabinet_Filter_Number__c FROM Service_History_del__r), 
                   (SELECT Id, Name, CreatedDate, LastModifiedDate, Alert_ID__c, Alert_Message__c, IMEI__c, Serial_Number__c, Servicer__c, Severity__c,Serial_Number_Cleared_Alert__c, Serial_Number_Model__c, Serial_Number_Name__c, Source_ID__c, Status__c, Machine_Total_Run_Hours__c, Machine_Load_Hours__c, Customer_Name__c, Serial_Number_Model_Number__c FROM Alerts__r), 
                   (SELECT Address__c, Aluminum__c, Barium__c, Calcium__c, Chromium__c, Collect_Date__c, Collecting_Distributor__c, Comment__c, Copper__c, Customer__c, Date_Range__c, Distributor__c, Hours_On_Machine__c, ISO_Code__c, Iron__c, Lead__c, Lubricant_Type__c, Magnesium__c, Molybdenum__c, Nickel__c, Oil_Hours__c, Phosphorus__c, Receive_Date__c, Report_Date__c, Report_Number__c, Serial_Number_Lookup__c, Serial_Number_Text__c, Service_History__c, Silicon__c, Silver__c, Sodium__c, Status__c, Tin__c, Titanium__c, Total_Acid_Number__c, Vanadium__c, Viscosity__c, Water_by_Karl_Fischer__c, Zinc__c, Account_Parent_ID__c, End_Customer_Account__c, Sample_QR_Code__c, Original_Servicer__c, CreatedDate, LastModifiedDate, Id, Name FROM Oil_Samples__r)
                   FROM Asset WHERE Name = :serialNum and (DChl__c ='CM' OR DChl__c ='CP') and Inactive__c != TRUE and (NOT MPG4_Code__r.Level_1__c like '%Parts%') and (NOT MPG4_Code__r.Level_1__c like '%Bare%') and (NOT MPG4_Code__r.Level_1__c like '%Reman%') limit 1];

            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(a));
        } catch (Exception e) {
           
            JSONGenerator gen = JSON.createGenerator(false);
            gen.writeStartObject();
            gen.writeNumberField('statusCode', 404);
            gen.writeStringField('error', 'There was an error with the request.');
            gen.writeEndObject();
            RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
            
        }
    }

}