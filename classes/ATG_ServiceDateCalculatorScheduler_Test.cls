@isTest
public class ATG_ServiceDateCalculatorScheduler_Test {
    @isTest public static void testScheduler() {
        Test.startTest();
        ATG_ServiceDateCalculatorScheduler scheduler = new ATG_ServiceDateCalculatorScheduler();
        String cronExp = '0 0 0 1 1 ? 2099';
        Id jobId = System.schedule('Test Scheduler', cronExp, scheduler);        
        
        CronTrigger cronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId]; 
        System.assertEquals(0, cronTrigger.TimesTriggered, 'We expect our job to have been scheduled and not yet triggered');
        Test.stopTest();
    }
}