/*
* Test cases for price book utility class
*/
@isTest
public class ensxtx_TSTU_Pricebook
{
    @isTest
    public static void test_getStandardPricebook()
    {
        Test.startTest();
        Id pb = ensxtx_UTIL_Pricebook.getStandardPriceBookId();
        Test.stopTest();
    }

    @isTest
    public static void test_getEntriesForPricebook()
    {
        Test.startTest();
        Account testAcct = ensxtx_TSTU_SFTestObject.createTestAccount();
        ensxtx_TSTU_SFTestObject.upsertWithRetry(testAcct);
        ensxtx_UTIL_Pricebook.getEntriesForPricebookById(testAcct.Id, null);
        ensxtx_UTIL_Pricebook.getEntriesForPricebookById(null, new Set<String>{'Test'});
        ensxtx_UTIL_Pricebook.loadResult(new List<PriceBookEntry>{new PriceBookEntry()}, new Map<Id, PriceBookEntry>());
        Test.stopTest();
    }
}