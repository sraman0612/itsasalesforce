@isTest
public class ensxtx_TSTU_Customer
{
    public class MOC_Customer implements ensxsdk.EnosixFramework.DetailSBOGetMock, ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        private boolean throwException = false;
    	public Boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            ensxtx_SBO_EnosixCustomer_Detail.EnosixCustomer result = new ensxtx_SBO_EnosixCustomer_Detail.EnosixCustomer();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            ensxtx_SBO_EnosixCustomer_Detail.EnosixCustomer result = (ensxtx_SBO_EnosixCustomer_Detail.EnosixCustomer) obj;
            result.setSuccess(success);
            if (throwException)
            {
                throw new CalloutException();
            }
            return result;
        }
    }

    @isTest
    static void test_TouchProps()
    {
        Test.startTest();
        String soldTo = ensxtx_UTIL_Customer.SOLD_TO_PARTNER_CODE;
        String shipTo = ensxtx_UTIL_Customer.SHIP_TO_PARTNER_CODE;
        String carrier = ensxtx_UTIL_Customer.CARRIER_PARTNER_CODE;
        String billTo = ensxtx_UTIL_Customer.BILL_TO_PARTNER_CODE;
        Test.stopTest();
    }

    @isTest
    static void test_getCustomerByNumber()
    {
        MOC_Customer mockCustomer = new MOC_Customer();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixCustomer_Detail.class, mockCustomer);

        Test.startTest();
        ensxtx_SBO_EnosixCustomer_Detail.EnosixCustomer result = ensxtx_UTIL_Customer.getCustomerByNumber('Id');
        mockCustomer.setSuccess(false);
        result = ensxtx_UTIL_Customer.getCustomerByNumber('Id');
        Test.stopTest();
    }
}