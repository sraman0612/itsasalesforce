/// enosiX Inc. Generated Apex Model
/// Generated On: 1/15/2021 4:32:05 PM
/// SAP Host: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixServiceNotificationSync_Search extends ensxsdk.EnosixFramework.SearchSBO
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixServiceNotificationSync_Search_Meta', new Type[] {
            SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC.class
            , SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SR.class
            , SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT.class
            , SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS.class
            , SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE.class
            , SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT.class
            }
        );
    }

    public SBO_EnosixServiceNotificationSync_Search()
    {
        super('EnosixServiceNotificationSync', SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC.class, SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SR.class);
    }

    public override Type getType() { return SBO_EnosixServiceNotificationSync_Search.class; }

    public EnosixServiceNotificationSync_SC search(EnosixServiceNotificationSync_SC sc)
    {
        return (EnosixServiceNotificationSync_SC)super.executeSearch(sc);
    }

    public EnosixServiceNotificationSync_SC initialize(EnosixServiceNotificationSync_SC sc)
    {
        return (EnosixServiceNotificationSync_SC)super.executeInitialize(sc);
    }

    public class EnosixServiceNotificationSync_SC extends ensxsdk.EnosixFramework.SearchContext
    {
        public EnosixServiceNotificationSync_SC()
        {
            super(new Map<string,type>
                {
                    'SEARCHPARAMS' => SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS.class
                    ,'NOTIF_TYPE' => SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE_COLLECTION.class
                });
        }

        public override Type getType() { return SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixServiceNotificationSync_Search.registerReflectionInfo();
        }

        public EnosixServiceNotificationSync_SR result { get { return (EnosixServiceNotificationSync_SR)baseResult; } }


        @AuraEnabled public SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS SEARCHPARAMS
        {
            get
            {
                return (SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS)this.getStruct(SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS.class);
            }
        }

            @AuraEnabled public NOTIF_TYPE_COLLECTION NOTIF_TYPE
        {
            get
            {
                return (NOTIF_TYPE_COLLECTION)this.getCollection(SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE_COLLECTION.class);
            }
        }

    }

    public class EnosixServiceNotificationSync_SR extends ensxsdk.EnosixFramework.SearchResult
    {
        public EnosixServiceNotificationSync_SR()
        {
            super(new Map<string,type>{'SEARCHRESULT' => SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT.class } );
        }

        public ensxsdk.EnosixFramework.FrameworkCollection SearchResults
        {
            get { return super.getCollection(SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT.class); }
        }

        public List<SEARCHRESULT> getResults()
        {
            List<SEARCHRESULT> results = new List<SEARCHRESULT>();
            SearchResults.copyTo(results);
            return results;
        }

        public override Type getType() { return SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SR.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixServiceNotificationSync_Search.registerReflectionInfo();
        }
    }

    public class SEARCHPARAMS extends ensxsdk.EnosixFramework.ValueObject
    {
        public override Type getType() { return SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixServiceNotificationSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public Date DateFrom
        {
            get { return this.getDate ('DATE_FROM'); }
            set { this.Set (value, 'DATE_FROM'); }
        }

    }

    public class NOTIF_TYPE extends ensxsdk.EnosixFramework.ValueObject
    {
        public override Type getType() { return SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixServiceNotificationSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public String NotificationType
        {
            get { return this.getString ('QMART'); }
            set { this.Set (value, 'QMART'); }
        }

    }

    public class NOTIF_TYPE_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public NOTIF_TYPE_COLLECTION()
        {
            super('NOTIF_TYPE', SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE.class, null);
        }

        public List<SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE> getAsList()
        {
            return (List<SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE>)this.buildList(List<SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE>.class);
        }
    }

    public class SEARCHRESULT extends ensxsdk.EnosixFramework.ValueObject
    {
        public override Type getType() { return SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixServiceNotificationSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public String NotificationNumber
        {
            get { return this.getString ('QMNUM'); }
            set { this.Set (value, 'QMNUM'); }
        }

        @AuraEnabled public String NotificationType
        {
            get { return this.getString ('QMART'); }
            set { this.Set (value, 'QMART'); }
        }

        @AuraEnabled public String NotificationTypeText
        {
            get { return this.getString ('QMARTX'); }
            set { this.Set (value, 'QMARTX'); }
        }

        @AuraEnabled public String ItemRecordNumber
        {
            get { return this.getString ('FENUM'); }
            set { this.Set (value, 'FENUM'); }
        }

        @AuraEnabled public Date DispositionDate
        {
            get { return this.getDate ('ZZCLOSEDT'); }
            set { this.Set (value, 'ZZCLOSEDT'); }
        }

        @AuraEnabled public String WarrantyCode
        {
            get { return this.getString ('ZZWARRCODE'); }
            set { this.Set (value, 'ZZWARRCODE'); }
        }

        @AuraEnabled public String WarrantyCodeDescription
        {
            get { return this.getString ('ZZWARRCODE_TEXT'); }
            set { this.Set (value, 'ZZWARRCODE_TEXT'); }
        }

        @AuraEnabled public String SalesOrderNumber
        {
            get { return this.getString ('VBELN'); }
            set { this.Set (value, 'VBELN'); }
        }

        @AuraEnabled public Date DateReturnRequested
        { 
            get { return this.getDate ('RTDAT'); } 
            set { this.Set (value, 'RTDAT'); }
        }

        @AuraEnabled public Date DateMaterialReceived
        { 
            get { return this.getDate ('MRDAT'); } 
            set { this.Set (value, 'MRDAT'); }
        }

        @AuraEnabled public Date DateInspected
        { 
            get { return this.getDate ('INDAT'); } 
            set { this.Set (value, 'INDAT'); }
        }

        @AuraEnabled public String CreditMemo
        { 
            get { return this.getString ('CRMEM'); } 
            set { this.Set (value, 'CRMEM'); }
        }

        @AuraEnabled public Date CreditMemoDate
        { 
            get { return this.getDate ('CRDAT'); } 
            set { this.Set (value, 'CRDAT'); }
        }

        @AuraEnabled public Decimal CreditMemoAmount
        { 
            get { return this.getDecimal ('NETWR'); } 
            set { this.Set (value, 'NETWR'); }
        }

    }

    public class SEARCHRESULT_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public SEARCHRESULT_COLLECTION()
        {
            super('SEARCHRESULT', SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT.class, null);
        }

        public List<SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT> getAsList()
        {
            return (List<SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT>)this.buildList(List<SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT>.class);
        }
    }

}