public with sharing class CaseNewFromContact {

    @AuraEnabled
    public static Contact getContact(Id contactId){
        //make your SOQL here from your desired object where you want to place your lightning action
        //return [SELECT Id, AccountId, (SELECT Brand__c, Department__c, Default_Product_Category__c FROM Users) FROM Contact WHERE Id = :contactId];
        return [SELECT Id, AccountId FROM Contact WHERE Id = :contactId];
    }

    @AuraEnabled
    public static User getUserData(){
        //make your SOQL here from your desired object where you want to place your lightning action
        Id userId = UserInfo.getUserId();
        return [SELECT Id, Brand__c, Department__c, Default_Product_Category__c FROM User WHERE Id = :userId];
    }

}