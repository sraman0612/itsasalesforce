@isTest
public class C_DelServiceHistoryBatchTest {
    @isTest static void test1()
    {
        string profileid = [select id from profile where name='Partner Community User'].id;
        string roleid =[Select id from UserRole where name='SFDC Consultants'].id;
        
        List<contact> cList = new List<Contact>();
        Account ap1 = new account(name='test3');
        insert ap1;
        Account ap2 = new account(name='test4');
        insert ap2;
        Account a1 = new account(name='test1', ParentId = ap2.id,Floor_Plan_Account__c = ap1.id);
        insert a1;
        Account a = new account(name='test',ParentId = ap1.id, Floor_Plan_Account__c=a1.id);
        insert a;
        
        contact con = new contact(firstname='j', lastname='b', email='jbtest1@test.com', accountid =a.id);
        //insert con;
        clist.add(con);
        
        contact con1 = new contact(firstname='j2', lastname='b2', email='jbtest2@test.com', accountid =a1.id);
        //insert con1;
        clist.add(con1);
        
        contact con2 = new contact(firstname='j2', lastname='b2', email='jbtest3@test.com', accountid =a1.id);
        //insert con2;
        clist.add(con2);
        
        contact con3 = new contact(firstname='j2', lastname='b2', email='jbtest4@test.com', accountid =a1.id);
        //insert con3;
        clist.add(con3);
        insert cList;
        List<user> uUpdt = new List<user>();
        User u = new User(
            ProfileId = profileid,
            Firstname='First',
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con.Id,
            LocaleSidKey = 'en_US',
            community_user_type__c='Single Account'
        );
        User u2 = new User(
            ProfileId = profileid,
            Firstname='First1',
            LastName = 'last1',
            Email = 'puser001@amamama.com',
            Username = 'puser001@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con1.Id,
            LocaleSidKey = 'en_US',
            community_user_type__c='Single Account'
        );
        User u3 = new User(
            ProfileId = profileid,
            Firstname='First2',
            LastName = 'last2',
            Email = 'puser002@amamama.com',
            Username = 'puser002@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con2.Id,
            LocaleSidKey = 'en_US',
            community_user_type__c='Single Account'
        );
        User u4 = new User(
            ProfileId = profileid,
            Firstname='First3',
            LastName = 'last3',
            Email = 'puser003@amamama.com',
            Username = 'puser003@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con3.Id,
            LocaleSidKey = 'en_US',
            community_user_type__c='Single Account'
        );
        User u5 = new User(
            ProfileId = profileid,
            Firstname='First4',
            LastName = 'last4',
            Email = 'puser004@amamama.com',
            Username = 'puser004@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con.Id,
            LocaleSidKey = 'en_US',
            community_user_type__c='Single Account'
        );
        uUpdt.add(u); 
        uUpdt.add(u2);
        uUpdt.add(u3);
        uUpdt.add(u4);
        //uUpdt.add(u5);
         insert uUpdt;
        
       // System.runAs(u){
        Asset serial = new Asset();
        serial.SerialNumber = '1234567';
        serial.Name = 'test';
        serial.AccountId=a.id;
        serial.current_servicer__c = a1.id;
        insert serial;
        
        Service_History__c sh = new Service_History__c();
        sh.Serial_Number__c = serial.id;
        sh.Technician_Email__c ='jbtest1@test.com';
        sh.account__c = a.id;
        insert sh; 
        
        List<string> shareId = new List<string>();
        List<string> shareId2 = new List<string>();
        List<string> shareId3 = new List<string>();
        //Service_History__c sh = [Select id from Service_History__c limit 1];
        
        Service_History__Share share = new Service_History__Share();
        share.AccessLevel='Edit';
        share.ParentId=sh.id;
        share.RowCause='Manual';
        share.UserOrGroupId=u.id;
        insert share;
        Service_History__Share share2 = new Service_History__Share();
        share2.AccessLevel='Edit';
        share2.ParentId=sh.id;
        share2.RowCause='Manual';
        share2.UserOrGroupId=u2.id;
        insert share2;
        Service_History__Share share3 = new Service_History__Share();
        share3.AccessLevel='Edit';
        share3.ParentId=sh.id;
        share3.RowCause='Manual';
        share3.UserOrGroupId=u3.id;
        insert share3;
        Service_History__Share share4 = new Service_History__Share();
        share4.AccessLevel='Edit';
        share4.ParentId=sh.id;
        share4.RowCause='Manual';
        share4.UserOrGroupId=u4.id;
        insert share4;
        shareId.add(string.valueOf(share.id)); 
        shareId2.add(string.valueOf(share3.id));  
        shareId3.add(''); 
        
        Test.startTest();
        //C_DelServiceHistoryBatch sd = new C_DelServiceHistoryBatch(shareId,shareId2,shareId3);
        //Database.executeBatch(sd);
        
        shareId.add(string.valueOf(share2.id)); 
        shareId2.add(string.valueOf(share4.id)); 
        shareId3.add(''); 
        C_DelServiceHistoryBatch sd = new C_DelServiceHistoryBatch(shareId,shareId2,shareId3);
        Database.executeBatch(sd);
        Test.stopTest();
        //}
    }
}