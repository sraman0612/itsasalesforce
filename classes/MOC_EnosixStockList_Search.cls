@isTest
public class MOC_EnosixStockList_Search
{
	public class MockEnosixStockListSuccess implements ensxsdk.EnosixFramework.SearchSBOSearchMock
	{
		public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext) 
		{
			SBO_EnosixStockList_Search.EnosixStockList_SR search_result = 
				new SBO_EnosixStockList_Search.EnosixStockList_SR();
            
            SBO_EnosixStockList_Search.SEARCHRESULT result = 
				new SBO_EnosixStockList_Search.SEARCHRESULT();
            
			result.Material = 'X';
			result.Plant = 'X';
			result.ReceiptRequirementDate = Date.valueOf('2020-12-31');
			result.MRPElement = 'X';
			result.MRPElementData = 'X';
			result.ReschedulingDate = Date.valueOf('2020-12-31');
			result.QtyReceivedReserved = 1.5;
			result.AvailableQty = 1.5;
			result.AvailableQtyAfterAssignment = 1.5;
			result.StorageLocation = 'X';
			
			search_result.SearchResults.add(result);
			
			search_result.setSuccess(true);
			searchContext.baseResult = search_result;		
			return searchContext;
        }
	}

	public class MockEnosixStockListFailure implements ensxsdk.EnosixFramework.SearchSBOSearchMock
	{
		public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext) 
		{
            SBO_EnosixStockList_Search.EnosixStockList_SR search_result = 
				new SBO_EnosixStockList_Search.EnosixStockList_SR();
            
            SBO_EnosixStockList_Search.SEARCHRESULT result = 
				new SBO_EnosixStockList_Search.SEARCHRESULT();
            
			result.Material = 'X';
			result.Plant = 'X';
			result.ReceiptRequirementDate = Date.valueOf('2020-12-31');
			result.MRPElement = 'X';
			result.MRPElementData = 'X';
			result.ReschedulingDate = Date.valueOf('2020-12-31');
			result.QtyReceivedReserved = 1.5;
			result.AvailableQty = 1.5;
			result.AvailableQtyAfterAssignment = 1.5;
			result.StorageLocation = 'X';
			
			search_result.SearchResults.add(result);
			
			search_result.setSuccess(false); 
			searchContext.baseResult = search_result;			
			return searchContext;
        }
	}
}