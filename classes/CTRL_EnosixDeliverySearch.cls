public with sharing class CTRL_EnosixDeliverySearch
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_ContactList.class);

   @AuraEnabled
    public static UTIL_Aura.Response doSearch(String deliveryNumber) {
        if (String.isEmpty(deliveryNumber)) {
            UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, 'Invalid Delivery Inquiry');
            return UTIL_Aura.createResponse(null);
        }

        logger.enterAura('doSearch', new Map<String, Object> {
            'deliveryNumber' => deliveryNumber
        });

        SBO_SFCIDelivery_Detail sbo = new SBO_SFCIDelivery_Detail();
        SBO_SFCIDelivery_Detail.SFCIDelivery result = new SBO_SFCIDelivery_Detail.SFCIDelivery();
        List<SBO_SFCIDelivery_Detail.PARTNERS> partners = new List<SBO_SFCIDelivery_Detail.PARTNERS>();
        DELIVERY_SPECIFIC_INFO retVal = new DELIVERY_SPECIFIC_INFO();

        try {
            result = sbo.getDetail(deliveryNumber);
            partners = result.PARTNERS.getAsList();
            retVal.Delivery = result.Delivery;
            retVal.DeliveryDate = result.DeliveryDate;
            retVal.DeliveryStatus = result.DeliveryStatus;
            retVal.TrackingNumber = result.BillofLading;

            for (SBO_SFCIDelivery_Detail.PARTNERS ptnr : partners) {
                //system.debug('Function, FunctionName, Vendor and Name ====> '+ptnr.PartnerFunction+', '+ptnr.PartnerFunctionName+', '+ptnr.Vendor+' and '+ptnr.PartnerName);
                if (ptnr.PartnerFunction == 'CR' && ptnr.PartnerFunctionName == 'Carrier') {
                    retVal.Vendor = ptnr.Vendor;
                    retVal.VendorName = ptnr.PartnerName;
                }
            }

            Map<String,GDI_Delivery_Vendors__c> urlMap = new Map<String,GDI_Delivery_Vendors__c>();
            for (GDI_Delivery_Vendors__c vend : [SELECT FLD_Tracking_URL__c,FLD_Tracking_URL_Label__c,FLD_Vendor_Code__c,
                                                FLD_Vendor_Description__c,FLD_Vendor_Num__c,Id,Name FROM GDI_Delivery_Vendors__c]) {
                if (vend.FLD_Vendor_Num__c != null) urlMap.put(vend.FLD_Vendor_Num__c,vend);
            }

            if (urlMap.containsKey(retVal.Vendor)) {
                retVal.TrackingURL = urlMap.get(retVal.Vendor).FLD_Tracking_URL__c + retVal.TrackingNumber;
                retVal.URL_Label = urlMap.get(retVal.Vendor).FLD_Tracking_URL_Label__c;
            }

        } catch (Exception e) {
            UTIL_PageMessages.addExceptionMessage(e);
        } finally {
            logger.exit();
        }

        return UTIL_Aura.createResponse(JSON.serialize(retVal));
    }

    public class DELIVERY_SPECIFIC_INFO {
        public String Delivery {get;set;}
        public Date DeliveryDate {get;set;}
        public String DeliveryStatus {get;set;}
        public String Vendor {get;set;}
        public String VendorName {get;set;}
        public String TrackingNumber {get;set;}
        public String TrackingURL {get;set;}
        public String URL_Label {get;set;}
    }

}