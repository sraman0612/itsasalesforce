public class versionQuoteController{
    public List<cafsl__Oracle_Quote__c> lstorfscl{get;set;}
    public static List<cafsl__Oracle_Quote__c> lstOracleQuote {get;set;}

    @TestVisible
    public versionQuoteController (ApexPages.StandardSetController stdSetController) {
        System.debug('stdSetController :'+stdSetController);
        this.lstorfscl = (List<cafsl__Oracle_Quote__c>)stdSetController.getSelected();
        System.debug('this.lstorfscl :'+this.lstorfscl);
        if(this.lstorfscl.size() > 0)
        lstOracleQuote = [SELECT Id,cafsl__Opportunity__c,cafsl__Transaction_ID__c, Work_Order__c, Work_Order__r.SFS_Work_Type__c FROM cafsl__Oracle_Quote__c WHERE Id IN : this.lstorfscl ];
        System.debug('orfscl id--->'+lstOracleQuote[0].Id);

    }

    public pageReference reDirectToQuoteRecord(){
        System.debug('lstOracleQuote: '+lstOracleQuote);

        Boolean allowRedirect = true;
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();

        if(this.lstorfscl.size() == 0){
            allowRedirect = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select atleast one quote to perform Versioning.'));
        return null;}
        if(lstOracleQuote.size() == 0){
            allowRedirect = false;
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No Oracle quotes available in this work order to perform version quote.'));
        return null;}

       if(this.lstorfscl.size() > 0){
           System.debug('###@@@@');
           System.debug('###@@@@ lstOracleQuote[0].Work_Order__c: '+lstOracleQuote[0].Work_Order__c);
           List<cafsl__Oracle_Quote__c> allOQs = [SELECT Id,cafsl__Opportunity__c,cafsl__Transaction_ID__c, Work_Order__c, Work_Order__r.SFS_Work_Type__c FROM cafsl__Oracle_Quote__c where Work_Order__c =: lstOracleQuote[0].Work_Order__c];
           System.debug('### allOQs: '+ allOQs);
           if(allOQs.size()>1){
           allowRedirect = false;
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Versioning on this oracle quote is already done.'));
               return null;}
       }

        if(lstOracleQuote[0].Work_Order__r.SFS_Work_Type__c != 'Diagnostic'){
            allowRedirect = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Version Quote can be performed only for Diagnostic Work Type'));
        return null;}

        if(allowRedirect){
        system.debug('orfscl cafsl__Transaction_ID__c id--->'+lstOracleQuote[0].cafsl__Transaction_ID__c);
        string recordId = HttpMethod(lstOracleQuote[0].cafsl__Transaction_ID__c,'OracleCPQVersionQuote','CPQ Sales Version Quote');
        String targetURL = sfdcBaseURL+'/apex/cafsl__EmbeddedTransaction?mode=edit&quoteId=' + recordId;
        PageReference pg = new PageReference (targetURL);
        pg.setRedirect(true);
        //RR String xmlString, String interfaceDetail, String interfaceLabel
            return pg;
        }else{
            return null;
        }
    }

    public static string HttpMethod(String xmlString, String interfaceDetail, String interfaceLabel){
        //Http Method to send xml and recieve success or failure code.
        System.debug('### xmlString: ' + xmlString);
        System.debug('### interfaceDetail: ' + interfaceDetail);
        SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT Label,SFS_Endpoint_URL__c, SFS_Username__c, SFS_Password__c,SFS_ContentType__c
                                                      FROM SFS_SFS_Integration_Endpoint__mdt
                                                      WHERE Label =: interfaceDetail];
        System.debug('### endpoint: ' + endpoint);

        //Encoding credentials for Authorization Header
        Blob headervalue = Blob.valueOf(endpoint.SFS_Username__c + ':' + endpoint.SFS_Password__c);
        //Blob headervalue = Blob.valueOf('nithinl' + ':' + 'Mrfox^4111');
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        String endpointUrl = endpoint.Label!='CPQ Get Price'&& endpoint.Label!='CertifyTax API' ?endpoint.SFS_Endpoint_URL__c:'callout:SFS_CPQ_DevBox_Endpoint';
        //String endpointUrl = endpoint.SFS_Endpoint_URL__C;
        //Http Request
        if(endpoint.Label =='OracleCPQVersionQuote'){
            system.debug('---->Before'+endpointUrl);
            endpointUrl = endpointUrl.replace(':id', xmlString);
            system.debug('---->After'+endpointUrl);
            system.debug('Username---->'+endpoint.SFS_Username__c+'password--->'+endpoint.SFS_Password__c+'header'+ authorizationHeader + 'headerValue-->'+headervalue);
        }
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpointUrl);
        request.setMethod('POST');
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type',endpoint.SFS_ContentType__c);
        request.setTimeout(120000);

        //Get Http Response
        //If(!Test.isRunningTest()){
            system.debug('request--->'+request);
            HttpResponse response = http.send(request);
            System.debug('response: ' + response);
            System.debug('STATUS: ' + response.getStatusCode());
            //System.debug('Body: ' + JSON.serializePretty(response.getBody()));
            System.debug('endpoint.Label: ' +endpoint.Label);
            // BELOW ADDED BY PARAG
            if(endpoint.Label=='OracleCPQVersionQuote' && response.getStatusCode() == 200){
                String bsIdFromResponse =  versionQuoteController.getCPQVersionResponse(response, 'bs_id');

                //Map<String, String> sellPriceService =  new Map<String, String>( versionQuoteController.getCPQVersionResponse(response, 'sellPriceService_l'));

                System.debug('### bsIdFromResponse: '+bsIdFromResponse);
                //====
                //string endPointCleanSave = 'https://ircompanytest3.bigmachines.com/rest/v14/commerceDocumentsOraclecpqo_bmClone_1Transaction/'+bsIdFromResponse;
                /*string endPointCleanSave = System.Label.CPQ_VersionClean_Save  ;
                endPointCleanSave = endPointCleanSave.replace(':id', bsIdFromResponse);
                system.debug('---->After'+endPointCleanSave);
                Http http1 = new Http();
                HttpRequest request1 = new HttpRequest();
                request1.setEndpoint(endPointCleanSave);
                request1.setMethod('POST');
                request1.setHeader('Authorization', authorizationHeader);
                request1.setHeader('Content-Type',endpoint.SFS_ContentType__c);
                request1.setTimeout(120000);
                HttpResponse response1 = http.send(request1);

                string recordId =  versionQuoteController.getCPQVersionResponse(response1, 'oRCL_SFDC_TransactionID_t');*/
                ///====
                System.debug('bsIdFromResponse:'+bsIdFromResponse);

                List<cafsl__Oracle_Quote__c> lstOfUpdatedOracleQuote = [SELECT id,createdDate,Work_Order__c,Service_Quote_Type__c from cafsl__Oracle_Quote__c where cafsl__Transaction_ID__c = :bsIdFromResponse order by CreatedDate desc];
                System.debug('lstOfUpdatedOracleQuote: '+lstOfUpdatedOracleQuote);
                WorkOrder wo = new WorkOrder();
                    wo.Id = lstOfUpdatedOracleQuote[0].Work_Order__c;
                    update wo;
                return lstOfUpdatedOracleQuote[0].Id;
            }
        //}
        return null;
    }

    public static String getCPQVersionResponse(HttpResponse response, String responseKey){
        System.debug('response:'+JSON.serializePretty(response.getBody()));
        System.debug('responseKey'+responseKey);
        Map<String,Object> getPriceResponse = (Map<String,Object>)JSON.deserializeUntyped(response.getBody());
        //system.debug('--->getPriceResponse'+JSON.serializePretty(getPriceResponse));
        Map<String,Object> getPriceResponseDocuments = (Map<String,Object>)getPriceResponse.get('documents');
        //system.debug('--->documents     '+string.ValueOf(getPriceResponseDocuments.get(responseKey)));
//        Map<String,Object> getTransactionLine = (Map<String,Object>)getPriceResponseDocuments.get('1');
//        system.debug('--->item 2:     '+string.ValueOf(getTransactionLine));

        return string.ValueOf(getPriceResponseDocuments.get(responseKey));
    }

}