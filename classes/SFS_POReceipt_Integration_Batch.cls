global class SFS_POReceipt_Integration_Batch implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts {
    
    global List<id> selectedList;
    
    public SFS_POReceipt_Integration_Batch(List<id> selectedList) {
        this.selectedList = selectedList;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select id,SFS_External_Id__c,SFS_Oracle_Requisition_Number__c,SFS_Organization_Code__c,SFS_Operating_Unit__c from productRequest where id IN:selectedList';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<ProductRequest> scope) {
        Set<Id> prId=new Set<Id>();
        Map<String,List<ShipmentItem>> mapofSplitShipment = new Map<String,List<ShipmentItem>>();
        for(ProductRequest pr : scope){
            prId.add(pr.id);
        }
        List<ShipmentItem> shipmentItemList=[SELECT id,SFS_PRLI_External_Id__c,SFS_Delivery_ID__c,SFS_Subinventory_Type__c,Product2Id,SFS_Product_Quantity_UnitOfMeasure__c,Quantity,
                                               SFS_Part_Number__c,SFS_Product_Oracle_Requisition_Number__c, SFS_Destination_Location_Locator__c,SFS_Oracle_Requisition_Line_ID__c,SFS_Oracle_Requisition_Line_Number__c from ShipmentItem 
                                               where SFS_Product_Request__c=:prId AND Product2.SFS_Serialized__c = false AND SFS_Status__c IN ('Shipped','DELIVERED','CLOSED')];       
       
       //SIF-4903 - Functionality for Split shipment
        if(shipmentItemList.size() > 0)
        {    
            for(ShipmentItem shipmentRec : shipmentItemList){
                if(mapofSplitShipment.containsKey(shipmentRec.SFS_Delivery_ID__c)){
                    mapofSplitShipment.get(shipmentRec.SFS_Delivery_ID__c).add(shipmentRec);
                } else {
                    mapofSplitShipment.put(shipmentRec.SFS_Delivery_ID__c, new List<ShipmentItem> {shipmentRec});
                }
            }
            System.debug('mapofSplitShipment' + mapofSplitShipment);
            if(!mapofSplitShipment.keyset().isEmpty()){
                for(ProductRequest pr : scope){
                    for(String deliveryId : mapofSplitShipment.keyset()){
                        SFS_POReceipt_Integration.ProcessData(pr,mapofSplitShipment.get(deliveryId),mapofSplitShipment.keyset().size());
                    }  
                }
            } 
        }
    }         
    
    global void finish(Database.BatchableContext BC) {
    }
}