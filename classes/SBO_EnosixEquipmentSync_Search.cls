/// enosiX Inc. Generated Apex Model
/// Generated On: 8/8/2019 10:24:07 AM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixEquipmentSync_Search extends ensxsdk.EnosixFramework.SearchSBO 
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixEquipmentSync_Search_Meta', new Type[] {
            SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC.class
            , SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR.class
            , SBO_EnosixEquipmentSync_Search.SEARCHRESULT.class
            , SBO_EnosixEquipmentSync_Search.SEARCHPARAMS.class
            , SBO_EnosixEquipmentSync_Search.SEARCHRESULT.class
            } 
        );
    }

    public SBO_EnosixEquipmentSync_Search() 
    {
        super('EnosixEquipmentSync', SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC.class, SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR.class);
    }
    
    public override Type getType() { return SBO_EnosixEquipmentSync_Search.class; }

    public EnosixEquipmentSync_SC search(EnosixEquipmentSync_SC sc) 
    {
        return (EnosixEquipmentSync_SC)super.executeSearch(sc);
    }

    public EnosixEquipmentSync_SC initialize(EnosixEquipmentSync_SC sc) 
    {
        return (EnosixEquipmentSync_SC)super.executeInitialize(sc);
    }

    public class EnosixEquipmentSync_SC extends ensxsdk.EnosixFramework.SearchContext 
    { 		
        public EnosixEquipmentSync_SC() 
        {		
            super(new Map<string,type>		
                {		
                    'SEARCHPARAMS' => SBO_EnosixEquipmentSync_Search.SEARCHPARAMS.class		
                });		
        }

        public override Type getType() { return SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixEquipmentSync_Search.registerReflectionInfo();
        }

        public EnosixEquipmentSync_SR result { get { return (EnosixEquipmentSync_SR)baseResult; } }


        @AuraEnabled public SBO_EnosixEquipmentSync_Search.SEARCHPARAMS SEARCHPARAMS 
        {
            get
            {
                return (SBO_EnosixEquipmentSync_Search.SEARCHPARAMS)this.getStruct(SBO_EnosixEquipmentSync_Search.SEARCHPARAMS.class);
            }
        }
        
        }

    public class EnosixEquipmentSync_SR extends ensxsdk.EnosixFramework.SearchResult 
    {
        public EnosixEquipmentSync_SR() 
        {
            super(new Map<string,type>{'SEARCHRESULT' => SBO_EnosixEquipmentSync_Search.SEARCHRESULT.class } );
        }
        
        public ensxsdk.EnosixFramework.FrameworkCollection SearchResults
        {
            get { return super.getCollection(SBO_EnosixEquipmentSync_Search.SEARCHRESULT.class); }
        }
        
        public List<SEARCHRESULT> getResults() 
        {
            List<SEARCHRESULT> results = new List<SEARCHRESULT>();
            SearchResults.copyTo(results);
            return results;
        }

        public override Type getType() { return SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixEquipmentSync_Search.registerReflectionInfo();
        }
    }

    public class SEARCHPARAMS extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixEquipmentSync_Search.SEARCHPARAMS.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixEquipmentSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public Date DateFrom
        { 
            get { return this.getDate ('DATE_FROM'); } 
            set { this.Set (value, 'DATE_FROM'); }
        }

        @AuraEnabled public Date DateTo
        { 
            get { return this.getDate ('DATE_TO'); } 
            set { this.Set (value, 'DATE_TO'); }
        }

        @AuraEnabled public Boolean InitialLoadFlag
        { 
            get { return this.getBoolean('X_INITIALLOAD'); } 
            set { this.setBoolean(value, 'X_INITIALLOAD'); }
        }

    }

    public class SEARCHRESULT extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixEquipmentSync_Search.SEARCHRESULT.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixEquipmentSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public String EquipmentNumber
        { 
            get { return this.getString ('EQUNR'); } 
            set { this.Set (value, 'EQUNR'); }
        }

        @AuraEnabled public String EquipmentNumberDescription
        { 
            get { return this.getString ('EQKTX'); } 
            set { this.Set (value, 'EQKTX'); }
        }

        @AuraEnabled public String Name
        { 
            get { return this.getString ('NAME1'); } 
            set { this.Set (value, 'NAME1'); }
        }

        @AuraEnabled public String Street
        { 
            get { return this.getString ('STREET'); } 
            set { this.Set (value, 'STREET'); }
        }

        @AuraEnabled public String City
        { 
            get { return this.getString ('CITY1'); } 
            set { this.Set (value, 'CITY1'); }
        }

        @AuraEnabled public String Region
        { 
            get { return this.getString ('REGION'); } 
            set { this.Set (value, 'REGION'); }
        }

        @AuraEnabled public String Country
        { 
            get { return this.getString ('COUNTRY'); } 
            set { this.Set (value, 'COUNTRY'); }
        }

        @AuraEnabled public String PostalCode
        { 
            get { return this.getString ('POST_CODE1'); } 
            set { this.Set (value, 'POST_CODE1'); }
        }

        @AuraEnabled public String TelephoneNumber
        { 
            get { return this.getString ('TEL_NUMBER'); } 
            set { this.Set (value, 'TEL_NUMBER'); }
        }

        @AuraEnabled public Date TechnicalObjectStartupDate
        { 
            get { return this.getDate ('INBDT'); } 
            set { this.Set (value, 'INBDT'); }
        }

        @AuraEnabled public String SalesOrganization
        { 
            get { return this.getString ('VKORG'); } 
            set { this.Set (value, 'VKORG'); }
        }

        @AuraEnabled public String SalesOrgDescription
        { 
            get { return this.getString ('VKORG_TEXT'); } 
            set { this.Set (value, 'VKORG_TEXT'); }
        }

        @AuraEnabled public String DistributionChannel
        { 
            get { return this.getString ('VTWEG'); } 
            set { this.Set (value, 'VTWEG'); }
        }

        @AuraEnabled public String DistributionChannelText
        { 
            get { return this.getString ('VTWEG_TEXT'); } 
            set { this.Set (value, 'VTWEG_TEXT'); }
        }

        @AuraEnabled public String Division
        { 
            get { return this.getString ('SPART'); } 
            set { this.Set (value, 'SPART'); }
        }

        @AuraEnabled public String DivisionDescription
        { 
            get { return this.getString ('SPART_TEXT'); } 
            set { this.Set (value, 'SPART_TEXT'); }
        }

        @AuraEnabled public String Material
        { 
            get { return this.getString ('MATNR'); } 
            set { this.Set (value, 'MATNR'); }
        }

        @AuraEnabled public String MaterialDescription
        { 
            get { return this.getString ('MAKTX'); } 
            set { this.Set (value, 'MAKTX'); }
        }

        @AuraEnabled public String Materialgroup4
        { 
            get { return this.getString ('MVGR4'); } 
            set { this.Set (value, 'MVGR4'); }
        }

        @AuraEnabled public String ProductHierarchy
        { 
            get { return this.getString ('PRODH'); } 
            set { this.Set (value, 'PRODH'); }
        }

        @AuraEnabled public String SerialNumber
        { 
            get { return this.getString ('SERNR'); } 
            set { this.Set (value, 'SERNR'); }
        }

        @AuraEnabled public String MasterWarrantyNumber
        { 
            get { return this.getString ('MGANR'); } 
            set { this.Set (value, 'MGANR'); }
        }

        @AuraEnabled public String MasterWarrantyText
        { 
            get { return this.getString ('GAKTX'); } 
            set { this.Set (value, 'GAKTX'); }
        }

        @AuraEnabled public String StroreNo
        { 
            get { return this.getString ('STORE_NO'); } 
            set { this.Set (value, 'STORE_NO'); }
        }

        @AuraEnabled public Date DeliveryDate
        { 
            get { return this.getDate ('AULDT'); } 
            set { this.Set (value, 'AULDT'); }
        }

        @AuraEnabled public String UserStatus
        { 
            get { return this.getString ('USER_STAT'); } 
            set { this.Set (value, 'USER_STAT'); }
        }

        @AuraEnabled public String UserStatusDescription
        { 
            get { return this.getString ('USER_STAT_TEXT'); } 
            set { this.Set (value, 'USER_STAT_TEXT'); }
        }

        @AuraEnabled public String SystemStatus1
        { 
            get { return this.getString ('SYS_STAT1'); } 
            set { this.Set (value, 'SYS_STAT1'); }
        }

        @AuraEnabled public String SystemStatus1Description
        { 
            get { return this.getString ('SYS_STAT1_TEXT'); } 
            set { this.Set (value, 'SYS_STAT1_TEXT'); }
        }

        @AuraEnabled public String SystemStatus2
        { 
            get { return this.getString ('SYS_STAT2'); } 
            set { this.Set (value, 'SYS_STAT2'); }
        }

        @AuraEnabled public String SystemStatus2Description
        { 
            get { return this.getString ('SYS_STAT2_TEXT'); } 
            set { this.Set (value, 'SYS_STAT2_TEXT'); }
        }

        @AuraEnabled public String SystemStatus3
        { 
            get { return this.getString ('SYS_STAT3'); } 
            set { this.Set (value, 'SYS_STAT3'); }
        }

        @AuraEnabled public String SystemStatus3Description
        { 
            get { return this.getString ('SYS_STAT3_TEXT'); } 
            set { this.Set (value, 'SYS_STAT3_TEXT'); }
        }

        @AuraEnabled public String SystemStatus4
        { 
            get { return this.getString ('SYS_STAT4'); } 
            set { this.Set (value, 'SYS_STAT4'); }
        }

        @AuraEnabled public String SystemStatus4Description
        { 
            get { return this.getString ('SYS_STAT4_TEXT'); } 
            set { this.Set (value, 'SYS_STAT4_TEXT'); }
        }

        @AuraEnabled public String SystemStatus5
        { 
            get { return this.getString ('SYS_STAT5'); } 
            set { this.Set (value, 'SYS_STAT5'); }
        }

        @AuraEnabled public String SystemStatus5Description
        { 
            get { return this.getString ('SYS_STAT5_TEXT'); } 
            set { this.Set (value, 'SYS_STAT5_TEXT'); }
        }

        @AuraEnabled public String characteristic1
        { 
            get { return this.getString ('CHAR1'); } 
            set { this.Set (value, 'CHAR1'); }
        }

        @AuraEnabled public String characteristic1Val
        { 
            get { return this.getString ('CHAR1_VAL'); } 
            set { this.Set (value, 'CHAR1_VAL'); }
        }

        @AuraEnabled public String ValText1
        { 
            get { return this.getString ('CHAR1_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR1_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic2
        { 
            get { return this.getString ('CHAR2'); } 
            set { this.Set (value, 'CHAR2'); }
        }

        @AuraEnabled public String characteristic2Val
        { 
            get { return this.getString ('CHAR2_VAL'); } 
            set { this.Set (value, 'CHAR2_VAL'); }
        }

        @AuraEnabled public String ValText2
        { 
            get { return this.getString ('CHAR2_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR2_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic3
        { 
            get { return this.getString ('CHAR3'); } 
            set { this.Set (value, 'CHAR3'); }
        }

        @AuraEnabled public String characteristic3Val
        { 
            get { return this.getString ('CHAR3_VAL'); } 
            set { this.Set (value, 'CHAR3_VAL'); }
        }

        @AuraEnabled public String ValText3
        { 
            get { return this.getString ('CHAR3_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR3_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic4
        { 
            get { return this.getString ('CHAR4'); } 
            set { this.Set (value, 'CHAR4'); }
        }

        @AuraEnabled public String characteristic4Val
        { 
            get { return this.getString ('CHAR4_VAL'); } 
            set { this.Set (value, 'CHAR4_VAL'); }
        }

        @AuraEnabled public String ValText4
        { 
            get { return this.getString ('CHAR4_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR4_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic5
        { 
            get { return this.getString ('CHAR5'); } 
            set { this.Set (value, 'CHAR5'); }
        }

        @AuraEnabled public String characteristic5Val
        { 
            get { return this.getString ('CHAR5_VAL'); } 
            set { this.Set (value, 'CHAR5_VAL'); }
        }

        @AuraEnabled public String ValText5
        { 
            get { return this.getString ('CHAR5_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR5_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic6
        { 
            get { return this.getString ('CHAR6'); } 
            set { this.Set (value, 'CHAR6'); }
        }

        @AuraEnabled public String characteristic6Val
        { 
            get { return this.getString ('CHAR6_VAL'); } 
            set { this.Set (value, 'CHAR6_VAL'); }
        }

        @AuraEnabled public String ValText6
        { 
            get { return this.getString ('CHAR6_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR6_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic7
        { 
            get { return this.getString ('CHAR7'); } 
            set { this.Set (value, 'CHAR7'); }
        }

        @AuraEnabled public String characteristic7Val
        { 
            get { return this.getString ('CHAR7_VAL'); } 
            set { this.Set (value, 'CHAR7_VAL'); }
        }

        @AuraEnabled public String ValText7
        { 
            get { return this.getString ('CHAR7_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR7_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic8
        { 
            get { return this.getString ('CHAR8'); } 
            set { this.Set (value, 'CHAR8'); }
        }

        @AuraEnabled public String characteristic8Val
        { 
            get { return this.getString ('CHAR8_VAL'); } 
            set { this.Set (value, 'CHAR8_VAL'); }
        }

        @AuraEnabled public String ValText8
        { 
            get { return this.getString ('CHAR8_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR8_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic9
        { 
            get { return this.getString ('CHAR9'); } 
            set { this.Set (value, 'CHAR9'); }
        }

        @AuraEnabled public String characteristic9Val
        { 
            get { return this.getString ('CHAR9_VAL'); } 
            set { this.Set (value, 'CHAR9_VAL'); }
        }

        @AuraEnabled public String ValText9
        { 
            get { return this.getString ('CHAR9_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR9_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic10
        { 
            get { return this.getString ('CHAR10'); } 
            set { this.Set (value, 'CHAR10'); }
        }

        @AuraEnabled public String characteristic10Val
        { 
            get { return this.getString ('CHAR10_VAL'); } 
            set { this.Set (value, 'CHAR10_VAL'); }
        }

        @AuraEnabled public String ValText10
        { 
            get { return this.getString ('CHAR10_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR10_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic11
        { 
            get { return this.getString ('CHAR11'); } 
            set { this.Set (value, 'CHAR11'); }
        }

        @AuraEnabled public String characteristic11Val
        { 
            get { return this.getString ('CHAR11_VAL'); } 
            set { this.Set (value, 'CHAR11_VAL'); }
        }

        @AuraEnabled public String ValText11
        { 
            get { return this.getString ('CHAR11_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR11_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic12
        { 
            get { return this.getString ('CHAR12'); } 
            set { this.Set (value, 'CHAR12'); }
        }

        @AuraEnabled public String characteristic12Val
        { 
            get { return this.getString ('CHAR12_VAL'); } 
            set { this.Set (value, 'CHAR12_VAL'); }
        }

        @AuraEnabled public String ValText12
        { 
            get { return this.getString ('CHAR12_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR12_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic13
        { 
            get { return this.getString ('CHAR13'); } 
            set { this.Set (value, 'CHAR13'); }
        }

        @AuraEnabled public String characteristic13Val
        { 
            get { return this.getString ('CHAR13_VAL'); } 
            set { this.Set (value, 'CHAR13_VAL'); }
        }

        @AuraEnabled public String ValText13
        { 
            get { return this.getString ('CHAR13_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR13_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic14
        { 
            get { return this.getString ('CHAR14'); } 
            set { this.Set (value, 'CHAR14'); }
        }

        @AuraEnabled public String characteristic14Val
        { 
            get { return this.getString ('CHAR14_VAL'); } 
            set { this.Set (value, 'CHAR14_VAL'); }
        }

        @AuraEnabled public String ValText14
        { 
            get { return this.getString ('CHAR14_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR14_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic15
        { 
            get { return this.getString ('CHAR15'); } 
            set { this.Set (value, 'CHAR15'); }
        }

        @AuraEnabled public String characteristic15Val
        { 
            get { return this.getString ('CHAR15_VAL'); } 
            set { this.Set (value, 'CHAR15_VAL'); }
        }

        @AuraEnabled public String ValText15
        { 
            get { return this.getString ('CHAR15_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR15_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic16
        { 
            get { return this.getString ('CHAR16'); } 
            set { this.Set (value, 'CHAR16'); }
        }

        @AuraEnabled public String characteristic16Val
        { 
            get { return this.getString ('CHAR16_VAL'); } 
            set { this.Set (value, 'CHAR16_VAL'); }
        }

        @AuraEnabled public String ValText16
        { 
            get { return this.getString ('CHAR16_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR16_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic17
        { 
            get { return this.getString ('CHAR17'); } 
            set { this.Set (value, 'CHAR17'); }
        }

        @AuraEnabled public String characteristic17Val
        { 
            get { return this.getString ('CHAR17_VAL'); } 
            set { this.Set (value, 'CHAR17_VAL'); }
        }

        @AuraEnabled public String ValText17
        { 
            get { return this.getString ('CHAR17_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR17_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic18
        { 
            get { return this.getString ('CHAR18'); } 
            set { this.Set (value, 'CHAR18'); }
        }

        @AuraEnabled public String characteristic18Val
        { 
            get { return this.getString ('CHAR18_VAL'); } 
            set { this.Set (value, 'CHAR18_VAL'); }
        }

        @AuraEnabled public String ValText18
        { 
            get { return this.getString ('CHAR18_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR18_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic19
        { 
            get { return this.getString ('CHAR19'); } 
            set { this.Set (value, 'CHAR19'); }
        }

        @AuraEnabled public String characteristic19Val
        { 
            get { return this.getString ('CHAR19_VAL'); } 
            set { this.Set (value, 'CHAR19_VAL'); }
        }

        @AuraEnabled public String ValText19
        { 
            get { return this.getString ('CHAR19_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR19_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic20
        { 
            get { return this.getString ('CHAR20'); } 
            set { this.Set (value, 'CHAR20'); }
        }

        @AuraEnabled public String characteristic20Val
        { 
            get { return this.getString ('CHAR20_VAL'); } 
            set { this.Set (value, 'CHAR20_VAL'); }
        }

        @AuraEnabled public String ValText20
        { 
            get { return this.getString ('CHAR20_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR20_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic21
        { 
            get { return this.getString ('CHAR21'); } 
            set { this.Set (value, 'CHAR21'); }
        }

        @AuraEnabled public String characteristic21Val
        { 
            get { return this.getString ('CHAR21_VAL'); } 
            set { this.Set (value, 'CHAR21_VAL'); }
        }

        @AuraEnabled public String ValText21
        { 
            get { return this.getString ('CHAR21_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR21_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic22
        { 
            get { return this.getString ('CHAR22'); } 
            set { this.Set (value, 'CHAR22'); }
        }

        @AuraEnabled public String characteristic22Val
        { 
            get { return this.getString ('CHAR22_VAL'); } 
            set { this.Set (value, 'CHAR22_VAL'); }
        }

        @AuraEnabled public String ValText22
        { 
            get { return this.getString ('CHAR22_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR22_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic23
        { 
            get { return this.getString ('CHAR23'); } 
            set { this.Set (value, 'CHAR23'); }
        }

        @AuraEnabled public String characteristic23Val
        { 
            get { return this.getString ('CHAR23_VAL'); } 
            set { this.Set (value, 'CHAR23_VAL'); }
        }

        @AuraEnabled public String ValText23
        { 
            get { return this.getString ('CHAR23_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR23_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic24
        { 
            get { return this.getString ('CHAR24'); } 
            set { this.Set (value, 'CHAR24'); }
        }

        @AuraEnabled public String characteristic24Val
        { 
            get { return this.getString ('CHAR24_VAL'); } 
            set { this.Set (value, 'CHAR24_VAL'); }
        }

        @AuraEnabled public String ValText24
        { 
            get { return this.getString ('CHAR24_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR24_VAL_TEXT'); }
        }

        @AuraEnabled public String characteristic25
        { 
            get { return this.getString ('CHAR25'); } 
            set { this.Set (value, 'CHAR25'); }
        }

        @AuraEnabled public String characteristic25Val
        { 
            get { return this.getString ('CHAR25_VAL'); } 
            set { this.Set (value, 'CHAR25_VAL'); }
        }

        @AuraEnabled public String ValText25
        { 
            get { return this.getString ('CHAR25_VAL_TEXT'); } 
            set { this.Set (value, 'CHAR25_VAL_TEXT'); }
        }

        @AuraEnabled public String PartnerFunction
        { 
            get { return this.getString ('PARVW'); } 
            set { this.Set (value, 'PARVW'); }
        }

        @AuraEnabled public String PartnerFunctionDescription
        { 
            get { return this.getString ('PARVW_TEXT'); } 
            set { this.Set (value, 'PARVW_TEXT'); }
        }

        @AuraEnabled public String SoldToPartnerNumber
        { 
            get { return this.getString ('SOLD_TO'); } 
            set { this.Set (value, 'SOLD_TO'); }
        }

        @AuraEnabled public String SoldToName
        { 
            get { return this.getString ('SOLD_TO_NAME'); } 
            set { this.Set (value, 'SOLD_TO_NAME'); }
        }

        @AuraEnabled public String MODEL_NUMBER_C
        { 
            get { return this.getString ('MODEL_NUMBER_C'); } 
            set { this.Set (value, 'MODEL_NUMBER_C'); }
        }

        @AuraEnabled public String MODEL_C
        { 
            get { return this.getString ('MODEL_C'); } 
            set { this.Set (value, 'MODEL_C'); }
        }

        @AuraEnabled public String SERNR_MODNR
        { 
            get { return this.getString ('SERNR_MODNR'); } 
            set { this.Set (value, 'SERNR_MODNR'); }
        }

        @AuraEnabled public Date DateChanged
        { 
            get { return this.getDate ('AEDAT'); } 
            set { this.Set (value, 'AEDAT'); }
        }

        @AuraEnabled public String IMEISerialNumber
        { 
            get { return this.getString ('IMEI_SERNR'); } 
            set { this.Set (value, 'IMEI_SERNR'); }
        }

        @AuraEnabled public String SalesOrderNumber
        { 
            get { return this.getString ('SALES_ORD'); } 
            set { this.Set (value, 'SALES_ORD'); }
        }

    }

    public class SEARCHRESULT_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public SEARCHRESULT_COLLECTION()
        {
            super('SEARCHRESULT', SBO_EnosixEquipmentSync_Search.SEARCHRESULT.class, null);
        }

        public List<SBO_EnosixEquipmentSync_Search.SEARCHRESULT> getAsList()
        {
            return (List<SBO_EnosixEquipmentSync_Search.SEARCHRESULT>)this.buildList(List<SBO_EnosixEquipmentSync_Search.SEARCHRESULT>.class);
        }
    }


}