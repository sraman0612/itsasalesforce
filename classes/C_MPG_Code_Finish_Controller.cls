//Allows for the 'Finish' button in the 'MPG Code Picker' Flow to redirect back to the Case when finished

 public class C_MPG_Code_Finish_Controller{
 
    private final Case theCase;
    private PageReference pageRef;



    public C_MPG_Code_Finish_Controller(ApexPages.StandardController controller) {

    }
    
    // Instantiate the Flow for use by the Controller - linked by VF "interview" attribute
    public Flow.Interview.MPG_Code_Picker f1Demo {get;set;}
    
    public PageReference getprFinishLocation(){
    String prfinishLocation;
    if(f1Demo != null) {
        prfinishLocation = (String)f1Demo.CaseID; // Access flow varibale called CaseID
    }else{
        prfinishLocation = theCase.Id;
    }
    pageRef= new PageReference('/' + prfinishLocation);
    pageRef.setRedirect(true);
    return pageRef;
}

    
}