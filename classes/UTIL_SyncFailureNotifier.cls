public class UTIL_SyncFailureNotifier {
    public static void syncFinished(Database.BatchableContext BC) {
        AsyncApexJob a = [Select Id, ApexClass.Name, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId() LIMIT 1];

        if ((a != null && a.NumberOfErrors > 0) || Test.isRunningTest()) {
            String[] toAddresses = new String[] {
                (String)UTIL_AppSettings.getValue('UTIL_SyncFailureNotifier.SFAdminEmail', a.CreatedBy.Email)
            };

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            mail.setToAddresses(toAddresses);
            mail.setSubject(a.ApexClass.Name + ' Failed');
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with ' + a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}