public with sharing class SFS_Agreement_Matrix1 {
    @AuraEnabled(cacheable=true)
    public static List<Integer> getYears(string svcAgreementId) {
        ServiceContract svcAgreement = [select Id,StartDate,EndDate from ServiceContract where Id = :svcAgreementId];
        Integer startYear = Integer.valueOf(String.valueOf(svcAgreement.StartDate).Split('-')[0]);
        Integer endYear = Integer.valueOf(String.valueOf(svcAgreement.EndDate).Split('-')[0]);
        List<Integer> allYears=new List<Integer>();
        for(Integer i=startYear;i<=endYear;i++){
            allYears.add(i);
        }
        return allYears;
    }
    @AuraEnabled(cacheable=true)
    public static List<Asset> getAssets(string svcAgreementId) {
        List<AggregateResult> assetsRecordIds=[Select AssetId FROM MaintenanceAsset where MaintenancePlan.ServiceContractId =:svcAgreementId group by AssetId];
        Set<Id> assetIds=new Set<Id>();
        if(assetsRecordIds.size()>0){
            for(AggregateResult result:assetsRecordIds){
                assetIds.add((Id)result.get('AssetId'));
            }
        }
        List<Asset> assets=new List<Asset>();
        if(assetIds.size()>0){
            assets=[Select Id,Name from Asset where id IN:assetIds];
        }       
        return assets;
    }
    @AuraEnabled(cacheable=false)
    public static List<MaintenanceWorkRule> getWorkRules(string svcAgreementId){
        try {
            List<MaintenanceWorkRule> workRuleList=[Select Id,Name,ParentMaintenanceRecord.MaintenancePlan.ServiceContractId,SFS_Month__c,SFS_Year__c,NextSuggestedMaintenanceDate,
                                                    ParentMaintenanceRecord.MaintenancePlan.ServiceContract.StartDate, 
                                                    ParentMaintenanceRecord.MaintenancePlan.ServiceContract.EndDate,
                                                    ParentMaintenanceRecord.assetId,ParentMaintenanceRecord.MaintenanceAssetNumber,ParentMaintenanceRecordId, 
                                                    WorkTypeId, WorkType.Name,SFS_Generation_Horizon_Date__c,SFS_Work_Scope__c,(Select Id from WorkOrderLineItems) 
                                                    from MaintenanceWorkRule 
                                                    where ParentMaintenanceRecord.MaintenancePlan.ServiceContractId=:svcAgreementId];          
            return workRuleList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    
    @AuraEnabled(cacheable=true)
    public static List<MaintenanceAsset> getMaintenanceAsset(string svcAgreementId){       
        List<MaintenanceAsset> MaintenanceRecord = new List<MaintenanceAsset>();
        
        MaintenanceRecord = [Select Id,WorkTypeId,AssetId,MaintenancePlan.ServiceContractId from MaintenanceAsset where MaintenancePlan.ServiceContractId =:svcAgreementId  ];
               
        return MaintenanceRecord;
        
    }
    
    
}