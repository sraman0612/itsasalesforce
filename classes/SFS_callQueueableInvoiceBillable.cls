/*=========================================================================================================
* @author : Srikanth P, Capgemini
* @date : 03/28/2023
* @description: Queueable apex to invoke Invoice Billing handler for the Bank Card

Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number               Date          Description
------------------------------------------------------------------------------------

============================================================================================================*/
public class SFS_callQueueableInvoiceBillable implements Queueable {
    @invocableMethod(label = 'Invoice Queueable' description = 'Send to CPI' Category = 'Invoice') 
    public static void SFS_callQueueableInvoiceBillable( List<Id> invoiceId){
          Integer delayInMinutes = 1;
         System.enqueueJob(new SFS_callQueueableInvoiceBillable(invoiceId),delayInMinutes);
    }
   public List<Id> invoiceId;
   public SFS_callQueueableInvoiceBillable(List<Id> invoiceId){
      System.debug('@@@invoiceIdQueueable'+invoiceId);
      this.invoiceId = invoiceId;
       
   }
    public void execute(QueueableContext context) { 
        
      List<Invoice__c> invList = [Select Id,SFS_Credit_Card_Authorization_Status__c from Invoice__c 
                                 where Id =:invoiceId AND SFS_Credit_Card_Authorization_Status__c='Accept' AND SFS_Status__c ='Submitted'];  
      System.debug('@@@creditcareauth'+invList);
        
      if(!invList.isEmpty()){  
         SFS_Billing_WorkOrder_Outbound_Handler.Run(invoiceId);
      }    
    }
}