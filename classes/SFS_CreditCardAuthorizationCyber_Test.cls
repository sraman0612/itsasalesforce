@isTest
global class  SFS_CreditCardAuthorizationCyber_Test
 {
   @isTest
    global static void SFS_Billing_WorkOrder_Outbound_Handler_Test()
    {
        test.setMock(HttpCalloutMock.class, new mockCallout());
            List<Account> accList=new List<Account>();
            Account acct = new Account();
        	acct.name = 'test account1';
        	acct.Currency__c='USD';
            acct.BillingCountry='USA';
            acct.AccountSource='Web';
            acct.BillingState='AP';
            acct.IRIT_Customer_Number__c='test123';
        	acct.Type = 'Prospect';
        	insert acct;
            accList.add(acct);

         List<Division__c> div = new List<Division__c> ();
        Division__c div1 = new Division__c( 
                                 Name = 'TestLoc',
                                 SFS_Org_Code__c = 'ABC',
                                 Division_Type__c='Customer Center' ,
                                 EBS_System__c='Oracle 11i');
        insert div1;
        div.add(div1);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct.Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,true);
        List<id> listOfIds=new List<id>(); 
        Account_Credit_Card__c aCreditCrad=new Account_Credit_Card__c();
        aCreditCrad.First_Name__c='Charles';
        aCreditCrad.Account__c=acct.id;
        aCreditCrad.Last_Name__c='Darwin';
        aCreditCrad.Last_4_Digits__c='8989';
        aCreditCrad.Bluefin_Key__c='029C01801F322400839B%*4124********9990^TEST/BLUEFIN^2212************?*;4124********9990=2212************?*0A062825FFC92BAA9A1C6801995A239577B1F0E377E5BF56CC25AE8553EA3EEB54E33D78CF3335815CF12EA3A99E9D4D446D5E1D0AB25A447EA056626203BCA84573C19181CD6BD7E4D6E639D062323F13AC10F37DCC63CDBD3F44FE2BD8A18D0000000000000000000000000000000000000000000000000000000000000000000000000000000036333455303632373931629949960E002F4000A4611503';
        insert aCreditCrad;
        listOfIds.add(aCreditCrad.id);
        //get invoice
         List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,acct.Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Monthly';
           svcAgreementList[0].SFS_Status__c = 'APPROVED';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='Rental';
           svcAgreementList[0].SFS_Account_Credit_Card__c=aCreditCrad.id;
           insert svcAgreementList[0];
           listOfIds.add(svcAgreementList[0].id);
        
        ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Vendor_Invoice'].id;
         Invoice__c inv = new Invoice__c(recordTypeId = invRecTypeId,
                                         SFS_Service_Agreement__c=svcAgreementList[0].Id
                                       	//SFS_Status__c='Created',
                                       //SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                       //SFS_Billing_Period_End_Date__c=system.today(),
                                        //SFS_Work_Order__c=wo[0].id,
                                       //SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                       //SFS_Billing_Period_Start_Date__c= system.today()
                                       );
            insert inv;
            listOfIds.add(inv.id);
        
           //List<Invoice__c> invList =SFS_TestDataFactory.createInvoice(1,true);
            SFS_Invoice_Line_Item__c inli = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=inv.Id,SFS_Work_Order__c=wo[0].Id/*,SFS_Work_Order_Line_Item__c=woliList[0].Id*/,SFS_Amount__c=123);
            insert inli;
            List<SFS_CreditCardAuthorizationCybersource.creditCrad> css=new List<SFS_CreditCardAuthorizationCybersource.creditCrad>();
			SFS_CreditCardAuthorizationCybersource.creditCrad cs=new SFS_CreditCardAuthorizationCybersource.creditCrad();
			cs.invoiceId=inv.id;
			cs.parenrSgId=svcAgreementList[0].id;
			//cs.parenrWoId=wo[0].id;
			css.add(cs);
            SFS_CreditCardAuthorizationCybersource.Run(css);

            //SFS_CreditCardAuthorizationCybersource.Call_API(listOfIds);
    }
      @isTest
    global static void SFS_Billing_WorkOrder_Outbound_Handler_Test1()
    {
         test.setMock(HttpCalloutMock.class, new mockCallout());
        //try{
            List<Account> accList=new List<Account>();
            Account acct = new Account();
        	acct.name = 'test account1';
        	acct.Currency__c='USD';
            acct.BillingCountry='USA';
            acct.AccountSource='Web';
            acct.BillingState='AP';
            acct.IRIT_Customer_Number__c='test123';
        	acct.Type = 'Prospect';
        	insert acct;
            accList.add(acct);

         List<Division__c> div = new List<Division__c> ();
        Division__c div1 = new Division__c( 
                                 Name = 'TestLoc',
                                 SFS_Org_Code__c = 'ABC',
                                 Division_Type__c='Customer Center' ,
                                 EBS_System__c='Oracle 11i');
        insert div1;
        div.add(div1);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct.Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,true);
        List<id> listOfIds=new List<id>(); 
        Account_Credit_Card__c aCreditCrad=new Account_Credit_Card__c();
        aCreditCrad.First_Name__c='Charles';
        aCreditCrad.Account__c=acct.id;
        aCreditCrad.Last_Name__c='Darwin';
        aCreditCrad.Last_4_Digits__c='8989';
        aCreditCrad.Bluefin_Key__c='029C01801F322400839B%*4124********9990^TEST/BLUEFIN^2212************?*;4124********9990=2212************?*0A062825FFC92BAA9A1C6801995A239577B1F0E377E5BF56CC25AE8553EA3EEB54E33D78CF3335815CF12EA3A99E9D4D446D5E1D0AB25A447EA056626203BCA84573C19181CD6BD7E4D6E639D062323F13AC10F37DCC63CDBD3F44FE2BD8A18D0000000000000000000000000000000000000000000000000000000000000000000000000000000036333455303632373931629949960E002F4000A4611503';
        insert aCreditCrad;
        listOfIds.add(aCreditCrad.id);
        //get invoice
         List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,acct.Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Monthly';
           svcAgreementList[0].SFS_Status__c = 'APPROVED';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='Rental';
           svcAgreementList[0].SFS_Account_Credit_Card__c=aCreditCrad.id;
           insert svcAgreementList[0];
           listOfIds.add(svcAgreementList[0].id);
        
        ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Vendor_Invoice'].id;
         Invoice__c inv = new Invoice__c(recordTypeId = invRecTypeId,
                                         SFS_Service_Agreement__c=svcAgreementList[0].Id
                                       	//SFS_Status__c='Created',
                                       //SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                       //SFS_Billing_Period_End_Date__c=system.today(),
                                        //SFS_Work_Order__c=wo[0].id,
                                       //SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                       //SFS_Billing_Period_Start_Date__c= system.today()
                                       );
            insert inv;
            listOfIds.add(inv.id);
        
           List<Invoice__c> invList =SFS_TestDataFactory.createInvoice(1,true);
            SFS_Invoice_Line_Item__c inli = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=inv.Id,SFS_Work_Order__c=wo[0].Id/*,SFS_Work_Order_Line_Item__c=woliList[0].Id*/,SFS_Amount__c=123);
            insert inli;
            List<SFS_CreditCardAuthorizationCybersource.creditCrad> css=new List<SFS_CreditCardAuthorizationCybersource.creditCrad>();
			SFS_CreditCardAuthorizationCybersource.creditCrad cs=new SFS_CreditCardAuthorizationCybersource.creditCrad();
			cs.invoiceId=inv.id;
			cs.parenrSgId=svcAgreementList[0].id;
			cs.parenrWoId=wo[0].id;
			css.add(cs);
            SFS_CreditCardAuthorizationCybersource.Run(css);
    }
 global class mockCallout implements HttpCalloutMock
    {
        global HttpResponse respond(HttpRequest request)
        {
            string body='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://www.w3.org/2005/08/addressing"> <env:Header> <wsa:Action>execute</wsa:Action> <wsa:MessageID>urn:06dbca19-9650-11ec-b011-02001708e866</wsa:MessageID> <wsa:ReplyTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> <wsa:ReferenceParameters> <instra:tracking.ecid xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">efdbe64f-8c4f-4d51-970a-9a23aca858b4-019cdae0</instra:tracking.ecid> <instra:tracking.FlowEventId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">65910440</instra:tracking.FlowEventId> <instra:tracking.FlowId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">10236104</instra:tracking.FlowId> <instra:tracking.CorrelationFlowId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">0000NwlWN346qIB_vXh8iX1Y5u^f00000k</instra:tracking.CorrelationFlowId> <instra:tracking.quiescing.SCAEntityId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">3760029</instra:tracking.quiescing.SCAEntityId> </wsa:ReferenceParameters> </wsa:ReplyTo> <wsa:FaultTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> </wsa:FaultTo> </env:Header> <env:Body> <ATPResponse xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns="http://xmlns.irco.com/ATPPriceOutResponseABCSImpl"> <ControlArea xmlns=""> <PartnerCode>CPQ</PartnerCode> <RequestType>QUERY</RequestType> <ExternalMessageID>CPQ TestArjun Test Opty 10012021_CTS-76421645803040608</ExternalMessageID> <Database>ISOADEV1</Database> <SourceSystem>EBS</SourceSystem> <Status>SUCCESS</Status> <Messsage>Request Processed Successfully</Messsage> <MessageID>D5DF380550575F90E0540021280BE0EF</MessageID> </ControlArea> <DataArea xmlns=""> <GItem> <Item> <ItemNumber>23231806</ItemNumber> <BestWHSE>DLC (DCL)</BestWHSE> <GAvailability> <Availability> <WHSE>DLC (DCL)</WHSE> <ResultCode>SUCCESS</ResultCode> <AvailableDate>25-FEB-2022</AvailableDate> </Availability> </GAvailability> </Item> </GItem> </DataArea> </ATPResponse> </env:Body> </env:Envelope>';
            HttpResponse res = new HttpResponse();
            res.setBody(body);
            res.setStatusCode(200);
            return res;
        } 
    }
}