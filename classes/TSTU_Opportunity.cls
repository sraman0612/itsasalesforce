@isTest
public class TSTU_Opportunity {

    @TestSetup
    public static void createData() {
        Account testAccount = TSTU_SFAccount.createTestAccount();
        testAccount.Name = 'test';
        testAccount.Type = 'CM';
        testAccount.Account_Number__c = '12345';
        upsert testAccount;

        Account childAccount = TSTU_SFAccount.createTestAccount();
        childAccount.Name = 'test2';
        childAccount.ParentId = testAccount.Id;
        childAccount.Type = 'CM';
        upsert childAccount;
        
        Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'TEST OPP';
        opp.Sales_Channel__c = 'CM';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.AccountId = testAccount.Id;
        opp.Pricebook2Id = pricebookId;

        TSTU_SFOpportunity.upsertOpportunity(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        TSTU_SFOpportunity.upsertOpportunity(opp);
    }
    
    @isTest
    public static void test() {
        List<Opportunity> opps = [SELECT Id from Opportunity];
        
        List<String> oppIds = new List<String>();
        
        for (Opportunity opp : opps) {
            oppIds.add(opp.Id);
        }
        
        List<Account> accounts = [SELECT Id, Name, Channel__c, ParentId, Parent_Account__c, TSM_of_DC_Account__c, Account_Type__c FROM Account];
        
        for (Account account : accounts) {
            System.debug('account.Name=' + account.Name);
            System.debug('account.Channel__c=' + account.Channel__c);
            System.debug('account.ParentId=' + account.ParentId);
            System.debug('account.Parent_Account__c=' + account.Parent_Account__c);
            System.debug('account.TSM_of_DC_Account__c=' + account.TSM_of_DC_Account__c);
            System.debug('account.Account_Type__c=' + account.Account_Type__c);
        }
        
        UTIL_Opportunity.updateOwner(oppIds);
    }
}