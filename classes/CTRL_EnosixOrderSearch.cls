public with sharing class CTRL_EnosixOrderSearch {
    public static final Id GLOBAL_USER_PROFILE_ID = [select Id from Profile Where Name = 'Partner Community - Global Account User'][0].Id;

    private static final Map<String,String> FIELD_MAP = new Map<String, String> {
        'CreateDate' => 'ERDAT', 
        'SalesDocument' => 'VBELN', 
        'SoldToName' => 'KUNAG_NAME', 
        'SoldToCity' => 'KUNAG_CITY1', 
        'SoldToRegion' => 'KUNAG_REGION', 
        'ShipToParty' => 'KUNWE', 
        'ShipToName' => 'KUNWE_NAME', 
        'ShipToCity' => 'KUNWE_CITY1', 
        'ShipToRegion' => 'KUNWE_REGION', 
        'NetOrderValue' => 'NETWR', 
        'CustomerPONumber' => 'BSTNK',
        'OrderStatus' => 'ORDERSTATUS'
    };

    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_EnosixOrderSearch.class);

    @AuraEnabled
    public static UTIL_Aura.Response getScreenData() {
        SCREEN_DATA retVal = new SCREEN_DATA();

        List<NVP> options = new List<NVP>();
        User currentUser = [Select AccountId, Account.Name, Account.AccountNumber, Account.ParentId, Account.Parent.AccountNumber, Account.Associated_Rep_Account__c, Account.Parent_Account_Global__c, Account.Parent_Acct_Name_with_Number__c, ProfileId from User where Id = :UserInfo.getUserId()];

        if (currentUser != null && currentUser.AccountId != null) {
            if (currentUser.ProfileId == GLOBAL_USER_PROFILE_ID && currentUser.Account.ParentId != null) {
                for (Account tmp : [select Id, Name, AccountNumber, Parent_Acct_Name_with_Number__c from Account where ParentId = :currentUser.Account.ParentId limit 1000]) {
		            options.add(new NVP(tmp.AccountNumber, tmp.AccountNumber));
                }
                
                List<Account> repAccounts = [
                    SELECT Id, 
                    	AccountNumber,
                    	Parent_Acct_Name_with_Number__c,
                    	Parent_Account__c
                    FROM Account
					WHERE Account_Type__c = 'Child'
					AND Order_Block__c != 'Blocked'
					AND ParentId != null
					AND Rep_Account2__c != null
					AND RecordType.DeveloperName = 'Customer'
					AND Parent_Account_Global__c != : currentUser.Account.Parent_Account_Global__c
					AND (Rep_Acct_Grandparent__c = : currentUser.Account.ParentId OR Rep_Acct_Grandparent__c = : currentUser.Account.Associated_Rep_Account__c)
                    ORDER BY Parent_Acct_Name_with_Number__c asc
                ];
                
                Map<String,NVP> repAccountOptions = new Map<String,NVP>();
                
                for (Account tmp: repAccounts) {
		            repAccountOptions.put(tmp.Parent_Acct_Name_with_Number__c, new NVP(tmp.Parent_Acct_Name_with_Number__c, tmp.Parent_Account__c));
                }
                
                options.addAll(repAccountOptions.values());
            } else {
                options.add(new NVP(currentUser.Account.AccountNumber, currentUser.Account.AccountNumber));
            }

            retVal.soldToAccounts = options;
        }

        retVal.orderStatuses = new List<NVP>();
        retVal.orderStatuses.add(new NVP('All', ''));
        retVal.orderStatuses.add(new NVP('Open Orders Only', 'open'));
        retVal.orderStatuses.add(new NVP('Completed Orders Only', 'complete'));

        return UTIL_Aura.createResponse(JSON.serialize(retVal));
    }

    @AuraEnabled
    public static UTIL_Aura.Response searchSAP(
        String soldTo,
        String orderStatus,
        String poNumber,
        String orderNumber,
        Date createdFrom,
        Date createdTo,
        String sortBy,
        String sortDirection,
        Integer pageSize,
        Integer pageNumber
    ) {

        logger.enterAura('searchSAP', new Map<String, Object> {
            'soldTo' => soldTo,
            'orderStatus' => orderStatus,
            'poNumber' => poNumber,
            'orderNumber' => orderNumber,
            'createdFrom' => createdFrom,
            'createdTo' => createdTo,
            'sortBy' => sortBy,
            'sortDirection' => sortDirection,
            'pageSize' => pageSize,
            'pageNumber' => pageNumber
        });

        SBO_EnosixSO_Search orderSearch = new SBO_EnosixSO_Search();
        SBO_EnosixSO_Search.EnosixSO_SC searchContext = new SBO_EnosixSO_Search.EnosixSO_SC();

        if (sortDirection != null && sortBy != null && FIELD_MAP.get(sortBy) != null) {
            System.debug('in sort ' + FIELD_MAP.get(sortBy));
            searchContext.pagingOptions.sortFields.add(FIELD_MAP.get(sortBy) ,sortDirection=='asc'?'ASC':'DESCENDING');
        }

        if (String.isNotBlank(soldTo)) {
            searchContext.SEARCHPARAMS.SoldToParty = soldTo;
        }

        if (String.isNotBlank(poNumber)) {
            searchContext.SEARCHPARAMS.CustomerPONumber = poNumber;
        }

        if (String.isNotBlank(orderNumber)) {
            searchContext.SEARCHPARAMS.FromSalesDocumentNumber = orderNumber;
            searchContext.SEARCHPARAMS.ToSalesDocumentNumber = orderNumber;
        }

        if (createdFrom != null) {
            searchContext.SEARCHPARAMS.FromCreateDate = Datetime.newInstance(createdFrom, Time.newInstance(0,0,0,0)).format('yyyyMMdd');
        }

        if (createdTo != null) {
            searchContext.SEARCHPARAMS.ToCreateDate = Datetime.newInstance(createdTo, Time.newInstance(0,0,0,0)).format('yyyyMMdd');
        }

        searchContext.pagingOptions.pageSize = pageSize;
        searchContext.pagingOptions.pageNumber = pageNumber;

        searchContext.SEARCHPARAMS.OpenOnly = false;
        searchContext.SEARCHPARAMS.CompletedOnly = false;

        if (orderStatus != null && orderStatus == 'open')
        {
            searchContext.SEARCHPARAMS.OpenOnly = true;
        }
        else if (orderStatus != null && orderStatus == 'complete')
        {
            searchContext.SEARCHPARAMS.CompletedOnly = true;
        }

        orderSearch.search(searchContext);
        SBO_EnosixSO_Search.EnosixSO_SR searchResult = searchContext.result;

        List<SBO_EnosixSO_Search.SEARCHRESULT> orders = null;

        if (searchResult.isSuccess()) {
            orders = searchResult.getResults();
        } else {
            UTIL_PageMessages.addFrameworkMessages(searchContext.getMessages());
        }

        SEARCH_RESULTS sr = new SEARCH_RESULTS();

        sr.soldTo = soldTo;
        sr.orderStatus = orderStatus;
        sr.poNumber = poNumber;
        sr.orderNumber = orderNumber;
        sr.createdFrom = createdFrom;
        sr.createdTo = createdTo;
        sr.sortBy = sortBy;
        sr.sortDirection = sortDirection;
        sr.pageSize = searchContext.pagingOptions.pageSize;
        sr.pageNumber = searchContext.pagingOptions.pageNumber;
        sr.totalRecords = searchContext.pagingOptions.totalRecords;
        sr.totalPages = searchContext.pagingOptions.totalRecords > 0 ? Integer.valueOf(Math.ceil(Double.valueOf(searchContext.pagingOptions.totalRecords) / Double.valueOf(searchContext.pagingOptions.pageSize))) : 0;
        sr.orders = orders;

        return UTIL_Aura.createResponse(JSON.serialize(sr));
    }

    public class SEARCH_RESULTS {
        public String soldTo {get; set;}
        public String orderStatus {get; set;}
        public String poNumber {get; set;}
        public String orderNumber {get; set;}
        public String sortBy {get; set;}
        public String sortDirection {get; set;}
        public Date createdFrom {get; set;}
        public Date createdTo {get; set;}
        public integer pageNumber {get; set;}
        public integer pageSize {get; set;}
        public integer totalRecords {get; set;}
        public integer totalPages {get; set;}
        public List<SBO_EnosixSO_Search.SEARCHRESULT> orders {get; set;}
    }

    public class SCREEN_DATA {
        public List<NVP> soldToAccounts {get;set;}
        public List<NVP> orderStatuses {get;set;}
    }

    public class NVP implements Comparable {
        public String name {get; set;}
        public String value {get; set;}

        public NVP(String nameValue) {
            this.name = nameValue;
            this.value = nameValue;
        }
        public NVP(String name, String value) {
            this.name = name;
            this.value = value;
        }
        public Integer compareTo(Object compareTo) {
            return this.name.compareTo(((NVP) compareTo).name);
        }
    }
}