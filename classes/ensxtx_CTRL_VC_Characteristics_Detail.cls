public with sharing class ensxtx_CTRL_VC_Characteristics_Detail {

    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_CTRL_VC_Characteristics_Detail.class);

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getVC_Characteristics_Detail(
        String AccountId, String quoteId, String itemId
    )
    {
        Object responseData = null;

        logger.enterAura('getVC_Characteristics_Detail', new Map<String,Object>{
            'accountId' => accountId,
            'quoteId' => quoteId,
            'itemId' => itemId
        });

        try
        {

            ensxtx_SBO_EnosixQuote_Detail.EnosixQuote quoteDetail = getQuoteDetail(quoteId);

            List<ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG> itemsConfig = quoteDetail.ITEMS_CONFIG.getAsList();
            List<ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG> selectedItems = new List<ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG>();
            List<ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG> displayedItems = new List<ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG>();

            Integer configTot = itemsConfig.size();
            for (Integer configCnt = 0 ; configCnt < configTot ; configCnt++)
            {
                ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG config = itemsConfig[configCnt];
                if(config.ItemNumber == itemId)
                {
                    selectedItems.add(config);
                }
            }

            //get material number
            List<ensxtx_SBO_EnosixQuote_Detail.ITEMS> theseItems = quoteDetail.ITEMS.getAsList();
            ensxtx_SBO_EnosixQuote_Detail.ITEMS thisItem = new ensxtx_SBO_EnosixQuote_Detail.ITEMS();

            Integer itemTot = theseItems.size();
            for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
            {
                ensxtx_SBO_EnosixQuote_Detail.ITEMS item = theseItems[itemCnt];
                if(item.ItemNumber == itemId)
                {
                    thisItem = item;
                }
            }

            // our selected items need to be formatted as a list of ensxtx_DS_VCCharacteristicValues
            List<ensxtx_DS_VCCharacteristicValues> convertedList = convertTo_CharacteristicValues(selectedItems);
            ensxtx_SBO_EnosixVC_Detail.EnosixVC config = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
            config.Material = thisItem.Material;

            ensxtx_DS_VCMaterialConfiguration materialConfig = proccessAndLogVCConfiguration(config, convertedList);

            List<ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS> itemCharacteristics = materialConfig.characteristics;

            Integer siTot = selectedItems.size();
            Integer icTot = itemCharacteristics.size();
            for (Integer siCnt = 0 ; siCnt < siTot ; siCnt++)
            {
                ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG item = selectedItems[siCnt];
                String CharName = item.CharacteristicName;
                for (Integer icCnt = 0 ; icCnt < icTot ; icCnt++)
                {
                    ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS thisChar = itemCharacteristics[icCnt];
                    if(thisChar.CharacteristicName == CharName && thisChar.NotToBeDisplayed != 'X')
                    {
                        displayedItems.add(item);
                    }
                }

            }

            responseData = displayedItems;

        } 
        catch (Exception ex) { ensxtx_UTIL_PageMessages.addExceptionMessage(ex); }
        finally
        {
            logger.exit();
        }

        return ensxtx_UTIL_Aura.createResponse(responseData);
    }

    /// This is the internal method that handles calling to SAP during processing of configuration.
    @TestVisible
    private static ensxtx_DS_VCMaterialConfiguration proccessAndLogVCConfiguration(
        ensxtx_SBO_EnosixVC_Detail.EnosixVC sboCFG
        , List<ensxtx_DS_VCCharacteristicValues> selectedValues)
    {
        logger.enterAura('proccessAndLogVCConfiguration', new Map<String, Object> {
            'sboCFG' => sboCFG
            , 'selectedValues' => selectedValues
        });
        try {

            Integer svTot = selectedValues.size();
            for (Integer svCnt = 0 ; svCnt < svTot ; svCnt++)
            {
                ensxtx_DS_VCCharacteristicValues selectedValue = selectedValues[svCnt];
                sboCFG.SELECTEDVALUES.add(selectedValue.getSBOASelectedValuesForModel());
            }

            ensxtx_SBO_EnosixVC_Detail sbo = new ensxtx_SBO_EnosixVC_Detail();

            sboCFG.setBoolean(true,'useTextSerialize');
            sboCFG = sbo.command('CMD_INITIALIZE_FROM_DATA', sboCFG);

            return new ensxtx_DS_VCMaterialConfiguration(sboCFG);

        } catch (Exception ex) {
            logger.log(LoggingLevel.ERROR, 'There was an exception thrown when attempting to process configuration', ex);
            //THIS NEEDS TO BE UDPATED SO IT DISPLAYS ON THE PAGE
            //ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        } finally {
            logger.exit();
        }
        //return ensxtx_UTIL_Aura.createResponse(responseData);
        return null;
    }
    @TestVisible
    private static List<ensxtx_DS_VCCharacteristicValues> convertTo_CharacteristicValues(List<ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG> selectedItems)
    {
        List<ensxtx_DS_VCCharacteristicValues> convertedList = new List<ensxtx_DS_VCCharacteristicValues>();

        try
        {
            Integer siTot = selectedItems.size();
            for (Integer siCnt = 0 ; siCnt < siTot ; siCnt++)
            {
                ensxtx_SBO_EnosixQuote_Detail.ITEMS_CONFIG item = selectedItems[siCnt];
                ensxtx_DS_VCCharacteristicValues newCV = new ensxtx_DS_VCCharacteristicValues();
                newCV.CharacteristicID = item.CharacteristicID;
                newCV.CharacteristicName = item.CharacteristicName;
                newCV.CharacteristicValue = item.CharacteristicValue;
                newCV.CharacteristicValueDescription = item.CharacteristicValueDescription;
                newCV.UserModified = true;

                convertedList.add(newCV);
            }
        } catch (Exception e)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(e);
        }

        return convertedList;
    }

    private static ensxtx_SBO_EnosixQuote_Detail.EnosixQuote getQuoteDetail(string quoteId)
    {
        ensxtx_SBO_EnosixQuote_Detail sbo = new ensxtx_SBO_EnosixQuote_Detail();
        ensxtx_SBO_EnosixQuote_Detail.EnosixQuote quoteDetail = new ensxtx_SBO_EnosixQuote_Detail.EnosixQuote();

        try 
        {
            quoteDetail = sbo.getDetail(quoteId);
        } 
        catch (Exception e) { logger.error(e); }

        return quoteDetail;
    }

}