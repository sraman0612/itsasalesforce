@isTest
public class ensxtx_TSTU_VC_PricingAndConfiguration
{
    public class MOC_EnosixVC_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock,
            ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock,
            ensxsdk.EnosixFramework.DetailSBOInitMock
    {
        public boolean success = true;
        public boolean throwException = false;

        public void setSuccess(boolean success)
        {
            this.success = success;
        }

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = (ensxtx_SBO_EnosixVC_Detail.EnosixVC) obj;
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string command, ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = (ensxtx_SBO_EnosixVC_Detail.EnosixVC) obj;
            result.setSuccess(success);
            return result;
        }

        public  ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject  initialState)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            ensxtx_SBO_EnosixVC_Detail.EnosixVC result = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
            result.setSuccess(success);
            return result;
        }
    }

    @isTest
    public static void test_logger()
    {
        ensxsdk.Logger logger = ensxtx_UTIL_VC_PricingAndConfiguration.logger;
    }

    @isTest
    public static void test_getProductIdForSAPMaterialNumber()
    {
        Product2 product = ensxtx_TSTU_SFTestObject.createTestProduct2();
        ensxtx_TSTU_SFTestObject.upsertWithRetry(product);
        String prodId = ensxtx_UTIL_VC_PricingAndConfiguration.getProductIdForSAPMaterialNumber(product.Name);
    }

    @isTest
    public static void test_getSAPMaterialNumberFromProductId()
    {
        Product2 product = ensxtx_TSTU_SFTestObject.createTestProduct2();
        ensxtx_TSTU_SFTestObject.upsertWithRetry(product);
        String prodId = ensxtx_UTIL_VC_PricingAndConfiguration.getSAPMaterialNumberFromProductId(product.Id);
    }

    @isTest
    public static void test_stripLeadingZeros()
    {
        System.assertEquals('421', ensxtx_UTIL_VC_PricingAndConfiguration.stripLeadingZeros('00421'));
        System.assertEquals('0', ensxtx_UTIL_VC_PricingAndConfiguration.stripLeadingZeros('0'));
    }

    @isTest
    public static void test_getInitialConfigFromMaterialAndPricing()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixVC_Detail.class, mocEnosixVCDetail);
        ensxtx_ENSX_VCConfiguration serializableCfg = ensxtx_UTIL_VC_PricingAndConfiguration.getInitialConfigFromMaterialAndPricing('material', new ensxtx_ENSX_VCPricingConfiguration());
    }

    @isTest
    public static void test_getInitialConfigFromMaterialAndPricingAndCustomConfig()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixVC_Detail.class, mocEnosixVCDetail);
        try
        {
            ensxtx_DS_VCMaterialConfiguration config =
                ensxtx_UTIL_VC_PricingAndConfiguration.getInitialConfigFromMaterialAndPricingAndCustomConfig('material', new ensxtx_ENSX_VCPricingConfiguration());
        }
        catch(Exception e)
        {}
        String TEST_JSON = '{"UseSapSession": false}';
        ensxtx_UTIL_AppSettings.settingsMap.put(ensxtx_UTIL_AppSettings.Prefix + ensxtx_UTIL_AppSettings.VC + ensxtx_UTIL_AppSettings.Suffix, (Map<String, Object>)JSON.deserializeUntyped(TEST_JSON));
        mocEnosixVCDetail.setThrowException(true);
        try
        {
            ensxtx_DS_VCMaterialConfiguration config = 
                ensxtx_UTIL_VC_PricingAndConfiguration.getInitialConfigFromMaterialAndPricingAndCustomConfig('material', new ensxtx_ENSX_VCPricingConfiguration());
        }
        catch(Exception e)
        {}
    }

    @isTest
    public static void test_applyPricingConfigurationToSBO()
    {
        ensxtx_SBO_EnosixVC_Detail.EnosixVC sboConfig = ensxtx_UTIL_VC_PricingAndConfiguration.applyPricingConfigurationToSBO(new ensxtx_SBO_EnosixVC_Detail.EnosixVC (), new ensxtx_ENSX_VCPricingConfiguration());
    }
    
    @isTest
    public static void test_getInitializedConfigSBOModelFromBOM()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixVC_Detail.class, mocEnosixVCDetail);
        mocEnosixVCDetail.setSuccess(false);
        List<ensxtx_DS_VCCharacteristicValues> BOM = new List<ensxtx_DS_VCCharacteristicValues>();
        ensxtx_DS_VCCharacteristicValues VCValue = new ensxtx_DS_VCCharacteristicValues();
        VCValue.UserModified = false;
        BOM.add(VCValue);
        ensxtx_UTIL_VC_PricingAndConfiguration.getInitializedConfigSBOModelFromBOM('', new ensxtx_ENSX_VCPricingConfiguration(), BOM);
    }

    @isTest
    public static void test_proccessAndLogVCConfiguration()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixVC_Detail.class, mocEnosixVCDetail);
        
        ensxtx_UTIL_VC_PricingAndConfiguration.proccessAndLogVCConfiguration(
            new ensxtx_DS_VCMaterialConfiguration(new ensxtx_SBO_EnosixVC_Detail.EnosixVC()), 
            new List<ensxtx_DS_VCCharacteristicValues>());
        ensxtx_UTIL_VC_PricingAndConfiguration.proccessAndLogVCConfiguration(
            new ensxtx_DS_VCMaterialConfiguration(new ensxtx_SBO_EnosixVC_Detail.EnosixVC()), 
            new List<ensxtx_DS_VCCharacteristicValues>(),
            new List<String>{'Test'});
    }

    @isTest
    public static void test_execCmd()
    {
        MOC_EnosixVC_Detail mocEnosixVCDetail = new MOC_EnosixVC_Detail();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixVC_Detail.class, mocEnosixVCDetail);
        String TEST_JSON = '{"UseSapSession": false}';
        ensxtx_UTIL_AppSettings.settingsMap.put(ensxtx_UTIL_AppSettings.Prefix + ensxtx_UTIL_AppSettings.VC + ensxtx_UTIL_AppSettings.Suffix, (Map<String, Object>)JSON.deserializeUntyped(TEST_JSON));
        ensxtx_UTIL_VC_PricingAndConfiguration.execCmd('Test', new ensxtx_SBO_EnosixVC_Detail.EnosixVC(), 'requestSessionData');
        mocEnosixVCDetail.setSuccess(false);
        ensxtx_UTIL_VC_PricingAndConfiguration.execCmd('Test', new ensxtx_SBO_EnosixVC_Detail.EnosixVC(), 'requestSessionData');
    }

    @isTest
    public static void test_closeSession()
    {
        ensxtx_UTIL_VC_PricingAndConfiguration.headers = new Map<String,String>();
        ensxtx_UTIL_VC_PricingAndConfiguration.headers.put('Test1', 'Test1');
        ensxtx_UTIL_VC_PricingAndConfiguration.headers.put('Test2', 'Test2');
        ensxtx_UTIL_VC_PricingAndConfiguration.closeSession(ensxtx_UTIL_VC_PricingAndConfiguration.SessionData);
    }

    @isTest
    public static void test_getObjectKey()
    {
        ensxtx_UTIL_VC_PricingAndConfiguration.getObjectKey(null);
        ensxtx_UTIL_VC_PricingAndConfiguration.getObjectKey(new ensxtx_SBO_EnosixVC_Detail.EnosixVC());
    }

    @isTest
    public static void test_isInitialKey()
    {
        ensxtx_UTIL_VC_PricingAndConfiguration.isInitialKey('Test');
    }

    @isTest
    public static void test_getBusinessObjectName()
    {
        ensxtx_UTIL_VC_PricingAndConfiguration.getBusinessObjectName(new ensxtx_SBO_EnosixVC_Detail());
    }

    @isTest
    public static void test_deserializeObject()
    {
        ensxtx_UTIL_VC_PricingAndConfiguration.deserializeObject(new ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS(), new Account());
    }

    @isTest
    public static void test_httpSend()
    {
        ensxtx_UTIL_VC_PricingAndConfiguration.headers = null;
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint('endPoint');
        ensxtx_UTIL_VC_PricingAndConfiguration.httpSend(httpReq, new Map<String,String>());
    }

    @isTest
    public static void test_executeInitialize()
    {
        ensxtx_UTIL_VC_PricingAndConfiguration.executeInitialize(new ensxtx_SBO_EnosixVC_Detail(), new ensxtx_SBO_EnosixVC_Detail.EnosixVC());
    }

    @isTest
    public static void test_executeCommand()
    {
        ensxtx_UTIL_VC_PricingAndConfiguration.executeCommand(new ensxtx_SBO_EnosixVC_Detail(), new ensxtx_SBO_EnosixVC_Detail.EnosixVC(), 'command');
    }
}