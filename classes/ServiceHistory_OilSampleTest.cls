@isTest
public class ServiceHistory_OilSampleTest {
    @isTest static void testOilSample()
    {
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Oil_Sample_Date_Range__c = 300.10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        cs.Hour_Overage_Threshold_Percent__c = 5000;
        insert cs;
        
        account a = new account(name='test1');
        insert a;
        
        account a2 = new account(name='Gardner Denver');
        insert a2;
        
        Asset aset = new Asset(name = 'D152581',SerialNumber='D152581',Current_Servicer__c=a.id,Hours_Oil_Sample__c=null);
        insert aset;
        
        Service_History__c sh = new Service_History__c(Serial_Number__c =aset.id,Date_of_Service__c=system.today(),Current_Hours__c=4000);
        insert sh;
        
        String JSONMsg = '{'+
            '"Oil_Sample": {'+
                '"Serial_Number_Text__c" : "D152581",'+
                '"Address__c": "Test address",'+
                '"Barium__c": "3",'+
                '"Oil_Hours__c": "11",'+
                '"Status__c": "Action Required",'+
                '"Collect_Date__c": "2018-01-05",'+
                '"Zinc__c": "0",'+
                '"Collecting_Distributor__c": "test",'+
                '"Report_Number__c": "11111111"'+
                '}'+
        '}';
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/oil-sample/';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        ServiceHistory_OilSample.doPost();
    }
    
    @isTest static void testOilSample2()
    {
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Oil_Sample_Date_Range__c = 300.10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        cs.Hour_Overage_Threshold_Percent__c = 5000;
        insert cs;
        
        account a = new account(name='test1');
        insert a;
        
        account a2 = new account(name='Gardner Denver');
        insert a2;
        
        String JSONMsg = '{'+
            '"Oil_Sample": {'+
                '"Serial_Number_Text__c" : "D152581-343 fss",'+
                '"Address__c": "Test address",'+
                '"Barium__c": "3",'+
                '"Oil_Hours__c": "11",'+
                '"Status__c": "Action Required",'+
                '"Collect_Date__c": "2018-01-05",'+
                '"Zinc__c": "0",'+
                '"Collecting_Distributor__c": "test",'+
                '"Report_Number__c": "11111111"'+
                '}'+
        '}';
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/oil-sample/';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        ServiceHistory_OilSample.doPost();
    }
    
    @isTest static void testOilSample3()
    {
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Oil_Sample_Date_Range__c = 300.10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        cs.Hour_Overage_Threshold_Percent__c = 5000;
        insert cs;
        
        account a = new account(name='test1');
        insert a;
        
        account a2 = new account(name='Gardner Denver');
        insert a2;
        
        Asset aset = new Asset(name = 'D152581',SerialNumber='D152581',Hours_Oil_Sample__c=null);
        insert aset;
        
        Service_History__c sh = new Service_History__c(Serial_Number__c =aset.id,Date_of_Service__c=system.today(),Current_Hours__c=4000);
        insert sh;
        
        String JSONMsg = '{'+
            '"Oil_Sample": {'+
                '"Serial_Number_Text__c" : "D152581",'+
                '"Address__c": "Test address",'+
                '"Barium__c": "3",'+
                '"Oil_Hours__c": "11",'+
                '"Status__c": "Action Required",'+
                '"Collect_Date__c": "2018-01-05",'+
                '"Zinc__c": "0",'+
                '"Collecting_Distributor__c": "test",'+
                '"Report_Number__c": "11111111"'+
                '}'+
        '}';
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/oil-sample/';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        ServiceHistory_OilSample.doPost();
    }
}