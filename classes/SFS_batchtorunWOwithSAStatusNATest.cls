@isTest
public class SFS_batchtorunWOwithSAStatusNATest {
    @isTest
    public static void UpdateIntegrationStatusTest() {
    String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
    List<Division__c> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
    	Account acct1 = new Account();
        acct1.name = 'test account1';
        acct1.Currency__c='USD';
        acct1.AccountSource='Web';
        acct1.IRIT_Customer_Number__c='1221';
        acct1.IRIT_Payment_Terms__c='NET 30';
        acct1.RecordTypeId=accRecID;
        insert acct1;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Type='Prospect';
        acct.Bill_To_Account__c=acct1.id;
        acct.IRIT_Customer_Number__c='1234';
        acct.Currency__c='USD';
        acct.Account_Region__c ='Oracle 11i';
        acct.AccountSource='Web';
        acct.IRIT_Customer_Number__c='4322';
        acct.IRIT_Payment_Terms__c='NET 30';
        insert acct;
                
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = acct.Id;
        insert assetList[0];
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,acct.Id,false);
        svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';           
        svcAgreementList[0].SFS_Status__c = 'APPROVED';
        svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
        svcAgreementList[0].SFS_PO_Number__c = '2';
        svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
        svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList[0].SFS_Renewal_Escalator_Start_Date__c=system.today().addDays(-10);
        svcAgreementList[0].SFS_Recurring_Adjustment__c=3;
        svcAgreementList[0].ShippingHandling=2;
        svcAgreementList[0].SFS_Type__c='Rental';
        svcAgreementList[0].AccountId=acct.Id;
        svcAgreementList[0].SFS_Bill_To_Account__c=acct1.Id;
        svcAgreementList[0].SFS_Consumables_Ship_To__c=acct.Id;
        svcAgreementList[0].SFS_Division__c=divisionsList[0].Id;
        insert svcAgreementList;
        Account billToAcc = SFS_TestDataFactory.getAccount();
        Update billToAcc;
        
        List<ContractLineItem> cLi=SFS_TestDataFactory.createServiceAgreementLineItem(1,svcAgreementList[0].Id,false);
        insert cLi[0];
        
        List<Entitlement> entitle = SFS_TestDataFactory.createEntitlement(1,acct.Id,null,svcAgreementList[0].Id,cLi[0].Id,false);
        entitle[0].SFS_X1st_PM_Month__c = 'October';
        entitle[0].ServiceContractId = svcAgreementList[0].id;
        
        insert entitle;
        System.debug('@@contractid'+entitle[0].ServiceContractId);
        List<MaintenancePlan> mpList=SFS_TestDataFactory.createMaintenancePlan(1,svcAgreementList[0].Id,false);
        insert mpList;
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acct.Id,null,divisionsList[0].Id,svcAgreementList[0].Id, false);
        WoList[0].MaintenancePlanId=mpList[0].Id;
        WoList[0].SuggestedMaintenanceDate=system.Today().addYears(1);
        WoList[0].SFS_Bill_To_Account__c=acct1.id;
        WoList[0].SFS_Requested_Payment_Terms__c='BANKCARD';
        WoList[0].Shipping_Account__c=acct.Id;
        WoList[0].SFS_Division__c=divisionsList[0].Id;        
        Wolist[0].SFS_Invoice_Submitted__c=true;
        WoList[0].ServiceContractId=svcAgreementList[0].id;
        WoList[0].SFS_Requested_Payment_Terms__c='NET 30';
        WoList[0].EntitlementId=entitle[0].Id;
        
        WoList[0].Status='Open';
        WoList[0].SFS_Integration_Status__c='N/A';
        WoList[0].AssetId = assetList[0].Id;
            
        insert WoList[0];
        Test.startTest();
        System.debug('@@testbatch'+ WoList[0].SFS_Integration_Status__c+','+WoList[0].MaintenancePlanId+','+WoList[0].Entitlement.ServiceContractId+','+WoList[0].EntitlementId+','+WoList[0].Status);
        SFS_batchtorunWOwithSAStatusNA bat = new SFS_batchtorunWOwithSAStatusNA();
        database.executeBatch(bat);
        Test.stopTest();
     
    }
}