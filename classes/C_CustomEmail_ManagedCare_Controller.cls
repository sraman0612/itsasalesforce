global class C_CustomEmail_ManagedCare_Controller {

    private Boolean isActionItem;
    private Boolean isCancelled;
    private Set<Id> startingAttachments; 
    public Map<Id,attachmentWrapper> wrapMap;
    public Boolean hasParentAttachments {get{
        List<Attachment> attachments = [SELECT Id FROM Attachment WHERE ParentId =: this.theCase.Id ORDER BY CreatedDate ASC];
        return attachments.size() > 0;}
     set;}
    public Boolean isResponse {get; private set;}
    public Case theCase {get; set;} 
    public Contact toContact {get; set;}
    @testVisible
    public List<addressWrapper> bccWrappers {get; set;}
    @testVisible
    public List<addressWrapper> ccWrappers {get; set;}
    @testVisible
    public List<addressWrapper> toWrappers {get; set;}
    
    @testVisible
    public List<attachmentWrapper> attachmentsWrapper {
        get{                   
            System.debug('getAttachmentsWrapper');
            //System.debug('this.isActionItem: ' + this.isActionItem);
            //System.debug('this.theCase.ParentId: ' + this.theCase.ParentId);
            System.debug('this.theCase.Id: ' + this.theCase.Id);
            List<Attachment> attachments = [SELECT Id, Name, CreatedDate, ContentType, Description FROM Attachment WHERE ParentId = :this.theCase.Id ORDER BY CreatedDate ASC];
            System.debug('attachments size: ' + attachments.size());
            List<attachmentWrapper> result = new List<attachmentWrapper>();
            if(attachments.size() > 0){
                for(Attachment att : attachments){
                    if(!wrapMap.containsKey(att.Id)){
                        attachmentWrapper aw = new attachmentWrapper(att, !startingAttachments.contains(att.Id), this);
                        wrapMap.put(att.Id,aw);
                        System.debug('wrapMapNew: ' + wrapMap.get(att.Id).theAttachment.Name + ' - ' + wrapMap.get(att.Id).include);
                        result.add(aw);
                    }
                    else{
                        result.add(wrapMap.get(att.Id));
                        System.debug('wrapMap: ' + wrapMap.get(att.Id).theAttachment.Name + ' - ' + wrapMap.get(att.Id).include);
                    }
                }
                this.hasParentAttachments = true;
            }
            else{
                this.hasParentAttachments = false;
            }    
            System.debug('ending');
            return result;
        } 
        private set;}
    public OrgWideEmailAddress fromAddress {get; private set;}  
    public String editorContent {get; set;}
    public String freeFormBCC {get; set;}
    public String freeFormCC {get; set;}
    public String emailSubject {get; set;}
    public String templatedValue {get; set;}
    public String originalBody {get; private set;}
    public String subjectHeader {get; private set;}
    @testVisible
    public String correctToAddress {get; private set;}
    
    private Contact theContact;
    private EmailMessage parentEm;
    private List<Attachment> parentAttachments {private get; private set;}
    public String parentEmailId {get; private set;}
    private String caseAction;
    public Boolean isDeclined {get; private set;}
    public Boolean declinedSet {get; private set;}
    public Boolean otherRequired {get; private set;}

    public C_CustomEmail_ManagedCare_Controller(){
        theCase = [SELECT OwnerName__c, Decline_Reason__c, Decline_Reason_Other__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c, Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, CaseNumber, Subject FROM Case WHERE Id =: Apexpages.currentpage().getparameters().get('CaseId')];      
        this.isDeclined = false;
        this.declinedSet = true;
        this.otherRequired = false;
        caseAction = Apexpages.currentpage().getparameters().get('action');
        parentEm = [Select e.Incoming,e.FromName, e.ToAddress, e.TextBody, e.Subject, e.MessageDate, e.ParentId, e.HtmlBody, e.FromAddress, e.CcAddress, e.BccAddress From EmailMessage e WHERE ParentId =: theCase.Id ORDER BY CreatedDate ASC LIMIT 1];              
        initialize();
    }
    
    public PageReference initialize(){
    try{
        if(caseAction == 'Declined'){
            this.isDeclined = true;
            if(String.isEmpty(this.theCase.Decline_Reason__c)){
                this.declinedSet = false;
            }
            else{
                
                if(this.theCase.Decline_Reason__c == 'Other' && String.isEmpty(this.theCase.Decline_Reason_Other__c)){
                    this.otherRequired = true;
                    this.declinedSet = false;
                }
                else{
                    this.otherRequired = false;
                    this.declinedSet = true;
                }
            }                       
        }
        
        if(isDeclined && declinedSet){
            templatedValue = generateBody();
        }
        else{
            templatedValue = generateBody();
        }
        
        System.debug('Initialize');
        this.isCancelled = false;                     
        
        toWrappers = new List<addressWrapper>();
        ccWrappers = new List<addressWrapper>();
        bccWrappers = new List<addressWrapper>();
        freeFormCC = '';       
               
        if(!String.isEmpty(parentEm.CcAddress)){
            List<String> ccAdd = parentEm.CcAddress.split(';');
            Set<String> ccAddSet = new Set<String>();
            ccAddSet.addAll(ccAdd);
            System.debug(ccAdd);
            List<Contact> ccAddressContacts = [SELECT Id, Name, Email FROM Contact WHERE Email IN : ccAdd];
            System.debug(ccAddressContacts);
            for(Contact c: ccAddressContacts){
                ccWrappers.add(new AddressWrapper(c));
                ccAddSet.remove(c.Email);
            }
            System.debug('ccAddSet ' + ccAddSet);
            if(ccAddSet.size() > 0){
                for(String s : ccAddSet){
                    freeFormCC += (s+';');
                }
            }
        }
        else{
            for(integer i = 0; i < 3; ++i){
                ccWrappers.add(new addressWrapper());
            }
        }
        
        if(!String.isEmpty(parentEm.BccAddress)){
            List<String> bccAdd = parentEm.BccAddress.split(';');
            Set<String> bccAddSet = new Set<String>();
            bccAddSet.addAll(bccAdd);
            System.debug(bccAdd);
            List<Contact> bccAddressContacts = [SELECT Id, Name, Email FROM Contact WHERE Email IN : bccAdd];
            System.debug(bccAddressContacts);
            for(Contact c: bccAddressContacts){
                bccWrappers.add(new AddressWrapper(c));
                bccAddSet.remove(c.Email);
            }
            if(bccAddSet.size() > 0){
                for(String s : bccAddSet){
                    freeFormBCC += (s+';');
                }
            }
        }    
        else{
            for(integer i = 0; i < 3; ++i){
                bccWrappers.add(new addressWrapper());
            }
        }              

        if(theCase.ContactId != null){
            theContact = [SELECT Id FROM Contact WHERE Id =: theCase.ContactId];
            toContact = theContact;
        }
        
        subjectHeader = caseAction + ' (C/P)';
        
        if(String.isEmpty(parentEm.Subject)){
            parentEm.Subject = '[No Subject]';
        }
        
        if(parentEm.Subject.toUpperCase().startsWith('RE:') || parentEm.Subject.toUpperCase().startsWith('FW:')){                  
            emailSubject = 'RE:' + parentEm.Subject.right(parentEm.subject.length() - 3);
        }
        else{
            emailSubject = 'RE: ' + parentEm.Subject;
        }
        
        ccWrappers.clear();
        bccWrappers.clear();
        freeFormCC = '';
        freeFormBCC = '';
        if(parentEm.Incoming){
            try{
                Contact c = [Select Id,Name,Email,Other_Email__c  FROM Contact WHERE Email =: parentEm.FromAddress OR Other_Email__c =: parentEm.FromAddress];
                
                if(c.Email == parentEm.FromAddress){
                    correctToAddress = c.Email;
                }
                else{
                    correctToAddress = c.Other_Email__c;
                }
                
                theCase.ContactId = c.Id;
                theCase.Contact = c;
            }
            catch(QueryException e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Email cannot be sent as there are no Contacts associated to this Email Address: ' + parentEm.FromAddress);
                ApexPages.addMessage(myMsg);
                theCase.ContactId = null;
                theCase.Contact = null;
            }
        }         
                
        if(!Test.isRunningTest()){
            fromAddress = [Select o.Id, o.DisplayName, o.Address From OrgWideEmailAddress o WHERE Id =: theCase.Org_Wide_Email_Address_ID__c];
        }
        else{
            fromAddress = [Select o.Id, o.DisplayName, o.Address From OrgWideEmailAddress o LIMIT 1];
        }
        
        wrapMap = new Map<Id,attachmentWrapper>();
        List<Attachment> parAtt = [SELECT Id FROM Attachment WHERE ParentId =: this.theCase.Id];
        startingAttachments = new Set<Id>();
        if(parAtt.size() > 0){
            for(Attachment a : parAtt){
                this.startingAttachments.add(a.Id);
            }
            this.hasParentAttachments = true;
        }
        }
        catch(exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Fatal Error: ' + e.getMessage());
            ApexPages.addMessage(myMsg);
            System.debug(e);
        }
 
        return null;
    }

    @testVisible
    private string generateBody(){
        string result;
        if(!String.isEmpty(parentEm.HTMLBody)){
            result = (caseAction + (String.isEmpty(this.theCase.Decline_Reason__c) ? '' : ('<br/>' + this.theCase.Decline_Reason__c)) + (String.isEmpty(this.theCase.Decline_Reason_Other__c) ? '' : ('<br/>' + this.theCase.Decline_Reason_Other__c)) +'<br/>' + getHeader() + parentEm.HTMLBody + '<br>' + theCase.Email_Ref_Id__c).escapeJava().replace('\'','&#39;');
        }
        else{     
            if(!String.isEmpty(parentEm.TextBody)){
                result = (caseAction + (String.isEmpty(this.theCase.Decline_Reason__c) ? '' : ('<br/>' + this.theCase.Decline_Reason__c))+(String.isEmpty(this.theCase.Decline_Reason_Other__c) ? '' : ('<br/>' + this.theCase.Decline_Reason_Other__c)) +'<br/>' + getHeader() +  parentEm.TextBody.escapeHTML4().replace('\n','<br>')  + '<br><br>' + theCase.Email_Ref_Id__c).escapeJava().replace('\'','&#39;');
            }
            else{
                result = (caseAction + (String.isEmpty(this.theCase.Decline_Reason__c) ? '' : ('<br/>' + this.theCase.Decline_Reason__c))+(String.isEmpty(this.theCase.Decline_Reason_Other__c) ? '' : ('<br/>' + this.theCase.Decline_Reason_Other__c)) +'<br/>' + getHeader() + '<br><br>' + theCase.Email_Ref_Id__c).escapeJava().replace('\'','&#39;');
            }
        }
        return result.replaceAll('<script\b[^>]*>(.*?)</script>',' ');
    }
    
    @testVisible
    private String getHeader(){
        /*
        --------------- Original Message ---------------
        From: Customer Service - Champion [champion.cs.qcy@gardnerdenver.com]
        Sent: 11/24/2015 2:51 PM
        To: christopher.knitter@corvisa.com
        Cc: eric.stolzmann@gmail.com
        Subject: Case Resolved: Champion Case 00002401: [Email Subject]
        */
    
        String result = '<br>--------------- Original Message ---------------<br>';
        result += 'From: ' + (String.isEmpty(parentEm.FromName) ? '' : parentEm.FromName) + ' [' + parentEm.FromAddress+ ']<br>';
        result += 'Sent: ' + parentEm.MessageDate + '<br>';
        result += 'To: ' + (String.isEmpty(parentEm.ToAddress) ? '' : parentEm.ToAddress) + '<br>';
        if(!String.isEmpty(parentEm.CcAddress)){
            result += 'Cc: ' + parentEm.CcAddress + '<br>';
        }
        result += 'Subject: ' + parentEm.Subject + '<br><br>';
        return result;
    }

    public PageReference addBCC() {
        System.debug('addBCC() action');
        BCCWrappers.add(new addressWrapper());
        return null;
    }

    public PageReference addCC() {
        System.debug('addCC() action');
        ccWrappers.add(new addressWrapper());
        return null;
    }  

    public PageReference cancel() {
        this.isCancelled = true;
        PageReference parentPR;
        if(!String.isEmpty(parentEmailId)){
            parentPR = new PageReference('/apex/C_Email_Object?Id=' + parentEmailId);  
            parentPR.setRedirect(true); 
        }
        else{
            parentPR = new PageReference('/' + theCase.Id);  
            parentPR.setRedirect(true); 
        }
              
        return parentPR;
    }

    public PageReference send() {
        System.debug('SENDING EMAIL');
        EmailMessage em;
        if(editorContent != null){
            templatedValue = editorContent.escapeJava();
        }
        
        List<Id> ccIds = new List<Id>();
        List<Id> bccIds = new List<Id>();
        
        for(addressWrapper aw : ccWrappers){
            ccIds.add(aw.theWrap.ContactId);
        }
        
        for(addressWrapper aw : bccWrappers){
            bccIds.add(aw.theWrap.ContactId);
        }
        

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setWhatId(this.theCase.Id);
        mail.setHTMLBody(this.editorContent);
        mail.setPlainTextBody(this.editorContent.stripHtmlTags());
        mail.setSubject(this.emailSubject);
        mail.setOrgWideEmailAddressId(this.theCase.Org_Wide_Email_Address_ID__c);
        
        List<Contact> theCC_Contacts = [SELECT Email FROM Contact WHERE Id IN :ccIds];
        List<Contact> theBCC_Contacts = [SELECT Email FROM Contact WHERE Id IN :bccIds];
        
        List<String> ccAddresses = new List<String>();
        for(Contact c: theCC_Contacts){
            ccAddresses.add(c.Email);
        }
        
        if(!String.isEmpty(freeFormCC)){
            List<String> ffCC = freeFormCC.split(';');
            ccAddresses.addAll(ffCC);
        }
        
        List<String> bccAddresses = new List<String>();
        for(Contact c: theBCC_Contacts){
            bccAddresses.add(c.Email);
        }
        
        if(!String.isEmpty(freeFormBCC)){
            List<String> ffBCC = freeFormBCC.split(';');
            bccAddresses.addAll(ffBCC);
        }
        
        mail.setCcAddresses(ccAddresses);
        mail.setBccAddresses(bccAddresses);
        
        mail.setTargetObjectId(this.theCase.ContactId);
             
        List<Messaging.EmailFileAttachment> theAttachments = new List<Messaging.EmailFileAttachment>();
            
        List<Id> attachmentIds = new List<Id>();
            
        for(attachmentWrapper aw : attachmentsWrapper){
            if(aw.include){
                attachmentIds.add(aw.theAttachment.Id);               
             }
        }
        
        List<Attachment> attachments = [SELECT Id, Body, Name FROM Attachment WHERE Id IN :attachmentIds];
        
        for(Attachment a : attachments){
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setBody(a.Body);
            efa.setFileName(a.Name);
            theAttachments.add(efa);
        }
        
        if(theAttachments.size() > 0){
            mail.setFileAttachments(theAttachments);
        }
        
        try{
        System.debug(mail);
            Messaging.SendEmailResult[] theResult = Messaging.sendEmail(new Messaging.Email[]{mail}, true);
            System.debug('SENDING EMAIL');
            
            for(Messaging.SendEmailResult ser : theResult){
                System.debug(ser.isSuccess());
                if(!ser.isSuccess()){
                    for(Messaging.SendEmailError err : ser.getErrors()){
                        System.debug(err.getFields());
                        System.debug(err.getMessage());
                        System.debug(err.getStatusCode());
                    }
                }
            }
            
            Case caseTemp = new Case();            
            caseTemp.OwnerId = UserInfo.getUserId();
            if(isDeclined){
                caseTemp.Decline_Reason__c = this.theCase.Decline_Reason__c;
                caseTemp.Decline_Reason_Other__c = this.theCase.Decline_Reason_Other__c;
                caseTemp.Status = 'Declined';
            }
            else{
                caseTemp.Status = 'Open';
            }
            caseTemp.Full_Case_Description__c = this.editorContent;
            caseTemp.Id = this.theCase.Id;
            update caseTemp;
            
            
            
            PageReference parentPR = new PageReference('/' + theCase.Id);  
            parentPR.setRedirect(true);        
            return parentPR;
        }
        catch(exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage());
            ApexPages.addMessage(myMsg);
            System.debug(e);
        }

       return ApexPages.currentPage();
    }
    
    public void addNewAttachment(){
         
    }
    
    public PageReference sendContent () {
        this.templatedValue = editorContent;
        return null;
    }
    
    public class attachmentWrapper{
        private boolean first = true;
        protected C_CustomEmail_ManagedCare_Controller controller;
        public Attachment theAttachment {get; private set;}
        public Boolean include {get; 
            set{
                if(value == false && first){
                    this.first = false;
                }
                else{
                this.include = value;
                this.controller.wrapMap.put(this.theAttachment.Id,this);       
                }
            }
                               }
        
        public attachmentWrapper(Attachment a, Boolean inc, C_CustomEmail_ManagedCare_Controller con){
            this.controller = con;
            this.theAttachment = a;
            this.include = inc;           
        }
    }
    
    public class addressWrapper{
        public Case theWrap {get; set;}
        
        public addressWrapper(){
            this.theWrap = new Case();
        }
        
        public addressWrapper(Contact c){
            this.theWrap = new Case();
            this.theWrap.ContactId = c.Id;
        }
    }
  
    @RemoteAction 
    public static RemoteSaveResult saveImage (String filename, String imageBody) {
        
        // Get the ID of the folder we wish to keep these pasted images in. The folder should be public if images are to be viewed in external emails.
        Id attachmentsFolderId = [Select Id From Folder Where Name = 'Attachments' Limit 1][0].Id;
        
        // Create the document that keeps the image.
        Document doc = new Document();
        doc.FolderId = attachmentsFolderId;
        doc.Name = filename+'_'+DateTime.now();
        doc.Description = filename;
        doc.ContentType = 'image/png';
        doc.Type = 'png';
        doc.Body = EncodingUtil.base64Decode(imageBody);
        doc.IsPublic = true;
        
        // Save the document.
        Database.saveResult result = Database.insert(doc, false);
        
        // Create the URL to the image being saved. This should be org-agnostic.
        String baseOrgURL = System.URL.getSalesforceBaseUrl().toExternalForm();
        Integer firstIndex = baseOrgURL.indexOf('.')+1;
        Integer secondIndex = baseOrgURL.indexOf('.', firstIndex);
        String orgBase = baseOrgURL.substring(firstIndex, secondIndex);
        String contentBaseURL = 'https://c.'+ orgBase +'.content.force.com/servlet/servlet.ImageServer?id=';
        String docID = Test.isRunningTest() ? '123test' : ((String)doc.Id).substring(0, 15);
        String orgID = ((String)[Select Id, Name From Organization Limit 1][0].Id).substring(0, 15);
        String imageURL = contentBaseURL+docID+'&oid='+orgID;
        
        
        // Put the results of this operation in a RemoteSaveResult object.
        RemoteSaveResult newResult = new RemoteSaveResult();
        newResult.success = result.isSuccess();
        newResult.successMessage = result.isSuccess() ? imageURL : '';
        newResult.errorMessage = result.isSuccess() ? '' : result.getErrors()[0].getMessage();
        
        return newResult;
    } 
    
    public class RemoteSaveResult {
        public Boolean success;
        public String errorMessage;
        public String successMessage;
    }
}