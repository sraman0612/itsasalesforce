@istest
public class SFS_WOforNAStatusQueueableTest {

    @istest  public static void SFS_WOforNAStatusQueueableTest() {
	
    String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
    List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].CurrencyIsoCode='USD';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, true);
        //assetList[0].AccountId = accountList[1].Id;
        //insert assetList[0];
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
        svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';           
        svcAgreementList[0].SFS_Status__c = 'APPROVED';
        svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
        svcAgreementList[0].SFS_PO_Number__c = '2';
        svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
        svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList[0].SFS_Renewal_Escalator_Start_Date__c=system.today().addDays(-10);
        svcAgreementList[0].SFS_Recurring_Adjustment__c=3;
        svcAgreementList[0].ShippingHandling=2;
        svcAgreementList[0].SFS_Type__c='Rental';           
        insert svcAgreementList;
        Account billToAcc = SFS_TestDataFactory.getAccount();
        Update billToAcc;
        
        List<ContractLineItem> cLi=SFS_TestDataFactory.createServiceAgreementLineItem(1,svcAgreementList[0].Id,false);
        insert cLi[0];
        
        List<Entitlement> entitle = SFS_TestDataFactory.createEntitlement(1,accountList[0].Id,null,svcAgreementList[0].Id,cLi[0].Id,false);
        entitle[0].SFS_X1st_PM_Month__c = 'October';
        entitle[0].ServiceContractId = svcAgreementList[0].id;
        
        insert entitle;
        
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, accountList[0].Id,null,null, null, false);
        
        woList[0].SFS_Bill_To_Account__c = billToAcc.Id;
        
        Wolist[0].SFS_Invoice_Submitted__c=true;
        WoList[0].ServiceContractId=svcAgreementList[0].id;
        woList[0].SFS_Requested_Payment_Terms__c='NET 30';
        insert WoList[0];
        WoList[0].Status='N/A';
        
        woList[0].SFS_Integration_Status__c ='Approved';
        woList[0].SFS_Work_Order_Resubmit__c = true;
        
        Update WoList[0];
          test.startTest();
         
         SFS_WOforNAStatusQueueable que = new SFS_WOforNAStatusQueueable(WoList);
         system.enqueueJob(que); 
         test.stopTest();
         //system.assertEquals(true,woList[0].SFS_Integration_Status__c!=null);
     }
}