/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 20/06/2022
* @description: Test class for SFS_warrantyClaimOutboundHandler

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

===========================================================================================================*/
@isTest

public class SFS_warrantyClaimOutboundHandler_Test {
    
    @isTest
    public static void warrantyClaimOutboundHandlerTestMethod(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponseWarrantyClaim();
     //Try{    
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        Account billToAcc = SFS_TestDataFactory.getAccount();
        billToAcc.RecordTypeId = accRecID;
        Update billToAcc;
        Account acc = SFS_TestDataFactory.getAccount();
        acc.Bill_To_Account__c=billToAcc.Id;
        acc.Type ='Prospect';
        Update acc;
        
       /* List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = acc.Id;
        insert assetList[0];*/
        
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,false);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='00250506';
        update proList[0];
        
        DateTime cDT = system.Now();  
        List<OperatingHours> opHoursList=new List<OperatingHours>();
        OperatingHours opHours=new OperatingHours();
        opHours.Name = 'Test Operating Hours';
        opHours.TimeZone = UserInfo.getTimeZone().getID();
        opHoursList.add(opHours);
        insert opHoursList[0];
   
        
        List<ServiceContract> svc = SFS_TestDataFactory.createServiceAgreement(1, acc.Id,true);
        List<ContractLineItem> cliRec = SFS_TestDataFactory.createServiceAgreementLineItem(1, svc[0].Id,true);
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
        insert lc; 
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acc.Id,loc[0].Id, div[0].Id, null, false);
        WoList[0].SFS_Bill_To_Account__c = billToAcc.Id;
        WoList[0].SFS_Requested_Payment_Terms__c = 'NET 30';
        WoList[0].SFS_PO_Number__c = '1';
        WoList[0].SFS_Travel_Time__c =10.00;
        WoList[0].SFS_Job_Code__c = '1';
        insert WoList[0];
        WoList[0].Status='Open';
        Update WoList[0];
        
        
        String dayOfWeek = cDT.format('EEEE'); 
        Time timeStart = Time.newInstance(Integer.valueOf('04'),Integer.valueOf('30'),0,0);
        Time timeEnd = Time.newInstance(Integer.valueOf('07'),Integer.valueOf('30'),0,0);
        List<TimeSlot> timeSloList=new List<TimeSlot>();
        TimeSlot timeslt=new TimeSlot();
        timeslt.StartTime=timeStart;
        timeslt.EndTime=timeEnd;
        timeslt.DayOfWeek=dayOfWeek;
        timeslt.OperatingHoursId = opHoursList[0].Id;
        timeSloList.add(timeslt);
        insert timeSloList;
        
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,true);
      //  insert workTypeList[0];
        
        
        List<WorkOrderLineItem> woliList = SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,workTypeList[0].Id,false);
        //woliList[0].AssetId = assetList[0].Id;
        insert woliList[0];
        
        List<Expense> Exp = SFS_TestDataFactory.createExpense(1, woList[0].Id, woliList[0].id, true);
        
        Blob b = Blob.valueOf('Test Data');
        
        ContentVersion content=new ContentVersion(); 
        content.Title='Header_Picture1'; 
        content.PathOnClient='/' + content.Title + '.jpg'; 
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
        content.VersionData=bodyBlob;
        content.origin = 'H';
        insert content;
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=woliList[0].Id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
        contentlink.ShareType = 'I';
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
        Test.startTest();
        ServiceTerritory st = new ServiceTerritory();
        st.Name ='Test territory';
        st.OperatingHoursId = opHoursList[0].Id;
        st.IsActive = true;
        st.SFS_External_Id__c ='123';
        insert st;
        
        PermissionSetGroup Pdg = [SELECT Id  from PermissionSetGroup where DeveloperName='SFS_Service_Technician'];
        User usr1 = SFS_TestDataFactory.createTestUser('System Administrator', False);
        usr1.Username = 'managersfs@testuser.com';
        insert usr1;
        
        lc.IsInventoryLocation = true;
        lc.IsMobile = true;
        update lc;
        
        ServiceResource sr=new ServiceResource();
        sr.IsActive = true;
        sr.Name = 'test';
        sr.LocationId = lc.Id;
        sr.ResourceType = 'T';
        sr.RelatedRecordId = usr1.Id;
        insert sr; 
        
     

        ServiceTerritoryMember stm = new ServiceTerritoryMember();
        stm.ServiceResourceId = sr.Id;
        stm.ServiceTerritoryId = st.Id;
        stm.TerritoryType = 'P';
        stm.EffectiveStartDate = cDT;
        insert stm;
        
        /*List<ServiceAppointment> saList=new List<ServiceAppointment>();
        ServiceAppointment appointment=new ServiceAppointment();
        appointment.ParentRecordId=woliList[0].Id;
       // appointment.Status='none';
        appointment.ServiceTerritoryId = st.Id;  
        appointment.Work_Order__c = WoList[0].id;
        appointment.EarliestStartTime = cDT;
        appointment.DueDate = cDT;
        appointment.SchedStartTime = cDT;
        appointment.SchedEndTime = cDT;
        saList.add(appointment);
        insert appointment;
        
               
        AssignedResource ar = new AssignedResource();
        ar.ServiceAppointmentId = saList[0].Id;
        ar.ServiceResourceId = sr.Id;
        insert ar; 
      

        appointment.Status = 'Dispatched';
        update appointment;
        
                    
        appointment.status = 'In Progress';
        update appointment;
        
        appointment.status = 'Completed';
        update appointment;
        
        system.debug('appointment*********'+appointment);

        */
        Warranty_Claim__c wa =  new Warranty_Claim__c();
        wa.SFS_Work_Order__c = WoList[0].id;
        wa.Name = 'test';
        insert wa;
      
            
        List<Id> waIds = new List<Id>{wa.id};
           
        SFS_warrantyClaimOutboundHandler.Run(waIds);
        SFS_warrantyClaimOutboundHandler.getClaimSubmissionResponse(res,wa.id);
        
        Test.stopTest();
//     }
       /* catch(exception ex){
            system.debug('@@exception'+ ex.getMessage());
            system.debug('@@exception line number'+ ex.getLineNumber());
        }  */    
        
    }
}