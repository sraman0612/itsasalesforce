/// enosiX Inc. Generated Apex Model
/// Generated On: 12/18/2019 9:43:17 AM
/// SAP Host: From REST Service On: https://gdi--UAT.my.salesforce.com
/// CID: From REST Service On: https://gdi--UAT.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
public with sharing class TST_EnosixServNotifUpdate_Detail
{
    public class MockSBO_EnosixServNotifUpdate_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { return null; }
    }

    @isTest
    static void testSBO()
    {
        SBO_EnosixServNotifUpdate_Detail sbo = new SBO_EnosixServNotifUpdate_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServNotifUpdate_Detail.class, new MockSBO_EnosixServNotifUpdate_Detail());
        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.command(null, null));
        System.assertEquals(null, sbo.getDetail(null));
        System.assertEquals(null, sbo.save(null));
    }

    @isTest
    static void testEnosixServNotifUpdate()
    {
        SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate result = new SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate();
        System.assertEquals(SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate.class, result.getType(), 'getType() does not match object type.');

        result.registerReflectionForClass();

        result.SalesDocument = 'X';
        System.assertEquals('X', result.SalesDocument);

        //Test child collections
    }

}