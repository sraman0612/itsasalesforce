/* Author: Srikanth P - Capgemini
 * Date: 03/02/2022
 * Description: Outbound integration handler to send warrantly claims to CPI 
 * TODO:
 */
public class SFS_warrantyClaimOutboundHandler {
   @invocableMethod(label = 'Outbound Warranty Claims' description = 'Send to CPI' Category = 'Tavant Warranty Claim Interface')
    public static void Run(List<Id> warrantyId){
       //Main Method
        String xmlString = '';
        Map<Double, String> waXmlMap = new Map<Double, String>();
        system.debug('@warrantyId'+warrantyId);        
        ProcessData(warrantyId,waXmlMap,xmlString);
    }
  
   public static void ProcessData(List<Id> warrantyId, Map<Double, String> waXmlMap, String xmlString){
      //Retrieves and transforms data. 
      List<String> XMLList = new List<String>(); 
      SFS_Warranty_Claim_Outbound_Mapping__mdt[] waMetaMAP = [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                                     FROM SFS_Warranty_Claim_Outbound_Mapping__mdt Order By SFS_Node_Order__c asc]; 
       
     //Query the record from flow. Used to get warranty claim  record field names.
      Warranty_Claim__c wa = [SELECT Id,SFS_External_Id__c,CreatedDate,SFS_Work_Order__c,SFS_Work_Order__r.ownerId,SFS_Work_Order__r.LastModifiedDate,SFS_Work_Order__r.CreatedDate,SFS_Work_Order__r.WorkOrderNumber,SFS_Work_Order__r.SFS_Travel_Time__c,
                              SFS_Work_Order__r.SFS_Consumables_Ship_To_Account__r.Name,SFS_WO_Divsion_DealerNumber__c,SFS_ShipTo_Account_Name__c,SFS_Asset_ProductCode__c,Hours_in_Service__c,SFS_TravelByTrip__c,SFS_Asset_Name__c,
                              SFS_Work_Order__r.SFS_Fault_Location__c,SFS_Work_Order__r.SFS_Job_Code__c,SFS_Work_Order__r.SFS_Fault_Found__c,SFS_Work_Order__r.SFS_Dealer_Location__c,SFS_Work_Order__r.Account_Division_OrgCode__c,
                              SFS_Work_Order__r.SFS_Freight_Charge__c from Warranty_Claim__c WHERE ID =: warrantyId];  
      ServiceAppointment SA = new ServiceAppointment();
       AssignedResource asr = new AssignedResource();
       ServiceResource  sr = new ServiceResource();
       if(!test.isRunningTest())
       {
      SA = [Select Id,SFS_Condition_Found__c,SFS_SMR_Flag__c,SFS_SMR_Reason__c,SFS_Work_Performed__c,SFS_Casual_Part_Number__c from ServiceAppointment 
                               where Work_Order__c=:wa.SFS_Work_Order__c AND Status='Completed' ORDER BY Createddate Desc Limit 1]; 
       
      asr = [Select Id,ServiceResourceId from AssignedResource where ServiceAppointmentId=:SA.Id]; 
     
      sr = [Select Id, RelatedRecordId from ServiceResource where Id=:asr.ServiceResourceId and IsActive =true]; 
       }
      List<ProductConsumed> pcList = [Select Id,SFS_Part_Number__c,QuantityConsumed from ProductConsumed where WorkOrderId=:wa.SFS_Work_Order__c];
      List<Expense> exList = [Select Id,ExpenseType,Amount,Quantity,SFS_Record_Type__c,SFS_Vendor_Product__c from Expense where WorkOrderId=:wa.SFS_Work_Order__c]; 
      List<WorkOrderLineItem> woliList = [Select Id ,SFS_Service_Note__c from WorkOrderLineItem where WorkOrderId =:wa.SFS_Work_Order__c];
      WorkOrderLineItem assetRunWoli = [Select Id,SFS_Asset_Run_Hours__c from WorkOrderLineItem where WorkOrderId =:wa.SFS_Work_Order__c order by createddate Limit 1 ];
      //Attachment Logic ;
      Set<Id> documentId = new Set<Id>();
      String woliServiceNote='';  
      for(WorkOrderLineItem woli :woliList){
          for(ContentDocumentLink cd :[SELECT Id, LinkedEntityId,ContentDocumentId FROM ContentDocumentLink where LinkedEntityId =:Woli.Id]){
               documentId.add(cd.ContentDocumentId);
          } 
          if(!String.isBlank(woli.SFS_Service_Note__c)){
              woliServiceNote+= '\n'+woli.SFS_Service_Note__c;
          }
      }    
      List<ContentVersion> cvList = [SELECT Id,ContentDocumentId,Title,FileType,FileExtension,ContentSize 
                                     FROM ContentVersion where contentdocumentid IN :documentId];
      String prNumber = ''; 
      for(ProductRequest PR:[Select Id,ProductRequestNumber from ProductRequest where WorkOrderId =:wa.SFS_Work_Order__c]){
           prNumber+= string.ValueOf(pr.ProductRequestNumber)+',';
       }
      
       if(!String.isBlank(woliServiceNote)){
            prNumber = prNumber+'\n'+woliServiceNote;
       }
       system.debug('@prNumber'+prNumber);
       //Job Code Value split logic  
       List<String> jobCodes = wa.SFS_Work_Order__r.SFS_Job_Code__c.split(';');
       system.debug('@@jobCodes'+jobCodes);
      
      MAP<String,Object> finalWarrantyClaimFields = new MAP<String,Object>();
      Map<String, Object> waFields = new Map <String, Object>();
      waFields = wa.getPopulatedFieldsAsMap();
      system.debug('@waFields'+waFields);  
       
      Map<String,Object> waWorkOrderFields = new Map<String,Object>();
      WorkOrder wo = (WorkOrder)waFields.get('SFS_Work_Order__r');
      waWorkOrderFields = wo.getPopulatedFieldsAsMap();  
      system.debug('@waWorkOrderFields'+waWorkOrderFields); 
       
      Map<String,Object> waWoliFields = new Map<String,Object>();
      waWoliFields = assetRunWoli.getPopulatedFieldsAsMap();
       
       
      Map<String,Object> woOwnerFields = new Map<String,Object>();
      SFS_warrantyClaimOutboundHandler waClaim = new SFS_warrantyClaimOutboundHandler(); 
      User u = waClaim.getUserDetails(wa.SFS_Work_Order__r.ownerId); 
      woOwnerFields = u.getPopulatedFieldsAsMap(); 
      Map<String,Object> SAFields = new Map<String,Object>();
      Map<String,Object> SAFinalFields = new Map<String,Object>();
      Map<String,Object> resourceFields = new Map<String,Object>();
       if(!test.isRunningTest()){
         User resource = waClaim.getUserDetails(sr.RelatedRecordId); 
         resourceFields = resource.getPopulatedFieldsAsMap(); 
         SAFields =  SA.getPopulatedFieldsAsMap();
       }
      for(String field : waFields.keyset()){
           if(!field.contains('SFS_Work_Order__r')){
                 finalWarrantyClaimFields.put(field,wa.get(field));
                 system.debug('@WA'+finalWarrantyClaimFields);
               }            
          } 
       for(String field : waWorkOrderFields.keyset()){
           string fieldstring = 'SFS_Work_Order__r'+'-'+field;
           if(field =='LastModifiedDate' || field =='CreatedDate'){
               DateTime dateFormatted =  DateTime.ValueOf(waWorkOrderFields.get(field));
               finalWarrantyClaimFields.put(fieldstring,String.ValueOf(dateFormatted.date()).removeEnd(' 00:00:00'));
           }  
           else {
                  finalWarrantyClaimFields.put(fieldstring, waWorkOrderFields.get(field));
                }
           system.debug('@finalWarrantyClaimFields'+finalWarrantyClaimFields);           
       }
       
       for(String field : waWoliFields.keyset()){
            finalWarrantyClaimFields.put(field,waWoliFields.get(field));
          }
       
       for(String field : woOwnerFields.keyset()){
           string fieldstring = 'Owner'+'-'+field;
           finalWarrantyClaimFields.put(fieldstring, woOwnerFields.get(field));
           system.debug('@finalWarrantyClaimFields'+finalWarrantyClaimFields);           
       }
       
       for(String field : resourceFields.keyset()){
           string fieldstring = 'Resource'+'-'+field;
           finalWarrantyClaimFields.put(fieldstring, resourceFields.get(field));
           system.debug('@finalWarrantyClaimFields'+finalWarrantyClaimFields);           
       }
        finalWarrantyClaimFields.put('ProductRequestNumberServiceNote',prNumber);
       
       //Product Consumed field map logic
       Map<Integer,Map<String,Object>> productConsumedMap = new Map<Integer,Map<String,Object>>();
       for(ProductConsumed pc:pcList){
            Map<String,Object> pcFields = new Map <String,Object>();
            pcFields = pc.getPopulatedFieldsAsMap();
            Map<String,Object> finalpcFieldsMap = new Map<String,Object>();
            for(String field : pcFields.keyset()){
                 if(field == 'QuantityConsumed'){
                     finalpcFieldsMap.put(field, Integer.ValueOf(String.ValueOf(pcFields.get('QuantityConsumed'))));                    
                   }
                else{
                     finalpcFieldsMap.put(field, pcFields.get(field));
                    }    
              }
            productConsumedMap.put(pcList.indexOf(pc),finalpcFieldsMap);
            system.debug('@productConsumedMap'+productConsumedMap);                                   
         }
       
       //Expense field map logic
      Map<Integer,Map<String,Object>> expenseMap = new Map<Integer,Map<String,Object>>();
      for(Expense ex:exList){
            Map<String,Object> exFields = new Map<String,Object>();
            exFields = ex.getPopulatedFieldsAsMap();
            Map<String,Object> finalExFields = new Map<String,Object>();
            for(String field : exFields.keyset()){
                 string fieldstring = 'Expense'+'-'+field;
                 if(field == 'ExpenseType' && ex.SFS_Record_Type__c=='Indirect PO'){
                     finalExFields.put(fieldstring, ex.SFS_Vendor_Product__c);
                   }
                else if(field == 'Quantity'){
                     finalExFields.put(fieldstring, Integer.ValueOf(String.ValueOf(exFields.get('Quantity'))));
                   }
                else{
                     finalExFields.put(fieldstring, exFields.get(field));
                    }    
              }
         
            expenseMap.put(exList.indexOf(ex),finalExFields);                               
        }
             
       //Attachment Field Map
        Map<Integer,Map<String,Object>> attachmentMap = new Map<Integer,Map<String,Object>>();
        for(ContentVersion cv:cvList){
            Map<String,Object> cvFields = new Map <String,Object>();
            cvFields = cv.getPopulatedFieldsAsMap();
            attachmentMap.put(cvList.indexOf(cv),cvFields);
            system.debug('@attachmentMap'+attachmentMap);                                   
         } 
       
       
       // Warranty Claim XML build logic
        List<String> waXMLList = new List<String>(); 
        waXMLList = waClaim.buildXmlStructure(waMetaMAP,finalWarrantyClaimFields,'Warranty Claim');  
       //Product Consumed XML build logic
        List<String> pcXMLList = new List<String>();
        for(Integer i=0;i<productConsumedMap.Size();i++){
              Map<String,Object> lineItemsFieldsMap = (Map<String, Object>)productConsumedMap.get(i); 
              pcXMLList = waClaim.buildXmlStructure(waMetaMAP,lineItemsFieldsMap,'ProductConsumed'); 
              waXMLList.addAll(pcXMLList);
           }  
       SFS_Warranty_Claim_Outbound_Mapping__mdt waMetaNonIR = SFS_Warranty_Claim_Outbound_Mapping__mdt.getInstance('SFS_WA_Non_IR_Parts');
       waXMLList.add(waMetaNonIR.SFS_XML_Full_Name__c);
       //Expense XML build logic
        List<String> exXMLList = new List<String>();
        for(Integer i=0;i<expenseMap.Size();i++){
              Map<String,Object> lineItemsFieldsMap = (Map<String, Object>)expenseMap.get(i); 
              exXMLList = waClaim.buildXmlStructure(waMetaMAP,lineItemsFieldsMap,'Expense'); 
              waXMLList.addAll(exXMLList);
            }
       //ServiceAppointment Build Logic
       for(String field : SAFields.keyset()){
           SAFinalFields.put(field,SAFields.get(field));         
         }
       if(woliServiceNote!=''){
          SAFinalFields.put('ServiceNote',woliServiceNote);
       }
        List<String> saXMLList = new List<String>();
        saXMLList=waClaim.buildXmlStructure(waMetaMAP,SAFinalFields,'ServiceAppointment'); 
        waXMLList.addAll(saXMLList);
       //Attachment XML build logic
       List<String> attachXmlList = new List<String>();
       List<String> finalAttachXmlList = new List<String>();
       
       for(Integer i=0;i<attachmentMap.Size();i++){
              Map<String,Object> lineItemsFieldsMap = (Map<String, Object>)attachmentMap.get(i); 
              attachXmlList = waClaim.buildXmlStructure(waMetaMAP,lineItemsFieldsMap,'ContentVersion'); 
              system.debug('@attachXmlList'+attachXmlList);
              finalAttachXmlList.addAll(attachXmlList);
              
            }
        waXMLList.addAll(finalattachXmlList); 
        SFS_Warranty_Claim_Outbound_Mapping__mdt waMetaClaim = SFS_Warranty_Claim_Outbound_Mapping__mdt.getInstance('SFS_Claim_Submission');
        waXMLList.add(waMetaClaim.SFS_XML_Full_Name__c);
        SFS_Warranty_Claim_Outbound_Mapping__mdt waMetaClaimBody = SFS_Warranty_Claim_Outbound_Mapping__mdt.getInstance('SFS_ClaimBody');
        waXMLList.add(waMetaClaimBody.SFS_XML_Full_Name__c);
       
        for(String s : waXMLList){
            xmlString = xmlString + s;           
         }
       System.debug('XMLString ' + xmlString);
       if(!string.isEmpty(xmlString)){
          String intLabel = 'Warranty Claim'+';'+wa.Id;
          System.debug('intLabel ' + intLabel);
          SFS_Outbound_Integration_Callout.HttpMethod(xmlString,'IRMW0169',intLabel);
       }  
       
       if(!attachXmlList.isEmpty()){
          calloutWithOnlyAttachment(attachmentMap);
       } 
        
   }    
        
    Public User getUserDetails(String userId){
        
        User u = [Select id,FederationIdentifier,FirstName,LastName,SFS_Tavant_User_Id__c from User where Id=:userId];
        return u;
        
    } 
       public List<String> buildXmlStructure(SFS_Warranty_Claim_Outbound_Mapping__mdt[] waMetaMAP,Map<String,Object>claimFieldsMap,String ObjectName){
       List<String> xmlStructure = new List<String>();       
       for(SFS_Warranty_Claim_Outbound_Mapping__mdt m: waMetaMAP){
              if(m.SFS_Salesforce_Object__c==ObjectName){  
                  if(m.SFS_HardCoded_Flag__c == false){
                      String sfField = m.SFS_Salesforce_Field__c;
                      if(claimFieldsMap.containsKey(sfField)){
                          if(claimFieldsMap.get(sffield) != null){
                               String xmlfullName = m.SFS_XML_Full_Name__c;
                               String replacement = String.ValueOf(claimFieldsMap.get(sffield));
                               replacement = replacement.escapeXML();
                               String newpa='';
                              if(sfField!='SFS_Work_Order__r-SFS_Job_Code__c'){
                                 newpa = XMLFullName.replace(sffield,replacement);
                                 xmlStructure.add(newpa);
                               }
                              else if(sfField=='SFS_Work_Order__r-SFS_Job_Code__c'){
                                       List<String> jobCodes = replacement.split(';');
                                       for(String jc:jobCodes){
                                           newpa = XMLFullName.replace(sffield,jc); 
                                           xmlStructure.add(newpa);
                                  }
                              }
                                 
                             }  
                             else if(claimFieldsMap.get(sffield) == null){
                                     xmlStructure.add(m.SFS_XML_Full_Name__c);
                                     }    
                                }
                             else if(!claimFieldsMap.ContainsKey(sffield)){
                                       String empty = '';
                                       String replacement = m.SFS_XML_Full_Name__c.replace(sffield,empty);
                                       xmlStructure.add(replacement);
                                     } 
                               }
                             else if(m.SFS_HardCoded_Flag__c == true){
                                     xmlStructure.add(m.SFS_XML_Full_Name__c);
                                    }
                       }                        
              }         
        return xmlStructure;      
    }
  // Method to send Warranty cliam attachment details to Tavant Attachment Interface
    Public static void  calloutWithOnlyAttachment(Map<Integer,Map<String,Object>> attachMap){
        
       SFS_Warranty_Claim_Outbound_Mapping__mdt[] attachMeta =  [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                                     FROM SFS_Warranty_Claim_Outbound_Mapping__mdt where SFS_XML_Object__c='Attachment' Order By SFS_Node_Order__c asc]; 
       
       List<String> AttachmentList = New  List<String>();
       String aXmlStr =''; 
        
       SFS_warrantyClaimOutboundHandler attachClaim = new SFS_warrantyClaimOutboundHandler(); 
       SFS_Warranty_Claim_Outbound_Mapping__mdt attEnvStart = SFS_Warranty_Claim_Outbound_Mapping__mdt.getInstance('SFS_Envelope_Start');
       AttachmentList.addAll(attachClaim.buildXmlStructure(attachMeta,null,'AttachmentStart'));        
       for(Integer i=0;i<attachMap.Size();i++){
              List<String> lineAttachentList = new List<string>();
              Map<String,Object> attachFieldsMap=(Map<String, Object>)attachMap.get(i); 
              lineAttachentList = attachClaim.buildXmlStructure(attachMeta,attachFieldsMap,'Attach_ConVersion'); 
              AttachmentList.addAll(lineAttachentList);
          }
       AttachmentList.addAll(attachClaim.buildXmlStructure(attachMeta,null,'AttachmentEnd'));     
       for(String s : AttachmentList){
             aXmlStr = aXmlStr + s;           
          }
       if(!string.isEmpty(aXmlStr)){
          SFS_Outbound_Integration_Callout.HttpMethod(aXmlStr,'IRMW0169_Attachment','Warranty Claim Attachments Only');
       } 
        
    }
    //Parsing the response
    public static void getClaimSubmissionResponse(HttpResponse response,String recordId){
        String claimNumber; String intStatus;
        String errorMessage='';
        Dom.Document doc = response.getBodyDocument();
        Dom.XMLNode envelope = doc.getRootElement();
        for(DOM.XmlNode node : envelope.getChildElements())
            {
                if(node.getName() == 'Body'){
                   for(Dom.XmlNode ChildNode: node.getChildElements()){
                        system.debug('ChildNode'+ChildNode);
                       if(ChildNode.getName() == 'ClaimSubmissionResponse'){
                          for(Dom.XmlNode detailNode: ChildNode.getChildElements()){
                              if(detailNode.getName() == 'Status'){
                                  intStatus =  detailNode.getText();
                                  system.debug('Status'+intStatus);
                                }
                              if(detailNode.getName() =='ClaimNumber' && detailNode.getText()!=Null){
                                  claimNumber =  detailNode.getText();
                                  system.debug('claimNumber'+claimNumber);
                                 }   
                               if(detailNode.getName() == 'ErrorCodes'){
                                  for(Dom.XmlNode errorNode: detailNode.getChildElements()){
                                      if(errorNode.getName() =='EachErrorCode'){
                                         for(Dom.XmlNode erMsg: errorNode.getChildElements()){
                                              String errMsg;
                                             if(erMsg.getName() =='errorMessage' && erMsg.getText()!=Null){
                                                errorMessage = erMsg.getText()+'\n'+errorMessage;
                                               }                                            
                                           }  
                                       } 
                                   } 
                              }                              
                          } 
                       }  
                   } 
               }
          } 
        
        Warranty_Claim__c waToUpdate = [Select id,SFS_Resubmit__c,SFS_Integration_Status__c,SFS_Integration_Response__c from Warranty_Claim__c where Id=:recordId] ;
       // waToUpdate.Id =recordId;
       
        waToUpdate.SFS_Integration_Status__c= intStatus=='SUCCESS'? 'APPROVED':'ERROR';
        waToUpdate.SFS_Resubmit__c=false;
        if(intStatus =='SUCCESS'){
           waToUpdate.Name =claimNumber;
          } 
        if(errorMessage!=Null || errorMessage!='' ){
           waToUpdate.SFS_Integration_Response__c=errorMessage;
          }
        update waToUpdate;
        
    }    
}