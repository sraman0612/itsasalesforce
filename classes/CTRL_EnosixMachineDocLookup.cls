public with sharing class CTRL_EnosixMachineDocLookup
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_EnosixMachineDocLookup.class);

    private static final String ALL_DOCUMENT_CATEGORY = 'All Documents';
    private static final String QUOTE_DOCUMENT_CATEGORY = 'Quote Documents';
    private static final String SAP_DOCUMENT_CATEGORY = 'SAP Documents';
    private static final String GDINSIDE_DOCUMENT_CATEGORY = 'GDInside Documents';
    private static final String TCO_DOCUMENT_CATEGORY = 'TCO Document';
    private static final Integer GDINSIDE_TIMEOUT = 20000;

    public static Integer QUOTE_DOCUMENT_SOURCE = 0;
    public static Integer SAP_DOCUMENT_SOURCE = 1;
    public static Integer GDINSIDE_DOCUMENT_SOURCE = 2;
    public static Integer TCO_DOCUMENT_SOURCE = 3;

    @AuraEnabled
    public static UTIL_Aura.Response doSearch(String token, String quoteId) {
        DOCUMENT_RESPONSE response = new DOCUMENT_RESPONSE();

        response.documents = new List<DOCUMENT_ENTRY>();

        List<DOCUMENT_ENTRY> quoteDocuments = getQuoteDocuments(quoteId);

        if (quoteDocuments.size() > 0) {
            response.documents.addAll(quoteDocuments);
        }

        DOCUMENT_ENTRY tcoDocument = getTCODocument(quoteId);

        if (tcoDocument != null) {
            tcoDocument.documentCategories.add(TCO_DOCUMENT_CATEGORY);
            response.documents.add(tcoDocument);
        }

        List<String> quoteLines = getProductProductCodesFromQuoteLines(quoteId);

        for (String curQuoteLine : quoteLines) {
            try{
            List<DOCUMENT_ENTRY> gdInsideDocuments = getGDInsideDocuments(token, curQuoteLine);

            if (gdInsideDocuments != null && gdInsideDocuments.size() > 0) {
                Set<String> productCategoryTypeSet = new Set<String>();

                for (DOCUMENT_ENTRY cur : gdInsideDocuments) {
                    productCategoryTypeSet.addAll(cur.documentCategories);
                    cur.documentCategories.add(ALL_DOCUMENT_CATEGORY);
                    cur.documentCategories.add(curQuoteLine);
                    response.documents.add(cur);
                }
            }
            }
            catch(exception e){
                System.debug(e);
            }
        }

        /* GCC-212 disabling SAP documents*/
        /*
        for (String curQuoteLine : quoteLines) {
            List<DOCUMENT_ENTRY> sapDocuments = getSAPDocuments(curQuoteLine);

            if (sapDocuments.size() > 0) {
                response.documents.addAll(sapDocuments);
                Set<String> productCategoryTypeSet = new Set<String>();

                for (DOCUMENT_ENTRY cur : sapDocuments) {
                    productCategoryTypeSet.addAll(cur.documentCategories);
                    cur.documentCategories.add(ALL_DOCUMENT_CATEGORY);
                    cur.documentCategories.add(curQuoteLine);
                    response.documents.add(cur);
                }
            }
        }
		*/

        response = removeDuplicateDocs(response);

        response = generateAvailableProductCategories(response);


        System.debug('response.availableProductCategories.size()=' + response.availableProductCategories.size());

        return UTIL_Aura.createResponse(JSON.serialize(response));
    }

    private static DOCUMENT_ENTRY getTCODocument(String quoteId) {
        if (quoteId == null) {
            return null;
        }
        
        List<String> fields = new List<String>{'Comp_PDF_UID__c'};

        List<SBQQ__Quote__c> quotes = (List<SBQQ__Quote__c>)UTIL_SObject.getSObjectsByIds(new List<String>{quoteId}, fields);

        if (quotes == null || quotes.size() == 0 || quotes.get(0).Comp_PDF_UID__c == null || Integer.valueOf(quotes.get(0).Comp_PDF_UID__c) == 0) {
            return null;
        }

        DOCUMENT_ENTRY retVal = new DOCUMENT_ENTRY();

        retVal.description = 'Total Cost of Ownership Document';
        retVal.documentCategories = new List<String>();
        retVal.documentCategories.add(ALL_DOCUMENT_CATEGORY);
        retVal.source = TCO_DOCUMENT_SOURCE;
        retVal.id = quotes.get(0).Comp_PDF_UID__c;
        retVal.title = 'TCO Document';
        retVal.type = 'PDF';
        retVal.filename = 'TCO.PDF';

        return retVal;
    }

    @AuraEnabled
    public static UTIL_Aura.Response getScreenData(String quoteId) {
        SCREEN_DATA retVal = new SCREEN_DATA();

        retVal.sendToEmailAddresses = getSendToEmailAddresses(quoteid);
        retVal.documentSources = getDocumentSources(quoteId);
        retVal.quoteProducts = getQuoteProducts(quoteId);

        return UTIL_Aura.createResponse(JSON.serialize(retVal));
    }

    private static List<LABEL_VALUE_PAIR> getDocumentSources(String quoteId) {
        List<LABEL_VALUE_PAIR> retVal = new List<LABEL_VALUE_PAIR>();

        List<String> fields = new List<String>{'Comp_PDF_UID__c'};

        retVal.add(new LABEL_VALUE_PAIR('Quote Documents'));
        retVal.add(new LABEL_VALUE_PAIR('SAP Documents'));
        retVal.add(new LABEL_VALUE_PAIR('GDInside Documents'));
        
        return retVal;
    }

    private static List<LABEL_VALUE_PAIR> getSendToEmailAddresses(String quoteId) {
        List<LABEL_VALUE_PAIR> retVal = new List<LABEL_VALUE_PAIR>();
        
        if (quoteId == null) {
            return retVal;
        }

        SBQQ__Quote__c quote = [SELECT SBQQ__PrimaryContact__c from SBQQ__Quote__c where id = : quoteId LIMIT 1];

        if (quote.SBQQ__PrimaryContact__c != null) {
            Contact contact = [SELECT Name, Email from Contact where Id = : quote.SBQQ__PrimaryContact__c];

            if (contact != null) {
                retVal.add(new LABEL_VALUE_PAIR(contact.Name + ' <' + contact.Email + '>', contact.Email));
            }
        }
        
        return retVal;
    }

    private static List<LABEL_VALUE_PAIR> getQuoteProducts(String quoteId) {
        List<LABEL_VALUE_PAIR> retVal = new List<LABEL_VALUE_PAIR>();

        Map<String,String> productCodeToNameMap = getProductProductCodeAndNameFromQuoteLines(quoteId);

        for (String key : productCodeToNameMap.keySet()) {
            String name = productCodeToNameMap.get(key);
            retVal.add(new LABEL_VALUE_PAIR(name, key));
        }

        return retVal;
    }

    private static DOCUMENT_RESPONSE generateAvailableProductCategories(DOCUMENT_RESPONSE response) {
        response.availableProductCategories = new List<String>();
        response.availableProductCategories.add(ALL_DOCUMENT_CATEGORY);

        for (DOCUMENT_ENTRY curDoc : response.documents) {
            for(String curCategory : curDoc.documentCategories) {
                if (!response.availableProductCategories.contains(curCategory)) {
                    response.availableProductCategories.add(curCategory);
                }
            }
        }

        return response;
    }

    private static DOCUMENT_RESPONSE removeDuplicateDocs(DOCUMENT_RESPONSE response) {
        Map<String,DOCUMENT_ENTRY> responseMap = new Map<String,DOCUMENT_ENTRY>();

        for (DOCUMENT_ENTRY doc : response.documents) {
            responseMap.put(doc.filename+doc.description, doc);
        }

        response.documents.clear();
        response.documents.addAll(responseMap.values());

        return response;
    }

    public static List<DOCUMENT_ENTRY> getQuoteDocuments(String quoteId) {
        List<DOCUMENT_ENTRY> retVal = new List<DOCUMENT_ENTRY>();

        List<SBQQ__QuoteDocument__c> quoteDocs = [SELECT Name, SBQQ__AttachmentId__c, SBQQ__DocumentId__c, SBQQ__Version__c, SBQQ__OutputFormat__c FROM SBQQ__QuoteDocument__c where SBQQ__Quote__c = : quoteId order by SBQQ__Version__c];

        List<String> docIds = new List<String>();

        for (SBQQ__QuoteDocument__c quoteDoc : quoteDocs) {
            docIds.add(quoteDoc.SBQQ__DocumentId__c);
        }

        Map<String,FILE_SIZE> fileSizeMap = getDocumentSize(docIds);

        for (SBQQ__QuoteDocument__c quoteDoc : quoteDocs) {
            DOCUMENT_ENTRY cur = new DOCUMENT_ENTRY();
            retVal.add(cur);
            cur.documentCategories = new List<String>();
            cur.documentCategories.add(ALL_DOCUMENT_CATEGORY);
            cur.documentCategories.add(QUOTE_DOCUMENT_CATEGORY);

            cur.title = quoteDoc.Name;
            cur.Description = 'Quote Document version ' + quoteDoc.SBQQ__Version__c;
            String fileExtension = 'PDF';

            if (quoteDoc.SBQQ__OutputFormat__c == 'MS Word') {
                fileExtension = 'DOC';
            }

            cur.filename = quoteDoc.Name + '.' + fileExtension;
            cur.id = quoteDoc.SBQQ__DocumentId__c;
            cur.type = quoteDoc.SBQQ__OutputFormat__c;
            cur.source = QUOTE_DOCUMENT_SOURCE;
            cur.fileSize = fileSizeMap.get(quoteDoc.SBQQ__DocumentId__c);
            if (cur.fileSize != null) {
                cur.fileSizeString = cur.fileSize.fileSizeString;
            }
        }

        return retVal;
    }

    public static List<DOCUMENT_ENTRY> getSAPDocuments(String selectedQuoteLine) {
        logger.enterAura('getSAPDocuments', new Map<String, Object> {
            'selectedQuoteLine' => selectedQuoteLine
        });

        System.debug('selectedQuoteLine:' + selectedQuoteLine);

        List<DOCUMENT_ENTRY> retVal = new List<DOCUMENT_ENTRY>();

        retVal.addAll(getSAPDocumentsImpl(selectedQuoteLine));

        return retVal;
    }

    public static List<DOCUMENT_ENTRY> getSAPDocumentsImpl(String productCode) {
        logger.enterAura('getSAPDocumentsImpl', new Map<String, Object> {
            'productCode' => productCode
        });

        SBO_EnosixDocument_Search.EnosixDocument_SC searchContext = new SBO_EnosixDocument_Search.EnosixDocument_SC();
        searchContext.pagingOptions.pageNumber = 1;
        searchContext.pagingOptions.pageSize = 200;

        searchContext.SEARCHPARAMS.Material = productCode;

        SBO_EnosixDocument_Search sbo = new SBO_EnosixDocument_Search();

        try {
            searchContext = sbo.search(searchContext);
        } catch (Exception e) {
            UTIL_PageMessages.addExceptionMessage(e);
        } finally {
            logger.exit();
        }

        List<DOCUMENT_ENTRY> retVal = new List<DOCUMENT_ENTRY>();

        if (searchContext == null || searchContext.result == null) {
            return retVal;
        }

        List<SBO_EnosixDocument_Search.SEARCHRESULT> results = searchContext.result.getResults();

        System.debug('results.size() = ' + results.size());
        System.debug('searchContext.pagingOptions.totalRecords=' + searchContext.pagingOptions.totalRecords);
        

        Date today = Date.today();

        for (SBO_EnosixDocument_Search.SEARCHRESULT result : results) {
    System.debug('result.DocumentNumber=' + result.DocumentNumber);
    System.debug('result.DocumentVersion=' + result.DocumentVersion);
    System.debug('result.DocumentType=' + result.DocumentType);
    System.debug('result.DocumentDescription=' + result.DocumentDescription);
    System.debug('result.StartDate=' + result.StartDate);
    System.debug('result.EndDate=' + result.EndDate);
            if (result.StartDate <= today && today < result.EndDate) {
                DOCUMENT_ENTRY cur = new DOCUMENT_ENTRY();
                cur.documentCategories = new List<String>();
                retVal.add(cur);
                cur.Id = result.EnosixObjKey;
                cur.title = result.DocumentDescription;
                cur.filename = result.GdDocumentType + '-' + result.DocumentNumber + '-' + result.DocumentVersion + '.' + result.DocumentType;
                
                cur.documentCategories.add(ALL_DOCUMENT_CATEGORY);
                //cur.documentCategories.add(SAP_DOCUMENT_CATEGORY);
                cur.documentCategories.add(productCode);
                //cur.documentCategories.add(result.GdDocumentType);
                cur.source = SAP_DOCUMENT_SOURCE;
                cur.type = result.DocumentType;
                cur.description = 'GD Document Type: ' + result.GdDocumentType + ', Version: ' + result.DocumentVersion;
            }
        }

        return retVal;
    }

    public static List<DOCUMENT_ENTRY> getGDInsideDocuments(String token, String quoteLine) {
        List<DOCUMENT_ENTRY> retVal = new List<DOCUMENT_ENTRY>();

        Map<String,Object> filterCriteria = getGDInsideFilterCriteria(quoteLine);

        if (filterCriteria == null) {
            return retVal;
        }

        String filterCriteriaStr = null;

        filterCriteriaStr = generateFilterText(filterCriteria);

        if (filterCriteriaStr == null) {
            System.debug('filter criteria was null');
            return null;
        }

        System.debug('filterCriteriaStr=' + filterCriteriaStr);

        String response = callGDInside(token, filterCriteriaStr);
        System.debug('gdi response=' + response);

        if (response == null) {
            return null;
        }

        GDINSIDE_ASSET_RESPONSE gdiresponse = (GDINSIDE_ASSET_RESPONSE)System.JSON.deserialize(response, GDINSIDE_ASSET_RESPONSE.class);

        for (GDINSIDE_ASSET_ROW row: gdiresponse.data) {
            System.debug('row.public_download_url=' + row.public_download_url);
            DOCUMENT_ENTRY cur = new DOCUMENT_ENTRY();
            retVal.add(cur);
            cur.documentCategories = new List<String>();
            cur.documentCategories.add(ALL_DOCUMENT_CATEGORY);
            //cur.documentCategories.add(GDINSIDE_DOCUMENT_CATEGORY);
            cur.documentCategories.add(quoteLine);
            cur.downloadLink = row.public_download_url;
            cur.filterCriteria = filterCriteriaStr;

            cur.fileSize = getFileSize(row.file_size);
            if (cur.fileSize != null) {
                cur.fileSizeString = cur.fileSize.fileSizeString;
            }

            if (row.salesForceAttributes != null && row.salesForceAttributes.product_category != null) {
                for (String curCategory : row.salesForceAttributes.product_category) {
                    cur.documentCategories.add(curCategory);
                }
            }

            cur.title = row.title;
            cur.description = row.description;
            cur.filename = row.asset_file;
            cur.id = String.valueOf(row.id);
            String fileType = cur.filename.substring(cur.filename.length() - 3);
            cur.type = fileType.toUpperCase();
            cur.source = GDINSIDE_DOCUMENT_SOURCE;
        }

        return retVal;
    }

    public static String callGDInside(String token, String filterCriteriaStr) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
		request.setHeader('Authorization', 'Bearer ' + token);
        request.setHeader('Content-Type', 'application/json');
                           
        request.setEndpoint('https://www.gdinside.com/api/v1/assets');
        request.setMethod('GET');

        request.setTimeout(GDINSIDE_TIMEOUT);
        
        request.setBody(filterCriteriaStr);

        if (Test.isRunningTest()) {
            return TSTC_ENosixMachineDocLookup.getGDInsideResponse();
        }
        
        HttpResponse response = http.send(request);

        if (response.getStatusCode() != 200) {
            UTIL_PageMessages.addMessage('ERROR', 'Unable to get assets from GDInside. ' + response.getStatus());

            return null;
        }

        return response.getBody();
    }

    public static Map<String,Object> getGDInsideFilterCriteria(String quoteLine) {
        if (quoteLine == null) {
            return null;
        }

        Map<String,Object> retVal = new Map<String,Object>();

        List<Machine_Doc_Product_Filter_Mapping__mdt> mappings = [SELECT Product_Field__c, Filter_Field__c from Machine_Doc_Product_Filter_Mapping__mdt];

        Map<String, String> filterFieldMap = new Map<String,String>();

        for (Machine_Doc_Product_Filter_Mapping__mdt mapping : mappings) {
            filterFieldMap.put(mapping.Filter_Field__c, mapping.Product_Field__c);
        }

        Product2 product = null;
        
        try {
            product = [SELECT Id from Product2 where ProductCode = : quoteLine LIMIT 1];
        } catch (Exception e) {
            
        }

        if (product == null) {
            UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, 'No quote lines have been added to the current quote.');

            return null;
        }

        List<String> fields = new List<String>();

        for (String key: filterFieldMap.keySet()) {
            fields.add(filterFieldMap.get(key));
        }

        List<String> productIds = new List<String>();

        productIds.add(product.Id);

        List<Product2> products = (List<Product2>)UTIL_SObject.getSObjectsByIds(productIds, fields);

        if (products != null && products.size() == 1) {
            Product2 thisProduct = products.get(0);

            for (String key: filterFieldMap.keySet()) {
                
                String productField = filterFieldMap.get(key);
                Object fieldValue = thisProduct.get(productField);

                if (fieldValue != null) {
                    retVal.put(key, fieldValue);
                }
            }
        }

        return retVal;
    }

    public static List<String> getProductProductCodesFromQuoteLines(String quoteId) {
        List<String> retVal = new List<String>();

        List<Id> productIds = getProductIdsFromQuoteLines(quoteId);

        List<String> fields = new List<String> { 'ProductCode' };

        List<Product2> products = (List<Product2>)UTIL_SObject.getSObjectsByIds(productIds, fields);

        if (products != null && products.size() > 0) {
            for (Product2 product : products) {
                retVal.add(product.ProductCode);
            }
        }

        return retVal;
    }

    public static Map<String,String> getProductProductCodeAndNameFromQuoteLines(String quoteId) {
        Map<String,String> retVal = new Map<String,String>();

        List<SBQQ__QuoteLine__c> quoteLines = [SELECT SBQQ__Number__c, SBQQ__ProductCode__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: quoteId and SBQQ__ProductCode__c != null order by SBQQ__Number__c asc];

        for (SBQQ__QuoteLine__c quoteLine : quoteLines) {
            retVal.put(quoteLine.SBQQ__ProductCode__c, quoteLine.SBQQ__Number__c + ' ' + quoteLine.SBQQ__ProductCode__c);
        }

        return retVal;
    }

    public static List<Id> getProductIdsFromQuoteLines(String quoteId) {
        List<Id> productIds = new List<Id>();

        List<SBQQ__QuoteLine__c> quoteLines = [SELECT SBQQ__Product__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: quoteId];

        for (SBQQ__QuoteLine__c quoteLine : quoteLines) {
            productIds.add(quoteLine.SBQQ__Product__c);
        }

        return productIds;
    }

    public static String generateFilterText(Map<String,Object> filterMap) {
        String retVal = '{"withs": ["salesForceAttributes"], "filters": {"live": 1, "available_for_sales_force": 1, "sales_force_attribute": {';
        Boolean isFirst = true;

        for (String key: filterMap.keySet()) {
            if (isFirst) {
                isFirst = false;
            } else {
                retVal += ', ';
            }

            Object filterValue = filterMap.get(key);
            String filterValueStr = '';

            if (filterValue instanceof Decimal) {
                filterValueStr = String.valueOf(((Decimal)filterValue).intValue());
            } else {
                filterValueStr = String.valueOf(filterValue);
            }

            retVal += '"' + key + '": "' + filterValueStr + '"';
        }

        retVal += '}}}';

        return retVal;
    }
    
    public static TOKEN getGDIInsideToken(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        System.debug('we are in getToken');
        request.setEndpoint((String)UTIL_AppSettings.getValue('CTRL_EnosixMachineDocLookup.GDInside.getToken.EndPoint'));
        request.setMethod('GET');

        if (Test.isRunningTest()) {
            return new TOKEN();
        }
        
        HttpResponse response = http.send(request);

        TOKEN thetoken = (TOKEN)System.JSON.deserialize(response.getBody(), TOKEN.class);

        return thetoken;
    }

    @AuraEnabled
    public static UTIL_Aura.Response getToken(){
        TOKEN thetoken = getGDIInsideToken();

        return UTIL_Aura.createResponse(thetoken.access_token);
    }

    @AuraEnabled
    public static UTIL_Aura.Response doDownload(String token, String id, Integer source, String filename) {

        if (source == null) {
            return UTIL_Aura.createResponse(null);
        }

        if (source == QUOTE_DOCUMENT_SOURCE) {
            return doDownloadQuoteDocument(id, filename);
        }

        if (source == SAP_DOCUMENT_SOURCE) {
            return doDownloadSAP(id);
        }

        //shouldn't hit here anymore since this is a client side call to gdinside
        return doDownloadGDInside(id, filename, token);
    }

    private static List<Document> getDocumentsForQuoteDocuments(List<String> quoteDocumentIds) {
        return [SELECT Name, ContentType, Body FROM Document WHERE Id in : quoteDocumentIds];
    }

    public static UTIL_Aura.Response doDownloadQuoteDocument(String id, String filename) {
        List<Document> quoteDocuments = getDocumentsForQuoteDocuments(new String[] {id});

        if (quoteDocuments == null || quoteDocuments.size() == 0) {
            return UTIL_Aura.createResponse(null);
        }
        
        Document theDoc = quoteDocuments.get(0);

        return UTIL_Aura.createResponse(JSON.serialize(
            generateDocumentDownload(
                EncodingUtil.base64Encode(theDoc.Body), 
                theDoc.ContentType,
                filename)
            ));
    }

    public static Map<String,FILE_SIZE> getAttachmentSize(List<String> docIds) {
        Map<String,FILE_SIZE> retVal = new Map<String,FILE_SIZE>();

        List<Attachment> attachments = [SELECT Id, BodyLength from Attachment where Id in : docIds];

        System.debug('attachments.size()=' + attachments.size());
        for (Attachment attachment : attachments) {
            retVal.put(attachment.Id, getFileSize(attachment.BodyLength));
        }

        return retVal;
    }

    public static Map<String,FILE_SIZE> getDocumentSize(List<String> docIds) {
        Map<String,FILE_SIZE> retVal = new Map<String,FILE_SIZE>();

        List<Document> documents = [SELECT Id, BodyLength from Document where Id in : docIds];

        System.debug('documents.size()=' + documents.size());

        for (Document document : documents) {
            retVal.put(document.Id, getFileSize(document.BodyLength));
        }

        return retVal;
    }

    public static FILE_SIZE getFileSize(Integer fileSizeInBytes) {
        FILE_SIZE retVal = new FILE_SIZE();
        Boolean inKB = fileSizeInBytes < MEGABYTE_SIZE_IN_BYTES;

        Decimal rowSizeInBytes = (Decimal)fileSizeInBytes;

        retVal.fileSizeInBytes = fileSizeInBytes;
        retVal.fileSize = (inKB? rowSizeInBytes / KILOBYTE_SIZE_IN_BYTES : rowSizeInBytes / MEGABYTE_SIZE_IN_BYTES);
        retVal.fileSizeUnit = (inKB? 'KB' : 'MB');
        retVal.fileSizeString = retVal.fileSize.setScale(2) + retVal.fileSizeUnit;

        return retVal;
    }

    public static UTIL_Aura.Response doDownloadGDInside(String id, String filename, String token) {
        String endpoint = 'https://www.gdinside.com/api/v1/assets/' + id + '/download';

        Http http = new Http();
        HttpRequest request = new HttpRequest();
		request.setHeader('Authorization', 'Bearer ' + token);
                           
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        if (Test.isRunningTest()) {
            return UTIL_Aura.createResponse(null);
        }
        HttpResponse response = http.send(request); //must be < 6mb
        // If the request is successful, parse the JSON response.
        System.debug('response.getBody()=' + response.getBody());
        System.debug('response.getStatusCode()=' + response.getStatusCode());
        if (response.getStatusCode() == 200) {
            return UTIL_Aura.createResponse(JSON.serialize(generateDocumentDownload(
                EncodingUtil.base64Encode(response.getBodyasBlob()), 
                response.getHeader('Content-Type'),
                filename)));
        } else {
            System.debug('response.getStatus()=' + response.getStatus());
        }


        return UTIL_Aura.createResponse(null);
    }

    @AuraEnabled
    public static UTIL_Aura.Response doDownloadSAP(String EnosixObjKey) {
        if (EnosixObjKey == null || String.isBlank(EnosixObjKey)) {
            return UTIL_Aura.createResponse(null);
        }
        logger.enterAura('doSearch', new Map<String, Object> {
            'EnosixObjKey' => EnosixObjKey
        });

        SBO_EnosixDocument_Detail sbo = new SBO_EnosixDocument_Detail();

        SBO_EnosixDocument_Detail.EnosixDocument doc = null;

        try {
            doc = sbo.getDetail(EnosixObjKey);
        } catch (Exception e) {
            UTIL_PageMessages.addExceptionMessage(e);
        } finally {
            logger.exit();
        }
        if (doc == null) {
            return UTIL_Aura.createResponse(null);
        }

        List<SBO_EnosixDocument_Detail.ATTACHMENTS> attachments = doc.ATTACHMENTS.getAsList();

        System.debug('attachments.size()=' + attachments.size());

        if (attachments.size() == 0) {
            UTIL_PageMessages.addMessage('ERROR', 'No Attachments found for ' + doc.DocumentNumber);
            return UTIL_Aura.createResponse(null);
        }

        return UTIL_Aura.createResponse(JSON.serialize(generateDocumentDownload(
            attachments.get(0).FileB64String, 
            attachments.get(0).HTMLContentType, 
            attachments.get(0).FileName)));
    }

    @AuraEnabled
    public static UTIL_Aura.Response getRelatedEmailAddresses(String quoteId) {
        List<LABEL_VALUE_PAIR> retVal = new List<LABEL_VALUE_PAIR>();

        SBQQ__Quote__c quote = [SELECT SBQQ__PrimaryContact__c from SBQQ__Quote__c where id = : quoteId LIMIT 1];

        if (quote.SBQQ__PrimaryContact__c != null) {
            Contact contact = [SELECT Name, Email from Contact where Id = : quote.SBQQ__PrimaryContact__c];

            if (contact != null) {
                retVal.add(new LABEL_VALUE_PAIR(contact.Name + ' <' + contact.Email + '>', contact.Email));
            }
        }

        return UTIL_Aura.createResponse(JSON.serialize(retVal));
    }

    @AuraEnabled
    public static UTIL_Aura.Response doSendEmail(String quoteId, List<String> sendToEmailAddresses, List<String> ccEmailAddresses, Boolean includeme, String subject, String body, List<String> selectedRows) {

        String newbody = body;

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses(sendToEmailAddresses);
        if (ccEmailAddresses != null && ccEmailAddresses.size() > 0) {
            mail.setCcAddresses(ccEmailAddresses);
        }

        mail.setReplyTo(UserInfo.getUserEmail());
        mail.setSenderDisplayName(UserInfo.getName());
        mail.setSubject(subject);
        mail.setBccSender(includeme);
        mail.setUseSignature(false);
        //mail.setPlainTextBody(body);
        //mail.setTargetObjectId(quoteId);
        mail.setWhatId(quoteId);
        //mail.setWhatId(quoteId);

        List<String> quoteDocIds = new List<String>();
        Boolean firstGDInsideDocument = true;

        for (String cur : selectedRows) {
            List<String> row = cur.split(',');
            Integer source = Integer.valueOf(row.get(0));
            String id = row.get(1);
            String filename = row.get(2);

            System.debug('source=' + source);
            System.debug('id=' + id);

            if (source == QUOTE_DOCUMENT_SOURCE) {
                quoteDocIds.add(id);
            } else if (source == GDINSIDE_DOCUMENT_SOURCE) {
                if (firstGDInsideDocument) {
                    firstGDInsideDocument = false;

                    newbody += '\n<br>Please use the link(s) below to download additional documents.';
                }
                newbody += '\n<br><a href="' + id + '">' + filename + '</a>';
            }
        }

        mail.setHtmlBody(newbody);

        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();

        List<Document> quoteDocuments = getDocumentsForQuoteDocuments(quoteDocIds);

        for (Document doc : quoteDocuments) {
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName(doc.Name);
            efa.setBody(doc.Body);
            efa.setContentType(doc.ContentType);
            fileAttachments.add(efa);
        }

        if (!fileAttachments.isEmpty()) {
            System.debug('file attachments were not empty');
            mail.setFileAttachments(fileAttachments);
        } else {
            System.debug('drat');
        }

        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        } catch (Exception e) {
            System.debug('we have a message');
            UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, e.getMessage());
        }

        return UTIL_Aura.createResponse(null);
    }

    private static DOCUMENT_DOWNLOAD generateDocumentDownload(String base64String, String contenttype, String filename) {

        System.debug('contenttype=' + contenttype);
        DOCUMENT_DOWNLOAD retVal = new DOCUMENT_DOWNLOAD();

        retVal.HTMLContentType = contenttype;
        retVal.FileName = filename;
        retVal.FileB64String = base64String;

        return retVal;
    }

    public class DOCUMENT_RESPONSE {
        public List<String> availableProductCategories { get; set; }
        public List<DOCUMENT_ENTRY> documents { get; set; }
    }

    public class DOCUMENT_ENTRY {
        public String id { get; set; }
        public String title { get; set; }
        public String description { get; set; }
        public List<String> documentCategories { get; set; }
        public String filename { get; set; }
        public String downloadLink { get; set; }
        public String filterCriteria { get; set; }
        public String type { get; set; }
        public Integer source { get; set; }
        public FILE_SIZE fileSize { get; set; }
        public String fileSizeString { get; set; }
    }

    public class DOCUMENT_DOWNLOAD {
        public String HTMLContentType { get; set; }
        public String FileName { get; set; }
        public String FileB64String { get; set; }
    }

    public class GDINSIDE_ASSET_RESPONSE {
        public Integer current_page { get; set; }
        public List<GDINSIDE_ASSET_ROW> data { get; set; }
        //public Integer from { get; set; }  //reserved word
        public Integer last_page { get; set; }
        public String next_page_url { get; set; }
        public String path { get; set; }
        public Integer per_page { get; set; }
        public String prev_page_url { get; set; }
        //public Integer to { get; set; }  //reserved word
        public Integer total { get; set; }
    }

    public class GDINSIDE_ASSET_ROW {
        public Integer alert_restock_qty { get; set; }
        public Integer archive { get; set; }
        public String asset_file { get; set; }
        public String author { get; set; }
        public Integer available_for_sales_force { get; set; }
        public Integer contact_map { get; set; }
        public String created_at { get; set; }
        public String custom_download_link { get; set; }
        public Date date_published { get; set; }
        public String description { get; set; }
        public String document_number { get; set; }
        public String download_api_link { get; set; }
        public Integer file_size { get; set; }
        public String public_download_url { get; set; }
        public Date expiration_date { get; set; }
        public Integer fast_facts { get; set; }
        public Integer featured { get; set; }
        public String featured_expiration { get; set; }
        public Integer hardcopy_orderable { get; set; }
        public Integer id { get; set; }
        public String keywords { get; set; }
        public Integer last_modfified_by { get; set; }
        public Integer live { get; set; }
        public Date live_date { get; set; }
        public Integer max_order_qty { get; set; }
        public Integer memo { get; set; }
        public Integer minimum_quantity { get; set; }
        public Integer non_emailable { get; set; }
        public Integer non_printable { get; set; }
        public Integer not_downloadable { get; set; }
        public String notes { get; set; }
        public Integer pending_approval { get; set; }
        //public Integer public { get; set; }  //reserved word
        public Integer qty_available { get; set; }
        public Integer qty_on_hand { get; set; }
        public Integer robuschi { get; set; }
        public SALESFORCE_ATTRIBUTES salesForceAttributes { get; set; }
        public String thumbnail { get; set; }
        public String thumbnail_download_api_link { get; set; }
        public String title { get; set; }
        public String updated_at { get; set; }
        public Integer updated_thumbnail { get; set; }
        public String video_embed { get; set; }
        public String vimeo_id { get; set; }
    }

    public class SALESFORCE_ATTRIBUTES {
        public List<String> Horsepower_range { get; set; }
        public String pressure { get; set; }
        public List<String> MG4 { get; set; } 
        public List<String> product_category { get; set; } 
    }

    public class SEARCH_RESULTS {
        public TOKEN token { get; set; }
        public List<DOCUMENT_ROW> results { get; set; }
    }

    public class DOCUMENT_ROW {
        public String Id { get; set; }
        public String Type { get; set; }
        public String FileName { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
    }

    public class TOKEN {
        public String token_type {get; set;}
        public Integer expires_in {get; set;}
        public String access_token {get; set;}
        public String refresh_token {get; set;}
    }

    public static DECIMAL KILOBYTE_SIZE_IN_BYTES = 1024.0;
    public static DECIMAL MEGABYTE_SIZE_IN_BYTES = KILOBYTE_SIZE_IN_BYTES * 1024.0;

    public class FILE_SIZE {
        public Integer fileSizeInBytes { get; set; }
        public Decimal fileSize { get; set; }
        public String fileSizeUnit { get; set; }
        public String fileSizeString { get; set; }
    }

    public class LABEL_VALUE_PAIR {
        public String label { get; set; }
        public String value { get; set; }

        public LABEL_VALUE_PAIR(String label) {
            this.label = label;
            this.value = label;
        }

        public LABEL_VALUE_PAIR(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    public class SCREEN_DATA {
        List<LABEL_VALUE_PAIR> sendToEmailAddresses { get; set; }
        List<LABEL_VALUE_PAIR> documentSources { get; set; }
        List<LABEL_VALUE_PAIR> quoteProducts { get; set; }
    }
}