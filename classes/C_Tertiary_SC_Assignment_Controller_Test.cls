@isTest
private class C_Tertiary_SC_Assignment_Controller_Test {

    @testSetup
    static void setupData(){
    
        Id customerRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id serviceRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Service Center').getRecordTypeId();
    
        Account a1 = new Account();
        a1.Name = 'Customer';
        a1.shippingStreet = '123 Fake St';
        a1.shippingCity = 'Milwaukee';
        a1.shippingState = 'WI';
        a1.shippingPostalCode = '53211';
        a1.RecordTypeId = customerRTId;
        a1.Location__Latitude__s = 43.0389;
        a1.Location__Longitude__s = 87.9065;
        insert a1;
        
        Contact cont = new Contact();
        cont.firstName = 'Can';
        cont.lastName = ' Pango';
        cont.email = 'service-gdi@canpango.com';
        cont.AccountId = a1.Id;
        insert cont;
        
        Case cse = new Case();
        cse.AccountId = a1.Id;
        cse.Contact = cont;
        cse.ContactId = cont.Id;
        cse.Subject = 'Test Case';        
        insert cse;       
        
        Account a2 = new Account();
        a2.Name = 'Service Center';
        a2.billingStreet = '123 Fake St';
        a2.billingCity = 'Racine';
        a2.billingState = 'WI';
        a2.billingPostalCode = '53402';
        a2.RecordTypeId = serviceRTId;
        a2.Location__Latitude__s = 43.0389;
        a2.Location__Longitude__s = 88.9065;
        insert a2;
        
        Account a3 = new Account();
        a3.Name = 'Test Center';
        a3.billingStreet = '123 Main St';
        a3.billingCity = 'Racine';
        a3.billingState = 'WI';
        a3.billingPostalCode = '53404';
        a3.RecordTypeId = serviceRTId;
        a3.Location__Latitude__s = 43.0489;
        a3.Location__Longitude__s = 88.8065;
        insert a3;
        
        Google_Maps_Settings__c gms = new Google_Maps_Settings__c();
        gms.API_Access_Key__c = 'testKey123_abc';
        gms.name = 'Google Maps API Key';
        insert gms;
        
        Account_Association__c aa1 = new Account_Association__c();
        aa1.Customer__c = a1.id;
        aa1.Service_Center__c = a2.id;
        aa1.Active__c = true; 
        aa1.Priority__c = 'Primary';
        aa1.Hours__c = 'Business Hours';
        insert aa1;
        
        Account_Association__c aa2 = new Account_Association__c();
        aa2.Customer__c = a1.id;
        aa2.Service_Center__c = a2.id;
        aa2.Active__c = true;
        aa2.Priority__c = 'Secondary';
        aa2.Hours__c = 'After Hours';
        insert aa2;
        
        Account_Association__c aa3 = new Account_Association__c();
        aa3.Customer__c = a1.id;
        aa3.Service_Center__c = a3.id;
        aa3.Active__c = true;
        aa3.Priority__c = 'Secondary';
        aa3.Hours__c = 'Business Hours';
        insert aa3;
    
    }
    
    static testMethod void runCOR(){
    
        Account cust = [SELECT Id FROM Account WHERE Name = 'Customer'];
        Account sc = [SELECT Id FROM Account WHERE Name = 'Service Center'];
        Case c = [SELECT Id, AccountId, Account.Id FROM Case];
        
        Test.setMock(HttpCalloutMock.class, new COR_HTTP_Mock());

        Test.setCurrentPageReference(new PageReference('C_Tertiary_SC_Assignment_Controller'));
        ApexPages.StandardController scon = new ApexPages.StandardController(c);
        C_Tertiary_SC_Assignment_Controller sca = new C_Tertiary_SC_Assignment_Controller(scon);
        String strOut = sca.strOutputVariable;
        sca.strOutputVariable = strOut;
        PageReference prf = sca.prFinishLocation;
        sca.prFinishLocation = prf;
        System.currentPageReference().getParameters().put('CaseId', c.Id);
        System.currentPageReference().getParameters().put('AccountId', cust.Id);
        Case tmpCase = new Case();
        scon = new ApexPages.StandardController(tmpCase);
        sca = new C_Tertiary_SC_Assignment_Controller(scon);
        Test.startTest();
        sca.getaccountList();
        sca.getNearbyAccounts();
        System.debug(sca.accountsOrderedByDistance);
        
        sca.accountsOrderedByDistance[1].Account.Tertiary_Disposition__c = 'Declined';
        
        sca.saveRecords();
        
        sca.accountsOrderedByDistance[0].Account.Tertiary_Disposition__c = 'Accepted';
        sca.saveRecords();
        
        Test.stopTest();
    }
    
}