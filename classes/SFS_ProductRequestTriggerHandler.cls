/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 07/12/2021
* @description: Apex Handler class for SFS_ProductRequestTrigger
* @Story Number: SIF-38

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

===============================================================================================================================*/

public class SFS_ProductRequestTriggerHandler {
    
    //SIF- 38 Logic to create Product transfer, product consumed records
    public static void createProductTransferAndConsumed(List<ProductRequest> prList,Map<Id,ProductRequest> prMap){
        Map<Id,ProductRequest> prAutoReceiveMap = new Map<Id,ProductRequest>();
        Boolean createPTandPC = false;
        for(ProductRequest pr :prList){
            
            if(pr.SFS_Manually_Received__c == true)
            {
                createPTandPC = true;
                
            }
            productRequest oldPr = prMap.get(pr.Id);
            if(pr.SFS_Auto_Receive__c && pr.SFS_Auto_Receive__c!=oldPr.SFS_Auto_Receive__c && (pr.WorkOrderId!=null || pr.WorkOrderLineItemId !=null) && pr.SFS_Destination_Location_Id__c !=null){
                prAutoReceiveMap.put(pr.Id,Pr); 
                system.debug('@SFS_Destination_Location_Id__c'+pr.SFS_Destination_Location_Id__c);
            }
        }
        system.debug('@prAutoReceiveMap'+prAutoReceiveMap);
        List<ProductRequestLineItem> prliList = new  List<ProductRequestLineItem>();
         if(!prAutoReceiveMap.keySet().isEmpty()){ // added the condition for SOQL error
         prliList = [Select Id,QuantityRequested,SFS_Product_QuantityUnitOfMeasure__c,SFS_Asset__c,Product2Id,Product2.SFS_Serialized_Code__c,SFS_Quantiy_Shipped__c,SFS_Quantity_Received__c,Parent.WorkOrderId,Parent.WorkOrderLineItemId,status,ParentId,Parent.WorkOrderLineItem.AssetId, 
                                                 parent.SFS_Destination_Location_Id__c,parent.SFS_Service_Appointment_Id__c,parent.Status from  ProductRequestLineItem where ParentId IN:prAutoReceiveMap.KeySet()];
         }
        system.debug('@prliList'+prliList);
        List<ProductTransfer> ptList = new  List<ProductTransfer>();
        List<ProductConsumed> pcList = new List<ProductConsumed>();
        List<ProductRequestLineItem> prliListToUpdate = new List<ProductRequestLineItem>();
        Map<String,ProductRequestLineItem> mapProductPrli = new Map<String,ProductRequestLineItem>();
        Map<string,String> mapPL = new  Map<string,String>();
        system.debug('@count'+prliList.size());
        if(prliList.size()>0){// added the condition for SOQL error
        for(ProductRequestLineItem prli : prliList){
            system.debug('@prli.Status'+prli.Status);
            if(prAutoReceiveMap.containsKey(prli.ParentId) && ((prli.parent.WorkOrderLineItemId!=null && prli.SFS_Asset__c==prli.Parent.WorkOrderLineItem.AssetId) || prli.parent.WorkOrderLineItemId==Null)) {
                if(prli.Status =='CLOSED' || prli.Status =='Shipped' || prli.Status=='Partially Shipped' || prli.Status=='Delivered' || prli.Status=='Partially Received'){
                                   
                    ptList.add(new ProductTransfer(
                        Product2Id = prli.Product2Id,
                        QuantitySent = prli.QuantityRequested,	
                        QuantityUnitOfMeasure = 'EA',
                        IsReceived = true,         
                        ProductRequestLineItemId = prli.Id,
                        QuantityReceived = prli.QuantityRequested,
                        ReceivedById = UserInfo.getUserId(),
                        SFS_Service_Appointment__c = prAutoReceiveMap.get(prli.ParentId).SFS_Service_Appointment_Id__c,
                        DestinationLocationId = prAutoReceiveMap.get(prli.ParentId).SFS_Destination_Location_Id__c));   
                    
                    mapProductPrli.put(prli.Product2Id,prli);
                    mapPL.put(prli.Product2Id,prAutoReceiveMap.get(prli.ParentId).SFS_Destination_Location_Id__c);
                    system.debug('@@mapPL'+mapPL);
                    
                }
                
            }
        }
        }
        
        if (createPTandPC == true)
        {
        system.debug('*** Product Transfer Records To Create ***'+ptList); 
        if(!ptList.isEmpty()){
            insert ptList; //Creates Product Transfer Records
        } 
        }
        system.debug('*** :mapPL.values() ***'+ mapPL.values()); 
        
        Map<string,ProductItem> mapProductAndPI = new Map<string,ProductItem>(); 
        for(ProductItem PI :[Select Id,LocationId,Product2Id,QuantityOnHand from ProductItem where Product2Id IN :mapProductPrli.KeySet() and LocationId IN :mapPL.values()]){
            mapProductAndPI.put(PI.Product2Id,PI);
            system.debug('*** productItem ***'+ PI); 
            
        }
        for(ProductRequestLineItem prli: mapProductPrli.Values()){   
          
            if(mapProductAndPI.containsKey(prli.Product2Id) && mapProductAndPI.get(prli.Product2Id).LocationId ==prAutoReceiveMap.get(prli.ParentId).SFS_Destination_Location_Id__c){
                system.debug('*** Product Consumed Records To Create ***'); 
                pcList.add(new ProductConsumed( 
                    
                    WorkOrderId = prAutoReceiveMap.get(prli.ParentId).WorkOrderId,
                    WorkOrderLineItemId =  prAutoReceiveMap.get(prli.ParentId).WorkOrderLineItemId,
                    ProductItemId = mapProductAndPI.get(prli.Product2Id).Id,    
                    SFS_Serial_Number__c = prli.Product2.SFS_Serialized_Code__c,
                    QuantityConsumed = prli.SFS_Quantity_Received__c));
            }  
            
          
        }
       
    }      
    
    //Update the freight charge on WorkOrder
    
    public static void updateFreightCharge(List<ProductRequest> prList,Map<Id,ProductRequest> prMap){
        
        try{
            Set<Id> woIDs = new Set<Id>();
            Map<Id,WorkOrder> woMap = new Map<Id,WorkOrder>();            
            for(ProductRequest pr1 : prList ){
                if(prMap==Null){
                    woIDs.add(pr1.WorkOrderId);
                }  
                else {
                    ProductRequest oldpr = prMap.get(pr1.Id);
                    if(pr1.SFS_Freight_Charge__c != oldpr.SFS_Freight_Charge__c)
                    {
                        woIDs.add(pr1.WorkOrderId);
                    }
                }
            } 
            for(Id woId : woIDs){
                woMap.put(woId, new WorkOrder(Id =woId,SFS_Freight_Charge__c=0));       
            }
            
            for(ProductRequest iLi : [select Id,SFS_Freight_Charge__c,WorkOrderId from ProductRequest where WorkOrderId IN :woIDs ]){
                woMap.get(iLi.WorkOrderId).SFS_Freight_Charge__c += iLi.SFS_Freight_Charge__c; 
            }
            
            if(woMap.size()>0 && woMap!=Null){
                database.update(woMap.values()); 
            }
        }Catch(Exception e){
            system.debug('@@exception'+e.getMessage());
        }
    }
    
    public static void updateLatestDates(List<ProductRequest> prList,Map<Id,ProductRequest> prMap){
        Set<Id> woIds=new Set<Id>();
        List<Datetime> allExpDelDates = new List<Datetime>(); 
        List<Datetime> allActDelDates = new List<Datetime>(); 
        list<WorkOrder> woToUpdate = new list<WorkOrder>();
        
        for(ProductRequest pList :prList){
            ProductRequest oldpr = prMap.get(pList.Id);
            
            if(pList.SFS_Latest_Expected_Delivery_Date__c!=oldpr.SFS_Latest_Expected_Delivery_Date__c||pList.SFS_Latest_Actual_Delivery_Date__c!=oldpr.SFS_Latest_Actual_Delivery_Date__c){
                WOIds.add(pList.WorkOrderId);
            }
        }
        system.debug('woid  '+WOIds);
        List<WorkOrder> WODetails=new List<WorkOrder>();
        if(WOIds.size()>0){
            for(Id WOId:WOIds){
                WorkOrder WODetail=[select SFS_Latest_Expected_Delivery_Date__c,SFS_Latest_Actual_Delivery_Date__c from WorkOrder where Id IN:WOIds ];
                WODetails.add(WODetail);
            }
        }
        List<ProductRequest> allPRList=new List<ProductRequest>();
        if(WOIds.size()>0){
            for(Id WOId:WOIds){
                
                List<ProductRequest> prodRList= [Select WorkOrderId,SFS_Latest_Expected_Delivery_Date__c,SFS_Latest_Actual_Delivery_Date__c from ProductRequest where WorkOrderId =:WOId ];
                system.debug('173--'+prodRList);
                for(ProductRequest proList:prodRList){
                    allPRList.add(proList);
                }
                
            }
        }
        system.debug('allprlist'+allPRList);
        if(allPRList.size()>0){
            for(ProductRequest pr :allPRList){
                allExpDelDates.add(pr.SFS_Latest_Expected_Delivery_Date__c);
                allActDelDates.add(pr.SFS_Latest_Actual_Delivery_Date__c);
            }
        }
        system.debug('beforesortdel'+allExpDelDates);
        system.debug('beforesortact'+allActDelDates);
        allExpDelDates.sort();
        system.debug('del'+allExpDelDates);
        allActDelDates.sort();
        system.debug('act'+allActDelDates);
        DateTime maxExpDate;
        DateTime maxActDate;
        if(allExpDelDates.size()>0){
             maxExpDate =allExpDelDates[allExpDelDates.size()-1];
        }
        if(allActDelDates.size()>0){
             maxActDate =allActDelDates[allActDelDates.size()-1];
        }
        system.debug('maxdel/maxact  '+maxExpDate+maxActDate);
      
        if(WODetails.size()>0){
            for(WorkOrder WO:WODetails){
                if((WO.SFS_Latest_Expected_Delivery_Date__c!=maxExpDate)||(WO.SFS_Latest_Actual_Delivery_Date__c!=maxActDate)){
                    WorkOrder updateWO=new WorkOrder();
                    updateWO.Id=WO.Id;
                    updateWO.SFS_Latest_Expected_Delivery_Date__c=maxExpDate;
                    updateWO.SFS_Latest_Actual_Delivery_Date__c=maxActDate;
                    woToUpdate.add(updateWO);
                    system.debug('200--'+woToUpdate);
                    
                    
                    
                }
            }
        }
        
        system.debug('213--'+woToUpdate);
        
        if(woToUpdate.size()>0){
            Update woToUpdate;
        } 
        
    }
  
    //New method for SIF-4642 - To post a chatter to WO Owner whenever PR is submitted
    public static void createChatterPosttoWOOwner(List<ProductRequest> prList,Map<Id,ProductRequest> prMap){
        Map<Id,ProductRequest> productRequestMap = new Map<Id,ProductRequest>();
        for(ProductRequest prRec :prList){
            ProductRequest oldprRec = prMap.get(prRec.Id);
            if(prRec.Status=='Submitted' && oldprRec.Status !=prRec.Status && prRec.SFS_ContainsSerializedParts__c){
                productRequestMap.put(prRec.WorkOrderId,prRec);
            }
        }
        if(!productRequestMap.keySet().isEmpty()){
            Map<Id,WorkOrder> workOrderMap = new Map<Id,WorkOrder>([SELECT OwnerId,Id FROM WorkOrder WHERE Id IN :productRequestMap.keySet()]);
            for(ProductRequest prRec : productRequestMap.values()){
                FeedItem post = new FeedItem();
                post.ParentId = workOrderMap.get(prRec.WorkOrderId).OwnerId;
                post.Body = prRec.ProductRequestNumber + ' has a Serialized Part(s). Receive the Serialized Parts manually in Oracle';
                insert post;
            }
        }

    }
}