public class C_GDIiConnAlertNotificationQueueable implements Queueable ,Database.AllowsCallouts {
    /* Author  : Emmanuel Nocks Mulea
* Email   : emmanuel.mulea@canpango.com
* Company : Canpango
* Date    : 11.12.2018
* 
*/
    
    static String gdiUserName;
    static String gdiPassword;
    static String baseURL;

    public void execute(QueueableContext qc){
        iConn_Admin__c iconnAdmin = iConn_Admin__c.getOrgDefaults();
        gdiUserName = iconnAdmin.Username__c;
        gdiPassword = iconnAdmin.Password__c;
        baseURL = iconnAdmin.API_URL__c;

        //SELECT existing client id that was used to subscribe on Notification
        list<RealTimeAlert__c> rta = [SELECT id,clientid__c FROM RealTimeAlert__c LIMIT 1];
        try{
            if(rta.size()>0){
                /*
* Listen to notification on Cumulocity Server using smart-rest-polling 
*/
                this.connect(rta[0].clientid__c);
            }
            else{
                /*
* This get executed if there is no record in realTime Object
* 1) handShake gets Clients ID from Cumulocity server
* 2) subcribe on the notification using that client id
* 3) connect in other words listen to notification
* 4) create a new client id , which will get updated when it expires 
*/
                string clientid = this.handShake();// get client ID
                RealTimeAlert__c newClient = new RealTimeAlert__c(clientid__c=clientid);           
                this.subscribe(clientid);
                this.connect(clientid);
                insert newClient;
            }
        }
        catch(Exception e){
            system.debug('error :'+e.getMessage());
        }
        /*
*Chain the job 
*/  
        if(!Test.isRunningTest()){
            system.enqueueJob(new C_GDIiConnAlertNotificationQueueable());       
        }
    }
    
    public string handShake(){
        try{
            HttpRequest req =  new HttpRequest();
            req.setEndpoint(baseURL + '/cep/realtime');
            req.setMethod('POST');
            
            Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('authorization', authorizationHeader);
            req.setHeader('Accept', '*/*');
            req.setHeader('Content-Type','application/json');
            req.setTimeout(119000);
            String body ='[{"channel": "/meta/handshake","ext": {"com.cumulocity.authn": {"token": "'+authorizationHeader+'"}},"version": "1.0","mininumVersion": "1.0beta","supportedConnectionTypes": ["websocket","long-polling"],"advice":{"timeout":120000,"interval":30000}}]';
            req.setBody(body);
            Http http = new Http();           
            HttpResponse response = http.send(req);
            List<Object> res =  (List<Object>)JSON.deserializeUntyped(response.getBody());
            Map<String,Object> res2 = (Map<String,Object>)res[0]; 
            
            return (String)res2.get('clientId');
        }
        catch(Exception e){
            system.debug('error :'+e.getMessage());
            return 'none';
        }
        
    }
    
    public string subscribe(string clientid){
        
        try{
            HttpRequest req =  new HttpRequest();
            req.setEndpoint(baseURL + '/cep/realtime');
            req.setMethod('POST');
            
            Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('authorization', authorizationHeader);
            req.setHeader('Accept', '*/*');
            req.setHeader('Content-Type','application/json');
            req.setTimeout(119000);
            String body ='[{"channel": "/meta/subscribe","clientId": "'+clientid+'","subscription": "/alarms/*"}]';
            req.setBody(body);
            Http http = new Http();           
            HttpResponse response = http.send(req);
            List<Object> res =  (List<Object>)JSON.deserializeUntyped(response.getBody());
            Map<String,Object> res2 = (Map<String,Object>)res[0]; 
            return 'success';
        }
        catch(Exception e){
            system.debug('error :'+e.getMessage());
            return 'error';
        }
    }
    
    public void connect(string clientid){
        
        HttpRequest req =  new HttpRequest();
        req.setEndpoint(baseURL + 'cep/realtime');
        req.setMethod('POST');
        
        Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('authorization', authorizationHeader);
        req.setHeader('Accept', '*/*');
        req.setHeader('Content-Type','application/json');
        req.setTimeout(119000);
        String body ='[{"channel": "/meta/connect","clientId": "'+clientid+'","connectionType": "smart-rest-polling","advice":{"timeout":1200000,"interval":30000}}]';
        req.setBody(body);
        Http http = new Http();           
        
        try {
            //Execute web service call here     
            HttpResponse response = http.send(req);
            List<Object> res =  (List<Object>)JSON.deserializeUntyped(response.getBody());
            if(res.size()>0){
                Map<String,Object> res2 = (Map<String,Object>)res[0];
                this.storeAlerts(res);
            }
        } catch(Exception e) {
            //Exception handling goes here.... retry the callout , assuming that it timeOut
            /*  HttpResponse response = http.send(req);
List<Object> res =  (List<Object>)JSON.deserializeUntyped(response.getBody());
Map<String,Object> res2 = (Map<String,Object>)res[0];


this.storeAlerts(res);
*/
            system.debug('error :'+e.getMessage());
        }
        
        
        
    }
    
    public void storeAlerts(list<object> obj){
        
        try{
            list<Alert__c>alerts = new list<Alert__c>();
            list<string>alertsId = new list<string>();
            Map<String,String>alertStatus = new Map<String,String>();
            
            for(Object data:obj ){
                
                Map<String,Object> data1 = (Map<String,Object>)data;
                if(data1.get('successful')==null){
                    
                    if(data1.get('data')!=null ){
                        Map<String,Object> data2 = (Map<String,Object>)data1.get('data');
                        if(data2!=null && data2.get('realtimeAction')=='UPDATE'){
                            Map<String,Object> data3 = (Map<String,Object>)data2.get('data');
                            alertsId.add((string)data3.get('id'));
                            alertStatus.put((String)data3.get('id'),(String)data3.get('status'));
                        }
                    }
                }
                else{
                    
                    if(data1.get('successful')==false){
                        throw new applicationException('ClientId invalid');
                    }
                }
            }
            
            List<Alert__c> existAlerts = [SELECT id,Status__c ,Alert_ID__c FROM Alert__c WHERE Alert_ID__c IN:alertsId];
            for(Alert__c al:existAlerts){ 
                alerts.add(new Alert__c(id=al.id,Status__c=alertStatus.get(String.valueOf(al.Alert_ID__c))));
            }
            if(alerts.size()>0){
                update alerts;
            }
        }
        catch(Exception e){
            
            if(e.getMessage()=='ClientId invalid'){
                list<RealTimeAlert__c> rta = [SELECT id,clientid__c FROM RealTimeAlert__c LIMIT 1];
                string clientid = this.handShake();// get client ID 
                rta[0].clientid__c = clientid;
                this.subscribe(clientid);
                this.connect(clientid);
                update rta[0];
            }
        }
    }
    
    //Custom Exception
    public class applicationException extends Exception {}
}