public class C_Customer_Invoice_With_Flow_Controller {

    public C_Customer_Invoice_With_Flow_Controller() {
        recordId = ApexPages.currentPage().getParameters().get('recordid');
        accountId = ApexPages.currentPage().getParameters().get('accountId');

    }

    public Id recordId {get;set;}
    public Id accountId {get;set;}

    public flow.interview.Customer_Invoice_Record_Creation myflow1 {get;set;}

/*   public String getendID() {        
        if (myflow1 ==null) return '/CaseID'; 
        else return myFlow1.CaseID;
    }       

    public PageReference getFinishLocation() {        
        PageReference endlocation = new PageReference('/' + getendID());
        return endlocation;
    }
*/
    
 // Set the page reference accessor methods
public PageReference finishLocation {
    get {
        PageReference pageRef = new PageReference('/' + newRecordId);
        pageRef.setRedirect(true);
        return pageRef;
        }
    set { finishlocation = value; }
}

// Set the accessor methods for the flow output variable
public String newRecordId {
    get {
        String strTemp = '';
            if(myflow1 != null) {
            strTemp = string.valueOf(myflow1.getVariableValue('CaseID'));
            }
        return strTemp;
    }
        set { newRecordId = value; }
    }
}