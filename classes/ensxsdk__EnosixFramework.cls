/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class EnosixFramework {
    global EnosixFramework() {

    }
    global static System.HttpRequest createRequest(Map<String,Object> requestArguments, Map<String,Object> requestParameters, Map<String,String> serializationAttributes) {
        return null;
    }
    global static void deserializeObject(ensxsdk.EnosixFramework.ValueObject valueObject, Object data) {

    }
    global static System.HttpResponse httpSend(System.HttpRequest request, Map<String,String> traceInfo) {
        return null;
    }
    global static ensxsdk.EnosixFramework.LoginResult login() {
        return null;
    }
    global static void registerReflectionResource(String resourceName, List<System.Type> typesToRegister) {

    }
    global static void setMock(System.Type t, Object inst) {

    }
global interface CollectionInfo extends ensxsdk.EnosixFramework.CompositeTypeInfo {
    ensxsdk.EnosixFramework.FrameworkCollection getCollection(ensxsdk.EnosixFramework.ValueObject param0);
}
global class CollectionInformation extends ensxsdk.EnosixFramework.CompositeType implements ensxsdk.EnosixFramework.CollectionInfo {
    global CollectionInformation() {

    }
    global ensxsdk.EnosixFramework.FrameworkCollection getCollection(ensxsdk.EnosixFramework.ValueObject instance) {
        return null;
    }
}
global abstract class CompositeType implements ensxsdk.EnosixFramework.CompositeTypeInfo {
    global CompositeType() {

    }
    global Map<String,ensxsdk.EnosixFramework.FieldInfo> getFields() {
        return null;
    }
    global System.Type getItemType() {
        return null;
    }
    global String getName() {
        return null;
    }
}
global interface CompositeTypeInfo {
    Map<String,ensxsdk.EnosixFramework.FieldInfo> getFields();
    System.Type getItemType();
    String getName();
}
global abstract class DetailObject extends ensxsdk.EnosixFramework.ValueObject implements ensxsdk.EnosixFramework.ReflectionCompositeTypeInfo, ensxsdk.EnosixFramework.Result {
    global DetailObject(String headerElement, Map<String,System.Type> typeMap) {

    }
    global Map<String,ensxsdk.EnosixFramework.CollectionInfo> getCollections() {
        return null;
    }
    global List<ensxsdk.EnosixFramework.Message> getMessages() {
        return null;
    }
    global Map<String,ensxsdk.EnosixFramework.StructInfo> getStructs() {
        return null;
    }
    global Boolean isSuccess() {
        return null;
    }
    global void setSuccess(Boolean success) {

    }
}
global abstract class DetailSBO implements ensxsdk.EnosixFramework.TypeInfo {
    global DetailSBO(String businessObjectName, System.Type businessObjectType) {

    }
    @TestVisible
    global ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) {
        return null;
    }
    @TestVisible
    global ensxsdk.EnosixFramework.DetailObject executeGetDetail(Object key) {
        return null;
    }
    @TestVisible
    global ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) {
        return null;
    }
    @TestVisible
    global ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) {
        return null;
    }
    global virtual String getBusinessObjectName() {
        return null;
    }
    global virtual System.Type getBusinessObjectType() {
        return null;
    }
    global abstract System.Type getType();
}
global interface DetailSBOCommandMock {
    ensxsdk.EnosixFramework.DetailObject executeCommand(String param0, ensxsdk.EnosixFramework.DetailObject param1);
}
global interface DetailSBOGetMock {
    ensxsdk.EnosixFramework.DetailObject executeGetDetail(Object param0);
}
global interface DetailSBOInitMock {
    ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject param0);
}
global interface DetailSBOSaveMock {
    ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject param0);
}
global interface FieldInfo {
    Integer getFieldWidth();
    String getName();
    String getTechnicalName();
    System.Type getType();
    Object getValue(ensxsdk.EnosixFramework.ValueObject param0);
    void setValue(ensxsdk.EnosixFramework.ValueObject param0, Object param1);
}
global class FieldInformation implements ensxsdk.EnosixFramework.FieldInfo {
    global FieldInformation() {

    }
    global Integer getFieldWidth() {
        return null;
    }
    global String getName() {
        return null;
    }
    global String getTechnicalName() {
        return null;
    }
    global System.Type getType() {
        return null;
    }
    global Object getValue(ensxsdk.EnosixFramework.ValueObject instance) {
        return null;
    }
    global void setValue(ensxsdk.EnosixFramework.ValueObject instance, Object newValue) {

    }
}
global virtual class FrameworkCollection implements ensxsdk.EnosixFramework.ReflectionTypeInfo {
    global FrameworkCollection(String elementName, System.Type elementType, ensxsdk.EnosixFramework.FrameworkCollectionDelegate delegate) {

    }
    global void add(ensxsdk.EnosixFramework.ValueObject obj) {

    }
    global void addAll(List<ensxsdk.EnosixFramework.ValueObject> items) {

    }
    global List<ensxsdk.EnosixFramework.ValueObject> buildList(System.Type listType) {
        return null;
    }
    global void clear() {

    }
    global void copyTo(List<ensxsdk.EnosixFramework.ValueObject> dest) {

    }
    global ensxsdk.EnosixFramework.ValueObject createItem() {
        return null;
    }
    global ensxsdk.EnosixFramework.ValueObject get(Integer index) {
        return null;
    }
    global Map<String,ensxsdk.EnosixFramework.FieldInfo> getFields() {
        return null;
    }
    global System.Type getItemType() {
        return null;
    }
    @TestVisible
    global virtual System.Type getType() {
        return null;
    }
    global Boolean isEmpty() {
        return null;
    }
    global void registerReflectionForClass() {

    }
    global ensxsdk.EnosixFramework.ValueObject remove(Integer index) {
        return null;
    }
    @TestVisible
    global Boolean remove(ensxsdk.EnosixFramework.ValueObject obj) {
        return null;
    }
    global void set(Integer index, ensxsdk.EnosixFramework.ValueObject listElement) {

    }
    global Integer size() {
        return null;
    }
}
global interface FrameworkCollectionDelegate {
}
global abstract class FunctionObject extends ensxsdk.EnosixFramework.ValueObject implements ensxsdk.EnosixFramework.ReflectionCompositeTypeInfo, ensxsdk.EnosixFramework.Result {
    global FunctionObject(Map<String,System.Type> typeMap) {

    }
    global Map<String,ensxsdk.EnosixFramework.CollectionInfo> getCollections() {
        return null;
    }
    global List<ensxsdk.EnosixFramework.Message> getMessages() {
        return null;
    }
    global Map<String,ensxsdk.EnosixFramework.StructInfo> getStructs() {
        return null;
    }
    global Boolean isSuccess() {
        return null;
    }
    global void setSuccess(Boolean success) {

    }
}
global class LoginResult extends ensxsdk.EnosixFramework.ValueObject implements ensxsdk.EnosixFramework.Result {
    global LoginResult() {

    }
    global List<ensxsdk.EnosixFramework.Message> getMessages() {
        return null;
    }
    @TestVisible
    global override System.Type getType() {
        return null;
    }
    global Boolean isSuccess() {
        return null;
    }
    global void setSuccess(Boolean success) {

    }
}
global class Message {
    global String Text {
        get;
    }
    global ensxsdk.EnosixFramework.MessageType Type {
        get;
    }
    global Message(ensxsdk.EnosixFramework.MessageType messageType, String message) {

    }
}
global enum MessageType {ABNORMALEND, ERROR, INFO, SAPEXIT, SUCCESS, WARNING}
global abstract class RFC implements ensxsdk.EnosixFramework.TypeInfo {
    global RFC(String functionName, System.Type functionResultType) {

    }
    @TestVisible
    global ensxsdk.EnosixFramework.FunctionObject executeFunction() {
        return null;
    }
    global virtual String getFunctionName() {
        return null;
    }
    global virtual System.Type getFunctionResultType() {
        return null;
    }
    global ensxsdk.EnosixFramework.FunctionObject getParameterContext() {
        return null;
    }
    global abstract System.Type getType();
}
global interface RFCMock {
    ensxsdk.EnosixFramework.FunctionObject executeFunction();
}
global interface ReflectionCompositeTypeInfo extends ensxsdk.EnosixFramework.ReflectionTypeInfo {
    Map<String,ensxsdk.EnosixFramework.CollectionInfo> getCollections();
    Map<String,ensxsdk.EnosixFramework.StructInfo> getStructs();
}
global interface ReflectionTypeInfo extends ensxsdk.EnosixFramework.TypeInfo {
    Map<String,ensxsdk.EnosixFramework.FieldInfo> getFields();
}
global interface Result {
    List<ensxsdk.EnosixFramework.Message> getMessages();
    Boolean isSuccess();
}
global abstract class SearchContext extends ensxsdk.EnosixFramework.ValueObject implements ensxsdk.EnosixFramework.ReflectionCompositeTypeInfo, ensxsdk.EnosixFramework.Result {
    global ensxsdk.EnosixFramework.SearchResult baseResult {
        get;
        set;
    }
    global ensxsdk.EnosixFramework.SearchPaging pagingOptions {
        get;
    }
    global SearchContext(Map<String,System.Type> elementMap) {

    }
    global Map<String,ensxsdk.EnosixFramework.CollectionInfo> getCollections() {
        return null;
    }
    global override Map<String,ensxsdk.EnosixFramework.FieldInfo> getFields() {
        return null;
    }
    global List<ensxsdk.EnosixFramework.Message> getMessages() {
        return null;
    }
    global Map<String,ensxsdk.EnosixFramework.StructInfo> getStructs() {
        return null;
    }
    global Boolean isSuccess() {
        return null;
    }
    global void setSuccess(Boolean success) {

    }
}
global class SearchPaging implements ensxsdk.EnosixFramework.Serialization {
    global Integer pageNumber {
        get;
        set;
    }
    global Integer pageSize {
        get;
        set;
    }
    global ensxsdk.EnosixFramework.SortFieldWrapper sortFields {
        get;
    }
    global List<ensxsdk.EnosixFramework.SearchSortField> sortOrder {
        get;
        set;
    }
    global Integer totalRecords {
        get;
    }
    global SearchPaging() {

    }
}
global abstract class SearchResult extends ensxsdk.EnosixFramework.ValueObject implements ensxsdk.EnosixFramework.ReflectionCompositeTypeInfo, ensxsdk.EnosixFramework.Result {
    global SearchResult(Map<String,System.Type> typeMap) {

    }
    global Map<String,ensxsdk.EnosixFramework.CollectionInfo> getCollections() {
        return null;
    }
    global override Map<String,ensxsdk.EnosixFramework.FieldInfo> getFields() {
        return null;
    }
    global List<ensxsdk.EnosixFramework.Message> getMessages() {
        return null;
    }
    global Map<String,ensxsdk.EnosixFramework.StructInfo> getStructs() {
        return null;
    }
    global Boolean isSuccess() {
        return null;
    }
    global void setSuccess(Boolean success) {

    }
}
global abstract class SearchSBO implements ensxsdk.EnosixFramework.TypeInfo {
    global SearchSBO(String businessObjectName, System.Type searchContextType, System.Type searchResultType) {

    }
    global ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc) {
        return null;
    }
    global ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc) {
        return null;
    }
    global virtual String getBusinessObjectName() {
        return null;
    }
    global virtual System.Type getSearchContextType() {
        return null;
    }
    global virtual System.Type getSearchResultType() {
        return null;
    }
    global abstract System.Type getType();
}
global interface SearchSBOInitMock {
    ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext param0);
}
global interface SearchSBOSearchMock {
    ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext param0);
}
global class SearchSortField implements System.Comparable {
    global String direction {
        get;
    }
    global String field {
        get;
    }
    global Integer precedence {
        get;
    }
    global SearchSortField(String fieldName, String direction) {

    }
    global SearchSortField(Integer precedence, String fieldName, String direction) {

    }
}
global class SortFieldWrapper {
    global SortFieldWrapper(List<ensxsdk.EnosixFramework.SearchSortField> sortFields) {

    }
    global void add(String fieldName, String direction) {

    }
    global void clear() {

    }
    global void set(Integer index, ensxsdk.EnosixFramework.SearchSortField newField) {

    }
    global void set(Integer index, String field, String direction) {

    }
}
global interface StructInfo extends ensxsdk.EnosixFramework.CompositeTypeInfo {
    Object getStruct(ensxsdk.EnosixFramework.ValueObject param0);
}
global class StructInformation extends ensxsdk.EnosixFramework.CompositeType implements ensxsdk.EnosixFramework.StructInfo {
    global StructInformation() {

    }
    global ensxsdk.EnosixFramework.ValueObject getStruct(ensxsdk.EnosixFramework.ValueObject instance) {
        return null;
    }
}
global interface TypeInfo {
    System.Type getType();
}
global abstract class ValueObject implements ensxsdk.EnosixFramework.FrameworkCollectionDelegate, ensxsdk.EnosixFramework.ReflectionTypeInfo, ensxsdk.EnosixFramework.Serialization {
    global ValueObject() {

    }
    global ValueObject(Map<String,System.Type> elementMap) {

    }
    global ValueObject(String elementName, Map<String,System.Type> elementMap) {

    }
    global virtual Object Get(String name) {
        return null;
    }
    @TestVisible
    global virtual void Set(Object value, String name) {

    }
    global Boolean getBoolean(String name) {
        return null;
    }
    @TestVisible
    global ensxsdk.EnosixFramework.FrameworkCollection getCollection(System.Type collectionItemType) {
        return null;
    }
    global Date getDate(String name) {
        return null;
    }
    global Datetime getDateTime(String name) {
        return null;
    }
    global Decimal getDecimal(String name) {
        return null;
    }
    global virtual Map<String,ensxsdk.EnosixFramework.FieldInfo> getFields() {
        return null;
    }
    global Integer getInteger(String name) {
        return null;
    }
    global virtual List<String> getKeyFields() {
        return null;
    }
    global String getNumC(String name) {
        return null;
    }
    global String getString(String name) {
        return null;
    }
    global ensxsdk.EnosixFramework.ValueObject getStruct(System.Type collectionItemType) {
        return null;
    }
    global Time getTime(String name) {
        return null;
    }
    global abstract System.Type getType();
    global Boolean isModified() {
        return null;
    }
    global virtual void registerReflectionForClass() {

    }
    global void setBoolean(Boolean value, String name) {

    }
    global virtual void setElementName(String name) {

    }
}
}
