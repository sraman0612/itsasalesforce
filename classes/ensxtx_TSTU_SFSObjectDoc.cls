/*
* Test cases for SFSObjectDoc utility class
*/
@isTest 
public class ensxtx_TSTU_SFSObjectDoc
{
    @isTest
    public static void test_loadSfsObjectDoc()
    {
        SObject testSObject = createTestObjects();

        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.loadSfsObjectDoc(testSObject);
        ensxtx_UTIL_SFSObjectDoc.loadSfsObjectDoc(testSObject.Id);
        ensxtx_UTIL_SFSObjectDoc.loadSfsObjectDoc('Opporutunity');
        Test.stopTest();
    }

    @isTest
    public static void test_initObjectsForSAP()
    {
        Test.startTest();
        Map<Id, SObject> testLines = null;
        Account testAccount = ensxtx_TSTU_SFTestObject.createTestAccount();
        testAccount.Name = 'test';
        ensxtx_TSTU_SFTestObject.upsertWithRetry(testAccount);
        ensxtx_UTIL_SFSObjectDoc.initObjectsForSAP('Bad Id', 'SAPDocNum', 'SAPDocType');
        ensxtx_UTIL_SFSObjectDoc.initObjectsForSAP(null, 'SAPDocNum', 'SAPDocType');
        Test.stopTest();
    }

    @isTest
    public static void test_getSObject()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.getSObject('bad id');
        ensxtx_UTIL_SFSObjectDoc.getSObject(testSObject.Id);
        ensxtx_UTIL_SFSObjectDoc.getSObject('Opportunity', 'sapType', 'sapDocNum');
        Test.stopTest();
    }

    @isTest
    static void test_getSObjectLineItems()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getSObjectLineItems(null);
        Test.stopTest();
    }

    @isTest
    static void test_getSObjectContacts()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getSObjectContacts(null);
        Test.stopTest();
    }

    @isTest
    static void test_getAccountId()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.getAccountId('bad id');
        ensxtx_UTIL_SFSObjectDoc.getAccountId(testSObject.Id);
        ensxtx_UTIL_SFSObjectDoc.getAccountId(testSObject);
        Test.stopTest();
    }

    @isTest
    static void test_getCustomerNumber()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.getCustomerNumber(testSObject);
        Test.stopTest();
    }

    @isTest
    static void test_getName()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getName(null);
        Test.stopTest();
    }

    @isTest
    static void test_getQuoteNumber()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getQuoteNumber(null);
        Test.stopTest();
    }

    @isTest
    static void test_getOrderNumber()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getOrderNumber(null);
        Test.stopTest();
    }

    @isTest
    static void test_getContractNumber()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getContractNumber(null);
        Test.stopTest();
    }

    @isTest
    static void test_getInquiryNumber()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getInquiryNumber(null);
        Test.stopTest();
    }

    @isTest
    static void test_getCreditMemoNumber()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getCreditMemoNumber(null);
        Test.stopTest();
    }

    @isTest
    static void test_getDebitMemoNumber()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getDebitMemoNumber(null);
        Test.stopTest();
    }

    @isTest
    static void test_getReturnOrderNumber()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getReturnOrderNumber(null);
        Test.stopTest();
    }

    @isTest
    static void test_getStatus()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getStatus(null);
        Test.stopTest();
    }

    @isTest
    static void test_getOpportunityId()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getOpportunityId(null);
        Test.stopTest();
    }

    @isTest
    static void test_getPriceBookId()
    {
        Test.startTest();
        ensxtx_UTIL_SFSObjectDoc.getPriceBookId(null);
        Test.stopTest();
    }

    @isTest
    static void test_getProductId()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.getProductId(testSObject, null);
        Test.stopTest();
    }

    @isTest
    static void test_getMaterial()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.getMaterial(testSObject, null);
        Test.stopTest();
    }

    @isTest
    static void test_getItemNumber()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.getItemNumber(testSObject, null);
        Test.stopTest();
    }

    @isTest
    static void test_validateSAPWithSfsObject()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.validateSAPWithSfsObject('', testSObject);
        Test.stopTest();
    }

    @isTest
    static void test_sObjectToSalesDocMapping()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.sObjectToSalesDocMapping(testSObject, new List<SObject>(), new ensxtx_DS_Document_Detail(), new ensxtx_DS_SalesDocAppSettings());
        ensxtx_UTIL_SFSObjectDoc.sObjectToSalesDocMapping(null, new List<SObject>(), new ensxtx_DS_Document_Detail(), new ensxtx_DS_SalesDocAppSettings());
        Test.stopTest();
    }

    @isTest
    static void test_salesDocMappingToSObject()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.salesDocMappingToSObject(testSObject, new ensxtx_DS_Document_Detail(), new ensxtx_DS_SalesDocAppSettings());
        ensxtx_UTIL_SFSObjectDoc.salesDocMappingToSObject(null, new ensxtx_DS_Document_Detail(), new ensxtx_DS_SalesDocAppSettings());
        Test.stopTest();
    }

    @isTest
    static void test_salesDocLineItemMappingToSObject()
    {
        Test.startTest();
        SObject testSObject = createTestObjects();
        ensxtx_UTIL_SFSObjectDoc.salesDocLineItemMappingToSObject(testSObject, new ensxtx_DS_Document_Detail.ITEMS(), new ensxtx_DS_SalesDocAppSettings(), new PricebookEntry(), null, null);
        ensxtx_UTIL_SFSObjectDoc.salesDocLineItemMappingToSObject(null, new ensxtx_DS_Document_Detail.ITEMS(), new ensxtx_DS_SalesDocAppSettings(), new PricebookEntry(), null, null);
        Test.stopTest();
    }

    private static Opportunity createTestObjects()
    {
        Account testAccount = ensxtx_TSTU_SFTestObject.createTestAccount();
        testAccount.Name = 'test';
        ensxtx_TSTU_SFTestObject.upsertWithRetry(testAccount);

        Id pricebookId = ensxtx_UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = ensxtx_TSTU_SFTestObject.createTestOpportunity();
        opp.Name = 'TEST OPP';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.AccountId = testAccount.Id;
        opp.Pricebook2Id = pricebookId;
        opp.put(ensxtx_UTIL_SFOpportunity.OrderFieldName, 'SAPOppO');
        opp.put(ensxtx_UTIL_SFOpportunity.QuoteFieldName, 'SAPOppQ');
        ensxtx_TSTU_SFTestObject.upsertWithRetry(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        ensxtx_TSTU_SFTestObject.upsertWithRetry(opp);
        return opp;
    }
}