@IsTest
public class TSTC_SAP_StartupClaimStatus {
    @IsTest
    public static void test() {
        Account account = new Account(Name = 'name', BillingCountry = 'United States');
        UTIL_SFAccount.setAccountCustomerNumber(account, 'test');
        insert account;

        Asset asset = new Asset();
        asset.Name = '8888';
        asset.put(UTIL_AssetSyncBatch.SFSyncKeyField,'8888-9999');
        asset.AccountId = account.Id;
        insert asset;
        

        Machine_Startup_Form__c msf = new Machine_Startup_Form__c(SAP_Status__c = '', SAP_Messages__c = '', Serial_Number__c = asset.Id);
        
        insert msf;
        
        CTRL_SAP_StartupClaimStatus.getStatus(msf.Id);
        CTRL_SAP_StartupClaimStatus.getStatus(null);
    }
}