/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 07/12/2021
* @description: Product Request Line Item Apex handler
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001      SIF-33     07/12/2021      Product Request Line Item Apex handler
============================================================================================================*/
public class SFS_PRLIHandler {
/**********************************************************************************************************
Purpose	: SIF-33 -- Product Request Line Item record cannot be deleted once submitted
Parameters: Trigger.Old from SFS_ProductrequestLineItemTrigger
************************************************************************************************************/
Public static void restrictDeletion(List<ProductRequestLineitem> lineItems){
    try{
        for(ProductRequestLineitem lineItem:lineItems){
            system.debug('19-'+lineItem.status);
            if(lineItem.status!='Open' && lineItem.status!=null)
                lineItem.adderror('Product Request Line Item cannot be deleted once submitted');
        }
    }Catch(Exception e){}
}
}