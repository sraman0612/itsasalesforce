public class iConn_Email_Invocable {
    
    @InvocableMethod(label='Send iConn Alert Email' description='Sends iConn VF Email Template to End User or Current Servicer associated to this Alert\'s Serial Number' category='Email')
    public static void sendIconnEmail(List<iConn_Email_Wrapper> wrappers) {
        
        EmailTemplate et;
        
        if(wrappers[0].IsToDist){
            et = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'iConn_Alarm_Dist_VisualForce'];
        }
        else{
            et = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'iConn_Alarm_VisualForce'];
        }
        
        OrgWideEmailAddress oea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'iconn.notification.qcy@gardnerdenver.com'];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        Contact con = [SELECT Id, EmailBouncedDate, EmailBouncedReason FROM Contact WHERE Id =: wrappers[0].contactId];
        
        if(String.isNotEmpty(con.EmailBouncedReason) && con.EmailBouncedDate!= null){
            return;
        }
        
        mail.setTemplateId(et.Id);
        mail.setWhatId(wrappers[0].alert.Id);
        mail.setTargetObjectId(wrappers[0].contactId);
        mail.setSaveAsActivity(true);
        mail.setOrgWideEmailAddressId(oea.Id);
        
        if(wrappers[0].bccAddresses != null){
            mail.setBccAddresses(wrappers[0].bccAddresses);
        }        
        
        if(!Test.isRunningTest()){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        //fade into oblivion
        return;
    } 
    
    
    public Class iConn_Email_Wrapper{
        @InvocableVariable(label='Alert' description='' required=true)
        public Alert__c alert; 
        
        @InvocableVariable(label='Contact' description='Recepient Id - Either Serial_Number_Contact__r.Contact__c Or Contact.Id' required=true)
        public Id contactId;
        
        @InvocableVariable(label='BCC Addresses' description='Addresses to BCC the alert' required=false)
        public List<String> bccAddresses;
        
        @InvocableVariable(label='To Distributor' description='True if sent to distributor' required=true)
        public Boolean isToDist;
        
    }
    
}