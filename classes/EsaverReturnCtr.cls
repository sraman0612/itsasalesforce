public with sharing class EsaverReturnCtr {

    public static final String SBQQ_EDITOR = '/s/sfdcpage/%2Fapex%2FSBQQ__sb%3F%26id%3D';
    public static final String QUOTE_ID_PARAM = 'Id';// referenced in test code for a tighter reference
    public static final String PDF_ID_PARAM = 'Comp';// referenced in test code for a tighter reference

    Map<String, String> urlParams;// Comp & Id

    public PageReference init(){

        PageReference page;

        try {

            urlParams = ApexPages.currentPage().getParameters();

            String quoteId = (Id)urlParams.get(QUOTE_ID_PARAM),
                    pdfId = urlParams.get(PDF_ID_PARAM),
                    baseURL = System.Site.getBaseUrl();

            if(System.Test.isRunningTest()){
                baseURL = EsaverReturnCtr_Test.MOCK_URL;
            }

            String url = baseURL + SBQQ_EDITOR + quoteId;

            page = new PageReference(url);
            page.setRedirect(true);

            update new SBQQ__Quote__c(
                Id = quoteId,
                Comp_PDF_UID__c = pdfId
            );

//            // get quote
//            QuoteModel quoteModel = getQuote(quoteId);
//
//            SBQQ.TriggerControl.disable();
//            quoteModel.record.Comp_PDF_UID__c = pdfId;
//
//            // update quote
//            SBQQ.ServiceRouter.save('SBQQ.QuoteAPI.QuoteSaver', JSON.serialize(quoteModel));
//
//            SBQQ.TriggerControl.enable();

            // redirect to configurator
            //String url = System.Site.getBaseUrl() + '/s/sfdcpage/%2Fapex%2FSBQQ__sb%3F%26id%3D' + quoteModel.record.Id;
        }
        catch(Exception e) {
            page = null;
            outputToUser(ApexPages.Severity.ERROR, e.getMessage());
        }

        return page;
    }

//    private QuoteModel getQuote(Id quoteId) {
//
//        QuoteModel quoteModel;
//        SBQQ__Quote__c quoteRecord;
//
//        String quoteJSON = SBQQ.ServiceRouter.read('SBQQ.QuoteAPI.QuoteReader', quoteId);
//        quoteModel = (QuoteModel) JSON.deserialize(quoteJSON, QuoteModel.class);
//
//        return quoteModel;
//    }
    private void outputToUser(ApexPages.Severity severity, String message) {
        ApexPages.addMessage(new ApexPages.Message(severity, message));
    }
}