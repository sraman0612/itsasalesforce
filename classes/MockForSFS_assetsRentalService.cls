@isTest
public class MockForSFS_assetsRentalService implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"field_types":[{"sid":"fieldTypeSid", "unique_name":"fieldTypeUniqueName"}]}');
        response.setStatusCode(200);
        return response; 
    }
}