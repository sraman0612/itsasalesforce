@isTest
private class C_ReparentAIAttachments_Test {

    @testSetup
    static void setupData(){
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Contact cont = new Contact();
        cont.firstName = 'Can';
        cont.lastName = ' Pango';
        cont.email = 'service-gdi@canpango.com';
        insert cont;
        
        Case cse = new Case();
        cse.Account = acct;
        cse.Contact = cont;
        cse.Subject = 'Test Case';
        insert cse;
        
        Case cse2 = new Case();
        cse2.Account = acct;
        cse2.Contact = cont;
        cse2.HIDDEN_Case_Number_for_AI_Record_for_IC__c  = cse.Id;
        cse2.Subject = 'Test Case';
        insert cse2;
        
        Attachment att = new Attachment();
        att.Name = 'Attachment Test';
        att.ParentId = cse.Id;
        att.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att.ContentType = 'image/jpg';
        insert att;
        
        Email_Field_Map__c efm = new Email_Field_Map__c();
        efm.Name = 'Test Email_Field_Map__c';
        insert efm;

    }
    
    static testMethod void testReParentAttachments() {
        Case c = [SELECT Id FROM Case WHERE HIDDEN_Case_Number_for_AI_Record_for_IC__c != null];
        C_ReparentAIAttachments.reParentAttachments(new List<Id>{c.Id});
    }
}