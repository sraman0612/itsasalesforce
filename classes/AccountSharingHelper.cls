public without sharing class AccountSharingHelper {
    public static String STR_SALES_REP_ACCOUNT_USER_PROFILE_NAME = 'Partner Community - Sales Rep';
    public static String GLOBAL_ACCOUNT_USER_PROFILE_NAME = 'Partner Community - Global Account User';
    
    public static AccountShare createAccountShareForUser(String accountId, String userId) {
        AccountShare accountShare = new AccountShare();
        
        accountShare.AccountId = accountId;
        accountShare.UserOrGroupId = userId;
        accountShare.AccountAccessLevel = 'Edit';
        accountShare.CaseAccessLevel = 'Edit';
        accountShare.OpportunityAccessLevel = 'Edit';
        
        return accountShare;
    }
    
    public static void shareAccounts(List<Account> accounts) {
        System.debug('accounts.size()=' + accounts.size());
        
        Map<String,List<String>> repAccountToAccountMap = new Map<String,List<String>>();
        
        for (Account account : accounts) {
            if(String.isNotBlank(account.Rep_Account2__c)) {
                if(repAccountToAccountMap.containsKey(account.Rep_Account2__c)) {
                    List<String> tempList = repAccountToAccountMap.get(account.Rep_Account2__c);
                    tempList.add(account.Id);
                    tempList.add(account.ParentId);
                    repAccountToAccountMap.put(account.Rep_Account2__c, tempList);
                }
                else {
                    repAccountToAccountMap.put(account.Rep_Account2__c, new List<String> {account.Id, account.ParentId});
                }
            }
        }
        
        List<User> salesRepUsers = [
            SELECT Id, 
            AccountId, 
            Account.Associated_Rep_Account__c 
            FROM User 
            WHERE (Profile_Name__c = : STR_SALES_REP_ACCOUNT_USER_PROFILE_NAME OR Profile_Name__c =: GLOBAL_ACCOUNT_USER_PROFILE_NAME) 
            AND (AccountId In : repAccountToAccountMap.keySet() OR Account.Associated_Rep_Account__c In : repAccountToAccountMap.keySet())
            AND IsActive = true
        ];
        
        System.debug('salesRepUsers.size()=' + salesRepUsers.size());
        
        List<AccountShare> accountShares = new List<AccountShare>();
        
        for (User salesRepUser : salesRepUsers) {
            List<String> accountsToShare = new List<String>();
            
            if(repAccountToAccountMap.containsKey(salesRepUser.AccountId)) {
                accountsToShare.addAll(repAccountToAccountMap.get(salesRepUser.AccountId));
            }
            
            if(repAccountToAccountMap.containsKey(salesRepUser.Account.Associated_Rep_Account__c)) {
                accountsToShare.addAll(repAccountToAccountMap.get(salesRepUser.Account.Associated_Rep_Account__c));
            }
            
            for(String accountIdToShare : accountsToShare) {
                if (accountIdToShare != null && accountIdToShare != 'null' && string.isNotBlank(accountIdToShare)) {
                    accountShares.add(createAccountShareForUser(accountIdToShare, salesRepUser.Id));
                }
            }
        }
        
        System.debug('inserting ' + accountShares.size() + ' account shares.');
        
        for(AccountShare shr : accountShares){
            System.debug(shr.UserOrGroupId + ' - ' + shr.AccountId);
        }
        
        if (accountShares.size() > 0) {
            insert accountShares;
        }
    }
    
    public static List<AccountShare> createShares(Account account, List<User> users) {
        List<AccountShare> accountShares = new List<AccountShare>();
        
        for (User user : users) {
            accountShares.add(createAccountShareForUser(account.Id, user.Id));
            if(String.isNotBlank(account.ParentId)){
                accountShares.add(createAccountShareForUser(account.ParentId, user.Id));
            }
        }
        
        return accountShares;
    }
    
    public static void processAccounts(List<String> accountIds) {
        if (System.isBatch() || System.isFuture() || Test.isRunningTest()) {
            processAccountsImpl(accountIds);
        } else {
            processAccountsFuture(accountIds);
        }
    }
    
    @future
    private static void processAccountsFuture(List<String> accountIds) {
        processAccountsImpl(accountIds);
    }
    
    private static void processAccountsImpl(List<String> accountIds) {
        List<Account> accounts = [SELECT Id, ParentId, Rep_Account2__c, Rep_Account2__r.Associated_Rep_Account__c from Account where Id in : accountIds];
        
        if (accounts.size() > 0) {
            shareAccounts(accounts); 
        }
    }
    
    public static void processAssociatedRepAccounts(List<String> accountIds) {
        if (System.isBatch() || System.isFuture() || Test.isRunningTest()) {
            processAssociatedRepAccountsImpl(accountIds);
        } else {
            processAssociatedRepAccountsFuture(accountIds);
        }
    }
    
    @future
    private static void processAssociatedRepAccountsFuture(List<String> accountIds) {
        processAssociatedRepAccountsImpl(accountIds);
    }
    
    private static void processAssociatedRepAccountsImpl(List<String> accountIds) {
        List<Account> accounts = [SELECT Id, ParentId, Associated_Rep_Account__c, Rep_Account2__c, Rep_Account2__r.Associated_Rep_Account__c from Account where Rep_Account2__c in : accountIds];
        
        if (accounts.size() > 0) {
            shareAccounts(accounts); 
        }
    }
    
    public static void updateShares(List<String> userIds) {
        if (System.isBatch() || System.isFuture() || Test.isRunningTest()) {
            updateSharesImpl(userIds);
        } else {
            updateSharesFuture(userIds);
        }
    }
    
    @future
    private static void updateSharesFuture(List<String> userIds) {
        updateSharesImpl(userIds);
    }
    
    private static void updateSharesImpl(List<String> userIds) {
        List<User> users = [SELECT Id, AccountId, Account.Associated_Rep_Account__c from User where Id in : userIds];
        
        Map<String,List<User>> accountToUserListMap = new Map<String,List<User>>();
        
        for (User u : users) {
            List<User> userList = accountToUserListMap.get(u.AccountId);
            
            if (userList == null) {
                userList = new List<User>();
                accountToUserListMap.put(u.AccountId, userList);
            }
            
            userList.add(u);
            
            if (u.Account.Associated_Rep_Account__c != null) {
                userList = accountToUserListMap.get(u.Account.Associated_Rep_Account__c);
                
                if (userList == null) {
                    userList = new List<User>();
                    accountToUserListMap.put(u.Account.Associated_Rep_Account__c, userList);
                }
                
                userList.add(u);
            }
        }
        
        if (accountToUserListMap.isEmpty()) {
            return;
        }
        
        List<Account> accounts = [SELECT Id, ParentId, Rep_Account2__c from Account where Rep_Account2__c in : accountToUserListMap.keySet()];
        
        List<AccountShare> accountShares = new List<AccountShare>();
        
        for (Account account : accounts) {
            accountShares.addAll(createShares(account, accountToUserListMap.get(account.Rep_Account2__c)));
        }
        
        if (accountShares.size() > 0) {
            insert accountShares;
        }
    }
}