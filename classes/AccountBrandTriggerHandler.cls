/****************************************************************************
* Name        - AccountBrandTriggerHandler
* Description - Handler for AccountBrandTrigger
                   
* Modification Log :
* ---------------------------------------------------------------------------
* Developer             Date            Description
* ---------------------------------------------------------------------------
* jmasters              06-05-2019          Created 
****************************************************************************/
public without sharing class AccountBrandTriggerHandler {
    
    public static boolean firstRun = true;
    private boolean m_isExecuting = false;
    
    public AccountBrandTriggerHandler( boolean isExecuting ) {
        m_isExecuting = isExecuting;
    }
    
    public void OnBeforeInsert( AccountBrand[] newAccountBrands ) {
        // EXECUTE BEFORE INSERT LOGIC
    }
    
    public void OnAfterInsert( AccountBrand[] newAccountBrands, Map<Id, AccountBrand> accountBrandNewMap ) {
        // AFTER INSERT LOGIC
        // 06-05-2019 jhm: Move the logo up to the Account, if it's there
        moveLogoToAccount( newAccountBrands );
    }
    
    public void OnBeforeUpdate( AccountBrand[] oldAccountBrands, AccountBrand[] updatedAccountBrands, Map<Id, AccountBrand> accountBrandOldMap, Map<Id, AccountBrand> AccountBrandNewMap ) {
        // BEFORE UPDATE LOGIC
    }
    
    public void OnAfterUpdate( AccountBrand[] oldAccountBrands, AccountBrand[] updatedAccountBrands, Map<Id, AccountBrand> accountBrandNewMap, Map<Id, AccountBrand> AccountBrandOldMap ) {
        // PREVENT RECURSIVE TRIGGER
        if (firstRun){
            firstRun = false;
            system.debug('First Run');
        } else {
            System.debug('Already ran!');
            return;
        }
        // AFTER UPDATE LOGIC
        // 06-05-2019 jhm: Move the logo up to the Account, if it's there
        moveLogoToAccount( updatedAccountBrands );
    }
        
    public void OnBeforeDelete( AccountBrand[] AccountBrandsToDelete, Map<Id, AccountBrand> accountBrandMap ) {
        // BEFORE DELETE LOGIC
    }
    
    public void OnAfterDelete( AccountBrand[] deletedAccountBrands, Map<Id, AccountBrand> accountBrandMap ) {
        // AFTER DELETE LOGIC
    }
        
    public void OnUndelete( AccountBrand[] restoredAccountBrands ) {
        // AFTER UNDELETE LOGIC
    }
    
    public boolean IsTriggerContext {
        get{ return m_isExecuting; }
    }
    
    private void moveLogoToAccount( AccountBrand[] updatedAccountBrands ) {
        // 06-05-2019 jhm: If the Logo is populated, let's move it over to the Account's field.
        List<Account> accountsToUpdate = new List<Account>();
        List<Account> accountsToUpdate2 = new List<Account>();
        
        for( AccountBrand ab : updatedAccountBrands ) {
            if( null != ab.LogoUrl ) {
                accountsToUpdate.add( new Account( Id = ab.AccountId, Distributor_Logo_URL__c = ab.LogoUrl ) );
            }
            }
            
        for( AccountBrand ab2 : updatedAccountBrands ) {
            if( null != ab2.Margin_or_Markup__c ) {
                accountsToUpdate2.add( new Account( Id = ab2.AccountId,   Markup_or_Margin__c = ab2.Margin_or_Markup__c ) );
            }
            }
            
            
            
        
        if( 0 < accountsToUpdate.size() ) {
            database.update( accountsToUpdate, false );
        }
        
        if( 0 < accountsToUpdate2.size() ) {
            database.update( accountsToUpdate2, false );
        }
    }
}