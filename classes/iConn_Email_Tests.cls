@isTest
private class iConn_Email_Tests {

    @testSetup
    private static void doSetup(){
        Account ac = new Account(Name='test');
        insert ac;
        System.debug(ac);
        
        Contact c = new Contact(FirstName = 'Test', LastName = 'Person', Email = 'sfadmin.qcy@gardnerdenver.com', AccountId = ac.Id);
        insert c;
        System.debug(c);
        
        Machine_Version_Parts_List__c mvpl = new Machine_Version_Parts_List__c();
        Service_History__c sh = new Service_History__c();
        Part__c oil = new Part__c(Name = 'OIL1:');
        Part__c air = new Part__c(Name = 'AIR1:');
        Part__c sep = new Part__c(Name = 'SEP1:');
        Part__c cb = new Part__c(Name = 'CAB1:');
        Part__c cab = new Part__c(Name = 'CBFILTER1:');
        Part__c lube = new Part__c(Name = 'LUBE1:');
        lube.Lubricant_Hours__c = '4000.0';
        
        insert oil;
        insert air;
        insert sep;
        insert cb;
        insert cab;
        insert lube;
        
        // MVPL parts
        mvpl.Oil_Filter_1__r = oil;
        mvpl.Air_Filter_1__r = air;
        mvpl.Separator_1__r = sep;
        mvpl.Cabinet_Filter_1__r = cab;
        mvpl.Control_Box_Filter_1__r = cb;
        mvpl.Lubricant_1__r = lube;
        
        // Quant expected
        mvpl.Oil_Filter_Quantity_Expected__c = 1;
        mvpl.Air_Filter_Quantity_Expected__c = 1;
        mvpl.Separator_Quantity_Expected__c = 1;
        mvpl.Cabinet_Filter_Quantity_Expected__c = 1;
        mvpl.Control_Box_Filter_Quantity_Expected__c = 1;
        mvpl.Lubricant_Quantity__c = 1;
        
        // Other mvpl
        mvpl.Configuration__c = 'no';
        
        insert mvpl;
        
        Asset a = new Asset(SerialNumber='123_TEST', Name='iConnMachinetest', AccountId = ac.Id, Current_Servicer__c = ac.Id, Warranty_End_Date__c = System.today().addDays(365), Machine_Version_Parts_List__c = mvpl.Id);
        insert a;
        System.debug(a);
        
        Alert__c al = new Alert__c(Alert_ID__c='622165533',Serial_Number__c=a.id, Code_Block_Status__c = true, Servicer__c = ac.Id);
        insert al;
        System.debug(al);
        
        
        Serial_Number_Contact__c sc = new Serial_Number_Contact__c();
        sc.First_Name__c ='j';
        sc.Last_Name__c = 'b';
        sc.Relationship__c = 'End User';
        sc.Email_Address__c  = 'jbtest3@test.com';
        sc.Serial_Number__c  = a.id;
        sc.Contact__c = c.Id;
        insert sc;
        System.debug(sc);
        
        
        
    }
    
    private static testmethod void doTest(){
        
        Alert__c theAlert = [SELECT Id FROM Alert__c LIMIT 1];
        Contact theContact = [SELECT Id FROM Contact LIMIT 1];
        
        
        iConn_Email_Invocable.iConn_Email_Wrapper invocableWrapper = new iConn_Email_Invocable.iConn_Email_Wrapper();
        invocableWrapper.alert = theAlert;
        invocableWrapper.contactId = theContact.Id;
        invocableWrapper.bccAddresses = null;
        invocableWrapper.isToDist = false;
        
        Test.startTest();
        iConn_Email_Invocable.sendIconnEmail(new List<iConn_Email_Invocable.iConn_Email_Wrapper>{invocableWrapper});
        iConn_Email_Controller iConnController = new iConn_Email_Controller();
        iConnController.endRecipient = theContact;
        iConnController.alert = theAlert;
        Test.stopTest();
    }
    
    private static testmethod void doBulkTest() {
        Alert__c theAlert = [SELECT Id FROM Alert__c LIMIT 1];
        List<Contact> serviceContact = [SELECT Id FROM Contact LIMIT 1];
        String serviceContactId;
        List<Contact> theContact = [SELECT Id FROM Contact LIMIT 2];
        String contactId;
        List<String> contactIds = new List<String>();
        List<String> serviceContactIds = new List<String>();
        
        for(Contact c : theContact) {
            contactId = c.Id;
            contactIds.add(contactId);
        }
        
        if(serviceContact != null) {
            for(Contact sc : serviceContact) {
                serviceContactId = sc.Id;
                serviceContactIds.add(serviceContactId);
            }
        }
        
        iConn_Bulk_Email_Invocable.iConn_Bulk_Email_Wrapper invocableWrapper = new iConn_Bulk_Email_Invocable.iConn_Bulk_Email_Wrapper();
        invocableWrapper.alert = theAlert;
        invocableWrapper.iConnContactIds = contactIds;
        invocableWrapper.bccAddresses = null;
        invocableWrapper.servicerContactId = serviceContactIds;
        
        Test.startTest();
        iConn_Bulk_Email_Invocable.sendiConnBulkEmail(new List<iConn_Bulk_Email_Invocable.iConn_Bulk_Email_Wrapper>{invocableWrapper});
    }
    
    
}