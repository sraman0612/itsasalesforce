@isTest
public class CZ_QuoteDetailPageExtensionTest {
    
    @testSetup
    public static void setupData(){
        Lookup_Quote_Docs_Distinct_Benefits__c db = new Lookup_Quote_Docs_Distinct_Benefits__c();
        insert db;
        
        List<Product2> productList = createProducts('Test Product', 2, true);
        
        productList[0].Technical_Information_Bullet_1__c = 'fadf';
        productList[0].Technical_Information_Bullet_2__c = 'fadf';        
        productList[0].Technical_Information_Bullet_3__c = 'fadf';
        productList[0].Technical_Information_Bullet_4__c = 'fadf';        
        productList[0].Technical_Information_Bullet_5__c = 'fadf';
        productList[0].Technical_Information_Bullet_6__c = 'fadf';
        
        MPG4_Code__c mc = new MPG4_Code__c();
        mc.Name = 'testName';
        mc.MPG4_Number__c = '2';
        mc.Level_3__c = 'Compressors';
        mc.Distinct_Benefit_1__c = db.Id;
       
        insert mc;
        
        Lookup_Quote_Docs_Machines__c qdm = new Lookup_Quote_Docs_Machines__c();
        qdm.MPG4_Code__c = mc.Id;
        qdm.Distribution_Channel__c = 'no';
        
        insert qdm;
        
        Lookup_Quote_Docs_Tech_Specs__c techspecs = new Lookup_Quote_Docs_Tech_Specs__c(Technical_Information_Bullet_1__c= 'a', Technical_Information_Bullet_2__c= 'a', 
                                                                      Technical_Information_Bullet_3__c= 'a', Technical_Information_Bullet_4__c= 'a', Technical_Information_Bullet_5__c= 'a', 
                                                                      Technical_Information_Bullet_6__c = 'a', MPG4_Code__c = mc.Id, Distribution_Channel__c = 'no');
        insert techspecs;
        
        Machine_Data_Lookup__c machineData = new Machine_Data_Lookup__c(
            DChl__c = 'CP',
            Frame_Size__c = '7',
            Configurator__c = 'test1',
            Gear_Size__c = '5',
            HP__c = '100',
            Pressure__c = '125',
            Recip_Model__c = 'NONE',
            Voltage__c = '380-3-50',
            Distinct_Benefit_1__c = db.Id,
            Distinct_Benefit_2__c = db.Id,
            Distinct_Benefit_3__c = db.Id,
            Feature_Benefits__c = qdm.Id,
            Tech_Specs__c = techspecs.Id,
            MPG4__c = '2'
        );
        
        insert machineData;

        productList[0].Feature_Benefits__c = qdm.Id;
            
        productList[0].Distinctive_Benefit_1__c = db.Id;
        productList[0].Distinctive_Benefit_2__c = db.Id;
        productList[0].Distinctive_Benefit_3__c = db.Id;
        productList[0].SBQQ__ExternallyConfigurable__c = false;
        
        
        
        productList[1].Distinctive_Benefit_1__c = db.Id;
        productList[1].Distinctive_Benefit_2__c = db.Id;
        productList[1].Distinctive_Benefit_3__c = db.Id;
        productList[1].SBQQ__ExternallyConfigurable__c = true;
        //productList[1].Product_Type__c = 'Compressors';
        productList[1].MPG4_Code__c = mc.Id;
        productList[1].FLD_Distribution_Channel__c = 'no';
        productList[1].HP_KW__c = 'maybe';
        productList[1].Technical_Information_Bullet_1__c = 'a';
        productList[1].Feature_Benefits__c = qdm.Id;
        productList[1].Distinctive_Benefit_1__c =  db.Id;
        
        update productList;
        
        Lookup_Quote_Docs_Tech_Specs__c dts = new Lookup_Quote_Docs_Tech_Specs__c();
		dts.MPG4_Code__c = mc.Id;
        dts.Distribution_Channel__c = 'no';  
        dts.HP__c = 'maybe';
        
        insert dts;

        createPricebookEntries(productList);

        List<Account> parentAccountList = createAccounts('TestAccount', null,  1, true);

        Account a = parentAccountList[0];
        a.Account_Number__c='1234';  
        Update a;
        createOpportunity('Test Opp', a.Id);

        PricebookEntry pbEntry1 = [Select Id from PricebookEntry where Product2Id = :productList[0].Id limit 1];

        PricebookEntry pbEntry2 = [Select Id from PricebookEntry where Product2Id = :productList[1].Id limit 1];
        
        Opportunity opp = [Select Id from Opportunity limit 1];

        SBQQ__Quote__c quote = createQuote('Test Quote',a.Id, opp.Id, productList[0].Id, pbEntry1.Id);

        SBQQ__DiscountSchedule__c ds = new SBQQ__DiscountSchedule__c();

        ds.Name = 'Test DS';
        ds.SBQQ__Account__c = a.Id;
        ds.SBQQ__Product__c = productList[0].Id;
        ds.SBQQ__AggregationScope__c = 'Quote';
        ds.SBQQ__QuoteLineQuantityField__c = 'Quantity';

        insert ds;

        SBQQ__DiscountTier__c dt = new SBQQ__DiscountTier__c();

        dt.SBQQ__Schedule__c = ds.Id;
        dt.Name = 'Tier1';
        dt.SBQQ__LowerBound__c = 1;
        dt.SBQQ__UpperBound__c = 1001;
        dt.SBQQ__DiscountAmount__c = -40;
        dt.SBQQ__Discount__c = 20;
        dt.SBQQ__Number__c = 1;

        insert dt;

        createQuoteLine(quote.Id, productList[0].Id, pbEntry1.Id,  ds.Id);
        createQuoteLine(quote.Id, productList[1].Id, pbEntry2.Id,  ds.Id);
    }

    @isTest
    public static void testController(){

        //Test.startTest();

        SBQQ__Quote__c quote = [Select Id from SBQQ__Quote__c limit 1];

        PageReference pageRef = Page.VFTest;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('qid', quote.id);

        CZ_QuoteDetailPageExtension cont = new CZ_QuoteDetailPageExtension();

        cont.showPage();

        //Test.stopTest();

    }

    public static List<Product2> createProducts(String productName, Integer size, boolean save){

        List<Product2> productList = new List<Product2>();

        Integer i = 0;

        while(i < size){

            Product2 prod = new Product2(Name = productName+'_'+size,ProductCode = 'TEST',isActive = true, SBQQ__AssetConversion__c = 'One per quote line',
                                         SBQQ__AssetAmendmentBehavior__c = 'Default', SBQQ__PricingMethod__c = 'List', SBQQ__SubscriptionPricing__c = 'Fixed Price',
                                         SBQQ__SubscriptionTerm__c = 12, SBQQ__SubscriptionType__c = 'Renewable');

System.debug(prod);
            productList.add(prod);

            i++;

        }

        if(save){

            insert productList;

        }

        return productList;

    }

    public static void createPricebookEntries(List<Product2> productList){

        Id pricebookId = Test.getStandardPricebookId();

        List<PricebookEntry> entryList = new List<PricebookEntry>();

        for(Product2 product : productList){

            PricebookEntry pbEntry = new PricebookEntry(Pricebook2Id = pricebookId,Product2Id = product.Id,UnitPrice = 100.00,IsActive = true);

            entryList.add(pbEntry);

        }

        insert entryList;

    }

    public static List<Account> createAccounts(String accountName, Id parentId, Integer size, boolean save){

        List<Account> accountList = new List<Account>();

        Integer i = 0;
        while(i < size){


        Account a = new Account(name = accountName+'__'+size, billingstreet = '101 Main st.', billingcity = 'Atlanta', billingstate = 'Georgia', billingpostalcode = '30303', billingcountry = 'United States');


        accountList.add(a);

            i++;

        }

        if(save){

            insert accountList;

        }

        return accountList;

    }

    public static Opportunity createOpportunity(String oppName, Id accountId ){

        Id pricebookId = Test.getStandardPricebookId();

        //test.startTest();
        Opportunity opp = new Opportunity(accountid = accountId, name = oppName, amount = 1000, closedate = date.today(),
                                          StageName = 'Idea', pricebook2Id = pricebookId);

        opp.LeadSource = 'Email';
        opp.AccountId = accountId;
        opp.OwnerId = UserInfo.getUserId();
        opp.Amount = 213230;
        opp.Probability = 25;
        opp.StageName = 'Needs Assessment';
        opp.TotalOpportunityQuantity = 27;


        insert opp;

        return opp;

    }

    public static SBQQ__Quote__c createQuote(String quoteName, Id accountId, Id oppId, Id prodId, Id pbeId){


        SBQQ__Quote__c originalQ = new SBQQ__Quote__c(SBQQ__LineItemsPrinted__c = false, SBQQ__OrderByQuoteLineGroup__c = false,
                                                      SBQQ__Ordered__c = false, SBQQ__Primary__c = true, SBQQ__Unopened__c = false, SBQQ__WatermarkShown__c = false,
                                                      sbqq__opportunity2__c = oppId, SBQQ__LineItemsGrouped__c = true, SBQQ__StartDate__c = date.today(),
                                                     SBQQ__PaymentTerms__c = 'Net 30');

        originalQ.SBQQ__Status__c = 'Approved';
        originalQ.SBQQ__Account__c = accountId;
        originalQ.SBQQ__Opportunity2__c = oppId;
        originalQ.SBQQ__Status__c = 'Approved';
        originalQ.SBQQ__Status__c = 'Signed';
        originalQ.SBQQ__Type__c = 'Quote';
        originalQ.SBQQ__Primary__c = true;


        insert originalQ;

        //createQuoteLine(originalQ.Id, prodId, pbeId);

        return originalQ;

    }

    public static void createQuoteLine(Id quoteId, Id prodId, Id pbeId, Id dsId){

        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();

        quoteLine.SBQQ__Product__c = prodId;
        quoteLine.SBQQ__DiscountSchedule__c = dsId;
        quoteLine.SBQQ__Quote__c = quoteId;
        quoteLine.SBQQ__ChargeType__c = 'One-Time';
        quoteLine.SBQQ__CustomerPrice__c = 100;
        quoteLine.SBQQ__Description__c =    'Professional Services';
        quoteLine.SBQQ__NetPrice__c = 100;
        quoteLine.SBQQ__Number__c = 1;
        quoteLine.SBQQ__RegularPrice__c = 100;
        quoteLine.SBQQ__PricebookEntryId__c = pbeId;
        quoteLine.SBQQ__SubscriptionScope__c = 'Quote';
        quoteLine.SBQQ__ListPrice__c = 100;
        //quoteLine.blng__BillingAccount__c = a.Id;

        insert quoteLine;

    }

}