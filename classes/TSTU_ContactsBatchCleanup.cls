@isTest
public with sharing class TSTU_ContactsBatchCleanup
{
    public static testMethod void test_ContactCleanup()
    {
        Database.executeBatch(new UTIL_ContactsBatchCleanup());
        createExistingContactAndAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ContactsBatchCleanup());
        Test.stopTest();
    }

    private static void createExistingContactAndAccount()
    {
        Account acct = TSTU_SFAccount.createTestAccount();
        acct.put(UTIL_SFAccount.CustomerFieldName, '12345');
        insert acct;

        Contact contact = TSTU_SFContact.createTestContact(null);
        contact.put(UTIL_SFContact.ContactCustomerFieldName, '12345');
        insert contact;
    }
}