@isTest 
public with sharing class EsaverOutbound_Ctrl_Mock implements HttpCalloutMock {

    static final String SUCCESS_URL = 'https://www.success.com';

    private ESAVER_REDIRECT_STATUS_MODE redirectMode;
    public enum ESAVER_REDIRECT_STATUS_MODE {SUCCESS, BAD_REQUEST, ESAVER_SERVER_ERROR_CAUGHT, ESAVER_SERVER_ERROR_UNCAUGHT}

    public EsaverOutbound_Ctrl_Mock(ESAVER_REDIRECT_STATUS_MODE mode){
        this.redirectMode = mode;
    }

    public HttpResponse respond(HttpRequest req) {

        HttpResponse response;

        if(req.getEndpoint() == EsaverOutbound_Ctrl.ESAVER_OAUTH_TOKEN_URL){
            response = oauth_success();
        }
        else if (req.getEndpoint() == EsaverOutbound_Ctrl.ESAVER_START_URL){

            switch on this.redirectMode {

                when SUCCESS{
                    response = redirect_success();
                }
                when BAD_REQUEST{
                    response = badRequest();
                }
                when ESAVER_SERVER_ERROR_CAUGHT{
                    response = esaverServerError_caught();
                }
                when ESAVER_SERVER_ERROR_UNCAUGHT{
                    response = esaverServerError_uncaught();
                }
            }
        }

        return response;
    }

    private HttpResponse oauth_success(){

        EsaverOutbound_Ctrl.GDInsideOAuthInfo body = new EsaverOutbound_Ctrl.GDInsideOAuthInfo();
        body.token_type = 'Bearer';
        body.expires_in = '1571751050';
        body.access_token = 'f01b96ee52003229d2a798885c5d5a78';
        body.refresh_token = '8f8164c6b41feb1bce544a8f1be6cf9d';

        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json;charset:utf-8;');
        response.setStatusCode(200);
        response.setBody(JSON.serialize(body));

        return response;
    }

    private HttpResponse redirect_success(){

        EsaverOutbound_Ctrl.EsaverResponse body = new EsaverOutbound_Ctrl.EsaverResponse();
        body.url = SUCCESS_URL;
        body.Success = true;
        body.Errors = new Map<String, List<String>>();

        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json;charset:utf-8;');
        response.setStatusCode(200);
        response.setBody(JSON.serialize(body));

        return response;
    }

    private HttpResponse badRequest(){

        EsaverOutbound_Ctrl.EsaverResponse body = new EsaverOutbound_Ctrl.EsaverResponse();
        body.Success = false;
        body.Errors = new Map<String, List<String>>();
        body.Errors.put('BAD_REQUEST', new List<String> {'BAD_REQUEST'});

        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json;charset:utf-8;');
        response.setStatusCode(400);
        response.setBody(JSON.serialize(body));

        return response;
    }


    private HttpResponse esaverServerError_caught(){

        EsaverOutbound_Ctrl.EsaverResponse body = new EsaverOutbound_Ctrl.EsaverResponse();
        body.Success = false;
        body.Errors = new Map<String, List<String>>();
        body.Errors.put('User Not Found', new List<String> {'User Not Found'});

        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json;charset:utf-8;');
        response.setStatusCode(500);
        response.setBody(JSON.serialize(body));

        return response;
    }


    private HttpResponse esaverServerError_uncaught(){

        EsaverOutbound_Ctrl.EsaverResponse body = new EsaverOutbound_Ctrl.EsaverResponse();
        body.Success = false;

        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json;charset:utf-8;');
        response.setStatusCode(500);
        response.setBody(JSON.serialize(body));

        return response;
    }
}