@IsTest
public class TSTU_ClaimAttachment {
    public class MOC_EnosixServiceNotification_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOInitMock
    {
        public boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }
        
        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            if (throwException)
            {
                throw new CalloutException();
            }

            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = (SBO_EnosixServiceNotification_Detail.EnosixServiceNotification) obj;
            result.SalesDocument = '12345';
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string command, ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = (SBO_EnosixServiceNotification_Detail.EnosixServiceNotification) obj;
            result.setSuccess(success);
            return result;
        }

        public  ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject  obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = (SBO_EnosixServiceNotification_Detail.EnosixServiceNotification) obj;
            result.setSuccess(success);
            return result;
        }
    }

    public static testMethod void test_handleZipAttachmentInsert()
    {
        MOC_EnosixServiceNotification_Detail mocEnosixServiceNotificationDetail = new MOC_EnosixServiceNotification_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotification_Detail.class, mocEnosixServiceNotificationDetail);

        Test.startTest();

        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.zip',
            VersionData = EncodingUtil.convertFromHex('504B030414000800080096BC7A4700000000000000000000000008001000746578742E74787455580C0055EC5756ECEB5756262ABF22F3C8E40200504B07089A3C22D50500000003000000504B0102150314000800080096BC7A479A3C22D5050000000300000008000C000000000000000040A48100000000746578742E7478745558080055EC5756ECEB5756504B05060000000001000100420000004B0000000000'),
            IsMajorVersion = true
        );
        insert contentVersion;
 
        ContentDocument document = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 1];

        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        
        Warranty_Claim__c warrantyClaim1 = new Warranty_Claim__c();
        warrantyClaim1.SAP_Service_Notification_Number__c = 'SAP_Service_Notification_Number__c';
        insert warrantyClaim1;


        ContentDocumentLink attachmentLink1 = new ContentDocumentLink();
        attachmentLink1.LinkedEntityId = warrantyClaim1.Id;
        attachmentLink1.ContentDocumentId = document.Id;
        insert attachmentLink1;

        cdlList.add(attachmentLink1);
        
        Warranty_Claim__c warrantyClaim2 = new Warranty_Claim__c();
        insert warrantyClaim2;

        ContentVersion contentVersion2 = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersion2;
 
        ContentDocument document2 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 1];

        ContentDocumentLink attachmentLink2 = new ContentDocumentLink();
        attachmentLink2.LinkedEntityId = warrantyClaim2.Id;
        attachmentLink2.ContentDocumentId = document.Id;
        insert attachmentLink2;

        cdlList.add(attachmentLink2);

        UTIL_ClaimAttachment.handleAttachmentInsert(cdlList);
        Test.stopTest();
    }    
    public static testMethod void test_handleWCAttachmentInsert()
    {
        MOC_EnosixServiceNotification_Detail mocEnosixServiceNotificationDetail = new MOC_EnosixServiceNotification_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotification_Detail.class, mocEnosixServiceNotificationDetail);

        Test.startTest();

        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersion;
 
        ContentDocument document = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 1];

        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        
        Warranty_Claim__c warrantyClaim1 = new Warranty_Claim__c();
        warrantyClaim1.SAP_Service_Notification_Number__c = 'SAP_Service_Notification_Number__c';
        insert warrantyClaim1;


        ContentDocumentLink attachmentLink1 = new ContentDocumentLink();
        attachmentLink1.LinkedEntityId = warrantyClaim1.Id;
        attachmentLink1.ContentDocumentId = document.Id;
        insert attachmentLink1;

        cdlList.add(attachmentLink1);
        
        Warranty_Claim__c warrantyClaim2 = new Warranty_Claim__c();
        insert warrantyClaim2;

        ContentVersion contentVersion2 = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersion2;
 
        ContentDocument document2 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 1];

        ContentDocumentLink attachmentLink2 = new ContentDocumentLink();
        attachmentLink2.LinkedEntityId = warrantyClaim2.Id;
        attachmentLink2.ContentDocumentId = document.Id;
        insert attachmentLink2;

        cdlList.add(attachmentLink2);

        UTIL_ClaimAttachment.handleAttachmentInsert(cdlList);
        Test.stopTest();
    }
    
    public static testMethod void test_handleStartupAttachmentInsert()
    {
        MOC_EnosixServiceNotification_Detail mocEnosixServiceNotificationDetail = new MOC_EnosixServiceNotification_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotification_Detail.class, mocEnosixServiceNotificationDetail);

        Test.startTest();

        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersion;
 
        ContentDocument document = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 1];

        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();

        Asset asset = new Asset();
        asset.Name = '8888';
        asset.put(UTIL_AssetSyncBatch.SFSyncKeyField,'8888-9999');
        insert asset;
        
        Machine_Startup_Form__c machineStartupForm = TSTU_MachineStartupForm.populateStartupForm();

        Machine_Startup_Form__c startupForm1 = new Machine_Startup_Form__c();
        startupForm1.SAP_Service_Notification_Number__c = 'SAP_Service_Notification_Number__c';
        startupForm1.Serial_Number__c = asset.Id;
        insert startupForm1;

        ContentDocumentLink attachmentLink1 = new ContentDocumentLink();
        attachmentLink1.LinkedEntityId = startupForm1.Id;
        attachmentLink1.ContentDocumentId = document.Id;
        insert attachmentLink1;

        cdlList.add(attachmentLink1);
        
        Warranty_Claim__c warrantyClaim2 = new Warranty_Claim__c();
        insert warrantyClaim2;

        ContentVersion contentVersion2 = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersion2;
 
        ContentDocument document2 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 1];

        ContentDocumentLink attachmentLink2 = new ContentDocumentLink();
        attachmentLink2.LinkedEntityId = warrantyClaim2.Id;
        attachmentLink2.ContentDocumentId = document.Id;
        insert attachmentLink2;

        cdlList.add(attachmentLink2);

        UTIL_ClaimAttachment.handleAttachmentInsert(cdlList);
        Test.stopTest();
    }
}