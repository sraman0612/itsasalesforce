public without sharing class EmailMessage_TriggerHandlerHelper {
    
    public enum CompanyDivision {CTS_OM} //, CLUB_CAR}
    //To count the recursion entry.
    public static Integer counter = 0;
    //Latest related case ID.     
    private static String previousCaseId ;
    
    //commneted as part of descoped data in August by Capgemini
   /* public static Boolean isPowerToolsOpportunityEmail(String subject)
    {
        if(subject.contains('~ 1.RFQ attached ~')) return true;
        else if(subject.contains('~ 3.RFQ incomplete ~')) return true;
        else if(subject.contains('~ 4.Quote complete ~')) return true;
        else if(subject.contains('~ 5.Quote incomplete ~')) return true;
        return false;
    }*/
    
    // Since ParentID cannot be updated we must reparent the email on Before insert
    public static void createNewCaseAndReparentEmail(List<EmailMessage> emails,Set<Id> caseRecordTypeIds, Id defaultCaseOwnerId, CompanyDivision cDivision){
        /*Start of ZEKS Split changes*/
        ID recordTypeId = null;
        Set<Id>NWCRtIdSet = new Set<Id>();
        Set<Id>ZEKSRtIdSet = new Set<Id>();
        Set<Id>AmericasRtIdSet = new Set<Id>();
        Set<Id> AIRTIdSet = new Set<Id>();
        Id ZEKSRtId,AmericasRtId,NWCRtId;
        //Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        //added by @Capgemini
        Email_Message_Setting__mdt settings = [SELECT DeveloperName,CTS_Default_Case_Owner_ID__c,CTS_OM_Email_Case_Record_Type_IDs__c,CTS_OM_ZEKS_Default_Case_Owner_Id__c,CTS_OM_Americas_Default_Case_Owner_Id__c,CTS_OM_Email_Case_Record_Type_IDs_1__c,	CTS_OM_Email_Case_Record_Type_IDs_2__c,	CTS_OM_Email_Case_Record_Type_IDs_3__c,	CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Email_Case_Record_Type_IDs_5__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c,CTS_OM_NWC_Default_Case_Owner_Id__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case' LIMIT 1];
        
        
        //Email_Message_Setting__mdt settings= Email_Message_Setting__mdt.getInstance('Email_Message_Setting_For_Case');
        //end
        Id ZEKSdefaultCaseOwnerId = settings != null && String.isNotBlank(settings.CTS_OM_ZEKS_Default_Case_Owner_Id__c) ? settings.CTS_OM_ZEKS_Default_Case_Owner_Id__c : null;
        Id AmericasdefaultCaseOwnerId = settings != null && String.isNotBlank(settings.CTS_OM_Americas_Default_Case_Owner_Id__c) ? settings.CTS_OM_Americas_Default_Case_Owner_Id__c : null;
        Id NWCdefaultCaseOwnerId = settings != null && String.isNotBlank(settings.CTS_OM_NWC_Default_Case_Owner_Id__c) ? settings.CTS_OM_NWC_Default_Case_Owner_Id__c : null;
        for(RecordType rt:RecordTypeUtilityClass.getRecordTypeList('Case')){
            if(rt.DeveloperName=='CTS_Non_Warranty_Claims'||rt.DeveloperName=='CTS_Non_Warranty_Claims_Closed' ){
                if(rt.DeveloperName=='CTS_Non_Warranty_Claims'){
                    NWCRtId = rt.Id;    
                }
                NWCRtIdSet.add(rt.Id); 
            }
            if(rt.DeveloperName=='CTS_OM_ZEKS'||rt.DeveloperName=='CTS_OM_ZEKS_Closed' ){
                if(rt.DeveloperName=='CTS_OM_ZEKS'){
                    ZEKSRtId = rt.Id;    
                }
                ZEKSRtIdSet.add(rt.Id); 
            }
            if(rt.DeveloperName=='CTS_OM_Americas' || rt.DeveloperName=='CTS_OM_Americas_Closed'||rt.DeveloperName=='CTS_OM_Americas_Transfers'){
                if(rt.DeveloperName=='CTS_OM_Americas'){
                    AmericasRtId = rt.Id;    
                }
                AmericasRtIdSet.add(rt.Id);
            }
            if(rt.DeveloperName=='Action_Item'||rt.DeveloperName=='Internal_Case' || rt.DeveloperName=='Action_Item_Locked' || rt.DeveloperName=='Internal_Case_Locked'){
                AIRTIdSet.add(rt.Id);
            }
            
        }
        /*End of ZEKS Split changes*/        
        
        Set<Id> caseIds = new Set<Id>();
        List<EmailMessage> emailsToProcess = new List<EmailMessage>();
        
        for (EmailMessage email : emails){
            //Need to filter out Power Tools Opportunity emails
            if (email.Incoming ) { //&& !isPowerToolsOpportunityEmail(email.Subject)){ //Commented as part of descoped data in august by Capgemini
                emailsToProcess.add(email);
            }
        }
        
        
        if (!emailsToProcess.isEmpty()){
            Map<Id, EmailMessage> parentIdEmailMessage = new Map<Id, EmailMessage>();
            Set<String> fromAddressEmailList = new Set<String>(); 
            
            for (EmailMessage email : emailsToProcess){
                if(email.ParentId!=null){
                    caseIds.add(email.ParentId);
                }
                parentIdEmailMessage.put(email.ParentId, email);
                fromAddressEmailList.add(email.FromAddress);
            }
            
            
            Map<Id, Case> openChildCases = new Map<Id, Case>();         
            /*Updated by Aman kumar Capgemini.
            * US SFAITSA-430 Changes START.
            */            
            if (!caseIds.isEmpty()){
                
                // Filter out cases to only ones that are closed and have applicable record types
                
                Map<Id, Case> cases = new Map<Id, Case>([Select Id, OwnerId, Owner.isActive, RecordTypeId, ContactId, AccountId, Subject,Status,ParentId,Parent.Status,
                                                         Parent.OwnerId,Parent.ContactId,Parent.AccountId,Parent.Subject,Parent.Description,Parent.RecordTypeId,Parent.parent.RecordTypeId,
                                                         Parent.parent.parent.RecordTypeId,Parent.parent.Status,Parent.parent.parent.Status From Case Where Id in :caseIds and RecordTypeId in :caseRecordTypeIds and (Status = 'Closed' OR Status = 'Closed as Merged')]);
              
                
                // Map craeted to get contact id from new email sent for related cases.
              //  Map<String, contact> contactIDs = new Map<String, Contact>();
                contact con = new Contact();
                if(!fromAddressEmailList.isEmpty())
                    try{
                        con = [Select id, email, accountid from contact where email IN: fromAddressEmailList LIMIT 1];
                    }
                catch(System.QueryException qe){
                    
                }
                catch(Exception e){
                    
                }
             
              
                //    contactIDs.put(con.email, con);   
                
                // To get the Lattest related case.
                getLatestCaseNode(new List<Id>(caseIds)[0]);
                //List<Id> extCases=new List<Id>();
               // Case parentCase;
                //extCases.addAll(caseIds);
               // Case extCase=[SELECT Id,ParentId from Case where Id=:extCases[0]];
                // if(extCase.RecordTypeName__c == 'Action Item'){
                 //    parentCase=[SELECT Id,Status from Case where Id=:extCase.ParentId];
               // }
                // Get the Status of Latest Case.
                List<Case> latestCase = new List<Case>();
                latestCase = [Select id,Status,ContactId,AccountId,Subject,Description,RecordTypeId,OwnerId,ParentId From Case where id =:previousCaseId]; 
                
                // Create a new case for each case associated
                Case[] newCases = new Case[]{};
                  
                for (Case c : cases.values()){
                    
                    // Do not create a new child case if an open one already exists
                    Case openChildCase = openChildCases.get(c.Id);  
                    
                    if (openChildCase == null){
                        system.debug('*** true/False***'+ AIRTIdSet.contains(c.RecordTypeId));
                        system.debug('*** AIRTIdSet***'+ AIRTIdSet);
                        system.debug('*** c status***'+ c.Status);
                        system.debug('*** c parent status***'+ c.Parent.Status);
                        if(AIRTIdSet.contains(c.RecordTypeId) && c.Status=='Closed' && (c.Parent.Status=='Closed' || c.Parent.Status == 'Closed as Merged')){
                            
                            //Only for GD Cases - If Parent's case recordtype is locked rcord type then assgined corrosponding normal record type
                            String normalRecordTypeId = convertLockedRT(c.Parent.RecordTypeId);
                            
                            system.debug('normalRecordTypeId==='+normalRecordTypeId);
                            System.debug('entered ext');
                             Case newCase = new Case(
                                    OwnerId = c.Parent.OwnerId, 
                                    Related_Case__c = c.ParentId,
                                    Email_Waiting_Icon__c=true,
                                    RecordTypeId =normalRecordTypeId,
                                    Status='New', // US_1215
                                    Origin = 'Email',
                                   // ContactId = c.Parent.ContactId ,
                    			//ContactId = contactIDs.get(parentIdEmailMessage.get(c.id).FromAddress).Id,
                    			   ContactId = con.id != null ? con.id : c.Parent.ContactId,
                                    AccountId = con.AccountId != null ? con.AccountId : c.Parent.AccountId,
                                  //  Subject = c.Parent.Subject,
                                   Subject = parentIdEmailMessage.get(c.id).subject,
                                    Description = c.Parent.Description != null ? c.Parent.Description :String.isNotBlank(parentIdEmailMessage.get(c.Id).TextBody) ? parentIdEmailMessage.get(c.Id).TextBody.left(32000) : ''
                                );
                            System.debug('DML Options');
                            Database.DMLOptions dmo = new Database.DMLOptions();
                            dmo.assignmentRuleHeader.useDefaultRule  = false;
                            newCase.setOptions(dmo);
                            System.debug(dmo);
                            newCases.add(newCase);
                        }
                        else{
                            if(!AIRTIdSet.contains(c.RecordTypeId) && !latestCase.isEmpty() && latestCase[0] != null && (latestCase[0].status == 'Closed' || latestCase[0].status == 'Closed as Merged')){
                                /*Start of ZEKS Split and NWC changes*/
                                if(AmericasRtIdSet.Contains(c.RecordTypeId)){
                                    recordTypeId = AmericasRtId;
                                        defaultCaseOwnerId = ZEKSdefaultCaseOwnerId;
                                    }
                                    if(ZEKSRtIdSet.Contains(c.RecordTypeId)){
                                        recordTypeId = ZEKSRtId;
                                        defaultCaseOwnerId = AmericasdefaultCaseOwnerId;
                                    }
                                    
                                if(NWCRtIdSet.Contains(c.RecordTypeId)){
                                    recordTypeId = NWCRtId;
                                    defaultCaseOwnerId = NWCdefaultCaseOwnerId;
                                }
                                    /*End of ZEKS Split changes*/
                                //Only for GD Cases - If Parent's case recordtype is locked rcord type then assgined corrosponding normal record type
                                String thisRTId = latestCase[0].RecordTypeId != null ? latestCase[0].RecordTypeId :c.RecordTypeId;
                                String normalRecordTypeId = convertLockedRT(thisRTId);
                                
                                    Case newCase = new Case(
                                        OwnerId = latestCase[0].OwnerId != null ? latestCase[0].OwnerId :(c.Owner.isActive || defaultCaseOwnerId == null || c.OwnerId.getSObjectType() == Group.getSObjectType()) ? c.OwnerId : defaultCaseOwnerId, 
                                        Related_Case__c = latestCase[0].id != null ? latestCase[0].id : c.id,
                                        //RecordTypeId = latestCase[0].RecordTypeId != null ? latestCase[0].RecordTypeId :/*recordTypeId != null ? recordTypeId :*/ c.RecordTypeId,
                                        RecordTypeId = normalRecordTypeId,
                                       // ContactId = latestCase[0].ContactId != null ? latestCase[0].ContactId : c.ContactId,
                                        ContactId = con.Id != null ? con.Id : latestCase[0].ContactId != null ? latestCase[0].ContactId : c.ContactId,
                                      //  AccountId = latestCase[0].AccountId != null ? latestCase[0].AccountId :c.AccountId,
                                        AccountId = con.AccountId != null ? con.AccountId : latestCase[0].AccountId != null ? latestCase[0].AccountId :c.AccountId,
                                        Status='New',
                                        Origin = 'Email',
                                       // Subject = latestCase[0].Subject != null ? latestCase[0].Subject :c.Subject,
                                         Subject = parentIdEmailMessage.get(latestCase[0].id) != null ? parentIdEmailMessage.get(latestCase[0].id).subject :latestCase[0].Subject,
                                        Description = latestCase[0].Description != null ? latestCase[0].Description :String.isNotBlank(parentIdEmailMessage.get(c.Id).TextBody) ? parentIdEmailMessage.get(c.Id).TextBody.left(32000) : ''
                                    );
                                    system.debug('****** case owner is*****'+ newCase.OwnerId);
                                //CK 7/5/22 Added logic to set email if contact not found
                                if(con.Id == null){
                                	newCase.SuppliedEmail = emailsToProcess[0].FromAddress;
                                }
                                
                                System.debug('DML Options');
                            Database.DMLOptions dmo = new Database.DMLOptions();
                            dmo.assignmentRuleHeader.useDefaultRule  = false;
                            newCase.setOptions(dmo);
                            System.debug(dmo);
                                    newCases.add(newCase);
                            }
                        }
                        
                    }
                         else if(!latestCase.isEmpty() && latestCase[0] != null && latestCase[0].status == 'Open'){
                             
                            latestCase[0].Email_Waiting_Icon__c = true;
                            newCases.add(latestCase[0]);
                        }
                    }   
                
                
                if (newCases.size() > 0){
                    System.debug('upsert operation');
                    Database.upsert(newCases, false);
                }

                // Build a map of parent Id and child Id
                Map<Id, Id> parentIdChildId = new Map<Id, Id>();    
                
                for (Case newCase : newCases){
                    if (newCase.Id != null && newCase.id != latestCase[0].id){
                        parentIdChildId.put(newCase.Related_Case__c, newCase.Id);
                        
                    }
                }       
                
                for (EmailMessage email : emailsToProcess){
                    if(parentIdChildId == null || parentIdChildId.isEmpty()){
                        email.ParentId =  previousCaseId;   
                        
                    }
                    
                    for(String newCase : parentIdChildId.keySet()){
                        if(previousCaseId == newCase){
                            email.ParentId =  parentIdChildId.get(previousCaseId);   
                           
                        }
                        else{
                            email.ParentId = parentIdChildId.get(newCase);
                           
                        }
                       
                        /*Updated by Aman kumar Capgemini.
                        * US SFAITSA-430 Changes END.
                        */
                    }
                }
            }    
        }
    }    
    
    public static void preventDuplicateEmails(List<EmailMessage> emails, Set<Id> caseRecordTypeIds){
                    
        system.debug('---preventDuplicateEmails');            
                    
        Map<Id, Case> caseMap = getCaseMap(emails, caseRecordTypeIds);
        
        if (!caseMap.isEmpty()){
        
            List<EmailMessage> emailsFromThreads = new List<EmailMessage>();
            
            for (EmailMessage email : emails) {
                
                // Process incoming case emails with headers
                if (email.Incoming && email.ParentId != null && email.ParentId.getSObjectType() == Case.getSObjectType() && email.Headers != null && caseMap.keySet().contains(email.ParentId)) {
                    
                    system.debug('Case ID: ' + email.ParentId);
                    system.debug('Headers are:\n' + email.Headers);
                    
                    String headersUpper = email.Headers.toUpperCase();
                    
                    // Check to see if this is the original email in a thread
                    if (headersUpper.substringBetween('MESSAGE-ID: <', '>') != null){
                        
                        email.Message_ID__c = headersUpper.substringBetween('MESSAGE-ID: <', '>');
                        email.Thread_ID__c = headersUpper.substringBetween('MESSAGE-ID: <', '>');
                        system.debug('Found original/first message in thread: ' + email.Thread_ID__c);
                    }
                    
                    // Check to see if this is a subsequent/reply/forward email from a thread
                    if (email.ParentId != null && headersUpper.substringBetween('REFERENCES: <', '>') != null) {
                        system.debug('In Duplicate email------------>');
                        email.Thread_ID__c = headersUpper.substringBetween('REFERENCES: <', '>');
                        emailsFromThreads.add(email);              
                    }                  
    
                    system.debug('Message ID: ' + email.Message_ID__c);   
                    system.debug('Thread ID: ' + email.Thread_ID__c);             
                }           
            }   
            
            if (!emailsFromThreads.isEmpty()){              
    
                system.debug('Processing emails from threads: ' + emails);
            	List<Group> groupList = [select id from group where DeveloperName = 'Deleted_Cases'];
                Set<String> threadIds = new Set<String>();
                Set<String> originalParentIds = new Set<String>();
            
                for (EmailMessage em : emailsFromThreads){
                    //em.ToAddress = ''; 
                    if (String.isNotBlank(em.Thread_ID__c)){
                        threadIds.add(em.Thread_ID__C);
                        originalParentIds.add(em.ParentId);
                    }
                }
            
                Map<String, Id> threadParentCaseMap = new Map<String, Id>();
                
                // Find the original case (if this isn't the first email) that came from the same thread
                for (EmailMessage em : [Select Id, Subject, ParentId, Thread_ID__c From EmailMessage Where ParentId != null and Thread_ID__c in :threadIds ORDER BY LastModifiedDate DESC ]){
                    
                    if (!threadParentCaseMap.containsKey(em.Thread_ID__c)){
                        threadParentCaseMap.put(em.Thread_ID__c, em.ParentId);
                    }   
                }
                
                system.debug('threadParentCaseMap: ' + threadParentCaseMap);
                
                if (!threadParentCaseMap.isEmpty()){
                    

                    Map<Id, Case> originalParentCaseMap = new Map<Id, Case>([Select Id, CaseNumber, IsClosed From Case Where Id in :originalParentIds]);
                            
                    Case[] casesToDelete = new Case[]{};
                            
                    // Prevent the email (and case) from being submitted into the database if there is already a case from the same thread
                    for (EmailMessage em : emailsFromThreads){
                        
                        Id parentCaseId = threadParentCaseMap.get(em.Thread_ID__c);
                        
                        if (String.isNotBlank(em.Thread_ID__c) && parentCaseId != em.ParentId){
                            
                            system.debug('Parent case found: ' + parentCaseId + ' for email: ' + em);
                            
                            // Block the new/duplicate case from being created
                            // Skip if the original case is closed
                            Case originalParentCase = originalParentCaseMap.get(em.ParentId);
                            system.debug('originalParentCase: ' + originalParentCase);

                            if (originalParentCase != null && !originalParentCase.IsClosed){
                                casesToDelete.add(new Case(Id = originalParentCase.Id,ownerId = groupList[0].id));
                                //Reparent the email's case to the original from the thread
                                em.ParentId = parentCaseId; 
                            }                            

                            // Commented for case duplication issue
                            //em.ParentId = parentCaseId; 
                            
                                                    
                        }
                    }                       
                    
                    system.debug('casesToDelete: ' + casesToDelete);
                    
                    if (!casesToDelete.isEmpty()){
                        Database.update(casesToDelete, false);
                    }
                }   
            }
        }
    }
    
    // Query cases to get record types so emails not related to the proper case record types can be filtered out of processing
    private static Map<Id, Case> getCaseMap(List<EmailMessage> emails, Set<Id> caseRecordTypeIds){
                
        Set<Id> caseIds = new Set<Id>(); 
        
        for (EmailMessage email : emails) {
            
            // Process incoming case emails with headers
            if (email.Incoming && email.ParentId != null && email.ParentId.getSObjectType() == Case.getSObjectType()) {
                caseIds.add(email.ParentId);
            }        
        }
        
        Map<Id, Case> caseMap = new Map<Id, Case>([Select Id, Subject From Case Where Id in :caseIds and RecordTypeId in :caseRecordTypeIds]);    
        
        system.debug('caseMap: ' + caseMap);
        
        return caseMap;
    }     
    
    /*Craeted By Aman Kumar Capgemini.
     * To get the Latest retalted Case of the Case on which the mail is coming using recursion.
     */
    private static void getLatestCaseNode (String mainCaseId){
        List<Case> caseId = new List<Case>();
        caseId = [Select id,Related_Case__c From Case where Related_Case__c =: mainCaseId];

        if(!caseId.isEmpty() && caseId[0] != null){
            counter++;
            previousCaseId = caseId[0].id ;
            getLatestCaseNode(caseId[0].id);
        }
        if(caseId.isEmpty() && counter == 0){
            previousCaseId = mainCaseId;
        }
    }
    //If Parent's case recordtype is locked rcord type then assgin corrosponding normal record type
    private static Id convertLockedRT(String rtId){
        String strRecordDevName = Schema.SObjectType.Case.getRecordTypeInfosById().get(rtId).getDeveloperName();
        String normalRecordTypeId = rtId;
        if(strRecordDevName.contains('_Locked')){
            String rtNm = strRecordDevName.removeEnd('_Locked');
            system.debug('rtNm==='+rtNm);
            normalRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtNm).getRecordTypeId();
        }
        return normalRecordTypeId;
    }
    
    //Getting ultimate parent case and its status
    //If ultimate parent case is not closed and Resolved then it will return as null parent case Map 
    //if ultimate parent case is closed then it will return case id of that case and Status as Closed
    //if ultimate parent case is Resolved then it will return case id of that case and Status as Resolved
    /*private static Map<id,Map<Id,String>> getUltimateParentCase(List<Case> casesList,Set<Id> AIRTIdSet){
        Map<Id,Map<Id,String>> parentMap = new Map<Id,Map<Id,String>>();
        
        
        for(Case cases:casesList){
            String ParentCaseId;
            String status;
            //Populating a Map of a Map of parent Cases along with the statud
            if(AIRTIdSet.contains(cases.recordtypeid) && !AIRTIdSet.contains(cases.parent.recordtypeid)){
               if(cases.parent.status == 'Closed' || cases.parent.status == 'Closed as Merged'){
                    ParentCaseId = cases.parentId;
                    status = 'Closed';
               }else if(cases.parent.status == 'Resolved'){
                    ParentCaseId = cases.parentId;
                    status = 'Resolved';
               }
            
            }else if(AIRTIdSet.contains(cases.parent.recordtypeid) && !AIRTIdSet.contains(cases.parent.parent.recordtypeid)){
               if (cases.parent.parent.status == 'Closed' || cases.parent.parent.status == 'Closed as Merged'){
                    ParentCaseId = cases.parent.parentId;
                    status = 'Closed';
               }else if(cases.parent.status == 'Resolved'){
                    ParentCaseId = cases.parent.parentId;
                    status = 'Resolved';
               }
            }else if(AIRTIdSet.contains(cases.parent.parent.recordtypeid) && !AIRTIdSet.contains(cases.parent.parent.parent.recordtypeid)){
               if (cases.parent.parent.parent.status == 'Closed' || cases.parent.parent.parent.status == 'Closed as Merged'){
                    ParentCaseId = cases.parent.parent.parentId;
                    status = 'Closed';
                }else if(cases.parent.status == 'Resolved'){
                    ParentCaseId = cases.parent.parent.parentId;
                    status = 'Resolved';
               }
            }
            Map<Id,String> parMap = new Map<Id,String>();
            parMap.put(ParentCaseId,status);
            parentMap.put(cases.id,parMap);
        }
        return parentMap;
    }*/
}