public with sharing class UTIL_CustomerGeocodeBatch
    implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful
{
    // Geocode update for SAP Account Sync process
    // Mainly to ensure the proper geocode location exists on all SAP sourced accounts

    private List<Account> modifiedAccounts = new List<Account>();

    // start()
    //
    // Start the batch job
    public Database.QueryLocator start(Database.BatchableContext context)
    {
        System.debug(context.getJobId() + ' Starts');

        try
        {
            String query = buildQueryString();
            return Database.getQueryLocator(query);
        }
        catch(Exception ex)
        {
            System.debug('Failed querying Accounts that need to be geocoded: ' + ex.getMessage());
        }

        // Returning null causes "System.UnexpectedException: Start did not return a valid iterable object."
        // So to avoid this we must return a query that will always give 0 results. Id should never be
        // null in any table so we can arbitrarily pick Account.
        return Database.getQueryLocator([SELECT Id FROM Account WHERE Id = null]);
    }

    // execute()
    //
    // Execute the batch job
    public void execute(Database.BatchableContext BC, List<Account> AccountUpdate)
    {
        System.debug(BC.getJobId() + ' Executing');
        for (Account a : AccountUpdate)
        {
            // Key for Google Maps Geocoding API
            String geocodingKey = 'AIzaSyA1Q-LAUvpPEyUPr-tVyRFkkpNrrzPDfck';
            String address = '';
            if (a.BillingStreet != null) address += a.BillingStreet + ', ';
            if (a.BillingCity != null) address += a.BillingCity + ', ';
            if (a.BillingState != null) address += a.BillingState + ' ';
            if (a.BillingPostalCode != null) address += a.BillingPostalCode + ', ';
            if (a.BillingCountry != null) address += a.BillingCountry;
            address = EncodingUtil.urlEncode(address, 'UTF-8');
            system.debug('address ===> '+address);

            try{
                Google gro = callGoogleApex(address,geocodingKey);
                System.debug('gro.results.size() ===> '+gro.results.size());
                if(gro.results.size() > 0)
                {
                    Double lat = gro.results[0].geometry.location.lat;
                    Double lon = gro.results[0].geometry.location.lng;

                    if (lat != null)
                    {
                        a.Location__Latitude__s = lat;
                        a.Location__Longitude__s = lon;
                        modifiedAccounts.add(a);
                        System.debug('++++a.BillingLatitude+++++' + a.Location__Latitude__s);
                    }
                }
                else
                {
                    System.debug('Nothing in list. No geocode update necessary');
                }
            }
            catch (Exception e) {
                system.debug('exception in execute method ===>'+e.getMessage());
            }
        }

        if (modifiedAccounts.size() > 0)
        {
            UTIL_SyncHelper.insertUpdateResults('Account', 'Update', null, null, modifiedAccounts, 'UTIL_CustomerGeocodeBatch', null);
        }
    }

    public static Google callGoogleApex(String address,String geocodingKey)
    {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        Google gro = new Google();
        if (address != null)
        {
            req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + geocodingKey + '&sensor=false');
            req.setMethod('GET');
            req.setTimeout(6000); // 60 seconds before the request call times out

            HttpResponse res = h.send(req);
            JSONParser parser = JSON.createParser(res.getBody());
            while(parser.nextToken() != null)
            {
                if(parser.getCurrentToken()==JSONToken.START_OBJECT){
                    gro = (Google) parser.readValueAs(Google.class);
                }
            }
        }
        system.debug('gro ===> '+gro);
        return gro;
    }

    public void finish(Database.BatchableContext BC) {
        System.debug(BC.getJobId() + ' finished');
    }

    // buildQueryString
    //
    // Build the query String
    private String buildQueryString()
    {
        String query = 'SELECT id,BillingStreet,BillingCity,BillingCountry,' +
                'BillingPostalCode,BillingState,Location__Latitude__s,Location__Longitude__s '+
                'FROM Account where Location__Latitude__s = NULL';

        return query;
    }
}