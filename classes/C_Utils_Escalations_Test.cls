@isTest
private class C_Utils_Escalations_Test {

    @testSetup
    static void setupData(){
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Contact cont1 = new Contact();
        cont1.firstName = 'Can';
        cont1.lastName = ' Pango';
        cont1.email = 'service-gdi1@canpango.com';
        cont1.Relationship__c = 'Customer';
        insert cont1;
        
        Contact cont2 = new Contact();
        cont2.firstName = 'Can';
        cont2.lastName = ' Pango';
        cont2.email = 'service-gdi2@canpango.com';
        cont2.Relationship__c = 'Vendor';
        insert cont2;
        
        Contact cont3 = new Contact();
        cont3.firstName = 'Can';
        cont3.lastName = ' Pango';
        cont3.email = 'service-gdi3@canpango.com';
        cont3.Relationship__c = 'Internal';
        insert cont3;
        
        Case cse1 = new Case();
        cse1.Account = acct;
        cse1.Contact = cont1;
        cse1.ContactId = cont1.Id;
        cse1.Type = 'CSE1';
        cse1.Subject = 'Test Case';
        insert cse1;
        
        Case cse2 = new Case();
        cse2.Account = acct;
        cse2.Contact = cont2;
        cse2.ContactId = cont2.Id;
        cse2.Type = 'CSE2';
        cse2.Subject = 'Test Case';
        insert cse2;
        
        Case cse3 = new Case();
        cse3.Account = acct;
        cse3.Contact = cont3;
        cse3.ContactId = cont3.Id;
        cse3.Type = 'CSE3';
        cse3.Subject = 'Test Case';
        insert cse3;
        
        Email_Template_Admin__c eta = new Email_Template_Admin__c();
        eta.Name = 'Test Email_Template_Admin__c';
        insert eta;

    }
    
    static testMethod void testsendTemplatedEmailCSE1 () {
        Case c1 = [SELECT Id, Escalation_Count__c FROM Case WHERE Type = 'CSE1'];
        
        for(integer i = 1; i < 4; ++i){
            c1.Escalation_Count__c = i;
            
            update c1;
        
            C_Utils_Escalations.sendTemplatedEmail(new List<String>{c1.Id});
        }          
    }
    
    static testMethod void testsendTemplatedEmailCSE2 () {
        Case c2 = [SELECT Id, Escalation_Count__c FROM Case WHERE Type = 'CSE2'];

        
        for(integer i = 1; i < 4; ++i){

            c2.Escalation_Count__c = i;

            update c2;

            C_Utils_Escalations.sendTemplatedEmail(new List<String>{c2.Id});

        }          
    }
    
    static testMethod void testsendTemplatedEmailCSE3 () {
        Case c3 = [SELECT Id, Escalation_Count__c FROM Case WHERE Type = 'CSE3'];
        
        for(integer i = 1; i < 4; ++i){
            c3.Escalation_Count__c = i;
            
            update c3;
        
            C_Utils_Escalations.sendTemplatedEmail(new List<String>{c3.Id});
        }          
    }
}