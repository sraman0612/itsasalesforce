//Allows for the 'Finish' button in the 'MPG Code Picker' Flow to redirect back to the Case when finished

 public class C_MPG_Code_Controller{

    public C_MPG_Code_Controller(ApexPages.StandardController controller) {

    }

 // Instantiate the Flow for use by the Controller - linked by VF "interview" attribute
 public Flow.Interview.MPG_Code_Picker flDemo {get;set;}

 // Factor PageReference as a full GET/SET
 public PageReference prFinishLocation {
 get {
 PageReference prRef = new PageReference('/' + strOutputVariable);
 prRef.setRedirect(true);
 return prRef;
 }
 set { prFinishLocation = value; }
 }

 // Factor Flow output variable pull as a full GET / SET
 public String strOutputVariable {
 get {
 String strTemp = '';

 if(flDemo != null) {
 strTemp = string.valueOf(flDemo.getVariableValue('CaseID'));
 }

 return strTemp;
 }

 set { strOutputVariable = value; }
 } 

}