@IsTest
public class C_Email_Attachment_Reassigner_Test {
    static testmethod void attachmentTester() {

        Case testCase = new Case();
        testCase.Subject = 'Test Case';
        insert testCase;

        EmailMessage msg = new EmailMessage(
            ToAddress = 'them.' + Datetime.now().format('yyyymmddhhmmss') + '@sforce.com',
            FromAddress = 'me.' + Datetime.now().format('yyyymmddhhmmss') + '@sforce.com',
            FromName = 'Thomas Train',
            TextBody = 'This is a test of the email attachment reassigner service',
            Incoming = true,
            ParentId = testCase.Id);

        insert msg;

        Attachment a = new Attachment();
        a.name = 'test attachment';
        a.body = blob.valueof('attachment body');
        a.parentid = msg.Id;

        //Test.startTest();
        insert a;   
        //Test.stopTest();

        Attachment testAtt = [SELECT ParentId from Attachment where Id = :a.Id];
        System.debug('CASE: '+ testCase.Id +' ATTACHMENT PARENT: '+ testAtt.ParentId);
        System.assertEquals(testCase.Id, testAtt.ParentId);


    }   
}