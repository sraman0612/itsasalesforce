global with sharing class AccountSharingBatch implements Database.Batchable<sObject> {
      global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator([SELECT Id from Account where Rep_Account2__c != null]);
      }

      global void execute(Database.BatchableContext BC, List<sObject> scope) {

            List<String> accountIds = new List<String>();

            for (sObject account : scope) {
                  accountIds.add(account.Id);
            }

            List<Account> accounts = [SELECT Id, ParentId, Rep_Account2__c, Rep_Account2__r.Associated_Rep_Account__c from Account where Id in : accountIds];

            AccountSharingHelper.shareAccounts(accounts);
      }

      global void finish(Database.BatchableContext BC) {

      }
}