@isTest
global class C_GDIHttpMockAlertTest  implements HttpCalloutMock  {
  global HTTPResponse respond(HTTPRequest req) {

        // Create a fake response.

        // Set response values, and

        HttpResponse res1 = new HttpResponse();
        res1.setHeader('Content-Type', 'application/json');
        res1.setBody('{"alarms":[{"count":89,"creationTime":"2018-08-24T03:57:55.459Z","time":"2018-09-06T03:58:12.000Z","firstOccurrenceTime":"2018-08-24T03:57:46.000Z","history":{"auditRecords":[],"self":"http://industrials.iconn.gardnerdenver.com/audit/auditRecords"},"id":"265832798","self":"http://industrials.iconn.gardnerdenver.com/alarm/alarms/265832798","severity":"WARNING","source":{"id":"142327920","name":"gd-gw-359804085474460","self":"http://industrials.iconn.gardnerdenver.com/inventory/managedObjects/142327920"},"status":"ACTIVE","text":"The service counter is below 200","type":"c8y_LowServiceCounterAlarm"}],"next":"http://industrials.iconn.gardnerdenver.com/alarm/alarms?dateTo=2018-09-06T06:05:00.000Z&pageSize=2000&dateFrom=2018-09-06T01:53:00.000Z&currentPage=74","statistics":{"currentPage":73,"pageSize":2000},"prev":"http://industrials.iconn.gardnerdenver.com/alarm/alarms?dateTo=2018-09-06T06:05:00.000Z&pageSize=2000&dateFrom=2018-09-06T01:53:00.000Z&currentPage=72","self":"http://industrials.iconn.gardnerdenver.com/alarm/alarms?dateTo=2018-09-06T06:05:00.000Z&pageSize=2000&dateFrom=2018-09-06T01:53:00.000Z&currentPage=73"}');
        res1.setStatusCode(200);
        return res1;

    }
}