global class C_createAssetShareGlobalUsrIterable implements Iterator<assetShare>{ 

   List<assetShare> accs {get; set;} 
   Integer i {get; set;} 

   public C_createAssetShareGlobalUsrIterable(List<assetShare> asRecs){ 
       accs = asRecs;
       i = 0; 
   }   

   global boolean hasNext(){ 
       if(i >= accs.size()) {
           return false; 
       } else {
           return true; 
       }
   }    

   global assetShare next(){ 
       // 8 is an arbitrary 
       // constant in this example
       // that represents the 
       // maximum size of the list.
       if(i == 20){return null;} 
       i++; 
       return accs[i-1]; 
   } 

}