/// enosiX Inc. Generated Apex Model
/// Generated On: 6/5/2019 2:20:19 PM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixVCSync_Search extends ensxsdk.EnosixFramework.SearchSBO 
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixVCSync_Search_Meta', new Type[] {
            SBO_EnosixVCSync_Search.EnosixVCSync_SC.class
            , SBO_EnosixVCSync_Search.EnosixVCSync_SR.class
            , SBO_EnosixVCSync_Search.SEARCHRESULT.class
            , SBO_EnosixVCSync_Search.SEARCHPARAMS.class
            , SBO_EnosixVCSync_Search.SEARCHRESULT.class
            } 
        );
    }

    public SBO_EnosixVCSync_Search() 
    {
        super('EnosixVCSync', SBO_EnosixVCSync_Search.EnosixVCSync_SC.class, SBO_EnosixVCSync_Search.EnosixVCSync_SR.class);
    }
    
    public override Type getType() { return SBO_EnosixVCSync_Search.class; }

    public EnosixVCSync_SC search(EnosixVCSync_SC sc) 
    {
        return (EnosixVCSync_SC)super.executeSearch(sc);
    }

    public EnosixVCSync_SC initialize(EnosixVCSync_SC sc) 
    {
        return (EnosixVCSync_SC)super.executeInitialize(sc);
    }

    public class EnosixVCSync_SC extends ensxsdk.EnosixFramework.SearchContext 
    { 		
        public EnosixVCSync_SC() 
        {		
            super(new Map<string,type>		
                {		
                    'SEARCHPARAMS' => SBO_EnosixVCSync_Search.SEARCHPARAMS.class		
                });		
        }

        public override Type getType() { return SBO_EnosixVCSync_Search.EnosixVCSync_SC.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixVCSync_Search.registerReflectionInfo();
        }

        public EnosixVCSync_SR result { get { return (EnosixVCSync_SR)baseResult; } }


        @AuraEnabled public SBO_EnosixVCSync_Search.SEARCHPARAMS SEARCHPARAMS 
        {
            get
            {
                return (SBO_EnosixVCSync_Search.SEARCHPARAMS)this.getStruct(SBO_EnosixVCSync_Search.SEARCHPARAMS.class);
            }
        }
        
        }

    public class EnosixVCSync_SR extends ensxsdk.EnosixFramework.SearchResult 
    {
        public EnosixVCSync_SR() 
        {
            super(new Map<string,type>{'SEARCHRESULT' => SBO_EnosixVCSync_Search.SEARCHRESULT.class } );
        }
        
        public ensxsdk.EnosixFramework.FrameworkCollection SearchResults
        {
            get { return super.getCollection(SBO_EnosixVCSync_Search.SEARCHRESULT.class); }
        }
        
        public List<SEARCHRESULT> getResults() 
        {
            List<SEARCHRESULT> results = new List<SEARCHRESULT>();
            SearchResults.copyTo(results);
            return results;
        }

        public override Type getType() { return SBO_EnosixVCSync_Search.EnosixVCSync_SR.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixVCSync_Search.registerReflectionInfo();
        }
    }

    public class SEARCHPARAMS extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixVCSync_Search.SEARCHPARAMS.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixVCSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public String Material
        { 
            get { return this.getString ('MATNR'); } 
            set { this.Set (value, 'MATNR'); }
        }

        @AuraEnabled public String Plant
        { 
            get { return this.getString ('WERKS'); } 
            set { this.Set (value, 'WERKS'); }
        }

    }

    public class SEARCHRESULT extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixVCSync_Search.SEARCHRESULT.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixVCSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public String Material
        { 
            get { return this.getString ('MATNR'); } 
            set { this.Set (value, 'MATNR'); }
        }

        @AuraEnabled public String Plant
        { 
            get { return this.getString ('WERKS'); } 
            set { this.Set (value, 'WERKS'); }
        }

        @AuraEnabled public String CharacteristicName
        { 
            get { return this.getString ('ATNAM'); } 
            set { this.Set (value, 'ATNAM'); }
        }

        @AuraEnabled public String CharacteristicDescription
        { 
            get { return this.getString ('ATBEZ'); } 
            set { this.Set (value, 'ATBEZ'); }
        }

        @AuraEnabled public String CharacteristicValue
        { 
            get { return this.getString ('ATWRT'); } 
            set { this.Set (value, 'ATWRT'); }
        }

        @AuraEnabled public String CharacteristicValueDescription
        { 
            get { return this.getString ('ATWTB'); } 
            set { this.Set (value, 'ATWTB'); }
        }

    }

    public class SEARCHRESULT_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public SEARCHRESULT_COLLECTION()
        {
            super('SEARCHRESULT', SBO_EnosixVCSync_Search.SEARCHRESULT.class, null);
        }

        public List<SBO_EnosixVCSync_Search.SEARCHRESULT> getAsList()
        {
            return (List<SBO_EnosixVCSync_Search.SEARCHRESULT>)this.buildList(List<SBO_EnosixVCSync_Search.SEARCHRESULT>.class);
        }
    }


}