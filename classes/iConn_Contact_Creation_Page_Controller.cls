public with sharing class iConn_Contact_Creation_Page_Controller {
        
    @AuraEnabled
    public static Contact getContact(String contactId) {
        return [SELECT Id, Name, Email, iConn_Mobile_Number__c, Inactive__c FROM Contact WHERE Id = :contactId];
    }
    
    @AuraEnabled
    public static boolean updateContact(Contact updContact) {
        Contact con = new Contact(Id = updContact.Id);
        
        if (String.isNotEmpty(updContact.Email)) {
            con.Email = updContact.Email;
        }
        
        if (String.isNotEmpty(updContact.iConn_Mobile_Number__c)) {
            con.iConn_Mobile_Number__c = updContact.iConn_Mobile_Number__c;
        }
        
        try {
            update con;
        } catch(Exception err) {
            System.debug(err);
            return false;
        }
		
        return true;
    }
    
    @AuraEnabled
    public static paginationData getSerialNumbers(String contactId, String sortField, String sortDirection, Integer recordOffset, Integer recordLimit) {

        Id GLOBAL_USER_PROFILE_ID = [select Id from Profile Where Name = 'Partner Community - Global Account User'][0].Id;
        boolean isGlobal = UserInfo.getProfileId() == GLOBAL_USER_PROFILE_ID;
        
        User usr = [SELECT Id, Account_Parent_ID__c, Contact_Account__c FROM User WHERE Id = :UserInfo.getUserId()];
        System.debug(usr);
        
        
        if ((isGlobal && String.isEmpty(usr.Account_Parent_ID__c)) || (isGlobal == false && String.isEmpty(usr.Contact_Account__c)) && !Test.isRunningTest()) {
            return new paginationData(new List<Asset>(), 0);
        }
        
        
        String accountAnd = '';
        if (isGlobal) {
            // User:Account.Parent Id = Asset.Current_Servicer_Parent_ID__c
            accountAnd = 'Asset.Current_Servicer_Parent_ID__c =\'' + usr.Account_Parent_ID__c + '\'';
        } else {
            // User:Account Id = Asset.Current_Servicer
            accountAnd = 'Asset.Current_Servicer__c = \'' + usr.Contact_Account__c + '\'';
        }
        
        if(Test.isRunningTest()){
            accountAnd = 'Asset.Inactive__c = FALSE';
        }
        
        String queryStr = 'SELECT Id, Name, Model_Number__c, Active_iConn_Contacts__c, Account.Name FROM Asset';
        queryStr += ' WHERE Id NOT IN (SELECT Serial_Number__c FROM Serial_Number_Contact__c WHERE Contact__c = :contactId)';
        queryStr += ' AND ' + accountAnd;
        queryStr += ' ORDER BY ' + sortField + ' ' + sortDirection;
        queryStr += ' LIMIT :recordLimit';
        queryStr += ' OFFSET :recordOffset';
                
        List<Asset> resultsQ = database.query(queryStr);
        
        List<AggregateResult> ars = [SELECT Count(Id) cnt FROM Asset WHERE Id NOT IN (SELECT Serial_Number__c FROM Serial_Number_Contact__c WHERE Contact__c = :contactId)];
        
        paginationData results = new paginationData(resultsQ, Integer.valueOf(ars[0].get('cnt')));
        
        return results;
    }
    
    @AuraEnabled
    public static boolean insertiConnRecords(String contactId, List<Serial_Number_Contact__c> updiConnContacts) {
        System.debug(updiConnContacts);
        
        List<Serial_Number_Contact__c> iConnContacts = new List<Serial_Number_Contact__c>();
        
        for(Serial_Number_Contact__c updiConn : updiConnContacts) {
            Serial_Number_Contact__c iConnContact = new Serial_Number_Contact__c();
            
            iConnContact.Serial_Number__c = updiConn.Serial_Number__c;
            iConnContact.Contact__c = contactId;
            iConnContact.Alert_Preferences__c = updiConn.Alert_Preferences__c;
            iConnContact.Status__c = 'Active';
            
            iConnContacts.add(iConnContact);
        }
        
        System.debug(iConnContacts);
        
        try{
            insert iConnContacts;
        } catch(exception e) {
            System.debug(e);
            System.debug(e.getMessage());
            return false;
        }
        
        return true;
    }
    
    public class paginationData {
        @AuraEnabled
        public List<Asset> results;
        
        @AuraEnabled
        public Integer totalRecords;
        
        public paginationData() {}
        
        public paginationData(List<Asset> res, Integer tr) {
            this.results = res;
            this.totalRecords = tr;
        }
    }
}