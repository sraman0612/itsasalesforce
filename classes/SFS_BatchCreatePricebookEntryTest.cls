@isTest
public with sharing class SFS_BatchCreatePricebookEntryTest {
	 static testmethod void test() {
        ID HibonProdRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Product2' AND DeveloperName = 'Hibon'].id;

        ID NaAirProdRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Product2' AND DeveloperName = 'NA_Air'].id;

        ID testStandardPBId = Test.getStandardPricebookId();
        Integer numOfPBs = 2; //Number of pricebooks used in the Batch Class
        Integer numOfProds;
		
        //Create a Service pricebook
        Pricebook2 newPB = new Pricebook2(Name = 'Service Price Book', isActive = TRUE);
        insert newPB;


        //Create new material products and a pricebook entry for each
        for(Integer i=1; i<3; i++) {
            Product2 hProd = new Product2(Name = 'TestBatchProduct' + i, IsActive = TRUE, RecordTypeId = HibonProdRecordTypeId);
            insert hProd;
            
            PriceBookEntry HibonPBEntry = new PriceBookEntry(PriceBook2Id = testStandardPBId, Product2Id = hProd.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = TRUE);
            insert HibonPBEntry;

            
            numOfProds = i;
        }

            //Create new labor products
            numOfProds++;
			
         
         	List <PricebookEntry> testPriceBooks = new List <PricebookEntry>();
         	list <Product2> testProducts = new List <Product2> ();
         
            Product2 naProd = new Product2 (Name = 'TestBatchProduct' + numOfProds, IsActive = TRUE, RecordTypeId = NaAirProdRecordTypeId, ProductCode = 'TEST');
         	testProducts.add(naProd);   
         	insert naProd;
         	
            PriceBookEntry naPBE = new PriceBookEntry(PriceBook2Id = testStandardPBId, Product2Id = naProd.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = TRUE);
            testPriceBooks.add(naPBE);
         	insert naPBE;

            numOfProds++;
            Product2 naProd2 = new Product2 (Name = 'TestBatchProduct' + numOfProds, IsActive = TRUE, RecordTypeId = NaAirProdRecordTypeId, ProductCode = NULL);   
         	insert naProd2;
            PriceBookEntry naPBE2 = new PriceBookEntry(PriceBook2Id = testStandardPBId, Product2Id = naProd2.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = TRUE);
         	insert naPBE2;
         
            Test.startTest();
			SFS_BatchCreatePricebookEntryScheduler bpes = new SFS_BatchCreatePricebookEntryScheduler();
         	String jobId = System.schedule('SchedulePricebookEntryCreation', '0 00 00 * * ?', bpes);
            SFS_BatchCreatePricebookEntry b = new SFS_BatchCreatePricebookEntry();
            Database.executeBatch(b);
        	
         	
             for(Product2 pr : testProducts){
                 System.assertEquals(naProd.Name, pr.Name);
             }
             for(PricebookEntry pbe : testPriceBooks){
                 System.AssertEquals(naPBE.Product2Id, pbe.Product2Id);
             }
         	
            

            Test.stopTest();

            //Get a list of all the active currencies in the org
            List<CurrencyType> fullCurrencyList = [SELECT Id, IsoCode, ConversionRate, DecimalPlaces, IsActive FROM CurrencyType];

            Integer numCreatedminus = (fullCurrencyList.size() * ((numOfProds-1) * numOfPBs)) + 1;	//number of currencies in the org multiplied by 
                                                                                                    //the number of products created (minus 1 because of the Labor product without a Labor Activity Code) 
                                                                                                    //and number of pricebooks
            Integer numCreated = fullCurrencyList.size() * (numOfProds * numOfPBs);


            // Verify pricebook entries created
            List<PriceBookEntry> pbeList = [SELECT Id, Name FROM PricebookEntry WHERE Product2.Name LIKE 'TestBatchProduct%'];
          
            //System.assert(pbeList.size() == numCreatedminus , 'Number of PriceBook Entries created minus 1 Labor Product: ' + numCreatedminus);
            System.assert(pbeList.size() <> numCreated, 'Testing that each Product does not have Pricebooks Created' + numCreated);

    }
}