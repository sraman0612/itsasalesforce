@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('https://example.com/example/test', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
         res.setBody('{"items": [{"itemIdentifier": "1234","partNumber": "00250506","unitPrice": 0.000000,"calculationInfo": [{"TaxExempt": "true","ExpirationDate": "1/30/2027 12:00:00 AM","certifyTaxErrorMessage": "03/29/2022 14:49:22 : Success"}]}]}');
        res.setStatusCode(200);
        return res;
    }
}