@isTest
public class SFS_CreatePartRequestLineItemsTest {
    static testmethod void createPRLITest(){
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[1].ShippingCountry = 'USA';
        accList[1].ShippingCity ='Montreat';
        accList[1].ShippingPostalCode='28757';
        accList[1].ShippingState='NC';
        accList[1].ShippingStreet='2342 Appalachian Way\nUnit 156';
        accList[1].IRIT_Customer_Number__c='54545454';
        //accList[1].RecordType.DeveloperName = 'CTS_Bill_To_Account';
        accList[1].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        insert accList[1];
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].ShippingCountry = 'USA';
        accList[0].ShippingCity ='Las Vegas';
        accList[0].ShippingPostalCode='89107';
        accList[0].ShippingState='NV';
        accList[0].ShippingStreet='4400 Meadows Ln';
        accList[0].IRIT_Customer_Number__c='45454545';
        insert accList[0];
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        List<WorkOrder> woList = SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,null,divisionsList[0].Id , null,true);
        List<WorkType> workTypeList = SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        List<WorkOrderLineItem> woliList = SFS_TestDataFactory.createWorkOrderLineItem(2,woList[0].Id,workTypeList[0].Id,false);
        insert woliList;
        List<Product2> productList = SFS_TestDataFactory.createProduct(3,true);
        List<ProductRequired> partRequiredList=SFS_TestDataFactory.createProductRequired(5,null,productList[0].Id,false);
        partRequiredList[0].ParentRecordId=woliList[0].Id;
        partRequiredList[1].Product2Id=productList[1].Id;
        partRequiredList[1].ParentRecordId=woliList[0].Id;
        
        partRequiredList[2].ParentRecordId=woliList[1].Id;
        partRequiredList[3].Product2Id=productList[1].Id;
        partRequiredList[3].ParentRecordId=woliList[1].Id;
        
        partRequiredList[4].Product2Id=productList[2].Id;
        partRequiredList[4].ParentRecordId=woList[0].Id;
        
        insert partRequiredList;
        
        List<ProductRequest> prList = SFS_TestDataFactory.createProductRequest(4,woList[0].Id,woliList[0].Id,false);
        prList[1].WorkOrderLineItemId=woliList[1].Id;
        prList[2].WorkOrderLineItemId=null;
        prList[3].WorkOrderLineItemId=null;
        insert prList;
        List<ProductRequestLineItem> prliList= SFS_TestDataFactory.createPRLI(1,prList[1].Id,productList[0].Id,false);
        insert prliList;
        List<Id> prIds = new List<Id>();
        prIds.add(prList[0].Id);
        prIds.add(prList[1].Id);
        prIds.add(prList[2].Id);
        prIds.add(prList[3].Id);
        Test.startTest();
        SFS_CreatePartRequestLineItems.createPRLI(prIds);
        Test.stopTest();
    }
}