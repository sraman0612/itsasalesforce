global with sharing class ensxtx_ENSX_Characteristic
{
    public String CharacteristicID { get; set; }
    public String CharacteristicName { get; set; }
    public String CharacteristicValue { get; set; }
    public String CharacteristicValueDescription { get; set; }
    public Boolean UserModified { get; set; }
}