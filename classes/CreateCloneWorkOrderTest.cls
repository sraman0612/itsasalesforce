@isTest
public class CreateCloneWorkOrderTest {
    private static testMethod void updateWorkOrderTest() {
        List<List<WorkOrder>> woLists = new List<List<WorkOrder>>();
        List<WorkOrder> woList = new List<WorkOrder>();
        List<Account> acc = SFS_TestDataFactory.createAccounts(1,true);
        List<ServiceContract> sa = SFS_TestDataFactory.createServiceAgreement(1,acc[0].Id,true);
        List<ContractLineItem> sali = SFS_TestDataFactory.createServiceAgreementLineItem(1,sa[0].Id,true);
        List<Asset> ast = SFS_TestDataFactory.createAssets(1,true);
        List<Entitlement> ent = SFS_TestDataFactory.createEntitlement(1,acc[0].Id,ast[0].Id,sa[0].Id,sali[0].Id,true);
        String woRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        WorkOrder wo=new WorkOrder();
        wo.AccountId=acc[0].Id;
        wo.SFS_Consumables_Ship_To_Account__c=acc[0].Id;
        wo.Shipping_Account__c=acc[0].Id;
        wo.ServiceContractId=sa[0].Id;
        wo.SFS_Work_Order_Type__c='Installation';
        wo.RecordTypeId=woRecordTypeId;
        wo.SFS_PO_Number__c='123';
        wo.SFS_Requested_Payment_Terms__c='NET 30';
        wo.status='New';
        wo.StartDate=system.Today();
        wo.EntitlementId = ent[0].Id;
        wo.AssetId = ast[0].Id;
        if(wo != NULL)
            insert wo;
        woList.add(wo);               
        woLists.add(woList);        
        Test.startTest();  
        CreateCloneWorkOrder.updateWorkOrder(woLists);        
        Test.stopTest();        
    }
}