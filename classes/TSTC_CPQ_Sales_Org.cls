@isTest public with sharing class TSTC_CPQ_Sales_Org
{
    private static final string TEST_JSON =
        '{"CPQSimulation.GenericCustomerSalesOrgs":["1000","2000"],' + 
        '"CPQSimulation.CustomerNumber":"TEST"}';

    @isTest public static void test_getSalesAreaList() {
        UTIL_AppSettings.resourceJson = TEST_JSON;
        
        Test.startTest();
        MockSBO_EnosixCustomer_Detail cdmock = new MockSBO_EnosixCustomer_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixCustomer_Detail.class, cdmock);
        cdmock.setSuccess(false);
        UTIL_Aura.Response resp = CTRL_CPQ_Sales_Org.getSalesAreaList(null);
        resp = CTRL_CPQ_Sales_Org.getSalesAreaList('TEST');
        cdmock.setSuccess(true);
        cdmock.setReturnSales(false);
        resp = CTRL_CPQ_Sales_Org.getSalesAreaList('TEST');
        cdmock.setReturnSales(true);
        resp = CTRL_CPQ_Sales_Org.getSalesAreaList('TEST');
        cdmock.setThrowException(true);
        resp = CTRL_CPQ_Sales_Org.getSalesAreaList('TEST');
        Test.stopTest();
    }
    
    public class MockSBO_EnosixCustomer_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        private boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        private boolean returnSales = true;

        public void setReturnSales(boolean returnSales)
        {
            this.returnSales = returnSales;
        }

        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) 
        { 
            return null; 
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) 
        { 
            return null; 
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) 
        { 
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixCustomer_Detail.EnosixCustomer result = new SBO_EnosixCustomer_Detail.EnosixCustomer();
            result.CustomerNumber = 'TEST';
            if (this.returnSales)
            {
                SBO_EnosixCustomer_Detail.SALES_DATA sd1 = new SBO_EnosixCustomer_Detail.SALES_DATA();
                sd1.SalesOrganization = '1000';
                result.SALES_DATA.add(sd1);
                SBO_EnosixCustomer_Detail.SALES_DATA sd2 = new SBO_EnosixCustomer_Detail.SALES_DATA();
                sd2.SalesOrganization = '3000';
                result.SALES_DATA.add(sd2);
            }
            
            result.setSuccess(this.success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) 
        { 
            return null; 
        }
    }
}