@isTest
public class MOC_EnosixWarehouseStock_Search
{
	public class MockEnosixWarehouseStockSuccess implements ensxsdk.EnosixFramework.SearchSBOSearchMock
	{
		public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext) 
		{
            SBO_EnosixWarehouseStock_Search.EnosixWarehouseStock_SR search_result = 
				new SBO_EnosixWarehouseStock_Search.EnosixWarehouseStock_SR();
            
            SBO_EnosixWarehouseStock_Search.SEARCHRESULT result = 
				new SBO_EnosixWarehouseStock_Search.SEARCHRESULT(); 
            
			result.Material = 'X';
			result.MaterialDescription = 'X';
			result.Plant = 'X';
			result.StorageLocation = 'X';
			result.StorageLocationDescription = 'X';
			result.MaterialType = 'X';
			result.MaterialGroup = 'X';
			result.SpecialStockInd = 'X';
			result.ValuationofSpecialStock = 'X';
			result.SpecialStockNumber = 'X';
			result.DeletionFLag = 'X';
			result.BatchNumber = 'X';
			result.FieldCurrency = 'X';
			result.UnrestrictedStock = 1.5;
			result.UnrestrictedValue = 1.5;
			result.TransitTransferStock = 1.5;
			result.TransitTransferValue = 1.5;
			result.QualityInspectionStock = 1.5;
			result.QualityInspectionValue = 1.5;
			result.RestrictedStock = 1.5;
			result.RestrictedValue = 1.5;
			result.BlockedStock = 1.5;
			result.BlockedValue = 1.5;
			result.ReturnsStock = 1.5;
			result.ReturnsValue = 1.5;
			result.SystemID = 'X';
			
			search_result.SearchResults.add(result);
			
			search_result.setSuccess(true);
			searchContext.baseResult = search_result;
			return searchContext;
        }
	}

	public class MockEnosixWarehouseStockFailure implements ensxsdk.EnosixFramework.SearchSBOSearchMock
	{
		public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext) 
		{
            SBO_EnosixWarehouseStock_Search.EnosixWarehouseStock_SR search_result = 
				new SBO_EnosixWarehouseStock_Search.EnosixWarehouseStock_SR();
            
            SBO_EnosixWarehouseStock_Search.SEARCHRESULT result = 
				new SBO_EnosixWarehouseStock_Search.SEARCHRESULT();
            
			result.Material = 'X';
			result.MaterialDescription = 'X';
			result.Plant = 'X';
			result.StorageLocation = 'X';
			result.StorageLocationDescription = 'X';
			result.MaterialType = 'X';
			result.MaterialGroup = 'X';
			result.SpecialStockInd = 'X';
			result.ValuationofSpecialStock = 'X';
			result.SpecialStockNumber = 'X';
			result.DeletionFLag = 'X';
			result.BatchNumber = 'X';
			result.FieldCurrency = 'X';
			result.UnrestrictedStock = 1.5;
			result.UnrestrictedValue = 1.5;
			result.TransitTransferStock = 1.5;
			result.TransitTransferValue = 1.5;
			result.QualityInspectionStock = 1.5;
			result.QualityInspectionValue = 1.5;
			result.RestrictedStock = 1.5;
			result.RestrictedValue = 1.5;
			result.BlockedStock = 1.5;
			result.BlockedValue = 1.5;
			result.ReturnsStock = 1.5;
			result.ReturnsValue = 1.5;
			result.SystemID = 'X';
			
			search_result.SearchResults.add(result);
			
			search_result.setSuccess(false); 
			searchContext.baseResult = search_result;
			return searchContext; 
        }
	}
}