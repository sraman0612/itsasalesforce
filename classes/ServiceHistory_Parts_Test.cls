@isTest(seeAllData=false)
public class ServiceHistory_Parts_Test {
    
	@testSetup
    static void setup() {
		
    }
    
    static testMethod void testApi() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/service-parts/';
        req.httpMethod = 'GET';
        
        Part__c part = new Part__c();
        part.Description__c = 'Test';
        insert part;
        
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        ServiceHistory_Parts.getParts();
        Test.stopTest();
        String testBlob = res.responseBody.toString();
		System.debug('Test blob: ' + testBlob);        
        System.assert(testBlob.contains(part.Description__c));
    }
}