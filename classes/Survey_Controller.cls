public without sharing class Survey_Controller {

    public PageReference cancel(){
         PageReference parentPR = new PageReference('/' + surveyCase.Id);  
         parentPR.setRedirect(true); 
         return parentPR ;
    }

    public void refreshGeneral() {
        if(!String.isEmpty(this.theSurvey.Number_of_Trips__c)){
            if(this.theSurvey.Number_of_Trips__c == '1'){
                this.showSCParts = true;
                this.showSCRecParts = false;
            }
            else{
                this.showSCParts = false;
                this.showSCRecParts = true;
            }
        }
    }
    
    public void saveSurvey() {
        score = 0;
        maxScore = 0;
        
        if(!String.IsEmpty(this.selectedAccount)){
            this.theSurvey.Service_Center__c = this.selectedAccount;
        }
        
        if(showSCParts){
            this.theSurvey.Service_Center_Parts__c = getSafeRadio(SCParts);
            if(this.theSurvey.Service_Center_Parts__c){
                score += 1;
            }
            maxScore += 1;
        }
        
        if(showSCRecParts){
            this.theSurvey.Second_Trip_No_Rec_Parts__c = getSafeRadio(SCRecParts);
            if(this.theSurvey.Second_Trip_No_Rec_Parts__c){
                score -= 1;
            }
            maxScore += 1;
        }
        
        this.theSurvey.Followed_Special_Instructions__c = getSafeRadio(SpecIns);
        this.theSurvey.Excessive_Charges__c = getSafeRadio(ECharges);
        
        if(this.theSurvey.Service_Response_Time__c.startsWith('Poor')){
            score += 0;
        }
        else if(this.theSurvey.Service_Response_Time__c.startsWith('Fair')){
            score += 1;
        }
        else{
            score += 2;
        }
        maxScore += 2;
         
        if(this.theSurvey.Followed_Special_Instructions__c){
                score += 1;
        }
        maxScore += 1;
         
        if(this.theSurvey.Quote_Paperwork__c.startsWith('Poor')){
            score += 0;
            maxScore += 2;
        }
        else if(this.theSurvey.Quote_Paperwork__c.startsWith('Fair')){
            score += 1;
            maxScore += 2;
        }
        else if(this.theSurvey.Quote_Paperwork__c.startsWith('Good')){
            score += 2;
            maxScore += 2;
        }
        else{
            //NA
        }
        
        if(this.theSurvey.Invoice_Work_Order__c.startsWith('Poor')){
            score += 0;
        }
        else if(this.theSurvey.Invoice_Work_Order__c.startsWith('Fair')){
            score += 1;
        }
        else{
            score += 2;
        }
        maxScore += 2;
        
        if(this.theSurvey.Excessive_Charges__c){
                score += 1;
        }
        maxScore += 1;
        
        this.theSurvey.Score__c = score;
        this.theSurvey.Max_Score__c = maxScore;
        
        String scoringString = 'Score {0} out of Max {1}';
        this.buttonToggle = false;
        try{
            insert this.theSurvey;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, String.format(scoringString,new String[]{score.format(), maxScore.format()})));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Success! This Survey has been completed. You may close this page at anytime.'));
        }
        catch(exception e){
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
        }

    }
    
    private Boolean getSafeRadio(String value){
        if(String.isEmpty(value)){return false;}
        
        return value == 'Yes';
    }

    public Managed_Care_Survey__c theSurvey {get; set;}
    public String SCParts {get; set;}
    public String SCRecParts {get; set;}
    public String SpecIns {get; set;}
    public String ECharges {get; set;}
    public Boolean showForm {get; private set;}
    public Boolean showSCParts {get; private set;}
    public Boolean showSCRecParts {get; private set;}
    public Boolean buttonToggle {get; private set;}
    public List<SelectOption> surveyAccounts {get; private set;}
    public String selectedAccount {get; set;}
    private Case surveyCase;
    private integer score;
    private integer maxScore;

    public Survey_Controller(){
        this.theSurvey = new Managed_Care_Survey__c();
        this.showSCParts = false;
        this.showSCRecParts = false;
        this.buttonToggle = true;
        score = 0;
        maxScore = 0;
        String caseId = ApexPages.CurrentPage().getParameters().get('CaseId');
        if(!String.isEmpty(caseId)){
            surveyCase = [SELECT Id, CaseNumber FROM Case WHERE Id =: CaseId];
            if(surveyCase != null){
                this.theSurvey.Case__c = surveyCase.Id;
                List<Site_Visit__c> siteVisits = [SELECT Id, Service_Center__c, Service_Center__r.Name FROM Site_Visit__c WHERE Case__c = :surveyCase.Id];
                List<Managed_Care_Survey__c> existingSurveys = [SELECT Id, Service_Center__c FROM Managed_Care_Survey__c WHERE Case__c = :surveyCase.Id]; 
                Set<String> existingAccounts = new Set<String>();
                for(Managed_Care_Survey__c mc : existingSurveys){
                    existingAccounts.add(mc.Service_Center__c);
                }
                if(siteVisits.size() > 0){
                    surveyAccounts = new List<SelectOption>();
                    surveyAccounts.add(new SelectOption('','- Please Select -'));
                    Map<String, String> serviceCenterMap = new Map<String,String>();
                    for(Site_Visit__c sv : siteVisits){
                        serviceCenterMap.put(sv.Service_Center__c, sv.Service_Center__r.Name);
                    }
                    for(String s : serviceCenterMap.keySet()){
                        if(!existingAccounts.contains(s)){
                            surveyAccounts.add(new SelectOption(s, serviceCenterMap.get(s)));
                        }
                    }    
                    
                        
                    if(surveyAccounts.size() == 1){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'All Service Centers associated to this Case have had their Surveys completed already.'));
                    }           
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The associated Case does not have any Site Visits logged to it.'));
                }
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The URL for this Page is missing the CaseId parameter. Please re-launch this page from the Case.'));
        }
    }
    
    public List<SelectOption> getYesNo() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); 
        return options; 
    }
                                
}