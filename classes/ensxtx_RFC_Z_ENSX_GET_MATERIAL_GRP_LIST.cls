/// enosiX Inc. Generated Apex Model
/// Generated On: 11/4/2020 9:54:30 AM
/// SAP Host: From REST Service On: https://gdi--enosixdev2.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev2.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST extends ensxsdk.EnosixFramework.RFC
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST_Meta', new Type[] {
            ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.RESULT.class
            , ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS.class
            } 
        );
    }

    public ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST()
    {
        super('Z_ENSX_GET_MATERIAL_GRP_LIST', ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.RESULT.class);
    }

    public override Type getType() { return ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.class; }

    public RESULT PARAMS
    {
        get { return (RESULT)this.getParameterContext(); }
    }

    public RESULT execute()
    {
        return (RESULT)this.executeFunction();
    }
    
    public class RESULT extends ensxsdk.EnosixFramework.FunctionObject
    {    	
        public RESULT()
        {
            super(new Map<string,type>
            {
                'ET_MATERIAL_GROUPS' => ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS.class
            });	
        }
        
        public override Type getType() { return ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.RESULT.class; }

        public override void registerReflectionForClass()
        {
            ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.registerReflectionInfo();
        }

        @AuraEnabled public List<ET_MATERIAL_GROUPS> ET_MATERIAL_GROUPS_List
    {
        get 
        {
            List<ET_MATERIAL_GROUPS> results = new List<ET_MATERIAL_GROUPS>();
            this.getCollection(ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS.class).copyTo(results);
            return results;
        }
    }
        @AuraEnabled public String IV_SPART
        { 
            get { return this.getString ('IV_SPART'); } 
            set { this.Set (value, 'IV_SPART'); }
        }

        @AuraEnabled public String IV_VKORG
        { 
            get { return this.getString ('IV_VKORG'); } 
            set { this.Set (value, 'IV_VKORG'); }
        }

        @AuraEnabled public String IV_VTWEG
        { 
            get { return this.getString ('IV_VTWEG'); } 
            set { this.Set (value, 'IV_VTWEG'); }
        }

    	
    }
    public class ET_MATERIAL_GROUPS extends ensxsdk.EnosixFramework.ValueObject
    {
        public ET_MATERIAL_GROUPS()
        {
            super('ET_MATERIAL_GROUPS', new Map<string,type>());
        }

        public override Type getType() { return ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS.class; }

        public override void registerReflectionForClass()
        {
            ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.registerReflectionInfo();
        }

                    @AuraEnabled public String MANDT
        { 
            get { return this.getString ('MANDT'); } 
            set { this.Set (value, 'MANDT'); }
        }

        @AuraEnabled public String SPRAS
        { 
            get { return this.getString ('SPRAS'); } 
            set { this.Set (value, 'SPRAS'); }
        }

        @AuraEnabled public String MATKL
        { 
            get { return this.getString ('MATKL'); } 
            set { this.Set (value, 'MATKL'); }
        }

        @AuraEnabled public String WGBEZ
        { 
            get { return this.getString ('WGBEZ'); } 
            set { this.Set (value, 'WGBEZ'); }
        }

        @AuraEnabled public String WGBEZ60
        { 
            get { return this.getString ('WGBEZ60'); } 
            set { this.Set (value, 'WGBEZ60'); }
        }

            
        }
}