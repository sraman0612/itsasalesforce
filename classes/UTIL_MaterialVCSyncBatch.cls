public with sharing class UTIL_MaterialVCSyncBatch
    implements Database.Batchable<SObject>,
	Database.AllowsCallouts,
	Database.Stateful
{
    // VC characteristic values load process for UTIL_MaterialSyncBatch()
    // Designed to add all available characteristic values for materials marked as a variant

    // jobInfo contains debug information persisted across contexts since start(),
    // execute(), and finish() all run in separate contexts with separate debug logs
    @TestVisible
    private static String SFSyncKeyField = UTIL_SFProduct.MaterialFieldName;
    @TestVisible
    private static String BatchClassName = 'UTIL_MaterialVCSyncBatch';
    private List<String> jobInfo = new List<String>();
    private List<SObject> errors = new List<SObject>();
    private static String ObjectType = 'Product2';
    private static String SAP_Material = (string) UTIL_AppSettings.getValue(BatchClassName + '.SAP_Material');
    private static String SAP_Plant = (string) UTIL_AppSettings.getValue(BatchClassName + '.SAP_Plant');

    // start()
    //
    // Start the batch job
    public Database.QueryLocator start(Database.BatchableContext context)
    {
        System.debug(context.getJobId() + ' ===> Starts');
        try
        {
            String query = buildQueryString();
            system.debug('query = '+query);
            return Database.getQueryLocator(query);
        }
        catch(Exception ex)
        {
            System.debug('Failed querying new SAP Characteristic Values for Variant products: ' + ex.getMessage());
        }

        // Returning null causes "System.UnexpectedException: Start did not return a valid iterable object."
        // So to NOOP we must return a query that will always give 0 results. Id should never be
        // null in any table so we can arbitrarily pick Account.
        return Database.getQueryLocator([SELECT Id FROM Account WHERE Id = null]);
    }

    // execute()
    //
    // Execute the batch job
    public void execute(Database.BatchableContext context, List<SObject> querySearchResults)
    {
        System.debug(context.getJobId() + ' Executing');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);

        SBO_EnosixVCSync_Search sbo = new SBO_EnosixVCSync_Search();
        SBO_EnosixVCSync_Search.EnosixVCSync_SC searchContext = new SBO_EnosixVCSync_Search.EnosixVCSync_SC();

        Set<String> SAPMaterialNumbers = new Set<String>();
        List<Product2> updatedProducts = new List<Product2>();

        if (querySearchResults.size() > 0)
        {
            // Update the Variant Product characteristic values from the searchResults
            integer materialCount = 1;
            for (SObject result1 : querySearchResults)
            {
                Product2 prod = (Product2) result1;
                SAPMaterialNumbers.add(prod.ENSX_EDM__Material__c);
                SAP_Material = prod.ENSX_EDM__Material__c;
                SAP_Plant = prod.Delivering_Plant__c;

                if (SAP_Material != null)
        		{
            		searchContext.SEARCHPARAMS.Material = SAP_Material;
        		}
        		if (SAP_Plant != null)
        		{
            		searchContext.SEARCHPARAMS.Plant = SAP_Plant;
        		}

                // Execute the search for each row returned by the product query
        		SBO_EnosixVCSync_Search.EnosixVCSync_SR result;
        		try
        		{
            		sbo.search(searchContext);
            		result = searchContext.result;
        		}
        			catch (Exception ex)
        		{
            		system.debug('Search Exception ====> '+ ex);
        		}

        		// Write any response messages to the debug log
        		String errorMessage = UTIL_SyncHelper.buildErrorMessage(BatchClassName, result.getMessages());

                List<Object> searchResults = result.getResults();
                system.debug('searchResults ===> '+searchResults);

                //Use a counter to dynamically increment numbers on the variable names used to store the new charactersitic values
                Integer i = 1;
                Boolean valuesFound = false;
                for (Object searchResult : searchResults)
                {
                    SBO_EnosixVCSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
                    system.debug('sboResult ===> '+sboResult);
                    String charName = 'char_Name'+i+'__c';
                    String charDescription = 'char_Description'+i+'__c';
                    String charValue = 'char_Value'+i+'__c';
                    String charValueDescription = 'char_ValueDescription'+i+'__c';
                    prod.put(charName,sboResult.CharacteristicName);
                    prod.put(charDescription,sboResult.CharacteristicDescription);
                    prod.put(charValue,sboResult.CharacteristicValue);
                    prod.put(charValueDescription,sboResult.CharacteristicValueDescription);
                    i++;

                    // Stop processing the variant characteristics after the first 25
                    if (i == 26) break;

                    valuesFound = true;
                }
                if (valuesFound) updatedProducts.add(prod);
            }
        }

        if (updatedProducts.size() > 0)
        {
            UTIL_SyncHelper.insertUpdateResults('Product2', 'Update', null, null, updatedProducts, 'UTIL_MaterialVCSyncBatch', null);
        }
    }

    // finish()
    //
    // Finish the batch job
    public void finish(Database.BatchableContext context)
    {
        System.debug(context.getJobId() + ' ===> Finished');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);
    }

    private SBO_EnosixVCSync_Search.SEARCHRESULT getSboResult(Object searchResult)
    {
        return (SBO_EnosixVCSync_Search.SEARCHRESULT) searchResult;
    }

    // buildQueryString
    //
    // Build the query String
    private String buildQueryString()
    {
        String query = 'SELECT Id, Name, Delivering_Plant__c, FLD_Variant_Material__c, ProductCode, ENSX_EDM__Material__c ' +
            'FROM Product2 WHERE FLD_Variant_Material__c = true AND IsActive = true ' +
            'AND ENSX_EDM__Material__c != null AND CreatedDate > YESTERDAY';

        return query;
    }
}