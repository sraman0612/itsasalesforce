@isTest
public class cz_tests {
    static testmethod void testAssetSharing() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community - Single Account User']; 
        Profile p2 = [SELECT Id FROM Profile WHERE Name='Partner Community - Global Account User']; 
        account par = new Account(name = 'parent');
        insert par;
        Account a = new Account(name='test', parentId = par.Id);
        insert a;
        Contact c = new contact(lastname = 'test', AccountId = a.Id);  
        insert c;
        Contact c2 = new contact(lastname = 'test', AccountId = a.Id);  
        insert c2;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs(thisUser){
        User u2 = new User(Alias = 'standt', Email='test32423554548@gardenerdenver.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p2.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test32423554548@gardenerdenver.com', contactId = c2.Id, Community_User_Type__c = 'Global Account');
             insert u2;
        }
        
         /*User u = new User(Alias = 'standt', Email='test8490238402938@gardenerdenver.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test8490238402938@gardenerdenver.com', contactId = c.Id, Community_User_Type__c = 'Single Account'); 
        insert u;*/
        
        
            Asset ast = new Asset(name = 'test', Current_Servicer__c = a.Id, AccountId = a.Id);
            insert ast;
            ast.Current_Servicer__c = a.Id;
            update ast;
            insert new Service_History__c(account__c = a.Id, Serial_Number_MD__c =ast.id,Serial_Number__c=ast.id,Date_of_Service__c=system.today());
        test.startTest();
            //c_AssetSharing.assetsharingfromusers(new List<User>{u});
            C_AssetSharingCreation.createAssetShareOnAssetInsert([select id, Current_Servicer__c, AccountId, Current_Servicer_Parent_ID__c, Parent_AccountID__c, Current_Servicer_ID__c from asset]);
        test.stoptest(); 
    }
    static testmethod void testFavoriteShares() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community - Single Account User']; 
        Profile p2 = [SELECT Id FROM Profile WHERE Name='Partner Community - Global Account User']; 
        account par = new Account(name = 'parent');
        insert par;
        Account a = new Account(name='test', parentId = par.Id);
        insert a;
        Contact c = new contact(lastname = 'test', AccountId = a.Id);  
        insert c;
        Contact c2 = new contact(lastname = 'test', AccountId = a.Id);  
        insert c2;
        Contact c3 = new contact(lastname = 'test', AccountId = a.Id);  
        insert c3;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
         /*User u = new User(Alias = 'standt', Email='test8490238402938@gardenerdenver.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test8490238402938@gardenerdenver.com', contactId = c.Id, Community_User_Type__c = 'Single Account');
        insert u;*/
         System.runAs(thisUser) {
             User u = new User(Alias = 'standt', Email='test8490238402938@gardenerdenver.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test8490238402938@gardenerdenver.com', contactId = c.Id, Community_User_Type__c = 'Single Account');
             SBQQ__Favorite__c favorite = new SBQQ__Favorite__c(Name = 'Services', CreatedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57),
                                                           LastModifiedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57),
                                                            List_Price__c = 300, Product_Display_Name__c = 'Services',
                                                             Product_Number__c = null,Unit_Cost__c=100);
                                                              
           /* SBQQ__Favorite__c favorite = new SBQQ__Favorite__c();
                favorite.Name = 'Services';
                favorite.CreatedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57);
                favorite.SBQQ__Description__c = null;
                //favorite.Favorite_Account_Id__c = a.Id;
                //favorite.Favorite_Parent_Account_Id__c = a.Id; 
                favorite.LastModifiedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57);
                favorite.List_Price__c = 300;
                favorite.Product_Display_Name__c = 'Services';
                favorite.Product_Number__c = null;
                favorite.Unit_Cost__c=100;
                insert favorite; */
        }
       /* User u2 = new User(Alias = 'standt', Email='test32423554548@gardenerdenver.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p2.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test32423554548@gardenerdenver.com', contactId = c2.Id, Community_User_Type__c = 'Global Account');
             insert u2;
    
    System.runAs(u2) {
        SBQQ__Favorite__c favorite = new SBQQ__Favorite__c();
                favorite.Name = 'Services';
                favorite.CreatedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57);
                favorite.SBQQ__Description__c = null;
                //favorite.Favorite_Account_Id__c = a.Id;
                //favorite.Favorite_Parent_Account_Id__c = a.Id; 
                favorite.LastModifiedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57);
                favorite.List_Price__c = 1;
                favorite.Product_Display_Name__c = 'Services';
                favorite.Product_Number__c = null;
                favorite.Unit_Cost__c = 100;
                insert favorite; 
        }
        User u3 = new User(Alias = 'standt', Email='test4534534545@gardenerdenver.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test4534534545@gardenerdenver.com', contactId = c3.Id, Community_User_Type__c = 'Single Account');
             insert u3;
    
    System.runAs(u3) {
          SBQQ__Favorite__c favorite = new SBQQ__Favorite__c();
                favorite.Name = 'Services';
                favorite.CreatedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57);
                favorite.SBQQ__Description__c = null;
                //favorite.Favorite_Account_Id__c = a.Id;
                //favorite.Favorite_Parent_Account_Id__c = a.Id; 
                favorite.LastModifiedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57);
                favorite.List_Price__c = 300;
                favorite.Product_Display_Name__c = 'Services';
                favorite.Product_Number__c = null;
                favorite.Unit_Cost__c = 100;
                insert favorite; 
        }*/
    }
}