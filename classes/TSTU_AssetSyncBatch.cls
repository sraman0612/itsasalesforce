@isTest
public with sharing class TSTU_AssetSyncBatch
{
    static final string TEST_JSON =
        '{"UTIL_AssetSyncBatch.Logging": true,' +
        '"UTIL_AssetSyncBatch.ExcludedDistributionChannels": ["00","CU","CV","CX","QT"],' +
        '"UTIL_AssetSyncBatch.SalesOrg": "GDMI",' +
        '"UTIL_AssetSyncBatch.Division": "00"}';

    public class MockSyncSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        private boolean success = true;

        public void setSuccess(boolean success)
        {
            this.success = success;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext)
        {
            if (this.throwException)
            {
                throw new UTIL_SyncHelper.SyncException('');
            }

            SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR searchResult =
                new SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR();

            // New Account
            SBO_EnosixEquipmentSync_Search.SEARCHRESULT result1 =
                new SBO_EnosixEquipmentSync_Search.SEARCHRESULT();

            result1.DistributionChannel = 'CM';
            result1.Division = '00';
            result1.EquipmentNumber = '1111';
            result1.Name = 'Name1';
            result1.Street = 'Street1';
            result1.City = 'City1';
            result1.Region = 'Region1';
            result1.Country = 'Country1';
            result1.TechnicalObjectStartupDate = System.today();
            result1.SalesOrganization = 'GDMI';
            result1.Material = 'Material1';
            result1.MaterialDescription = 'MaterialDescription';
            result1.Materialgroup4 = 'Materialgroup4';
            result1.MODEL_NUMBER_C = '1212';
            result1.ProductHierarchy = 'ProductHierarchy';
            result1.SerialNumber = '2222';
            result1.SERNR_MODNR = '2222-1212';
            result1.MasterWarrantyNumber = '9898';
            result1.MasterWarrantyText = 'MasterWarrantyText';
            result1.StroreNo = '40';

            searchResult.SearchResults.add(result1);

            // Existing Account
            SBO_EnosixEquipmentSync_Search.SEARCHRESULT result2 =
                new SBO_EnosixEquipmentSync_Search.SEARCHRESULT();

            result2.DistributionChannel = 'CM';
            result2.Division = '00';
            result2.EquipmentNumber = '3333';
            result2.Name = 'Name2';
            result2.Street = 'Street2';
            result2.City = 'City2';
            result2.Region = 'Region2';
            result2.Country = 'Country2';
            result2.TechnicalObjectStartupDate = System.today();
            result2.SalesOrganization = 'GDMI';
            result2.Material = 'Material2';
            result2.MaterialDescription = 'MaterialDescription';
            result2.Materialgroup4 = 'Materialgroup4';
            result2.MODEL_NUMBER_C = '3434';
            result2.ProductHierarchy = 'ProductHierarchy';
            result2.SerialNumber = '4444';
            result2.SERNR_MODNR = '4444-3434';
            result2.MasterWarrantyNumber = '3434';
            result2.MasterWarrantyText = 'MasterWarrantyText';
            result2.StroreNo = '50';

            searchResult.SearchResults.add(result2);

            SBO_EnosixEquipmentSync_Search.SEARCHRESULT result3 =
                new SBO_EnosixEquipmentSync_Search.SEARCHRESULT();

            result3.DistributionChannel = 'CV';
            result3.Division = '00';
            result3.EquipmentNumber = '5555';
            result3.Name = 'Name3';
            result3.Street = 'Street3';
            result3.City = 'City3';
            result3.Region = 'Region3';
            result3.Country = 'Country3';
            result3.TechnicalObjectStartupDate = System.today();
            result3.SalesOrganization = 'GDMI';
            result3.Material = 'Material3';
            result3.MaterialDescription = 'MaterialDescription';
            result3.Materialgroup4 = 'Materialgroup4';
            result3.MODEL_NUMBER_C = '4444';
            result3.ProductHierarchy = 'ProductHierarchy';
            result3.SerialNumber = '6666';
            result3.SERNR_MODNR = '6666-4444';
            result3.MasterWarrantyNumber = '5656';
            result3.MasterWarrantyText = 'MasterWarrantyText';
            result3.StroreNo = '60';

            searchResult.SearchResults.add(result3);

            SBO_EnosixEquipmentSync_Search.SEARCHRESULT result4 =
                new SBO_EnosixEquipmentSync_Search.SEARCHRESULT();

            result4.DistributionChannel = 'CM';
            result4.Division = '00';
            result4.EquipmentNumber = '7777';
            result4.Name = 'Name4';
            result4.Street = 'Street4';
            result4.City = 'City4';
            result4.Region = 'Region4';
            result4.Country = 'Country4';
            result4.TechnicalObjectStartupDate = System.today();
            result4.SalesOrganization = 'GDMI';
            result4.Material = 'Material4';
            result4.MaterialDescription = 'MaterialDescription';
            result4.Materialgroup4 = 'C65';
            result4.MODEL_NUMBER_C = '9999';
            result4.ProductHierarchy = 'ProductHierarchy';
            result4.SerialNumber = '8888';
            result4.SERNR_MODNR = '8888-9999';
            result4.MasterWarrantyNumber = '7878';
            result4.MasterWarrantyText = 'MasterWarrantyText';
            result4.StroreNo = '70';

            searchResult.SearchResults.add(result4);

            searchResult.setSuccess(this.success);
            searchContext.baseResult = searchResult;
            return searchContext;
        }
    }

    public static testMethod void test_AssetSync()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixEquipmentSync_Search.class, new MockSyncSearch());

        UTIL_AppSettings.resourceJson = TEST_JSON;
        createExistingObject();
        Test.startTest();
        Asset asst = new Asset();
        asst.Name = '9001';
        asst.Equipment_Number__c = '7777';
        asst.Material__c = 'GCMAT';
        asst.Model_Number__c = 'GCSERIAL';
        asst.SerialNumber = '9001';
        asst.Serial_Number_Model_Number__c = '9001'+'-'+'GCSERIAL';
        asst.FLD_Serial_Number_Equipment_Number__c = '9001'+'-'+'7777';
        asst.SOrg__c = 'GDMI';
        insert asst;
        UTIL_AssetSyncBatch controller = new UTIL_AssetSyncBatch();
        UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
        lastSync.retryCnt = 1;
        controller.setBatchParam(lastSync);
        Database.executeBatch(controller);
        Test.stopTest();
    }

    public static testMethod void test_AssetSyncFailure()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixEquipmentSync_Search.class, mockSyncSearch);
        mockSyncSearch.setSuccess(false);

        createExistingObject();
        Test.startTest();
        UTIL_AssetSyncBatch controller = new UTIL_AssetSyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
        }
        catch (Exception e) {}
        Test.stopTest();
    }

    public static testMethod void test_AssetSyncException()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMatSync_Search.class, mockSyncSearch);
        mockSyncSearch.setThrowException(true);

        createExistingObject();
        Test.startTest();
        UTIL_AssetSyncBatch controller = new UTIL_AssetSyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
            controller.start(null);
        }
        catch (Exception e) {}
        Test.stopTest();
    }
    
    public static testMethod void test_UTIL_AssetSyncSchedule()
    {
     	Test.StartTest();
		UTIL_AssetSyncSchedule sh1 = new UTIL_AssetSyncSchedule();
		String sch = '0 0 23 * * ?'; 
        system.schedule('Test Asset Sync Schedule', sch, sh1); 
        Test.stopTest();
    }

    private static void createExistingObject()
    {
        Asset currentObject = new Asset();
        currentObject.Name = '8888';
        currentObject.put(UTIL_AssetSyncBatch.SFSyncKeyField,'8888-9999');
        insert currentObject;

        MPG4_Code__c mpg4 = new MPG4_Code__c();
        mpg4.MPG4_Number__c = 'C65';
        mpg4.Level_1__c = 'Rotary Vane';
        mpg4.Level_2__c = 'Rotary Lubricated';
        mpg4.Level_3__c = 'Compressors';
        insert mpg4;

        OBJ_SAP_Sync__c sapSync = new OBJ_SAP_Sync__c();
        sapSync.Name = 'UTIL_AssetSyncSchedule';
        sapSync.FLD_Sync_DateTime__c = System.today().addDays(-1);
        sapSync.FLD_Page_Number__c = 0;
        upsert sapSync;
    }
}