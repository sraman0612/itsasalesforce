@isTest
private class C_Files_Controller_Test {

    @testSetup
    static void setupData(){
        Account acct = new Account();
        acct.name = 'SCC Test Account';
        insert acct;

        Contact cont1 = new Contact();
        cont1.firstName = 'SC';
        cont1.lastName = ' Consulting';
        cont1.email = 'service-gdi@scc.com';
        insert cont1;

        Case cse1 = new Case();
        cse1.Account = acct;
        cse1.Contact = cont1;
        cse1.ContactId = cont1.Id;
        cse1.Type = 'CSE1';
        cse1.Subject = 'Testing Case';

        insert cse1;
        System.debug(cse1);

        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.PathOnClient = 'File Test';
        cv.Origin = 'H';
        cv.Title = 'File Test';
        cv.VersionData = blob.valueOf('Test content for file');
        insert cv;

        ContentVersion cv2 = new ContentVersion();
        cv2.ContentLocation = 'S';
        cv2.PathOnClient = 'File Test 2';
        cv2.Origin = 'H';
        cv2.Title = 'File Test 2';
        cv2.VersionData = blob.valueOf('Test content for file 2');
        insert cv2;

        EmailMessage em = new EmailMessage();
        em.Incoming = true;
        em.FromName = 'SCC';
        em.ToAddress = 'salesforce@gardnerdenver.com';
        em.TextBody = 'Body';
        em.Subject = 'Subject';
        em.MessageDate = System.today();
        em.ParentId = cse1.Id;
        em.HtmlBody = '<p>Body</p>';
        em.FromAddress = 'service-gdi@scc.com';
        em.CcAddress = 'service-gdi@scc.com';
        em.BccAddress = 'service-gdi@scc.com';
        insert em;
    }

    static testmethod void testC_CustomEmail_Controller(){
        Case c = [SELECT Id FROM Case];
        ContentVersion cv = [SELECT Id, ContentLocation, PathOnClient, Origin, Title, VersionData FROM ContentVersion WHERE Title = 'File Test'];

        Test.setCurrentPageReference(new PageReference('C_Files'));
        System.currentPageReference().getParameters().put('CaseId', c.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        C_Files_Extension cfe = new C_Files_Extension(sc);
        List<C_Files_Extension.FileWrapper> wrappers = cfe.thewrappers;

        for(C_Files_Extension.FileWrapper wrap: wrappers){
            wrap.cBox = false;
        }

        cfe.fileID = cv.Id;
        cfe.createNewFiles_Stage();
    }

    static testmethod void testC_CustomEmail_Controller2(){
        Case c = [SELECT Id, AccountId, ContactId FROM Case];
        ContentVersion cv = [SELECT Id, ContentLocation, PathOnClient, Origin, Title, VersionData FROM ContentVersion  WHERE Title = 'File Test 2'];

        Case cse1 = new Case();
        cse1.AccountId = c.AccountId;
        cse1.ContactId = c.ContactId;
        cse1.Type = 'CSE1';
        cse1.ParentId = c.id;
        cse1.Subject = 'Testing Case';
        insert cse1;

        Test.setCurrentPageReference(new PageReference('C_Files'));
        System.currentPageReference().getParameters().put('CaseId', cse1.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(cse1);
        C_Files_Extension cfe = new C_Files_Extension(sc);
        List<C_Files_Extension.FileWrapper> wrappers = cfe.thewrappers;

        for(C_Files_Extension.FileWrapper wrap: wrappers){
            wrap.cBox = true;
        }

        cfe.fileID = cv.Id;
        cfe.createNewFiles_Stage();
        cfe.cloneFiles();
        cfe.getTopLevelCase(cse1.id);
        cfe.deleteFiles();
    }
}