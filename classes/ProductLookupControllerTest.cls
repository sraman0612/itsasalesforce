@IsTest
public class ProductLookupControllerTest {
	@IsTest
    public static void test1() {
         SBQQ__Quote__c quote = [SELECT Id from SBQQ__Quote__c LIMIT 1];
         Product2 newProd = [Select Id from Product2 LIMIT 1];
        ProductLookupController.appendLineItems(quote.Id, new List<String> { newProd.Id }, new List<Integer> { 5 });
       
        SBQQ__QuoteLine__c quoteLine = [SELECT Id, SBQQ__Product__c, SBQQ__Quantity__c from SBQQ__QuoteLine__c where SBQQ__Quote__c = : quote.Id LIMIT 1];
         System.assert(quoteLine != null, 'Quote line was not created.');
        System.assert(quoteLine.SBQQ__Quantity__c == 5, 'Quantity of Quote Line was not 5.');
        System.assert(quoteLine.SBQQ__Product__c == newProd.Id, 'Quote Line product does not match.');
        
        ProductLookupController.appendLineItems(quote.Id, new List<String> { newProd.Id }, new List<Integer> { 3 });
    }

	@IsTest
    public static void test2() {
        SBQQ__Quote__c quote = [SELECT Id from SBQQ__Quote__c LIMIT 1];
        Product2 newProd = [Select Id from Product2 LIMIT 1];

        ProductLookupCntrl.searchDB(quote.Id, 'Product2', 'Name', 'ProductCode', 10, 'ProductCode', '1');
    }

    @TestSetup
    private static void createTestObjects()
    {
        Account testAccount = TSTU_SFAccount.createTestAccount();
        testAccount.Name = 'test';
        upsert testAccount;

        Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'TEST OPP';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.AccountId = testAccount.Id;
        opp.Pricebook2Id = pricebookId;

        TSTU_SFOpportunity.upsertOpportunity(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        TSTU_SFOpportunity.upsertOpportunity(opp);

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__Primary__c = true;
        quote.End_Customer_Account__c = testAccount.Id;
        quote.Distribution_Channel__c = 'CM';
        quote.SBQQ__Account__c = testAccount.Id;
        
        upsert quote;

        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        newProd.IsActive = true;
        newProd.ProductCode = '1234';
        newProd.FLD_Distribution_Channel__c = 'CM';

        upsert newProd;

        PriceBookEntry standardPbe = new PriceBookEntry();
        standardPbe.UnitPrice = 100;
        standardPbe.Pricebook2Id = pricebookId;
        standardPbe.Product2Id = newProd.Id;
        standardPbe.UseStandardPrice = false;
        standardPbe.IsActive = true;
        
        upsert standardPbe;
    }
}