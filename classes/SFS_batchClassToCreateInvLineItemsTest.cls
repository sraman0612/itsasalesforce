/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 14/10/2021
* @description: Test class for SFS_batchClassToCreateInvoiceLineItems
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

====================================================================================*/
@isTest
public class SFS_batchClassToCreateInvLineItemsTest {
    
    @isTest  
    public static void setStatusToSubmittTest() {
        
        Id invRecTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Agreement Invoice').getRecordTypeId();
        Id CAREInvoiceRecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('SFS_CARE_Invoice').getRecordTypeId();
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].CurrencyIsoCode='USD';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        accountList[1].CurrencyIsoCode='USD';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0];
     
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
        svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';           
        svcAgreementList[0].SFS_Status__c = 'APPROVED';
        svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
        svcAgreementList[0].SFS_PO_Number__c = '2';
        svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
        svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList[0].SFS_Renewal_Escalator_Start_Date__c=system.today().addDays(-10);
        svcAgreementList[0].SFS_Recurring_Adjustment__c=3;
        svcAgreementList[0].ShippingHandling=2;
        svcAgreementList[0].SFS_Type__c='Rental';           
        insert svcAgreementList;
        
        
        List<ContractLineItem> cLi=SFS_TestDataFactory.createServiceAgreementLineItem(1,svcAgreementList[0].Id,false);
        insert cLi[0];
        
        Invoice__c inv = new Invoice__c(RecordTypeId = invRecTypeId,
                                        SFS_Service_Agreement__c=svcAgreementList[0].Id,
                                        SFS_Status__c='Created',
                                        SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                        SFS_Billing_Period_End_Date__c=system.today(),
                                        SFS_PO_Number__c = svcAgreementList[0].SFS_PO_Number__c,
                                        SFS_Invoice_Type__c = svcAgreementList[0].SFS_Invoice_Type__c,
                                        SFS_Billing_Period_Start_Date__c= system.today()
                                       );
        insert inv;
        
        Invoice__c inv1 = new Invoice__c(RecordTypeId = invRecTypeId,
                                         SFS_Service_Agreement__c=svcAgreementList[0].Id,
                                         SFS_Status__c='Created',
                                         SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                         SFS_Billing_Period_End_Date__c=system.today(),
                                         SFS_PO_Number__c = svcAgreementList[0].SFS_PO_Number__c,
                                         SFS_Invoice_Type__c = svcAgreementList[0].SFS_Invoice_Type__c,
                                         SFS_Billing_Period_Start_Date__c= system.today(),
                                         SFS_Invoice4Multiples__c=true);
        insert inv1;
        
        Invoice__c inv2 = new Invoice__c(RecordTypeId = CAREInvoiceRecordTypeId,                                       
                                         SFS_Status__c='Created',
                                         SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                         SFS_Billing_Period_End_Date__c=system.today(),                                    
                                         SFS_PO_Number__c = svcAgreementList[0].SFS_PO_Number__c,
                                         SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                         SFS_Service_Agreement_1__c=svcAgreementList[0].Id,
                                         SFS_Invoice_Type__c = svcAgreementList[0].SFS_Invoice_Type__c,
                                         SFS_Billing_Period_Start_Date__c= system.today()
                                        );
        insert inv2;
        
        Set<Id> scIds = new Set<Id>();
        Set<Id> scIds1 = new Set<Id>();
        scIds.add(svcAgreementList[0].Id);       
        test.startTest();
        SFS_batchClassToCreateInvoiceLineItems bcli = new SFS_batchClassToCreateInvoiceLineItems(scIds);
        database.executeBatch(bcli);          
        test.stopTest();
        
    }
    
    
    @istest  
    public static void setStatusToSubmitt1Test() {
        Id invRecTypeId1 = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Agreement Invoice').getRecordTypeId();
        Id CAREInvoiceRecordTypeId1 = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('SFS_CARE_Invoice').getRecordTypeId();
        String accRecID1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList1=SFS_TestDataFactory.createAccounts(2, false);
        accountList1[0].IRIT_Customer_Number__c='123';
        accountList1[0].CurrencyIsoCode='USD';
        accountList1[0].RecordTypeId=accRecID1;
        insert accountList1[0];
        
        accountList1[1].IRIT_Customer_Number__c='123';
        accountList1[1].Bill_To_Account__c = accountList1[0].Id;
        accountList1[1].CurrencyIsoCode='USD';
        accountList1[1].Type='Prospect';
        insert accountList1[1];   
        
        List<Asset> assetList1=SFS_TestDataFactory.createAssets(1, false);
        assetList1[0].AccountId = accountList1[1].Id;
        insert assetList1[0];
        
        List<ServiceContract> svcAgreementList1=SFS_TestDataFactory.createServiceAgreement(1,accountList1[1].Id,false);           
        svcAgreementList1[0].SFS_Invoice_Frequency__c = 'Annually';
        svcAgreementList1[0].Service_Charge_Frequency__c = 'Annually';
        svcAgreementList1[0].SFS_Status__c = 'APPROVED';
        svcAgreementList1[0].SFS_Invoice_Format__c = 'Detail';
        svcAgreementList1[0].SFS_PO_Number__c = '2';
        svcAgreementList1[0].SFS_Invoice_Type__c = 'Receivable';
        svcAgreementList1[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList1[0].SFS_Renewal_Escalator_Start_Date__c=system.today().addDays(-10);
        svcAgreementList1[0].SFS_Recurring_Adjustment__c=3;       
        svcAgreementList1[0].SFS_Type__c='PlannedCare';           
        insert svcAgreementList1;       
        
        List<ContractLineItem> cLi1=SFS_TestDataFactory.createServiceAgreementLineItem(1,svcAgreementList1[0].Id,false);
        insert cLi1[0];
        
        Invoice__c inv3 = new Invoice__c(RecordTypeId = CAREInvoiceRecordTypeId1,                                        
                                         SFS_Status__c='Created',
                                         SFS_Invoice_Format__c=svcAgreementList1[0].SFS_Invoice_Format__c,
                                         SFS_Billing_Period_End_Date__c=system.today(),                                    
                                         SFS_PO_Number__c = svcAgreementList1[0].SFS_PO_Number__c,
                                         SFS_Service_Agreement_Line_Item__c=cli1[0].Id,
                                         SFS_Service_Agreement_1__c=svcAgreementList1[0].Id,
                                         SFS_Invoice_Type__c = svcAgreementList1[0].SFS_Invoice_Type__c,
                                         SFS_Billing_Period_Start_Date__c= system.today()
                                        );
        insert inv3;
        
        Set<Id> scIds1 = new Set<Id>();       
        scIds1.add(svcAgreementList1[0].Id);        
        test.startTest();
        SFS_batchClassToCreateInvoiceLineItems bcli1 = new SFS_batchClassToCreateInvoiceLineItems(scIds1);
        database.executeBatch(bcli1);           
        test.stopTest();
    }
    
}