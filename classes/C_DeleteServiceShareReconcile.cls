global class C_DeleteServiceShareReconcile implements Database.Batchable<sObject>{ 
    global final String Query;
    
    global C_DeleteServiceShareReconcile(){
        
        Query='select id from Service_History__Share';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }   
    
    
    global void execute(Database.BatchableContext info, List<Service_History__Share> scope){
        List<Service_History__Share> jobShrs  = new List<Service_History__Share>();
        
        system.debug(scope);
        if(scope.size()>0)
        {
            for(Service_History__Share sh: scope)
            {
                system.debug('*****SHARE HERE INSERT***'+ sh); 
                jobShrs.add(sh);
            }
            
            if(jobShrs.size()>0)
                system.debug(jobShrs);
            delete jobShrs;
        }
    }     
    global void finish(Database.BatchableContext info){     
    } 

}