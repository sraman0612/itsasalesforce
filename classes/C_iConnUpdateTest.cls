@isTest
public class C_iConnUpdateTest {
	@isTest static void test()
    {
        Machine_Version_Parts_List__c mvpl = new Machine_Version_Parts_List__c(Oil_Filter_Quantity_Expected__c=1);
        insert mvpl;
        
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        
        Asset serial1 = new Asset(name = 'S483816-EDE99N', accountid = acct.id,Serial_Number_Model_Number__c='S483816-EDE99N',Machine_Version_Parts_List__c=mvpl.id,
                                 Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0);
        insert serial1;
        
        String JSONMsg = '{'+	
            '"Asset":[{'+
            '"IMEI__c":"tes123",'+
            '"Serial_Number_Model_Number__c" : "S483816-EDE99N"'+
            '}]'+
            '}';

        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            'https://cs53.salesforce.com/services/apexrest/iConn-IMEI/*';
        request.httpMethod = 'Post';
        //request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        system.debug(request);
        C_iConnUpdate.doPost();
        JSONMsg = '{'+	
            '"Asset":[{'+
            '"IMEI__c":"tes123",'+
            '"Serial_Number_Model_Number__c" : ""'+
            '}]'+
            '}';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = res;
        C_iConnUpdate.doPost();
    }
}