public with sharing class CTRL_QuoteDocGenerator
{
    @AuraEnabled
    public static UTIL_Aura.Response generateQuoteDocument(String quoteDocumentName, String paperSize, String quoteId, String templateId, String language, String outputFormat) {
        System.debug('we are inside generateQuoteDocument');
        QuoteProposalModel model = new QuoteProposalModel();
        model.name = quoteDocumentName;
        model.quoteId = quoteId;
        System.debug('templateId = ' + templateId);
        model.templateId = templateId;
        model.language = language;
        model.paperSize = paperSize;
        String fileExtension = 'PDF';
        model.outputFormat = outputFormat;
        if (model.outputFormat == 'MS Word') {
            fileExtension = 'DOC';
        }
        GenerateQuoteProposal proposalGenerator = new GenerateQuoteProposal();
        String jobId = proposalGenerator.save(model);
        System.debug('jobId' + jobId);

        if (jobId.contains('"')) {
            jobId = jobId.remove('"');
        }

        return UTIL_Aura.createResponse(jobId);
    }
    
    @AuraEnabled
    public static UTIL_Aura.Response getJobStatus(String jobId) {
        System.debug('jobId=' + jobId);

        if (jobId != null && jobId.contains('"')) {
            jobId = jobId.remove('"');
        }

        System.debug('jobId=' + jobId);
        
        List<AsyncApexJob> jobs = [SELECT Id, ApexClassID, CompletedDate, Status, ExtendedStatus, JobItemsProcessed, JobType, MethodName, TotalJobItems, NumberOfErrors from AsyncApexJob where id = : jobId];

        if (jobs.size() > 0) {
            System.debug('jobs.get(0).Status=' + jobs.get(0).Status);
        } else {
            System.debug('can\'t find job.');
        }

        return UTIL_Aura.createResponse(jobs.size() == 0 ? 'Queued' : jobs.get(0).Status);
    }

    @AuraEnabled
    public static UTIL_Aura.Response getQuoteDocument(String quoteDocumentName, String quoteId) {
        List<SBQQ__QuoteDocument__c> quoteDocuments = [SELECT Id,Name,SBQQ__DocumentId__c,SBQQ__Version__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__c = : quoteId AND Name = : quoteDocumentName ORDER BY SBQQ__Version__c DESC];

        if (quoteDocuments.size() == 0) {
            return UTIL_Aura.createResponse(null);
        }

        SBQQ__QuoteDocument__c latestDoc = quoteDocuments.get(0);

        Document theDoc = [SELECT Body FROM Document WHERE Id = : latestDoc.SBQQ__DocumentId__c LIMIT 1];

        Blob bodyBlob = theDoc.Body;

        String bodyStr = EncodingUtil.base64Encode(bodyBlob);
        //'data:application/pdf;base64,' + 
        return UTIL_Aura.createResponse(bodyStr);
    }

    @AuraEnabled
    public static UTIL_Aura.Response getScreenData(String quoteId) {
        SCREEN_DATA retVal = new SCREEN_DATA();

        retVal.quoteId = quoteId;

        retVal.showOutputFormat = getAllowOutputFormatChange();

        retVal.quoteDocumentName = generateQuoteDocumentName(quoteId);
        retVal.templateIdMap = generateQuoteTemplateMap();
        retVal.languageMap = generateLanguageMap();
        retVal.paperSizeMap = generatePaperSizeMap();

        if (retVal.showOutputFormat) {
            retVal.outputFormatMap = generateOutputFormatMap();
        }

        return UTIL_Aura.createResponse(JSON.serialize(retVal));
    }

    private static Boolean getAllowOutputFormatChange() {
        User currentUser = [SELECT SBQQ__OutputFormatChangeAllowed__c from User where Id =: UserInfo.getUserId() LIMIT 1];

        return (Test.isRunningTest()?true:currentUser.SBQQ__OutputFormatChangeAllowed__c);
    }

    private static String generateQuoteDocumentName(String quoteId) {
        SBQQ__Quote__c quote = null;
        
        if (quoteId != null) {
	        quote = [SELECT Id, Name, Account_Name__c from SBQQ__Quote__c where id =: quoteId LIMIT 1];
        }
        
        String dateFormat = 'yyyyMMdd-HHmm';

        DateTime dt = DateTime.now();
        String dateString = dt.format(dateFormat);
        System.debug(dateString);
        String formattedAccountName = (quote!=null&&quote.Account_Name__c!=null?quote.Account_Name__c:'');
        String quoteName = (quote!=null&&quote.Name!=null?quote.Name:'');

        formattedAccountName = formattedAccountName.replaceAll(' ','_');
        formattedAccountName = formattedAccountName.replaceAll('[^0-9A-Za-z_]','');

        String retVal = quoteName + '-' + formattedAccountName + (formattedAccountName != ''? '-' : '') + dateString;

        return retVal;
    }

    private static List<NAME_VALUE_PAIR> generateQuoteTemplateMap() {
        List<NAME_VALUE_PAIR> retVal = new List<NAME_VALUE_PAIR>();

        Profile curProfile = [SELECT Name from Profile where id =: UserInfo.getProfileId() LIMIT 1];

        Boolean isInternalUser = !curProfile.Name.startsWith('Partner Community');
        
        List<SBQQ__QuoteTemplate__c> templates = [SELECT Id, Name from SBQQ__QuoteTemplate__c where Share_With_Distributors__c = : !isInternalUser order by Name ASC];

        for (SBQQ__QuoteTemplate__c template : templates) {
            retVal.add(new NAME_VALUE_PAIR(template.Name, template.Id));
        }

        return retVal;
    }

    private static List<NAME_VALUE_PAIR> generateLanguageMap() {
        List<NAME_VALUE_PAIR> retVal = new List<NAME_VALUE_PAIR>();

        retVal.add(new NAME_VALUE_PAIR('English (US)', 'en_US'));

        return retVal;
    }

    private static List<NAME_VALUE_PAIR> generatePaperSizeMap() {
        List<NAME_VALUE_PAIR> retVal = new List<NAME_VALUE_PAIR>();
        
        retVal.add(new NAME_VALUE_PAIR('Default'));
        retVal.add(new NAME_VALUE_PAIR('Letter'));
        retVal.add(new NAME_VALUE_PAIR('Legal'));
        retVal.add(new NAME_VALUE_PAIR('A4'));

        return retVal;
    }

    private static List<NAME_VALUE_PAIR> generateOutputFormatMap() {
        List<NAME_VALUE_PAIR> retVal = new List<NAME_VALUE_PAIR>();
        
        retVal.add(new NAME_VALUE_PAIR('PDF', 'PDF'));
        retVal.add(new NAME_VALUE_PAIR('MS Word', 'MS Word'));

        return retVal;
    }

    public class NAME_VALUE_PAIR {
        public String name { get; set; }
        public String value { get; set; }

        public NAME_VALUE_PAIR(String name) {
            this.name = name;
            this.value = name;
        }

        public NAME_VALUE_PAIR(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    public class SCREEN_DATA {
        public String quoteDocumentName { get; set; }
        public String quoteId { get; set; }
        public Boolean showOutputFormat { get; set; }

        public List<NAME_VALUE_PAIR> templateIdMap { get; set; }
        public List<NAME_VALUE_PAIR> languageMap { get; set; }
        public List<NAME_VALUE_PAIR> paperSizeMap { get; set; }
        public List<NAME_VALUE_PAIR> outputFormatMap { get; set; }
    }
}