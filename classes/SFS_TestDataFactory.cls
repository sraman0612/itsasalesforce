@isTest
public class SFS_TestDataFactory {
    
      
    public static Account getAccount(){
        Double latitude = 36.127443;
        Double longitude = -115.171651;
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId(); 
        Account acct = new Account();
        acct.RecordTypeId = accRecID;
        acct.name = 'test account';
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.Currency__c='USD'; 
       // acct.Account_Division__c = 'a5c1h000000MSt1AAG';
        //acct.Account_Division__r.Name = 'Atlanta Customer Center';
        acct.ShippingStreet = '2342';
        acct.ShippingState = 'NC';
        acct.ShippingPostalCode = '28757';
        acct.ShippingCity = 'Montreat';
        acct.ShippingCountry = 'USA';
        //acct.Bill_To_Account__c = 'CTS_Bill_To_Account';
        acct.County__c = 'USA';
        acct.IRIT_Customer_Number__c = 'test123';
        insert acct;
        return acct;
    }
    public static Account createShipToAccount(){
        Double latitude = 36.127443;
        Double longitude = -115.171651;

        Account acct = new Account();
        acct.RecordTypeId = '0121h0000021AmCAAU';
        acct.name = 'test account';
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.Currency__c='USD'; 
        acct.Account_Division__r.Name = 'Atlanta Customer Center';
        acct.ShippingState = 'NC';
        acct.ShippingStreet = '2342 Appalachian Way Unit 156';
        acct.ShippingPostalCode = '28757';
        acct.ShippingCity = 'Montreat';
        acct.ShippingCountry = 'USA';
        acct.Bill_To_Account__c = 'CTS_Bill_To_Account';
        acct.County__c = 'USA';
       
        

        insert acct;
        return acct;
    }
    public static Contact getContact(){
        Contact con = new Contact( 
                                 Firstname = 'Test',
                                 LastName = 'Contact',
                                 title = 'tester',
                                 email = 'test@gmail.com',
                                 phone = '1234567890');
        insert con;
        return con;
    }
    
    public static Division__C getDivision(){
        Division__c div = new Division__c( 
                                 Name = 'TestLoc',
                                 SFS_Org_Code__c = 'DMN',
                                 Division_Type__c='Customer Center' ,
                                 EBS_System__c='Oracle 11i'   );
        insert div;
        return div;
    }
    public static List<Account> createAccounts(Integer numOfRecords, Boolean insertRecords){
        List<Account> accList=new List<Account>();
        for(Integer i=0;i<numOfRecords;i++){
            Account acc=new Account();
            acc.name = 'test account'+i;
            acc.Currency__c='USD';
            acc.ShippingCountry='USA';
            acc.ShippingCity ='Montreat';
            acc.ShippingPostalCode='28757';
            acc.ShippingState='NC';
            acc.ShippingStreet='2342 Appalachian Way\nUnit 156';
            
            accList.add(acc);
        }
        if(insertRecords && accList.size()>0)
            insert accList;
        return accList;
    }
    public static List<Division__C> createDivisions(Integer numOfRecords, Boolean insertRecords){
        //String RecordTypeId = Schema.SObjectType.Division__C.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<Division__C> divisionList=new List<Division__C>();
         
        for(Integer i=1;i<=numOfRecords;i++){
            Division__C Division1=new Division__C();
            //Location1.RecordTypeId=LocationRecordTypeId;
            Division1.Name='Division'+i;
            Division1.SFS_Org_Code__c='x9e';
            Division1.Division_Type__c='Customer Center';
            Division1.EBS_System__c='Oracle 11i';
            divisionList.add(Division1);
        }
        if(insertRecords && divisionList.size()>0)
            insert divisionList;
        return divisionList;
    }
    
    
    
    
    
    
     public static List<Schema.Location> createLocations(Integer numOfRecords, String diviId, Boolean insertRecords){
        String LocationRecordTypeId = Schema.SObjectType.Location.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<Schema.Location> locationList=new List<Schema.Location>();
         
        for(Integer i=1;i<=numOfRecords;i++){
            Schema.Location Location1=new Schema.Location();
            Location1.RecordTypeId=LocationRecordTypeId;
            Location1.Name='Location'+i;
            Location1.SFS_Division__c=diviId;
            locationList.add(Location1);
        }
        if(insertRecords && locationList.size()>0)
            insert locationList;
        return locationList;
    }
    
    
    
//Create Pricebook     
    public static Pricebook2 getPricebook2(){
        //Instantiate the Pricebook2 record with StandardPricebookId
        //Pricebook2 standardPricebook = [SELECT Id, Name FROM Pricebook2 WHERE IsStandard = true limit 1];
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        //Execute an update DML on the Pricebook2 record, to make IsStandard to true
        Update standardPricebook;
        
        //Query for the Pricebook2 record, to check IsStandard field
        standardPricebook = [SELECT Id, IsStandard FROM Pricebook2 WHERE Id = :standardPricebook.Id];
        //It should return true
        System.assertEquals(true, standardPricebook.IsStandard);
        return standardPricebook;
    }
//Method to create PriceBookEntry    
    public static List<PricebookEntry> createPricebookEntry(Integer numOfRecords,Boolean insertRecords){
        //Inserting Product
        Product2 pro = new Product2(Name = 'testProduct');
        insert pro;
        //Inserting PricebookEntry
        Pricebook2 standardPricebook = getPricebook2();

        List<PricebookEntry> entriesList=new List<PricebookEntry>();
        for(Integer i=1;i<=numOfRecords;i++){
        	PricebookEntry entry = new PricebookEntry();
        	entry.Pricebook2Id = standardPricebook.Id;
        	entry.Product2Id = pro.Id;
        	entry.UnitPrice = 2.00;
        	entry.IsActive = true;
        	//entry.UseStandardPrice = true;
            entriesList.add(entry);
        }
        if(insertRecords && entriesList.size()>0)
        	insert entriesList;
        return entriesList;
    }
//Create Frame Type Records
    public static List<CTS_IOT_Frame_Type__c> createFrameTypes(Integer numOfRecords,Boolean insertRecords, String type){
        List<CTS_IOT_Frame_Type__c> frameTypeList=new List<CTS_IOT_Frame_Type__c>();
        for(Integer i=1;i<=numOfRecords;i++){
            CTS_IOT_Frame_Type__c ft=new CTS_IOT_Frame_Type__c();
            ft.Name='test '+i;
            ft.CTS_IOT_Type__c=type;
            frameTypeList.add(ft);
        }
        if(insertRecords && frameTypeList.size()>0)
            insert frameTypeList;
        return frameTypeList;
    }
//Create Asset Records
    public static List<Asset> createAssets(Integer numOfRecords,Boolean insertRecords){
        String AssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('NA Air Edit').getRecordTypeId();
  
        List<Asset> assetList=new List<Asset>();
        Account acc=new Account();
        acc.name = 'test account';
        acc.Currency__c='USD';
        acc.ShippingCountry='USA';
        acc.ShippingCity ='Montreat';
        acc.ShippingPostalCode='28757';
        acc.ShippingState='NC';
        acc.ShippingStreet='2342 Appalachian Way\nUnit 156';
        Database.DMLOptions opts = new Database.DMLOptions();       
        opts.DuplicateRuleHeader.AllowSave = true;        
        Database.insert(acc, opts);
        for(Integer i=1;i<=numOfRecords;i++){
            Asset asset1=new Asset();
            asset1.RecordTypeId=AssetRecordTypeId;
            asset1.Name='Asset'+'i';
            //asset1.CurrencyIsoCode='USD';
            asset1.Model_Name__c='2345';
            //asset1.CG_Org_Channel__c ='IR';
            asset1.AccountId = acc.Id;
            assetList.add(asset1);
        }
        if(insertRecords && assetList.size()>0)
            insert assetList;
        return assetList;
    }
//Create Service Agreement records
    public static List<ServiceContract> createServiceAgreement(Integer numOfRecords,String accId,Boolean insertRecords){
        String sA_RECORDTYPEID = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('PackageCARE').getRecordTypeId();
        Pricebook2 priceBook=getPricebook2();
        List<ServiceContract> svcAgreementList=new List<ServiceContract>();
        for(Integer i=1;i<=numOfRecords;i++){
            ServiceContract agreement=new ServiceContract();
            agreement.Name='agreement'+i;
            agreement.AccountId=accId;
            agreement.SFS_Type__c='PackageCARE';
            agreement.SFS_Portable_Required__c='Yes';
            agreement.StartDate=System.today();
            agreement.EndDate=System.today().addYears(2);
            agreement.SFS_Freight_Terms__c='Prepaid';
            agreement.Pricebook2Id=priceBook.Id;
            agreement.recordTypeId=sA_RECORDTYPEID;
            agreement.SFS_Invoice_Frequency__c='Quarterly';
            svcAgreementList.add(agreement);
        }
        if(insertRecords && svcAgreementList.size()>0)
            insert svcAgreementList;
        return svcAgreementList;
    }
//Create Service Agreement Line Item records
    public static List<ContractLineItem> createServiceAgreementLineItem(Integer numOfRecords,String svcAgreementId,Boolean insertRecords){
        List<ContractLineItem> lineItemList=new List<ContractLineItem>();
        List<PricebookEntry> entry=createPricebookEntry(1,true);
        for(Integer i=1;i<=numOfRecords;i++){
            ContractLineItem lineItem=new ContractLineItem();
            lineItem.ServiceContractId=svcAgreementId;
            lineItem.Quantity=3;
            lineItem.UnitPrice=2.00;
            lineItem.PricebookEntryId=entry[0].Id;
            lineItemList.add(lineItem);
        }
        if(insertRecords && lineItemList.size()>0)
            insert lineItemList;
        return lineItemList;
    }
//Create Invoice reords
    public static List<Invoice__c> createInvoice(Integer numOfRecords,Boolean insertRecords){
        String invoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Service Invoice').getRecordTypeId();
        List<Invoice__c> invoiceList=new List<Invoice__c>();
        for(Integer i=1;i<=numOfRecords;i++){
            Invoice__c invoice=new Invoice__c();
            invoice.RecordTypeId=invoiceRecordTyeId;
            invoice.SFS_Invoice_Format__c='Detail';
            invoice.SFS_Invoice_Print_Code__c='Send to Customer';
            invoiceList.add(invoice);
        }
        if(insertRecords && invoiceList.size()>0)
            insert invoiceList;
        return invoiceList;
    }
//Create Charges for Work Order
    public static List<CAP_IR_Charge__c> createCharge(Integer numOfRecords,String woId,Boolean insertRecords){
        String chargeRecordTyeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        List<CAP_IR_Charge__c> chargeList=new List<CAP_IR_Charge__c>();
        for(Integer i=1;i<=numOfRecords;i++){
            CAP_IR_Charge__c charge=new CAP_IR_Charge__c();
            charge.CAP_IR_Work_Order__c=woId;
            charge.CAP_IR_Amount__c=1000;
            charge.SFS_Sell_Price__c=1200;
            chargeList.add(charge);
        }
        if(insertRecords && chargeList.size()>0)
            insert chargeList;
        return chargeList;
    }
//Create Entitlement Records
//Make sure asset's Product code and service agreement line item's product code matches
    public static List<Entitlement> createEntitlement(Integer numOfRecords,String accId,String assetId,String svcAggId,String svcAggLineItemId,Boolean insertRecords){
    	system.debug('svcAggId'+svcAggId);
        List<Entitlement> entitlementList=new List<Entitlement>();
        for(Integer i=1;i<=numOfRecords;i++){
        	Entitlement entitlement1=new Entitlement();
            entitlement1.Name='entitlement '+i;
            entitlement1.AccountId=accId;
            entitlement1.StartDate=System.today();
            entitlement1.EndDate=System.today().addYears(2);
            entitlement1.SFS_Contract_Hours__c=20000;
            entitlement1.AssetId=assetId;
            entitlement1.ContractLineItemId=svcAggLineItemId;
            entitlement1.ServiceContractId=svcAggId;
            entitlement1.SFS_X1st_Work_Scope__c = '8';
            entitlement1.SFS_X1st_PM_Month__c = 'September';
            entitlementList.add(entitlement1);
        }
        if(insertRecords && entitlementList.size()>0)
            insert entitlementList;
        return entitlementList;
    }
//Create Product Records
    public static List<Product2> createProduct(Integer numOfRecords,Boolean insertRecords){
        String productRecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('IR Comp Field Service Lightning').getRecordTypeId();
        List<Product2> productList=new List<Product2>();
        for(Integer i=1;i<=numOfRecords;i++){
            Product2 product=new Product2();
            product.Name='test product'+i;
            product.productCode='test product'+i+2;
            product.IsActive=true;
            product.SFS_Oracle_Orderable__c='y';
            product.SFS_External_Id__c='test'+i;
            product.RecordTypeId = productRecordTypeId;
            productList.add(product);
        }
        if(insertRecords && productList.size()>0)
            insert productList;
        return productList;
    }
//Create Work Type records
    public static List<WorkType> createWorkType(Integer numOfRecords,Boolean insertRecords){
        String recordTypeId = Schema.SObjectType.WorkType.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<WorkType> workTypeList=new List<WorkType>();
		for(Integer i=1;i<=numOfRecords;i++){
            WorkType wt=new WorkType();
            wt.Name='test'+i;
            wt.EstimatedDuration=2;
            wt.DurationType='Hours';
            wt.SFS_Work_Scope1__c='2';
            wt.SFS_Work_Order_Type__c='Preventive Maintenance';
            wt.SFS_Exclude_Autogenerate__c=false;
            workTypeList.add(wt);
        }
        if(insertRecords && workTypeList.size()>0)
            insert workTypeList;
        return workTypeList;
    }
//Create Work Order Records
    public static List<WorkOrder> createWorkOrder(Integer numOfRecords,String accId,String locId, String divId, String svcAgreementId,Boolean insertRecords){
        String woRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<WorkOrder> woList=new List<WorkOrder>();
        for(Integer i=1;i<=numOfRecords;i++){
            WorkOrder wo=new WorkOrder();
            wo.AccountId=accId;
            wo.SFS_Consumables_Ship_To_Account__c=accId;
            wo.Shipping_Account__c=accId;
            wo.ServiceContractId=svcAgreementId;
            wo.SFS_Work_Order_Type__c='Installation';
            wo.RecordTypeId=woRecordTypeId;
            wo.SFS_Division__c=divId;
            wo.SFS_PO_Number__c='123';
            wo.SFS_Requested_Payment_Terms__c='NET 30';
            wo.LocationId=locId;
            wo.status='New';
            wo.StartDate=system.Today();
            //wo.EndDate=system.Today()+2;
            woList.add(wo);
        }
        if(insertRecords && woList.size()>0)
            insert woList;
        return woList;
    }
//Create workorderlineitem records
    public static List<WorkOrderLineItem> createWorkOrderLineItem(Integer numOfRecords,String woId,String workTypeId,Boolean insertRecords){
        String woliRecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        for(Integer i=1;i<=numOfRecords;i++){
            WorkOrderLineItem woli=new WorkOrderLineItem();
            woli.WorkOrderId=woId;
            woli.WorkTypeId=workTypeId;
            woli.RecordTypeId=woliRecordTypeId;
            woliList.add(woli);
        }
        if(insertRecords && woliList.size()>0)
            insert woliList;
        return woliList;
    }
//Create Service Appointment records
    public static List<ServiceAppointment> createServiceAppointment(Integer numOfRecords,String woliId,Boolean insertRecords){
        String saRecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<ServiceAppointment> saList=new List<ServiceAppointment>();
        for(Integer i=1;i<=numOfRecords;i++){
            ServiceAppointment appointment=new ServiceAppointment();
            appointment.ParentRecordId=woliId;
            saList.add(appointment);
        }
        if(insertRecords && saList.size()>0)
            insert saList;
        return saList;
    }
//Create Product Transfer records
    public static List<ProductTransfer> createProductTransfer(Integer numOfRecords,String productId,String locId,Boolean insertRecords){
        List<ProductTransfer> productTransferList=new List<ProductTransfer>();
        for(Integer i=1;i<=numOfRecords;i++){
            ProductTransfer prodTransfer=new ProductTransfer();
            prodTransfer.DestinationLocationId=locId;
            prodTransfer.Product2Id=productId;
            prodTransfer.QuantitySent=1;
            productTransferList.add(prodTransfer);
        }
        if(insertRecords && productTransferList.size()>0)
            insert productTransferList;
        return productTransferList;
    }
//Create Product Consumed records
    public static List<ProductConsumed> createProductConsumed(Integer numOfRecords,String woliId,String productItemId,Boolean insertRecords){
        List<ProductConsumed> productConsumedList=new List<ProductConsumed>();
        for(Integer i=1;i<=numOfRecords;i++){
            ProductConsumed prodConsumed=new ProductConsumed();
            prodConsumed.QuantityConsumed=1;
            prodConsumed.ProductItemId=productItemId;
            prodConsumed.WorkOrderLineItemId=woliId;
            productConsumedList.add(prodConsumed);
        }
        if(insertRecords && productConsumedList.size()>0)
            insert productConsumedList;
        return productConsumedList;
    }
    //Create Product item records
     public static List<ProductItem> createProductItem(Integer numOfRecords,String LocId,String productId,Boolean insertRecords){
        List<ProductItem> productItemList=new List<ProductItem>();
        for(Integer i=1;i<=numOfRecords;i++){
            ProductItem prodItem=new ProductItem();
            prodItem.LocationId=LocId;
            prodItem.Product2Id=productId;
            prodItem.QuantityOnHand=10;
            productItemList.add(prodItem);
        }
        if(insertRecords && productItemList.size()>0)
            insert productItemList;
        return productItemList;
    }
//Create Product Required records
    public static List<ProductRequired> createProductRequired(Integer numOfRecords,String woId,String productId,Boolean insertRecords){
        List<ProductRequired> productRequiredList=new List<ProductRequired>();
        for(Integer i=1;i<=numOfRecords;i++){
            ProductRequired prodRequired=new ProductRequired();
            prodRequired.ParentRecordId=woId;
            prodRequired.Product2Id=productId;
            prodRequired.QuantityRequired=1;
            productRequiredList.add(prodRequired);
        }
        if(insertRecords && productRequiredList.size()>0)
            insert productRequiredList;
        return productRequiredList;
    }
//Create Product Request records
    public static List<ProductRequest> createProductRequest(Integer numOfRecords,String woId,String woliId,Boolean insertRecords){
        String pR_RECORDTYPEID = Schema.SObjectType.ProductRequest.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<ProductRequest> productRequestList=new List<ProductRequest>();
        for(Integer i=1;i<=numOfRecords;i++){
            ProductRequest request=new ProductRequest();
            request.WorkOrderId=woId;
            request.WorkOrderLineItemId=woliId;
            request.SFS_Freight_Terms__c='Prepaid';
            request.Status='Open';
            request.SFS_Priority__c='150';
            request.RecordTypeId=pR_RECORDTYPEID;
            productRequestList.add(request);
        }
        if(insertRecords && productRequestList.size()>0)
            insert productRequestList;
        return productRequestList;
    }
//Create Product Request Line Item records
	public static List<ProductRequestLineItem> createPRLI(Integer numOfRecords,String prodRequestId,String productId,Boolean insertRecords){
        String pRLI_RECORDTYPEID = Schema.SObjectType.ProductRequestLineItem.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<ProductRequestLineItem> prliList=new List<ProductRequestLineItem>();
        for(Integer i=1;i<=numOfRecords;i++){
            ProductRequestLineItem prli=new ProductRequestLineItem();
            prli.ParentId=prodRequestId;
            prli.Product2Id=productId;
            prli.QuantityRequested=2;
            prli.Status='Open';
            prli.RecordTypeId=pRLI_RECORDTYPEID;
            prliList.add(prli);
        }
        if(insertRecords && prliList.size()>0)
            insert prliList;
        return prliList;
    }
//Create Return order records
    public static List<ReturnOrder> createReturnOrders(Integer numOfRecords,String woId,Boolean insertRecords){
        List<ReturnOrder> orderList=new List<ReturnOrder>();
        for(Integer i=1;i<=numOfRecords;i++){
            ReturnOrder order=new ReturnOrder();
            order.SFS_WorkOrder__c=woId;
			orderList.add(order);
        }
        if(insertRecords && orderList.size()>0)
            insert orderList;
        return orderList;
    }
//Create ReturnOrderLineItem records
    public static List<ReturnOrderLineItem> createROLIs(Integer numOfRecords,String returnOrderId,String productId,Boolean insertRecords){
        List<ReturnOrderLineItem> roliList=new List<ReturnOrderLineItem>();
        for(Integer i=1;i<=numOfRecords;i++){
            ReturnOrderLineItem lineItem=new ReturnOrderLineItem();
            lineItem.ReturnOrderId=returnOrderId;
            lineItem.QuantityReturned=1;
            lineItem.Product2Id=productId;
            roliList.add(lineItem);
        }
        if(insertRecords && roliList.size()>0)
            insert roliList;
        return roliList;
    }
//Create Expense records
    public static List<Expense> createExpense(Integer numOfExpenses,String woId,String woliId,Boolean insertRecords){
        List<Expense> expensesList=new List<Expense>();
        for(Integer i=1;i<=numOfExpenses;i++){
            Expense exp=new Expense();
            exp.WorkOrderId=woId;
            exp.SFS_Work_Order_Line_Item__c=woliId;
            exp.Amount=100;
            exp.TransactionDate=System.today();
            expensesList.add(exp);
        }
        if(insertRecords && expensesList.size()>0)
        	insert expensesList;
        return expensesList;
    }
//Create Labor Hour records
    public static List<CAP_IR_Labor__c> createLaborHours(Integer numOfRecords,String woId,String woliId,Boolean insertRecords){
        List<CAP_IR_Labor__c> laborHoursList=new List<CAP_IR_Labor__c>();
        for(Integer i=1;i<=numOfRecords;i++){
            CAP_IR_Labor__c laborHour=new CAP_IR_Labor__c();
            laborHour.CAP_IR_Work_Order__c=woId;
            laborHour.CAP_IR_Work_Order_Line_Item__c=woliId;
            laborHour.CAP_IR_Start_Date__c=System.today()-1;
            laborHour.CAP_IR_End_Date__c=System.today();
            laborHour.CAP_IR_Labor_Rate__c='Standard';
            laborHoursList.add(laborHour);
        }
        if(insertRecords && laborHoursList.size()>0)
        	insert laborHoursList;
        return laborHoursList;
    }
//Create maintenance plan records
    public static List<MaintenancePlan> createMaintenancePlan(Integer numOfRecords,String svcAgreementId,Boolean insertRecords){
        List<MaintenancePlan> mPlansList=new List<MaintenancePlan>();
        for(Integer i=1;i<=numOfRecords;i++){
            MaintenancePlan mPlan=new MaintenancePlan();
            mPlan.StartDate=Date.newInstance(2022, 01, 01);
            mPlan.EndDate=Date.newInstance(2022, 01, 31);
            mPlan.ServiceContractId=svcAgreementId;
            //mPlan.Frequency=5;
            //mPlan.FrequencyType='Months';
            mPlan.GenerationHorizon=60;
            mPlan.GenerationTimeframe=1;
            mPlan.GenerationTimeframeType='Months';
            mPlan.NextSuggestedMaintenanceDate=System.today()+175;
            mPlansList.add(mPlan);
        }
        if(insertRecords && mPlansList.size()>0)
        	insert mPlansList;
        return mPlansList;
    }
//Create Maintenance Asset records
    public static List<MaintenanceAsset> createMaintenanceAsset(Integer numOfRecords,String maintenancePlanId,String assetId,Boolean insertRecords){
        List<MaintenanceAsset> mAssetsList=new List<MaintenanceAsset>();
        for(Integer i=1;i<=numOfRecords;i++){
            MaintenanceAsset mAsset=new MaintenanceAsset();
            mAsset.MaintenancePlanId=maintenancePlanId;
            mAsset.AssetId=assetId;
            mAssetsList.add(mAsset);
        }
        if(insertRecords && mAssetsList.size()>0)
        	insert mAssetsList;
        return mAssetsList;
    }
//Create Maintenance Work Rule records
    public static List<MaintenanceWorkRule> createMaintenanceWorkRule(Integer numOfRecords,String maintenanceAssetId,String workTypeId,Boolean insertRecords){
        List<MaintenanceWorkRule> mwrList=new List<MaintenanceWorkRule>();
        for(Integer i=1;i<=numOfRecords;i++){
            MaintenanceWorkRule mwr=new MaintenanceWorkRule();
            mwr.ParentMaintenanceRecordId=maintenanceAssetId;
            mwr.WorkTypeId=workTypeId;
            mwr.SortOrder=1;
            mwr.Title='mwr '+i;
            mwr.RecurrencePattern='FREQ=MONTHLY;BYSETPOS=-1;BYDAY=SU,MO,TU,WE,TH,FR,SA;COUNT=1;';
            mwrList.add(mwr);
        }
        if(insertRecords && mwrList.size()>0)
        	insert mwrList;
        return mwrList;
    }
//Create Product Relationship records
	public static List<SFS_Product_Relationship__c> createProductRelationships(Integer numOfRecords,String productId,String newProductCode,Boolean insertRecords){
    	List<SFS_Product_Relationship__c> recordsList=new List<SFS_Product_Relationship__c>();
        for(Integer i=1;i<=numOfRecords;i++){
            SFS_Product_Relationship__c record=new SFS_Product_Relationship__c();
            //record.Name='test'+i;
            record.SFS_Product__c=productId;
            record.SFS_Relationship_Type__c='Supersession';
            record.SFS_Related_Item__c=newProductCode;
            recordsList.add(record);
        }
        if(insertRecords && recordsList.size()>0)
        	insert recordsList;
        return recordsList;
    }
//Create Asset Parts Required
    public static List<CAP_IR_Asset_Part_Required__c> createPartsRequired(Integer numOfRecords,String assetId,String productId,Boolean insertRecords){
        List<CAP_IR_Asset_Part_Required__c> partsRequiredList=new List<CAP_IR_Asset_Part_Required__c>();
        for(Integer i=1;i<=numOfRecords;i++){
            CAP_IR_Asset_Part_Required__c record=new CAP_IR_Asset_Part_Required__c();
            record.Type__c='Standard';
            record.Asset__c=assetId;
            record.Product__c=productId;
            record.SFS_Quantity__c=2;
            partsRequiredList.add(record);
        }
        if(insertRecords && partsRequiredList.size()>0)
        	insert partsRequiredList;
        return partsRequiredList;
    }
    // create shipment records
    public static List<Shipment> createShipment(Integer numOfRecords,String prId,Boolean insertRecords){
        List<shipment> shipmentList=new List<Shipment>();
        for(Integer i=1;i<=numOfRecords;i++){
            Shipment record=new Shipment();
            record.ShipToName='test'+i;
            record.SFS_Product_Request__c=prId;
           
            shipmentList.add(record);
        }
        if(insertRecords && shipmentList.size()>0)
        	insert shipmentList;
        return shipmentList;
    }
    // create shipmentItems records
    public static List<ShipmentItem> createShipmentItem(Integer numOfRecords,String shipmentId,String prId,String prliId,Boolean insertRecords){
        List<shipmentItem> shipmentItemList=new List<ShipmentItem>();
        for(Integer i=1;i<=numOfRecords;i++){
            ShipmentItem record=new ShipmentItem();
            record.ShipmentId=shipmentId;
            record.SFS_Product_Request__c=prId;
           record.SFS_Product_Request_Line_Item__c=prliId;
            record.SFS_Freight_Charges__c=100;
            record.Quantity=10;
            record.SFS_Status__c='Shipped';
            shipmentItemList.add(record);
        }
        if(insertRecords && shipmentItemList.size()>0)
        	insert shipmentItemList;
        return shipmentItemList;
    }
    // create Location Skill Requirement records
    public static List<SFS_Location_Skill_Requirement__c> createLocationSkillReq(Integer numOfRecords,String skillId, String locationId, Boolean insertRecords){
        List<SFS_Location_Skill_Requirement__c> recordsList=new List<SFS_Location_Skill_Requirement__c>();
        for(Integer i=1;i<=numOfRecords;i++){
            SFS_Location_Skill_Requirement__c record=new SFS_Location_Skill_Requirement__c();
            record.Location__c = locationId;
            record.Skill_Id__c = skillId;
            recordsList.add(record);
        }
        if(insertRecords && recordsList.size()>0)
        	insert recordsList;
        return recordsList;
    }
    // Create Skill Requirement records
    public static List<SkillRequirement> createSkillReq(Integer numOfRecords,String skillId, String woliId, Boolean insertRecords){
        List<SkillRequirement> recordsList=new List<SkillRequirement>();
        for(Integer i=1;i<=numOfRecords;i++){
            SkillRequirement record=new SkillRequirement();
            record.RelatedRecordId = woliId;
            record.SkillId = skillId;
            recordsList.add(record);
        }
        if(insertRecords && recordsList.size()>0)
        	insert recordsList;
        return recordsList;
    }
    //Create Test User
        public static List<User> createTestUsers(Id profileId, String username) {
        List<User> listOfUser = new List<User>();

        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User dummyUser = new User(UserRoleId = portalRole.Id,
                ProfileId = profileId,
                Username = username,
                Alias = 'User1',
                Email = username,
                EmailEncodingKey = 'UTF-8',
                Firstname = 'Jane',
                Lastname = 'Doe',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                TimeZoneSidKey = 'America/Chicago',
                FederationIdentifier = '1231'
        );
        listOfUser.add(dummyUser);
        insert listOfUser;
        return listOfUser;
    }
    
    //Create Sample User
        public static User createTestUser(String ProfileName,Boolean isInsert)
    {
        Id p = [select id from profile where name=:ProfileName].id;
        User SampleUser = new User(alias = 'Sample', email='SampleUser@gmail.com',
                emailencodingkey='UTF-8', lastname='Sample User', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                timezonesidkey='America/Los_Angeles', username='SampleUserSFS@gmail.com'
                );

        if(isInsert)
            insert sampleUser;

        return SampleUser;

    }
    
    
}