public with sharing class GDInsideUserWrapper {
	public String username;
	public String first_name;
	public String last_name;
	public String phone_number;
	public String sap_account_number;
	public String address_1;
	public String city;
	public String state;
	public String zip;
	public String sales_force_profile_id;
	public String identifier;
}