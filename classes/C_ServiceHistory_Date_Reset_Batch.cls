//test class C_Warranty_Checker_Test
public class C_ServiceHistory_Date_Reset_Batch implements Database.batchable<sObject>{

    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id FROM Asset WHERE Id IN (SELECT Serial_Number__c FROM Service_History__c WHERE CreatedDate >= 2021-03-12T00:00:00Z)';
        return Database.getQueryLocator(query);
    }
    
    
    public void execute(Database.BatchableContext info, List<Asset> scope){
        System.debug(scope[0]);        
        
        Service_History__c sh = [SELECT Date_Of_Service__c FROM Service_History__c WHERE Serial_Number__c = :scope[0].Id ORDER BY Date_Of_Service__c DESC LIMIT 1];
        
        Asset sn = scope[0];
        
        sn.Last_Logged_Service_Visit__c = sh.Date_Of_Service__c;
        System.debug(sn);
        
        update sn;        
    }     
    
    public void finish(Database.BatchableContext info){     
        //nothing to do here
    } 
    
    
}