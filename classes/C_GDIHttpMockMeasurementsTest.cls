@isTest
global class C_GDIHttpMockMeasurementsTest implements HttpCalloutMock{
  global HTTPResponse respond(HTTPRequest req) {

        // Create a fake response.

        // Set response values, and

        HttpResponse res1 = new HttpResponse();
        res1.setHeader('Content-Type', 'application/json');
        res1.setBody('{"measurements":[{"time":"2018-09-06T03:59:00.000Z","id":"317096134","self":"https://industrials.iconn.gardnerdenver.com/measurement/measurements/317096134","source":{"id":"142327920","self":"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/142327920"},"type":"","c8y_H_Last_Fault":{"H_Last_Fault":{"unit":"h","value":1534978688}}}],"statistics":{"currentPage":1,"pageSize":2000,"totalPages":1},"self":"https://industrials.iconn.gardnerdenver.com/measurement/measurements?dateTo=2018-09-06T03:59:00.000Z&pageSize=2000&source=142327920&dateFrom=2018-09-06T03:59:00.000Z&currentPage=1&withTotalPages=true"}');
        res1.setStatusCode(200);
        return res1;

    }
}