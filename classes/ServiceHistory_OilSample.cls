@RestResource(urlMapping='/oil-sample/*')
global class ServiceHistory_OilSample {
    
    private class Oil_Sample {
        Oil_Sample__c Oil_Sample;
    }
    
    @httpPost
    global static void doPost() {
        RestContext.response.addHeader('Content-Type', 'application/json');
        JSONGenerator gen = JSON.createGenerator(false);
        
        Oil_Sample container = (Oil_Sample)System.JSON.deserialize(RestContext.request.requestBody.toString(), Oil_Sample.class);
        Oil_Sample__c newRec = container.Oil_Sample;
        
        Warranty_Compliance__c dateRange = Warranty_Compliance__c.getOrgDefaults();

        try {
            // Format serial num if it is incorrectly formatted
            if (newRec.Serial_Number_Text__c.containsWhitespace()) {
                newRec.Serial_Number_Text__c = newRec.Serial_Number_Text__c.substringBefore(' ');
            }
            
            if (newRec.Serial_Number_Text__c.contains('-')) {
                newRec.Serial_Number_Text__c = newRec.Serial_Number_Text__c.substringBefore('-');
            }
            
            // Asset lookup with SN
            // Using SN/Oil sample data, associate this to a SH record (based on creation date of SH [Date_of_Service__c] and collection date of oil sample [Collect_Date__c])
            Service_History__c[] sh = null;
            Asset sn = null;
            Account acc;
            Decimal shHours = null;
            Boolean noSN = false;
            
            try {
                // If cannot find sn, orphan them 
                //sn = [SELECT Id, Name FROM Asset WHERE Serial_Number_Model_Number__c = :newRec.Serial_Number_Text__c];
                sn = [SELECT Id, Name, SerialNumber, Current_Servicer__c, Oil_Sample_Next_Expected_Date__c, Hours_Oil_Sample__c FROM Asset WHERE Name = :newRec.Serial_Number_Text__c LIMIT 1];
                System.debug('sn: ' + sn);
                newRec.Serial_Number_Lookup__c = sn.Id;
                Decimal dr = newRec.Date_Range__c;
                
                // Allow for customization in date range on request, if field is blank use defaults from custom settings
                if (newRec.Date_Range__c == null) {
                    dr = dateRange.Oil_Sample_Date_Range__c;
                }
 
                // Finding SH in date range
                //sh = [SELECT Id, Name, Date_of_Service__c FROM Service_History__c WHERE Serial_Number__r.Serial_Number_Model_Number__c = :newRec.Serial_Number_Text__c];
                sh = [SELECT Id, Name, Date_of_Service__c, Account__c, Current_Hours__c FROM Service_History__c WHERE Serial_Number__r.Name = :newRec.Serial_Number_Text__c];
                for (Service_History__c s : sh) {
                    if (newRec.Collect_Date__c.daysBetween(s.Date_of_Service__c) <= dr && newRec.Collect_Date__c.daysBetween(s.Date_of_Service__c) >= -dr) {
                        System.debug('Found Service History rec. Dates in range. Date of service: ' + s.Date_of_Service__c + '; Oil collection date: ' + newRec.Collect_Date__c);
                        newRec.Service_History__c = s.Id;
                        System.debug('Current Hours: ' + s.Current_Hours__c);
                        shHours = s.Current_Hours__c;
                        //newRec.Distributor__c = s.Account__c;
                        System.debug('SH Id: ' + s.Id);
                        System.debug('SH account: ' + s.Account__c);
                        break;
                    }
                }
                
                // Tie lube sample to account
                try {
                    string cd = '%'+newRec.Collecting_Distributor__c+'%';
                    acc = [SELECT Id FROM Account WHERE Lube_Sample_Account_Name__c LIKE :cd];
                    newRec.Distributor__c = acc.Id;
                } catch (Exception z) {
                    acc = [SELECT Id FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
                    newRec.Distributor__c = acc.Id;
                }
            } catch (Exception e) {
                // If invalid SN (or doesn't exist)
                System.debug('No SN Error: ' + e.getMessage());
                // Tie lube sample to account
                try {
                    string cd = '%'+newRec.Collecting_Distributor__c+'%';
                    acc = [SELECT Id FROM Account WHERE Lube_Sample_Account_Name__c LIKE :cd];
                    newRec.Distributor__c = acc.Id;
                } catch (Exception z) {
                    acc = [SELECT Id FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
                    newRec.Distributor__c = acc.Id;
                }
            }
            
            // If no service history was attached, use the account from the SN
            if(sn != null){
                if (sn.Current_Servicer__c == null) {
                    gen.writeStartObject();
                    gen.writeNumberField('statusCode', 404);
                    gen.writeStringField('error', 'No current servicer or valid account found for serial number.');
                    gen.writeEndObject();
                    RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
                }
                // Oil sample compliance logic
                if (sn.oil_sample_next_expected_date__c == null) sn.oil_sample_next_expected_date__c = date.today().addMonths(6);
                Integer dateDiff = newRec.Collect_Date__c.daysBetween(sn.oil_sample_next_expected_date__c);
                Date expDate = sn.oil_sample_next_expected_date__c;
                Integer oilSampleHrs = 2000;
                decimal hourOverageThreshold = Warranty_Compliance__c.getOrgDefaults().Hour_Overage_Threshold_Percent__c/100;
                
                System.debug('date diff: ' + dateDiff);
                
                if (shHours == null) shHours = 0;
                
                System.debug('Current hours: ' + shHours);
                
                if (sn.Hours_Oil_Sample__c == null) sn.Hours_Oil_Sample__c = 0;
                
                System.debug('Hours oil sample: ' + sn.Hours_Oil_Sample__c);
                
                // only check hours if we received parts, otherwise wait until 6 mon check
                boolean hoursCompliance = true;   
                decimal OilSampleHourDifference = shHours - sn.Hours_Oil_Sample__c;
                
                boolean warrantyCheck = false;
                
                decimal oilSampleMultiplier = OilSampleHourDifference.divide(oilSampleHrs,2);
                decimal oilSampleFloor = Math.floor(oilSampleMultiplier);
                decimal oilSampleRemainder = oilSampleMultiplier - oilSampleFloor;
                
                if (oilSampleRemainder  < hourOverageThreshold){
                    // OK
                }
                else if (oilSampleRemainder > 1 - hourOverageThreshold) {
                    oilSampleMultiplier = oilSampleFloor + 1;
                }
                else {
                    // hours overage - remainder is great than the threshold allowed
                    hoursCompliance = false;
                }
                
                if(OilSampleHourDifference > oilSampleHrs + (oilSampleHrs * hourOverageThreshold)) { 
                    // we are over hours at this point so we are not compliant
                    // actual logic for that will be handled later
                    sn.Hours_Oil_Sample__c = shHours;
                    sn.Compliant_Lubricant__c = false;
                }
                else if(OilSampleHourDifference > oilSampleHrs - (oilSampleHrs * hourOverageThreshold)) {
                    // we are above the miminum threshold but below the maximum
                    // treat this as a 1000 hour mark service
                    sn.Hours_Oil_Sample__c += (oilSampleHrs * oilSampleMultiplier);
                }
                else {
                    // we are below the service schedule, so create a new schedule
                    sn.Hours_Oil_Sample__c = shHours;
                    sn.Oil_Sample_Next_Expected_Date__c = date.today().addMonths(6);
                    warrantyCheck = true;
                }
                
                if(!hoursCompliance){
                    sn.Compliant_Lubricant__c = false;
                }
                
                if (!warrantyCheck) {
                    if (dateDiff > 0 && dateDiff < 45) {
                        // early; use expected due date + 6
                        sn.oil_sample_next_expected_date__c = expDate.addMonths(6);
                    } else if(dateDiff < 0 && dateDiff > -119) {
                        // bit late; use expected due date
                        sn.oil_sample_next_expected_date__c = expDate.addMonths(6);
                    } else if (dateDiff > 45) { 
                        // too early; calculate from that day + 6 mo
                        sn.oil_sample_next_expected_date__c = date.today().addMonths(6);
                    } else {
                        // too late, out of compliance
                        sn.Compliant_Lubricant__c = false;
                    } 
                }
                
                if (sn.oil_sample_next_expected_date__c == null) sn.oil_sample_next_expected_date__c = date.today().addMonths(6);
            }
        
            
            // Name will now be report number, null if none attached
            if (newRec.Report_Number__c != null) {
                newRec.Name = newRec.Report_Number__c;    
            } else {
                newRec.Name = 'Report Number Required';
            }
               
            System.debug('New record data: ' + newRec + ' ' + sn + ' ' + sh);
            upsert newRec;
            String record = 'Message successfully received, record added. Record Id: ' + newRec.Id;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(record));
        } catch (Exception e) {
            if (!test.isRunningTest()) {
                System.debug('Error: ' + e.getMessage());
                gen.writeStartObject();
                gen.writeNumberField('statusCode', 404);
                gen.writeStringField('error', e.getMessage());
                gen.writeEndObject();
                RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
            }
        }
    }
}