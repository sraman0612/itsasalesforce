/// enosiX Inc. Generated Apex Model
/// Generated On: 11/3/2020 5:47:45 PM
/// SAP Host: From REST Service On: https://gdi--enosixdev2.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev2.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class ensxtx_SBO_EnosixMaterialBOM_Search extends ensxsdk.EnosixFramework.SearchSBO
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('ensxtx_SBO_EnosixMaterialBOM_Search_Meta', new Type[] {
            ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SC.class
            , ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SR.class
            , ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT.class
            , ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS.class
            , ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT.class
            } 
        );
    }

    public ensxtx_SBO_EnosixMaterialBOM_Search()
    {
        super('EnosixMaterialBOM', ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SC.class, ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SR.class);
    }
    
    public override Type getType() { return ensxtx_SBO_EnosixMaterialBOM_Search.class; }

    public EnosixMaterialBOM_SC search(EnosixMaterialBOM_SC sc) 
    {
        return (EnosixMaterialBOM_SC)super.executeSearch(sc);
    }

    public EnosixMaterialBOM_SC initialize(EnosixMaterialBOM_SC sc) 
    {
        return (EnosixMaterialBOM_SC)super.executeInitialize(sc);
    }

    public class EnosixMaterialBOM_SC extends ensxsdk.EnosixFramework.SearchContext 
    { 		
        public EnosixMaterialBOM_SC() 
        {		
            super(new Map<string,type>		
                {		
                    'SEARCHPARAMS' => ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS.class
                });		
        }

        public override Type getType() { return ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SC.class; }

        public override void registerReflectionForClass()
        {
            ensxtx_SBO_EnosixMaterialBOM_Search.registerReflectionInfo();
        }

        public EnosixMaterialBOM_SR result { get { return (EnosixMaterialBOM_SR)baseResult; } }


        @AuraEnabled public ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS SEARCHPARAMS
        {
            get
            {
                return (ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS)this.getStruct(ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS.class);
            }
        }
        
        }

    public class EnosixMaterialBOM_SR extends ensxsdk.EnosixFramework.SearchResult 
    {
        public EnosixMaterialBOM_SR() 
        {
            super(new Map<string,type>{'SEARCHRESULT' => ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT.class } );
        }
        
        public ensxsdk.EnosixFramework.FrameworkCollection SearchResults
        {
            get { return super.getCollection(ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT.class); }
        }
        
        public List<SEARCHRESULT> getResults() 
        {
            List<SEARCHRESULT> results = new List<SEARCHRESULT>();
            SearchResults.copyTo(results);
            return results;
        }

        public override Type getType() { return ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SR.class; }

        public override void registerReflectionForClass()
        {
            ensxtx_SBO_EnosixMaterialBOM_Search.registerReflectionInfo();
        }
    }

    public class SEARCHPARAMS extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS.class; }

        public override void registerReflectionForClass()
        {
            ensxtx_SBO_EnosixMaterialBOM_Search.registerReflectionInfo();
        }
        @AuraEnabled public String Material
        { 
            get { return this.getString ('MATNR'); } 
            set { this.Set (value, 'MATNR'); }
        }

        @AuraEnabled public String Plant
        { 
            get { return this.getString ('DWERK'); } 
            set { this.Set (value, 'DWERK'); }
        }

        @AuraEnabled public Decimal QuantityForKit
        { 
            get { return this.getDecimal ('EXPLQTY'); } 
            set { this.Set (value, 'EXPLQTY'); }
        }

        @AuraEnabled public String SalesOrganization
        { 
            get { return this.getString ('VKORG'); } 
            set { this.Set (value, 'VKORG'); }
        }

        @AuraEnabled public String DistributionChannel
        { 
            get { return this.getString ('VTWEG'); } 
            set { this.Set (value, 'VTWEG'); }
        }

    }

    public class SEARCHRESULT extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT.class; }

        public override void registerReflectionForClass()
        {
            ensxtx_SBO_EnosixMaterialBOM_Search.registerReflectionInfo();
        }
        @AuraEnabled public Boolean SoldSeparately
        { 
            get { return this.getBoolean('SEL'); } 
            set { this.setBoolean(value, 'SEL'); }
        }

        @AuraEnabled public String BOMItemNumber
        { 
            get { return this.getString ('POSNR'); } 
            set { this.Set (value, 'POSNR'); }
        }

        @AuraEnabled public String BOMComponent
        { 
            get { return this.getString ('IDNRK'); } 
            set { this.Set (value, 'IDNRK'); }
        }

        @AuraEnabled public String ItemDescription
        { 
            get { return this.getString ('OJTXP'); } 
            set { this.Set (value, 'OJTXP'); }
        }

        @AuraEnabled public Decimal ComponentQuantity
        { 
            get { return this.getDecimal ('MNGLG'); } 
            set { this.Set (value, 'MNGLG'); }
        }

        @AuraEnabled public String ComponentUnitOfMeasure
        { 
            get { return this.getString ('MEINS'); } 
            set { this.Set (value, 'MEINS'); }
        }

    }

    public class SEARCHRESULT_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public SEARCHRESULT_COLLECTION()
        {
            super('SEARCHRESULT', ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT.class, null);
        }

        public List<ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT> getAsList()
        {
            return (List<ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT>)this.buildList(List<ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT>.class);
        }
    }


}