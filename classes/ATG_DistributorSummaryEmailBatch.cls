global class ATG_DistributorSummaryEmailBatch implements Database.Batchable<sObject> {  
    private final List<String> customerServicedStatuses = new List<String>{'Customer Servicing - with OEM Parts', 'Customer Servicing - without OEM Parts'};
        
    global Database.QueryLocator start(Database.BatchableContext bc){
        List<Account> accounts = [SELECT Service_Notification_Contact__c FROM Account WHERE Inactive__c = false AND Service_Notification_Contact__c != null];
        List<Id> contactIds = new List<Id>();
        for(Account acct : accounts) {
            contactIds.add(acct.Service_Notification_Contact__c);
        }
        String query = 'SELECT Id, Name, Email FROM Contact WHERE Inactive__c = false AND RecordType.DeveloperName = \'Customer\' AND Id IN :contactIds';
        return Database.getQueryLocator(query);
    } 
    
    global void execute(Database.BatchableContext bc, List<Contact> scope){ 
        System.debug('Scope: '+scope);
        // Fetch email data
        EmailTemplate template = [SELECT Id, HtmlValue, Markup, Body, Subject FROM EmailTemplate WHERE DeveloperName = 'ATG_DistributorSummaryEmailTemplate' LIMIT 1];
        System.debug('template: '+template);
        Date today = System.today();
        String replyToEmail = System.Label.ATG_ReplyToEmailAddress;
        String senderDisplayName = System.Label.ATG_EmailSenderDisplayName;
        
        // Query for active Accounts, child Assets, child Lubricant Samples. Build map structures.
		List<Account> accounts = [SELECT Id, Service_Notification_Contact__c, Service_Notification_Contact__r.Name, Service_Notification_Contact__r.Email
                                  FROM Account 
                                  WHERE Service_Notification_Contact__c IN :scope 
                                  	AND Inactive__c = false];
        System.debug('Accounts: '+accounts);
        Map<Id, List<Account>> accountsByNotificationContactId = new Map<Id, List<Account>>();
        for(Account acct : accounts) {
            if(!accountsByNotificationContactId.containsKey(acct.Service_Notification_Contact__c)) {
                accountsByNotificationContactId.put(acct.Service_Notification_Contact__c, new List<Account>());
            }
            List<Account> mappedAccounts = accountsByNotificationContactId.get(acct.Service_Notification_Contact__c);
            mappedAccounts.add(acct);
            accountsByNotificationContactId.put(acct.Service_Notification_Contact__c, mappedAccounts);
        }
        
        List<Asset> assets = [SELECT Id, Expected_Next_Service_Date__c, Current_Servicer__c, Machine_Status__c,
                             	(SELECT Id FROM Oil_Samples__r
                                 WHERE Status__c = 'Action Required'
                                 AND Distributor_Acknowledgement_Received__c = false)
                              FROM Asset 
                              WHERE Current_Servicer__c IN :accounts
                              AND Inactive__c = false];
        Map<Id, List<Asset>> assetsByAccountId = new Map<Id, List<Asset>>();
        for(Asset a : assets) {
            if(!assetsByAccountId.containsKey(a.Current_Servicer__c)) {
                assetsByAccountId.put(a.Current_Servicer__c, new List<Asset>());
            }
            List<Asset> mappedAssets = assetsByAccountId.get(a.Current_Servicer__c);
            mappedAssets.add(a);
            assetsByAccountId.put(a.Current_Servicer__c, mappedAssets);
        }
       
        // Build dynamic emails
        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
        for(Contact con : scope) {
            Integer countMachinesNextWeek_Distributor = 0;
            Integer countMachinesNextMonth_Distributor = 0;
            Integer countMachinesPastDue_Distributor = 0;
            Integer countMachinesNextWeek_Customer = 0;
            Integer countMachinesNextMonth_Customer = 0;
            Integer countMachinesPastDue_Customer = 0;
            Integer countLubricantSamplesRequireAttention = 0;
                                    
            for(Account acct : accountsByNotificationContactId.get(con.Id)) {
                for(Asset asset : assetsByAccountId.get(acct.Id)) {
                    if(this.customerServicedStatuses.contains(asset.Machine_Status__c)) {
                        if(asset.Expected_Next_Service_Date__c >= today && asset.Expected_Next_Service_Date__c <= today.addDays(7)) {
                            countMachinesNextWeek_Customer++;
                        }
                        if(asset.Expected_Next_Service_Date__c >= today && asset.Expected_Next_Service_Date__c <= today.addMonths(1)) {
                            countMachinesNextMonth_Customer++;
                        }
                        if(asset.Expected_Next_Service_Date__c < today) {
                            countMachinesPastDue_Customer++;
                        }
                    }
                    else {
                        if(asset.Expected_Next_Service_Date__c >= today && asset.Expected_Next_Service_Date__c <= today.addDays(7)) {
                            countMachinesNextWeek_Distributor++;
                        }
                        if(asset.Expected_Next_Service_Date__c >= today && asset.Expected_Next_Service_Date__c <= today.addMonths(1)) {
                            countMachinesNextMonth_Distributor++;
                        }
                        if(asset.Expected_Next_Service_Date__c < today) {
                            countMachinesPastDue_Distributor++;
                        }
                    }
                    if(asset.Oil_Samples__r != null && asset.Oil_Samples__r.size() > 0) {
                        countLubricantSamplesRequireAttention += asset.Oil_Samples__r.size();
                    }
                }
            }
            String body = template.body;
            body = body.replaceAll('<RecipientName>', con.Name);
            body = body.replaceAll('<MachinesNextWeek_Distributor>', String.valueOf(countMachinesNextWeek_Distributor));
            body = body.replaceAll('<MachinesNextMonth_Distributor>', String.valueOf(countMachinesNextMonth_Distributor));
            body = body.replaceAll('<MachinesPastDue_Distributor>', String.valueOf(countMachinesPastDue_Distributor));
            body = body.replaceAll('<MachinesNextWeek_Customer>', String.valueOf(countMachinesNextWeek_Customer));
            body = body.replaceAll('<MachinesNextMonth_Customer>', String.valueOf(countMachinesNextMonth_Customer));
            body = body.replaceAll('<MachinesPastDue_Customer>', String.valueOf(countMachinesPastDue_Customer));
            body = body.replaceAll('<LubricantSamples>', String.valueOf(countLubricantSamplesRequireAttention));
            System.debug('%%% Body:' +body);
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(con.Id);
            mail.setReplyTo(replyToEmail);
            mail.setSubject(template.Subject);
            mail.setSenderDisplayName(senderDisplayName);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setPlainTextBody(body);
            emailsToSend.add(mail);
        }
		if(emailsToSend != null && !emailsToSend.isEmpty()) {
        	Messaging.sendEmail(emailsToSend, false);
        }    
    }
    
    global void finish(Database.BatchableContext BC) {}
}