@isTest
public class SFS_ShipmentItemTriggerHandlerTest {
    @isTest
    public static void rollUpFreightChargeTest(){
        
         List<Division__C> divList=SFS_TestDataFactory.createDivisions(1, false);
         insert divList[0];
         List<Schema.Location> locationList=SFS_TestDataFactory.createLocations(1,divList[0].Id, false);
        insert locationList[0];
        List<Account> acc=SFS_TestDataFactory.createAccounts(1, false);
        acc[0].IRIT_Customer_Number__c = 'test123';
        insert acc[0];
         
        List<WorkOrder> wO=SFS_TestDataFactory.createWorkOrder(1,acc[0].Id,locationList[0].Id,divList[0].Id ,null,false);
        insert wO[0];
        List<WorkType> wT=SFS_TestDataFactory.createWorkType(1,false);
        insert wT[0];
        List<WorkOrderLineItem> wOLI=SFS_TestDataFactory.createWorkOrderLineItem(1,wO[0].Id,wT[0].Id,false);
        insert wOLI[0];
        List<ProductRequest> pR =SFS_TestDataFactory.createProductRequest(1,wO[0].Id,wOLI[0].Id,false);
        insert pR[0];
        List<Product2> prod=SFS_TestDataFactory.createProduct(1,false);
        insert prod[0];
        List<ProductRequestLineItem> pRLI=SFS_TestDataFactory.createPRLI(1,pR[0].Id,prod[0].Id,false);
        pRLI[0].SFS_Freight_Charge__c =0;
        Insert pRLI[0];
        List<Shipment> shipmentList =SFS_TestDataFactory.createShipment(1,pR[0].Id,true);
        List<ShipmentItem> shipmentItemList=SFS_TestDataFactory.createShipmentItem(1,shipmentList[0].Id,pR[0].Id,pRLI[0].Id,true);
        SFS_ShipmentItemTriggerHandler.rollUpFreightCharge(shipmentItemList);
        
    }

}