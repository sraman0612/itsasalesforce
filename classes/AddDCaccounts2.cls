public class AddDCaccounts2 implements Queueable, Database.AllowsCallouts {

    private Lead currLead;
    private String salesOrg;
    private String dcVal;
    private String country;
    private String region;
    private String county;

    public AddDCaccounts2(Lead CurrLead, String SalesOrg, String DCval, String Country, String Region, String County) {
        this.currLead = CurrLead;
        this.salesOrg = SalesOrg;
        this.dcVal = DCval;
        this.country = Country;
        this.region = Region;
        this.county = County;
    }
    public void execute(QueueableContext context) {

        SBO_EnosixZAPR_Search.EnosixZAPR_SC searchContext = new SBO_EnosixZAPR_Search.EnosixZAPR_SC();
        SBO_EnosixZAPR_Search.EnosixZAPR_SR result = new SBO_EnosixZAPR_Search.EnosixZAPR_SR();

        searchContext.SEARCHPARAMS.SalesOrganization = salesOrg;
        searchContext.SEARCHPARAMS.DistributionChannel = dcVal;
        searchContext.SEARCHPARAMS.Country = country;
        searchContext.SEARCHPARAMS.Region = region;
        searchContext.SEARCHPARAMS.County = county;

        SBO_EnosixZAPR_Search sbo = new SBO_EnosixZAPR_Search();

        try {
            searchContext = sbo.search(searchContext);
        } catch (Exception e) {
            System.debug('We have an SBO_EnosixZAPR_Search callout error: ' + e.getMessage());
        }

        result = searchContext.result;
        List<String> customerNumbers = new List<String>();

        for (SBO_EnosixZAPR_Search.SEARCHRESULT vals : result.getResults()) {
            customerNumbers.add(vals.CustomerNumber);
        }

        system.debug('customerNumbers from AddDCaccounts = '+customerNumbers);
        Set<String> uniqueCustNumbersSet = new Set<String>(customerNumbers);
        List<String> uniqueCustNumbersList = new List<String>();
        uniqueCustNumbersList.addAll(uniqueCustNumbersSet);
        system.debug('uniqueCustNumbersList ===> '+uniqueCustNumbersList);
        if (uniqueCustNumbersList != null && !uniqueCustNumbersList.isEmpty()) currLead.FLD_Customer_Numbers__c = currLead.FLD_Customer_Numbers__c +','+ String.join(uniqueCustNumbersList, ',');

        update currLead;

        if (!Test.isRunningTest()) System.enqueueJob(new AddDCaccounts3(currLead,salesOrg, 'GT', country, region, county));
    }
}