public class SFS_createReturnOrder {
    @InvocableMethod(label='Get Product Transfers' description='Create return Order And Return order line Item')
    Public Static void createReturnOrders(List<ID> ids){
        try{
            Map<Id,ServiceAppointment> svcAppointmentMap=new Map<Id,ServiceAppointment>();
            List<WorkOrderLineItem> wolineitemList = new List<WorkOrderLineItem>();
            List<ServiceAppointment> svcList=[Select Id,ContactID, AccountID, Work_Order__c,Work_Order__r.IsGeneratedFromMaintenancePlan,
                                              ParentRecordId,ownerId,CTS_Work_Order_Line_Item__c from ServiceAppointment 
                                              where Id IN: ids];
            Set<Id> woliIds=new Set<Id>();
            Set<Id> woIds=new Set<Id>();
            Set<Id> resourceIds=new Set<Id>();
            for(ServiceAppointment app : svcList){
                // Added as part of SIF-5035 - Start
                if(app.Work_Order__r.IsGeneratedFromMaintenancePlan){
                    woIds.add(app.Work_Order__c);
                }
                // Added as part of SIF-5035 - End
                svcAppointmentMap.put(app.id,app);
                woliIds.add(app.ParentRecordId);
                resourceIds.add(app.ownerId); 
            }
            // Added as part of SIF-5035 - Start
            if(!woIds.isEmpty()){
                wolineitemList = [SELECT Id,Description FROM WorkOrderLineItem WHERE WorkOrderId IN :woIds];
                for(WorkOrderLineItem woliRec : wolineitemList){
                    woliIds.add(woliRec.Id);
                }
            }
            // Added as part of SIF-5035 - End
            System.debug('woliIds' + woliIds);
            List<ServiceResource> resourcesList =[Select Id,RelatedRecordId,LocationId From ServiceResource 
                                             where RelatedRecordId IN : resourceIds];
            Map<Id,Id> serviceResourceMap=new Map<Id,Id>();
            for(ServiceResource owner:resourcesList){
                serviceResourceMap.put(owner.RelatedRecordId,owner.LocationId);
            }
            List<WorkOrderLineItem> woliList =[Select Id,workOrderId,(Select Id from ProductRequests),
                                               (Select Id,ProductItemId,QuantityConsumed from ProductsConsumed) From WorkOrderLineItem where ID IN : woliIds];
            Set<Id> productRequestIds=new Set<Id>();
            Map<Id,List<ProductConsumed>> productConsumedMap=new Map<Id,List<ProductConsumed>>();
            Map<Id,Id> woliMap=new Map<Id,Id>();
            Map<Id,Id> productRequestMap=new Map<Id,Id>();
            for(WorkOrderLineItem woli:woliList){
                woliMap.put(woli.Id,woli.WorkOrderId);
                productConsumedMap.put(woli.Id,woli.ProductsConsumed);
                for(ProductRequest pr:woli.ProductRequests){
                    productRequestMap.put(woli.Id,pr.Id);
                }
            }
            System.debug('productConsumedMap' + productConsumedMap);
            List<ProductTransfer> productTransferList=[Select Id,ProductTransferNumber,ProductRequestId,ProductRequest.WorkOrderLineItemId,
                                                       QuantitySent,QuantityReceived,SourceProductItemId,QuantityUnitOfMeasure,Product2Id,DestinationLocationId,
                                                       ProductRequestLineItemId from ProductTransfer where ProductRequest.WorkOrderLineItemId IN:woliIds];
            Map<Id,List<ProductTransfer>> productTransferMap=new Map<Id,List<ProductTransfer>>();
            Set<Id> productIds=new Set<Id>();
            Set<Id> locationIds=new Set<Id>();
            for(ProductTransfer transfer:productTransferList){
                if(!productTransferMap.containsKey(transfer.ProductRequest.WorkOrderLineItemId)){
                    list<ProductTransfer> ptList=new list<ProductTransfer>();
                    ptList.add(transfer);
                    productTransferMap.put(transfer.ProductRequest.WorkOrderLineItemId,ptList);                    
                }else{
                    list<ProductTransfer> ptList=productTransferMap.get(transfer.ProductRequest.WorkOrderLineItemId);
                    ptList.add(transfer);
                    productTransferMap.put(transfer.ProductRequest.WorkOrderLineItemId,ptList);
                }
                if(transfer.Product2Id!=null)
                    productIds.add(transfer.Product2Id);
                If(transfer.DestinationLocationId!=null)
                    locationIds.add(transfer.DestinationLocationId);
            }
            Map<Id, ProductItem> productItemMap = new Map<Id, ProductItem>([SELECT Id,Product2Id,LocationId FROM ProductItem 
                                                                                where Product2Id IN:productIds and LocationId IN:locationIds]);
            List<ReturnOrder> returnOrderList=new List<ReturnOrder>();
            List<ReturnOrderLineItem> lineitemList=new List<ReturnOrderLineItem>();            
            for(Id svcId:ids){
                
                ServiceAppointment appointment=svcAppointmentMap.get(svcId);
                ReturnOrder order=new ReturnOrder(status='Draft',
                                                  SFS_WorkOrder__c=woliMap.get(appointment.parentRecordId),
                                                  SourceLocationId=serviceResourceMap.get(appointment.OwnerId),
                                                  SFS_Service_Appointment__c=ids[0],
                                                 Return_Reason__c='Overage');
                returnOrderList.add(order);
                System.debug('productTransferMap.get(appointment.ParentRecordId) -->'+productTransferMap.get(appointment.ParentRecordId));
                for(ProductTransfer producttransfer:productTransferMap.get(appointment.ParentRecordId)){
                    for(Id key: productItemMap.keySet()){
                System.debug('productItemMap.get(key) -->'+productItemMap.get(key));

                        ProductItem item=productItemMap.get(key);
                        if(item.Product2Id==producttransfer.Product2Id && item.LocationId==producttransfer.DestinationLocationId){
                            Boolean createLineItem=true;
                            Integer quantity=Integer.valueOf(producttransfer.QuantitySent);
                            //Added the woliIds loop to check for all PC's under all WOLI's for PM generated WO
                            for(Id woliId : woliIds){
                                for(ProductConsumed pc:productConsumedMap.get(woliId)){
                                    if(item.Id==pc.ProductItemId){
                                        if(producttransfer.QuantitySent==pc.QuantityConsumed){
                                            createLineItem=false;                                        
                                        }else{
                                            quantity=Integer.valueOf(producttransfer.QuantitySent)-Integer.valueOf(pc.QuantityConsumed);
                                        }
                                    }
                                }
                            }
                            if(createLineItem==true){
                                ReturnOrderLineItem lineItem=new ReturnOrderLineItem(QuantityReturned=quantity,SFS_Product_Request_Id__c=producttransfer.ProductRequestId,DestinationLocationID=producttransfer.DestinationLocationId,
                                                                                     Product2Id=producttransfer.Product2Id,ReasonForReturn='Overage', QuantityUnitOfMeasure=producttransfer.QuantityUnitOfMeasure);
                                lineitemList.add(lineItem);
                            }
                        }
                    }
                }
            }
            List<Database.SaveResult> results=new List<Database.SaveResult>();
            if(returnOrderList.size()>0 && lineitemList.size()>0){
                results=Database.insert(returnOrderList);
            }
            Set<Id> orderIds=new Set<Id>();
            for(Database.SaveResult result:results){
                orderIds.add(result.id);
            }
            List<ReturnOrder> orderList=new List<ReturnOrder>();
            if(orderIds.size()>0){
                orderList=[Select Id,ProductRequestId from ReturnOrder where Id IN:orderIds];
            }
            for(ReturnOrderLineItem roli:lineitemList){
                for(ReturnOrder ro:orderList){
               
                        roli.ReturnOrderId=ro.Id;
                    
                }
            }
            System.debug('111---'+lineitemList);
            insert lineitemList;
        }catch(Exception e){}
    }
}