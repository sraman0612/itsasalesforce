public class ServiceHistoryJSON {

    public enum Typex {AIR,OIL,MAINT,CABINET,SEP,CONTROL,LUBE}
    
    public Integer TotalQuantity;
    public Integer UniqueParts;
    public String Type;
    
    public List<Tuple> parts;
    
    public class Tuple {
    
        public String key;
        public Integer installed;
        public Integer remaining;
        
        public Tuple(String k, Integer i, Integer v) {
            this.key = k;
            this.installed = i;
            this.remaining = v;
        }
        
        //serialization constructor
        public Tuple(){
		
        }
    
    }

}