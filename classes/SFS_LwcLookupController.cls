public with sharing class SFS_LwcLookupController {
    
        @AuraEnabled(cacheable=true)  
        public static List<sobject> findRecords(String searchKey, String objectName) {  
          string searchText = '\'' + String.escapeSingleQuotes(searchKey) + '%\'';  
          string query = 'SELECT Id, MasterLabel FROM ' +objectName+ ' WHERE MasterLabel LIKE '+searchText+' LIMIT 5';  
          return Database.query('SELECT Id, MasterLabel FROM ' +objectName+ ' WHERE MasterLabel LIKE '+searchText+' LIMIT 5');  
        }  
    
}