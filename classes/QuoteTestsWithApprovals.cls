@isTest 
private class QuoteTestsWithApprovals {
    
    public static Account newAccount() {
        Account testAccount = new Account(Name='Test Company Name123',Account_Number__c='1234');
        return testAccount;
    }
    
    public static Opportunity newOpportunity(ID accountId) {
        Opportunity testOpportunity = new Opportunity(Name='Test Opportunity123', Accountid = accountId,
            CloseDate = Date.parse('12/31/2020'), Stagename = 'Prospecting');
        return testOpportunity;
    }
    
    public static SBQQ__Quote__c newQuote(ID accountId, ID opportunityId) {
        SBQQ__Quote__c testQuote = new SBQQ__Quote__c(SBQQ__Type__c='Quote', SBQQ__Status__c='Draft',
            SBQQ__Opportunity2__c = opportunityId, SBQQ__Account__c = accountId);
        return testQuote;
    }
    
    static testMethod void QuoteExtControllerTest () {
        Account testAccount = newAccount();
        insert testAccount;
        
        Opportunity testOpportunity = newOpportunity(testAccount.Id);
        insert testOpportunity;
        
        SBQQ__Quote__c testQuote = newQuote(testAccount.Id, testOpportunity.Id);
        insert testQuote;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testQuote);
        QuoteExtController cntrllr = new QuoteExtController(sc);
        
        PageReference submitPg = cntrllr.onSubmit();
        PageReference recallPg = cntrllr.onRecall();
        PageReference previewPg = cntrllr.onPreview();
        PageReference returnToQuotePg = cntrllr.returnToQuote();
    }
    
    static testMethod void QuoteStatusTestRejected () {
        Account testAccount = newAccount();
        insert testAccount;
        
        Opportunity testOpportunity = newOpportunity(testAccount.Id);
        insert testOpportunity;
        
        SBQQ__Quote__c testQuote = newQuote(testAccount.Id, testOpportunity.Id);
        insert testQuote;
        
        Test.startTest();
        
        testQuote.ApprovalStatus__c = 'Pending';
        update testQuote;      
        
        testQuote.ApprovalStatus__c = 'Rejected';
        update testQuote; 
        
        Test.stopTest();
    }
    
    static testMethod void QuoteStatusTestApproved () {
        Account testAccount = newAccount();
        insert testAccount;
        
        Opportunity testOpportunity = newOpportunity(testAccount.Id);
        insert testOpportunity;
        
        SBQQ__Quote__c testQuote = newQuote(testAccount.Id, testOpportunity.Id);
        insert testQuote;
        
        Test.startTest();
        
        testQuote.ApprovalStatus__c = 'Pending';
        update testQuote;      
        
        testQuote.ApprovalStatus__c = 'Approved';
        update testQuote; 
        
        Test.stopTest();
    }
    
    static testMethod void QuoteStatusTestValidated () {
        Account testAccount = newAccount();
        insert testAccount;
        
        Opportunity testOpportunity = newOpportunity(testAccount.Id);
        insert testOpportunity;
        
        SBQQ__Quote__c testQuote = newQuote(testAccount.Id, testOpportunity.Id);
        insert testQuote;
        
        Test.startTest();
        
        testQuote.SBQQ__Status__c = 'Quote Validated';
        update testQuote;      
        
        testQuote.SBQQ__Status__c = 'Draft';
        update testQuote; 
        
        Test.stopTest();
    }
    
    static testMethod void CustomApprovalsControllersTest () {
        Account testAccount = newAccount();
        insert testAccount;
        
        Opportunity testOpportunity = newOpportunity(testAccount.Id);
        insert testOpportunity;
        
        SBQQ__Quote__c testQuote = newQuote(testAccount.Id, testOpportunity.Id);
        insert testQuote;
        
        sbaa__Approver__c testApprover = new sbaa__Approver__c(name = 'test', sbaa__User__c = userinfo.getUserId());
        
        insert testApprover;
        
        sbaa__ApprovalRule__c testApprovalRule = new sbaa__ApprovalRule__c(Name = 'TEST Approval Rule', sbaa__TargetObject__c = 'SBQQ__Quote__c',
            sbaa__ApprovalStep__c = 1, sbaa__Approver__c = testApprover.id);
        
        insert testApprovalRule;
        
        sbaa__Approval__c testApproval = new sbaa__Approval__c(sbaa__status__c = 'Requested', sbaa__ApprovalStep__c = 1, sbaa__RecordField__c = 'Quote__c',
        Quote__c = testQuote.id, sbaa__Rule__c = testApprovalRule.id, sbaa__Approver__c = testApprover.id, sbaa__AssignedTo__c = userInfo.getUserId());
        
        insert testApproval;
        
        Test.startTest();
        
        QSApprovalsListViewController cont = new QSApprovalsListViewController();
        
        QSQuoteLinesForApprovalEmailController cont2 = new QSQuoteLinesForApprovalEmailController();
        cont2.setQuote(testQuote.id);
        cont2.getQuote();
        cont2.getQuoteLines();
        
        Test.stopTest();
    }
    
    static testMethod void QSActiveSubscriptionControllerTest () {
        Account testAccount = newAccount();
        insert testAccount;
        
        Contract testcontract = new Contract(AccountId = testAccount.Id);
        insert testcontract;
        
        Product2 testProduct = new Product2(Name = 'Test Product');
        insert testProduct;
        
        SBQQ__Subscription__c testSubscription = new SBQQ__Subscription__c(SBQQ__Account__c = testAccount.id, SBQQ__Quantity__c = 1, SBQQ__Product__c = testProduct.id, SBQQ__Contract__c = testcontract.id);
        insert testSubscription;
        
        Test.startTest();
        
        PageReference pr = Page.QSActiveSubscriptions;
        Test.setCurrentPage(pr);
        QSActiveSubscriptionController controller = new QSActiveSubscriptionController(new ApexPages.StandardController(testcontract));
        
        Test.stopTest();
    }

    static testMethod void QSApprovalsForQuoteControllerTest() {
        Account testAccount = newAccount();
        insert testAccount;
        
        Opportunity testOpportunity = newOpportunity(testAccount.Id);
        insert testOpportunity;
        
        SBQQ__Quote__c testQuote = newQuote(testAccount.Id, testOpportunity.Id);
        insert testQuote;
        
        sbaa__Approver__c testApprover = new sbaa__Approver__c(name = 'test', sbaa__User__c = userinfo.getUserId());
        
        insert testApprover;
        
        sbaa__ApprovalRule__c testApprovalRule = new sbaa__ApprovalRule__c(Name = 'TEST Approval Rule', sbaa__TargetObject__c = 'SBQQ__Quote__c',
            sbaa__ApprovalStep__c = 1, sbaa__Approver__c = testApprover.id);
        
        insert testApprovalRule;
        
        sbaa__Approval__c testApproval = new sbaa__Approval__c(sbaa__status__c = 'Requested', sbaa__ApprovalStep__c = 1, sbaa__RecordField__c = 'Quote__c',
        Quote__c = testQuote.id, sbaa__Rule__c = testApprovalRule.id, sbaa__Approver__c = testApprover.id, sbaa__AssignedTo__c = userInfo.getUserId());
        
        insert testApproval;
        
        Test.startTest();
        
        QSApprovalsForQuoteController cont = new QSApprovalsForQuoteController();
        cont.setQuote(testQuote.Id);
        cont.getQuote();
        cont.getApprovalsForQuote();
        cont.getApprovals();
        
        Test.stopTest();
    }

    static testMethod void CZ_QuoteSaveAlertExtTest() {
        Account testAccount = newAccount();
        insert testAccount;
        
        Opportunity testOpportunity = newOpportunity(testAccount.Id);
        insert testOpportunity;
        
        SBQQ__Quote__c testQuote = newQuote(testAccount.Id, testOpportunity.Id);
        insert testQuote;

        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        CZ_QuoteSaveAlertExt tester = new CZ_QuoteSaveAlertExt();

        PageReference pageRef = Page.CZ_QuoteSaveAlert;
        pageRef.getParameters().put('id', String.valueOf(testQuote.Id));
        Test.setCurrentPage(pageRef);

        tester.initPage();
        tester.getDoNotShow();
        tester.setDoNotShow(false);
        tester.cancel();
        tester.continueToQuote();

        Test.stopTest();
    }
}