public class C_WarrantyInfoController {
    @AuraEnabled
    public static Asset getAsset(string recordId) {
        system.debug(recordId);
        return [select Id, Name, Oil_Filter_Compliance__c, Oil_Filter_Next_Expected_Date__c,Hours_Oil_Filter__c,Compliance_Exception_Oil_Filter__c,Lubricant_Next_Expected_Date__c,
                 Lubricant_Compliance__c,Compliance_Exception_Lubricant__c,Hours_Separator__c,Separator_Compliance__c,Separator_Next_Expected_Date__c,Compliance_Exception_Separator__c,Control_Box_Filter_Compliance__c,
                Hours_Control_Box_Filter__c, Control_Box_Filter_Next_Expected_Date__c,Compliance_Exception_Control_Box__c, Air_Filter_Compliance__c, Air_Filter_Next_Expected_Date__c,Compliance_Exception_Air_Filter__c, 
                Hours_Air_Filter__c, Cabinet_Filter_Compliance__c,Cabinet_Filter_Next_Expected_Date__c,Hours_Cabinet_Filter__c,Compliance_Exception_Cabinet_Filter__c,Machine_Version_Parts_List__r.Air_Filter_1__r.Name,
                Machine_Version_Parts_List__r.Cabinet_Filter_1__r.Name,Machine_Version_Parts_List__r.Control_Box_Filter_1__r.Name,Machine_Version_Parts_List__r.Oil_Filter_1__r.Name,Machine_Version_Parts_List__r.Separator_1__r.Name,
                Machine_Version_Parts_List__r.Lubricant_Quantity__c, Machine_Version_Parts_List__r.Maintenance_Kit_1__c, Machine_Version_Parts_List__r.X10_YR_Warranty_Kit__c,
                Warranty_Type__c, Warranty_Start_Date__c, Warranty_End_Date__c, Machine_Version_Parts_List__r.Maintenance_Kit_1__r.Name, Machine_Version_Parts_List__r.X10_YR_Warranty_Kit__r.Name from Asset where id = :recordId Limit 1];
    }
}