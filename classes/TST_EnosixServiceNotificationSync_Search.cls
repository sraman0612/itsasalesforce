/// enosiX Inc. Generated Apex Model
/// Generated On: 1/15/2021 4:32:05 PM
/// SAP Host: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class TST_EnosixServiceNotificationSync_Search
{

    public class MockSBO_EnosixServiceNotificationSync_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotificationSync_Search.class, new MockSBO_EnosixServiceNotificationSync_Search());
        SBO_EnosixServiceNotificationSync_Search sbo = new SBO_EnosixServiceNotificationSync_Search();
        System.assertEquals(SBO_EnosixServiceNotificationSync_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC sc = new SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC();
        System.assertEquals(SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);
        System.assertNotEquals(null, sc.NOTIF_TYPE);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS childObj = new SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS();
        System.assertEquals(SBO_EnosixServiceNotificationSync_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.DateFrom = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateFrom);


    }

    @isTest
    static void testNOTIF_TYPE()
    {
        SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE childObj = new SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE();
        System.assertEquals(SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE_COLLECTION childObjCollection = new SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.NotificationType = 'X';
        System.assertEquals('X', childObj.NotificationType);


    }

    @isTest
    static void testEnosixServiceNotificationSync_SR()
    {
        SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SR sr = new SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SR();

        sr.registerReflectionForClass();

        System.assertEquals(SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT childObj = new SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT();
        System.assertEquals(SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT_COLLECTION childObjCollection = new SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.NotificationNumber = 'X';
        System.assertEquals('X', childObj.NotificationNumber);

        childObj.NotificationType = 'X';
        System.assertEquals('X', childObj.NotificationType);

        childObj.NotificationTypeText = 'X';
        System.assertEquals('X', childObj.NotificationTypeText);

        childObj.ItemRecordNumber = 'X';
        System.assertEquals('X', childObj.ItemRecordNumber);

        childObj.DispositionDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DispositionDate);

        childObj.WarrantyCode = 'X';
        System.assertEquals('X', childObj.WarrantyCode);

        childObj.WarrantyCodeDescription = 'X';
        System.assertEquals('X', childObj.WarrantyCodeDescription);

        childObj.SalesOrderNumber = 'X';
        System.assertEquals('X', childObj.SalesOrderNumber);

        childObj.DateReturnRequested = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateReturnRequested);

        childObj.DateMaterialReceived = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateMaterialReceived);

        childObj.DateInspected = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateInspected);

        childObj.CreditMemo = 'X';
        System.assertEquals('X', childObj.CreditMemo);

        childObj.CreditMemoDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.CreditMemoDate);

        childObj.CreditMemoAmount = 1.5;
        System.assertEquals(1.5, childObj.CreditMemoAmount);


    }

}