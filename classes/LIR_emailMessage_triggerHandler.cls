public class LIR_emailMessage_triggerHandler {
    /**22-12-2021 - added emailTriggerMethod logic as part of delta components*/
/*    public static void emailTriggerMethod(List<EmailMessage> ems){
      
        //added by @Capgemini
         Email_Message_Setting__mdt settings=Email_Message_Setting__mdt.getInstance('Email_Message_Setting_For_Case');
         if (settings.CTS_OM_Email_Trigger_Enabled__c){
            isInsertBefore(ems);
         }
        //end
        
    }*/
    
    //Populating Case on Email Message based on the reference ID mathcing with Legacy reference Id on IR migrated Case
    
    public static void updateCaseBasedOnEmailReference(List<EmailMessage> newEmailList){
        system.debug('in lir handler');
        List<String> refCaseList = new List<String>();
        List<Id> dupliateCases = new List<Id>();         
       	Map<Id,Id> parentCaseIdMap = new Map<Id,Id>();
        Map<Id,String> emailRefMap = new Map<Id,String>();
        Set<Id> emailMsgCaseIds = new Set<Id>();
        for(EmailMessage email:newEmailList){
            //if email if from GD then skip this logic and run only for IR cases.
            if(getEmailMap().get(email.ToAddress) == 'LGD'){
                system.debug('This is GD email '+email.ToAddress);
                return;
            }
            system.debug('this is LIR Email');
            emailMsgCaseIds.add(email.ParentId);
            String txtbody = email.TextBody;  
            
            if(txtbody != null){
                String str2 = 'ref:_'+txtbody.substringAfter('ref:_');
                String str3 = str2.substringBefore(':ref')+':ref';
                system.debug('str3===='+str3);
                refCaseList.add(str3);
                emailRefMap.put(email.id, str3);
            }
        }
        Map<Id,Case> emailCaseMap = new Map<Id,Case>([select id,createddate,lastmodifieddate,status from Case where id in:emailMsgCaseIds]);
        if(refCaseList.size() > 0){
            List<Case> caseRef = [Select id,IR_Legacy_Reference_Id__c from Case where IR_Legacy_Reference_Id__c in: refCaseList];
           
            system.debug('caseRef=='+caseRef);
            //If a Case found as per the email refernce id then parent id is getting populating for that case
            for(EmailMessage email:newEmailList){                
                for(Case cs:caseRef){
                    if(emailRefMap.get(email.id).equals(cs.IR_Legacy_Reference_Id__c) && emailCaseMap.get(email.ParentId) != null && email.ParentId != cs.id){
                        if(emailCaseMap.get(email.ParentId).status == 'New'){
                        	dupliateCases.add(email.ParentId);
                            email.ParentId = cs.id;
                        }
                        
                    }
                }
              
            }
        }
        if(dupliateCases.size() > 0)
        	AssignedTodeleteQueue(dupliateCases);
        system.debug('dupliateCases==='+dupliateCases);
    }
    
    //This method is assinigng all the Cases to delete queue which are created by email-to-case based on the old email refernce in legacy IR Cases
    @future
    Public static void AssignedTodeleteQueue(List<Id> caseIds){
        List<Case> caseToDeleteQueue = [select id,ownerId from Case where id in :caseIds];
        List<Group> groupList = [select id from group where DeveloperName = 'Deleted_Cases'];
        for(Case cs:caseToDeleteQueue){
            cs.ownerid = groupList[0].id;
        }
        if(caseToDeleteQueue.size() >0)
            update caseToDeleteQueue;
        system.debug('Cases Update========'+caseToDeleteQueue);
    }
    
    public static void isInsertBefore(List<EmailMessage> ems){
    //added by @Capgemini 23 December'21
      Email_Message_Setting__mdt  settings = [SELECT DeveloperName,CTS_OM_Email_Case_Record_Type_IDs__c,CTS_OM_Email_Case_Record_Type_IDs_1__c,CTS_OM_Email_Case_Record_Type_IDs_2__c,	CTS_OM_Email_Case_Record_Type_IDs_3__c,	CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Email_Case_Record_Type_IDs_5__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c,CTS_OM_Email_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case'];

       /* 
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'CTS_NA_North_Central_Parts', 'In Progress');
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'CTS_NA_South_Parts', 'In Progress');
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'CTS_NA_South_Central_Parts', 'In Progress');
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'South_Area_Area_Specialists', 'In Progress');
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'North_Central_Area_Area_Specialists', 'In Progress');*/
        if (settings.CTS_OM_Email_Trigger_Enabled__c){
            System.debug('CTS email trigger enabled');
            CTS_OM_EmailMessage_TriggerHandler.beforeInsert(ems);
        }
    }
    
    public static Map<String, String> getEmailMap(){
        List<Case_Email_Field_Setting__mdt> emailList = [SELECT Org_Wide_Email_Address__c, Legacy_Org__c FROM Case_Email_Field_Setting__mdt];
        
        Map<String, String> emailMap = new Map<String, String>();
        for(Case_Email_Field_Setting__mdt efm : emailList){
            emailMap.put(efm.Org_Wide_Email_Address__c, efm.Legacy_Org__c);
        }
        return emailMap;
    }
        
}