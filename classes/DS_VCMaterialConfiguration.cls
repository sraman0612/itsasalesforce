public with sharing class DS_VCMaterialConfiguration
{
    @AuraEnabled
    public Boolean ConfigValid { get; set; }

    // This block of properties are required to populat the header of the VC object
    @AuraEnabled
    public String Material { get; set; }
    @AuraEnabled
    public String ConfigInstance { get; set;}
    @AuraEnabled
    public String SalesDocumentType { get; set;}
    @AuraEnabled
    public String SalesOrganization { get; set;}
    @AuraEnabled
    public String DistributionChannel { get; set;}
    @AuraEnabled
    public String Division { get; set;}
    @AuraEnabled
    public String SoldToParty { get; set;}
    @AuraEnabled
    public String Plant { get; set;}
    @AuraEnabled
    public String ShipToParty { get; set;}
    @AuraEnabled
    public String SalesDocumentCurrency { get; set;}
    @AuraEnabled
    public Decimal OrderQuantity { get; set;}
    @AuraEnabled
    public String ObjectKey { get; set;}
    @AuraEnabled
    public Date ConfigDate { get; set; }
    @AuraEnabled
    public Boolean CalculatePrice { get; set; }
    @AuraEnabled
    public Boolean ConfigurationIsValid { get; set; }

    @AuraEnabled
    public List<SBO_EnosixVC_Detail.CHARACTERISTICS> characteristics;

    // Key: Characteristic Name
    // Value: All allowed values for that characterisitc
    @AuraEnabled
    public Map<string, List<SBO_EnosixVC_Detail.ALLOWEDVALUES>> indexedAllowedValues{get; set;}

    // Key: Characteristic Name
    // Value: All selected values for that characteristic
    @AuraEnabled
    public Map<string, List<SBO_EnosixVC_Detail.SELECTEDVALUES>> indexedSelectedValues{get; set;}

    // Debating doing this server side during processing rather than client.
    // Currently migrated it client side, but it might make sense to do it right away.
    //@AuraEnabled
    //public string inputType {get; set;}

    /* Kingspan doesn't use pricing
    @AuraEnabled
    public ENSX_VCPricingConfiguration PricingConfiguration { get; set; }

    @AuraEnabled
    public Decimal Price { get; set; }
 
    @AuraEnabled 
    public Decimal Cost { get; set; }
    */

    public DS_VCMaterialConfiguration(SBO_EnosixVC_Detail.EnosixVC model)
    {
        try
        {
            System.debug('getConfigurationFromSBOModel');

            this.Material = model.Material;
            this.ConfigInstance = model.ConfigInstance;
            this.SalesDocumentType = model.SalesDocumentType;
            this.SalesOrganization = model.SalesOrganization;
            this.DistributionChannel = model.DistributionChannel;
            this.Division = model.Division;
            this.SoldToParty = model.SoldToParty;
            this.Plant = model.Plant;
            this.ShipToParty = model.ShipToParty;
            this.SalesDocumentCurrency = model.SalesDocumentCurrency;
            this.OrderQuantity = model.OrderQuantity;
            this.ObjectKey = model.ObjectKey;

            this.ConfigDate = model.ConfigDate;
            this.CalculatePrice = model.CalculatePrice;
            this.ConfigurationIsValid = model.ConfigurationIsValid;

            this.characteristics = model.CHARACTERISTICS.getAsList();
            prepIndexedCollections(model);

            getAllowedValuesFromSBOModel(model);
            getSelectedValuesFromSBOModel(model);
        } catch(Exception e)
        {
            System.debug('Exception in DS_VCMaterialConfiguration = ' + e);
        }
    }

    public SBO_EnosixVC_Detail.EnosixVC convertToSBO(List<DS_VCCharacteristicValues> selectedValues)
    {
        SBO_EnosixVC_Detail.EnosixVC sbo = new SBO_EnosixVC_Detail.EnosixVC();
        try
        {
            sbo.Material = this.Material;
            sbo.ConfigInstance = this.ConfigInstance;
            sbo.SalesDocumentType = this.SalesDocumentType;
            sbo.SalesOrganization = this.SalesOrganization;
            sbo.DistributionChannel = this.DistributionChannel;
            sbo.Division = this.Division;
            sbo.SoldToParty = this.SoldToParty;
            sbo.Plant = this.Plant;
            sbo.ShipToParty = this.ShipToParty;
            sbo.SalesDocumentCurrency = this.SalesDocumentCurrency;
            sbo.OrderQuantity = this.OrderQuantity;
            sbo.ObjectKey = this.ObjectKey;

            sbo.ConfigDate = this.ConfigDate;
            sbo.CalculatePrice = this.CalculatePrice;
            sbo.ConfigurationIsValid = this.ConfigurationIsValid;

            Integer svTot = selectedValues.size();
            for (Integer svCnt = 0 ; svCnt < svTot ; svCnt++) 
            {
                DS_VCCharacteristicValues selectedValue = selectedValues[svCnt];
                sbo.SELECTEDVALUES.add(selectedValue.getSBOASelectedValuesForModel());
            }
        }catch (exception e)
        {
            System.debug('error in convertToSBO is ' + e); 
        }
        return sbo;

    }

    // Pre-loads the indexed collections with empty lists of values for both allowed and selected options.
    @testVisible
    private void prepIndexedCollections(SBO_EnosixVC_Detail.EnosixVC vcMaterial)
    {
        try
        {
            indexedAllowedValues = new Map<string, List<SBO_EnosixVC_Detail.ALLOWEDVALUES>>();
            indexedSelectedValues = new Map<string, List<SBO_EnosixVC_Detail.SELECTEDVALUES>>();
            List<SBO_EnosixVC_Detail.CHARACTERISTICS> charList = vcMaterial.CHARACTERISTICS.getAsList();
            Integer charTot = charList.size();
            for (Integer charCnt = 0 ; charCnt < charTot ; charCnt++)
            {
                SBO_EnosixVC_Detail.CHARACTERISTICS characteristic = charList[charCnt];
                indexedAllowedValues.put(characteristic.CharacteristicName, new List<SBO_EnosixVC_Detail.ALLOWEDVALUES>());
                indexedSelectedValues.put(characteristic.CharacteristicName, new List<SBO_EnosixVC_Detail.SELECTEDVALUES>());
            }
        }catch (Exception e)
        {
            System.debug('Error in prepIndexedCollections is' + e);
        }

    }
 
    @testVisible
    private void getAllowedValuesFromSBOModel(SBO_EnosixVC_Detail.EnosixVC vcMaterial)
    {
        try
        {
            List<SBO_EnosixVC_Detail.ALLOWEDVALUES> avList = vcMaterial.ALLOWEDVALUES.getAsList();
            Integer avTot = avList.size();
            for (Integer avCnt = 0 ; avCnt < avTot ; avCnt++)
            {
                SBO_EnosixVC_Detail.ALLOWEDVALUES allowed = avList[avCnt];
                indexedAllowedValues.get(allowed.CharacteristicName).add(allowed);
            }
        } catch (Exception e)
        {
            System.debug('Exception in getAllowedValuesFromSBOModel=' + e);
        }
    }

    @testVisible
    private void getSelectedValuesFromSBOModel(SBO_EnosixVC_Detail.EnosixVC vcMaterial)
    {
        try
        {
            /// indexedSelectedValues is populated from a list of Characteristics 
            /// that have been passed back from SAP; 
            /// this method is intended to match SELECTEDVALUE with their 
            /// corresponding CHARACTERISTIC
            List<SBO_EnosixVC_Detail.SELECTEDVALUES> svList = vcMaterial.SELECTEDVALUES.getAsList();
            Integer svTot = svList.size();
            for (Integer svCnt = 0 ; svCnt < svTot ; svCnt++)
            {
                SBO_EnosixVC_Detail.SELECTEDVALUES selected = svList[svCnt];
                /// this is added to check to see if a given characteristic has had a selected value returned for it
                if(indexedSelectedValues.get(selected.CharacteristicName) != null)
                {
                    indexedSelectedValues.get(selected.CharacteristicName).add(selected);
                }
            }
        } catch (Exception e)
        {
            System.debug('Exception in getSelectedValuesFromSBOModel = ' + e);
        }
    }
}