/// enosiX Inc. Generated Apex Model
/// Generated On: 9/15/2020 12:31:34 PM
/// SAP Host: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
public with sharing class TST_Z_ENSX_GET_TAX_JURISDICTION
{
    public class MockRFC_Z_ENSX_GET_TAX_JURISDICTION implements ensxsdk.EnosixFramework.RFCMock
    {
        public ensxsdk.EnosixFramework.FunctionObject executeFunction() 
        {
            return null;
        }
    }

    @isTest
    static void testExecute()
    {
        ensxsdk.EnosixFramework.setMock(RFC_Z_ENSX_GET_TAX_JURISDICTION.class, new MockRFC_Z_ENSX_GET_TAX_JURISDICTION());
        RFC_Z_ENSX_GET_TAX_JURISDICTION rfc = new RFC_Z_ENSX_GET_TAX_JURISDICTION();
        RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT params = rfc.PARAMS;
        System.assertEquals(null, rfc.execute());
    }

    @isTest
    static void testRESULT()
    {
        RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT funcObj = new RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT();

        funcObj.registerReflectionForClass();

        System.assertEquals(RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT.class, funcObj.getType(), 'getType() does not match object type.');

        funcObj.IV_CITY = 'X';
        System.assertEquals('X', funcObj.IV_CITY);

        funcObj.IV_COUNTRY = 'X';
        System.assertEquals('X', funcObj.IV_COUNTRY);

        funcObj.IV_COUNTY = 'X';
        System.assertEquals('X', funcObj.IV_COUNTY);

        funcObj.IV_REGION = 'X';
        System.assertEquals('X', funcObj.IV_REGION);

        funcObj.IV_ZIPCODE = 'X';
        System.assertEquals('X', funcObj.IV_ZIPCODE);

        //Check all the collections
        funcObj.getCollection(RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS.class).add(new RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS());
        System.assertEquals(1,funcObj.ET_LOCATION_RESULTS_List.size());

    }

    @isTest
    static void testET_LOCATION_RESULTS()
    {
        RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS funcObj = new RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS();

        funcObj.registerReflectionForClass();

        System.assertEquals(RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS.class, funcObj.getType(), 'getType() does not match object type.');
        funcObj.Country = 'X';
        System.assertEquals('X', funcObj.Country);

        funcObj.STATE = 'X';
        System.assertEquals('X', funcObj.STATE);

        funcObj.COUNTY = 'X';
        System.assertEquals('X', funcObj.COUNTY);

        funcObj.CITY = 'X';
        System.assertEquals('X', funcObj.CITY);

        funcObj.ZIPCODE = 'X';
        System.assertEquals('X', funcObj.ZIPCODE);

        funcObj.TXJCD_L1 = 'X';
        System.assertEquals('X', funcObj.TXJCD_L1);

        funcObj.TXJCD_L2 = 'X';
        System.assertEquals('X', funcObj.TXJCD_L2);

        funcObj.TXJCD_L3 = 'X';
        System.assertEquals('X', funcObj.TXJCD_L3);

        funcObj.TXJCD_L4 = 'X';
        System.assertEquals('X', funcObj.TXJCD_L4);

        funcObj.TXJCD = 'X';
        System.assertEquals('X', funcObj.TXJCD);

        funcObj.OUTOF_CITY = 'X';
        System.assertEquals('X', funcObj.OUTOF_CITY);

    }
}