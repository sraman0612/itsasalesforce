// Weekend batch
global class C_SerialNumAccUpdateBatchWSch implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Id batchId = Database.executeBatch(new C_SerialNumAccUpdateBatch('W'));
    }
}