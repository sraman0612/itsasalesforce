@IsTest
public class SFS_Outbound_Integration_Callout_Test {
	@isTest
    public static void OutboundCalloutTest(){
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, true);
        List<Account> Acc = SFS_TestDataFactory.createAccounts(1, true);
        List<Schema.Location> loc = SFS_TestDataFactory.CreateLocations(1,div[0].id, true);
        List<WorkOrder> woList = SFS_TestDataFactory.createWorkOrder(1, acc[0].id, loc[0].id, div[0].id, null, true);
        String xmlString = '<?xml version="1.0"?>-<IR_EBS_CUSTOMERSYNC>-<CUSTOMER><CHANGE_ID>219606156</CHANGE_ID><EVENT_CRUD_TYPE>U</EVENT_CRUD_TYPE><EVENT_CRUD_DATE>2015-05-19T13:42:13</EVENT_CRUD_DATE><ENTIRE_PARTY>N</ENTIRE_PARTY><MIN_CHANGE_ID>219606154</MIN_CHANGE_ID><MAX_CHANGE_ID>219606156</MAX_CHANGE_ID><DATABASE_NAME>IREBSPSH</DATABASE_NAME><SOURCE_SYSTEM>EBS</SOURCE_SYSTEM><SOURCE_SYSTEM_NAME>Oracle Enterprise Business Solutions</SOURCE_SYSTEM_NAME><PARTY_ID>3125552</PARTY_ID><REGISTRY_ID>1032653</REGISTRY_ID><ORGANIZATION_NAME>MABION S.A.</ORGANIZATION_NAME><TAX_REGISTRATION_NUMBER>PL7752561383</TAX_REGISTRATION_NUMBER><ORG_ID>94</ORG_ID><OPERATING_UNIT>IE OU EUR IRI</OPERATING_UNIT><OPERATING_UNIT_CODE>IEOUEUR</OPERATING_UNIT_CODE><PARTNER_OWNER>Industrial Technologies - ATFM</PARTNER_OWNER><SEND_TO_TAVANT>Y</SEND_TO_TAVANT>-<CUSTOMER_ACCOUNT>-<ACCOUNT><CUST_ACCOUNT_ID>2251110</CUST_ACCOUNT_ID><ACCOUNT_NUMBER>185367</ACCOUNT_NUMBER><ACCOUNT_TYPE>External</ACCOUNT_TYPE><SALES_CHANNEL>UNASSIGNED</SALES_CHANNEL><ACCOUNT_STATUS>Inactive</ACCOUNT_STATUS>-<PROFILE><CUST_ACCOUNT_PROFILE_ID>2272118</CUST_ACCOUNT_PROFILE_ID><PROFILE_CLASS>AP Trade</PROFILE_CLASS><COLLECTOR>Default Collector</COLLECTOR><CREDIT_CLASSIFICATION>HIGH</CREDIT_CLASSIFICATION><CREDIT_CHECK>Y</CREDIT_CHECK><CREDIT_HOLD>N</CREDIT_HOLD><REVIEW_CYCLE>QUARTERLY</REVIEW_CYCLE><SEND_STATEMENTS>Y</SEND_STATEMENTS><SEND_DUNNING_LETTERS>Y</SEND_DUNNING_LETTERS><PAYMENT_TERMS>NET 30</PAYMENT_TERMS></PROFILE>-<PROFILE_AMT><CUST_ACCT_PROFILE_AMT_ID>2347561</CUST_ACCT_PROFILE_AMT_ID><CREDIT_LIMIT>.01</CREDIT_LIMIT><ORDER_CREDIT_LIMIT>.01</ORDER_CREDIT_LIMIT><CURRENCY>USD</CURRENCY></PROFILE_AMT></ACCOUNT>-<ACCOUNT><CUST_ACCOUNT_ID>2250113</CUST_ACCOUNT_ID><ACCOUNT_NUMBER>185347</ACCOUNT_NUMBER><ACCOUNT_DESCRIPTION>MABION S.A.</ACCOUNT_DESCRIPTION><CLASSIFICATION>PRIVATE</CLASSIFICATION><ACCOUNT_TYPE>External</ACCOUNT_TYPE><SALES_CHANNEL>DIRECT</SALES_CHANNEL><ACCOUNT_STATUS>Active</ACCOUNT_STATUS><PRICE_LIST>IRI PRIMARY EUR</PRICE_LIST><PRICE_LIST_CURR_CODE>EUR</PRICE_LIST_CURR_CODE><FREE_ON_BOARD_POINT>DAP</FREE_ON_BOARD_POINT><FREIGHT_TERMS>Prepay & Add</FREIGHT_TERMS><DERIVED_PRICE_LIST>IRI PRIMARY EUR</DERIVED_PRICE_LIST>-<PROFILE><CUST_ACCOUNT_PROFILE_ID>2271118</CUST_ACCOUNT_PROFILE_ID><PROFILE_CLASS>EMEIA Trade</PROFILE_CLASS><COLLECTOR>EMEA-50</COLLECTOR><CREDIT_CLASSIFICATION>MODERATE</CREDIT_CLASSIFICATION><RISK_CODE>4</RISK_CODE><CREDIT_CHECK>Y</CREDIT_CHECK><CREDIT_HOLD>N</CREDIT_HOLD><REVIEW_CYCLE>YEARLY</REVIEW_CYCLE><SEND_STATEMENTS>Y</SEND_STATEMENTS><SEND_DUNNING_LETTERS>Y</SEND_DUNNING_LETTERS><PAYMENT_TERMS>CIA</PAYMENT_TERMS></PROFILE>-<PROFILE_AMT><CUST_ACCT_PROFILE_AMT_ID>2346561</CUST_ACCT_PROFILE_AMT_ID><CREDIT_LIMIT>1500</CREDIT_LIMIT><ORDER_CREDIT_LIMIT>1500</ORDER_CREDIT_LIMIT><CURRENCY>USD</CURRENCY></PROFILE_AMT>-<ADDRESSES>-<ADDRESS><PARTY_SITE_ID>2491073</PARTY_SITE_ID><SITE_NAME>Konstantyn?w Lodzki</SITE_NAME<SITE_NUMBER>448761</SITE_NUMBER><STATUS>Active</STATUS><CUST_ACCT_SITE_ID>350327</CUST_ACCT_SITE_ID><OPERATING_UNIT>IEOUEUR</OPERATING_UNIT><OPERATING_UNIT_CODE>IEOUEUR</OPERATING_UNIT_CODE><TERRITORY>American English. dd-MMM-yyyy</TERRITORY><TERRITORY_ID>1001</TERRITORY_ID><LOCATION_ID>500275</LOCATION_ID><COUNTRY>Poland</COUNTRY><ADDRESS_LINE1>ul. Langiewicza 60.</ADDRESS_LINE1><CITY>Konstantynow Lodzki</CITY><POSTAL_CODE>95-050</POSTAL_CODE><COUNTRY_CODE>PL</COUNTRY_CODE><SITE_OWNERSHIP>Industrial Technologies - ATFM</SITE_OWNERSHIP><DEFAULT_BRANCH>IRI ITS Poland</DEFAULT_BRANCH>-<SITE_USES>-<SITE_USE><SITE_USE_ID>455333</SITE_USE_ID><PURPOSE>Deliver To</PURPOSE><PRIMARY>Y</PRIMARY><OPERATING_UNIT>IEOUEUR</OPERATING_UNIT><FREE_ON_BOARD_POINT>DAP</FREE_ON_BOARD_POINT><FREIGHT_TERMS>Prepay & Add</FREIGHT_TERMS><STATUS>Active</STATUS><LOCATION>Konstantynow Lodzki</LOCATION></SITE_USE>-<SITE_USE><SITE_USE_ID>798023</SITE_USE_ID><BILL_TO_SITE_USE_ID>455330</BILL_TO_SITE_USE_ID><PURPOSE>Ship To</PURPOSE><PRIMARY>Y</PRIMARY><OPERATING_UNIT>IEOUEUR</OPERATING_UNIT><FREE_ON_BOARD_POINT>CIF</FREE_ON_BOARD_POINT><FREIGHT_TERMS>Prepaid</FREIGHT_TERMS><STATUS>Active</STATUS><LOCATION>Konstantynow Lodzki</LOCATION></SITE_USE></SITE_USES></ADDRESS></ADDRESSES></ACCOUNT></CUSTOMER_ACCOUNT></CUSTOMER></IR_EBS_CUSTOMERSYNC>';
        String JsonString  = '{"items":[{"itemIdentifier":"1","partNumber":"COMP26610","unitPrice":0.000000,"calculationInfo":[{"Standard":199,"Overtime":299,"Double":398,"RATE_TYPE":"HOURLY","ITEM_NUMBER":"SMALL ROTARY","STANDARD_HOURS":"0","LaborId":"","WOLIId":"1WLDT000000FjN84AK","RoundTripDistance":0,"RoundTripDuration":0,"RoundTripError":"INVALID_REQUEST"}],"_bomItemVariableName":""}]}';
        String interfaceDetail = 'XXPA2381';
        String interfaceLabel = 'Service Agreement with SALI';
        String interfaceLabel1 = 'Work Order with Service Agreement';
        
        String interfaceDetail2 = 'XXPA2589';
        String interfaceLabel2 = 'Activity Cost Labor Hour';
        
        String interfaceDetail3 = 'OracleCPQVersionQuote';
        
        String interfaceLabel4 = 'Labor Rate Interface';
        
        String interfaceDetail5 = 'XXPA2381';
        String interfaceLabel51 = 'Service Agreement with SALI';
        String interfaceLabel52 = 'Work Order with Service Agreement';
        
        String interfaceDetail6 = 'XXPA1951';
        //String interfaceDetail7 = 'XXPA1951_PC';
       // String interfaceDetail8 = 'XXPA1951_RO';
        
        String interfaceDetail9 = 'INT-155';
         String interfaceLabel91 = 'SARevenue';
         String interfaceLabel92 = 'WorkorderBilling';
         String interfaceLabel93 = 'WorkOrderRevenue';
        
        String interfaceDetail10 = 'IRMW0169';
        Test.startTest();
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail9, interfaceLabel);
        //SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail9, interfaceLabel92);
//SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail9, interfaceLabel93);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail10, interfaceLabel);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail6, interfaceLabel);
       // SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail7, interfaceLabel);
        //SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail8, interfaceLabel);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel1);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail2, interfaceLabel2);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail2, interfaceLabel);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail3, interfaceLabel);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel4);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail5, interfaceLabel51);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail5, interfaceLabel52);
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel);
        SFS_Outbound_Integration_Callout.HttpCalloutTest('WorkOrder', 'TEST', xmlString, interfaceDetail, interfaceLabel);
        Test.StopTest();
    
    }
}