/// enosiX Inc. Generated Apex Model
/// Generated On: 1/15/2020 10:59:35 AM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixSOSync_Search extends ensxsdk.EnosixFramework.SearchSBO
{
    static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixSOSync_Search_Meta', new Type[] {
            SBO_EnosixSOSync_Search.EnosixSOSync_SC.class
            , SBO_EnosixSOSync_Search.EnosixSOSync_SR.class
            , SBO_EnosixSOSync_Search.SEARCHRESULT.class
            , SBO_EnosixSOSync_Search.SEARCHPARAMS.class
            , SBO_EnosixSOSync_Search.DOC_TYPE.class
            , SBO_EnosixSOSync_Search.SEARCHRESULT.class
            }
        );
    }

    public SBO_EnosixSOSync_Search()
    {
        super('EnosixSOSync', SBO_EnosixSOSync_Search.EnosixSOSync_SC.class, SBO_EnosixSOSync_Search.EnosixSOSync_SR.class);
    }

    public override Type getType() { return SBO_EnosixSOSync_Search.class; }

    public EnosixSOSync_SC search(EnosixSOSync_SC sc)
    {
        return (EnosixSOSync_SC)super.executeSearch(sc);
    }

    public EnosixSOSync_SC initialize(EnosixSOSync_SC sc)
    {
        return (EnosixSOSync_SC)super.executeInitialize(sc);
    }

    public class EnosixSOSync_SC extends ensxsdk.EnosixFramework.SearchContext
    {
        public EnosixSOSync_SC()
        {
            super(new Map<string,type>
                {
                    'SEARCHPARAMS' => SBO_EnosixSOSync_Search.SEARCHPARAMS.class
                    ,'DOC_TYPE' => SBO_EnosixSOSync_Search.DOC_TYPE_COLLECTION.class
                });
        }

        public override Type getType() { return SBO_EnosixSOSync_Search.EnosixSOSync_SC.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixSOSync_Search.registerReflectionInfo();
        }

        public EnosixSOSync_SR result { get { return (EnosixSOSync_SR)baseResult; } }


        @AuraEnabled public SBO_EnosixSOSync_Search.SEARCHPARAMS SEARCHPARAMS
        {
            get
            {
                return (SBO_EnosixSOSync_Search.SEARCHPARAMS)this.getStruct(SBO_EnosixSOSync_Search.SEARCHPARAMS.class);
            }
        }

            @AuraEnabled public DOC_TYPE_COLLECTION DOC_TYPE
        {
            get
            {
                return (DOC_TYPE_COLLECTION)this.getCollection(SBO_EnosixSOSync_Search.DOC_TYPE_COLLECTION.class);
            }
        }

    }

    public class EnosixSOSync_SR extends ensxsdk.EnosixFramework.SearchResult
    {
        public EnosixSOSync_SR()
        {
            super(new Map<string,type>{'SEARCHRESULT' => SBO_EnosixSOSync_Search.SEARCHRESULT.class } );
        }

        public ensxsdk.EnosixFramework.FrameworkCollection SearchResults
        {
            get { return super.getCollection(SBO_EnosixSOSync_Search.SEARCHRESULT.class); }
        }

        public List<SEARCHRESULT> getResults()
        {
            List<SEARCHRESULT> results = new List<SEARCHRESULT>();
            SearchResults.copyTo(results);
            return results;
        }

        public override Type getType() { return SBO_EnosixSOSync_Search.EnosixSOSync_SR.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixSOSync_Search.registerReflectionInfo();
        }
    }

    public class SEARCHPARAMS extends ensxsdk.EnosixFramework.ValueObject
    {
        public override Type getType() { return SBO_EnosixSOSync_Search.SEARCHPARAMS.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixSOSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public Date DateFrom
        {
            get { return this.getDate ('DATE_FROM'); }
            set { this.Set (value, 'DATE_FROM'); }
        }

        @AuraEnabled public Boolean InitialLoad
        {
            get { return this.getBoolean('X_INITIALLOAD'); }
            set { this.setBoolean(value, 'X_INITIALLOAD'); }
        }

    }

    public class DOC_TYPE extends ensxsdk.EnosixFramework.ValueObject
    {
        public override Type getType() { return SBO_EnosixSOSync_Search.DOC_TYPE.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixSOSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public String SalesDocumentType
        {
            get { return this.getString ('AUART'); }
            set { this.Set (value, 'AUART'); }
        }

    }

    public class DOC_TYPE_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public DOC_TYPE_COLLECTION()
        {
            super('DOC_TYPE', SBO_EnosixSOSync_Search.DOC_TYPE.class, null);
        }

        public List<SBO_EnosixSOSync_Search.DOC_TYPE> getAsList()
        {
            return (List<SBO_EnosixSOSync_Search.DOC_TYPE>)this.buildList(List<SBO_EnosixSOSync_Search.DOC_TYPE>.class);
        }
    }

    public class SEARCHRESULT extends ensxsdk.EnosixFramework.ValueObject
    {
        public override Type getType() { return SBO_EnosixSOSync_Search.SEARCHRESULT.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixSOSync_Search.registerReflectionInfo();
        }
        @AuraEnabled public String SalesDocument
        {
            get { return this.getString ('VBELN'); }
            set { this.Set (value, 'VBELN'); }
        }

        @AuraEnabled public String ItemNumber
        {
            get { return this.getString ('POSNR'); }
            set { this.Set (value, 'POSNR'); }
        }

        @AuraEnabled public String Material
        {
            get { return this.getString ('MATNR'); }
            set { this.Set (value, 'MATNR'); }
        }

        @AuraEnabled public String ItemDescription
        {
            get { return this.getString ('ARKTX'); }
            set { this.Set (value, 'ARKTX'); }
        }

        @AuraEnabled public Decimal OrderQuantity
        {
            get { return this.getDecimal ('KWMENG'); }
            set { this.Set (value, 'KWMENG'); }
        }

        @AuraEnabled public String SalesUnit
        {
            get { return this.getString ('VRKME'); }
            set { this.Set (value, 'VRKME'); }
        }

        @AuraEnabled public Decimal NetItemPrice
        {
            get { return this.getDecimal ('NETPR'); }
            set { this.Set (value, 'NETPR'); }
        }

        @AuraEnabled public Decimal NetOrderItemValue
        {
            get { return this.getDecimal ('NETWR'); }
            set { this.Set (value, 'NETWR'); }
        }

        @AuraEnabled public Decimal NetOrderValue
        {
            get { return this.getDecimal ('NETWR_AK'); }
            set { this.Set (value, 'NETWR_AK'); }
        }

        @AuraEnabled public String SalesDocumentCurrency
        {
            get { return this.getString ('WAERK'); }
            set { this.Set (value, 'WAERK'); }
        }

        @AuraEnabled public String SpecifyWarrantyConditions
        {
            get { return this.getString ('CLAIM_TEXT'); }
            set { this.Set (value, 'CLAIM_TEXT'); }
        }

        @AuraEnabled public String ReasonForRejection
        {
            get { return this.getString ('ABGRU'); }
            set { this.Set (value, 'ABGRU'); }
        }

        @AuraEnabled public String ReasonForRejectionDescription
        {
            get { return this.getString ('ABGRU_TEXT'); }
            set { this.Set (value, 'ABGRU_TEXT'); }
        }

        @AuraEnabled public String OrderItemRejectionFlag
        {
            get { return this.getString ('REJECTED_FLAG'); }
            set { this.Set (value, 'REJECTED_FLAG'); }
        }

    }

    public class SEARCHRESULT_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public SEARCHRESULT_COLLECTION()
        {
            super('SEARCHRESULT', SBO_EnosixSOSync_Search.SEARCHRESULT.class, null);
        }

        public List<SBO_EnosixSOSync_Search.SEARCHRESULT> getAsList()
        {
            return (List<SBO_EnosixSOSync_Search.SEARCHRESULT>)this.buildList(List<SBO_EnosixSOSync_Search.SEARCHRESULT>.class);
        }
    }

}