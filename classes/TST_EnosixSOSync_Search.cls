/// enosiX Inc. Generated Apex Model
/// Generated On: 1/15/2020 10:59:35 AM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class TST_EnosixSOSync_Search
{

    public class MockSBO_EnosixSOSync_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSOSync_Search.class, new MockSBO_EnosixSOSync_Search());
        SBO_EnosixSOSync_Search sbo = new SBO_EnosixSOSync_Search();
        System.assertEquals(SBO_EnosixSOSync_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        SBO_EnosixSOSync_Search.EnosixSOSync_SC sc = new SBO_EnosixSOSync_Search.EnosixSOSync_SC();
        System.assertEquals(SBO_EnosixSOSync_Search.EnosixSOSync_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);
        System.assertNotEquals(null, sc.DOC_TYPE);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        SBO_EnosixSOSync_Search.SEARCHPARAMS childObj = new SBO_EnosixSOSync_Search.SEARCHPARAMS();
        System.assertEquals(SBO_EnosixSOSync_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.DateFrom = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.DateFrom);

        childObj.InitialLoad = true;
        System.assertEquals(true, childObj.InitialLoad);


    }

    @isTest
    static void testDOC_TYPE()
    {
        SBO_EnosixSOSync_Search.DOC_TYPE childObj = new SBO_EnosixSOSync_Search.DOC_TYPE();
        System.assertEquals(SBO_EnosixSOSync_Search.DOC_TYPE.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixSOSync_Search.DOC_TYPE_COLLECTION childObjCollection = new SBO_EnosixSOSync_Search.DOC_TYPE_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.SalesDocumentType = 'X';
        System.assertEquals('X', childObj.SalesDocumentType);


    }

    @isTest
    static void testEnosixSOSync_SR()
    {
        SBO_EnosixSOSync_Search.EnosixSOSync_SR sr = new SBO_EnosixSOSync_Search.EnosixSOSync_SR();

        sr.registerReflectionForClass();

        System.assertEquals(SBO_EnosixSOSync_Search.EnosixSOSync_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        SBO_EnosixSOSync_Search.SEARCHRESULT childObj = new SBO_EnosixSOSync_Search.SEARCHRESULT();
        System.assertEquals(SBO_EnosixSOSync_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixSOSync_Search.SEARCHRESULT_COLLECTION childObjCollection = new SBO_EnosixSOSync_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.SalesDocument = 'X';
        System.assertEquals('X', childObj.SalesDocument);

        childObj.ItemNumber = 'X';
        System.assertEquals('X', childObj.ItemNumber);

        childObj.Material = 'X';
        System.assertEquals('X', childObj.Material);

        childObj.ItemDescription = 'X';
        System.assertEquals('X', childObj.ItemDescription);

        childObj.OrderQuantity = 1.5;
        System.assertEquals(1.5, childObj.OrderQuantity);

        childObj.SalesUnit = 'X';
        System.assertEquals('X', childObj.SalesUnit);

        childObj.NetItemPrice = 1.5;
        System.assertEquals(1.5, childObj.NetItemPrice);

        childObj.NetOrderItemValue = 1.5;
        System.assertEquals(1.5, childObj.NetOrderItemValue);

        childObj.NetOrderValue = 1.5;
        System.assertEquals(1.5, childObj.NetOrderValue);

        childObj.SalesDocumentCurrency = 'X';
        System.assertEquals('X', childObj.SalesDocumentCurrency);

        childObj.SpecifyWarrantyConditions = 'X';
        System.assertEquals('X', childObj.SpecifyWarrantyConditions);

        childObj.ReasonForRejection = 'X';
        System.assertEquals('X', childObj.ReasonForRejection);

        childObj.ReasonForRejectionDescription = 'X';
        System.assertEquals('X', childObj.ReasonForRejectionDescription);

        childObj.OrderItemRejectionFlag = 'X';
        System.assertEquals('X', childObj.OrderItemRejectionFlag);


    }

}