public class C_InvoiceBanner {

    ApexPages.StandardController stdController;
    private Invoice__c theInvoice;

    public C_InvoiceBanner(ApexPages.StandardController controller) {
        stdController = controller;
        this.theInvoice = (Invoice__c) stdController.getRecord();
        
        if(this.theInvoice.RecordType.Name == 'Customer Invoice'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'CUSTOMER INVOICE'));
        }
        else if(this.theInvoice.RecordType.Name == 'Vendor Invoice'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'VENDOR INVOICE'));
        }
                
        }
        }