public class CZ_QuoteSaveAlertExt {
    
    private SBQQ__Quote__c quote;
    
    private Id quoteId;
    
    public boolean doNotShow = false;
    
    public Pagereference initPage(){
        
        quoteId = System.currentPagereference().getParameters().get('Id');
        
        System.debug('Id found: '+quoteId);
        /*
        String quoteJSON = SBQQ.ServiceRouter.read('SBQQ.QuoteAPI.QuoteReader', quoteId);

        QuoteModel model =  (QuoteModel)JSON.deserialize(quoteJSON, QuoteModel.class);

        quoteJSON = SBQQ.ServiceRouter.save('SBQQ.QuoteAPI.QuoteSaver', JSON.serialize(model));
        
        model =  (QuoteModel) JSON.deserialize(quoteJSON, QuoteModel.class);
        */
        quote = [Select Id, Show_Dryer_Alert__c from SBQQ__Quote__c where Id = :quoteId];
        
        System.debug('Show Dryer Alert: '+quote.Show_Dryer_Alert__c);
        
        if(quote.Show_Dryer_Alert__c){
            
            List<SBQQ__QuoteLine__c> quoteLineList = [Select Product_MG1_Code__c, Product_Material_Group_Desc__c from SBQQ__QuoteLine__c where SBQQ__Quote__c = :quoteId];
            
            Integer deviceCount = 0;
            
            Integer dryerCount = 0;
            
            for(SBQQ__QuoteLine__c quoteLine : quoteLineList){
                
                if(quoteLine.Product_MG1_Code__c == 'M'){
                    
                    deviceCount++;
                    
                }
                
                if(quoteLine.Product_Material_Group_Desc__c.contains('Dryer')){
                    
                    dryerCount++;
                    
                }
                
            }
            System.debug('Device/Dryer: '+deviceCount+'/'+dryerCount);
            
            if(deviceCount == 0 || dryerCount > 0){
                
                return redirectQ(quoteId);
                
            }else{
                
                return null;
                
            }
            
        }else{
            
            return redirectQ(quoteId);
            
        }
        
    }
    
    public PageReference redirectQ(id qid){
        
        PageReference QuotePage = new PageReference(qid);
        System.debug('QuotePage: ' + QuotePage);
        QuotePage.setRedirect(true);
        
        return QuotePage;
        
    }
    
    public boolean getDoNotShow(){
        
        return doNotShow;
        
    }
    
    public void setDoNotShow(boolean dns){
        
        doNotShow = dns;
        
    }
    
    public PageReference cancel(){
        
        System.debug('In Save: '+doNotShow);
        
        quote = [Select Id, Show_Dryer_Alert__c from SBQQ__Quote__c where Id = :quoteId];
        
        quote.Show_Dryer_Alert__c = !doNotShow;
        
        update quote;
        
        return redirectQ(quoteId);
        
    }
    
    public PageReference continueToQuote(){
        
        System.debug('In Save: '+doNotShow);
        
        quote = [Select Id, Show_Dryer_Alert__c from SBQQ__Quote__c where Id = :quoteId];
        
        quote.Show_Dryer_Alert__c = !doNotShow;
        
        update quote;
        
        return redirectQ(quoteId);
        
    }

}