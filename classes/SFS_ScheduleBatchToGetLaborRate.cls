/* Author: Arati - Capgemini
 * Date: 07/24/23
 * Description: Scheduleable class to getLaborRate for PM WOLI's.
 * TODO:
 */
public class SFS_ScheduleBatchToGetLaborRate implements schedulable {
public void execute(SchedulableContext sc) { 
    List<WorkOrderLineItem> WoliLIst = [select Id,workorderId,workorder.SFS_WOLI_Count__c,CreatedDate,SFS_Overtime_Labor_Rate__c,SFS_Standard_Labor_Rate__c, SFS_Double_Labor_Rate__c from WorkOrderLineItem 
                                        where (SFS_Overtime_Labor_Rate__c = null OR SFS_Standard_Labor_Rate__c = null OR SFS_Double_Labor_Rate__c = null) AND (WorkOrder.IsGeneratedFromMaintenancePlan = true OR WorkOrder.SFS_CPQ_Transaction_Id__c != null) AND SFS_Labor_Rate_Triggered__c = false order by CreatedDate desc limit 150];

    system.debug('WoliLIst'+WoliLIst.size());
    set<ID> woID =  new set<ID>();
    for(Workorderlineitem woli:WoliLIst){
        if(!woID.contains(woli.WorkOrderId)){
            woID.add(woli.workorderId);
        }   
    }
      system.debug('woID'+woID.size());
    system.debug('woID'+woID);
    List<WorkOrder> WoList = [Select Id,SFS_WOLI_Count__c,
                              (select Id,workorderId,workorder.SFS_WOLI_Count__c,CreatedDate,SFS_Overtime_Labor_Rate__c,SFS_Standard_Labor_Rate__c, SFS_Double_Labor_Rate__c from WorkOrderLineItems where (SFS_Overtime_Labor_Rate__c = null OR SFS_Standard_Labor_Rate__c = null OR SFS_Double_Labor_Rate__c = null) AND (WorkOrder.IsGeneratedFromMaintenancePlan = true OR WorkOrder.SFS_CPQ_Transaction_Id__c != null) AND SFS_Labor_Rate_Triggered__c = false limit 1)
                               from WorkOrder where ID IN :woID limit 49];        
    Integer delayInMinutes =1;
    for(WorkOrder wo : WoList){
        system.debug('childs'+wo.Workorderlineitems);
        if(!wo.Workorderlineitems.isEmpty()){
            for(Workorderlineitem woli:wo.Workorderlineitems){
                  system.debug('delayInMinutes'+delayInMinutes);
                  List<Id> requestIDs = new  List<Id>();
                  requestIDs.add(woli.Id);
                  System.enqueueJob(new SFS_GetLaborRateQueueableWoliUpdate(requestIDs),delayInMinutes);
               }  
          }
       }    
   }
}