global class C_ShareRuleReconcile implements Database.Batchable<sObject>{ 
    global final String Query;
    
    global C_ShareRuleReconcile(){
        
        Query='select id, account__c, serial_number__r.Parent_AccountID__c,Serial_Number__r.Current_Servicer_ID__c,'+
               'Serial_Number__r.Current_Servicer__r.parentID, Account_Parent_Id__c  From Service_history__c';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }   
    
    
    global void execute(Database.BatchableContext info, List<Service_history__c> scope){
        List<Service_History__Share> jobShrs  = new List<Service_History__Share>();
        
        system.debug(scope);
        if(scope.size()>0)
        {
            for(Service_history__c sh: scope)
            {
                system.debug('*****SHARE HERE INSERT***'+ sh); 
                if(!string.isBlank(sh.account__c))
                {
                    for(User u : [Select id,Partner_Account_ID__c from user where Partner_Account_ID__c = :string.valueOf(sh.account__c).substring(0,15)
                                 and isActive = true])
                    {
                        Service_History__Share share = new Service_History__Share();
                        share.AccessLevel='Edit';
                        share.ParentId=sh.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        jobShrs.add(share);
                    }
                }
                
                //Create sharing for Global Account users
                if(!string.isBlank(sh.Account_Parent_ID__c)){
                    for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :string.valueOf(sh.Account_Parent_ID__c).substring(0,15)
                                 and isActive = true])
                    {
                        Service_History__Share share = new Service_History__Share();
                        share.AccessLevel='Edit';
                        share.ParentId=sh.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        jobShrs.add(share);
                    }
                }
            }
            
            if(jobShrs.size()>0)
                system.debug(jobShrs);
            insert jobShrs;
        }
    }     
    global void finish(Database.BatchableContext info){     
    } 
}