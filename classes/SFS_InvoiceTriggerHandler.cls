/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 24/11/2021
* @description: Apex Handler class for SFS_InvoiceTrigger.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Bhargavi Nerella	M-001		06/04/2022		SIF-651
Srikanth P          M-002       29/06/2022      SIF-1009
============================================================================================================*/

public class SFS_InvoiceTriggerHandler {
    // to assign currency value on invoice from workorder and service agreements accounts currency.
    public static void invoicecurrency(List<Invoice__c> invList)
    {
        
        try{
            
            List<WorkOrder> wo=[Select Id,AccountId,Account.CurrencyIsoCode,recordType.developerName,Account.Bill_To_Account__r.CurrencyIsoCode from WorkOrder where Id =: invList[0].SFS_Work_Order__c];
            List<ServiceContract> sc=[Select Id,AccountId,Account.CurrencyIsoCode,recordType.developerName from ServiceContract where Id =: invList[0].SFS_Service_Agreement__c];
            //system.debug('invlist'+invList[0]);
            String recordtypeName = '';
            for(ServiceContract s: sc){
                System.debug('RecordType:'+s.recordType.developerName);
                recordTypeName = s.recordType.developerName;
            }
            String WorkRecordTypeName = '';
            for(WorkOrder w : wo){
                WorkRecordTypeName = w.recordType.developerName;
            }
            
            for(Invoice__c inv : invList){
                System.debug('RecordType:'+inv.SFS_Service_Agreement__r.RecordType.Name);
                System.debug('WorkOrder:'+inv.SFS_Work_Order__c);
                //if(inv.SFS_Service_Agreement__r.RecordType.Name!=null){
                if(inv.SFS_Work_Order__c == null ){
                    if(recordTypeName == 'SFS_Rental'){
                        inv.CurrencyIsoCode = sc[0].Account.CurrencyIsoCode;
                        //system.debug('CurrencyIsoCode'+sc[0].Account.CurrencyIsoCode);
                    }
                    
                }
                else if(inv.SFS_Work_Order__c != null){
                    inv.CurrencyIsoCode = wo[0].Account.Bill_To_Account__r.CurrencyIsoCode;
                    system.debug('**InvCurrencyIsoCode'+wo[0].Account.Bill_To_Account__r.CurrencyIsoCode);
                }
                //}
            }
            
        }
        catch(Exception e){
            system.debug('@@@exception'+e.getMessage());
        }
        
        
        
    }
/*Compare the summation of all submitted invoices amount and upcoming submitted invoice amount with service agreement PO Value, if 
it exceeds create the task for the Service Agreement*/
    public static void assignTaskOnInvoiceSubmission(List<Invoice__c> invoiceList,Map<Id,Invoice__c> invMap){
        try{
            system.debug('@@@@invoiceList'+invoiceList);
            Set<Id> saIds = new Set<Id>();
            // Set<Id> woIds = new Set<Id>();
            for(Invoice__c record : invoiceList){
                System.debug('salist:'+record.SFS_Service_Agreement__c);
                saIds.add(record.SFS_Service_Agreement__c);
                //woIds.add(record.SFS_Work_Order__c);            
            }
            Map<Id,List<Invoice__c>> agreementToInvoicesMap=new Map<Id,List<Invoice__c>>();
            Map<Id,ServiceContract> agreementIdToRecord=new Map<Id,ServiceContract>();
            Map<Id,Double> invoiceToNextInvoiceAmount=new Map<Id,Double>();
            List<ServiceContract> agreementList =new List<ServiceContract>();
            if((saIds.size()>0)){
             agreementList=[Select Id,Name,SFS_PO_Value__c,ContractNumber,Account.Name,(Select Id,Name,SFS_Billing_Period_Start_Date__c,SFS_Invoice_Amount__c, SFS_Service_Agreement__c,SFS_Status__c from Invoices__r order by Name) from ServiceContract where Id IN: saIds];
            }
            if(agreementList.size()>0){
                for(ServiceContract agreement:agreementList){
                    Date nextInvoiceDate;
                    Double nextInvoiceAmount;
                    List<Invoice__c> updatedList=new List<Invoice__c>();
                    List<Invoice__c> invoicesList=agreement.Invoices__r;
                    for(Invoice__c record : invoicesList){
                        if(record.SFS_Status__c=='Submitted'){
                            updatedList.add(record);
                            nextInvoiceDate=record.SFS_Billing_Period_Start_Date__c+7;
                            System.debug('67--'+nextInvoiceDate);
                            System.debug('68--'+record.Name);
                        }else{
                            if(record.SFS_Billing_Period_Start_Date__c==nextInvoiceDate){
                                nextInvoiceAmount=record.SFS_Invoice_Amount__c;
                                System.debug('72--'+record.Name);
                                System.debug('73--'+nextInvoiceDate);
                                break;
                            }
                        }
                    }
                    System.debug('76--'+nextInvoiceAmount);
                    agreementToInvoicesMap.put(agreement.Id,updatedList);
                    agreementIdToRecord.put(agreement.Id,agreement);
                    invoiceToNextInvoiceAmount.put(agreement.Id,nextInvoiceAmount);
                }
            }
            List<Task> taskList=new List<Task>();
            for(Invoice__c newRecord : invoiceList){
                Invoice__c oldRecord =invMap.get(newRecord.Id);
                if(newRecord.SFS_Status__c =='Submitted' && newRecord.SFS_Status__c!=oldRecord.SFS_Status__c){
                    Double sum=0;
                    Date billDate;
                     
                        
                    if(agreementToInvoicesMap.size()>0){
                      for(Invoice__c invoice:agreementToInvoicesMap.get(newRecord.SFS_Service_Agreement__c)){
                      
                         system.debug('@@invoice.SFS_Invoice_Amount__c'+invoice.SFS_Invoice_Amount__c);
                          sum+= invoice.SFS_Invoice_Amount__c;
                      }
                    }
                    if(!invoiceToNextInvoiceAmount.keyset().isEmpty() && invoiceToNextInvoiceAmount.get(newRecord.SFS_Service_Agreement__c)!=null){
                        sum+= invoiceToNextInvoiceAmount.get(newRecord.SFS_Service_Agreement__c);
                    }
                    system.debug('Sum'+sum);
                    ServiceContract agreement;
                    if(agreementIdToRecord.size()>0){
                       agreement=agreementIdToRecord.get(newRecord.SFS_Service_Agreement__c);
                    }
                    if(agreement!=null){
                     if(sum > agreement.SFS_PO_Value__c){
                        Task record = new Task();
                        record.Subject='Rental Agreement '+agreement.ContractNumber+' for '+agreement.Account.Name+' - Update Agreement Value';
                        record.ActivityDate=system.today()+7;
                        record.Description='The funds on Rental Agreement '+agreement.ContractNumber+' are due to run out on '+(system.today()+7)+'.Please contact your customer for additional funds and update the Agreement Value field';
                        record.WhatId=newRecord.SFS_Service_Agreement__c;
                        taskList.add(record);
                     }
                    }
                }  
            }  
            insert taskList;
        } 
        catch(Exception e){}
    }  
    // Logic to create Invoice and Invoice Line Items
    @InvocableMethod(label='Create Invoice and Invoice Line Items') 
public static void createInvoiceAndLineItems(List<id> workOrderId){

        /*List<WorkOrder> woList = [ Select Id,SFS_PO_Number__c,SFS_Oracle_Quote__c,SFS_Quoted_Invoice_Generated__c,
        (Select Id,workOrderId,workOrder.ServiceContractId,SFS_PO_Number__c,SFS_Invoice_Generated__c
        from WorkOrderLineItems  Where (Status='Completed' OR Status='Closed') AND SFS_Invoice_Generated__c =false
        Order By LineItemNumber ASC) from WorkOrder where ID IN:workOrderId];*/
        
        /*List<CAP_IR_Charge__c> chargeList = [select Id, CAP_IR_Work_Order__c,CAP_IR_Work_Order_Line_Item__c,SFS_Charge_Type__c,CAP_IR_Amount__c,CAP_IR_Description__c,SFS_Quoted_Charge__c,
        SFS_PO_Number__c,CAP_IR_Product_Consumed__c,SFS_Work_Order_Line_Item__c,SFS_Expense__c,CAP_IR_Labor__c,CurrencyIsoCode,SFS_Sell_Price__c,SFS_Work_Order__c,SFS_Quote_Charge_Line_Number__c
        from CAP_IR_Charge__c where CAP_IR_Work_Order__c IN:workOrderId OR SFS_Work_Order__c=:workOrderId ORDER BY SFS_Quote_Charge_Line_Number__c ASC];*/
        //List<WorkOrderLineItem> woliList = new List<WorkOrderLineItem>();
        // woliList.addAll(woList[0].WorkOrderLineItems);
        
        List<WorkOrder> woList = [Select Id, SFS_Oracle_Quote__c from WorkOrder where Id IN: workOrderId];
        Set<Id> quoteWoIds = new Set<Id>();
        Set<Id> tmWoIds = new Set<Id>();
        for(WorkOrder wo : woList){
            if(wo.SFS_Oracle_Quote__c == null){               
                tmWoIds.add(wo.Id);
            }else{
                quoteWoIds.add(wo.Id);
            }
        }
        String queryString='Select Id, CAP_IR_Work_Order__c,CAP_IR_Work_Order_Line_Item__c,SFS_Invoice_Status__c,SFS_Charge_Type__c,CAP_IR_Amount__c,CAP_IR_Description__c,SFS_Quoted_Charge__c,SFS_PO_Number__c,CAP_IR_Product_Consumed__c,SFS_Work_Order_Line_Item__c,SFS_Expense__c,CAP_IR_Labor__c,CurrencyIsoCode,SFS_Sell_Price__c,SFS_Work_Order__c,SFS_Quote_Charge_Line_Number__c from CAP_IR_Charge__c where ';
        if(quoteWoIds.size()>0){
            queryString+= '(SFS_Work_Order__c IN : quoteWoIds OR (CAP_IR_Work_Order__c IN : quoteWoIds AND SFS_Charge_Type__c = \'Freight / S&H\'))';
        }else if(tmWoIds.size()>0){
            queryString+= '((CAP_IR_Work_Order__c IN : tmWoIds AND (CAP_IR_Work_Order_Line_Item__r.Status=\'Completed\' OR CAP_IR_Work_Order_Line_Item__r.Status=\'Closed\')) OR (CAP_IR_Work_Order__c IN : tmWoIds AND (SFS_Charge_Type__c = \'Freight / S&H\' OR SFS_Charge_Type__c=\'Ordered Item\')))';
        }
        queryString+= 'AND (SFS_Invoice__c=null OR (SFS_Invoice__c!=null AND SFS_Invoice_Status__c=\'Canceled\')) ORDER BY SFS_Quote_Charge_Line_Number__c ASC';
		System.debug('155--'+queryString);
        List<CAP_IR_Charge__c> chargeList = Database.query(queryString);
        
        ID invRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'SFS_WO_Invoice'].id;
        List<SFS_Invoice_Line_Item__c> invLiList = [Select Id from SFS_Invoice_Line_Item__c where SFS_Work_Order__c =:workOrderId AND SFS_Type__c = 'Freight / S&H'];  
        
        Map<String,WorkOrderLineItem> mapPOWoli = new Map<String,WorkOrderLineItem>();
        List<WorkOrderLineItem> woliUpdate = new List<WorkOrderLineItem>();
        List<ID> woliIDs = new  List<ID>();
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        List<String> uniquePONumber = new List<String>();
        List<CAP_IR_Charge__c> freEmercharges = new List<CAP_IR_Charge__c>();
        List<CAP_IR_Charge__c> emergencyCharges = new List<CAP_IR_Charge__c>();
        List<CAP_IR_Charge__c> purchaseExCharges = new List<CAP_IR_Charge__c>();
        List<CAP_IR_Charge__c> freightCharges = new List<CAP_IR_Charge__c>();
        Boolean quotedChargesExist = woList[0].SFS_Oracle_Quote__c!=Null ? True : False;
        
        for(CAP_IR_Charge__c charge:chargeList){
            if(charge.SFS_PO_Number__c=='' || !uniquePONumber.contains(charge.SFS_PO_Number__c) ||charge.CAP_IR_Description__c =='Emergency Freight Fee' || charge.SFS_Charge_Type__c == 'Ordered Item'){
                if(charge.CAP_IR_Description__c !='Emergency Freight Fee' && charge.SFS_Charge_Type__c != 'Ordered Item'){
                    if(quotedChargesExist && (charge.SFS_Quoted_Charge__c||charge.CAP_IR_Description__c =='Freight / S&H')){  
                        uniquePONumber.add(charge.SFS_PO_Number__c);
                    }
                    else if(!quotedChargesExist && !charge.SFS_Quoted_Charge__c){
                        uniquePONumber.add(charge.SFS_PO_Number__c);
                        if(!woliIDs.contains(charge.CAP_IR_Work_Order_Line_Item__c)){
                            woliIDs.add(charge.CAP_IR_Work_Order_Line_Item__c); 
                        }
                    }
                }
            
               if(invLiList.isEmpty() ||charge.SFS_Invoice_Status__c== 'Canceled'){  
                   				System.debug('Charge Type '+charge.SFS_Charge_Type__c);
                               if(charge.SFS_Charge_Type__c == 'Ordered Item'){
                                   purchaseExCharges.add(charge);
                                 }
                               /*if(charge.CAP_IR_Description__c =='Freight / S&H'){
                                   freightCharges.add(charge);
                               }*/
                              if(charge.CAP_IR_Description__c =='Emergency Freight Fee'){
                                  emergencyCharges.add(charge);
                              }
                
                          } 
            }  
            // Logic to filter freight and emergency charges
            
        
      }    
         /* if(!purchaseExCharges.isEmpty()){
                  System.debug('295--'+purchaseExCharges);
             freEmercharges.addAll(purchaseExCharges);
          }*/
         /* if(!freightCharges.isEmpty()){
             freEmercharges.addAll(freightCharges);
          }*/
         /* if(!emergencyCharges.isEmpty()){
             freEmercharges.addAll(emergencyCharges);
          }*/
        
        //Invoice Creation
         List<Invoice__c> nsInvList = [Select Id,SFS_Status__c,SFS_PO_Number__c 
                                       from Invoice__c where SFS_Status__c !='Submitted'AND  SFS_Status__c !='Canceled' AND SFS_Work_Order__c =:workOrderId];
         Map<Id,String> mapInvPoNum = new Map<Id,String>();
         if(!nsInvList.IsEmpty()){
             for(Invoice__c inv :nsInvList){
                  mapInvPoNum.put(inv.Id,inv.SFS_PO_Number__c);
               }
          }   
        system.debug('***mapInvPoNum'+mapInvPoNum);
        system.debug('***mapPOWoli'+mapPOWoli.KeySet());
        system.debug('***uniquePONum'+uniquePONumber);
       if(!uniquePONumber.isEmpty()){
          for(String poNumber : uniquePONumber){
             if(!mapInvPoNum.values().Contains(poNumber)){
                invoiceList.add( new Invoice__c(
                                           recordTypeId = invRecId,
                                           SFS_Work_Order__c = woList[0].Id ,
                                           SFS_Status__c='Created',
                                           SFS_Invoice_Type__c = 'Receivable',   
                                           SFS_PO_Number__c = poNumber ));                
               } 
           }
        } 
 
         system.debug('@@ Invoice Creation List'+invoiceList);
         if(!invoiceList.isEmpty() || !mapInvPoNum.isEmpty()){
              Database.SaveResult[] srList = Database.insert(invoiceList, false);  
              Set<Id> invIds = new Set<Id>();
              for(Database.SaveResult sr : srList){
                    if(sr.isSuccess()){
                        invIds.add(sr.getId());
                      }    
                  } 
             if(quotedChargesExist){
                woList[0].SFS_Quoted_Invoice_Generated__c =true; 
             }
          invIds.addAll(mapInvPoNum.KeySet());
          List<Invoice__c> invList = [Select Id, SFS_PO_Number__c,CurrencyIsoCode, SFS_Maximum_Sequence__c from Invoice__c where Id IN :invIds]; 
          String invCurrency = invList[0].CurrencyIsoCode;                  
          Boolean firstInv = true;
          List<SFS_Invoice_Line_Item__c> invliListToInsert = new List<SFS_Invoice_Line_Item__c>();
          Map<Id,List<CAP_IR_Charge__c>> mapInvCharges = new Map<Id,List<CAP_IR_Charge__c>>();
          Map<Id,Double> mapWoliTTAmount  = new Map<Id,Double>();
          Map<Id,Double> mapWoliTTSellPrice  = new Map<Id,Double>();
          Map<Id,Decimal> invoiceToSequenceMap = new Map<Id,Decimal>();
          for(Invoice__c inv : invList) {
              invoiceToSequenceMap.put(inv.Id,inv.SFS_Maximum_Sequence__c);
              List<CAP_IR_Charge__c> quotedCharges = new List<CAP_IR_Charge__c>();
              List<CAP_IR_Charge__c> ptCharge = new List<CAP_IR_Charge__c>();
              List<CAP_IR_Charge__c> exCharge = new List<CAP_IR_Charge__c>();
              List<CAP_IR_Charge__c> lhCharge = new List<CAP_IR_Charge__c>();
              List<CAP_IR_Charge__c> allCharges = new List<CAP_IR_Charge__c>(); 
              List<CAP_IR_Charge__c> freigtCharges = new List<CAP_IR_Charge__c>(); 
              
              for(CAP_IR_Charge__c c :chargeList){ 
                   if(quotedChargesExist){
                       if((c.SFS_Quoted_Charge__c||c.SFS_Charge_Type__c== 'Freight / S&H')&& inv.SFS_PO_Number__c == c.SFS_PO_Number__c ){
                           //quotedCharges.add(c); 
                            if(inv.SFS_PO_Number__c == c.SFS_PO_Number__c){
                                      Double tTimeAmount = 00.0;
                                       
                                       if(c.SFS_Charge_Type__c=='Time' ){
                                          lhCharge.add(c);
                                         }                                        
                                       if(c.SFS_Charge_Type__c=='Expense'){
                                            exCharge.add(c);
                                         } 
                                       if(c.SFS_Charge_Type__c =='Freight / S&H'){
                                           freigtCharges.add(c);
                            
                                       }
                                       if(c.SFS_Charge_Type__c=='Part Movement'){
                                           ptCharge.add(c);
                                       }
                            }
                          }
                     }
                                  
             else 
                if(!quotedChargesExist && !c.SFS_Quoted_Charge__c){                
                    if(inv.SFS_PO_Number__c == c.SFS_PO_Number__c){
                                      Double tTimeAmount = 00.0;
                                       if(c.SFS_Charge_Type__c=='Time' ){
                                          lhCharge.add(c);
                                         }
                                       if(c.SFS_Charge_Type__c =='Time' && c.CAP_IR_Description__c =='Travel Time Labor'){
                                           mapWoliTTAmount.put(c.CAP_IR_Work_Order_Line_Item__c,c.CAP_IR_Amount__c);
                                           mapWoliTTSellPrice.put(c.CAP_IR_Work_Order_Line_Item__c,c.SFS_Sell_Price__c);
                                         }
                                       if(c.SFS_Charge_Type__c=='Part Movement'){
                                           ptCharge.add(c);
                                         }
                                       if(c.SFS_Charge_Type__c=='Expense'){
                                            exCharge.add(c);
                                         } 
                                       if(c.SFS_Charge_Type__c =='Freight / S&H'){
                                           freigtCharges.add(c);
                            
                                       }
                    }
              }   
          }         
              /*if(!quotedCharges.isEmpty()){
                  system.debug('***quotedCharges'+quotedCharges);
                  allCharges.addAll(quotedCharges);
                }  */
             // if(quotedCharges.isEmpty()){ 
                  if(!lhCharge.isEmpty()){
                     	 system.debug('***lhCharge'+lhCharge);
                     	 allCharges.addAll(lhCharge);
                    }
                   if(!ptCharge.isEmpty()){
                      	system.debug('***ptCharge'+ptCharge);
                      	allCharges.addAll(ptCharge);
                    } 
                   if(!exCharge.isEmpty()){
                      	system.debug('***exCharge'+exCharge);
                      	allCharges.addAll(exCharge);
                    }
                   if(!freigtCharges.isEmpty() && !firstInv){
                      	system.debug('***freigtCharges'+freigtCharges);
                      	allCharges.addAll(freigtCharges);
                    }
                // }
              
               if(!purchaseExCharges.isEmpty() && firstInv ){
                  System.debug('295--'+purchaseExCharges);
                  allCharges.addAll(purchaseExCharges);
               }
              if(!freigtCharges.isEmpty() && firstInv){
                  system.debug('***freigtCharges'+freigtCharges);
                  allCharges.addAll(freigtCharges);
              }
        
             if(!emergencyCharges.isEmpty() && firstInv ){
                 allCharges.addAll(emergencyCharges);
             }
             
              mapInvCharges.put(inv.Id,allCharges);
              firstInv = false;
              system.debug('***mapInvCharges'+mapInvCharges);
          
       }
        //Logic to create Invoice Line Items and update invoice on charge
        List<CAP_IR_Charge__c> chargeToUpdate = new List<CAP_IR_Charge__c>();
        for(Id invId : mapInvCharges.keySet()){
           List<CAP_IR_Charge__c>  invChargesList =  mapInvCharges.get(invId);
           Double Amount = 0;
             Decimal sellPrice = 0;
            Integer sequence = 0;
            if(invoiceToSequenceMap.containsKey(invId) && invoiceToSequenceMap.get(invId)!=null){
                sequence = Integer.valueOf(invoiceToSequenceMap.get(invId));
            }
           for(CAP_IR_Charge__c c : invChargesList){
               sequence+=1;
                Amount = (c.SFS_Charge_Type__c == 'Time' && mapWoliTTAmount.get(c.CAP_IR_Work_Order_Line_Item__c)!=null) ? c.CAP_IR_Amount__c + mapWoliTTAmount.get(c.CAP_IR_Work_Order_Line_Item__c):c.CAP_IR_Amount__c;
               sellPrice = (c.SFS_Charge_Type__c == 'Time' && mapWoliTTSellPrice.get(c.CAP_IR_Work_Order_Line_Item__c)!=null) ? c.SFS_Sell_Price__c + mapWoliTTSellPrice.get(c.CAP_IR_Work_Order_Line_Item__c):c.SFS_Sell_Price__c;
                String woliId = c.SFS_Quoted_Charge__c? NULL:c.CAP_IR_Work_Order_Line_Item__c;
                String woId = c.SFS_Quoted_Charge__c? c.SFS_Work_Order__c :c.CAP_IR_Work_Order__c;
                if(c.CAP_IR_Description__c !='Travel Time Labor'){
        	        invliListToInsert.add(new SFS_Invoice_Line_Item__c(
                                               SFS_Invoice__c = invId,
                                               SFS_Work_Order__c = woId,
                                               SFS_Work_Order_Line_Item__c = woliId,
                                               SFS_Amount__c = Amount,
                                               SFS_Sequence__c = sequence,
                                               SFS_Description__c = c.CAP_IR_Description__c,
                                               SFS_Type__c = c.SFS_Charge_Type__c,
                                               SFS_Part_Consumed__c = c.CAP_IR_Product_Consumed__c,
                                               SFS_Expense__c = c.SFS_Expense__c,
                                               SFS_Labor__c = c.CAP_IR_Labor__c,
                                               SFS_Sell_Price__c=(c.SFS_Charge_Type__c == 'Time')?sellPrice:c.SFS_Sell_Price__c,
                                               CurrencyIsoCode = invCurrency
                                          ));
                }   
               System.debug('Invoice recors'+ invliListToInsert);
               c.SFS_Invoice__c =invId;
               
               chargeToUpdate.add(c);
          } 
            
        }
       if(!invliListToInsert.isEmpty()){
         // Database.insert(invliListToInsert,true);
           insert invliListToInsert;
           system.debug('*** Generated Invoice Line Items***'+invliListToInsert);
        }
        if(!chargeToUpdate.isEmpty()){     
            update chargeToUpdate;
        }   
               
      }        
    }      
}