public class SFS_CreatePartRequestLineItems {
	@InvocableMethod(label='Create Part Request Line Items' description='Create Part Request Line Items')
    Public Static void createPRLI(List<ID> ids){
        try{
            List<ProductRequest> prList=[Select Id,ProductRequestNumber,WorkOrderId,WorkOrderLineItemId,SourceLocationId,
                                         (Select Id,Product2Id from ProductRequestLineItems) from ProductRequest
                                         where Id IN : ids];
            system.debug('prList'+prList);
            
            Set<Id> woIds = new Set<Id>();
            Set<Id> woliIds = new Set<Id>();
            Map<Id,Set<Id>> prToProductMap = new Map<Id,Set<Id>>();
            for(ProductRequest record : prList){
                set<Id> prliProductIds= new Set<Id>();
                if(record.ProductRequestLineItems.size()>0){
                    for(ProductRequestLineItem prliRec : record.ProductRequestLineItems){
                        prliProductIds.add(prliRec.Product2Id);
                        system.debug('prList'+prList);
                    }
                    prToProductMap.put(record.Id,prliProductIds);
                }
                if(record.WorkOrderLineItemId == null){
                    woIds.add(record.WorkOrderId);
                    system.debug('prList'+prList);
                }else{
                    woliIds.add(record.WorkOrderLineItemId);
                    system.debug('prList'+prList);
                }
            }
            Map<Id,List<ProductRequired>> partRequiredMap = new Map<Id,List<ProductRequired>>();
            Set<Id> productIds = new Set<Id>();
            Map<Id,Id> assetMap=new Map<Id,Id>();
            List<WorkOrder> woList = [Select Id,AssetId,(Select Id,Product2Id,QuantityRequired from ProductsRequired) from WorkOrder where Id IN : woIds];
            if(woList.size()>0){
                for(WorkOrder wo : woList){
                    assetMap.put(wo.Id, wo.AssetId);
                    system.debug('prList'+prList);
                    if(wo.ProductsRequired.size()>0){
                        partRequiredMap.put(wo.Id,wo.ProductsRequired);
                        for(ProductRequired preq : wo.ProductsRequired){
                            productIds.add(preq.Product2Id);
                            system.debug('prList'+prList);
                        }
                    }
                }
            }
            List<WorkOrderLineItem> woliList = [Select Id,AssetId,(Select Id,Product2Id,QuantityRequired from ProductsRequired) from WorkOrderLineItem where Id IN : woliIds];
            if(woliList.size()>0){
                for(WorkOrderLineItem woli : woliList){
                    assetMap.put(woli.Id, woli.AssetId);
                    system.debug('prList'+prList);
                    if(woli.ProductsRequired.size()>0){
                        partRequiredMap.put(woli.Id,woli.ProductsRequired);
                        for(ProductRequired preq : woli.ProductsRequired){
                            productIds.add(preq.Product2Id);
                            system.debug('prList'+prList);
                        }
                    }
                }
            }
            Map<Id,Product2> productMap = new Map<Id,Product2>();
            List<Product2> productsList=[Select Id,Status__c,SFS_Superceded__c from Product2 where Id IN :productIds];
            if(productsList.size()>0){
                for(Product2 product : productsList){
                    productMap.put(product.Id,product);
                    system.debug('prList'+prList);
                }
            }
            List<ProductRequestLineItem> prliList = new List<ProductRequestLineItem>();
            String prliRecordTypeID = Schema.SObjectType.ProductRequestLineItem.getRecordTypeInfosByName().get('Service').getRecordTypeId();
            for(ProductRequest record : prList){
                List<ProductRequired> preqList=new List<ProductRequired>();
                if(record.WorkOrderLineItemId != null && partRequiredMap.containsKey(record.WorkOrderLineItemId)){
                    preqList=partRequiredMap.get(record.WorkOrderLineItemId);
                    system.debug('prList'+prList);
                }else if(record.WorkOrderId != null && partRequiredMap.containsKey(record.WorkOrderId)){
                    preqList=partRequiredMap.get(record.WorkOrderId);
                    system.debug('prList'+prList);
                }
                if(preqList.size()>0){
                    for(ProductRequired preq : preqList){
                        
                        Product2 relatedProduct=productMap.get(preq.Product2Id);
                        system.debug('relatedProduct'+relatedProduct.id);
                        Set<Id> existingProducts = new Set<Id>();
                        if(prToProductMap.containsKey(record.Id)){
                            existingProducts=prToProductMap.get(record.Id);
                            system.debug('existingProducts'+existingProducts);
                        }
                        system.debug('existingProducts'+existingProducts);
                        if(!existingProducts.contains(relatedProduct.Id)){
                            system.debug('prList'+prList);
                            if(relatedProduct.SFS_Superceded__c == false && relatedProduct.Status__c !='OBSOLETE'){
                                ProductRequestLineItem prli=new ProductRequestLineItem(ParentId=record.Id,
                                                                                       WorkOrderId=record.WorkOrderId,
                                                                                       Product2Id=preq.Product2Id,
                                                                                       QuantityRequested=preq.QuantityRequired,
                                                                                       Status='Open',
                                                                                       SourceLocationId=record.SourceLocationId,
                                                                                       RecordTypeId=prliRecordTypeID);
                                system.debug('prList'+prList);
                                if(record.WorkOrderLineItemId != null){
                                    if(assetMap.containsKey(record.WorkOrderLineItemId)){
                                        prli.SFS_Asset__c=assetMap.get(record.WorkOrderLineItemId);
                                    }
                                    prli.WorkOrderLineItemId=record.WorkOrderLineItemId;
                                    system.debug('prList'+prList);
                                }else{
                                    if(assetMap.containsKey(record.WorkOrderId)){
                                        prli.SFS_Asset__c=assetMap.get(record.WorkOrderId);
                                        system.debug('prList'+prList);
                                    }
                                }
                                prliList.add(prli);
                            }
                        }
                    }
                }
            }
            insert prliList;
            system.debug('prliList'+prliList);
        }Catch(Exception e){}
    }
}