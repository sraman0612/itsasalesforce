@isTest
public with sharing class TSTC_OpportunityInfo
{
    @isTest 
    static void test_properties()
    {
        Account account = TSTU_SFAccount.createTestAccount();
        account.Name = 'acct 1';
        insert account;

        Opportunity opp = createOpportunity();

        CTRL_OpportunityInfo controller = new CTRL_OpportunityInfo();

        Test.startTest();
        controller.sfAccountId = account.Id;
        controller.sfOpportunityId = opp.Id;

        System.assert(controller.sfAccountId == account.Id);
        System.assert(controller.sfOpportunityId == opp.Id);
        Test.stopTest();

        System.assert(controller.sfAccount.Name == account.Name);
        System.assert(controller.sfOpportunity.Name == opp.Name);
        // System.assert(controller.sfOpportunity.ENSX_EDM__Quote_Number__c == UTIL_SFSObjectDoc.getQuoteNumber(opp));
    }

    @isTest 
    static void test_redirectToQuoteDetail()
    {
        Opportunity opp = createOpportunity();

        CTRL_OpportunityInfo controller = new CTRL_OpportunityInfo();
        controller.sfOpportunityId = opp.Id;

        Test.startTest();
        PageReference result = controller.redirectToQuoteDetail();
        PageReference result2 = controller.redirectToOrderDetail();
        Test.stopTest();

        System.assert(result.getUrl().startsWith(UTIL_PageFlow.getPageURL(UTIL_PageFlow.VFP_QuoteDetail)));
        System.assert(result2.getUrl().startsWith(UTIL_PageFlow.getPageURL(UTIL_PageFlow.VFP_OrderDetail)));
    }

    private static Opportunity createOpportunity()
    {
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'opp 1';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        opp.ENSX_EDM__Quote_Number__c = 'TEST-QUOTE';
        TSTU_SFOpportunity.upsertOpportunity(opp);
        return opp;
    }
}