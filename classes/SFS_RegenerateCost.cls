/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 18/07/2022
* @description:Class to regenerate the cost charges
* @Story Number: SIF-268

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
=============================================================================================================*/
public class SFS_RegenerateCost {

    @InvocableMethod(label='Regenerate Cost')
    public static void regenerateCost(List<String> ids){
        
     List<WorkOrderLineItem> woliList = [Select Id,LineitemNumber,PricebookEntryId,SFS_Total_Amount__c,SFS_PO_Number__c,SFS_Round_Trip_Time_Hrs__c,SFS_Standard_Labor_Rate__c,WorkOrderId,
                                         (Select Id,Name,CAP_IR_Unit_Price__c,CAP_IR_Quantity__c,CAP_IR_Total_Price__c,CAP_IR_Labor_Rate__c,SFS_Reverse_Cost__c,SFS_Labor_Rate_Formula__c from Labor__r where SFS_Reverse_Cost__c=false  AND SFS_Transaction_Type__c !='Reverse Transaction'),
                                         (Select Id,Amount,Description,TotalPrice,SFS_Reverse_Cost__c,ExpenseType from Expenses1__r where SFS_Reverse_Cost__c=false AND SFS_Transaction_Type__c !='Reverse Transaction')
                                         from WorkOrderLineItem where Id IN:ids];  
        
      String chargeRecordTypeID =Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByDeveloperName().get('SFS_WO_Charge').getRecordTypeId();
                                  
      List<CAP_IR_Labor__c> lhList = new List<CAP_IR_Labor__c>();
      List<Expense> exList = new List<Expense>();
        List<WorkOrder> woList = new List<WorkOrder>();
      List<CAP_IR_Charge__c> regenChargeToInsert = new List<CAP_IR_Charge__c>();
        
      if(woliList.size()>0){ 
                for(WorkOrderLineItem woli: woliList){
                    lhList.addAll(woli.Labor__r);
                    exList.addAll(woli.Expenses1__r);
                    
                } 
            }
      if(!lhList.isEmpty()){
          List<CAP_IR_Charge__c> exChargeList = [Select Id,CAP_IR_Amount__c,SFS_Charge_Type__c,SFS_Expense_Type__c,CAP_IR_Work_Order_Line_Item__c,SFS_PO_Number__c,CAP_IR_Work_Order__c,CAP_IR_Description__c
                                                   from CAP_IR_Charge__c where CAP_IR_Work_Order_Line_Item__c  IN: woliList AND SFS_Charge_Type__c ='Time'];
          Set<Id> lhWithoutTTL = new Set<Id>();
          for(CAP_IR_Charge__c lhCharge:exChargeList){
              if(lhCharge.CAP_IR_Description__c!='Travel Time Labor'){
                  lhWithoutTTL.add(lhCharge.Id);
              }
              
          }
           Double totAmount =00.00;
          if(lhWithoutTTL.isEmpty()) {
           for(CAP_IR_Labor__c lh : lhList){
               if(lh.SFS_Labor_Rate_Formula__c != null && lh.CAP_IR_Quantity__c!=null){
                  totAmount=totAmount+(lh.SFS_Labor_Rate_Formula__c * lh.CAP_IR_Quantity__c);
                 } 
            }
           CAP_IR_Charge__c charge = new CAP_IR_Charge__c(); 
                        charge.RecordTypeId=chargeRecordTypeID;
                        charge.CAP_IR_Work_Order__c = woliList[0].WorkOrderId; 
                        charge.CAP_IR_Work_Order_Line_Item__c = woliList[0].Id; 
                        charge.CAP_IR_Date__c=system.Today();
                        charge.CAP_IR_Description__c='Labor';
                        charge.SFS_Charge_Type__c='Time';
                        charge.CAP_IR_Amount__c=totAmount; 
                        charge.SFS_Sell_Price__c=totAmount;
                        charge.SFS_PO_Number__c=woliList[0].SFS_PO_Number__c;
                        regenChargeToInsert.add(charge);
           }
         }
        
        if(!exList.isEmpty()){
            List<CAP_IR_Charge__c> exChargeList = [Select Id,CAP_IR_Amount__c,SFS_Charge_Type__c,SFS_Expense_Type__c,SFS_PO_Number__c,CAP_IR_Work_Order_Line_Item__c,CAP_IR_Work_Order__c,SFS_Sell_Price__c,SFS_Expense__c
                                                   from CAP_IR_Charge__c where CAP_IR_Work_Order_Line_Item__c  IN: woliList AND SFS_Charge_Type__c ='Expense'];
            
            Map<String,CAP_IR_Charge__c> mapChargeType = new  Map<String,CAP_IR_Charge__c>();
            Map<String,Double> mapTypeAmountUpdate = new Map<String,Double>();
            List<CAP_IR_Charge__c> exChargeUpdate = new List<CAP_IR_Charge__c>();
            Map<String,Double> mapTypeAmountInsert = new Map<String,Double>();
            List<CAP_IR_Charge__c> exChargeInsert = new List<CAP_IR_Charge__c>();
            Map<Id,Expense> mapMiscOutsideType = new Map<Id,Expense>();
            Set<Id> miscOutsideIds = new Set<Id>();
            for(CAP_IR_Charge__c ch : exChargeList ){
                if(ch.SFS_Expense_Type__c != 'Misc' && ch.SFS_Expense_Type__c !='Outside Services'){
                      mapChargeType.put(ch.SFS_Expense_Type__c,ch);
                    }             
                else{
                      miscOutsideIds.add(ch.SFS_Expense__c);
                   } 
                }
            
            for(Expense ex : exList){
                if(ex.ExpenseType != 'Misc' && ex.ExpenseType !='Outside Services'){
                    if(mapChargeType.containsKey(ex.ExpenseType)){
                       if(!mapTypeAmountUpdate.containsKey(ex.ExpenseType)){
                           mapTypeAmountUpdate.put(ex.ExpenseType,ex.Amount);
                         }
                       else{
                             //Double totAmount = mapTypeAmount.get(ex.ExpenseType) +ex.Amount;
                            mapTypeAmountUpdate.put(ex.ExpenseType,mapTypeAmountUpdate.get(ex.ExpenseType)+ex.Amount);
                           }
                      } else if(!mapTypeAmountInsert.containsKey(ex.ExpenseType)){
                                 mapTypeAmountInsert.put(ex.ExpenseType,ex.Amount);
                            } else {
                                     mapTypeAmountInsert.put(ex.ExpenseType,mapTypeAmountInsert.get(ex.ExpenseType)+ex.Amount);
                                  }
                     }
                else{
                    if(!miscOutsideIds.contains(ex.Id)){
                        mapMiscOutsideType.put(ex.Id,ex);
                    }
                }
                                       
                }  
            if(woliList[0].WorkOrderId!=null){
                  woList = [Select Id,SFS_Division__r.SFS_Margin__c, SFS_Division__r.SFS_Expense_margin__c 
                                          from WorkOrder where Id = :woliList[0].WorkOrderId ];
                 }
               if(!mapTypeAmountUpdate.KeySet().IsEmpty()){
                   for(CAP_IR_Charge__c ch : mapChargeType.Values() ){
                       if(mapTypeAmountUpdate.containsKey(ch.SFS_Expense_Type__c)){
                           ch.CAP_IR_Amount__c = mapTypeAmountUpdate.get(ch.SFS_Expense_Type__c);
                           ch.SFS_Sell_Price__c = mapTypeAmountUpdate.get(ch.SFS_Expense_Type__c)/(1-(woList[0].SFS_Division__r.SFS_Expense_margin__c/100));
                           exChargeUpdate.add(ch);
                       }
                   }
               }
             if(!mapTypeAmountInsert.keySet().isEmpty()){
                 
                 for(string exType :mapTypeAmountInsert.KeySet()){
                     
                      regenChargeToInsert.add(new CAP_IR_Charge__c(
                                           RecordTypeId=chargeRecordTypeID,
                                           CAP_IR_Work_Order__c = woliList[0].WorkOrderId,
                                           CAP_IR_Work_Order_Line_Item__c = woliList[0].Id,  
                                           CAP_IR_Date__c=system.Today(),
                                           CAP_IR_Amount__c=mapTypeAmountInsert.get(exType), 
                                           SFS_Charge_Type__c='Expense',
                                           SFS_PO_Number__c=woliList[0].SFS_PO_Number__c,
                                           CTS_Quanity__c=1,
                                           SFS_Expense_Type__c=exType,
                                           SFS_Sell_Price__c=mapTypeAmountInsert.get(exType)/(1-(woList[0].SFS_Division__r.SFS_Expense_margin__c/100))
                                        ));
                     }
               }
            if(!mapMiscOutsideType.isEmpty()){
                for(Id moIds : mapMiscOutsideType.keySet()){
                     regenChargeToInsert.add(new CAP_IR_Charge__c(
                                           RecordTypeId=chargeRecordTypeID,
                                           CAP_IR_Work_Order__c = woliList[0].WorkOrderId,
                                           CAP_IR_Work_Order_Line_Item__c = woliList[0].Id,  
                                           CAP_IR_Date__c=system.Today(),
                                           CAP_IR_Amount__c=mapMiscOutsideType.get(moIds).Amount, 
                                           SFS_Charge_Type__c='Expense',
                                           SFS_Expense__c=mapMiscOutsideType.get(moIds).Id,
                                           CAP_IR_Description__c=mapMiscOutsideType.get(moIds).Description,
                                           CTS_Quanity__c=1,
                                           SFS_PO_Number__c=woliList[0].SFS_PO_Number__c,
                                           SFS_Expense_Type__c=mapMiscOutsideType.get(moIds).ExpenseType,
                                           SFS_Sell_Price__c=mapMiscOutsideType.get(moIds).Amount/(1-(woList[0].SFS_Division__r.SFS_Expense_margin__c/100))
                                        ));
                }
            }
              if(!exChargeUpdate.isEmpty()){
                 update exChargeUpdate;
              }  
            }
             if(!regenChargeToInsert.isEmpty()){
                 insert regenChargeToInsert;
              }
         }    
    
}