@RestResource(urlMapping='/service-records/*')
global with sharing class ServiceHistory_Records {
    
    private class Attach {
        String Body;
        String ContentType;
        String Name;
    }
    
    private class Service_History {
        Service_History__c Service_History;
        List<Attach> attachments;
        List<String> submissions;
    }
    
    // parse data into json
    public static string getJSON(List<String> lst, String pt) {
        ServiceHistoryJSON shJSON = new ServiceHistoryJSON();
        shJSON.TotalQuantity = lst.size();
        //ServiceHistoryJSON.Typex t = ServiceHistoryJSON.Typex.AIR;
        shJSON.Type = pt;
        Integer installed;
        List<Integer> totalInst = new List<Integer>();
        Map<String, Integer> parts = new Map<String, Integer>();
        Set<String> setS = new Set<String>();
        for (String a : lst) {
            a = a.trim();
            setS.add(a);
        }
        
        System.debug(pt);
        
        for(String a : setS) {
            Integer cnt = 0;
            installed = 0;
            String[] splt = a.split(':');
            a = splt[0].trim();
            for(String x : lst) {
                String[] splt2 = x.split(':');
                if (a == splt2[0].trim()) {
                    cnt++;
                    if (splt2[1] == 'T') {
                        installed++;
                    }
                }
            }
            totalInst.add(installed);
            parts.put(a, cnt);
        }
        
        System.debug('parts: ' + parts);
        
        shJSON.UniqueParts = parts.size();
        shJSON.parts = new List<ServiceHistoryJSON.Tuple>();
        Integer i = 0;
        
        for (String key : parts.keySet()) {
            Integer val = parts.get(key);
            String[] partSplit = key.split(':'); // returns (example): (A11207674, T)
            ServiceHistoryJSON.Tuple tup = new ServiceHistoryJSON.Tuple(key, totalInst[i], val-totalInst[i]); // add installed amount
            shJSON.parts.add(tup);
            i++;
        }
        
        System.debug('Before serialize: ' + shJSON);
        String jsonStr = JSON.serialize(shJSON);
        System.debug('JSON: ' + jsonStr);
        return jsonStr;
    }
    
    // No values exist
    public static string noJSON(String pt) {
        ServiceHistoryJSON shJSON = new ServiceHistoryJSON();
        shJSON.TotalQuantity = 0; 
        shJSON.Type = pt;
        shJSON.UniqueParts = 0;
        shJSON.parts = new List<ServiceHistoryJSON.Tuple>();
        System.debug('Before serialize: ' + shJSON);
        String jsonStr = JSON.serialize(shJSON);
        System.debug('JSON: ' + jsonStr);
        return jsonStr;
    }
    
    @HttpPost
    global static void postRecord() {
        system.debug(RestContext.response);
        RestContext.response.addHeader('Content-Type', 'application/json');
        JSONGenerator gen = JSON.createGenerator(false);
        Boolean noSN = false;
        
        Service_History container = (Service_History)System.JSON.deserialize(RestContext.request.requestBody.toString(),
                                                                             Service_History.class);
        
        Service_History__c newRec = container.Service_History;
        System.debug('New record: ' + newRec);
        // Note: try with valid serial, if not catch and return error that serial doesn't exist
        try {
            // Find valid serial
            // Example serial model num: 'RO18200X-9CDL18NZ', its the serial + model num (separated by -)  
            try {
                system.debug(' ***'+container.Service_History.Serial__c);
                if (container.Service_History.Serial__c != '') {
                    Asset relAsset = [SELECT Id FROM Asset WHERE Serial_Number_Model_Number__c = :container.Service_History.Serial__c];
                    newRec.Serial_Number__c = relAsset.Id;
                    system.debug(relAsset+' ***'+container.Service_History.Serial__c);
                }
            } catch (Exception e) {
                // Create new SN if one doesn't exist
                Asset sn = new Asset();
                Account acc = [SELECT Id FROM Account WHERE Name = 'Gardner Denver' LIMIT 1];
                
                sn.Name = newRec.Serial__c;
                sn.Current_Servicer__c = acc.Id;
                sn.AccountId = acc.Id;
                sn.Serial_Number_Model_Number__c = newRec.Serial__c;
                sn.Inventory_Oil_Filter__c = 0;
                sn.Inventory_Air_Filter__c = 0;
                sn.Inventory_Cabinet_Filter__c = 0;
                sn.Inventory_Control_Box_Filter__c = 0;
                sn.Inventory_Lubricant__c = 0;
                sn.Inventory_Separator__c = 0;
                
                insert sn;
                newRec.Serial_Number__c = sn.Id;
                
                System.debug('Inserted sn... ' + sn);
                noSN = true;
            }                             
            
            // Parse filters/parts into JSON
            List<String> lstAirFilter = null;
            List<String> lstOilFilter = null;
            List<String> lstMaintKit = null;
            List<String> lstCabFilter = null;
            List<String> lstSep = null;
            List<String> lstCtrlFilter = null;
            List<String> lstLube = null;
            try {
                if (container.Service_History.Air_Filter__c != null && container.Service_History.Air_Filter__c != '') {
                    lstAirFilter = container.Service_History.Air_Filter__c.split(',');
                    String json = getJSON(lstAirFilter, 'AIR');
                    newRec.AirFilterJSON__c = json;
                } else {
                    String json = noJSON('AIR');
                    newRec.AirFilterJSON__c = json;
                }
                if (container.Service_History.Oil_Filter__c != null && container.Service_History.Oil_Filter__c != '') {
                    lstOilFilter = container.Service_History.Oil_Filter__c.split(',');
                    String json = getJSON(lstOilFilter, 'OIL');
                    newRec.Oil_Filter_JSON__c = json;
                } else {
                    String json = noJSON('OIL');
                    newRec.Oil_Filter_JSON__c = json;
                }
                if (container.Service_History.Maintenance_Kit__c != null && container.Service_History.Maintenance_Kit__c != '') {
                    lstMaintKit = container.Service_History.Maintenance_Kit__c.split(',');
                    String json = getJSON(lstMaintKit, 'MAINT');
                    newRec.Maintenance_Kit_JSON__c = json;
                } else {
                    String json = noJSON('MAINT');
                    newRec.Maintenance_Kit_JSON__c = json;
                }
                if (container.Service_History.Cabinet_Filter__c != null && container.Service_History.Cabinet_Filter__c != '') { 
                    lstCabFilter = container.Service_History.Cabinet_Filter__c.split(',');
                    String json = getJSON(lstCabFilter, 'CABINET');
                    newRec.Cabin_Filter_JSON__c = json;
                } else {
                    String json = noJSON('CABINET');
                    newRec.Cabin_Filter_JSON__c = json;
                }
                if (container.Service_History.Separator__c != null && container.Service_History.Separator__c != '') { 
                    lstSep = container.Service_History.Separator__c.split(',');
                    String json = getJSON(lstSep, 'SEP');
                    newRec.Separator_JSON__c = json;
                } else {
                    String json = noJSON('SEP');
                    newRec.Separator_JSON__c = json;
                }
                if (container.Service_History.Control_Box_Filter__c != null && container.Service_History.Control_Box_Filter__c != '') { 
                    lstCtrlFilter = container.Service_History.Control_Box_Filter__c.split(',');
                    String json = getJSON(lstCtrlFilter, 'CONTROL');
                    newRec.Control_Filter_JSON__c = json;
                } else {
                    String json = noJSON('CONTROL');
                    newRec.Control_Filter_JSON__c = json;
                }
                if (container.Service_History.Lubricant__c != null && container.Service_History.Lubricant__c != '') { 
                    lstLube = container.Service_History.Lubricant__c.split(',');
                    String json = getJSON(lstLube, 'LUBE');
                    newRec.Lube_JSON__c = json;
                } else {
                    String json = noJSON('LUBE');
                    newRec.Lube_JSON__c = json;
                }
            } catch(Exception e) {
                System.debug('Error: ' + e.getMessage());
            }
            
            System.debug('Inserting new SH record... ');
            insert newRec; 

            // Parse attachments
            try {
                List<Attachment> att = new List<Attachment>();
                integer cnt = 1;
                for(Attach at : container.attachments) {
                    att.add(
                        new Attachment(parentId = newRec.Id, name = at.name, ContentType = at.ContentType, 
                                       Body = EncodingUtil.base64Decode(at.body)));
                    cnt++;
                }
                insert att;
            } catch (Exception e) {
                gen.writeStartObject();
                gen.writeNumberField('statusCode', 404);
                gen.writeStringField('error', 'Error parsing attachments: '+e.getMessage());
                gen.writeEndObject();
                RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
            }
            
            String record = 'Message successfully received, record added. Record Id: ' + newRec.Id;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(record));
            
            if (noSN) {
                // Simply return for now, we cannot run the logic without an mvpl
                gen.writeStartObject();
                gen.writeNumberField('statusCode', 200);
                gen.writeStringField('message', 'Service History Record & Serial Number submitted. Once a Machine Version Parts List is attached to Serial compliance will be evaluated.');
                gen.writeEndObject();
                RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
                return;
            }
            
        } catch (Exception e) {
            System.debug('error: ' + e.getMessage());
            gen.writeStartObject();
            gen.writeNumberField('statusCode', 404);
            gen.writeStringField('error', 'There was an error. Record could not be posted: '+e.getMessage());
            gen.writeEndObject();
            RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
            system.debug(RestContext.response.responseBody+ '***** '+gen.getAsString());
        }
    }
}