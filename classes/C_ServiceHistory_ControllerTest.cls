@isTest
public class C_ServiceHistory_ControllerTest {
	@isTest static void testReadOnly()
    {
        C_ServiceHistory_Controller.getIsReadOnly();
    }
    
    @isTest static void testGetAsset()
    {
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        asset serial1 = new asset(name = 'S483816-EDE99N', accountid =acct.id,Serial_Number_Model_Number__c='S483816-EDE99N',Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0);
        insert serial1;
        C_ServiceHistory_Controller.queryAsset(serial1.id);
    }
    
    @isTest static void testgetServiceHistory()
    {
        Machine_Version_Parts_List__c mvpl = new Machine_Version_Parts_List__c(Oil_Filter_Quantity_Expected__c=1);
        insert mvpl;
        
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Asset serial1 = new Asset(name = 'S483816-EDE99N', accountid = acct.id,Serial_Number_Model_Number__c='S483816-EDE99N',Machine_Version_Parts_List__c=mvpl.id,
                                 Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0);
        insert serial1;
        
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Hour_Overage_Threshold_Percent__c = .10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        insert cs;
        
        Service_History__c sh = new Service_History__c(Date_of_Service__c=date.valueOf('2018-01-30'),Serial_Number_MD__c =serial1.id,Serial_Number__c =serial1.id,Current_Hours__c=23650,Hours_Loaded__c= 0,
                Hours_Unloaded__c=0,Gallons_of_Lubricant__c= 0,Separator__c='202ECH6013:T',Oil_Filter__c='2116110:T, 2116110:T, 2116110:F',Air_Filter__c='2118315:T',
                Other_Parts__c= '',X10_Yr_Warranty_Kit__c='',Maintenance_Kit__c='',Customer_Feedback__c='This is only test feedback from a customer',
                Service_Notes__c='Here are some service notes',Oil_Sample_Taken__c=true,Error_Overrides__c= 'Some generated errors are going here.', account__C=acct.id);
        insert sh;
        
        C_ServiceHistory_Controller.queryServiceHistory(sh.id);
    }
    
    @isTest static void testSaveAsset()
    {
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
     	Asset serial1 = new Asset(name = 'S483816-EDE99N', accountid = acct.id,Serial_Number_Model_Number__c='S483816-EDE99N',
                                 Oil_Filter_Next_Expected_Date__c= system.today(),Hours_Oil_Filter__c=0);
        insert serial1;
        
        C_ServiceHistory_Controller.saveAsset(serial1);
        
        //serial1.AccountId='';
        //serial1.name='';
        
        //C_ServiceHistory_Controller.saveAsset(serial1);
        
    }
}