public with sharing class UTIL_CPQ_Setup
{
    public String customScriptName = 'enosix_sap_simulation';
    public String staticresourceCodeName = 'enosix_sap_simulation';
    public String staticresourceTranspiledCodeName = 'enosix_sap_simulation_transpiled';

    public void installCustomScript()
    {
        Map<String,Object> config = new Map<String,Object>{
            'DEBUG' => true,
            'enosixSapSimulationApexService' => '/ensxCPQQuoteCalculationService',
            'quoteSimulationEnabledField' => 'FLD_enosixPricingSimulationEnabled__c',
            'instanceUrl' => URL.getOrgDomainUrl().toExternalForm().toLowercase()
        };

        upsertCustomScript(
            customScriptName,
            applyConfig(UTIL_StaticResource.getStringResourceContents(staticresourceCodeName),config),
            applyConfig(UTIL_StaticResource.getStringResourceContents(staticresourceTranspiledCodeName),config)
        );
    }

    private String applyConfig(String code, Map<String,Object> config)
    {
        return code.replace('{/*CONFIG*/}', JSON.serialize(config));
    }

    private void upsertCustomScript(String customScriptName, String code, String transpiledCode)
    {
        SBQQ__CustomScript__c customScript = new SBQQ__CustomScript__c(
            Name = customScriptName
        );
        // obtain existing record if it exists
        List<SBQQ__CustomScript__c> existingCustomScript = [SELECT Id FROM SBQQ__CustomScript__c WHERE Name = :customScriptName];
        if (existingCustomScript.size() > 0) {
            customScript = existingCustomScript.get(0);
        }
        customScript.SBQQ__Code__c = code;
        customScript.SBQQ__TranspiledCode__c = transpiledCode;
        customScript.SBQQ__QuoteFields__c = 'FLD_enosixPricingSimulationEnabled__c';
        upsert customScript;
    }
}