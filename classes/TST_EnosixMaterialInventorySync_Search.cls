/// enosiX Inc. Generated Apex Model
/// Generated On: 6/27/2019 9:41:46 AM
/// SAP Host: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class TST_EnosixMaterialInventorySync_Search
{

    public class MockSBO_EnosixMaterialInventorySync_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMaterialInventorySync_Search.class, new MockSBO_EnosixMaterialInventorySync_Search());
        SBO_EnosixMaterialInventorySync_Search sbo = new SBO_EnosixMaterialInventorySync_Search();
        System.assertEquals(SBO_EnosixMaterialInventorySync_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC sc = new SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC();
        System.assertEquals(SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS childObj = new SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS();
        System.assertEquals(SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);

        childObj.CustomerNumber = 'X';
        System.assertEquals('X', childObj.CustomerNumber);


    }

    @isTest
    static void testEnosixMaterialInventorySync_SR()
    {
        SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR sr = new SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR();

        sr.registerReflectionForClass();

        System.assertEquals(SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT childObj = new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();
        System.assertEquals(SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT_COLLECTION childObjCollection = new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.Material = 'X';
        System.assertEquals('X', childObj.Material);

        childObj.MaterialDescription = 'X';
        System.assertEquals('X', childObj.MaterialDescription);

        childObj.MaterialFamily = 'X';
        System.assertEquals('X', childObj.MaterialFamily);

        childObj.Stock = 1.5;
        System.assertEquals(1.5, childObj.Stock);

        childObj.Scheduled = 1.5;
        System.assertEquals(1.5, childObj.Scheduled);

        childObj.UOM = 'X';
        System.assertEquals('X', childObj.UOM);

        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);


    }

}