@isTest
public class C_SerialNumAccUpdateBatchTest {
    
    @testSetup
    public static void runSetup()
    {
        // create sales district users
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(alias = 'jfelty', email='jfelty@acme.com.yi', 
                          emailencodingkey='UTF-8', firstname='Jim', lastname='Felty', 
                          languagelocalekey='en_US', Sales_District_Code__c='30',
                          localesidkey='en_US', profileid = p.Id,
                          timezonesidkey='America/Los_Angeles', 
                          ManagerId=UserInfo.getUserId(),
                          username='jfelty@acme.com.yi');
        insert u1;
        User u2 = new User(alias = 'jjanssen', email='jjanssen@acme.com.yi', 
                          emailencodingkey='UTF-8', firstname='Joe', lastname='Janssen', 
                          languagelocalekey='en_US', Sales_District_Code__c='L3',
                          localesidkey='en_US', profileid = p.Id,
                          timezonesidkey='America/Los_Angeles', 
                          ManagerId=UserInfo.getUserId(),
                          username='jjanssen@acme.com.yi');
        insert u2;
        
        
        // parent account for serial number owner
        Account acc1 = new Account(
            Name='Acc1',
            Account_Number__c='104382'
        );
        insert acc1;
        
        // sub account parent account for serial number owner
        Account acc2 = new Account(
            Name='Acc2',
            ParentId=acc1.Id,
            Account_Number__c='104382 - CM',
            Sales_district__c='30 - Felty, Jim'
        );
        insert acc2;
        
        // parent account for current servicer owner
        Account acc3 = new Account(
            Name='Acc3',
            Account_Number__c='105381'
        );
        insert acc3;
        
        // sub account parent account for current servicer owner
        Account acc4 = new Account(
            Name='Acc4',
            ParentId=acc3.Id,
            Account_Number__c='105381 - CM',
            Sales_district__c='L3 - Janssen, Joe'
        );
        insert acc4;
        
        Asset sn = new Asset(
            Name='DM006400',
            DChl__c='CM',
            AccountId=acc1.Id,
            Current_Servicer__c=acc3.Id
        );
        insert sn;
        
    }
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    @isTest
    public static void runTests()
    {
        Test.startTest();
        String jobIdD = System.schedule('ScheduledApexTestD',
                                      CRON_EXP, 
                                      new C_SerialNumAccUpdateBatchDSch());  
        Id batchIdD = Database.executeBatch(new C_SerialNumAccUpdateBatch('D'));
        String jobIdW = System.schedule('ScheduledApexTestW',
                                      CRON_EXP, 
                                      new C_SerialNumAccUpdateBatchWSch());  
        Id batchIdW = Database.executeBatch(new C_SerialNumAccUpdateBatch('W'));
        Test.stopTest();
    }
}