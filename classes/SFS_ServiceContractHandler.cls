/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 22/09/2021
* @description: SFS_ServiceContractHandler Apex Handler.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001     SIF-34  22/09/2021  Apex Handler to call PolygonUtils Apex class to get ServiceTerritoryId
Bhargavi Nerella		M-002	  SIF-59  22/10/2021  assignOwner Method to route Service Contract to a user or a queue based on Mapping Rule
Sucharitha Suragala     M-003     SIF-65  09/11/2021  When Closing ServiceAgreement, WorkOrder Status and WOLI Status must be completed(Validation) 
============================================================================================================================================================*/
public class SFS_ServiceContractHandler {
    //Method to call callPolygonUtils to get ServiceTerritory Id
    public static void callPolygonUtils(Map<Id,ServiceContract> sContractOldMap,List<ServiceContract> sContractNew){
        
        try{            
            Set<Id> accId = new Set<Id>();
            Id serviceTerritoryId;
            Map<Id,Id> serviceTerritoryMap = new Map<Id,Id>();
            for(ServiceContract sc: sContractNew ){
                accId.add(sc.AccountId);
            }
            if(accId != null){
                List<Account>  acc = [Select Id, Name, ShippingLatitude,ShippingLongitude from Account where Id IN: accId];
                for(Account acct : acc){
                    Double longitude = double.valueOf(acct.ShippingLongitude);
                    Double latitude= double.valueOf(acct.ShippingLatitude);
                    serviceTerritoryId = FSL.PolygonUtils.getTerritoryIdByPolygons(longitude,latitude);
                    serviceTerritoryMap.put(acct.Id,serviceTerritoryId);
                }                
            }
            for(ServiceContract st: sContractNew ) {
                Id serviceTerrId = serviceTerritoryMap.get(st.AccountId);
                if(serviceTerrId != null)
                    st.SFS_Service_Territory__c = serviceTerritoryId;
            }
        }
        catch(Exception e){
            system.debug('@@@exception'+e.getMessage());
        }
    }
    
    
    //Logic to invoke batch class to create Invoices based on the Service Agreement Status and Invoice Frquency
    public static void callbatchClassToCreateInvoices(List<ServiceContract> sContractList,Map<Id,ServiceContract> sContractoldMap){
        try{
            Boolean error=false;
            Set<ID> sAIds = new Set<ID>();	
            Map<ID,Schema.RecordTypeInfo> scRecMap = ServiceContract.sObjectType.getDescribe().getRecordTypeInfosById();
            
            for(ServiceContract sc:sContractList)
            {    
              String recName = scRecMap.get(sc.RecordTypeId).getName();
              if(recName != 'Advanced Billing' && recName != 'Contracting' && recName != 'Preventive Maintenance' && sc.SFS_CPQ_Transaction_Id__c==Null){
                  if(sContractOldMap!=null){
                       ServiceContract oldsc = sContractOldMap.get(sc.Id);
                         if((sc.SFS_Status__c=='APPROVED' &&  sc.SFS_Status__c!=oldsc.SFS_Status__c  &&  sc.SFS_Invoice_Frequency__c!=null) ||
                              (sc.SFS_Status__c=='APPROVED' && sc.SFS_Invoice_Frequency__c!= oldsc.SFS_Invoice_Frequency__c )){
                               sAIds.add(sc.Id);    
                            }
                    
                    }
                   else if(sc.SFS_Status__c=='APPROVED' && sc.SFS_Invoice_Frequency__c!=null){
                    sAIds.add(sc.Id);
                   }
               }   
            }             
            if(!sAIds.isEmpty()){
               /* SFS_batchClassToCreateRecurringCharge chargeBatch = new SFS_batchClassToCreateRecurringCharge(sAIds);
                Id processedBatch = Database.executeBatch(chargeBatch);*/ //commented for code deployment- manimozhi(10/14)
            }
        }
        catch(Exception e){
        }
    }
    
    public static void assignOwner(List<ServiceContract> sContractNew){
        
        try{
            Account acc=new Account();
            if(sContractNew.size()>0 && sContractNew[0].SFS_Assign_Service_Agreement__c==true){
                acc=[Select Id,Name,Account_Division__c from Account where Id=:sContractNew[0].AccountId];
            }
            SFS_Mapping_Rule__c mappingRules= new SFS_Mapping_Rule__c();            
            if(acc.Account_Division__c!=null){
                mappingRules=[Select Id,Name,SFS_Ownership_Type__c,SFS_Ownership_Value__c,SFS_Division__c from SFS_Mapping_Rule__c
                              where SFS_Division__c=:acc.Account_Division__c];
            }            
            if(mappingRules.SFS_Ownership_Type__c=='Queue'){
                Group getQueueDetails=[SELECT Id,DeveloperName,Type FROM Group 
                                       WHERE Type = 'Queue' AND DeveloperName=:mappingRules.SFS_Ownership_Value__c];
                sContractNew[0].OwnerId=getQueueDetails.Id;
            }else if(mappingRules.SFS_Ownership_Type__c=='User'){
                User getUserDetails=[Select id,name,username from User where username=:mappingRules.SFS_Ownership_Value__c];
                sContractNew[0].OwnerId=getUserDetails.Id;
            }
            Update sContractNew;
        }catch(Exception e){
        }
    }
    
	public static void updateChargeType(Map<Id,ServiceContract> sContractOldMap,List<ServiceContract> sContractNew){
        try{
            Set<Id> idContract = new Set<Id>();
            List<CAP_IR_Charge__c> ch = new List<CAP_IR_Charge__c>();
            List<CAP_IR_Charge__c> chList = new List<CAP_IR_Charge__c>();
            Map<Id,String> chargeMap=new Map<Id,String>(); 
            Map<ID,Schema.RecordTypeInfo> rt_Map = ServiceContract.sObjectType.getDescribe().getRecordTypeInfosById();
            String stRecordType = '', stType = '';
            
            for(ServiceContract sc: sContractNew){
                  ServiceContract Oldcontract = sContractOldMap.get(sc.id);              
                  if(Oldcontract.RecordTypeId!=sc.RecordTypeId){    
                     stRecordType = rt_Map.get(sc.RecordTypeId).getName();
                      
                     ch = [SELECT Id,CAP_IR_Type__c From CAP_IR_Charge__c where CAP_IR_Service_Contract__c  =: sc.Id];                     
                      if(ch.size()>0 && (stRecordType == 'PackageCARE' || stRecordType == 'Rental')){
                     	stType = 'Recurring'; 
                      } 
                      else if(ch.size()>0 && (stRecordType == 'PartsCARE' || stRecordType == 'PlannedCARE' || stRecordType == 'Advanced Billing' || stRecordType == 'Contracting')){
                      	stType = 'Milestone'; 
                      }
                      
                      for(CAP_IR_Charge__c chargeColl:ch){
                          chargeColl.CAP_IR_Type__c = stType;
                      }
                  }
              }
            
            if (ch != null) {
                update ch;
            }
        }
        catch(Exception e){
        }
    }
    
        Public static void checkProductCode(List<ServiceContract> sc,Map<Id,ServiceContract> newMap){
        try{
            Boolean error=false;
            Set<Id> AgrIds=new Set<Id>();
            for(ServiceContract scIds:sc){
                AgrIds.add(scIds.Id);
            }
            Map<Id,List<Entitlement>> entMap=new Map<Id,List<Entitlement>>();
            Map<Id,Boolean> AgrMap=new Map<Id,Boolean>();
            List<ContractLineItem> cLineTemList=new List<ContractLineItem>();
            if(AgrIds.size()>0){
                cLineTemList=[select id,SFS_Product_Code__c,ServiceContractId,
                        (select id,Asset.SFS_Product_Code__c from entitlements) from ContractLineItem where ServiceContractId IN:AgrIds];
            }
            if(cLineTemList.size()>0){
                For(ContractLineItem record:cLineTemList){
                    entMap.put(record.Id,record.entitlements);
                }
            }
            if(entMap.size()>0){
                for(ContractLineItem clList:cLineTemList){                    
                    List<Entitlement> entitlementList = new List<Entitlement>();
                    entitlementList=entMap.get(clList.Id);
                    if(entitlementList.size()>0){
                        for(Entitlement ent:entitlementList){
                        	if(ent.Asset.SFS_Product_Code__c!=clList.SFS_Product_Code__c){
                            	AgrMap.put(clList.ServiceContractId,TRUE);
                        	}
                    	}
                    }
            	}
            }
            if(AgrMap.size()>0){
                for(ServiceContract scError:sc){
                    System.debug('Checkerror:'+AgrMap.get(scError.Id));
                    if(AgrMap.get(scError.Id))
                        scError.addError('One or multiple Entitlement asset product codes does not match with Service Agreement Line Item product code');
                }
            }
        }
        catch(Exception e){}
    }
}