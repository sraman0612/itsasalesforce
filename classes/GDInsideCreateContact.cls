public class GDInsideCreateContact implements Queueable{
    final List<Contact> cons;
    
    public GDInsideCreateContact(List<contact> cons)
    {
        this.cons = cons;
    }
    
    public void execute(QueueableContext context) {
        Database.SaveResult[] srList = Database.insert(cons);
        for(Database.SaveResult sr : srList)
        {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted account. contact ID: ' + sr.getId());
            }
            
        }
    }
}