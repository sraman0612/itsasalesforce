/**
 * Created by OlhaHulenko on 19/09/2023.
 */
@IsTest
public class SoldToAccountsCtrlTest {
    static final Id ACC_BILL_TO_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
    static final Id OPP_CTS_AIRD_RECORDTYPE = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_AIRD_Opportunity').getRecordTypeId();
    @TestSetup
    public static void createTestData() {
        String priceList = 'ITS_PRIMARY';
        Account testAccBillTo = new Account(Name = 'Test Account Bill To', BillingCountry = 'USA', ShippingCountry='USA',
                ShippingState='NC',ShippingCity='Davidson', ShippingStreet = '800D Betty Street', ShippingPostalCode='6380',
                County__c='Davidson',RecordTypeId = ACC_BILL_TO_RECORDTYPE, PriceList__c = priceList);
        insert testAccBillTo;
        Account accBillTo = [SELECT Id, Name, RecordType.DeveloperName FROM Account WHERE Name = 'Test Account Bill To' LIMIT 1];

        Account testAcc = new Account(Name = 'Test Account', Bill_To_Account__c = accBillTo.Id, BillingCountry = 'Canada',
                ShippingCountry='USA', ShippingState='ONTARIO',ShippingCity='PORT STANLEY', ShippingStreet = '400D Betty Street', ShippingPostalCode='N5L 1J4',
                County__c='test', RecordTypeId = ACC_BILL_TO_RECORDTYPE,  Type = 'Sold To');
        insert testAcc;

        Id pricebookId = Test.getStandardPricebookId();
        System.debug(testAcc);
        Opportunity opp = new Opportunity(AccountId = testAcc.Id, Name = 'test opp', Amount = 1000, CloseDate = Date.today(),
                StageName = 'Idea', Pricebook2Id = pricebookId, CTS_BillToAccount__c = accBillTo.Id, RecordTypeId = OPP_CTS_AIRD_RECORDTYPE);
        insert opp;
    }

    @IsTest
    static void fetchAcctsTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        List<Account> accList = new List<Account>();
        Test.startTest();
        accList = SoldToAccountsCtrl.fetchAccts(opp.Id);
        Test.stopTest();
        System.assertEquals(1,accList.size());
    }

    @IsTest
    static void getRecordTypeNameTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        String oppRecType = '';
        Test.startTest();
        oppRecType = SoldToAccountsCtrl.getRecordTypeName(opp.Id);
        Test.stopTest();
        System.debug('oppRecType:'+oppRecType);
        System.assertNotEquals(null,oppRecType);
    }

    @IsTest
    static void setSoldToAccountTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        String result = '';
        Test.startTest();
        result = SoldToAccountsCtrl.setSoldToAccount(opp.Id, acc.Id);
        Test.stopTest();
        System.debug('result:'+result);
        System.assertNotEquals(null,result);
    }

}