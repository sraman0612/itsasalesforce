/*=========================================================================================================
* @author Manimozhi B, Capgemini
* @date 28/4/2022
* @description: Apex Handler class for SFS_ShipmentTrigger
* @Story Number: SIF-827

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

===============================================================================================================================*/
// to rollup the freight charge values from shipmentitem to PRLI
public class SFS_ShipmentItemTriggerHandler {
    public static void rollUpFreightCharge(List<ShipmentItem> shipmentItemList){
        Set<Id> prliIds = new Set<Id>();
        Map<Id,ProductRequestLineItem> prliToUpdate =new Map<Id,ProductRequestLineItem>();
        List<Id> prliList=new List<Id>();
        if(shipmentItemList.size()>0){
            for(ShipmentItem sItem : shipmentItemList){
                prliIds.add(sItem.SFS_Product_Request_Line_Item__c);
            } 
        }
        prliList.addAll(prliIds);
        Map<Id,ProductRequestLineItem> prliMap = new Map<Id,ProductRequestLineItem>([Select Id,SFS_Freight_Charge__c from ProductRequestLineItem where Id IN:prliList]);
        map<Id,ProductRequestLineItem> prlItemMap =new Map<Id,ProductRequestLineItem>([Select Id,SFS_Freight_Charge__c,(Select Id,ShipmentId,SFS_Product_Request_Line_Item__c,SFS_Freight_Charges__c,SFS_Status__c from Shipment_Items__r where SFS_Product_Request_Line_Item__c IN:prliList ) from ProductRequestLineItem where Id IN:prliList]);
        for(ShipmentItem shipitemList: shipmentItemList ){  
            for(Id prliId:prliList ){                
                if(prliId!=null || prliId!=''){
                    for(ProductRequestLineItem prliItem : prlItemMap.values()){
                        if(prliItem.Id==prliId){
                            Decimal rollUp=0.00;                           
                            for(ShipmentItem shpItem:prliItem.Shipment_Items__r){                                
                                if(shpItem.SFS_Product_Request_Line_Item__c==shipitemList.SFS_Product_Request_Line_Item__c){                                    
                                    if(shpItem.SFS_Freight_Charges__c !=Null){                                        
                                        rollUp += shpItem.SFS_Freight_Charges__c; 
                                        prliMap.get(prliItem.Id).SFS_Freight_Charge__c =rollUp; 
                                        prliToUpdate.put(prliItem.Id,prliMap.get(prliItem.Id));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(prliToUpdate.size()>0){            
            Update prliToUpdate.values();         
        }
    }        
    
}