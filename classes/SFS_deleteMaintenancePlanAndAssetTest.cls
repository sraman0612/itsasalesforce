/*=========================================================================================================
* @author : Geethanjali SP
* @description: Test Class for SFS_deleteMaintenancePlanAndAsset
*/
@isTest
public class SFS_deleteMaintenancePlanAndAssetTest {  
    @testSetup static void setup(){
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='1222';
        accountList[1].IRIT_Customer_Number__c='1212';
        accountList[1].RecordTypeId=rtAcc;
        accountList[1].IRIT_Payment_Terms__c='NET 30';
        insert accountList;
        accountList[0].Bill_To_Account__c=accountList[1].Id;
        update accountList;
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
        insert svcAgreementList[0];
        
        List<ContractLineItem> lineItemList=SFS_TestDataFactory.createServiceAgreementLineItem(1,svcAgreementList[0].Id,false);
        insert lineItemList[0];
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1,false);
        insert assetList[0];
        
        List<Entitlement> entitlementList=SFS_TestDataFactory.createEntitlement(1,accountList[1].Id,assetList[0].Id,svcAgreementList[0].Id,lineItemList[0].Id,false);
        insert entitlementList[0];
        
        List<MaintenancePlan> maintenancePlanList=SFS_TestDataFactory.createMaintenancePlan(1,svcAgreementList[0].Id,false);
        insert maintenancePlanList[0];
        
        List<MaintenanceAsset> maintenanceAssetList=SFS_TestDataFactory.createMaintenanceAsset(1,maintenancePlanList[0].Id,assetList[0].Id,false);
        maintenanceAssetList[0].SFS_Service_Agreement_Line_Item__c = lineItemList[0].Id;
        maintenanceAssetList[0].SFS_Entitlement__c = entitlementList[0].Id;
        insert maintenanceAssetList[0];        
        
    }
    Static testmethod void testDeleteMaintenancePlanAndAsset(){
        
        Test.startTest();
        
        List<ContractLineItem> lineItemList=[Select Id from ContractLineItem];
        List<Id> MPAIds=new List<Id>();
        MPAIds.add(lineItemList[0].Id);
        
		SFS_deleteMaintenancePlanAndAsset.deleteMaintenancePlanAndAsset(MPAIds);
        
        Test.stopTest();
    }
    
    Static testmethod void testDeleteMaintenancePlanAndAsset1(){
        
        Test.startTest();
        
        List<ServiceContract> svcAgreementList=[Select Id from ServiceContract];
        List<Id> MPAIds=new List<Id>();
        MPAIds.add(svcAgreementList[0].Id);
        
		SFS_deleteMaintenancePlanAndAsset.deleteMaintenancePlanAndAsset(MPAIds);
        
        Test.stopTest();
    }
    
    Static testmethod void testDeleteMaintenancePlanAndAsset2(){
        
        Test.startTest();
        
        List<Entitlement> entitlementList=[Select Id from Entitlement];
        List<Id> MPAIds=new List<Id>();
        MPAIds.add(entitlementList[0].Id);
        
		SFS_deleteMaintenancePlanAndAsset.deleteMaintenancePlanAndAsset(MPAIds);
        
        Test.stopTest();
    }
}