@isTest
public with sharing class EditAccountHierarchy_Test {
    public static testMethod void doTest() {
    	// Create some data
    	Account blueSun = new Account( Name = 'Blue Sun Corp' );
    	insert blueSun;
    	Account serenity = new Account( Name = 'Serenity' );
    	insert Serenity;
    	Account badger = new Account( Name = 'Badger', ParentId = blueSun.Id );
    	insert badger;
    	
    	// do some stuff
    	PageReference pageRef = page.EditAccountHierarchy;
    	Test.setCurrentPageReference( pageRef );
    	EditAccountHierarchyExt theExt = new EditAccountHierarchyExt( new ApexPages.standardController( badger ) );
    	EditAccountHierarchyExt.getAccounts();
    	theExt.cancelEdit();
    	theExt.saveRecord();
    	    	
    	// Validate
    	// 100% coverage.  I'm not changing a thing.
    }
}