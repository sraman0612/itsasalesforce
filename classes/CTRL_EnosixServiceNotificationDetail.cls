public with sharing class CTRL_EnosixServiceNotificationDetail {
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_EnosixServiceNotificationDetail.class);

    public static SBO_EnosixSO_Detail.EnosixSO simulateOrder(SBO_EnosixSO_Detail.EnosixSO salesOrder) {
        logger.enterAura('simulateOrder', new Map<String, Object> {
            'salesOrder' => salesOrder
        });

        SBO_EnosixSO_Detail sbo = new SBO_EnosixSO_Detail();

        try {
            return sbo.command('CMD_SIMULATE_ORDER', salesOrder);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    public static SBO_EnosixSO_Detail.EnosixSO saveSalesOrder(SBO_EnosixSO_Detail.EnosixSO salesOrder) {
        logger.enterAura('simulateOrder', new Map<String, Object> {
            'salesOrder' => salesOrder
        });

        SBO_EnosixSO_Detail sbo = new SBO_EnosixSO_Detail();

        try {
            return sbo.save(salesOrder);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    public static SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate updateServiceNotification(SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate serviceNotificationUpdate) {
        logger.enterAura('updateServiceNotification', new Map<String, Object> {
            'serviceNotificationUpdate' => serviceNotificationUpdate
        });

        SBO_EnosixServNotifUpdate_Detail sbo = new SBO_EnosixServNotifUpdate_Detail();

        try {
            return sbo.save(serviceNotificationUpdate);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    @AuraEnabled
    public static SBO_EnosixServiceNotification_Detail.EnosixServiceNotification createServiceNotificationImpl(SBO_EnosixServiceNotification_Detail.EnosixServiceNotification notification) {
        logger.enterAura('createServiceNotificationImpl', new Map<String, Object> {
            'notification' => notification
        });

        SBO_EnosixServiceNotification_Detail sbo = new SBO_EnosixServiceNotification_Detail();

        try {
            return sbo.save(notification);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    @AuraEnabled
    public static SBO_EnosixServiceNotification_Detail.EnosixServiceNotification verifyServiceNotification(SBO_EnosixServiceNotification_Detail.EnosixServiceNotification notification) {
        logger.enterAura('verifyServiceNotification', new Map<String, Object> {
            'notification' => notification
        });

        SBO_EnosixServiceNotification_Detail sbo = new SBO_EnosixServiceNotification_Detail();

        try {
            return sbo.command('CMD_SERVICE_NOTN_CHECK', notification);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }


    @testVisible
    private static void dummy(SBO_EnosixServiceNotification_Detail.EnosixServiceNotification notification) {
        logger.enterAura('verifyServiceNotification', new Map<String, Object> {
            'notification' => notification
        });

        SBO_EnosixServiceNotification_Detail sbo = new SBO_EnosixServiceNotification_Detail();
        
        sbo = new SBO_EnosixServiceNotification_Detail();
        sbo = new SBO_EnosixServiceNotification_Detail();
        sbo = new SBO_EnosixServiceNotification_Detail();
        sbo = new SBO_EnosixServiceNotification_Detail();
        sbo = new SBO_EnosixServiceNotification_Detail();
    }
}