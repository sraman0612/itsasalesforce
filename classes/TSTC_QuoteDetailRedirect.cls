@isTest
private class TSTC_QuoteDetailRedirect
{
    @isTest
    static void test_ConstructorAndRedirect()
    {
        Test.startTest();
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'TEST OPP';
        opp.StageName = 'Closed';
        opp.CloseDate = Date.today();
        opp.ENSX_EDM__Quote_Number__c = '12345';
        TSTU_SFOpportunity.upsertOpportunity(opp);
        Test.setCurrentPageReference(new PageReference('Page.VFP_QuoteCreateUpdate'));
        Test.stopTest();
    }
}