/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SObjectUtilities {
    global SObjectUtilities(System.Type t) {

    }
    global Boolean doesSObjectHaveCustomerMapping(Id recordId) {
        return null;
    }
    global Boolean doesSObjectHaveMaterialMapping(Id sObjectId) {
        return null;
    }
    global String getMaterialNumberFieldFromSObjectId(Id sObjectId) {
        return null;
    }
    global String getMaterialNumberValuefromSObjectId(Id sObjectId) {
        return null;
    }
    global String getSAPCustomerNumberFieldfromsObject(Id sObjectId) {
        return null;
    }
    global String getSAPCustomerNumberFieldfromsObject(SObject sObjectType) {
        return null;
    }
    global String getSAPCustomerNumberFromsObjectId(Id sObjectId) {
        return null;
    }
    global Boolean isSObjectLinkedToCustomer(Id recordId) {
        return null;
    }
    global Boolean isSObjectLinkedToMaterial(Id sObjectId) {
        return null;
    }
    global void setSAPCustomerNumberFromsObjectId(Id sObjectId, String customerNumber) {

    }
}
