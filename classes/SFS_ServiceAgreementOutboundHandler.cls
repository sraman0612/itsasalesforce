/* Author: Ryan Reddish - Capgemini
* Date: 12/1/21
* Description: Outbound integration to CPI. Service Agreements and Service Agreement Line Items.
* MOD-001: Added logic to choose <STATE> or <PROVINCE> tag based on Account Country - Ryan Reddish
*/
public class SFS_ServiceAgreementOutboundHandler{
    @invocableMethod(label = 'Outbound ServiceAgreement' description = 'Send to CPI' Category = 'ServiceContract')
    public static void Run(List <ServiceContract> newServiceContracts){
        //Main Method
        String xmlString = '';
        Map<Double, String> SAxmlMap = new Map<Double, String>();
        Map<Double,String> CLIXMLMap = new Map<Double, String>();        
        TransformData(newServiceContracts, saXmlMap, xmlString);
        system.debug('SFS_ServiceAgreementOutboundHandler');
        
    }
    
    public static void TransformData(List <ServiceContract> newServiceContracts, Map<Double, String> SAXMLMap, String xmlString){    
        SFS_XML_Outbound_Mapping__mdt[] agreementMaps =[SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Child_Start__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                        FROM SFS_XML_Outbound_Mapping__mdt Order By SFS_Node_Order__c asc]; 
        /*for(ServiceContract sc : newServiceContracts[0]){
            for(String x : sc){
                if(x == 'SFS_Siebel_Id__c' && x != null){
                    
                }
            }
            
        }*/
        Set<String> xmlNodes = new Set<String>();
        ServiceContract sa = newServiceContracts[0];
        Division__c div = new Division__c();
        Account acc = new Account();
        List<ContractLineItem> cli = new List<ContractLineItem>();
        Account billTo = new Account();
        Map<String, Object> servAgMap = new Map<String, Object>();
        double dropNode = 0;
        try{
            /*sa = [SELECT ContractNumber, SFS_Oracle_Start_Date__c, SFS_Integration_Status__c, SFS_Integration_Response__c, Description, SFS_Oracle_End_Date__c, SFS_Oracle_Status__c, SFS_Oracle_Type__c, CurrencyIsoCode, SFS_Agreement_Value__c,SFS_PO_Number__c, AccountId, SFS_Division__c, SFS_Bill_To_Account__c
FROM ServiceContract
WHERE Id =: newServiceContracts[0].id];*/
            //System.debug('SA:: ' + sa);
            cli = [SELECT Id,EndDate, SFS_Product_Line_Formula__c,StartDate, SFS_Billing_Method__c,Product2Id,sfs_product_description__c,LineItemNumber,SFS_Ready_To_Bill__c,sfs_oracle_end_date__c,
                   SFS_Billable_Flag__c, SFS_Ready_To_Bill_Flag__c,SFS_Ready_To_Distribute__c, SFS_Chargeable_Flag__c, SFS_Oracle_Billing_Method__c, SFS_SALI_Oracle_Start_Date__c,sfs_external_id__c
                   FROM ContractLineItem
                   WHERE ServiceContractId =: sa.Id Order By CreatedDate ASC];
            for(ContractLineItem c : cli){
                System.debug('CLI Query: ' + c);
            }
        }Catch(System.Exception e){
            
        }
        
        try{
            div = [SELECT SFS_Org_Code__c, SFS_Employee_Number__c,SFS_External_System_Value__c, SFS_Category__c, SFS_Class_Code__c, SFS_Sales_Branch__c
                   FROM Division__c
                   WHERE Id =: SA.SFS_Division__c];
        }catch(System.Exception e){
            
        }
        try{
            acc = [SELECT irit_Customer_Number__c, Oracle_Site_Use_Id__c, SFS_Oracle_Site_Use_Id__c,cts_global_shipping_address_1__c,cts_global_shipping_address_2__c,County__c,
                   shippingcity, shippingstate, shippingcountry, shippingpostalcode,alias_name__c, SFS_Oracle_Country__c,Siebel_ID__c
                   FROM Account
                   WHERE Id =: SA.AccountId];
            System.debug('Account ' + acc.shippingcountry);
        }catch(System.Exception e){
            
        }
        
        
        
        //Checks if this Account is based in the US.
        
        String CountryPointer = '';
        if (acc.shippingcountry != 'USA' && acc.shippingCountry != 'US' && acc.ShippingCountry != 'United States') {
            SFS_XML_Outbound_Mapping__mdt state = SFS_XML_Outbound_Mapping__mdt.getInstance('SFS_Customer_State');  
            dropNode = double.ValueOf(state.SFS_Node_Order__c);
            //CountryPointer = String.ValueOf(state.SFS_XML_Full_Name__c);
        } else {
            SFS_XML_Outbound_Mapping__mdt prov = SFS_XML_Outbound_Mapping__mdt.getInstance('SFS_Customer_Province');  
            dropNode = double.ValueOf(prov.SFS_Node_Order__c);
        }
        
        try{
            billTo = [SELECT Id, Oracle_ID__c, SFS_Oracle_Site_Use_Id__c, irit_Customer_Number__c
                      FROM Account 
                      WHERE Id =: SA.SFS_Bill_To_Account__c];
        }catch(System.Exception e){
            
        }
        
        
        //Map Queries, then populate using getPopulatedFieldAsMap()
        
        servAgMap = sa.getPopulatedFieldsAsMap();
        
        //Map<String, List<Object>>cliMap = new Map<String, List<Object>>();
        Map<String,Object> cliMap = new Map<String,Object>();

        /*for(ContractLineItem clis : cli){

}*/
        
        
        
        Map<String,Object> divMap = new Map<String,Object>();
        divMap = div.getPopulatedFieldsAsMap();
        
        Map<String,Object> accMap = new Map<String,Object>();
        accMap = acc.getPopulatedFieldsAsMap();
        
        Map<String,Object> billMap = new Map<String,Object>();
        billMap = billTo.getPopulatedFieldsAsMap();
        
        //Loop through each map.keyset(), populate one map with all fieldname values from individual maps
        Map<String, Object> cliFieldValues = new Map<String, Object>();
        
        Boolean legacy = false;
        Map<String, Object> ScFieldValues = new Map<String, Object>();
        try{ 
            Map<String,Object> contractFieldStore = new Map<String,Object>();
            for(String field: servAgMap.keyset()){
                ScFieldValues.put(field.toLowerCase(), sa.get(field));
            }
            
            for(String field: divMap.keyset()){
                ScFieldValues.put(field.toLowerCase(), div.get(field));
            }
            for(String field: accMap.keyset()){
                ScFieldValues.put(field.toLowerCase(), acc.get(field));
            }
            for(String field: billMap.keyset()){
                
                if(field == 'irit_Customer_Number__c'){
                    
                    ScFieldValues.put('SFS_BillTo_Customer_Number', billTo.get(field));
                }
                else
                {
                ScFieldValues.put(field.toLowerCase(), billTo.get(field));
                }
            }
        }Catch(System.Exception e){
        }
        List<String> headerList =BuildXML(agreementMaps,ScFieldValues, 'Service Contract', dropNode);
        For(String s : headerList){
            System.debug('First transformation: ' + s);
        }
        
        List<String> FinalXML = new List<String>();
        //FinalXML = BuildXML(agreementMaps, ScFieldValues,'ContractLineItem');
        for(ContractLineItem c : cli){
            List<String> x = new List<String>();
            cliMap = c.getPopulatedFieldsAsMap();
            for(String field: cliMap.keySet()){
                cliFieldValues.put(field.toLowerCase(), cliMap.get(field)); 
                System.debug('CLIFIELDVALUES::: ' + field.toLowerCase() + ':::' + cliMap.get(field));
            }
            x = BuildXML(agreementMaps, cliFieldValues,'ContractLineItem', null);
            FinalXML.addAll(x);
            //System.debug('CLIFIELDVALUES::: ' + FinalXML);
            for(String t : finalXML){
                System.debug('CLI:  ' + t);
            }
        }
        //SFS_XML_Outbound_Mapping__mdt st = SFS_XML_Outbound_Mapping__mdt.getInstance('m171h000000Cdrk');
        //SFS_Parts_Used_XML_Structure__mdt et = SFS_Parts_Used_XML_Structure__mdt.getInstance('m171h000000Cdrp');
        String startTag ='<G_TASKS>';
        String endTag = '</G_TASKS>';
        String ChildXML = startTag; 
        for(String z : finalXML){
            ChildXML = ChildXML + z;
        }
        ChildXML = ChildXML + endTag;
        System.debug('HEADER IN LIST: ' + headerList);
        for(String s: headerList){
            xmlString = xmlString + s;
            System.debug('HEADERS: ' + s);
        }
        
        xmlString = xmlString.replace('<BEGINREPLACE>', ChildXML);
        system.debug('XML::::::' + xmlString);
        //SET FALSE IF NEEDING TO SEND TO ORACLE 
        
        String IdString = String.ValueOf(newServiceContracts[0].Id);
        String interfaceDetail = 'XXPA2381';
        String interfaceLabel = 'Service Agreement with SALI|' + IdString;
        if(!Test.isRunningTest()){
            SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel);
        }
       
        
        //SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel);
        System.debug('Interface Payload:   ' + xmlString);
        System.debug('Interface Detail:  '  + interfaceDetail);
        System.debug('Interface Label: ' + interfaceLabel);
    }
    public static List<String> BuildXML(SFS_XML_Outbound_Mapping__mdt[] agreementMaps, Map<String, Object> FieldMaps, String ObjectName, double dropNode){
        List<String> xmlList = new List<String>();
        List<Double> newNodeList = new List<Double>();
        for(SFS_XML_Outbound_Mapping__mdt xmlStructure : agreementMaps){
            if(xmlStructure.SFS_Node_Order__c != dropNode){
               
            
            newNodeList.add(xmlStructure.SFS_Node_Order__c);
            if(xmlStructure.SFS_Salesforce_Object__c == ObjectName){
                if(xmlStructure.SFS_Hardcoded_Flag__c == false){
                    String sfField = xmlStructure.SFS_Salesforce_Field__c;
                    if(FieldMaps.containsKey(sfField)){
                        double nodeOrder = xmlStructure.SFS_Node_Order__c;
                        //check if value from query is null
                        if(FieldMaps.get(sfField) != null){
                            String xmlFullName = xmlStructure.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(FieldMaps.get(sfField));
                            replacement = replacement.escapeXml();
                            String newLine = xmlFullName.replace(sfField, replacement);
                            xmlList.add(newLine);
                            if(xmlStructure.SFS_Node_Order__c == 41.00){
                                xmlList.add('<BEGINREPLACE>');
                            }
                        }
                        else if(FieldMaps.get(sfField) == null){
                            String empty = '';
                            String xmlFullName = xmlStructure.SFS_XML_Full_Name__c;
                            String newLine = xmlFullName.replace(sfField, empty);
                            xmlList.add(newLine);
                            if(xmlStructure.SFS_Node_Order__c == 41.00){
                                xmlList.add('<BEGINREPLACE>');
                            }
                        }
                    }
                    //if SCFieldMap does not contain sffield, the field value is null. Replace null value with just tags.
                    else if(!FieldMaps.ContainsKey(sfField)){
                        String empty = '';
                        String replacement = xmlStructure.SFS_XML_Full_Name__c.replace(sfField, empty);
                        xmlList.add(replacement);
                        if(xmlStructure.SFS_Node_Order__c == 41.00){
                            xmlList.add('<BEGINREPLACE>');
                        }
                    }
                }
                else if(xmlStructure.SFS_Hardcoded_Flag__c == true){
                    xmlList.add(xmlStructure.SFS_XML_Full_Name__c);
                    if(xmlStructure.SFS_Node_Order__c == 41.00){
                        xmlList.add('<BEGINREPLACE>');
                    }
                }
            }
            }
        }
        
        //check for hardcoded flag. If false, replace data.
        
        
        return xmlList;
        
    }
    
   Public static void InitialResponse(HttpResponse res, String interfaceLabel){
       	List<String> serviceContractId = interfaceLabel.split('\\|');
        Id AgreementId = Id.ValueOf(ServiceContractId[1]);
        
        ServiceContract sc = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM ServiceContract WHERE Id =: AgreementId];
        List<ContractLineItem> cli = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM ContractLineItem WHERE ServiceContractId =: AgreementId];
        
        If(res.getStatusCode() != 200){
            sc.SFS_Integration_Status__c = 'ERROR';
            sc.SFS_Integration_Response__c = 'Project Creation - Service Agreement with Service Agreement Line Item'+ '\nIntegration Response:\t' + res.getBody();
            for(ContractLineItem c: cli){
                c.SFS_Integration_Status__c = 'ERROR';
            	c.SFS_Integration_Response__c = 'Project Creation - Service Agreement with Service Agreement Line Item'+ '\nIntegration Response:\t' + res.getBody();
            }
        } else{
            sc.SFS_Integration_Status__c = 'SYNCING';
            sc.SFS_Integration_Response__c = 'Project Creation - Service Agreement with Service Agreement Line Item'+ '\nIntegration Response:\t' + res.getBody();
            for(ContractLineItem c: cli){
                c.SFS_Integration_Status__c = 'SYNCING';
                sc.SFS_Integration_Response__c = 'Project Creation - Service Agreement with Service Agreement Line Item'+ '\nIntegration Response:\t' + res.getBody();
            }
        }
        
        update sc;
        update cli;
       
       if(test.isRunningTest()){
           System.assert(sc.SFS_Integration_status__c == 'SYNCING');
       }
    }
    
}