@isTest
public with sharing class AssetTrigger_Test {
    
    public static testMethod void doTest() {
    	// Create some data
    	Account serenity = new Account( Name = 'Serenity', BillingStreet = 'teststreet' );
    	insert serenity;
        MPG4_Code__c m = new MPG4_Code__c(Distributor_Warehouse_points__c = 2, MPG4_Number__c = 'TESTexternalID', Description__c = 'Test description');
    	insert m;
        Asset a = new Asset(AccountId = serenity.Id, 
                            Name = 'Test Asset', 
                            Owner_Address_Line_1__c = 'teststreet', 
                            Hide_in_Distributor_Warehouse__c = false, 
                            Dchl__c = 'CM',
                            Last_Logged_Service_Visit__c = Date.Today().addDays(-365),
                            MPG4_Code__c = m.Id);
    	insert a;
    }

     @isTest
     public static void setDCChildAccountTest() {
         Account testAssetAccount = new Account(Name = 'TestAccount', Account_Number__c = '12345');
         insert testAssetAccount;

         Account testDCAccount = new Account(Name = 'TestAccount', DC__c = 'DC', ParentId = testAssetAccount.Id);
         insert testDCAccount;

         Asset testAsset = new Asset(AccountId = testAssetAccount.Id, Name = 'TestAsset', DChl__c = 'DC');

         Test.startTest();
             insert testAsset;
         Test.stopTest();

         List<Asset> actualAssets = [SELECT Id, DC_Child_Account__c FROM Asset];
         System.debug('Value of actualasset: '+actualAssets);

         System.assertNotEquals(true, actualAssets.isEmpty(), 'We expect assets to have been returned');
        
         for(Asset actualAsset : actualAssets) {
             System.assertEquals(true, String.isNotBlank(actualAsset.DC_Child_Account__c), 'We expect the Distributor channel account field to be populated');
         }
     }
}