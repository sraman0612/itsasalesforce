@isTest
public class ensxtx_TSTD_SalesDocAppSettings
{
    @isTest public static void test_appSettings()
    {
        ensxtx_DS_SalesDocAppSettings appSettings = buildAppSettings();
    }

    public static ensxtx_DS_SalesDocAppSettings buildAppSettings()
    {
        ensxtx_DS_SalesDocAppSettings appSettings = new ensxtx_DS_SalesDocAppSettings();
        appSettings.itemNumberIncrement = 10;
        appSettings.autoSimulate = new Map<String, Boolean>();
        appSettings.enableBoMItemEdit = false;
        appSettings.enableConfiguration = true;
        appSettings.SAPDocType = 'Quote';
        appSettings.SBODetailType = '';
        appSettings.DefaultDocType = 'OR';
        ensxtx_DS_SalesDocAppSettings.DocumentType documentType = new ensxtx_DS_SalesDocAppSettings.DocumentType();
        documentType.id = 'OR';
        documentType.label = 'TEST';
        appSettings.DocTypes = new List<ensxtx_DS_SalesDocAppSettings.DocumentType>{documentType};
        appSettings.salesAreasFromCustomer = false;
        appSettings.SalesAreas = new List<Object>();
        ensxtx_DS_SalesDocAppSettings.DocumentSetting documentSetting = new ensxtx_DS_SalesDocAppSettings.DocumentSetting();
        ensxtx_DS_SalesDocAppSettings.PartnerSetting partnerSetting = new ensxtx_DS_SalesDocAppSettings.PartnerSetting();
        partnerSetting.PartnerFunction = 'SH';
        partnerSetting.PartnerFunctionName = 'TEST';
        partnerSetting.ComponentType = 'Component';
        partnerSetting.SearchType = 'Partner';
        partnerSetting.autoSearch = true;
        partnerSetting.allowSearch = true;
        partnerSetting.allowAddressOverride = true;
        partnerSetting.autoPopulateAddressOverrideFromCustomer = true;
        ensxtx_DS_SalesDocAppSettings.PartnerSetting partnerSetting2 = new ensxtx_DS_SalesDocAppSettings.PartnerSetting();
        partnerSetting2.PartnerFunction = 'SP';
        partnerSetting2.PartnerFunctionName = 'TEST';
        partnerSetting2.ComponentType = 'Component';
        partnerSetting2.SearchType = 'Partner';
        partnerSetting2.autoSearch = true;
        documentSetting.PartnerPickers = new List<ensxtx_DS_SalesDocAppSettings.PartnerSetting>{partnerSetting, partnerSetting2};
        documentSetting.Texts = new Map<String, String>{
            '0001' => 'TEST'
        };
        appSettings.Header = documentSetting;
        appSettings.Item = documentSetting;
        appSettings.updateLineItems = true;
        appSettings.deleteLineItems = true;
        return appSettings;
    }
}