@isTest
private class C_Attachments_Controller_Test {

    @testSetup
    static void setupData(){
    
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Contact cont1 = new Contact();
        cont1.firstName = 'Can';
        cont1.lastName = ' Pango';
        cont1.email = 'service-gdi@canpango.com';
        cont1.Relationship__c = 'Customer';
        insert cont1;
        
        Case cse1 = new Case();
        cse1.Account = acct;
        cse1.Contact = cont1;
        cse1.ContactId = cont1.Id;
        cse1.Type = 'CSE1';
        cse1.Subject = 'Testing Case';
        
        // need to associate an org-wide email address to this case for testing
        OrgWideEmailAddress owea = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        System.debug(owea);
        cse1.Org_Wide_Email_Address_ID__c = ' ' + owea.Id;
        
        insert cse1;
        System.debug(cse1);
        
        Email_Template_Admin__c eta = new Email_Template_Admin__c();
        eta.Name = 'Test Email_Template_Admin__c';
        insert eta;

        Attachment att = new Attachment();
        att.Name = 'Attachment Test';
        att.ParentId = cse1.Id;
        att.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att.ContentType = 'image/jpg';
        insert att;

        EmailMessage em = new EmailMessage();
            em.Incoming = true;
            em.FromName = 'Canpango';
            em.ToAddress = 'salesforce@gardnerdenver.com';
            em.TextBody = 'Body';
            em.Subject = 'Subject';
            em.MessageDate = System.today();
            em.ParentId = cse1.Id;
            em.HtmlBody = '<p>Body</p>';
            em.FromAddress = 'service-gdi@canpango.com';
            em.CcAddress = 'service-gdi@canpango.com';
            em.BccAddress = 'service-gdi@canpango.com';
        insert em;
    }
   
    static testmethod void testC_CustomEmail_Controller(){
        Case c = [SELECT Id FROM Case];
        Attachment a = [Select Id FROM Attachment];
        
        Test.setCurrentPageReference(new PageReference('C_Attachments_Extension'));       
        System.currentPageReference().getParameters().put('CaseId', c.Id);        

        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        C_Attachments_Extension cae = new C_Attachments_Extension(sc);
        List<C_Attachments_Extension.AttachmentWrapper> wrappers = cae.thewrappers;
        
        for(C_Attachments_Extension.AttachmentWrapper wrap: wrappers){
            wrap.cBox = false;
        }

        cae.attchID = a.Id;
        cae.createNewFiles_Stage();
        cae.cloneAttachments();
        cae.getTopLevelCase(c.id);
        cae.deleteAttachments();
    }
    
    static testmethod void testC_CustomEmail_Controller2(){
        Case c = [SELECT Id, AccountId, ContactId FROM Case];
        Attachment a = [Select Id FROM Attachment];

        Case cse1 = new Case();
        cse1.AccountId = c.AccountId;
        cse1.ContactId = c.ContactId;
        cse1.Type = 'CSE1';
        cse1.ParentId = c.id;
        cse1.Subject = 'Testing Case';
        insert cse1;
        
        Test.setCurrentPageReference(new PageReference('C_Attachments_Extension'));       
        System.currentPageReference().getParameters().put('CaseId', cse1.Id);        

        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        C_Attachments_Extension cae = new C_Attachments_Extension(sc);
        List<C_Attachments_Extension.AttachmentWrapper> wrappers = cae.thewrappers;
        
        for(C_Attachments_Extension.AttachmentWrapper wrap: wrappers){
            wrap.cBox = true;
        }

        cae.attchID = a.Id;
        cae.createNewFiles_Stage();
        cae.cloneAttachments();
        cae.getTopLevelCase(cse1.id);
        cae.deleteAttachments();
    }    
}