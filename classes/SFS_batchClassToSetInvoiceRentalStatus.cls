/*=========================================================================================================
* @author Yadav, Arati
* @date 07/10/2021
* @description:This is used to update the Rental Invoice status to 'Submitted
* @Story Number: SIF-3937

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Srikanth P         M-001      04/08/2022   Replaced Status__c field with SFS_Status__c 

============================================================================================================================================================*/
public class SFS_batchClassToSetInvoiceRentalStatus implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
     public ID sAgreeRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'ServiceContract' AND DeveloperName = 'SFS_Advanced_Billing'].id;
     public ID rentalAgreeRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'ServiceContract' AND DeveloperName = 'SFS_Rental'].id;
     public Database.QueryLocator start(database.BatchableContext bc) {
         String Query = 'Select Id,SFS_Status__c,SFS_Service_Agreement__r.SFS_Invoice_Submission__c,SFS_Service_Agreement__r.SFS_Special_Note__c,SFS_Billing_Period_End_Date__c,SFS_Service_Agreement__r.SFS_Invoice_Frequency__c,SFS_Service_Agreement__r.SFS_Special_Invoice_Note__c,SFS_Service_Agreement__r.recordtypeId,SFS_Service_Agreement__r.Id,SFS_Service_Agreement__r.SFS_Requested_Payment_Terms__c,SFS_Service_Agreement__r.AccountId,SFS_Service_Agreement__r.SFS_Agreement_Value__c,SFS_Service_Agreement__r.CurrencyIsoCode,SFS_Work_Order__r.Id  from Invoice__c Where SFS_Billing_Period_Start_Date__c<=TODAY AND SFS_Status__c =\'Created\'AND SFS_Service_Agreement__r.RecordType.DeveloperName = \'SFS_Rental\' AND SFS_Service_Agreement__c!=null AND SFS_Service_Agreement__r.SFS_Invoice_Submission__c != \'Hold\' AND SFS_Service_Agreement__r.SFS_Special_Note__c != true AND SFS_Service_Agreement__r.SFS_Status__c =\'APPROVED\' AND SFS_Charge_Count__c != NULL AND SFS_Service_Agreement__r.SFS_Invoice_Frequency__c!=NULL AND SFS_Work_Order__r.Id=NULL AND SFS_Service_Agreement__r.SFS_Integration_Status__c = \'APPROVED\' order by SFS_Billing_Period_Start_Date__c desc LIMIT 40';
         return Database.getQueryLocator(Query);
          
    }
    
    public void execute(Database.BatchableContext bc, List<Invoice__c> scope){
	 Try{     
         for(Invoice__c inv : scope){
              List<Invoice__c> invToUpdate = new List<Invoice__c>();
               system.debug('inv.SFS_Service_Agreement__r.SFS_Special_Note__c '+inv.SFS_Service_Agreement__r.SFS_Special_Note__c);
         	  if(inv.SFS_Service_Agreement__r.recordtypeId !=sAgreeRecId && inv.SFS_Service_Agreement__r.recordtypeId == rentalAgreeRecId &&  inv.SFS_Billing_Period_End_Date__c == system.today()){
               	inv.SFS_Status__c ='Submitted';
                   system.debug('in IF');
               	invToUpdate.add(inv); 
             }
          if(!invToUpdate.isEmpty()){
             System.enqueueJob(new SFS_setInvoiceStatusQueueable(invToUpdate));
           } 
         }
     }
        Catch(Exception e){
        }		
     }   
    public void finish(Database.BatchableContext bc){
    }
    
 }