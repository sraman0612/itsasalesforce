/// enosiX Inc. Generated Apex Model
/// Generated On: 11/2/2020 2:27:37 PM
/// SAP Host: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class ensxtx_TST_EnosixMaterial_Search
{

    public class Mockensxtx_SBO_EnosixMaterial_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixMaterial_Search.class, new Mockensxtx_SBO_EnosixMaterial_Search());
        ensxtx_SBO_EnosixMaterial_Search sbo = new ensxtx_SBO_EnosixMaterial_Search();
        System.assertEquals(ensxtx_SBO_EnosixMaterial_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        ensxtx_SBO_EnosixMaterial_Search.EnosixMaterial_SC sc = new ensxtx_SBO_EnosixMaterial_Search.EnosixMaterial_SC();
        System.assertEquals(ensxtx_SBO_EnosixMaterial_Search.EnosixMaterial_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);
        System.assertNotEquals(null, sc.MATERIAL_TYPE);
        System.assertNotEquals(null, sc.PRODUCT_ATTRIB);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        ensxtx_SBO_EnosixMaterial_Search.SEARCHPARAMS childObj = new ensxtx_SBO_EnosixMaterial_Search.SEARCHPARAMS();
        System.assertEquals(ensxtx_SBO_EnosixMaterial_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.MaterialNumberFrom = 'X';
        System.assertEquals('X', childObj.MaterialNumberFrom);

        childObj.MaterialNumberTo = 'X';
        System.assertEquals('X', childObj.MaterialNumberTo);

        childObj.MaterialDescription = 'X';
        System.assertEquals('X', childObj.MaterialDescription);

        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);

        childObj.MaterialGroup = 'X';
        System.assertEquals('X', childObj.MaterialGroup);

        childObj.ProductHierarchy = 'X';
        System.assertEquals('X', childObj.ProductHierarchy);

        childObj.CrossPlantMaterialStatus = 'X';
        System.assertEquals('X', childObj.CrossPlantMaterialStatus);

        childObj.FromCreateDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.FromCreateDate);

        childObj.ToCreateDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.ToCreateDate);

        childObj.FromChangeDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.FromChangeDate);

        childObj.ToChangeDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.ToChangeDate);

        childObj.KitFlag = true;
        System.assertEquals(true, childObj.KitFlag);


    }

    @isTest
    static void testMATERIAL_TYPE()
    {
        ensxtx_SBO_EnosixMaterial_Search.MATERIAL_TYPE childObj = new ensxtx_SBO_EnosixMaterial_Search.MATERIAL_TYPE();
        System.assertEquals(ensxtx_SBO_EnosixMaterial_Search.MATERIAL_TYPE.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        ensxtx_SBO_EnosixMaterial_Search.MATERIAL_TYPE_COLLECTION childObjCollection = new ensxtx_SBO_EnosixMaterial_Search.MATERIAL_TYPE_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.MaterialType = 'X';
        System.assertEquals('X', childObj.MaterialType);


    }

    @isTest
    static void testPRODUCT_ATTRIB()
    {
        ensxtx_SBO_EnosixMaterial_Search.PRODUCT_ATTRIB childObj = new ensxtx_SBO_EnosixMaterial_Search.PRODUCT_ATTRIB();
        System.assertEquals(ensxtx_SBO_EnosixMaterial_Search.PRODUCT_ATTRIB.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        ensxtx_SBO_EnosixMaterial_Search.PRODUCT_ATTRIB_COLLECTION childObjCollection = new ensxtx_SBO_EnosixMaterial_Search.PRODUCT_ATTRIB_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.ProductAttribute = 'X';
        System.assertEquals('X', childObj.ProductAttribute);


    }

    @isTest
    static void testEnosixMaterial_SR()
    {
        ensxtx_SBO_EnosixMaterial_Search.EnosixMaterial_SR sr = new ensxtx_SBO_EnosixMaterial_Search.EnosixMaterial_SR();

        sr.registerReflectionForClass();

        System.assertEquals(ensxtx_SBO_EnosixMaterial_Search.EnosixMaterial_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        ensxtx_SBO_EnosixMaterial_Search.SEARCHRESULT childObj = new ensxtx_SBO_EnosixMaterial_Search.SEARCHRESULT();
        System.assertEquals(ensxtx_SBO_EnosixMaterial_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        ensxtx_SBO_EnosixMaterial_Search.SEARCHRESULT_COLLECTION childObjCollection = new ensxtx_SBO_EnosixMaterial_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.Material = 'X';
        System.assertEquals('X', childObj.Material);

        childObj.MaterialDescription = 'X';
        System.assertEquals('X', childObj.MaterialDescription);

        childObj.MaterialType = 'X';
        System.assertEquals('X', childObj.MaterialType);

        childObj.MaterialTypeDescription = 'X';
        System.assertEquals('X', childObj.MaterialTypeDescription);

        childObj.MaterialGroup = 'X';
        System.assertEquals('X', childObj.MaterialGroup);

        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);

        childObj.ProductHierarchy = 'X';
        System.assertEquals('X', childObj.ProductHierarchy);

        childObj.ProductHierarchyDescription = 'X';
        System.assertEquals('X', childObj.ProductHierarchyDescription);

        childObj.CrossPlantMaterialStatus = 'X';
        System.assertEquals('X', childObj.CrossPlantMaterialStatus);

        childObj.MaterialStatusDescription = 'X';
        System.assertEquals('X', childObj.MaterialStatusDescription);

        childObj.CreationDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.CreationDate);

        childObj.ChangeDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.ChangeDate);

        childObj.AlternateUnitOfMeasure = 'X';
        System.assertEquals('X', childObj.AlternateUnitOfMeasure);

        childObj.NumeratorForConversion = 1.5;
        System.assertEquals(1.5, childObj.NumeratorForConversion);

        childObj.DenominatorForConversion = 1.5;
        System.assertEquals(1.5, childObj.DenominatorForConversion);

        childObj.Quotient = 1.5;
        System.assertEquals(1.5, childObj.Quotient);

        childObj.BaseUnitOfMeasure = 'X';
        System.assertEquals('X', childObj.BaseUnitOfMeasure);

        childObj.BaseUoMDescription = 'X';
        System.assertEquals('X', childObj.BaseUoMDescription);

        childObj.ConfigurableMaterial = 'X';
        System.assertEquals('X', childObj.ConfigurableMaterial);

        childObj.BOMUsage = 'X';
        System.assertEquals('X', childObj.BOMUsage);

        childObj.BOMUsageDescription = 'X';
        System.assertEquals('X', childObj.BOMUsageDescription);

        childObj.Stock = true;
        System.assertEquals(true, childObj.Stock);

        childObj.Available = 1.5;
        System.assertEquals(1.5, childObj.Available);

        childObj.Plant = 'X';
        System.assertEquals('X', childObj.Plant);

        childObj.PlantDescription = 'X';
        System.assertEquals('X', childObj.PlantDescription);

        childObj.DistChainStatus = 'X';
        System.assertEquals('X', childObj.DistChainStatus);

        childObj.DistChainStatusDecription = 'X';
        System.assertEquals('X', childObj.DistChainStatusDecription);

        childObj.Materialgroup4 = 'X';
        System.assertEquals('X', childObj.Materialgroup4);

        childObj.MaterialGroup4Text = 'X';
        System.assertEquals('X', childObj.MaterialGroup4Text);


    }
}