@IsTest
public class ATG_DistributorSummaryEmailBatchTest {
	@IsTest
    public static void batchFlow() {
        Id distributorRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        Date today = System.today();
        Contact testContact = new Contact(FirstName = 'Wile', 
                                          LastName = 'Coyote',
                                          RecordTypeId = distributorRecordTypeId, 
                                          Email = 'WileECoyote@Acme.com');
        insert testContact;
        
        Account testAccount = new Account(Name = 'Acme inc', 
                                          Service_Notification_Contact__c = testContact.Id);
        insert testAccount;
        
        List<Asset> serialNumbersToInsert = new List<Asset>();
        serialNumbersToInsert.add(new Asset(Name = 'SR-0000',
                                    AccountId = testAccount.Id,
                                    Current_Servicer__c = testAccount.Id,
                                    Expected_Next_Service_Date__c = today.addDays(3),
                                    Machine_Status__c = 'Active - Primary Unit'));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0001',
                                   AccountId = testAccount.Id,
                                   Current_Servicer__c = testAccount.Id,
                                   Expected_Next_Service_Date__c = today.addDays(20),
                                   Machine_Status__c = 'Active - Primary Unit'));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0002',
                                   AccountId = testAccount.Id,
                                   Current_Servicer__c = testAccount.Id,
                                   Expected_Next_Service_Date__c = today.addDays(-7),
                                   Machine_Status__c = 'Active - Primary Unit'));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0003',
                                    AccountId = testAccount.Id,
                                    Current_Servicer__c = testAccount.Id,
                                    Expected_Next_Service_Date__c = today.addDays(3),
                                    Machine_Status__c = 'Customer Servicing - with OEM Parts'));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0004',
                                   AccountId = testAccount.Id,
                                   Current_Servicer__c = testAccount.Id,
                                   Expected_Next_Service_Date__c = today.addDays(20),
                                   Machine_Status__c = 'Customer Servicing - with OEM Parts'));
        serialNumbersToInsert.add(new Asset(Name = 'SR-0005',
                                   AccountId = testAccount.Id,
                                   Current_Servicer__c = testAccount.Id,
                                   Expected_Next_Service_Date__c = today.addDays(-7),
                                   Machine_Status__c = 'Customer Servicing - with OEM Parts'));
        insert serialNumbersToInsert;
        
        List<Oil_Sample__c> oilSamplesToInsert = new List<Oil_Sample__c>();
        oilSamplesToInsert.add(new Oil_Sample__c(Name = 'LS-0000',
                                                Serial_Number_Lookup__c = serialNumbersToInsert[0].Id,
                                                Status__c = 'Action Required'));
        oilSamplesToInsert.add(new Oil_Sample__c(Name = 'LS-0001',
                                                Serial_Number_Lookup__c = serialNumbersToInsert[0].Id,
                                                Status__c = 'No Action Required'));
        insert oilSamplesToInsert;
                
        Test.startTest();
        	ATG_DistributorSummaryEmailBatch batch = new ATG_DistributorSummaryEmailBatch();
        	Database.executeBatch(batch);
        Test.stopTest();
    }
}