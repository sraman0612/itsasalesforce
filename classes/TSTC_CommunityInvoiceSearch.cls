@IsTest
private class TSTC_CommunityInvoiceSearch {

    class MOC_getSalesAreaMaster implements ensxsdk.EnosixFramework.RFCMock
    {
        public ensxsdk.EnosixFramework.FunctionObject executeFunction()
        {
            RFC_SD_GET_SALES_AREAS.RESULT result = new RFC_SD_GET_SALES_AREAS.RESULT();
            result.setSuccess(false);
            return result;
        }
    }

    public class MockSBO_EnosixSalesDocOutput_Search implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
		public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
		{
            SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC searchContext = (SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC)sc;
            SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SR searchResult = new SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SR();
            SBO_EnosixSalesDocOutput_Search.SEARCHRESULT result = new SBO_EnosixSalesDocOutput_Search.SEARCHRESULT();

            result.SalesDocument = '8675309';
            result.ConditionType = 'X';
            result.ConditionTypeDescription = 'X';
            result.Language = 'X';
            result.PDFB64String = 'X';

            searchResult.SearchResults.add(result);
            searchContext.baseResult = searchResult;

			return searchContext;
        }
    }

    @isTest
    static void tesInvoiceSearchSuccess()
    {
        ensxsdk.EnosixFramework.setMock(RFC_SD_GET_SALES_AREAS.class, new MOC_getSalesAreaMaster());
        ensxsdk.EnosixFramework.setMock(SBO_EnosixInvoice_Search.class, new MOC_EnosixInvoice_Search.MockEnosixInvoiceSuccess());
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSalesDocOutput_Search.class, new MockSBO_EnosixSalesDocOutput_Search());

        Account account = new Account(Name = 'name', BillingCountry = 'United States');
        UTIL_SFAccount.setAccountCustomerNumber(account, 'test');
        insert account;

        System.currentPageReference().getParameters().put('customerid',String.valueOf(account.Id));
        UTIL_PageState.current.sfAccountId = account.Id;

        Test.startTest();
        CTRL_CommunityInvoiceSearch.getResultList(account.Id);
        system.assertEquals(System.currentPageReference().getParameters().get('customerid'),  String.valueOf(account.Id));
        UTIL_Aura.Response resp1 = CTRL_CommunityInvoiceSearch.reprintInvoice('8675309');
        Test.stopTest();
    }

    @isTest
    static void tesInvoiceSearchFailure()
    {
        ensxsdk.EnosixFramework.setMock(RFC_SD_GET_SALES_AREAS.class, new MOC_getSalesAreaMaster());
        ensxsdk.EnosixFramework.setMock(SBO_EnosixInvoice_Search.class, new MOC_EnosixInvoice_Search.MockEnosixInvoiceFailure());

        Account account = new Account(Name = 'name', BillingCountry = 'United States');
        UTIL_SFAccount.setAccountCustomerNumber(account, 'test');
        insert account;

        System.currentPageReference().getParameters().put('customerid',String.valueOf(account.Id));
        UTIL_PageState.current.sfAccountId = account.Id;

        Test.startTest();
        CTRL_CommunityInvoiceSearch.getResultList(account.name);
        Test.stopTest();
    }

    @isTest
    static void tesInvoiceReprint()
    {
        ensxsdk.EnosixFramework.setMock(RFC_SD_GET_SALES_AREAS.class, new MOC_getSalesAreaMaster());
        ensxsdk.EnosixFramework.setMock(SBO_EnosixInvoice_Search.class, new MOC_EnosixInvoice_Search.MockEnosixInvoiceSuccess());

        Account account = new Account(Name = 'name', BillingCountry = 'United States');
        UTIL_SFAccount.setAccountCustomerNumber(account, 'test');
        insert account;

        System.currentPageReference().getParameters().put('customerid',String.valueOf(account.Id));
        UTIL_PageState.current.sfAccountId = account.Id;

        Test.startTest();
        CTRL_CommunityInvoiceSearch.reprintInvoice('900660461');
        system.assertEquals(System.currentPageReference().getParameters().get('customerid'),  String.valueOf(account.Id));
        Test.stopTest();
    }
    
    @IsTest
    static void testEnosixOutputSearch() 
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSalesDocOutput_Search.class, new MockSBO_EnosixSalesDocOutput_Search());
        
        UTIL_EnosixOutput_Search.getBOLSearchContext('12345');
    }
}