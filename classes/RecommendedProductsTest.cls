/**
* @author           Amit Datta
* @description      Test Class for RecommendedProducts.
*
* Modification Log
* ------------------------------------------------------------------------------------------
*         Developer                   Date                Description
* ------------------------------------------------------------------------------------------
*         Amit Datta                  14/01/2024          Original Version
*		  Amit Datta				  08/05/2024		  Adapted the Class to cover the updated methods
**/

@isTest
public with sharing class RecommendedProductsTest {
    @testSetup static void setup() {
        
        UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' limit 1];
        User usr = CTS_TestUtility.createUser(false);
        usr.UserRoleId = r1.Id;
        insert usr;
        System.runAs(usr){
            Account account = CTS_TestUtility.createAccount('TestAccount', false);
            account.OwnerId = usr.Id;
            insert account;
            Contact con = CTS_TestUtility.createContact('lastnameContact', 'firstnameContact', 'test@test.com', account.Id, true);
            WebStore webStore = new WebStore(Name='TestWebStore');
            insert webStore;
            List<Product2> prdList = new List<Product2>();
            Product2 pr1 = new Product2(Name = 'prd1');
            prdList.add(pr1);
            Product2 pr2 = new Product2(Name = 'prd2');
            prdList.add(pr2);
            Product2 pr3 = new Product2(Name = 'prd3');
            prdList.add(pr3);
            insert prdList;
            CTS_IOT_Frame_Type__c cif = new CTS_IOT_Frame_Type__c(name = 'frameType',CTS_IOT_Type__c = 'Compressor');
            insert cif;
            Cross_Sell_Recommendations__c csr = new Cross_Sell_Recommendations__c(Recommended_ProductId__c = prdList[0].Id, Recommended_Model_ParentId__c = cif.Id,Recommended_Product_ParentId__c = prdList[2].Id);
            insert csr;
            Asset asset1 = new Asset(Name = 'Asset1',AccountId = account.Id,Product2Id =prdList[0].Id);
            insert asset1;
        }
    }
    
    @isTest static void getCSRecommendationProductsTest() {
        String frameTypeId = [Select Recommended_Model_ParentId__c from Cross_Sell_Recommendations__c limit 1 ].Recommended_Model_ParentId__c;
        String contactId = [Select Id from Contact limit 1 ].Id;
        String profileId = [Select Id from Profile where name = 'IR Comp Customer Community - Standard User' limit 1 ].Id;
        user usr = CTS_TestUtility.createUser(contactId,profileId,true);
        System.runAs(usr){
            Test.startTest();
            List<RecommendedProducts.CartItemWrapper> ciwList = RecommendedProducts.getCSRecommendationProducts('');
            Assert.isNull(ciwList);
            ciwList = RecommendedProducts.getCSRecommendationProducts(frameTypeId);
            Assert.areEqual(1, ciwList.size());
            Test.stopTest();
        }
        
    }
    @isTest static void addToCartTest() {
        String webStoreId = [Select Id from WebStore limit 1].Id;
        String productId = [Select Id from product2 limit 1].Id;
        String effectiveAccountId = [Select Id from Account limit 1].Id;
        Test.startTest();
        ConnectApi.CartItem cartItem = RecommendedProducts.addToCart(webStoreId,productId,'5',effectiveAccountId);
        Assert.isNull(cartItem);
        Test.stopTest();
    }
    
    @isTest static void getCSRecommendationProductsByProductIdTest() {
        String productId = [Select Recommended_Product_ParentId__c from Cross_Sell_Recommendations__c limit 1 ].Recommended_Product_ParentId__c;
        String contactId = [Select Id from Contact limit 1 ].Id;
        String profileId = [Select Id from Profile where name = 'IR Comp Customer Community - Standard User' limit 1 ].Id;
        user usr = CTS_TestUtility.createUser(contactId,profileId,true);
        System.runAs(usr){
            Test.startTest();
            List<RecommendedProducts.CartItemWrapper> ciwList = RecommendedProducts.getCSRecommendationProductsByProductId('');
            Assert.isNull(ciwList);
            ciwList = RecommendedProducts.getCSRecommendationProductsByProductId(productId);
            Assert.areEqual(1, ciwList.size());
            Test.stopTest();
        }
    }
    @isTest static void getIRLogoUrlTest() {
        String contactId = [Select Id from Contact limit 1 ].Id;
        String profileId = [Select Id from Profile where name = 'IR Comp Customer Community - Standard User' limit 1 ].Id;
        user usr = CTS_TestUtility.createUser(contactId,profileId,true);
        System.runAs(usr){
            Test.startTest();
            	String irLogoUrl = RecommendedProducts.getIRLogoUrl();
            	Assert.isNotNull(irLogoUrl);
            Test.stopTest();
        }
    }
    @isTest static void getWebCartUrlTest() {
        String contactId = [Select Id from Contact limit 1 ].Id;
        String profileId = [Select Id from Profile where name = 'IR Comp Customer Community - Standard User' limit 1 ].Id;
        user usr = CTS_TestUtility.createUser(contactId,profileId,true);
        System.runAs(usr){
            Test.startTest();
            	String webCartUrl = RecommendedProducts.getWebCartUrl();
            	Assert.isNotNull(webCartUrl);
            Test.stopTest();
        }
    }
    
    @isTest static void getAssetDataTest() {
        String assetId = [Select Id from Asset limit 1 ].Id;
        String contactId = [Select Id from Contact limit 1 ].Id;
        String profileId = [Select Id from Profile where name = 'IR Comp Customer Community - Standard User' limit 1 ].Id;
        user usr = CTS_TestUtility.createUser(contactId,profileId,true);
        System.runAs(usr){
            Test.startTest();
            	Asset asset = RecommendedProducts.getAssetData(assetId);
            	Assert.isNotNull(asset);
            Test.stopTest();
        }
    }
}