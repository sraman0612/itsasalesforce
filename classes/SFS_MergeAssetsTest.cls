/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 13/12/2021
* @description: SFS_MergeAssetsTest
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001     SIF-183  13/12/2021  Merge Assets Test Class
===========================================================================================================================*/
@isTest
public class SFS_MergeAssetsTest {
    private static final String WO_RECORDTYPEID = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
    private static final String SA_RECORDTYPEID = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Advanced Billing').getRecordTypeId();
    private static final String PR_RECORDTYPEID = Schema.SObjectType.ProductRequest.getRecordTypeInfosByName().get('Service').getRecordTypeId();
    
    @testSetup static void createtestData(){
        Double latitude = 36.127443;
        Double longitude = 	-115.171651;
        
        //Inserting Account
        List<Account> accList=SFS_TestDataFactory.createAccounts(2, false);
        accList[1].recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        //insert accList;
        Database.DMLOptions opts = new Database.DMLOptions();       
        opts.DuplicateRuleHeader.AllowSave = true;        
        Database.insert(accList, opts);
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].Type='Prospect';
        update accList;
        
        //Inserting Contact
        Contact con = new Contact(Firstname = 'Test',
                                  LastName = 'Contact',
                                  title = 'tester',
                                  email = 'test@gmail.com',
                                  phone = '1234567890');
        insert con;
        
        //Inserting Frametype
        CTS_IOT_Frame_Type__c frmeTypeFil = new CTS_IOT_Frame_Type__c(Name = 'test', CTS_IOT_Type__c = 'Filter');
        insert frmeTypeFil;
        
        
        
        //Inserting child Asset
        Asset astC = new Asset(Name = 'mergeId', CurrencyIsoCode = 'USD', CTS_Frame_Type__c = frmeTypeFil.Id,
                               AccountId = accList[0].Id);
        insert astC;
        
        //Inserting master Asset
        List<Asset> ast = SFS_TestDataFactory.createAssets(1,false);
        ast[0].ParentId = astC.Id;
        ast[0].AccountId = accList[0].Id;
        insert ast[0];
        
        Case case1 = new Case(Subject = 'CASE 2229',AssetId=astC.id);
        insert case1; 
        
        //Inserting WorkType
        /*WorkType wkFil = new WorkType(SFS_Work_Scope1__c = '8', Name = 'test', EstimatedDuration = 8, 
CTS_Frame_Type__c = frmeTypeFil.Id, SFS_Work_Order_Type__c = 'Preventive Maintenance');
insert wkFil;*/
        
        
        
        //Inserting Product
        Product2 pro = new Product2(Name = 'testProduct',SFS_External_Id__c='42527216',IsActive = true,SFS_Oracle_Orderable__c = 'Y');
        insert pro;
        
        
        
        
        
        //Instantiate the Pricebook2 record with StandardPricebookId
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        //Execute an update DML on the Pricebook2 record, to make IsStandard to true
        Update standardPricebook;
        
        //Query for the Pricebook2 record, to check IsStandard field
        standardPricebook = [SELECT Id, IsStandard FROM Pricebook2 WHERE Id = :standardPricebook.Id];
        //It should return true
        System.assertEquals(true, standardPricebook.IsStandard);
        
        //Inserting PricebookEntry
        PricebookEntry pbE = new PricebookEntry(Pricebook2Id = standardPricebook.Id, Product2Id = pro.Id, UnitPrice = 2.00,
                                                IsActive = true);
        insert pbE;
        
        //Inserting ServiceContract
        ServiceContract sc = new ServiceContract(ContactId = con.Id, AccountId = accList[0].Id,
                                                 SFS_Agreement_Value__c = 78,SFS_Invoice_Format__c = 'Detail',
                                                 SFS_Invoice_Submission__c = 'Hold',StartDate = Date.newInstance(2021,09,29),EndDate = Date.newInstance(2021,10,07),
                                                 SFS_Bill_To_Account__c = accList[0].Id,SFS_Portable_Required__c = 'Yes', SFS_Consumables_Ship_To__c = accList[0].Id, 
                                                 RecordTypeId = SA_RECORDTYPEID, Name = '1234',SFS_Status__c='Draft',SFS_Invoice_Frequency__c='Daily',
                                                 Pricebook2Id = standardPricebook.Id );
        insert sc;
        
        //Inserting ContractLineItem
        /*ContractLineItem scli = new ContractLineItem(UnitPrice = 2.00, SFS_Billing_Method__c = 'Schedule Based',
Quantity = 2.00, ServiceContractId = sc.Id,
StartDate = Date.newInstance(2021,09,29),
EndDate = Date.newInstance(2021,10,07),
PricebookEntryId = pbE.Id);
insert scli;*/
        
        //Inserting Case
        /*Case cs = new Case(Origin = 'Phone', Status = 'New', AccountId = accList[0].Id, AssetId =  ast[0].Id, Account_DC__c=);
insert cs;*/
        
    }
    @isTest
    public static void mergeAssetsTest(){
        List<Asset> assetList=[Select Id,ParentId,AccountId from Asset];
        List<Account> accList=[Select Id from Account];
        //setting Ids
        List<Id> mergeId = new List<Id>();
        mergeId.add(assetList[0].Id);
        //Calling Apex Class
        Test.startTest();
        List<WorkOrder> woList= SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,null, null, null,false);
        woList[0].AssetId=assetList[0].Id;
        insert woList;
        //Inserting WorkOrderLineItem
        List<WorkOrderLineItem> woli = SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,null,false);
        woli[0].AssetId = assetList[0].Id;
        insert woli[0];
        //Inserting ProductRequest
        ProductRequest pr = new ProductRequest(NeedByDate = Date.newInstance(2022,09,29),SFS_Ship_To_Account__c = accList[0].Id,
                                               RecordTypeId = PR_RECORDTYPEID);
        insert pr;
        
        //Inserting Location
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true);
        insert lc;
        Product2 prod =[Select Id from Product2 where SFS_External_Id__c != null Limit 1];
        //Inserting ProductRequestLineItem
        ProductRequestLineItem prli = new ProductRequestLineItem(Product2Id = prod.Id, QuantityRequested = 27,
                                                                 SourceLocationId = lc.Id, ParentId = pr.Id,
                                                                 SFS_Asset__c =  assetList[0].Id);
        insert prli;
        
        
        //Inserting Entitlement
        Entitlement ent = new Entitlement(AccountId = accList[0].Id, EndDate = System.today().addYears(2), StartDate =  System.today(),
                                          SFS_Contract_Hours__c = 12000, SFS_Start_Hours__c = 9, AssetId = assetList[0].Id,
                                          Name = 'testEnt',SFS_CreateMWR__c=false);
        insert ent;
        //Inserting WorkPlanTemplate
        WorkPlanTemplate wpt = new WorkPlanTemplate(Name = 'testWorkPlanTemplate', RelativeExecutionOrder = 7,
                                                    SFS_Type__c = 'Oil');
        insert wpt;
        
        //Inserting WorkPlanSelectionRule
        WorkPlanSelectionRule wpsr = new WorkPlanSelectionRule(WorkPlanTemplateId = wpt.Id, AssetId = assetList[0].Id);
        insert wpsr;
        SFS_MergeAssets.mergeAssets(mergeId);
        Test.stopTest();
        
    }
    
    
}