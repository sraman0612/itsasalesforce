@isTest
private class TSTC_ServiceTechnician
{
    public class MOC_RFC_Z_ENSX_TECHNICIAN_LIST implements ensxsdk.EnosixFramework.RFCMock
    {
        public boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        public ensxsdk.EnosixFramework.FunctionObject executeFunction()
        {
            RFC_Z_ENSX_TECHNICIAN_LIST.RESULT result = new RFC_Z_ENSX_TECHNICIAN_LIST.RESULT();
            result.setSuccess(this.success);

            RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST cur = new RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST();
            result.getCollection(RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST.class).add(cur);
            
            cur.ABTNR = 'X';
            cur.Name1 = 'X';
            cur.NAMEV = 'X';
            cur.PAFKT = 'X';
            cur.PARNR = 'X';
            
            return result;
        }
    }

    @isTest
    public static void test_ConstructorAndItems()
    {
        ensxsdk.EnosixFramework.setMock(RFC_Z_ENSX_TECHNICIAN_LIST.class, new MOC_RFC_Z_ENSX_TECHNICIAN_LIST());

        CTRL_ServiceTechnician.getServiceTechnicians('X', 'X');
    }
}