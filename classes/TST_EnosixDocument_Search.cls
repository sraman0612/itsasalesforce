/// enosiX Inc. Generated Apex Model
/// Generated On: 10/14/2019 1:24:03 PM
/// SAP Host: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class TST_EnosixDocument_Search
{

    public class MockSBO_EnosixDocument_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixDocument_Search.class, new MockSBO_EnosixDocument_Search());
        SBO_EnosixDocument_Search sbo = new SBO_EnosixDocument_Search();
        System.assertEquals(SBO_EnosixDocument_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        SBO_EnosixDocument_Search.EnosixDocument_SC sc = new SBO_EnosixDocument_Search.EnosixDocument_SC();
        System.assertEquals(SBO_EnosixDocument_Search.EnosixDocument_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        SBO_EnosixDocument_Search.SEARCHPARAMS childObj = new SBO_EnosixDocument_Search.SEARCHPARAMS();
        System.assertEquals(SBO_EnosixDocument_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.Material = 'X';
        System.assertEquals('X', childObj.Material);

        childObj.GdDocumentType = 'X';
        System.assertEquals('X', childObj.GdDocumentType);

        childObj.Language = 'X';
        System.assertEquals('X', childObj.Language);


    }

    @isTest
    static void testEnosixDocument_SR()
    {
        SBO_EnosixDocument_Search.EnosixDocument_SR sr = new SBO_EnosixDocument_Search.EnosixDocument_SR();

        sr.registerReflectionForClass();

        System.assertEquals(SBO_EnosixDocument_Search.EnosixDocument_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        SBO_EnosixDocument_Search.SEARCHRESULT childObj = new SBO_EnosixDocument_Search.SEARCHRESULT();
        System.assertEquals(SBO_EnosixDocument_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixDocument_Search.SEARCHRESULT_COLLECTION childObjCollection = new SBO_EnosixDocument_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.StartDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.StartDate);

        childObj.EndDate = Date.valueOf('2020-12-31');
        System.assertEquals(Date.valueOf('2020-12-31'), childObj.EndDate);

        childObj.DocumentNumber = 'X';
        System.assertEquals('X', childObj.DocumentNumber);

        childObj.DocumentVersion = 'X';
        System.assertEquals('X', childObj.DocumentVersion);

        childObj.GdDocumentType = 'X';
        System.assertEquals('X', childObj.GdDocumentType);

        childObj.DocumentType = 'X';
        System.assertEquals('X', childObj.DocumentType);

        childObj.DocumentPart = 'X';
        System.assertEquals('X', childObj.DocumentPart);

        childObj.DocumentDescription = 'X';
        System.assertEquals('X', childObj.DocumentDescription);


    }

}