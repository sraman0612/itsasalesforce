@isTest
public class SFS_LaborHourOutbound_Test {
	@isTest
    public static void testLaborHourOutbound(){
        Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        List<Account> acc = SFS_TestDataFactory.createAccounts(1, false);
        acc[0].Type = 'Ship To';
        insert acc[0];
        Account accId = new Account();
        AccId = acc[0];
        Schema.Location ptLoc = new Schema.Location();
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        /*List<ServiceContract> sct = SFS_TestDataFactory.createServiceAgreement(1, accId.Id, true);
        ServiceContract sctr = new ServiceContract();
        sctr = sct[0];*/
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(2,div[0].Id,true);
       
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, accId.Id, loc[0].Id, div[0].Id, null, true);
		String woId = String.ValueOf(wo[0].id);
        
        WorkType wt = new WorkType();
        wt.Name = 'WOLITesting';
        wt.EstimatedDuration = 10.0;
        insert wt;
        
        List<WorkOrderLineItem> woli = SFS_TestDataFactory.createWorkOrderLineItem(1, wo[0].id,wt.id, true); 
        List<CAP_IR_Labor__c> labor = SFS_TestDataFactory.createLaborHours(1,woId,String.ValueOf(woli[0].id),false);
        labor[0].CAP_IR_Status__c = 'New';
        insert labor[0];
        List<Id> WoliId = new List<Id>();
        
       	WoliId.add(woli[0].id);
        
        Test.startTest();
        String interfaceLabel = 'Activity Cost Expense' + '|' + string.ValueOf(labor[0].Id);
        SFS_LaborHourOutboundHandler.Run(woliId);
        SFS_LaborHourOutboundHandler.IntegrationRespone(res, InterfaceLabel);
       	System.assert(res.getStatusCode()== 200);
       	Test.stopTest();
    }
    
    @IsTest
    public static void TestResubmit(){
         Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        List<Account> acc = SFS_TestDataFactory.createAccounts(1, false);
        acc[0].Type = 'Ship To';
        insert acc[0];
        Account accId = new Account();
        AccId = acc[0];
        Schema.Location ptLoc = new Schema.Location();
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        /*List<ServiceContract> sct = SFS_TestDataFactory.createServiceAgreement(1, accId.Id, true);
        ServiceContract sctr = new ServiceContract();
        sctr = sct[0];*/
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(2,div[0].Id,true);
       
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, accId.Id, loc[0].Id, div[0].Id, null, true);
		String woId = String.ValueOf(wo[0].id);
        
        WorkType wt = new WorkType();
        wt.Name = 'WOLITesting';
        wt.EstimatedDuration = 10.0;
        insert wt;
        List<WorkOrderLineItem> woli = SFS_TestDataFactory.createWorkOrderLineItem(1, wo[0].id,wt.id, true); 
        List<CAP_IR_Labor__c> labor = SFS_TestDataFactory.createLaborHours(1,woId,String.ValueOf(woli[0].id),false);
        labor[0].CAP_IR_Status__c = 'New';
        insert labor[0];
        List<Id> LaborId = new List<Id>();
        
       	LaborId.add(labor[0].id);
        
        Test.startTest();
        String interfaceLabel = 'Activity Cost Expense' + '|' + string.ValueOf(labor[0].Id);
        SFS_LaborHourResubmit.Resubmit(LaborId);
        SFS_LaborHourOutboundHandler.IntegrationRespone(res, InterfaceLabel);
       	System.assert(res.getStatusCode()== 200);
       	Test.stopTest();
    }
}