/**
 * @author           Amit Datta
 * @description      Test Class for B2BTaxCalculator.
 *
 * Modification Log
 * ------------------------------------------------------------------------------------------
 *         Developer                   Date                Description
 * ------------------------------------------------------------------------------------------
 *         Amit Datta                  21/02/2024          Original Version
 **/

@isTest
public with sharing class B2BTaxCalculatorTest {
    @isTest static void getSalesTaxByAddressTest() {

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('zipTaxAPITestResponse');
        mock.setStatusCode(200);
        Test.startTest();
        Double taxFromStaticMock = 0.06;
        Test.setMock(HttpCalloutMock.class, mock);
        B2BTaxCalculator b2bTaxCal = new B2BTaxCalculator();
        B2BTaxCalculator.TaxAddressWrapper addWrap = new B2BTaxCalculator.TaxAddressWrapper(); 
        addWrap.fullAddress = 'Valtech Detroit W 4th St. Royal Oak, MI 48067';
        addWrap.postalCode = '48067';
        addWrap.stateCode = 'MI';
        addWrap.city = 'Royal Oak';
        Double taxFromService = B2BTaxCalculator.getSalesTaxByAddress(addWrap);
        System.assertEquals(taxFromStaticMock, taxFromService, 'SalesTax does not match.');
        Test.stopTest();
        
    }
}