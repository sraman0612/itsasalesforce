/*
    This class serves as the controller for a visualforce page that is exposed in the gdiconnectedcustomer partner community.
    The visualforce page is used as an entry point for an external integration for the QuickPik functionality. The page is
    called with URL parameters that describe a bundle product that should be added to a quote. The quote may or may not exist.

    The page currently supports three parameters.
    Id = The Id of the existing quote if one exists.
    Prod = The Id of the Product2 record (bundle product) that should be added as a quoteline.
    SpecId = A text value that should be stored in the quote line's SpecId__c field.
* */

public class QuickPikReturnCntrl {

    private final Id pricebookId = [
        SELECT Id
        FROM Pricebook2
        WHERE Name = 'Standard Price Book'
    ][0].Id;

    private final Id OppRecordType = [
        SELECT Id
        FROM RecordType
        WHERE DeveloperName = 'Sales'
        AND SobjectType = 'Opportunity'
    ][0].Id;

    private final Id quoteRecordType = [
        SELECT Id
        FROM RecordType
        WHERE DeveloperName = 'Quote_Draft'
        AND SobjectType = 'SBQQ__Quote__c'
    ][0].Id;

    private Account distributorAccount;
    private Map<String, String> urlParams;

    public PageReference doRedirect() {

        PageReference page;
        system.debug(System.Site.getBaseUrl());

        try {

            urlParams = ApexPages.currentPage().getParameters();

            String prodId = urlParams.get('Prod');

            if(prodId == null || [SELECT Id FROM Product2 WHERE Id = :prodId].size() != 1){

                outputToUser(ApexPages.Severity.ERROR, 'Invalid Product[' + prodId + '] = ' + [SELECT Id FROM Product2 WHERE Id = :prodId].size());
                return null;
            }

            User currentUser = [
                SELECT Partner_Account_ID__c
                FROM User
                WHERE Id = :UserInfo.getUserId()
                LIMIT 1
            ];

            distributorAccount = [
                SELECT Name
                FROM Account
                WHERE Id = :currentUser.Partner_Account_ID__c
                LIMIT 1
            ];

            // get quote
            QuoteModel quoteModel = getQuote((Id) urlParams.get('Id'));

            // add quoteline
            quoteModel = addLine(quoteModel, urlParams.get('Prod'), urlParams.get('SpecId'));

            // update quote
            SBQQ.ServiceRouter.save('SBQQ.QuoteAPI.QuoteSaver', JSON.serialize(quoteModel));

            // redirect to configurator
            String url = System.Site.getBaseUrl() + '/s/sfdcpage/%2Fapex%2FSBQQ__sb%3F%26id%3D' + quoteModel.record.Id;
            //page = new PageReference(CPQ_URL__c.getOrgDefaults().CPQ_URL__c + quoteModel.record.Id);
            page = new PageReference(url);
            page.setRedirect(true);
        } catch(Exception e) {
            page = null;
            outputToUser(ApexPages.Severity.ERROR, e.getMessage());
        }

        return page;
    }

    private void outputToUser(ApexPages.Severity severity, String message) {
        ApexPages.addMessage(new ApexPages.Message(severity, message));
    }
    private QuoteModel getQuote(Id quoteId) {

        QuoteModel quoteModel;
        SBQQ__Quote__c quoteRecord;

        String channel = [
            SELECT FLD_Distribution_Channel__c
            FROM Product2
            WHERE Id = :urlParams.get('Prod')
        ][0].FLD_Distribution_Channel__c;

        if(quoteId == null){

            Opportunity newOpp = new Opportunity(
                AccountId = distributorAccount.Id,
                RecordTypeId = OppRecordType,
                CloseDate = System.today(),
                Name = 'QuickPik - ' + distributorAccount.Name,
                SBQQ__QuotePricebookId__c = pricebookId,
                Sales_Channel__c = channel,
                StageName = 'Stage 1 - Qualified Lead'
            );

            insert newOpp;

            quoteRecord = new SBQQ__Quote__c(
                SBQQ__Account__c = distributorAccount.Id,
                SBQQ__PricebookId__c = pricebookId,
                RecordTypeId = quoteRecordType,
                SBQQ__Opportunity2__c = newOpp.Id,
                SBQQ__Primary__c = true,
                Distribution_Channel__c = channel
            );

            insert quoteRecord;
            quoteId = quoteRecord.Id;
        }

        String quoteJSON = SBQQ.ServiceRouter.read('SBQQ.QuoteAPI.QuoteReader', quoteId);
        quoteModel = (QuoteModel) JSON.deserialize(quoteJSON, QuoteModel.class);

        return quoteModel;
    }
    private QuoteModel addLine(QuoteModel quote, String productId, String specId) {

        final String currencyCode = 'USD';//UserInfo.getDefaultCurrency();
        final ProductReaderContext productContext = new ProductReaderContext(pricebookId, currencyCode);

        final String productJSON = SBQQ.ServiceRouter.load(
            'SBQQ.ProductAPI.ProductLoader',
            productId,
            JSON.serialize(productContext)
        );

        final ProductModel productModel = (ProductModel) JSON.deserialize(productJSON, ProductModel.class);

        final AddProductsContext addProductRequestInfo = new AddProductsContext(
            quote,
            new List<ProductModel>{
                    productModel
            },
            0
        );

        final String quoteJSON = SBQQ.ServiceRouter.load(
            'SBQQ.QuoteAPI.QuoteProductAdder',
            null,
            JSON.serialize(addProductRequestInfo)
        );

        QuoteModel result = (QuoteModel) JSON.deserialize(quoteJSON, QuoteModel.class);

        // the below logic only works because we are adding only one new quoteline. If the use case is ever expanded
        // to where more than one line is being added in the QuickPik tool, another solution will be needed to ensure
        // the appropriate specId makes it to the correct quote line.
        if(!String.isBlank(specId)){

            for(QuoteLineModel line : result.lineItems){

                if(line.record.Id == null){
                    line.record.SpecId__c = specId;
                    insert line.record;
                    break;
                }
            }
        }

        return result;
    }
    private class ProductReaderContext {

        private Id pricebookId;
        private String currencyCode;

        private ProductReaderContext(Id pricebookId, String currencyCode) {
            this.pricebookId = pricebookId;
            this.currencyCode = currencyCode;
        }
    }
    private class AddProductsContext {

        private QuoteModel quote;
        private ProductModel[] products;
        private Integer groupKey;
        private final Boolean ignoreCalculate = true; //Must be hardcoded to true

        private AddProductsContext(QuoteModel quote, ProductModel[] products, Integer groupKey) {
            this.quote = quote;
            this.products = products;
            this.groupKey = groupKey;
        }
    }
}