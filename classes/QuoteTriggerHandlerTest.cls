@isTest
public class QuoteTriggerHandlerTest {
    
    public static SBQQ__Quote__c createQuote(String status, Integer countOfCompressors, Integer countOfFilters, Integer countOfDryers, Integer countOfPiping, Integer countOfOWs, Boolean dismissFilterAlert, Boolean dismissPipingAlert) {
                Account testAccount = TSTU_SFAccount.createTestAccount();
        testAccount.Name = 'test';
        upsert testAccount;

        Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'TEST OPP';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.AccountId = testAccount.Id;
        opp.Pricebook2Id = pricebookId;

        TSTU_SFOpportunity.upsertOpportunity(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        TSTU_SFOpportunity.upsertOpportunity(opp);

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__Primary__c = true;
        quote.End_Customer_Account__c = testAccount.Id;
        quote.Distribution_Channel__c = 'CM';
        quote.SBQQ__Account__c = testAccount.Id;
        quote.Count_of_Compressors__c = countOfCompressors;
        quote.Count_of_Filters__c = countOfFilters;
        quote.Count_of_Dryers__c = countOfDryers;
        quote.Count_of_Piping__c = countOfPiping;
        quote.Count_of_OWSs__c = countOfOWs;
        quote.Dismiss_Filter_Alert__c = dismissFilterAlert;
        quote.Dismiss_Piping_Alert__c = dismissPipingAlert;
        quote.SBQQ__Status__c = 'Draft';
        quote.My_Quote_Terms_and_Conditions__c = 'first';
        
        upsert quote;

        return quote;
    }

    @isTest
    public static void test1() {
        SBQQ__Quote__c quote = createQuote('Quote Validated', 0, 0, 0, 0, 0, false, false);

        quote.ApprovalStatus__c = 'Rejected';
        quote.SBQQ__Status__c = 'Draft';

        update quote;
    }

    @isTest
    public static void test2() {
        SBQQ__Quote__c quote = createQuote('Quote Validated', 1, 0, 0, 0, 0, false, false);

        quote.ApprovalStatus__c = 'Rejected';
        quote.SBQQ__Status__c = 'Draft';

        update quote;
    }

    @isTest
    public static void test3() {
        SBQQ__Quote__c quote = createQuote('Draft', 1, 0, 1, 0, 0, false, false);

        quote.ApprovalStatus__c = 'Approved';
        quote.SBQQ__Status__c = 'Quote Validated';

        update quote;
    }
    
    @isTest
    public static void test4() {
        SBQQ__Quote__c quote = createQuote('Draft', 1, 0, 1, 0, 0, false, false);

        quote.ApprovalStatus__c = 'Pending';
        quote.SBQQ__Status__c = 'Quote Validated';

        update quote;
    }
    
    @isTest
    public static void test5() {
        SBQQ__Quote__c quote = createQuote('Draft', 1, 1, 1, 0, 0, false, false);

        quote.ApprovalStatus__c = 'Pending';
        quote.SBQQ__Status__c = 'Quote Validated';

        update quote;
    }
    
    @isTest
    public static void test6() {
        SBQQ__Quote__c quote = createQuote('Draft', 1, 1, 1, 1, 0, false, false);

        quote.ApprovalStatus__c = 'Pending';
        quote.SBQQ__Status__c = 'Quote Validated';

        update quote;
    }
}