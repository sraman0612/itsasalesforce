/*
    Serverside controller for the DistributorWarehouseSearch Aura component
*/
public with sharing class DistributorWarehouseSearchController {

    @testVisible
    static final Id GLOBAL_USER_PROFILE_ID = [select Id from Profile Where Name = 'Partner Community - Global Account User'][0].Id;

    /*
        Initialization Method
    * */
    @AuraEnabled 
    public static ViewData doInit(String accId, String dcAccountId, String channel){
        System.debug(LoggingLevel.ERROR, accId );
        System.debug(LoggingLevel.ERROR, channel );
        
        //accId = '001j000000dHUZCAA4'; //Drew Hoffman's Account
        /*
            if the accId parameter is null, it means it is a true initialization.
            if the accId parameter has a value, it means the global user just switched
            the account context they want the component to run under the new account
        */

        ViewData iData = new ViewData();
        iData.accountOptions = new List<AccountOption>();
        iData.isGlobalUser = false;
        
        //String userId = '005f100000HJZ8FAAX'; //Drew Hoffman
        String userId = UserInfo.getUserId();

        system.debug(UserInfo.getUserId());
        User currentUser = [Select AccountId, Account.ParentId, ProfileId from User where Id = : userId];
        system.debug(currentUser.AccountId);
        iData.activeAccountId = currentUser.AccountId;
		system.debug(currentUser.AccountId);
        
        if(accId != null && accId != ''){
            iData.activeAccountId = accId;
            iData.activeAccount = null;
        }

        if(currentUser.ProfileId == GLOBAL_USER_PROFILE_ID && currentUser.Account.ParentId != null){

            iData.isGlobalUser = true;

            for(Account tmp : [select Name, Account_Number__c from Account where ParentId = :currentUser.Account.ParentId AND Child_Account_Status__c != 'NO ACTIVE CHILD ACCOUNTS' LIMIT 1000]){
                iData.accountOptions.add(new AccountOption(tmp.Id,tmp.Name + ' - ' + tmp.Account_Number__c,(tmp.Id == iData.activeAccountId)));
            }
        }

        System.debug(LoggingLevel.ERROR, iData );

        List<Account> accs = new List<Account>([
            SELECT  Account_Number__c,
            		AccountNumber,
                    BillingStreet,
                    BillingCity,
                    BillingState,
                    BillingPostalCode,
                    Name,
                    Phone,
                    Points__c,
                    RequiredStockLevel_DC__c,
                    Share_Assets__c,
                    Child_DCs__c
            FROM    Account
            WHERE   Id = :iData.activeAccountId
        ]);

        if( !accs.isEmpty() ) {

            Map<String, Distribution_Channel__mdt> channelsByCode = new Map<String, Distribution_Channel__mdt>();

            for(Distribution_Channel__mdt dc : [
                SELECT  DeveloperName,
                        MasterLabel
                FROM    Distribution_Channel__mdt
                LIMIT   10000
            ]){

                channelsByCode.put(dc.DeveloperName, dc);
            }

            iData.availableChannels = new List<Distribution_Channel__mdt>();

            if(String.isNotBlank(accs[0].Child_DCs__c)){
                for(String channelCode : accs[0].Child_DCs__c.split(',|;')){

                    if(channelsByCode.containsKey(channelCode)){
                        iData.availableChannels.add(channelsByCode.get(channelCode));
                    }
                }
            }
            
            if (String.isBlank(channel) && !iData.availableChannels.isEmpty()) {
                channel = iData.availableChannels.get(0).DeveloperName;
            }
            
        	iData.activeAccount = accs[0];
            iData.myInventory = new MyInventory(getInventory(iData.activeAccountId, channel), channel, iData.activeAccount);

            if(String.isNotBlank(iData.myInventory.dcAccountId)) {
                iData.dcAccountId = iData.myInventory.dcAccountId;
                iData.dcAccount = iData.myInventory.dcAccount;
            }
        }
        else {
            if(System.Test.isRunningTest() == FALSE) {System.assert(false, 'Account not found');}
        }

        //todo: remove debugInfo
        //iData.debugInfo = JSON.serialize(iData);
        return iData;
    }

    /*
        Search method for "Search Other Distributors"
    */
	@AuraEnabled public static SearchResult search( Integer rowsPerPage,
                                                    Integer pageNumber,
                                                    String modelNumber,
                                                    String descr,
                                                    Integer power,
                                                    Integer pressure,
                                                    Integer voltage,
                                                    String channel){

        // assert valid parameters
        System.assert(rowsPerPage != null && rowsPerPage > 0, 'Must have at least one row per page');
        System.assert(pageNumber != null && pageNumber >= 0, 'Invalid page number specified');

        // SOQL Injection prevention
        modelNumber = String.isNotBlank(modelNumber) ? String.escapeSingleQuotes(modelNumber) : null;
        descr = descr != null ? String.escapeSingleQuotes(descr) : null;
        channel = channel != null ? String.escapeSingleQuotes(channel) : null;

        Integer offset = rowsPerPage * pageNumber;

        // String query =
        //     'Select ' +
        //         'serial_number__c, ' +
        //         'serial_number__r.Model_Number__c, ' +
        //         'serial_number__r.name, ' +
        //         'serial_number__r.Account.Name, ' +
        //         'serial_number__r.Account.ShippingCity, ' +
        //         'serial_number__r.Account.Phone, ' +
        //         'serial_number__r.Product2.Name, ' +
        //         'serial_number__r.MPG4_Code__r.Distributor_Warehouse_points__c, ' +
        //         'serial_number__r.Horsepower__c, ' +
        //         'serial_number__r.pressure__c, ' +
        //         'serial_number__r.Voltage__c, ' +
        //         'serial_number__r.enclosure__c, ' +
        //         'serial_number__r.Start_up_Date__c, ' +
        //         'serial_number__r.ship_Date__c, ' +
        //         'name ' +
        //     'from Asset_share__c ';

        String query =
            'Select ' +
                'serial_number__c, ' +
                'Model_Number__c, ' +
                'serial_number__r.name, ' +
                'Account_Name__c, ' +
                'Shipping_City__c, ' +
                'Phone__c, ' +
                'Horsepower__c, ' +
                'pressure__c, ' +
                'Voltage__c, ' +
                'enclosure__c, ' +
                'name ' +
            'from Asset_share__c ';

        String whereClause = 'WHERE Share__c = true';

        if (modelNumber != null){
            whereClause += ' AND Model_Number__c LIKE \'%'+modelNumber+'%\'';
        }

        if (power != null && power > 0){
            whereClause += ' AND Horsepower__c >= '+ power;
        }

        if (pressure != null && pressure > 0){
            whereClause += ' AND pressure__c >= '+ pressure;
        }

        if (voltage != null && voltage > 0){
            whereClause += ' AND Voltage__c >= '+ voltage;
        }

        if (channel != null && channel.length() > 0){
            whereClause += ' AND DChl__c = \''+ channel +'\'';
        }

        String pageination = ' LIMIT :rowsPerPage OFFSET :offset';

        Integer count = database.countQuery('Select count() from Asset_share__c ' + whereclause);
        List<Asset_Share__c> records = database.query(query + whereclause + pageination);
        System.debug(records);
        SearchResult result = new SearchResult(records, count, rowsPerPage);

        return result;
    }

    /*
        Method to hide assets from view in the distributor warehouse
    */
    @AuraEnabled public static void removeFromDW(List<Id> assetIds) {

        List<Asset> machinesToHide = new List<Asset>();

        for(Asset machine : [select Hide_in_Distributor_Warehouse__c from Asset where Id IN :assetIds]){

            machine.Hide_in_Distributor_Warehouse__c = true;
            machinesToHide.add(machine);
        }

        if(!machinesToHide.isEmpty()){
            update machinesToHide;
        }
    }
    

    /*
        Returns the assets under a specific account
    */
    static List<Asset> getInventory(Id accId, String channel) {

        String query =
            'SELECT ' +
                ' Name, ' +
                ' Model_Number__c, ' +
                ' HorsePower__c, ' +
                ' DChl__c, ' +
                ' Ship_Date__c, ' +
                ' DelDate__c, ' +
                ' Points__c, ' +
                ' Voltage__c, ' +
                ' Pressure__c, ' +
                ' Enclosure__c, ' +
                ' DC_Child_Account__c, ' +
                ' DC_Child_Account__r.RequiredStockLevel_DC__c, ' +
                ' DC_Child_Account__r.Account_Number__c, ' +
                ' DC_Child_Account__r.Points__c ' +
            ' FROM 	Asset ' +
            ' WHERE Show_in_DW__c = true ' +
            ' 	AND AccountId = \'' + accId + '\' ';

        if(String.isNotEmpty(channel)){
            query += ' AND DChl__c = \'' + channel + '\'';
        }

        query += ' ORDER BY DelDate__c desc';

        return database.query( query );
    }


    // DATA STRUCTURES

    public class AccountOption {

        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public Boolean disabled;

        public AccountOption(String val, String labl, Boolean isDisabled){
            this.value = val;
            this.label = labl;
            this.disabled = isDisabled;
        }
    }

    public class ViewData {

        @AuraEnabled public Id activeAccountId;
        @AuraEnabled public Boolean isGlobalUser;
        @AuraEnabled public List<AccountOption> accountOptions;//List<SelectOption> not aura supported
        @AuraEnabled public Account activeAccount;
        @AuraEnabled public Id dcAccountId;
        @AuraEnabled public Account dcAccount;
        @AuraEnabled public MyInventory myInventory;
        @AuraEnabled public List<Distribution_Channel__mdt> availableChannels;
        @AuraEnabled public String debugInfo;
    }

    public class MyInventory {

        @AuraEnabled public List<InventoryItem> inventoryItems = new List<InventoryItem>();
        @AuraEnabled public Id dcAccountId;
        @AuraEnabled public Account dcAccount = new Account();

        public MyInventory(List<Asset> assets){
            for(Asset tmpAsset : assets){
                this.inventoryItems.add(new InventoryItem(tmpAsset));
            }
            System.debug('#### inventoryItems = ' + inventoryItems);
        }
        public MyInventory(List<Asset> assets, String channel, Account activeAccount){
            String childAccountNumber = activeAccount.AccountNumber;
            if (String.isNotBlank(channel)) {
                childAccountNumber +=  ' - ' + channel;
            }
            System.debug('childAccountNumber=' + childAccountNumber);
            try {
                System.debug([SELECT Id, Account_Number__c, Name, Points__c, RequiredStockLevel_DC__c from Account where Account_Number__c = '312755 - CM']);
				Account childAccount = [SELECT Id, Account_Number__c, Name, Points__c, RequiredStockLevel_DC__c from Account where Account_Number__c = : childAccountNumber LIMIT 1];
                System.debug('found child account ' + childAccount.Account_Number__c);
                dcAccountId = childAccount.Id;
                dcAccount.Points__c = childAccount.Points__c;
                dcAccount.RequiredStockLevel_DC__c = childAccount.RequiredStockLevel_DC__c;
                dcAccount.Account_Number__c = childAccount.Account_Number__c;
            } catch (Exception e) {
                System.debug('child account not found');
                dcAccountId = null;
                dcAccount = null;
            }
            
            if (assets == null || assets.size() == 0) {
                return;
            }

            for(Asset tmpAsset : assets){
                System.debug('####@@@@ asset = ' + tmpAsset);
                this.inventoryItems.add(new InventoryItem(tmpAsset));
            }
            
            System.debug('####@@@ inventoryItems = ' + inventoryItems[inventoryItems.size()-1]);
        }
    }

    public class InventoryItem {

        @AuraEnabled public String Id;
        @AuraEnabled public String modelNumber;
        @AuraEnabled public String name;
        @AuraEnabled public Decimal horsePower;
        @AuraEnabled public String iconName;
        @AuraEnabled public String placeHolder;
        @AuraEnabled public Date shipDate;
        @AuraEnabled public String points;
        @AuraEnabled public String channel;

        public InventoryItem(Asset record){

            this.Id = record.Id;
            this.modelNumber = record.Model_Number__c;
            this.name = record.Name;
            this.horsePower = record.HorsePower__c;
            this.shipDate = record.DelDate__c;
            this.points = String.valueOf(record.Points__c);
            this.channel = record.DChl__c != null ? record.DChl__c.toUpperCase() : '';
            this.placeHolder = '';
            
            System.debug('#### item = ' + this);

            if(record.DelDate__c != null && record.DelDate__c > System.today().addYears(-2)){
                this.iconName = 'action:approval';
            }
            else {
                this.iconName = 'action:close';
                this.points = '0';
            }
        }
    }

    public class SearchResult {

        @AuraEnabled public List<SearchResultItem> resultItems = new List<SearchResultItem>();
        @AuraEnabled public Integer totalRowCount;
        @AuraEnabled public Integer totalPageCount;

        public SearchResult(List<Asset_Share__c> shares, Integer totalRecordCount, Integer recordsPerPage){

            System.assert(recordsPerPage != null && recordsPerPage > 0, 'Must have at least one row per page');

            this.totalRowCount = totalRecordCount;

            Integer totalPages;

            if(totalRecordCount == 0){
                totalPages = 0;
            }
            else {

                totalPages = totalRecordCount / recordsPerPage;

                if(Math.mod(totalRecordCount, recordsPerPage) > 0){
                    totalPages++;
                }
            }

            this.totalPageCount = totalPages;

            for(Asset_Share__c share : shares){
                this.resultItems.add(new SearchResultItem(share));
            }
        }
    }

    public class SearchResultItem {

        @AuraEnabled public String Id;
        @AuraEnabled public String modelNumber;
        @AuraEnabled public String name;
        @AuraEnabled public Decimal horsePower;
        @AuraEnabled public Decimal voltage;
        @AuraEnabled public Decimal pressure;
        @AuraEnabled public String enclosure;
        @AuraEnabled public String distName;
        @AuraEnabled public String distCity;
        @AuraEnabled public String distPhone;

        public SearchResultItem(Asset_Share__c record){

            this.Id = record.Serial_Number__c;//record.Id;
            this.modelNumber = record.Model_Number__c;
            this.name = record.Serial_Number__r.Name;
            this.horsePower = record.HorsePower__c;
            this.voltage = record.Voltage__c;
            this.pressure = record.Pressure__c;
            this.enclosure = record.Enclosure__c;
            this.distName = record.Account_Name__c;
            this.distCity = record.Shipping_City__c;
            this.distPhone = record.Phone__c;
            // this.horsePower = record.Serial_Number__r.HorsePower__c;
            // this.modelNumber = record.Serial_Number__r.Model_Number__c;
            // this.voltage = record.Serial_Number__r.Voltage__c;
            // this.pressure = record.Serial_Number__r.Pressure__c;
            // this.enclosure = record.Serial_Number__r.Enclosure__c;
            // this.distName = record.Serial_Number__r.Account.Name;
            // this.distCity = record.Serial_Number__r.Account.ShippingCity;
            // this.distPhone = record.Serial_Number__r.Account.Phone;
        }
    }
}