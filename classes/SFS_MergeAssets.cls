/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 13/12/2021
* @description: SFS_MergeAssets
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001     SIF-183  13/12/2021  Merge Assets
============================================================================================================================================================*/
public class SFS_MergeAssets {
    
    @InvocableMethod(label='SFS_Merge_Assets' description='Merge Assets' category='Asset')
    public static List<String> mergeAssets(List<Id> mergeId) {
        Asset assetList;
       // Asset assetList2;
        Savepoint sp;
        try{
            assetList = [Select Id, ParentId,Parent.AccountId,AccountId,SFS_Merged__c  FROM Asset where Id =: mergeId[0]];
            Id masterId = assetList.ParentId;
           // assetList2 = [Select Id, AccountId  FROM Asset where Id =: masterId];
            
            Id account =assetList.Parent.AccountId;
            List<Case> caseList = [Select Id, AssetId FROM Case where AssetId =: mergeId];
            List<WorkOrder> woList = [Select Id, AccountId,AssetId,SFS_Asset_Merge__c FROM WorkOrder where AssetId =: mergeId];
            List<WorkOrderLineItem> woliList = [Select Id, AssetId FROM WorkOrderLineItem where AssetId =: mergeId];
            List<ProductRequestLineItem> prliList = [Select Id, SFS_Asset__c FROM ProductRequestLineItem where SFS_Asset__c =: mergeId];
            List<Entitlement> eList = [Select Id, AssetId FROM Entitlement where AssetId =: mergeId];
            List<WorkPlanSelectionRule> wpsrList = [Select Id, AssetId FROM WorkPlanSelectionRule where AssetId =: mergeId];
            sp = Database.setSavepoint();
            system.debug('@@@asset'+masterId);
            system.debug('@@@account'+account);
            //Updating Case with MasterId
            if(caseList.size() > 0){
                for(Case cl : caseList){
                    cl.AssetId = masterId;
                } 
                update caseList;
            }
            
            //Updating Entitlement with MasterId
            if(eList.size() > 0){
                for(Entitlement el : eList){
                    el.AssetId = masterId;
                }
                update eList;
            }
            
            //Updating WorkOrderLineItem with MasterId
            if(woliList.size() > 0){
                for(WorkOrderLineItem woli : woliList){
                    woli.AssetId = masterId;
                    system.debug('@@@insidewoliloop');
                } 
                update woliList;
            }
            
            //Updating WorkOrder with MasterId
            if(woList.size() > 0){
                for(WorkOrder wo : woList){
                    wo.AccountId=account;
                    wo.AssetId = masterId;
                    wo.SFS_Asset_Merge__c =true;
                    system.debug('@@@insideloop');
          
                } 
                system.debug('@@@wolist'+woList);
                update woList;
            }
           
            
            //Updating ProductRequestLineItem with MasterId
            if(prliList.size() > 0){
                for(ProductRequestLineItem prli : prliList){
                    prli.SFS_Asset__c = masterId;
                } 
                update prliList;
            }
            
            //Updating WorkPlanSelectionRule with MasterId
            if(wpsrList.size() > 0){
                for(WorkPlanSelectionRule wpsr : wpsrList){
                    wpsr.AssetId = masterId;
                }
                update wpsrList;
            }
            String su = 'Success';
            List<String> result = new List<String>();
            result.add(su);
            return result;
          }
        
        catch(Exception e){
            String er = 'Error';
            List<String> result = new List<String>();
            result.add(er);
            Database.rollback(sp);
            system.debug('@@@exceptionMergerAssets'+e.getMessage());
            return result;
        }
    }
}