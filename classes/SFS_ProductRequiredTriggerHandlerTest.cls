/**=========================================================================================================
* @author: Bhargavi Nerella, Capgemini
* @date: 05/05/2022
* @description: Test Class for SFS_ProductRequiredTrigger and SFS_ProductRequiredTriggerHandler.
  Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

=============================================================================================================*/
@isTest
public class SFS_ProductRequiredTriggerHandlerTest {
//test method for deleteRecords in SFS_ProductRequiredTriggerHandler
    Static testmethod void testDeletion(){
        List<CTS_IOT_Frame_Type__c> FrameTypeList=SFS_TestDataFactory.createFrameTypes(1,false,'Compressor');
        insert FrameTypeList;
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1,false);
        assetList[0].CTS_Frame_Type__c=FrameTypeList[0].Id;
        insert assetList;
        List<Product2> productList=SFS_TestDataFactory.createProduct(2,false);
        insert productList;
        List<CAP_IR_Asset_Part_Required__c> aspList=SFS_TestDataFactory.createPartsRequired(2,assetList[0].Id,null,false);
        aspList[0].Product__c=productList[0].Id;
        aspList[1].Product__c=productList[1].Id;
        insert aspList;
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,null,null,null,null,false);
        insert woList;
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        workTypeList[0].CTS_Frame_Type__c=FrameTypeList[0].Id;
        insert workTypeList;
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,workTypeList[0].Id,false);
        woliList[0].AssetId=assetList[0].Id;
        insert woliList;
        List<ProductRequired> productRequiredList=SFS_TestDataFactory.createProductRequired(1,woliList[0].Id,productList[0].Id,false);
        Test.startTest();
        insert productRequiredList;
        Test.stopTest();
    }
    Static testmethod void testCreateAsset(){
        List<CTS_IOT_Frame_Type__c> FrameTypeList=SFS_TestDataFactory.createFrameTypes(1,false,'Compressor');
        insert FrameTypeList;
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1,false);
        assetList[0].CTS_Frame_Type__c=FrameTypeList[0].Id;
        insert assetList;
        List<Product2> productList=SFS_TestDataFactory.createProduct(2,false);
        insert productList;
        List<CAP_IR_Asset_Part_Required__c> aspList=SFS_TestDataFactory.createPartsRequired(2,assetList[0].Id,null,false);
        aspList[0].Product__c=productList[0].Id;
        aspList[1].Product__c=productList[1].Id;
        insert aspList;
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,null,null,null,null,false);
        insert woList;
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        workTypeList[0].CTS_Frame_Type__c=FrameTypeList[0].Id;
        insert workTypeList;
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,workTypeList[0].Id,false);
        woliList[0].AssetId=assetList[0].Id;
        insert woliList;
        List<ProductRequired> productRequiredList=SFS_TestDataFactory.createProductRequired(1,workTypeList[0].Id,productList[0].Id,false);
        Test.startTest();
        insert productRequiredList;
        productRequiredList[0].SFS_2k__c=true;
        update productRequiredList;
        Test.stopTest();
    }
}