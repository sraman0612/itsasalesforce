public class SFS_splitStringForCredit {
     @invocableMethod(label = 'SplitIDs for Credit' description = 'Split ChargeIds' Category = 'Charge')
    public static List<ReturnVariables> Run( List<InputVariables> inputVariables){
        //get input 
        String passedInvLi = inputVariables.get(0).ids;
        //define output
        List<CAP_IR_Charge__c> chrgList = new List<CAP_IR_Charge__c>();
        
        List<Id> chargeid =new List<Id>();

        string sString = passedInvLi;
        string[] splitted = sString.split(';');
        
        Integer length = splitted.size();
        for(Integer i = 0; i < length; i++) {
            String s = splitted.get(i);
            chargeid.add(s);
        }
    
        chrgList=[select id,SFS_charge_details_for_credit__c,SFS_Charge_Type__c,CAP_IR_Description__c, SFS_Credit_Amount__c,SFS_Sell_Price__c from CAP_IR_Charge__c where id IN: chargeid ];
        List<ReturnVariables> returnVarsList =new List<ReturnVariables>();
        ReturnVariables returnVars = new ReturnVariables();
        returnVars.returnCharge =chrgList;
        returnVarsList.add(returnVars);
        return returnVarsList;
    }
    
    public class InputVariables{
        @InvocableVariable
        public String ids;
    }
    public class ReturnVariables{
        @InvocableVariable
        public List<CAP_IR_Charge__c> returnCharge;
    }

}