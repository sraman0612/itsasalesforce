@isTest
public with sharing class C_AssetShareCARL_Batch_Test {
    static list<string> uId = new List<string>();
    static list<string> cId = new List<string>();
    
    
    @testSetup static void setup()
    {
        
        List<contact> cList = new List<contact>();
        
        Account a1 = new account(name='Accnt Parent Test');
        insert a1;
        Account a = new account(name='Accnt Test',ParentId = a1.id);
        insert a;
        
        contact con5 = new contact(firstname='j2', lastname='b2', email='jbtest7@test.com', accountId = a.id);
        //insert con5;
        cList.add(con5);
        
        contact con6 = new contact(firstname='j', lastname='b', email='jbtest6@test.com', accountId = a.id);
        //insert con6;
        clist.add(con6);
        
        insert clist;
        
        string singleProfileId = [select id from profile where name='Partner Community - Single Account User'].id;
        string globalProfileId = [select id from profile where name='Partner Community - Global Account User'].id;
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        List<user> uaList = new List<user>();
        User ua1 = new User(
            ProfileId = globalProfileId,
            Firstname='First',
            LastName = 'last-community',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con6.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            IsActive = true
        );
        User ua2 = new User(
            ProfileId = singleProfileId,
            Firstname='First2',
            LastName = 'last-community',
            Email = 'puser0001@amamama.com',
            Username = 'puser0001@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con5.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            IsActive = true
        );
        uaList.add(ua1);
        uaList.add(ua2);
        insert uaList;        
        
        List<asset> aList = new List<asset>();
        List<Serial_Number_Contact__c> snList = new List<Serial_Number_Contact__c>();
        List<Service_History__c> shList = new List<Service_History__c>();
        
        Asset serial = new Asset();
        serial.SerialNumber = '1234567';
        serial.Name = 'test';
        serial.AccountId=a.id;
        serial.current_servicer__c = a1.id;
        //insert serial;
        aList.add(serial);
        
        Asset serial2 = new Asset();
        serial2.SerialNumber = '1234568';
        serial2.Name = 'test';
        serial2.AccountId=a1.id;
        serial2.current_servicer__c = a.id;
        //insert serial2;
        aList.add(serial2);
        
        Asset serial3 = new Asset();
        serial3.SerialNumber = '1234569';
        serial3.Name = 'test';
        serial3.AccountId=a1.id;
        serial3.current_servicer__c = a.id;
        //insert serial3;
        aList.add(serial3);
        insert aList;
        
        Child_Asset__c childTestAsset = new Child_Asset__c(Name = 'testChildAsset1', Parent_Asset__c = serial.Id, Current_Servicer__c = a.Id);
        insert childTestAsset;
        Child_Asset__c childTestAsset2 = new Child_Asset__c(Name = 'testChildAsset2', Parent_Asset__c = serial2.Id, Current_Servicer__c = a.Id);
        insert childTestAsset2;
        
        
        Serial_Number_Contact__c sc = new Serial_Number_Contact__c();
        sc.First_Name__c ='j';
        sc.Last_Name__c = 'b';
        sc.Relationship__c = 'End User';
        sc.Email_Address__c  = 'jbtest3@test.com';
        sc.Serial_Number__c  = serial2.id;
        //insert sc;
        
        Serial_Number_Contact__c sc1 = new Serial_Number_Contact__c();
        sc1.First_Name__c ='j';
        sc1.Last_Name__c = 't';
        sc1.Relationship__c = 'Distributor';
        sc1.Email_Address__c  = 'jbtest4@test.com';
        sc1.Serial_Number__c  = serial.id;
        
        //insert sc1;   
        snList.add(sc);
        snList.add(sc1);
        insert snList;
        test.starttest();
        serial.Current_Servicer__c = a1.id;
        //update serial;
        serial2.Current_Servicer__c = a1.id;
        serial3.Current_Servicer__c = a1.id;
        //update serial2;
        update aList;
        Service_History__c sh = new Service_History__c();
        sh.Serial_Number__c = serial.id;
        sh.Technician_Email__c ='jbtest1@test.com';
        sh.account__c = a.id;
        //insert sh; 
        
        Service_History__c sh2 = new Service_History__c();
        sh2.Serial_Number__c = serial2.id;
        sh2.Technician_Email__c ='jbtest2@test.com';
        sh2.account__c = a1.id;
        //insert sh2; 
        //List<user> uList = new List<user>(); 
        //insert u;//uList.add(u);
        //insert u2;//uList.add(u2);
        //insert u3;//uList.add(u3);
        //insert u4;//uList.add(u4);
        //insert u5;//uList.add(u5);
        //insert uList;
        
    }
    
    
    @isTest 
    public static void doTest() {
        
        //UserRole role = [SELECT Id FROM UserRole WHERE Name = 'SFDC System Administrator'];
        User thisUser = new User(Id = UserInfo.getUserId());
        
        //thisUser.UserRoleId = role.Id;
        //update thisUser;
        System.runAs(thisUser) {
            User globalUser = [SELECT Id FROM User WHERE Email = 'puser0001@amamama.com' LIMIT 1];
            User singleUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com' LIMIT 1];
            Account globalAccnt = [SELECT Id FROM Account WHERE Name = 'Accnt Parent Test' LIMIT 1];
            Account singleAccnt = [SELECT Id FROM Account WHERE Name = 'Accnt Test' LIMIT 1];
            System.debug('globalAccnt.Id' + globalAccnt.Id);
            System.debug('singleAccnt.Id' + singleAccnt.Id);
            C_AssetShareCARL_Batch globalBatch = new C_AssetShareCARL_Batch(globalAccnt.Id, globalUser.Id, false);
            C_AssetShareCARL_Batch singleBatch = new C_AssetShareCARL_Batch(singleAccnt.Id, singleUser.Id, false);
            Test.startTest();
            Database.executeBatch(globalBatch);
            Database.executeBatch(singleBatch);
            Test.stopTest();
        }
    }
    
    @isTest 
    public static void doTest2() {
        
        C_ChildAssetShare_Batch globalBatch = new C_ChildAssetShare_Batch();
        Test.startTest();
        Database.executeBatch(globalBatch);
        Test.stopTest();
        
    }
}