global class C_DeleteAssetSharingReconcile implements Database.Batchable<sObject>{ 
    global final String Query;
    
    global C_DeleteAssetSharingReconcile(){
        
        Query='select id from AssetShare';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }   
    
    
    global void execute(Database.BatchableContext info, List<AssetShare > scope){
        List<AssetShare > jobShrs  = new List<AssetShare>();
        
        system.debug(scope);
        if(scope.size()>0)
        {
            for(AssetShare  sh: scope)
            {
                system.debug('*****SHARE HERE INSERT***'+ sh); 
                jobShrs.add(sh);
            }
            
            if(jobShrs.size()>0)
                system.debug(jobShrs);
            delete jobShrs;
        }
    }     
    global void finish(Database.BatchableContext info){     
    } 

}