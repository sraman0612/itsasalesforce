/*=========================================================================================================
* @author Manimozhi, Capgemini
* @date 10/08/2023
* @description:
* @Story Number: 

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================*/


public class SFS_Invoice_submit_batch implements Database.Batchable<sObject>, Schedulable{
    
    public Database.QueryLocator start (Database.BatchableContext bc){
        Id invRecType =[select id from RecordType where DeveloperName ='SFS_WO_Invoice'].Id;
        return Database.getQueryLocator([SELECT Id,SFS_Work_Order__c FROM Invoice__c WHERE SFS_Status__c = 'Pending Submission' And RecordTypeId =:invRecType]);
    }
    
    public void execute(Database.BatchableContext BC, list<Invoice__c> invoiceList){
        Set<Id> woIds = new Set<Id>();
        Set<Id> woliIds = new Set<Id>();
        Set<Id> woliIds2 = new Set<Id>();
        Set<Id> removeWOLIIds = new Set<Id>();
         if(invoiceList.size()>0){
        for(Invoice__c invoice :invoiceList){
            woIds.add(invoice.SFS_Work_Order__c);
        }
         }
        List<WorkOrderLineItem> woliList= new List<WorkOrderLineItem>();
        if(woIds.size()>0){
        woliList =([Select Id from WorkOrderLineItem where WorkOrderId IN:woIds AND Status='Closed' ]);
        }
        for(WorkOrderLineItem woli :woliList){
            woliIds.add(woli.Id);
            woliIds2.add(woli.Id);
        }       
        List<ProductConsumed> pcList= new List<ProductConsumed>();
        if(woliIds.size()>0){
            pcList=([Select Id,SFS_Integration_Status__c,WorkOrderLineItemId from ProductConsumed where WorkOrderLineItemId IN:woliIds AND SFS_Integration_Status__c!='APPROVED' ]);
        }
        if(pcList.size()>0){
            for(ProductConsumed pc :pcList){
                woliIds.remove(pc.WorkOrderLineItemId);
                removeWOLIIds.add(pc.WorkOrderLineItemId);
            }
        }
        List<Expense> expenseList= new List<Expense>();
        if(woliIds.size()>0){
            expenseList=([Select Id,SFS_Integration_Status__c,SFS_Work_Order_Line_Item__c from Expense where SFS_Work_Order_Line_Item__c IN:woliIds AND SFS_Integration_Status__c!='APPROVED' AND ((SFS_Transaction_Status__c!='CLOSED FOR RECEIVING' AND ExpenseType='Purchased Expense') OR (NOT ExpenseType='Purchased Expense')) ]);
        }
        if(expenseList.size()>0){
            for(Expense exp :expenseList){
                woliIds.remove(exp.SFS_Work_Order_Line_Item__c);
                removeWOLIIds.add(exp.SFS_Work_Order_Line_Item__c);
            }
        }
        List<CAP_IR_Labor__c> laborList= new List<CAP_IR_Labor__c>();
        if(woliIds.size()>0){
            laborList=([Select Id,SFS_Integration_Status__c,CAP_IR_Work_Order_Line_Item__c from CAP_IR_Labor__c where CAP_IR_Work_Order_Line_Item__c IN:woliIds AND SFS_Integration_Status__c!='APPROVED']);
        }
        if(laborList.size()>0){
            for(CAP_IR_Labor__c lab :laborList){
                woliIds.remove(lab.CAP_IR_Work_Order_Line_Item__c);
                removeWOLIIds.add(lab.CAP_IR_Work_Order_Line_Item__c);
            }
        }
        Set<Invoice__c> finalInvoiceListSet= new Set<Invoice__c>(); 
        List<Invoice__c> finalInvoiceList= new List<Invoice__c>(); 
        List<Invoice__c> updateInvoiceList= new List<Invoice__c>(); 
        List<WorkOrderLineItem> eliminatingWOLIList= new List<WorkOrderLineItem>(); 
        Set<Id> eliminatingWOIds= new Set<Id>();
        Set<Id> invIds= new Set<Id>();
        Set<Id> finalWOIds = new Set<Id>();
        if(removeWOLIIds.size()>0){
            eliminatingWOLIList=([Select Id,WorkOrderId from WorkOrderLineItem where Id IN:removeWOLIIds]);
        }
        if(eliminatingWOLIList.size()>0){
            for(WorkOrderLineItem wolis :eliminatingWOLIList){                    
                eliminatingWOIds.add(wolis.WorkOrderId);            
            }
        }
        if(eliminatingWOIds.size()>0){
            for(Id wo:eliminatingWOIds){
                woIds.remove(wo); 
            }
        }
        if(woIds.size()>0){
            finalInvoiceList = ([Select Id,SFS_Status__c,SFS_Work_Order__c from Invoice__c where SFS_Work_Order__c IN:woIds and SFS_Status__c='Pending Submission' order by LastModifiedDate asc  limit 1]);
        }
        
        if(finalInvoiceList.size()>0){
            for(Invoice__c lastInv :finalInvoiceList){ 
                lastInv.SFS_Status__c='Submitted';
                finalInvoiceListSet.add(lastInv);
                invIds.add(lastInv.Id);
                
            }
            if(!finalInvoiceListSet.isEmpty()){
             updateInvoiceList.addall(finalInvoiceListSet);
            }
        }
       
        if(updateInvoiceList.size()>0){
            System.enqueueJob(new SFS_setInvoiceSubmittedQueueable(updateInvoiceList));
        }
        
    }
    public void finish(Database.BatchableContext BC){
        
    }
    public void execute(SchedulableContext SC){
        SFS_Invoice_submit_batch inv =new SFS_Invoice_submit_batch();
        Database.executeBatch(inv,1);
    }
}