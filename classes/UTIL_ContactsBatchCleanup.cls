public with sharing class UTIL_ContactsBatchCleanup
    implements Database.Batchable<SObject>, Database.Stateful
{
    // Cleanup process for UTIL_ContactsSync_Batch();

    // jobInfo contains debug information persisted across contexts since start(),
    //   execute(), and finish() all run in separate contexts with separate debug logs
    private List<String> jobInfo = new List<String>();
    private List<SObject> errors = new List<SObject>();

    // start()
    //
    // Start the batch job
    public Database.QueryLocator start(Database.BatchableContext context)
    {
        System.debug(context.getJobId() + ' Starts');

        try
        {
            String query = buildQueryString();
            return Database.getQueryLocator(query);
        }
        catch(Exception ex)
        {
            System.debug('Failed querying Contacts: ' + ex.getMessage());
        }

        // Returning null causes "System.UnexpectedException: Start did not return a valid iterable object."
        // So to NOOP we must return a query that will always give 0 results. Id should never be
        // null in any table so we can arbitrarily pick Contact.
        return Database.getQueryLocator([SELECT Id FROM Contact WHERE Id = null]);
    }

    // execute()
    //
    // Execute the batch job
    public void execute(Database.BatchableContext context, List<SObject> contacts)
    {
        System.debug(context.getJobId() + ' Executing');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);

        Set<String> customerNumbers = new Set<String>();
        Map<String, Account> customerAccountMap = new Map<String, Account>();
        List<Contact> updatedContacts = new List<Contact>();

        if (contacts.size() > 0)
        {
            for (SObject contObj : contacts)
            {
                Contact cont = (Contact) contObj;
                String key = ((String) cont.get(UTIL_SFContact.ContactCustomerFieldName)).replaceFirst('^0+(?!$)', '');
                if (String.isNotBlank(key))
                {
                    customerNumbers.add(key);
                }
            }

            customerAccountMap = UTIL_SyncHelper.createAccountMap(customerNumbers);

            for (SObject contObj : contacts)
            {
                Contact cont = (Contact) contObj;
                String key = ((String) cont.get(UTIL_SFContact.ContactCustomerFieldName)).replaceFirst('^0+(?!$)', '');
                if (String.isNotBlank(key))
                {
                    if (customerAccountMap.containsKey(key))
                    {
                        Account account = customerAccountMap.get(key);
                        if (account.Owner.IsActive)
                        {
                            cont.OwnerID = account.OwnerID;
                        }
                        cont.AccountId = account.Id;
                        updatedContacts.add(cont);
                    }
                }
            }

            UTIL_SyncHelper.insertUpdateResults('Contact', 'Update', null, null, updatedContacts, 'UTIL_ContactsBatchCleanup', null);
        }
    }

    // finish()
    //
    // Finish the batch job
    public void finish(Database.BatchableContext context)
    {
        System.debug(context.getJobId() + ' finished');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);
    }

    // buildQueryString
    //
    // Build the query String
    private String buildQueryString()
    {
        String query = 'SELECT Id, ' + UTIL_SFContact.ContactCustomerFieldName + ', AccountId ';
            query += 'FROM Contact WHERE AccountId = null AND ' + UTIL_SFContact.ContactCustomerFieldName + ' != null';

        return query;
    }
}