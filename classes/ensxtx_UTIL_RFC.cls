public with sharing class ensxtx_UTIL_RFC
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_UTIL_RFC.class);
    
    // getDocTypeMaster()
    //
    // Get the document type master from the RFC
    // Return the result    
    public static ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.RESULT getDocTypeMaster()
    {
        ensxtx_RFC_SD_GET_DOC_TYPE_VALUES rfc = new ensxtx_RFC_SD_GET_DOC_TYPE_VALUES();
        ensxtx_RFC_SD_GET_DOC_TYPE_VALUES.RESULT result = rfc.execute();

        if (!result.isSuccess())
        {
            ensxtx_UTIL_ViewHelper.displayResultMessages(result.getMessages(), ensxsdk.EnosixFramework.MessageType.INFO); 
        }

        return result;
    }
}