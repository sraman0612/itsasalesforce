/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class C_Action_Item_Extension_Test {

    @testSetup
    static void setupData(){
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Contact cont = new Contact();
        cont.firstName = 'Can';
        cont.lastName = ' Pango';
        cont.email = 'service-gdi@canpango.com';
        insert cont;
        
        Case cse = new Case();
        cse.Account = acct;
        cse.Contact = cont;
        cse.Subject = 'Test Case';
        insert cse;

        Case cse2 = new Case();
        cse2.Account = acct;
        cse2.Contact = cont;
        cse2.Subject = 'Test Case 2';
        cse2.GDI_Department__c = 'Customer Service';
        cse2.Action_Item_Status_Rollup__c = 0;
        insert cse2;

        Attachment att = new Attachment();
        att.Name = 'Attachment Test';
        att.ParentId = cse.Id;
        att.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att.ContentType = 'image/jpg';
        insert att;

        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.PathOnClient = 'File Test';
        cv.Origin = 'H';
        cv.Title = 'File Test';
        cv.VersionData = blob.valueOf('Test content for file');
        insert cv;

        ContentVersion cv2 = new ContentVersion();
        cv2.ContentLocation = 'S';
        cv2.PathOnClient = 'File Test 2';
        cv2.Origin = 'H';
        cv2.Title = 'File Test 2';
        cv2.VersionData = blob.valueOf('Test content for file 2');
        insert cv2;

        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv2.Id].ContentDocumentId;

        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = conDocId;
        cdl.LinkedEntityId = cse.Id;
        cdl.ShareType = 'V'; //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cdl.Visibility = 'AllUsers';
        insert cdl;
        
        Email_Field_Map__c efm = new Email_Field_Map__c();
        efm.Name = 'Test Email_Field_Map__c';
        insert efm;

        EmailMessage em = new EmailMessage();
        em.Incoming = true;
        em.FromName = 'SCC';
        em.ToAddress = 'salesforce@gardnerdenver.com';
        em.TextBody = 'Body';
        em.Subject = 'Subject';
        em.MessageDate = System.today();
        em.ParentId = cse.Id;
        em.HtmlBody = '<p>Body</p>';
        em.FromAddress = 'service-gdi@scc.com';
        em.CcAddress = 'service-gdi@scc.com';
        em.BccAddress = 'service-gdi@scc.com';
        insert em;
    }

    static testMethod void testEmails() {       
    
        Case c = [SELECT Id FROM Case WHERE Subject = 'Test Case'];
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' AND Name='Customer Care' LIMIT 1];
        
        Test.setCurrentPageReference(new PageReference('C_Action_Item_Extension'));       
        System.currentPageReference().getParameters().put('ParentId', c.Id);
        System.currentPageReference().getParameters().put('RecordTypeId', rt.Id);
        System.currentPageReference().getParameters().put('OrgBody', 'test');
        System.currentPageReference().getParameters().put('Subject', 'test');
        System.currentPageReference().getParameters().put('isFollowUp', 'false');
        System.currentPageReference().getParameters().put('OriginatingId', c.Id);
        
        C_Action_Item_Extension aie = new C_Action_Item_Extension();
        
        Contact con = [SELECT Id, Email FROM Contact];
        aie.theCase.ContactId = con.Id;
        aie.theCase.Contact = con;
        Email_Field_Map__c efm = [SELECT Id FROM Email_Field_Map__c];
        aie.theCase.Send_To_Queue__c = efm.Id;
        
        for(C_Action_Item_Extension.FileWrapper fw : aie.filesWrapper){
            fw.include = true;
        }
        
        aie.AIeditorContent = 'Text<br/>Text<badhtmltag>';
        aie.theCase.Subject = 'Testing Case';
        Boolean hpf = aie.hasParentFiles;
        aie.editorContent = 'Test Editor Content';
        
        aie.save();
        aie.cancel();

        Case c2 = [SELECT Id, Brand__c, Product_Category__c, Assigned_From_Address_Picklist__c FROM Case WHERE Subject = 'Test Case 2'];
        RecordType rt2 = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' AND Name='Internal Action Item' LIMIT 1];
        system.debug('c2 ====> '+c2);
        
        C_Action_Item_Extension.saveImage('test','VGVzdFN0cmluZw==');

        Test.setCurrentPageReference(new PageReference('C_Action_Item_Extension'));       
        System.currentPageReference().getParameters().put('ParentId', c2.Id);
        System.currentPageReference().getParameters().put('RecordTypeId', rt.Id);
        System.currentPageReference().getParameters().put('OrgBody', 'test');
        System.currentPageReference().getParameters().put('Subject', 'test');
        System.currentPageReference().getParameters().put('isFollowUp', 'true');
        System.currentPageReference().getParameters().put('OriginatingId', c2.Id);
        
        aie = new C_Action_Item_Extension();

        aie.theCase.ContactId = con.Id;
        aie.theCase.Contact = con;
        aie.theCase.Send_To_Queue__c = efm.Id;
        aie.theCase.Brand__c = c2.Brand__c;
        aie.theCase.Product_Category__c = c2.Product_Category__c;
        aie.theCase.Assigned_From_Address_Picklist__c = c2.Assigned_From_Address_Picklist__c;
        
        for(C_Action_Item_Extension.FileWrapper fw : aie.filesWrapper){
            fw.include = true;
        }
        
        aie.AIeditorContent = 'Text<br/>Text<badhtmltag>';
        aie.theCase.Subject = 'Testing Case';
        hpf = aie.hasParentFiles;
        aie.editorContent = 'Test Editor Content';
        
        aie.sendContent();
        aie.save();
        aie.cancel();
    }

    static testMethod void testFileCreation() {
        Case c = [SELECT Id, AccountId, ContactId FROM Case WHERE Subject = 'Test Case'];
        ContentVersion cv = [SELECT Id, ContentLocation, PathOnClient, Origin, Title, FileExtension, VersionData FROM ContentVersion WHERE Title = 'File Test'];
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' AND Name='Customer Care' LIMIT 1];

        Test.setCurrentPageReference(new PageReference('C_Action_Item'));
        System.currentPageReference().getParameters().put('ParentId', c.Id);
        System.currentPageReference().getParameters().put('RecordTypeId', rt.Id);
        System.currentPageReference().getParameters().put('OrgBody', 'test');
        System.currentPageReference().getParameters().put('Subject', 'test');
        System.currentPageReference().getParameters().put('isFollowUp', 'false');
        System.currentPageReference().getParameters().put('OriginatingId', c.Id);

        C_Action_Item_Extension aie = new C_Action_Item_Extension();

        Contact con = [SELECT Id, Email FROM Contact];
        aie.theCase.ContactId = con.Id;
        aie.theCase.Contact = con;
        Email_Field_Map__c efm = [SELECT Id FROM Email_Field_Map__c];
        aie.theCase.Send_To_Queue__c = efm.Id;

        for(C_Action_Item_Extension.FileWrapper wrapf : aie.filesWrapper){
            wrapf.include = true;
        }

        aie.fileID = cv.Id;
        aie.createNewFiles_Stage();
    }
}