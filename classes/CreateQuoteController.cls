/**
* This Controller is using in the CreateQuote Lightning web component.
* */
public without sharing class CreateQuoteController {
    
    //Get Account Details - Parag Bhatt
    @AuraEnabled
    public static Account getAccountDetails(String recordId){
        Account acct = [SELECT Id,Name,Oracle_Number__c,Owner.Name,OwnerId,
                BillingStreet,BillingCity,BillingPostalCode,
                Quote_Type__c,Service_Quote_Type__c,Relate_Bill_To_Account__c,Type,Bill_To_Account__c, Stock_Ship_To_Customer_Category__c, Related_Contacts_List__c
                FROM Account
                WHERE Id=:recordId];
                System.debug(acct);
                return acct;
    }
    
    //getRelatedBillToAccounts: to get the list of related bill to accounts based on the Oracle Number
   @AuraEnabled
    public static list<account> getRelatedBillToAccounts(String oracleNumber){
        List<Account> billtoaccount =new List<Account>();
        list<String> accList= new list<String>();
        for(Customer_Relationship__c customerrelationobj :[select Name from Customer_Relationship__c where Shipping_Customer__c=:oracleNumber]){
            accList.add(customerrelationobj.Name);
        } 
        billtoaccount =[Select Id, Name,Oracle_Number__c,BillingStreet, BillingCity, BillingState, BillingPostalCode from Account where Oracle_Number__c IN:accList AND type='Bill To'];
        return billtoaccount;
    }
    
    
    @AuraEnabled
    public static Contact getPrimaryContactDetails(Id accId)
    {
        try{
            Contact primaryContact = [select Id, Name, Phone,Email from Contact where Primary__c = true and AccountId=: accId limit 1];
            return primaryContact;
        }
        catch(exception e){
            return null;
        }
        
    }
    
    @AuraEnabled
    public static void deleteOpportunityUponPartialSave(Id oppId)
    {
        Opportunity opp = new Opportunity();
        opp.Id = oppId;
        Delete opp;
    }
    
    @AuraEnabled
    public static list<Asset> getRelatedAssetList(Id recordId){
        List<Asset> assetsList = new List<Asset>();
        assetsList = [SELECT id,Name,SerialNumber,accountid,Manufacturer__c,IRIT_Frame_Type__c,
                      IRITParent_Frame_Type_Name__c,Model_Name_CPQ__c,Description,HP__c,Start_Date__c,
                      Status,Operating_Status__c,IRIT_Last_Hours_on_Asset__c, Hours_Year__c, Ship_Date__c, 
                      Siebel_ID__c FROM Asset WHERE accountid =:recordId];
        return assetsList;
    }
    
    @AuraEnabled
    public static string saveAccount (Account acc, String oppOwnerId, String assetSerialNo, String updatedContactId, String billToAcc) {
      System.debug('billToAcc--> ' + billToAcc);
        try{
            upsert acc;
            string acctId = acc.Id;
            boolean allowQuoteCreation = false;
            String nameDateFormat = 'yyyy/MM/dd'; 
            String recordId;
            Account shipToAccount;
            //Get the shipto account details
            if(acctId != null){
                shipToAccount = [SELECT Id, Quote_Type__c,Service_Quote_Type__c,Bill_To_Account__c,Relate_Bill_To_Account__c,Related_Contacts_List__c, OwnerId, Owner.Name 
                                 FROM Account 
                                 WHERE Id = :acctId];
            }
            system.debug('shipToAccount--->'+shipToAccount);
            if(oppOwnerId != null && oppOwnerId != ''){
                for(User oUser : [SELECT Id FROM USER WHERE Id=:oppOwnerId AND Name=:System.Label.CQ_IntegrationUserName]){
                    throw new AuraException('User needs to change Account Manager.');
                }
            }
            Contact oldPrimaryContact = new Contact();
            if(acc.Id != null){
                oldPrimaryContact = getPrimaryContactDetails(acc.Id);
            }
            
            system.debug('@@oldPrimaryContact--->'+oldPrimaryContact);
            system.debug('@@updatedContactId--->'+updatedContactId);
            if(oldPrimaryContact != null && (updatedContactId != null && oldPrimaryContact.Id != updatedContactId  )){oldPrimaryContact.Primary__c = false; update oldPrimaryContact;if(updatedContactId != null){Contact con = new Contact();con.Id = updatedContactId;con.Primary__c = true;update con; }
            }else if(oldPrimaryContact == null && updatedContactId != null){Contact con = new Contact();con.Id = updatedContactId;con.Primary__c = true;update con; }

            Opportunity opp = new Opportunity(
                Accountid=acctId, 
                amount=0,
                stagename = 'Stage 1. Target/Qualify',
                ForecastCategoryName='Omitted', 
                name =Datetime.Now().format(nameDateFormat),
                Type=shipToAccount.Quote_Type__c,//'New Business',
                Confidence__c='100%', 
                Was_this_the_result_of_an_assessment__c='No',
                Was_This_the_Result_of_ALP__c='No',
                closedate = Date.today().addDays(30),
                Related_Contacts_List__c = shipToAccount.Related_Contacts_List__c,
                Quote_Type__c = shipToAccount.Quote_Type__c,
                Service_Quote_Type__c = shipToAccount.Service_Quote_Type__c,
                OwnerId = oppOwnerId,
                Oracle_Asset_Serial_Number__c= assetSerialNo,
                isCreatedFromCreateQuotePage__c = true,
                Bill_To_Account__c = billToAcc,
                CTS_BillToAccount__c = billToAcc
            );
            System.debug('opp:'+opp);
            System.debug('shipToAccount:'+shipToAccount.Bill_To_Account__c );
            //If the related bill to account is selected, then set the related bill to account to Opportunity Bill to Account           
            if(shipToAccount != null && shipToAccount.Relate_Bill_To_Account__c != '' && shipToAccount.Relate_Bill_To_Account__c != null){
                opp.CTS_BillToAccount__c = shipToAccount.Relate_Bill_To_Account__c;
            }else if(shipToAccount != null && shipToAccount.Bill_To_Account__c != null ){
                // && shipToAccount.Bill_To_Account__c != ''
                opp.CTS_BillToAccount__c = shipToAccount.Bill_To_Account__c;
            }
            //Based on the Quote Type, setting the opportunity record type
            if(shipToAccount != null && shipToAccount.Quote_Type__c != '' && shipToAccount.Quote_Type__c != null){
                if(shipToAccount.Quote_Type__c == 'Parts' ){
                    opp.RecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByDeveloperName().get('Quick_Quote').getRecordTypeId();
                }else if(shipToAccount.Quote_Type__c == 'Service' || shipToAccount.Quote_Type__c=='Rental'){
                    opp.RecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByDeveloperName().get('ITSA_Service').getRecordTypeId();
                }
            }
            system.debug('Opportunity-- '+opp);
            Database.insert(opp);
            system.debug('Opportunity id Created-- '+opp.Id);
            return opp.id;
           
        }catch(DMLException dmlex){
            throw new AuraException(dmlex.getDMLMessage(0));
        }catch(Exception ex){
            system.debug('ex.getMessage()---> '+ex.getMessage()+'---->Stack Trace'+ex.getStackTraceString());
            throw new AuraException(ex.getMessage());
        }
        
    } 

    //Added by Parag Bhatt
   @AuraEnabled
    public static String genQuote(String acctId, String oppOwnerId, String oppId){
        try{
            boolean allowQuoteCreation = false;
            
            if(oppOwnerId != null && oppOwnerId != ''){
                for(User oUser : [SELECT Id FROM USER WHERE Id=:oppOwnerId AND Name=:System.Label.CQ_IntegrationUserName]){
                    throw new AuraException('User needs to change Account Manager.');
                }
            }
            
            String currentUserId = UserInfo.getUserId();
            
            //Checking is the current user is avaiable in Oracle Users object or not
            List<cafsl__Oracle_User__c> oracleUserList = [SELECT Id, cafsl__User__c, cafsl__Embedded_CPQ_Settings__c, cafsl__Allow_Quote_Creation__c FROM cafsl__Oracle_User__c WHERE cafsl__User__c =:currentUserId];
            if(oracleUserList.size()>0){
                cafsl__Oracle_User__c oracleUser = oracleUserList[0];
                allowQuoteCreation = oracleUser.cafsl__Allow_Quote_Creation__c;
                String siteSettingsId = oracleUser.cafsl__Embedded_CPQ_Settings__c;
                //Error message: If the current user is not allowed to create quote
                if(!allowQuoteCreation){
                    throw new AuraException('User do not have access to Create/Edit the Oracle Quote. Verify Allow Quote Creation flag');
                }
                List<cafsl__Embedded_CPQ_Settings__c> embeddedCPQSettingsList =[SELECT  Id 
                                                                                FROM cafsl__Embedded_CPQ_Settings__c WHERE Id=:siteSettingsId];
                //Checking for connected app access and 
                //populating the required parameters from CPQ setting
                if(embeddedCPQSettingsList.size() > 0){
                    cafsl__Embedded_CPQ_Settings__c embeddedCPQSettings = embeddedCPQSettingsList[0];
                    
                }else{
                    //Error message: If the current user is not having access to connected app.
                    allowQuoteCreation = false;
                    throw new AuraException('User do not have access to Oracle CPQ Connected App.');
                    
                }
            }
            
            String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
            // CONSTRUCT URL FOR CPQ CARTy

            system.debug('sfdcBaseURL--->'+sfdcBaseURL);
            String targetURL = '';

            if(sfdcBaseURL.contains('partners.ingersollrand')){ //PROD AIRD
                targetURL =  sfdcBaseURL+'/s/quotecreator?id='+oppId;
            }
            else if(sfdcBaseURL.contains('ircoam--uat.sandbox.my.site.com')){ //SANDBOX // below URL for Community
                targetURL =  sfdcBaseURL+'/ctsglobalpartners/s/quotecreator?id='+oppId;
            }
            else{ //Internal SF URL
                targetURL = sfdcBaseURL+'/apex/cafsl__EmbeddedTransaction?retURL=/'+oppId+'&wrapMassAction=1&scontrolCaching=1&id='+oppid;
            }
            
            system.debug('targetURL--->'+targetURL);
            return targetURL;
        }catch(DMLException dmlex){
            throw new AuraException(dmlex.getDMLMessage(0));
        }catch(Exception ex){
            throw new AuraException(ex.getMessage());
        }
    } 
    
    @AuraEnabled(cacheable=true) //get Account QuoteType Picklist Values
    public static Map<String, String> getQuoteType(){
        Map<String, String> mapOfQuoteType = new Map<String, String>();
        String prfId = UserInfo.getProfileId();
        system.debug('getQuoteType getProfileId@@@---->'+prfId);
        
        List<Profile> lstPrf = [Select id,Name from Profile where Id = :prfid limit 1];
        if(lstPrf.size() > 0){
            if(lstPrf[0].Name == 'SFS Service Technician' || lstPrf[0].Name == 'SFS Service'){
                mapOfQuoteType.put('Service','Service');
                mapOfQuoteType.put('Rental','Rental');
            }else if(lstPrf[0].Name == 'System Administrator'){
                mapOfQuoteType.put('Inside Sales','Parts');
                mapOfQuoteType.put('Service','Service');
                mapOfQuoteType.put('Rental','Rental');
            }else if(lstPrf[0].Name == 'IR Comp Sales'){
                mapOfQuoteType.put('Inside Sales','Parts');
            }else{
                mapOfQuoteType.put('Inside Sales','Parts');
                mapOfQuoteType.put('Rental','Rental');
            }
            
        }
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
            // CONSTRUCT URL FOR CPQ CARTy
            //  String sfdcBaseURL = 'https://gdi--dprj01.my.salesforce.com';
            system.debug('sfdcBaseURL--->'+sfdcBaseURL);
            String targetURL = '';
            if(sfdcBaseURL.contains('gdiconnectedcustomer') || sfdcBaseURL.contains('partners.ingersollrand') || sfdcBaseURL.contains('ctsglobalpartners')){
                mapOfQuoteType.clear();
                mapOfQuoteType.put('Inside Sales','Parts');
            }
         system.debug('getQuoteType mapOfQuoteType@@@---->'+mapOfQuoteType);
        return mapOfQuoteType;
    } 
    
    @AuraEnabled(cacheable=true) //get Account ServiceQuoteType Picklist Values
    public static Map<String, String> getWorkQuoteType(){
        Map<String, String> serviceQuoteOptions = new Map<String, String>();
        //get Account Quote Field Describe
        Schema.DescribeFieldResult fieldResult1 = Account.Service_Quote_Type__c.getDescribe();
        //get Account Quote Picklist Values
        List<Schema.PicklistEntry> pList1 = fieldResult1.getPicklistValues();
        for (Schema.PicklistEntry p1: pList1) {
            //Put Picklist Value & Label in Map
            if(p1.getLabel() != 'Diagnostic w/Repair' && p1.getLabel() != 'Installation w/Start up' ){
                serviceQuoteOptions.put(p1.getValue(), p1.getLabel());
            }
        }
        return serviceQuoteOptions;
    }
    @AuraEnabled //get Account Type Picklist Values
    public static Map<String, String> getType1(){
        Map<String, String> serviceQuoteOptions = new Map<String, String>();
        //get Account Quote Field Describe
        Schema.DescribeFieldResult fieldResult1 = Account.Type.getDescribe();
        //get Account Quote Picklist Values
        List<Schema.PicklistEntry> pList1 = fieldResult1.getPicklistValues();
        for (Schema.PicklistEntry p1: pList1) {
            //Put Picklist Value & Label in Map
            serviceQuoteOptions.put(p1.getValue(), p1.getLabel());
        }
        system.debug('serviceQuoteOptions@@--->'+serviceQuoteOptions);
        return serviceQuoteOptions;
    }
    @AuraEnabled //get Account Status Picklist Values
    public static Map<String, String> getStatus1(){
        Map<String, String> serviceQuoteOptions = new Map<String, String>();
        //get Account Quote Field Describe
        Schema.DescribeFieldResult fieldResult1 = Account.Status__c.getDescribe();
        //get Account Quote Picklist Values
        List<Schema.PicklistEntry> pList1 = fieldResult1.getPicklistValues();
        for (Schema.PicklistEntry p1: pList1) {
            //Put Picklist Value & Label in Map
            serviceQuoteOptions.put(p1.getValue(), p1.getLabel());
        }
        return serviceQuoteOptions;
    }
    
}