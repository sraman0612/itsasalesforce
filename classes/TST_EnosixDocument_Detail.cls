/// enosiX Inc. Generated Apex Model
/// Generated On: 8/27/2019 1:20:31 PM
/// SAP Host: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
public with sharing class TST_EnosixDocument_Detail
{
    public class MockSBO_EnosixDocument_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) { return null; }
        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { return null; }
    }

    @isTest
    static void testSBO()
    {
        SBO_EnosixDocument_Detail sbo = new SBO_EnosixDocument_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixDocument_Detail.class, new MockSBO_EnosixDocument_Detail());
        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.command(null, null));
        System.assertEquals(null, sbo.getDetail(null));
        System.assertEquals(null, sbo.save(null));
    }

    @isTest
    static void testEnosixDocument()
    {
        SBO_EnosixDocument_Detail.EnosixDocument result = new SBO_EnosixDocument_Detail.EnosixDocument();
        System.assertEquals(SBO_EnosixDocument_Detail.EnosixDocument.class, result.getType(), 'getType() does not match object type.');

        result.registerReflectionForClass();

        result.DocumentType = 'X';
        System.assertEquals('X', result.DocumentType);

        result.DocumentTypeDescription = 'X';
        System.assertEquals('X', result.DocumentTypeDescription);

        result.DocumentNumber = 'X';
        System.assertEquals('X', result.DocumentNumber);

        result.DocumentVersion = 'X';
        System.assertEquals('X', result.DocumentVersion);

        result.DocumentPart = 'X';
        System.assertEquals('X', result.DocumentPart);

        result.DocumentDescription = 'X';
        System.assertEquals('X', result.DocumentDescription);

        result.DocumentStatus = 'X';
        System.assertEquals('X', result.DocumentStatus);

        result.DocumentStatusDescription = 'X';
        System.assertEquals('X', result.DocumentStatusDescription);

        result.CreatedBy = 'X';
        System.assertEquals('X', result.CreatedBy);

        result.Laboratory = 'X';
        System.assertEquals('X', result.Laboratory);

        result.ChangeNumber = 'X';
        System.assertEquals('X', result.ChangeNumber);

        result.AuthorizationGroup = 'X';
        System.assertEquals('X', result.AuthorizationGroup);

        //Test child collections
        System.assertNotEquals(null,result.ATTACHMENTS.getAsList());
    }

    @isTest
    static void testATTACHMENTS()
    {
        SBO_EnosixDocument_Detail.ATTACHMENTS childObj = new SBO_EnosixDocument_Detail.ATTACHMENTS();
        System.assertEquals(SBO_EnosixDocument_Detail.ATTACHMENTS.class,childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.FileIndex = 0;
        System.assertEquals(0, childObj.FileIndex);

        childObj.FileType = 'X';
        System.assertEquals('X', childObj.FileType);

        childObj.FileTypeDescription = 'X';
        System.assertEquals('X', childObj.FileTypeDescription);

        childObj.FileName = 'X';
        System.assertEquals('X', childObj.FileName);

        childObj.HTMLContentType = 'X';
        System.assertEquals('X', childObj.HTMLContentType);

        childObj.GetFile = 'X';
        System.assertEquals('X', childObj.GetFile);

        childObj.FileB64String = 'X';
        System.assertEquals('X', childObj.FileB64String);

        List<string> keyFields = new List<string>{ 'EnosixObjKey','iindex' };
    
        List<string> keys = childObj.getKeyFields();
        for(Integer i = 0; i < keys.size(); i++)
        {
            system.assertEquals(keyFields[i],keys[i]);
        }
    }
}