public class FavoriteShareCreate {
    public static void createFromFavorite(List<SBQQ__Favorite__c> favorites){
        List<Id> accs = new List<Id>();
        List<Id> parentIds = new List<Id>();
        Map<Id, SBQQ__Favorite__c> accToFavorite = new Map<Id, SBQQ__Favorite__c>();
        for (SBQQ__Favorite__c f : favorites){
            accs.add(f.Favorite_Account_Id__c);
            parentIds.add(f.Favorite_Parent_Account_Id__c);
            accToFavorite.put(Id.valueOf(f.Favorite_Account_Id__c), f);
            accToFavorite.put(Id.valueOf(f.Favorite_Parent_Account_Id__c), f);
        }
        List<User> singleUsers = [SELECT Id, Contact.AccountId FROM User WHERE Contact.AccountId in :accs AND Contact.AccountId != ''
                                  AND Profile.Name LIKE '%Single Account User%'];
        List<User> globalUsers = [SELECT Id, Contact.Account.ParentId FROM User WHERE Contact.Account.ParentId in :parentIds AND Contact.Account.ParentId != ''
                                  AND Profile.Name LIKE '%Global Account User%'];
        system.debug('singleUsers = '+ singleUsers);
        system.debug('globalUsers = '+ globalUsers);
        system.debug('accToFavorite = ' +accToFavorite);
        List<SBQQ__FavoriteShare__c> shares = new List<SBQQ__FavoriteShare__c>();
        for (User u : singleUsers){
            if (!accToFavorite.containsKey(u.Contact.AccountId))
                continue;
            SBQQ__Favorite__c f = accToFavorite.get(u.Contact.AccountId);
            if (f.Id != null )
                shares.add(new SBQQ__FavoriteShare__c(SBQQ__User__c = u.Id, SBQQ__Favorite__c = f.Id));
        }
        for (User u : globalUsers){
            system.debug('u.Contact.Account.ParentId = ' + u.Contact.Account.ParentId);
            if (!accToFavorite.containsKey(u.Contact.Account.ParentId))
                continue;
            SBQQ__Favorite__c f = accToFavorite.get(u.Contact.Account.ParentId);
            if (f.Id != null )
                shares.add(new SBQQ__FavoriteShare__c(SBQQ__User__c = u.Id, SBQQ__Favorite__c = f.Id));
        }
        system.debug(shares);
        if (!shares.isEmpty())
            insert shares;
    }
    @future
    public static void createFromUser(set<Id> usrs){
        Map<Id, List<User>> usertoaccMap = new Map<Id, List<User>>();
        List<User> users = [SELECT Id, Profile.Name, Partner_Account_ID__c, Partner_Account_Parent_ID__c FROM User WHERE Id in :usrs and isPortalEnabled = true];
        for (User u : users){
            List<User> us = new List<User>{u};
                if (u.Profile.Name.contains('Single Account')){
                    if (u.Partner_Account_ID__c != null) {
                        if (usertoaccMap.containsKey(Id.valueOf(u.Partner_Account_ID__c))){
                            us.addAll( usertoaccMap.get(Id.valueOf(u.Partner_Account_ID__c) ));
                        }
                        usertoaccMap.put(Id.valueOf(u.Partner_Account_ID__c), us);
                    }
                }else{
                    if (u.Partner_Account_Parent_ID__c != null) {
                        if (usertoaccMap.containsKey(Id.valueOf(u.Partner_Account_Parent_ID__c) )){
                            us.addAll( usertoaccMap.get(Id.valueOf(u.Partner_Account_Parent_ID__c)) );
                        }
                        usertoaccMap.put( Id.valueOf(u.Partner_Account_Parent_ID__c), us);
                    }
                }
        }
        List<SBQQ__Favorite__c> favorites = [SELECT Id, Favorite_Account_Id__c, Favorite_Parent_Account_Id__c FROM SBQQ__Favorite__c
                                             WHERE CreatedBy.Contact.AccountId in :usertoaccMap.keyset() OR
                                             CreatedBy.Contact.Account.ParentId in :usertoaccMap.keyset()];
        List<SBQQ__FavoriteShare__c> fs = new List<SBQQ__FavoriteShare__c>();
        for (SBQQ__Favorite__c fav : favorites){
            if (usertoaccMap.containsKey(Id.valueOf(fav.Favorite_Account_Id__c))){
                for (User u : usertoaccMap.get(Id.valueOf(fav.Favorite_Account_Id__c) )){
                    fs.add( new SBQQ__FavoriteShare__c(SBQQ__User__c = u.Id, SBQQ__Favorite__c = fav.Id) );
                }
            }
            if (usertoaccMap.containsKey(Id.valueOf(fav.Favorite_Parent_Account_Id__c) )){
                for (User u : usertoaccMap.get( Id.valueOf(fav.Favorite_Parent_Account_Id__c) )){
                    fs.add( new SBQQ__FavoriteShare__c(SBQQ__User__c = u.Id, SBQQ__Favorite__c = fav.Id) );
                }
            }
        }
        insert fs;
    }
}