public class CXUtils_CSVReader implements Iterable<List<String>>, Iterator<List<String>> {
    // Modified below class from github, but Salesforce might have standard parsing functionality soon
    // https://github.com/FishOfPrey/apex-csv/blob/master/src/classes/RT_CSVReader.cls
    // https://help.salesforce.com/s/articleView?id=release-notes.rn_apex_DataWeaveInApex_DevPreview.htm&type=5&release=240

	private final static String CR = '\r';
	private final static String LF = '\n';
	private final static String CRLF = '\r\n';
	private final static String QUOTE = '"';
	private final static String DOUBLE_QUOTE = '""';

	private String csvString;
  private String delimiter;
	private Integer csvStringLength;
	private Integer position = 0;
	private Iterator<List<String>> it;

	public CXUtils_CSVReader(String csvString, String delimiter) {
		this.delimiter = delimiter;
    this.csvString = removeBOMChars(convertLineEndings(csvString.trim()));
		this.csvStringLength = csvString.length();
		this.it = iterator();
	}

	public static List<List<String>> parse(String csvString, String delimiter) {
		List<List<String>> res = new List<List<String>>();
		CXUtils_CSVReader reader = new CXUtils_CSVReader(csvString, delimiter);
		while (reader.it.hasNext()) {
			res.add(reader.it.next());
		}
		return res;
	}

    public static List<Map<String, String>> parseToMapList(String csvString, String delimiter) {
        List<List<String>> data = parse(csvString, delimiter);
        List<Map<String, String>> objList = new List<Map<String, String>>();
        for (Integer recNum = 1; recNum < data.size(); recNum++) {
            Map<String, String> obj = new Map<String, String>();
            for (Integer fieldNum = 0; fieldNum < data[0].size(); fieldNum++) {
                obj.put(data[0][fieldNum], data[recNum][fieldNum]);
            }
            objList.add(obj);
        }
        return objList;
    }

    // Remove BOM (Byte Order Mark). Some systems begin files with these. Not recognized by Apex
    private static String removeBOMChars(String csvString) {
      return csvString.replace('\uFEFF', '').replace('\uFFFE', '');
    }

	private static String convertLineEndings(String str) {
		return str.replace(CRLF, LF).replace(CR, LF);
	}

	public Iterator<List<String>> iterator() {
		return this;
	}

	public Boolean hasNext() {
		return position < csvStringLength;
	}

	public List<String> next() {
		List<String> values = new List<String>();
		position = readLine(position, values);
		return values;
	}

	private Integer readLine(Integer position, List<String> values) {
		Integer startPos = position;

		String currentValue;
		Integer delimAt;
    Integer nlAt;
    Integer foundAt;

		List<String> tmpValues = new List<String>();
		while (position < csvStringLength) {
			delimAt = csvString.indexOf(DELIMITER, position);
			nlAt = csvString.indexOf(LF, position);
			foundAt = Math.min(delimAt, nlAt);

			if (foundAt < 0) {
				foundAt = Math.max(delimAt, nlAt);
			}
			if (foundAt < 0) {
				currentValue = csvString.substring(startPos);
				position = csvStringLength;
			} else {
				currentValue = csvString.substring(startPos, foundAt);
				position = foundAt + 1;
			}

			if (!currentValue.startsWith(QUOTE)) {
				tmpValues.add(currentValue);

				if (foundAt == nlAt) {
					break;
				}
				startPos = position;
			} else if (currentValue == DOUBLE_QUOTE) {
				tmpValues.add('');
				if (foundAt == nlAt) {
					break;
				}
				startPos = position;
			} else if (currentValue.endsWith(QUOTE)) {
				Integer lastIndex = currentValue.length() - 1;
				currentValue = currentValue.substring(1, lastIndex);

				// Does it contain double quoted quotes? I.e. Escaped double quotes
				Integer doubleQuotedQuotesIndex = currentValue.indexOf(DOUBLE_QUOTE);
				if(doubleQuotedQuotesIndex >= 0) {
						// Replace double quoted quotes with double quotes
						currentValue = currentValue.replace(DOUBLE_QUOTE, QUOTE);
				}

				tmpValues.add(currentValue);
				if (foundAt == nlAt) {
					break;
				}
				startPos = position;
			}
		}
		values.addAll(tmpValues);
		return position;
	}
}