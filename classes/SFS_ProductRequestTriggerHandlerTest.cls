/*=========================================================================================================
* @author : Srikanth P, Capgemini
* @date : 04/01/2022
* @description: Test Class for Product Request Trigger Apex handler
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------

============================================================================================================*/
@isTest(SeeAllData=true)
public class SFS_ProductRequestTriggerHandlerTest {
    
    @isTest
    private static void SFS_ProductRequestTriggerHandlerMethod1(){
     //Try{
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        /*List<Account> accList=SFS_TestDataFactory.createAccounts(2, false);
        accList[0].AccountSource='Web';
        accList[0].IRIT_Customer_Number__c='1234';
        accList[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        accList[0].IRIT_Payment_Terms__c='BANKCARD';
        insert accList[0]; 
        
        accList[1].Bill_To_Account__c = accList[0].Id;
        accList[1].AccountSource='Web';
        accList[1].name = 'test account';
        accList[1].CurrencyIsoCode = 'USD';
        accList[1].IRIT_Customer_Number__c='1234';
        insert accList[1];
        //insert accList;
        //accList[0].Bill_To_Account__c=accList[1].Id;
        //update accList;
        */
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[0].AccountSource='Web';
        accList[0].IRIT_Customer_Number__c='1234';
        accList[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        accList[0].IRIT_Payment_Terms__c='BANKCARD';
        insert accList[0];
        accList[1].Bill_To_Account__c=accList[0].Id;
        accList[1].Type='Prospect';
        accList[1].ShippingPostalCode='28759';
        accList[1].ShippingCity ='Montreat2';
        
        insert accList[1];
         
        //List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,false);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        //List<ServiceContract> svc = SFS_TestDataFactory.createServiceAgreement(1, accList[1].Id, true);
        //List<ContractLineItem> cliRec = SFS_TestDataFactory.createServiceAgreementLineItem(1, svc[0].Id,true);
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true,SFS_Division__c=div[0].Id);
        insert lc; 
        List<Pricebook2> servicePricebook = [SELECT Id, isActive FROM Pricebook2 WHERE Name = 'Service Price Book'];
         
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, accList[0].Id,lc.Id, div[0].Id, null, false);
        woList[0].Pricebook2Id = servicePricebook[0].Id;
        insert WoList[0];
       
        WorkType wt = new WorkType();
           String wt_RECORDTYPEID = Schema.SObjectType.WorkType.getRecordTypeInfosByName().get('Service').getRecordTypeId();
           wt.Name = 'test';
           wt.SFS_Product__c = proList[0].Id;
           wt.RecordTypeId = wt_RECORDTYPEID;
           wt.EstimatedDuration = 12.00;
           wt.DurationType ='Hours';
           insert wt;
        system.debug('worktype '+wt);
        List<WorkOrderLineItem> woliList =  SFS_TestDataFactory.createWorkOrderLineItem(1,WoList[0].Id,wt.Id,true);        
        List<ProductRequest> prList =  SFS_TestDataFactory.createProductRequest(2,WoList[0].Id,woliList[0].Id,false);
        
        prList[0].SFS_Destination_Location_Id__c=String.valueOf(lc.Id);
        prList[0].DestinationLocationId = lc.Id;
        prList[0].SFS_Manually_Received__c = true;
        prList[0].SFS_Auto_Receive__c = false;
        prList[0].SFS_Latest_Expected_Delivery_Date__c =System.Now();
        
        prList[1].SFS_Destination_Location_Id__c=String.valueOf(lc.Id);
        prList[1].DestinationLocationId = lc.Id;
        prList[1].SFS_Manually_Received__c = true;
        prList[1].SFS_Auto_Receive__c = false;
        prList[1].SFS_Latest_Expected_Delivery_Date__c =System.Now();
        insert prList;
        
       ProductRequest pr1 = new ProductRequest();
       pr1.SFS_Manually_Received__c = true;
       pr1.WorkOrderId=WoList[0].Id;
       pr1.WorkOrderLineItemId=woliList[0].Id;
       pr1.SFS_Manually_Received__c = true;
       insert pr1;
       //Map<Id,ProductRequest> prMap = new Map<Id,ProductRequest>();
       //prMap.put(pr1.Id, pr1);
      
        
         
        List<ProductRequestLineItem> prliList =  SFS_TestDataFactory.createPRLI(2,prList[0].Id,proList[0].Id,false);
        prliList[0].Status = 'Shipped';
        prliList[0].QuantityUnitOfMeasure = 'EA';
        prliList[0].SFS_Quantiy_Shipped__c = 2;
        prliList[0].SFS_Quantity_Received__c =2;
        insert prliList[0];
         
        prliList[1].Status = 'CLOSED';
        prliList[1].QuantityUnitOfMeasure = 'EA';
        prliList[1].SFS_Quantiy_Shipped__c = 3;
        prliList[1].SFS_Quantity_Received__c =3;
        prliList[1].SFS_Freight_Charge__c = 50.00;
        insert prliList[1];
         
         //Test.startTest();
        prList[0].SFS_Auto_Receive__c = true;
        prList[0].SFS_Manually_Received__c = false;
        prList[0].SFS_Latest_Expected_Delivery_Date__c = System.Now()+1;
        update prList[0];
        //SFS_ProductRequestTriggerHandler.createProductTransferAndConsumed(prList, prMap);
         //Test.stopTest();
       Test.startTest();
        prliList[1].SFS_Freight_Charge__c = 100.00;
        update prliList[1];
       Test.stopTest();
        
        
       
        
    //} catch(Exception Ex){
      //  system.debug('Error' +ex.getMessage());
      //}   
    }
    
    
    
     
    }