public with sharing class sFS_LocationSkillController {
    
    @AuraEnabled(cacheable=true)
    public static List<SFS_Location_Skill_Requirement__c> getRecords(String locationName, String skillCheck) {

        List<SFS_Location_Skill_Requirement__c> records = [SELECT Id, Location__c, Skill__c 
                                                           FROM SFS_Location_Skill_Requirement__c
                                                           WHERE Location__c = :locationName AND Skill__c = :skillCheck];
                           return records;
    }


    @AuraEnabled(cacheable=true)
    public static Skill getSkill(String skillName) {     
        skill records = [SELECT Id , MasterLabel
                                FROM Skill
                                    WHERE MasterLabel= :skillName limit 1];                                    
                           return records;
    }
    
}