/*=========================================================================================================
* @author Manimozhi, Capgemini
* @date 10/08/2023
* @description:
* @Story Number: 

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================*/
public class SFS_setInvoiceSubmittedQueueable implements Queueable {
    public List<Invoice__c> invList ;
    public SFS_setInvoiceSubmittedQueueable( List<Invoice__c> invList){
        this.invList = invList ; 
    }
    public void execute(QueueableContext context) { 
        if(!invList.isEmpty()){
            update invList;
        }
        
       
    }
}