/*
* Author   : Nocks Emmanuel Mulea 
* Company  : Canpango LLC
* Email    : emmanuel.mulea@canpango.com
* 
* 
* 
*/


@isTest
global class C_GDIHttpMockTest implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response.
        
        // Set response values, and
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        
        String json = '{'+
            '  \"additionParents\": {'+
            '    \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/46408955/additionParents\",'+
            '    \"references\": []'+
            '  },'+
            '  \"owner\": \"device_iconn-sn-A06C6F\",'+
            '  \"childDevices\": {'+
            '    \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/46408955/childDevices\",'+
            '    \"references\": []'+
            '  },'+
            '  \"childAssets\": {'+
            '    \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/46408955/childAssets\",'+
            '    \"references\": []'+
            '  },'+
            '  \"creationTime\": \"2018-04-05T05:08:05.116Z\",'+
            '  \"type\": \"ControllerModel_16\",'+
            '  \"lastUpdated\": \"2020-11-11T12:32:01.894Z\",'+
            '  \"childAdditions\": {'+
            '    \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/46408955/childAdditions\",'+
            '    \"references\": ['+
            '      {'+
            '        \"managedObject\": {'+
            '          \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/366363352\",'+
            '          \"id\": \"366363352\"'+
            '        },'+
            '        \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/46408955/childAdditions/366363352\"'+
            '      }'+
            '    ]'+
            '  },'+
            '  \"name\": \"Crown Equipment Plant 7 : LRSFR5A\",'+
            '  \"assetParents\": {'+
            '    \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/46408955/assetParents\",'+
            '    \"references\": []'+
            '  },'+
            '  \"deviceParents\": {'+
            '    \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/46408955/deviceParents\",'+
            '    \"references\": []'+
            '  },'+
            '  \"self\": \"https://industrials.iconn.gardnerdenver.com/inventory/managedObjects/46408955\",'+
            '  \"id\": \"46408955\",'+
            '  \"c8y_Notes\": \"***This hardware is not compatible with SAM (Smart AirMaster)***\\n\\nDiscovered compressor after update on 2018-09-18T16:37:39Z: Delcos XL-LRS (idx. 16)\",'+
            '  \"c8y_Firmware\": {'+
            '    \"moduleVersion\": \"Owa31I_3G_r64MB_f64MB_1xRS485\",'+
            '    \"name\": \"iconn-v1-v3.1.3_20200304_112117Z\",'+
            '    \"generationDate\": \"2020-03-04T11:21:17Z\",'+
            '    \"installationDate\": \"2020-04-28T04:49:31Z\",'+
            '    \"version\": \"3.1.3\"'+
            '  },'+
            '  \"c8y_Availability\": {'+
            '    \"lastMessage\": \"2020-11-11T12:31:15.319Z\",'+
            '    \"status\": \"AVAILABLE\"'+
            '  },'+
            '  \"c8y_H_Load\": 58368,'+
            '  \"com_cumulocity_model_Agent\": {},'+
            '  \"c8y_RequiredAvailability\": {'+
            '    \"responseInterval\": 3000'+
            '  },'+
            '  \"c8y_Connection\": {'+
            '    \"status\": \"DISCONNECTED\"'+
            '  },'+
            '  \"owasys_ServerStats\": {'+
            '    \"servers\": {'+
            '      \"srv1\": {'+
            '        \"count\": 458,'+
            '        \"lastTs\": 1605097833'+
            '      },'+
            '      \"srv0\": {'+
            '        \"count\": 457,'+
            '        \"lastTs\": 1605063805'+
            '      }'+
            '    },'+
            '    \"count\": 10653,'+
            '    \"lastTs\": 1605097833'+
            '  },'+
            '  \"c8y_Address\": {'+
            '    \"territory\": \" \"'+
            '  },'+
            '  \"c8y_SupportedOperations\": ['+
            '    \"c8y_Restart\",'+
            '    \"c8y_Command\",'+
            '    \"c8y_Firmware\",'+
            '    \"c8y_MeasurementPollFrequencyOperation\"'+
            '  ],'+
            '  \"c8y_Position\": {'+
            '    \"lng\": -84.3919832,'+
            '    \"lat\": 40.4369351'+
            '  },'+
            '  \"c8y_customerName\": \"SEDALIA RETRO FIT - US\",'+
            '  \"c8y_H_Total\": 59194,'+
            '  \"c8y_Hardware\": {'+
            '    \"serialNumber\": \"CD10006024001\",'+
            '    \"model\": \"Delcos XL-LRS\",'+
            '    \"revision\": \"232\"'+
            '  },'+
            '  \"Folder\": \"AMERICA > USA - US > Air Handling > CROWN EQUIPMENT\",'+
            '  \"iConn_Service\": {'+
            '    \"dateOfNextService\": \"2021-04-28\",'+
            '    \"hRunDayAvg\": 1.9,'+
            '    \"daysToNextService\": 168'+
            '  },'+
            '  \"c8y_Hardware.serialNumber\": \"CD10006024001\",'+
            '  \"c8y_H_Service\": 1658,'+
            '  \"c8y_Mobile\": {'+
            '    \"iccid\": \"8944500711179486426\",'+
            '    \"mnc\": \"410\",'+
            '    \"imei\": \"359804083684946\",'+
            '    \"imsi\": \"234507091948642\",'+
            '    \"mcc\": \"310\",'+
            '    \"cellId\": null,'+
            '    \"lac\": null'+
            '  },'+
            '  \"iConn_MissedServices\": {'+
            '    \"nOfMsSinceLastService\": 0,'+
            '    \"hLostServiceTotal\": 0,'+
            '    \"activationAt\": \"2020-04-28T04:56:30Z\",'+
            '    \"hLastServiceAt\": 58854,'+
            '    \"nOfMs\": 0,'+
            '    \"lastServiceAt\": \"2020-09-14T07:11:30Z\",'+
            '    \"status\": \"green\"'+
            '  },'+
            '  \"c8y_ActiveAlarmsStatus\": {'+
            '    \"major\": 0,'+
            '    \"minor\": 0,'+
            '    \"critical\": 0,'+
            '    \"warning\": 0'+
            '  },'+
            '  \"c8y_CellInfo\": {'+
            '    \"cellTowers\": ['+
            '      {'+
            '        \"mobileCountryCode\": 310,'+
            '        \"mobileNetworkCode\": 410'+
            '      }'+
            '    ],'+
            '    \"radioType\": \"umts\"'+
            '  },'+
            '  \"c8y_IsDevice\": {}'+
            '}';
        
        res.setBody(json);
        res.setStatusCode(200);
        return res;
        
    }
}