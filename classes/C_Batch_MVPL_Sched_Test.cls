@isTest
private class C_Batch_MVPL_Sched_Test {

        private static testmethod void testScheduler() {
        Test.startTest();
			C_Batch_MVPL_Sched scheduler = new C_Batch_MVPL_Sched();   
        	String cronExp = '0 0 0 1 1 ? 2099';
        	Id jobId = System.schedule('Test Scheduler', cronExp, scheduler);        
        	
			CronTrigger cronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId]; 
        	System.assertEquals(0, cronTrigger.TimesTriggered, 'We expect our job to have been scheduled and not yet triggered');
        Test.stopTest();
    }
    
}