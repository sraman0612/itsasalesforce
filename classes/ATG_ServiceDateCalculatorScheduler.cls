global class ATG_ServiceDateCalculatorScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new ATG_ServiceDateCalculatorBatch(), 10);
    }
}