global class C_CreateFPAssetShareBatch implements Database.Batchable<sObject> {
    global final String Query;
    global final List<string> aIDs;
    global final List<string> nfpIDs;
    
    global C_CreateFPAssetShareBatch(string q, List<string> nfpIDs, List<string> acctIDs)
    {
        this.nfpIDs = nfpIDs;
        system.debug(nfpIDs + ' ' + nfpids);
        aIDs = acctIDs;
        system.debug(aIDs);
        if(q != null)
        {
            Query=q;
        }
        else
        {
            Query='Select id from asset where accountID IN :nfpIDs';
        }
   	}
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Asset> scope){
        List<assetShare> assetShareToCreate = new List<assetShare>();
        //List<id> uId = new List<id>();
        List<User> usr = [Select id from user where accountID IN :aIDs];
        for(Asset ash:scope)
        {
            //system.debug(account.id);
            for(User Us : usr)
            {
                AssetShare share = new AssetShare();
                share.assetAccessLevel='Edit';
                share.AssetId=ash.id;
                share.RowCause='Manual';
                share.UserOrGroupId=us.id;
                
                assetShareToCreate.add(share);
                
            }
        }
        system.debug(assetShareToCreate.size());
        insert assetShareToCreate;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        system.debug('Finished creating rights!!!');
    }
    
}