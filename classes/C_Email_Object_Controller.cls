public class C_Email_Object_Controller {

    public Boolean hasParentAttachments { get{return this.EmailMessage.hasAttachment;} private set; }

    public PageReference forward() {
        PageReference result = new PageReference('/apex/C_CustomEmail?CaseId='+EmailMessage.ParentId+'&emailId='+theEmailId+'&type=forward');
        result.setRedirect(True);
        return result;
    }


    public PageReference replyAll() {
        PageReference result = new PageReference('/apex/C_CustomEmail?CaseId='+EmailMessage.ParentId+'&emailId='+theEmailId+'&type=replyall');
        result.setRedirect(True);
        return result;
    }


    public PageReference reply() {
        PageReference result = new PageReference('/apex/C_CustomEmail?CaseId='+EmailMessage.ParentId+'&emailId='+theEmailId+'&type=reply');
        result.setRedirect(True);
        return result;
    }
    
    public PageReference newActionItem() {
        Case c = [SELECT Id, ParentId FROM Case WHERE Id = : EmailMessage.ParentId];
        PageReference result;
        if(String.isEmpty(c.ParentId)){        
            result = new PageReference('/apex/C_Action_Item?RecordTypeId=012j0000000L9vh&ParentId='+c.Id+'&OriginatingId='+c.Id+'&isFollowUp=FALSE&EmailId='+EmailMessage.Id);
        }
        else{
            result = new PageReference('/apex/C_Action_Item?RecordTypeId=012j0000000L9vh&ParentId='+c.ParentId+'&OriginatingId='+c.ParentId+'&isFollowUp=FALSE&EmailId='+EmailMessage.Id);        
        }
        result.setRedirect(True);
        return result;
    }


    public EmailMessage EmailMessage { get; set; }
    private String theEmailId;
    
    public C_Email_Object_Controller() {
        this.theEmailId = ApexPages.currentPage().getParameters().get('id');
        EmailMessage = [Select e.ToAddress, e.TextBody, e.SystemModstamp, e.Subject, e.Status, e.ReplyToEmailMessageId, e.ParentId, e.MessageDate, e.LastModifiedDate, e.LastModifiedById, e.IsExternallyVisible, e.IsDeleted, e.Incoming, e.Id, e.HtmlBody, e.Headers, e.HasAttachment, e.FromName, e.FromAddress, e.CreatedDate, e.CreatedById, e.CreatedBy.Name, e.LastModifiedBy.Name, e.CcAddress, e.BccAddress, e.ActivityId, (Select Name, BodyLength, ContentType, Description, CreatedDate, CreatedById From Attachments) From EmailMessage e WHERE Id = : theEmailId];
    }
    
}