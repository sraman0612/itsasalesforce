public class CaseNewSiteVisit {
    
    @AuraEnabled
    public static Case getCase(Id caseId){
        //make your SOQL here from your desired object where you want to place your lightning action
        return [SELECT Id, AccountId, Active_Service_Center__c FROM Case WHERE Id = :caseId];
    }
    
}