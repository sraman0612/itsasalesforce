global class C_DeleteFPAssetShareBatch implements Database.Batchable<sobject>{
    global final String query;
    global final List<string> ofpID;
    global final List<string> nfpID;
    global final List<assetShare> assetShareToDel;
    global final List<string> aID;
    
    global C_DeleteFPAssetShareBatch(string q, List<string> ofpID, List<string> nfpID, List<string> aID)
    {
        this.aID = aID;
        this.nfpID = nfpID;
        this.ofpID = ofpID;
        system.debug(this.aid + ' ' + aid);
        if(q != null)
        {
            query = q;
        }
        else{
            query = 'Select id,assetID from assetshare where userorgroupid IN (Select id from user where accountID in :aID)';
        }
        system.debug(' *****:'+query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<AssetShare> shares)
    {
        List<assetshare> delShare = new List<assetShare>();
        for(AssetShare share : shares)
        {
            for(Asset a : [select id from asset where accountId IN :ofpID])
            {
                if(share.assetID == a.id)
                    delShare.add(share);
            }
        }
        Delete delShare;
    }
    
    global void finish(Database.BatchableContext bc)
    {
        system.debug('Finished deleting rights now to create!!!!!');
        if(nfpId.size() >0)
        {
            C_CreateFPAssetShareBatch cBatch = new C_CreateFPAssetShareBatch(null, nfpID, aID);
            Database.executeBatch(cBatch);
        }
    }
    
}