public with sharing class AccountAssociationMileageUpdater implements Schedulable {
    
    private Account_Association__c a;
    
    public AccountAssociationMileageUpdater (Account_Association__c a) {
        this.a = a;
    }
    
    
    public static void scheduleJob (Account_Association__c a) {
        DateTime now            = DateTime.now();
        DateTime nextRunTime    = now.addSeconds(10);
        String cronString = '' + nextRunTime.second() +' '+ nextRunTime.minute() +' '+ nextRunTime.hour() +' '+ nextRunTime.day() +' '+ nextRunTime.month() +' ? '+ nextRunTime.year(); 
        System.schedule(AccountAssociationMileageUpdater.class.getName() + ' - ' + a.Id + ' - ' + DateTime.now().format(), cronString, new AccountAssociationMileageUpdater(a));
    }
    
    
    
    public void execute (SchedulableContext sc) {
        doWork(a.ID);
        System.abortJob(sc.getTriggerId());
    }
    
    
    @future(callout=true)
    private static void doWork (ID accountAssociationID) {
        try {
            // Retrieve the association we need to update mileage on.
            Account_Association__c a = [Select Id, Customer__c, Service_Center__c From Account_Association__c Where ID = :accountAssociationID Limit 1];
            
            // Get the origin account and create a list containing a single destination account (because the distance finding function is built for taking in multiple destinations).
            Account originAccount                 = [Select Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Tertiary_Disposition__c, Tertiary_Decline_Reason__c, Managed_Care_Survey_Average__c From Account Where Id = :a.Customer__c Limit 1];
            List <Account> destinationAccounts    = new List <Account> ([Select Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Tertiary_Disposition__c, Tertiary_Decline_Reason__c, Managed_Care_Survey_Average__c  From Account Where Id = :a.Service_Center__c Limit 1]);
            
            // Obtain an ordered list of the distances between the origin and all destinations from Google Maps.
            List <COR_GMapsDistanceFinder.GMapsDistance> distances = COR_GMapsDistanceFinder.orderServiceCentersByDistance(originAccount, destinationAccounts);
            
            // Grab the first (and it should be the only) distance entry to come back.
            COR_GMapsDistanceFinder.GMapsDistance distance = distances[0];
            
            if (distance.isValid) {
                a.Mileage__c   = distance.distanceInMiles;
                a.Travel__c    = distance.durationInMinutes;
            }
            
            update a;
        }
        catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
    
}