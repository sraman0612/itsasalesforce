public with sharing class ensxtx_ENSX_VCSettingsSelection{
    @AuraEnabled
    public String Value { get; set;}
    @AuraEnabled
    public String ValueDescription { get; set;}
}