@isTest 
public class CZ_FavoriteTriggerTest {
    public static testmethod void test(){
        User user = [SELECT Id from User where Alias = 'robin'];
        
        System.runAs(user) {
            runFavorites();
        }
    }

    private static void runFavorites() {
        SBQQ__Favorite__c favorite = new SBQQ__Favorite__c();
        favorite.Name = 'Services';
        favorite.CreatedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57);
        favorite.SBQQ__Description__c = null;
        favorite.LastModifiedDate = Datetime.newInstance(2019, 10, 16, 15, 6, 57);
        favorite.List_Price__c = 300;
        favorite.Product_Display_Name__c = 'Services';
        favorite.Product_Number__c = null;
        favorite.Unit_Cost__c = 100;
        insert favorite;
        Product2 prod = new Product2();
        prod.Name = 'test';
        insert prod;
        
        SBQQ__FavoriteProduct__c fp = new SBQQ__FavoriteProduct__c(SBQQ__Favorite__c = favorite.Id, SBQQ__Product__c = prod.Id);
        insert fp;
        
        SBQQ__FavoriteShare__c fs = new SBQQ__FavoriteShare__c(SBQQ__Favorite__c = favorite.Id, SBQQ__User__c = userinfo.getUserId());
        insert fs;
        
        update favorite;
    }
    
    @testSetup
    private static void createData() {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = 'testerson' + System.now().millisecond() + '@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            Community_User_Type__c = 'Global Account'
        );
        Database.insert(portalAccountOwner1);
        
        User user1 = null;
        
        System.runAs ( portalAccountOwner1 ) {
            Account repParentAccount = new Account(
                Name = 'RepParentAccount',
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(repParentAccount);
            
            Account repAccount = new Account(
                Name = 'RepAccount',
                ParentId = repParentAccount.Id,
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(repAccount);
            
            Account portalAccount = new Account(
                Name = 'PortalAccount',
                OwnerId = portalAccountOwner1.Id,
                Rep_Account2__c = repAccount.Id
            );
            Database.insert(portalAccount);
            
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'GlobalUser',
                AccountId = repAccount.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
            Database.insert(contact1);
            
            Profile portalProfile = [SELECT Id FROM Profile where name =: AccountSharingHelper.STR_SALES_REP_ACCOUNT_USER_PROFILE_NAME Limit 1];
            user1 = new User(
                Username = System.now().millisecond() + 'test@test.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'robin',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                Firstname='Bruce',
                LastName = 'GlobalUser',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                Community_User_Type__c = 'Single Account'
            );
            Database.insert(user1);
            
        }
    }
}