public with sharing class UTIL_QuoteLineProdCharacteristicValues {
    public static void updateQuoteLine(Id quoteLineId) {
        if (Test.isRunningTest()) {
            updateQuoteLineData(quoteLineId);
        } else {
            updateQuoteLineFuture(quoteLineId);
        }
    }
    
    @future(callout=true)
    public static void updateQuoteLineFuture(Id quoteLineId) {
        updateQuoteLineData(quoteLineId);
    }
    
    public static void updateQuoteLineData(Id quoteLineId) {
        SBQQ__QuoteLine__c quoteLine = [
            SELECT Id,
                SAP_Configuration__c,
                Distribution_Channel__c,
                SBQQ__Product__r.Name,
                Sales_Text__c,
                Product_Description__c,
                SBQQ__Product__r.Gear_Size__c,
                SBQQ__Product__r.SBQQ__ExternallyConfigurable__c,
                Horsepower__c, 
                Mounting__c, 
                SBQQ__Product__r.MPG4_Code__r.MPG4_Number__c, 
                Mounted_Dryer__c, 
                Pressure__c,
                Recip_Model__c, 
                Voltage__c, 
                Cooling__c, 
                Frame_Size__c,
            	Enclosure__c,
            	Model_Matrix__c
            FROM SBQQ__QuoteLine__c 
            WHERE Id = :quoteLineId 
            LIMIT 1
        ];
        
        Map<String,Characteristic_Values> cvMap = new Map<String,Characteristic_Values>();
        
        Map<String,String> valueDescriptionMap = new Map<String,String>();
        
        if (String.isBlank(quoteLine.SAP_Configuration__c)) {
            return;
        }
        
        SAP_Product_Configuration sapProductConfig = (SAP_Product_Configuration)System.JSON.deserialize(quoteLine.SAP_Configuration__c, SAP_Product_Configuration.class);

        if (sapProductConfig == null || sapProductConfig.selectedCharacteristics == null || sapProductConfig.selectedCharacteristics.isEmpty()) {
            return;
        }
        
        for (Characteristic_Values cv : sapProductConfig.selectedCharacteristics) {
            if (cv.CharacteristicDescription != null) {
                if (excludedCharacteristicDescriptionList.contains(cv.CharacteristicDescription)) {
                    continue;
                }
                
                cvMap.put(cv.CharacteristicDescription, cv);
            }
        }
        
        for (String cvDesc : frameSizeCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Frame_Size__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Frame_Size__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        
        for (String cvDesc : horsepowerCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Horsepower__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Horsepower__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        
        for (String cvDesc : pressureCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Pressure__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Pressure__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        
        for (String cvDesc : recipModelCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Recip_Model__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Recip_Model__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        
        for (String cvDesc : voltageCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Voltage__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Voltage__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        
        for (String cvDesc : mountingCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Mounting__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Mounting__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        
        for (String cvDesc : mountedDryerCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Mounted_Dryer__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Mounted_Dryer__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        for (String cvDesc : enclosureCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Enclosure__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Enclosure__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        
        for (String cvDesc : modelMatrixCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Model_Matrix__c = cv.CharacteristicValue.left(3);
                valueDescriptionMap.put('Model_Matrix__c', cv.CharacteristicValueDescription.left(3));
                break;
            }
        }
        for (String cvDesc : coolingCharacteristicDescriptionList) {
            if (cvMap.containsKey(cvDesc)) {
                Characteristic_Values cv = cvMap.remove(cvDesc);
                quoteLine.Cooling__c = cv.CharacteristicValue;
                valueDescriptionMap.put('Cooling__c', cv.CharacteristicValueDescription);
                break;
            }
        }
        
        List<Machine_Data_Lookup__c> machineDataLookupList = UTIL_MachineDataLookup.getMachineDataLookupForQuoteLine(quoteLine);
        
        Machine_Data_Lookup__c machineDataLookup = null;
        
        if (machineDataLookupList.size() > 0) {
            machineDataLookup = machineDataLookupList.get(0);
        }
        
        if(machineDataLookup != null){
            if(machineDataLookup.Model_Description__c == null) {
                quoteLine.Sales_Text__c = quoteLine.Product_Description__c;
            } else {
                quoteLine.Sales_Text__c = machineDataLookup.Model_Description__c;
            }
        }
        else{
            quoteLine.Sales_Text__c = quoteLine.Product_Description__c;
        }
            
            if (valueDescriptionMap.containsKey('Frame_Size__c')) {
                quoteLine.Sales_Text__c += ' | ' + valueDescriptionMap.get('Frame_Size__c');
            }
            
            if (valueDescriptionMap.containsKey('Horsepower__c')) {
                quoteLine.Sales_Text__c += ' | ' + valueDescriptionMap.get('Horsepower__c');
            }
            
            if (valueDescriptionMap.containsKey('Pressure__c')) {
                quoteLine.Sales_Text__c += ' | ' + valueDescriptionMap.get('Pressure__c');
            }
            
            if (valueDescriptionMap.containsKey('Voltage__c')) {
                quoteLine.Sales_Text__c += ' | ' + valueDescriptionMap.get('Voltage__c');
            }
        
            if(machineDataLookup != null){
                if (!String.isBlank(machineDataLookup.CFM__c)) {
                    quoteLine.Sales_Text__c += ' | ' + machineDataLookup.CFM__c;
                }
            }
            
            if (valueDescriptionMap.containsKey('Mounting__c')) {
                quoteLine.Sales_Text__c += ' | ' + valueDescriptionMap.get('Mounting__c');
            }
            
            if (valueDescriptionMap.containsKey('Mounted_Dryer__c')) {
                quoteLine.Sales_Text__c += ' | ' + valueDescriptionMap.get('Mounted_Dryer__c');
            }
            if (valueDescriptionMap.containsKey('Enclosure__c')) {
                quoteLine.Sales_Text__c += ' | ' + valueDescriptionMap.get('Enclosure__c');
            }
            
            if (valueDescriptionMap.containsKey('Cooling__c')) {
                quoteLine.Sales_Text__c += ' | ' + valueDescriptionMap.get('Cooling__c');
            }
            
            if (!String.isBlank(quoteLine.Sales_Text__c)) {
                for (Characteristic_Values cv : cvMap.values()) {
                    quoteLine.Sales_Text__c += ' | ' + cv.CharacteristicValueDescription;
                }
                if (String.isnotblank(quoteLine.Sales_Text__c)){
                    quoteLine.Product_Display_Description__c = quoteLine.Sales_text__c;
                }
            }
            
            if(String.isBlank(quoteLine.Sales_Text__c)){
                quoteLine.Sales_Text__c = quoteLine.Product_Description__c;
                quoteLine.Product_Display_Description__c = quoteLine.Product_Description__c;
            }
        
        
        update quoteLine;
    }
    
    private static String ClassName = 'UTIL_QuoteLineProdCharacteristicValues';
    
    private static List<String> frameSizeCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.FrameSizeCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> horsepowerCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.HorsepowerCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> pressureCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.PressureCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> recipModelCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.RecipModelCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> voltageCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.VoltageCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> mountingCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.MountingCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> mountedDryerCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.MountedDryerCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> enclosureCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.EnclosureCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> modelMatrixCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.modelMatrixCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> coolingCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.CoolingCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    private static List<String> excludedCharacteristicDescriptionList
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                ClassName + '.ExcludedCharacteristicDescriptions', String.class, new List<String>{});
        }
    }
    
    public class SAP_Product_Configuration {
        public String plant {get;set;}
        public Integer OrderQuantity {get;set;}
        public List<Characteristic_Values> selectedCharacteristics {get;set;}
    }
    
    public class Characteristic_Values {
        public String CharacteristicID {get;set;}
        public String CharacteristicName {get;set;}
        public String CharacteristicDescription {get;set;}
        public String CharacteristicValue {get;set;}
        public String CharacteristicValueDescription {get;set;}
        public Boolean UserModified {get;set;}
    }
}