global with sharing class ENSX_CPQ_Exceptions
{
    global class SimulationException extends Exception { }
}