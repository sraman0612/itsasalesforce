@IsTest
public class TSTC_PausedClaimInterviews {
    @IsTest
    public static void test() {
        CTRL_PausedClaimInterviews.getPausedClaims();
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'Warranty Administration'];
        User user1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = 'testerson' + Math.random() + '@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(user1);

        System.runAs(user1) {
	        CTRL_PausedClaimInterviews.getPausedClaims();
        }
        
        CTRL_PausedClaimInterviews.deleteInterview(null);
    }
}