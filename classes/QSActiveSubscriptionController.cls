public class QSActiveSubscriptionController {

    private final Contract contrct;
    public List<QSActiveSubscriptionController.subscriptionLine> subscriptionLines {set;get;}
    
    public QSActiveSubscriptionController (ApexPages.StandardController stdController) {

        //Set contrct to Contract record
        this.contrct = (Contract) stdController.getRecord();
        
        subscriptionLines = new List<QSActiveSubscriptionController.subscriptionLine>();
        
        Set <String> products = new Set <String>();
        List <SBQQ__Subscription__c> subscriptions = [select id, SBQQ__ProductName__c, SBQQ__Quantity__c, SBQQ__RenewalPrice__c from SBQQ__Subscription__c where SBQQ__Contract__c =: contrct.id];

        for (SBQQ__Subscription__c sub : subscriptions)
        {
            if (!products.contains(sub.SBQQ__ProductName__c) )
            {
                products.add(sub.SBQQ__ProductName__c);
            }
            
            //Error handling to prevent null pointer errors
            if (sub.SBQQ__RenewalPrice__c == null)
            {
                sub.SBQQ__RenewalPrice__c = 0;
            }
        }
        
        if (products.size() > 0)
        {
            Map <String, Decimal> prodQuantities = new Map <String, Decimal>();
            Map <String, Decimal> prodAvgNetPrices = new Map <String, Decimal>();
            
            for (String prod : products)
            {
                Decimal totalQuantity = 0;
                Decimal totalNetUnitPrice = 0;

                for (SBQQ__Subscription__c sub : subscriptions)
                {
                    if (prod == sub.SBQQ__ProductName__c)
                    {
                        totalQuantity = totalQuantity + sub.SBQQ__Quantity__c;
                        totalNetUnitPrice = totalNetUnitPrice + (sub.SBQQ__Quantity__c * sub.SBQQ__RenewalPrice__c);
                    }
                }
                
                if ( totalQuantity > 0 )
                {
                    prodQuantities.put(prod, totalQuantity);
                    prodAvgNetPrices.put(prod, (totalNetUnitPrice/totalQuantity) );
                }
                
            }

            for (String prod : products)
            {
                if (prodQuantities.get(prod) > 0)
                {
                    QSActiveSubscriptionController.subscriptionLine sl = new QSActiveSubscriptionController.subscriptionLine();
                    sl.prodName = prod;
                    sl.prodQuantity = prodQuantities.get(prod).format();
                    sl.prodAveragePrice = '$' + prodAvgNetPrices.get(prod).format();
                                    
                    subscriptionLines.add(sl);
                }
            }
        }
    }
    
    public class subscriptionLine {
    
        public String prodName {set; get;}
        public String prodQuantity {set; get;}
        public String prodAveragePrice {set; get;}
        
        public subscriptionLine () {}
    
    }
}