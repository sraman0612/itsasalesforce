public class AssetCPQFieldMapJsonClass {
    public cls_documents documents;
    public class cls_documents {
        public cls_items[] items;
    }
    public class cls_items {
        public String  Unique_ID;
        public String  Serial_Number;
        public String  Rental_Type;
        public String  Area;
        public String  Division;
        //public String  Branch;
       //public String  Service_Location;
        public String  Primary;
        public String  Model;
        public String  Parent_Frame;
        public String  Frame_Type;
        public String  Compressor_Type;
        public String  Dryer_Type;
        public String  Part_Number;
        public Decimal  PSI;
        public Decimal  HP;
        public Decimal  AMPS;
        public Decimal  Air_Dis_Inch_NPT;
        public String  Cable_Wire_Size;
        public Decimal  Hose_Size_Inches;
        public String  No_Of_Hose_Kits;
        public String  No_Of_Cable_Kits;
        public String  IR_Rental_Contact;
        public String  IR_Rental_Con_Phone;
        public String  IR_Rental_Con_Email;
        public String  Outdoor_Mod;
        public String  Quick_Connects;
       // public String  CableKits;
        public String  Dimensions;
        public String  Asset_ID;
        public String  Corp;
        public String  Country;
        public String  District;
        public String  Street;
        public String  City;
        public String  State;
        public String  Part_Number_New;
        public String  Zip;
        public String  Account;
        //public String  Ship_To_ID;
        public String  Status;
       // public String  Site_Use_ID;https://ircoam--uat.sandbox.my.salesforce.com/_ui/common/apex/debug/ApexCSIPage#
        public String  Manufacturer;
        public String  SFS_Part_Number;
        public String  Product_Description;
       // public String  Product_Type;
        public String  Product_Line;
        public Decimal  Voltage;
        public Decimal  CFM;
        public Decimal  Length;
        public Decimal  Width;
        public Decimal  Height;
        public Decimal  Weekly_Rate;
        public Decimal  Monthly_Rate;
       // public String  Sort_Order;
        public String  Active;
        public Decimal  Weight;
        public String  Skid_Mount;
        public String  sync_action;	//create
        public String Shipping_Latitude;
        public String Shipping_Longitude;
    }
    public static AssetCPQFieldMapJsonClass parse(String json){

        json = json.replaceAll('\n', '\\n');

        system.debug('JSON@ after---->'+json);
        System.debug('json pretty'+(AssetCPQFieldMapJsonClass) System.JSON.deserialize(json, AssetCPQFieldMapJsonClass.class));
        return (AssetCPQFieldMapJsonClass) System.JSON.deserialize(json, AssetCPQFieldMapJsonClass.class);
    }
}