public class GDInsideSyncAcctToContactUsr {
    static String gdiUserName = 'salesForce';
    static String gdiPassword = 'EfkQrXBYhTEk12b8m2NH';
    
    static Map<string,contact> conMap = new Map<string,contact>();
    static Map<String, GDInsideSync.Account> gdiMap = new Map<String, GDInsideSync.Account>();
    static map<string, GDInsideSync.Account> contactsToCreate = new map<string, GDInsideSync.Account>();
    static Map<string,string> sfAcctIds = new Map<string,string>();
    static List<string> sapAcctNumbs = new List<string>();
    static List<Contact> createC = new List<Contact>();
    static Contact c;
    static String response;
    
    public static void sync(string myDate){
        
        Date startDate = Date.valueOf(myDate);//go live 2013-03-13
        integer i=0;
        
        //Map of all salesforce contacts to compare to accounts from GDInside
        for(Contact c:[select id, email, firstname, lastname from contact where email!=''])
        {
            if(c.email == 'kevin@compairsolutions.com')
                    system.debug(Logginglevel.ERROR , 'SF CNTACT EMAIL IS:  '+ c.email);
            conMap.put(c.email.toLowerCase(), c);
        }
        system.debug(Logginglevel.ERROR , conMap.size());
        
        for(account acct: [SELECT Id,account_number__c FROM Account WHERE Account_Number__c != null])
        {
            sfAcctIds.put(acct.account_number__c, acct.id);
        }
        List<string> dupPrev = new List<string>();
        while(i<=1)
        {
            HTTP h = new HTTP();
            HTTPRequest r = new HTTPRequest();
            r.setEndpoint('https://www.gdinside.com/api/public/Accounts?updated_at='+string.valueOf(startDate)+'&limit=1000');
            Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            r.setHeader('Authorization', authorizationHeader);
            r.setMethod('GET');
            HTTPResponse resp = h.send(r);
            response = resp.getBody();
            //response = response.substring(1,response.length()-1);
            //System.debug('WHY IS IT NOT SHOWING**: '+response);
            
            string newResp = '{"Accounts":'+response+'}';
            //System.debug('WHY IS IT NOT SHOWING**: '+newResp);
            
            startDate = startDate.addDays(1);
            
            GDInsideSync gs = GDInsideSync.parse(newResp);
            
            
            //map of all the GDInside Accounts pulled back
            for(GDInsideSync.Account result: gs.accounts) {
                gdiMap.put(result.username.remove('/').toLowerCase(), result);
            }
            //system.debug(Logginglevel.ERROR , gdiMap);
            
            
            For(String tmpKey : gdiMap.keySet())
            {
                if(tmpKey == 'kevin@compairsolutions.com')
                    system.debug(Logginglevel.ERROR , tmpKey);
                
                if(!conMap.containsKey(tmpKey) && gdiMap.get(tmpKey).last_name != '')
                {
                    //put information in a map to be created
                    contactsToCreate.put(tmpKey,gdiMap.get(tmpKey));
                    sapAcctNumbs.add((string)gdiMap.get(tmpKey).sap_account_number);
                }
            }
            system.debug(Logginglevel.ERROR , contactsToCreate.size() + '** '+contactsToCreate);            
                                   
            try{
                //List<string> dupPrev = new List<string>();
                for(String email: contactsToCreate.keySet())
                {
                    string sap = (String)contactsToCreate.get(email).sap_account_number;                
                    
                    c = new Contact();
                    if(sfAcctIds.containsKey(sap))
                        c.accountId = sfAcctIds.get(sap);
                    c.email = (String) contactsToCreate.get(email).username;
                    c.firstName = (String) contactsToCreate.get(email).first_name;
                    c.lastName = (String)contactsToCreate.get(email).last_name;
                    if(!string.isBlank((String)contactsToCreate.get(email).last_name) && !dupPrev.contains((String) contactsToCreate.get(email).username)){
                    	system.debug(Logginglevel.ERROR , 'The username to find DUPE: ' + (String) contactsToCreate.get(email).username);
                        createC.add(c);
                    }
                    dupPrev.add((String) contactsToCreate.get(email).username);
                }
                
            }Catch(Exception e)
            {
                system.debug(Logginglevel.ERROR , 'There was an error somewhere in the code check it out!!! '+e.getMessage() + ' '+e.getLineNumber());
            }
            //contactsToCreate=new Map<string,GDInsideSync.Account>();
            
            system.debug(Logginglevel.ERROR , 'The last date to be updated: ' + startDate);
            system.debug(Logginglevel.ERROR , 'Size of contacts being inserted: ' +CreateC.size());
            i++;
        }
        //GDInsideCreateContact gdi = new GDInsideCreateContact(createC);
        ID jobID = System.enqueueJob(new GDInsideCreateContact(createC));

        //Database.SaveResult[] sList = Database.insert(createC);
        system.debug(Logginglevel.ERROR , 'Size of contacts being inserted: ' +CreateC.size());
        
        /*
Profile proRO = [SELECT Id FROM profile WHERE name='Partner Community User - Read Only'];
Profile proE = [SELECT Id FROM profile WHERE name='Partner Community User - Edit'];

String sfProfileId = (String) dataMap.get('sales_force_profile_id');

User u = new User();

u.username = (String) dataMap.get('username');
u.email = (String) dataMap.get('username');
u.firstName = (String) dataMap.get('first_name');
u.lastName = (String) dataMap.get('last_name');        
String alias = (String) dataMap.get('username');
//Alias must be 8 characters or less
if(alias.length() > 8) {
alias = alias.substring(0, 8);
}
u.alias = alias;
u.languagelocalekey = UserInfo.getLocale();
u.localesidkey = UserInfo.getLocale();
u.emailEncodingKey = 'UTF-8';
u.timeZoneSidKey = 'America/Chicago';

if(sfProfileId == '1'){
u.profileId = proRO.Id;
u.Community_User_Type__c = 'Single Account';
}
else if(sfProfileId == '2'){
u.profileId = proE.Id;
u.Community_User_Type__c = 'Single Account';
}
else if(sfProfileId == '3'){
u.profileId = proE.Id;
u.Community_User_Type__c = 'Single Account';
}
else if(sfProfileId == '4'){
u.profileId = proRO.Id;
u.Community_User_Type__c = 'Global Account';
}
else if(sfProfileId == '5'){
u.profileId = proE.Id;
u.Community_User_Type__c = 'Global Account';
}
else if(sfProfileId == '6'){
u.profileId = proE.Id;
u.Community_User_Type__c = 'Global Account';
}
else{
// return null;
}     

u.contactId = c.Id;
u.GDInside_User_Id__c = (String) dataMap.get('id');

System.debug(u);
insert u;

if(sfProfileId == '3' || sfProfileId == '6'){
PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Export_Report_Permission_Community'];
assignPermissionSet(u.Id,ps.Id);
}


// return u;*/
    }
    
}