@isTest
public class DistiAssetTest {
    private static final Integer RECORD_COUNT = 20;

    @TestSetup
    public static void setupData(){

        Account servicerAccount = new Account(Name = 'testCurrentSErvicer');
        insert servicerAccount;

        List<Asset> assetsToInsert = new List<Asset>();

        for(Integer i = 0; i < RECORD_COUNT; i++) {
            Asset tempAsset = new Asset(
                name = '123456789' + i,
                Current_Servicer__c = servicerAccount.Id,
                AccountId = servicerAccount.Id,
                Dchl__c = 'CM'
            );

            assetsToInsert.add(tempAsset);
        }

        insert assetsToInsert;
    }

    @isTest
    public static void createChildTest() {

        Map<Id, Asset> mapIdToAsset = new Map<Id, Asset>([SELECT Id FROM Asset]);

        List<Child_Asset__c> childAssets = [SELECT Id, Parent_Asset__c FROM Child_Asset__c];

        System.assertEquals(RECORD_COUNT, childAssets.size(), 'We expect a child asset to have been inserted for each Asset inserted');

        for(Child_Asset__c childAsset : childAssets) {
            System.assertEquals(true, mapIdToAsset.containsKey(childAsset.Parent_Asset__c), 'We expect the asset to have the correct parent');
        }
    }

    @isTest
    public static void createChildExistingChildrenTest() {

        Account newServicerAccount = new Account(Name = 'newTestCurrentServicer');
        insert newServicerAccount;

        List<Asset> assetsToUpdate = [SELECT Id, Current_Servicer__c FROM Asset];

        for(Asset assetToUpdate : assetsToUpdate) {
            assetToUpdate.Current_Servicer__c = newServicerAccount.Id;
        }

        Test.startTest();
            update assetsToUpdate;
        Test.stopTest();

        List<Child_Asset__c> childAssets = [SELECT Id, Current_Servicer__c, Parent_Asset__c FROM Child_Asset__c];

        for(Child_Asset__c childAsset : childAssets) {
            System.assertEquals(newServicerAccount.Id, childAsset.Current_Servicer__c, 'We expect the asset to have the service provider updated');
        }
    }
}