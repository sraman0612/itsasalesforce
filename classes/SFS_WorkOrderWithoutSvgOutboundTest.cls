@isTest
public class SFS_WorkOrderWithoutSvgOutboundTest {
	 @isTest
    public static void workOrderWithoutSvgOutboundHandlerTestMethod(){
        //create mock response
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        res.setStatusCode(200);
        //creating data
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        Account billToAcc = SFS_TestDataFactory.getAccount();
        billToAcc.RecordTypeId = accRecID;
        Update billToAcc;
        Account acc = SFS_TestDataFactory.getAccount();
        
        acc.Bill_To_Account__c=billToAcc.Id;
        acc.ShippingCountry = 'USA';
        acc.Type ='Prospect';
        Update acc;
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,true);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='00250506';
        update proList[0]; 
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
        insert lc; 
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acc.Id,loc[0].Id, div[0].Id, null, false);
         WoList[0].SFS_Bill_To_Account__c = billToAcc.Id;
        WoList[0].SFS_Requested_Payment_Terms__c = 'NET 30';
         WoList[0].SFS_PO_Number__c = '1';
        insert WoList[0];
        WoList[0].Status='Open';
        Update WoList[0];
        
        List<Id>WOIdList = new List<Id>();
        WoIdList.add(wolist[0].id);
        String interfaceLabel = 'Work Order without Service Agreement|' + String.ValueOf(wolist[0].id);
        SFS_WorkOrderWithoutSvgOutboundHandler.Run(WoIdList);
        
        try{
            SFS_WorkOrderWIthoutSvgOutboundHandler.IntegrationResponse(res, interfaceLabel);
            System.assert(res.getStatusCode() == 200);
        }catch(System.Exception e){
            
        }
    }
     @isTest
    public static void workOrderWithoutSvgOutboundHandlerTestMethod2(){
        //create mock response
        Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        res.setStatusCode(00);
        //creating data
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        Account billToAcc = SFS_TestDataFactory.getAccount();
        Update billToAcc;
        Account acc = SFS_TestDataFactory.getAccount();
        //acc.Bill_To_Account__c=billToAcc.Id;
        acc.ShippingCountry = 'USA';
        acc.Type ='Ship To';
        Update acc;
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,true);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='00250506';
        update proList[0]; 
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
        insert lc; 
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acc.Id,loc[0].Id, div[0].Id, null, false);
         WoList[0].SFS_Bill_To_Account__c = billToAcc.Id;
        WoList[0].SFS_PO_Number__c = '1';
        WoList[0].SFS_Requested_Payment_Terms__c = 'NET 30';
        insert WoList[0];
        WoList[0].Status='Open';
        Update WoList[0];
        
        List<Id>WOIdList = new List<Id>();
        WoIdList.add(wolist[0].id);
        String interfaceLabel = 'Work Order without Service Agreement|' + String.ValueOf(wolist[0].id);
        SFS_WorkOrderWithoutSvgOutboundHandler.Run(WoIdList);
        
        try{
            SFS_WorkOrderWIthoutSvgOutboundHandler.IntegrationResponse(res, interfaceLabel);
            System.assert(res.getStatusCode() != 200);
        }catch(System.Exception e){
            
        }
    }
    
   
      @isTest
    public static void workOrderWithoutSvgOutboundHandlerTestMethod3(){
        //create mock response
        Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        res.setStatusCode(200);
        //creating data
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        Account billToAcc = SFS_TestDataFactory.getAccount();
        Update billToAcc;
        Account acc = SFS_TestDataFactory.getAccount();
        //acc.Bill_To_Account__c=billToAcc.Id;
        acc.ShippingCountry = 'USA';
        acc.Type ='Ship To';
        Update acc;
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,true);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='00250506';
        update proList[0]; 
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
        insert lc; 
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acc.Id,loc[0].Id, div[0].Id, null, false);
         WoList[0].SFS_Bill_To_Account__c = billToAcc.Id;
        WoList[0].SFS_PO_Number__c = '1';
        WoList[0].SFS_Requested_Payment_Terms__c = 'NET 30';
        WoList[0].SFS_Integration_Status__c = '';
        WoList[0].SFS_Integration_Response__c = '';
        insert WoList[0];
        WoList[0].Status='Open';
        Update WoList[0];
        
        List<Id>WOIdList = new List<Id>();
        WoIdList.add(wolist[0].id);
        String interfaceLabel = 'Work Order without Service Agreement|' + String.ValueOf(wolist[0].id);
        SFS_WorkOrderWithoutSvgOutboundHandler.Run(WoIdList);
        
        try{
            SFS_WorkOrderWIthoutSvgOutboundHandler.IntegrationResponse(res, interfaceLabel);
            System.assert(res.getStatusCode() == 200);
        }catch(System.Exception e){
            
        }
    }
}