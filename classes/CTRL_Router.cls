// Class acts as a redirect based on query string value ENSX_Page (UTIL_PageState.ensxPageRoute)
// - A map of pageKeys to URLs exists in UTIL_PageFlow and can be overridden in appSettings

public with sharing class CTRL_Router
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_Router.class);
    
    public String recordId {get; set;}

    public CTRL_Router() {
        
    }
    
    public CTRL_Router(ApexPages.StandardController controller) {
        if (controller != null) recordId = controller.getId();
    }

    public PageReference actionRoute()
    {
        String pageKey = UTIL_PageState.current.ensxPageRoute;
        UTIL_PageState.current.ensxPageRoute = null;
        PageReference destination = UTIL_PageFlow.redirectTo(pageKey, UTIL_PageState.current);
        System.debug(destination);
        return destination;
    }

    public PageReference quoteCreateActionRoute() {
        String pageKey = UTIL_PageFlow.VFP_OrderCreateUpdate;
        UTIL_PageState.current.ensxPageRoute = null;
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('SAP_Mode', 'SAP_ModeCreate');
        parameters.put('SF_CPQQuoteId', this.recordId);
        PageReference destination = UTIL_PageFlow.redirectTo(pageKey, UTIL_PageState.current);
        destination.getParameters().putAll(parameters);
        System.debug(destination);
        return destination;
    }

}