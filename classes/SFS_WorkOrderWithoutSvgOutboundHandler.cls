/* Author: Sucharitha Suragala - Capgemini
* Date: 20/1/21
* Description: Outbound integration to CPI. Service Agreement without Work Order.
* TODO:
*/
public class SFS_WorkOrderWithoutSvgOutboundHandler {
    @invocableMethod(label = 'Outbound WorkOrder Without Service Agreement' description = 'Send to CPI' Category = 'WorkOrder')
    public static void Run(List<Id> newWorkOrderId){
        //Main Method
        String xmlString = '';
        Map<Double,String> WoxmlMap = new Map<Double,String>();
        Map<Double,String> CLIXMLMap = new Map<Double,String>();
        system.debug('@newWorkOrderId'+newWorkOrderId);        
        ProcessData(newWorkOrderId, WoxmlMap,xmlString);
    }
    public static void ProcessData(List<Id> newWorkOrderId, Map<Double, String> WoxmlMap, String xmlString){
        //Retrieves and transforms data. 
        List<String> XMLList = new List<String>();
        //Get XML lines from SFS_Service_Agreement_With_WO_XML__mdt. 
        SFS_Work_Order_XML_Structure__mdt[] woMaps = [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                      FROM SFS_Work_Order_XML_Structure__mdt Order By SFS_Node_Order__c asc];
        //query the record field names from the flow
        String query1 ='SELECT Id,sfs_po_number__c,WorkOrderNumber,SFS_Ship_To_Party_Site_Use__c,SFS_Division__c,sfs_requested_payment_terms__c,sfs_project_type__c,AccountId,ServiceContractId,SFS_Bill_to_Account__c,SFS_External_Id__c,StartDate,EndDate,SFS_Chargeable__c,SFS_Chargeable_Flag_WO__c,SFS_Ready_To_Bill__c,SFS_Oracle_Billing_Method__c,Description,SFS_Ready_To_Distribute_WO__c, AssetId,SFS_Work_Order_Type__c,SFS_Oracle_End_Date__c,SFS_Oracle_Start_Date__c,SFS_Oracle_Ready_To_Bill__c,SFS_Estimated_Work_Order_Value__c,CurrencyIsoCode,SFS_Integration_Response__c,SFS_Work_Order_Resubmit__c,SFS_Integration_Status__c,SFS__Site_Id__,SFS_Oracle_Status__c,SFS_Asset_Product_Code__c from WorkOrder WHERE Id =: newWorkOrderId';
        String query2 ='SELECT Id, SFS_PO_Number__c,SFS_Siebel_Id__c,SFS_Division__c,SFS_Ship_To_Party_Site_Use__c,sfs_requested_payment_terms__c,sfs_project_type__c,AccountId,ServiceContractId,SFS_Bill_to_Account__c,SFS_External_Id__c,StartDate,EndDate,SFS_Chargeable__c,SFS_Chargeable_Flag_WO__c,SFS_Ready_To_Bill__c,SFS_Oracle_Billing_Method__c,Description,SFS_Ready_To_Distribute_WO__c, AssetId,SFS_Work_Order_Type__c,SFS_Oracle_End_Date__c,SFS_Oracle_Start_Date__c,SFS_Oracle_Ready_To_Bill__c,SFS_Estimated_Work_Order_Value__c,CurrencyIsoCode,SFS_Integration_Response__c,SFS_Work_Order_Resubmit__c,SFS_Integration_Status__c,SFS_Ship_To_Site_Id__c,SFS_Oracle_Status__c,SFS_Asset_Product_Code__c from WorkOrder WHERE Id =: newWorkOrderId';
        
        String siebelId = String.ValueOf([SELECT WorkOrderNumber from WorkOrder where Id =: newWorkOrderId]);
        String query;
        
        //check for siebelId is present else WorkOrderNumber
        If(siebelId != null){
            query = query2;
        }else{
            query = query1;
        }
       
        //Query the record from flow. Used to get record field names.
        WorkOrder wo = new WorkOrder();
        try{
            wo = Database.query(query);
            
        }catch(System.QueryException e){
            //WorkOrder w = [SELECT SFS_Integration_Response__c FROM WorkOrder WHERE Id =: newWorkOrderId];
           /* wo.SFS_Integration_Response__c = String.ValueOf(e);
            wo.SFS_Integration_Status__c = 'ERROR';
            update wo;*/
        }
        
        Map<String, Object> workOrderFields = new Map <String, Object>();
        workOrderFields = wo.getPopulatedFieldsAsMap();
        //Asset a = [SELECT Product2.ProductCode FROM Asset WHERE Id =: wo.AssetId];
        //Account
       
        Account workOrderAcc = new Account();
        try{
            workOrderAcc = [SELECT Id,Oracle_Site_Use_Id__c,SFS_Oracle_Site_Use_Id__c,ShippingState, /*SFS_External_Id__c*/ ShippingPostalCode,ShippingCity, ShippingCountry,IRIT_Customer_Number__c, CTS_Global_Shipping_Address_1__c, CTS_Global_Shipping_Address_2__c, County__c, CTS_Global_Shipping_City__c,
                                CTS_Global_Shipping_State_Province__c, shippingstreet, Country__c, CTS_Global_Shipping_Postal_Code__c, IRIT_Payment_Terms__c,Alias_Name__c, SFS_Oracle_Country__c,Type,Siebel_Id__c
                                FROM Account WHERE Id =: wo.AccountId]; 
        }catch(System.QueryException e){
            /*wo.SFS_Integration_Response__c = String.ValueOf(e);
            wo.SFS_Integration_Status__c = 'ERROR';
            update wo;*/
        }
        
          
        double dropNode = 0;
        if (workOrderAcc.shippingcountry != 'USA' && workOrderAcc.ShippingCountry != 'US' && workOrderAcc.ShippingCountry != 'United States') {
            SFS_Work_Order_XML_Structure__mdt state = SFS_Work_Order_XML_Structure__mdt.getInstance('SFS_Customer_State');  
            dropNode = double.ValueOf(state.SFS_Node_Order__c);
            //CountryPointer = String.ValueOf(state.SFS_XML_Full_Name__c);
        } else {
            SFS_Work_Order_XML_Structure__mdt prov = SFS_Work_Order_XML_Structure__mdt.getInstance('SFS_Customer_Province');  
            dropNode = double.ValueOf(prov.SFS_Node_Order__c);
        }
        
        Map<String, Object> AccountFields = new Map <String, Object>(); 
        AccountFields = workOrderAcc.getPopulatedFieldsAsMap();
        //Bill To Account
        Account BillToAcc = new Account();
        try{
            BillToAcc = [SELECT Id, Oracle_Id__c, SFS_Oracle_Site_Use_Id__c,irit_Customer_Number__c FROM Account WHERE Id=: wo.SFS_Bill_to_Account__c];
        }Catch(System.QueryException e){
           /* wo.SFS_Integration_Response__c = String.ValueOf(e);
            wo.SFS_Integration_Status__c = 'ERROR';
            update wo;
            return;*/
        }
        
        Map<String, Object> BillToFields = BillToAcc.getPopulatedFieldsAsMap();
        
        //Asset
        Asset workOrderAss = new Asset();
        try{
               workOrderAss= [Select Id,Product_Line__c FROM Asset WHERE Id =: wo.AssetId];
        }catch(System.QueryException e){
           /* wo.SFS_Integration_Response__c = String.ValueOf(e);
            wo.SFS_Integration_Status__c = 'ERROR';
            update wo*/
        }
      
        Map<String, Object> AssetFields = new Map <String, Object>(); 
        AssetFields = workOrderAss.getPopulatedFieldsAsMap();
        
        //Division 
        Division__c workOrderDiv = new Division__c();
        try{
            workOrderDiv = [SELECT Id,SFS_Org_Code__c, SFS_Category__c, SFS_Employee_Number__c, SFS_Class_Code__c, SFS_Sales_Branch__c, EBS_System__c,
                                    SFS_External_System_Value__c
                                    FROM Division__c WHERE Id =:wo.SFS_Division__c];
        }catch(System.QueryException e){
           /* wo.SFS_Integration_Response__c = String.ValueOf(e);
            wo.SFS_Integration_Status__c = 'ERROR';
            update wo;*/
        }
        
        
        Map<String, Object> DivisionFields = new Map <String, Object>(); 
        DivisionFields = workOrderDiv.getPopulatedFieldsAsMap(); 
        
        Map<String, Object> ScFieldValues = new Map<String, Object>();
        
        for(String field : workOrderFields.keyset()){
            try{
                ScFieldValues.put(field.toLowerCase(), wo.get(field));
            }catch(SObjectException e){
            }
        }
       /* System.debug('@@@@ACCOUNT MAP LENGTH:   ' + accountFields.Size());
        for(String field : AccountFields.Keyset()){
            ScFieldValues.put(field.toLowerCase(), workOrderAcc.get(field));
        }*/
        
        for(String field : AccountFields.keyset()){
            try{
                if(WorkOrderAcc.Type == 'Prospect'){
                    if(field != 'sfs_external_id__c'){
                        ScFieldValues.put(field.toLowerCase(), workOrderAcc.get(field));
                        System.debug('@@@Field: ' +field.toLowerCase()+ ' Value: '+ workOrderAcc.get(field));
                    }
                    else{
                        ScFieldValues.put('account-sfs_external_id__c', workOrderAcc.get(field));
                        System.debug('@@@TRUE');
                    }
                }else{
                    ScFieldValues.put(field.toLowerCase(), workOrderAcc.get(field));
                    System.debug('@@@Field: ' +field.toLowerCase()+ ' Value: '+ workOrderAcc.get(field));
                }
            }catch(SObjectException e){
            }
        }
        
        for(string field : BillToFields.keyset()){
            if(field == 'irit_Customer_Number__c'){
                ScFieldValues.put('SFS_BillTo_Customer_Number', BillToFields.get(field));
               }
            else {
                 ScFieldValues.put(field.toLowerCase(), BillToFields.get(field));
            }
        }
        
         for(String field : AssetFields.keyset()){
            try{
                ScFieldValues.put(field.toLowerCase(), workOrderAss.get(field));
            }catch(SObjectException e){
            }
        }
        
        for(String field : DivisionFields.Keyset()){
            try{
                ScFieldValues.put(field.toLowerCase(), workOrderDiv.get(field));
            }catch(SObjectException e){
            }
        }
        List<Double> nodeList = new List<Double>();
        for(SFS_Work_Order_XML_Structure__mdt m: woMaps){
            //Some data will need to be hardcoded. We will check for fields that do not need to be hardcoded first.
            if(m.SFS_Node_Order__c != dropNode){
            if(m.SFS_HardCoded_Flag__c == false ){
                nodeList.add(m.SFS_Node_Order__c);
                String sfField = m.SFS_Salesforce_Field__c;
                if(SCFIeldValues.containsKey(sfField)){ 
                    double nodeOrder = m.SFS_Node_Order__c;
                    if(SCFieldValues.get(sffield) != null){
                        //Making sure sffield != null. If not replace with values in SA map, else add the m.SFS_XML_Full_Name value to the map.
                        String xmlfullName = m.SFS_XML_Full_Name__c;
                        String replacement = String.ValueOf(ScFieldValues.get(sffield));
                        replacement = replacement.escapeXML();
                        String newpa = XMLFullName.replace(sffield,replacement); 
                        WoxmlMap.put(nodeOrder,newpa);
                    }  
                    else if(SCFieldValues.get(sffield) == null){
                        WoxmlMap.put(m.SFS_Node_Order__c, m.SFS_XML_Full_Name__c);
                    }
                }
                //Added by Ryan Reddish to replace null fields with empty strings between corresponding xml tags.
                else if(!SCFieldValues.ContainsKey(sffield)){
                    String empty = '';
                    system.debug('m.SFS_XML_Full_Name__c'+m.SFS_XML_Full_Name__c);
                    system.debug('replacement' +m.SFS_XML_Full_Name__c.replace(sffield, empty));
                    String replacement = m.SFS_XML_Full_Name__c.replace(sffield, empty);
                    WoxmlMap.put(m.SFS_Node_Order__c, replacement);
                }
            }
            else if (m.SFS_HardCoded_Flag__c == true){
                nodeList.add(m.SFS_Node_Order__c);
                WoxmlMap.put(m.SFS_Node_Order__c, m.SFS_XML_Full_Name__c);
            }
        }
        }
        
        nodeList.Sort();
        List<String> finalXML = new List<String>();
        for(Double n : nodeList){
            if(n != dropNode){
                 finalXML.add(WoxmlMap.get(n));    
            }
                
        }
        for(String s : finalXML){
            xmlString = xmlString + s;
            System.debug('PAYLOAD: ' + s);
            //System.debug('XMLString ' + xmlString);
        }
        
        
        /*SFS_Integration_Log__c log = new SFS_Integration_Log__c(SFS_Label__c='Work Order');
        Insert log;
        Attachment att = new Attachment();
        att.ParentId = log.Id;
        Att.name = 'XSD ' +DateTime.now();
        att.ContentType = 'text/xml';
        att.Body = Blob.ValueOf(xmlString);
        insert att;*/
        String recordId = String.valueOf(newWorkOrderId[0]);
        System.debug('recordId: ' + recordId);
        String interfaceDetail = 'XXPA2381';
        String interfaceLabel = 'Work Order without Service Agreement|' + recordId;
        
        /*if(wo.SFS_Work_Order_Resubmit__c == true){
            wo.SFS_Work_Order_Resubmit__c = false;
            update wo;
        }*/
        
        if(!Test.isRunningTest()){
            ///interfaceLabel = interfaceLabel + '|' + recordId;
          SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel);
        }else{
            SFS_Outbound_Integration_Callout.HttpCalloutTest('WorkOrder', recordId, xmlString, interfaceDetail, interfaceLabel);
        }
        
    }
    
    public static void IntegrationResponse(HttpResponse res, String interfaceLabel){
        List<String> woId = interfaceLabel.split('\\|');
      
        System.debug('WOID: ' + woId);
        WorkOrder wo = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM WorkOrder WHERE Id =: woId];
        
        if(res.getStatusCode() != 200){
            wo.SFS_Integration_Status__c = 'ERROR';
            wo.SFS_Integration_Response__c = 'Project Creation - Standalone Work Order'+ '\nIntegration Response:\t' + res.getBody();
        }else{
            wo.SFS_Integration_Status__c = 'SYNCING';
            wo.SFS_Integration_Response__c = 'Project Creation - Standalone Work Order'+ '\nIntegration Response:\t' + res.getBody();
        }
        
        update wo;
    }
}