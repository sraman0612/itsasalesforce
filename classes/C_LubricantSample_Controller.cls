public class C_LubricantSample_Controller {
	@AuraEnabled
    public static List<Oil_Sample__c> queryOilSample(Id assetId){
        return [SELECT Id, Name, Collect_Date__c , Hours_On_Machine__c, Oil_Hours__c, Status__c, Comment__c FROM Oil_Sample__c WHERE Serial_Number_Lookup__c =: assetId];
    }
}