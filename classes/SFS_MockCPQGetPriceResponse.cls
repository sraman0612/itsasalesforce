@isTest
global class SFS_MockCPQGetPriceResponse implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"items":[{"itemIdentifier":"1","partNumber":"00250506","unitPrice":102.000000,"calculationInfo":[{"DNCost":81.6,"unitPrice":102}]}]}');
        res.setStatusCode(200);
        return res;
    }
    
        global static HttpResponse invokeMockResponse(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.com/test/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        res.setStatusCode(200);
        //res.setBody('<?xml version="1.0" encoding="UTF-8"?><Response><Status><StatusText><Status><G_TRANSACTIONS><MTL_TRANSACTIONS_INTERFACE><UNIQUE_REFERENCE_NO>123</UNIQUE_REFERENCE_NO><code>ABC</code><errorMessage>Error message 1</errorMessage></MTL_TRANSACTIONS_INTERFACE></G_TRANSACTIONS></Status></StatusText></Status></Response>');
        res.setBody('{"items":[{"itemIdentifier":"1","partNumber":"00250506","unitPrice":102.000000,"calculationInfo":[{"DNCost":81.6,"unitPrice":102}]}]}');
        return res;
    }
}