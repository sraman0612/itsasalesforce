@isTest
private class GDInsideSyncAcctToContactBatchSchedulerT {

    private static testmethod void testScheduler() {
        Test.startTest();
			GDInsideSyncAcctToContactBatchScheduler scheduler = new GDInsideSyncAcctToContactBatchScheduler();   
        	String cronExp = '0 0 0 1 1 ? 2099';
        	Id jobId = System.schedule('Test Scheduler', cronExp, scheduler);        
        	
			CronTrigger cronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId]; 
        	System.assertEquals(0, cronTrigger.TimesTriggered, 'We expect our job to have been scheduled and not yet triggered');
        Test.stopTest();
    }
    
}