@isTest
public class C_Service_History_TriggerTest {
    @isTest static void testWarrantyCompliance()
    {
        Account testAcc = new Account(name = 'Gardner Denver');
        insert testAcc;
        
        Asset serial = new Asset();
        serial.AccountId = testAcc.Id;
        serial.SerialNumber = '1234567';
        serial.Name = 'test';
        serial.Dchl__c = 'test';
        insert serial;
        
    	Service_History__c sh = new Service_History__c();
        sh.Serial_Number__c = serial.id;
        insert sh;
    }
}