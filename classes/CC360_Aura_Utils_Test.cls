@isTest
public with sharing class CC360_Aura_Utils_Test {
  public static testMethod void doTest() {
    Asset a = new Asset(Name = 'Test 987');
    insert a;
    Child_Asset__c child = new Child_Asset__c( Parent_Asset__c = a.Id );
    insert child;
    String id = CC360_Aura_Utils.getCARLRecord(a.Id);
  
    System.assert(!String.isEmpty(id));
      
    Asset b = CC360_Aura_Utils.getAssetRecord(a.Id);
    System.assert(b != Null);
  }
}