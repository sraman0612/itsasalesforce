public class C_AssetShareCARL_Batch implements Database.batchable<sObject> {
    
    public final String query_template_global = 'SELECT Id, Parent_Asset__c FROM Child_Asset__c WHERE Current_Servicer__r.ParentId = \'\'{0}\'\''; 
    public final String query_template_single = 'SELECT Id, Parent_Asset__c FROM Child_Asset__c WHERE Current_Servicer__c = \'\'{0}\'\''; 
    public final Boolean isGlobal;
    public final String query;
    public final String accountId;
    public final Id userId;
    
    
    
    public C_AssetShareCARL_Batch(String x_accountId, Id x_userId, Boolean x_isGlobal){
        this.userId = x_userId;
        this.isGlobal = x_isGlobal;
        this.accountId = x_accountId; 
        
        if(x_isGlobal){
            query = String.format(query_template_global, new List<Object>{x_accountId});
        }
        else{
            query = String.format(query_template_single, new List<Object>{x_accountId});
        }
        
        System.debug(query);

    }
        
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);
        
    }
    
    
    public void execute(Database.BatchableContext info, List<Child_Asset__c> scope){
        List<AssetShare> shareRecords = new List<AssetShare>();
        
        for(Child_Asset__c row : scope){
            
            AssetShare share = new AssetShare();
            share.assetAccessLevel='Edit';
            share.AssetId = row.Parent_Asset__c;
            share.RowCause='Manual';
            share.UserOrGroupId=userId;
            
            shareRecords.add(share);
        }
        System.debug('shareRecords');
        System.debug(shareRecords);
        insert shareRecords;
    }     
    
    public void finish(Database.BatchableContext info){     

    } 
    
}