public class CTRL_SAP_WarrantyClaimStatus {

    @AuraEnabled
    public static UTIL_Aura.Response getStatus(String warrantyClaimId) {
        if (String.isBlank(warrantyClaimId)) {
            return UTIL_Aura.createResponse(null);
        }
        
        Warranty_Claim__c warrantyClaim = [SELECT Id, SAP_Status__c, SAP_Messages__c from Warranty_Claim__c where Id = : warrantyClaimId LIMIT 1];
        
        return UTIL_Aura.createResponse(warrantyClaim);
    }
}