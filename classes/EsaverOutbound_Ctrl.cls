public with sharing class EsaverOutbound_Ctrl {
    
    @TestVisible static final String ESAVER_OAUTH_TOKEN_URL = 'https://esaver.demo.vervocity.io/api/oauth/token';
    @TestVisible static final String ESAVER_START_URL = 'https://esaver.demo.vervocity.io/api/v1/comparisons/start';
    
    public static GDInsideOAuthInfo gdInsideOauthInfo {
        get {
            
            if(gdInsideOauthInfo == null){
                
                HttpRequest req = new HttpRequest();
                req.setHeader('Content-Type','application/json');
                req.setMethod('POST');
                req.setEndpoint(ESAVER_OAUTH_TOKEN_URL);
                
                Map<String, String> bodyMap = new Map<String, String>{
                    'username' => 'sfdcadmin2@gardnerdenver.com',
                        'password' => 'Gardner1859',
                        'client_id' => '1',
                        'client_secret' => '4FlbnXHfkVEFkzIAmr34xFAj4wnese9kKtI9PHgs',
                        'scope' => '*',
                        'grant_type' => 'password'
                        };
                            
                            req.setBody(JSON.serialize(bodyMap));
                
                HttpResponse resp = (new HTTP()).send(req);
                
                if(resp.getStatusCode() == 200){
                    gdInsideOauthInfo = (EsaverOutbound_Ctrl.GDInsideOAuthInfo)JSON.deserialize(resp.getBody(), EsaverOutbound_Ctrl.GDInsideOAuthInfo.class);
                }
            }
            
            return gdInsideOauthInfo;
        }
        private set;
    }
    
    @testVisible private String getAccessToken(){
        return gdInsideOauthInfo.access_token;
    }
    
    public PageReference doRedirect(){
        
        PageReference result;
        
        String quoteId = ApexPages.currentPage().getParameters().get('Id');
        
        SBQQ__Quote__c quote = [
            SELECT  End_Customer_Account__c, SBQQ__Distributor__c, Name
            FROM    SBQQ__Quote__c
            WHERE   Id = :quoteId
        ];
        
        Account distributor, customer;
        Boolean hasMissingInfo = false;
        
        if(quote.End_Customer_Account__c != null){
            customer = [
                SELECT  Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, Phone, Fax
                FROM    Account
                WHERE   Id = : quote.End_Customer_Account__c
            ];
        }
        
        if(customer == null){
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Customer information not found.')
                                );
            
            hasMissingInfo = true;
        }
        
        if(quote.SBQQ__Distributor__c != null){
            distributor = [
                SELECT  Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, Phone, Fax
                FROM    Account
                WHERE   Id = : quote.SBQQ__Distributor__c
            ];
        }
        
        if(distributor == null){
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Distributor information not found.')
                                );
            
            hasMissingInfo = true;
        }
        
        if(hasMissingInfo){
            return null;
        }
        
        List<SBQQ__QuoteLine__c> quoteLines = [
            SELECT 
            SBQQ__Product__r.HP_KW_Mapping_Table__r.HP__c, 
            SBQQ__Product__r.mg4__c, 
            SBQQ__Product__r.pressure_capability_psig__c 
            FROM SBQQ__QuoteLine__c
            WHERE SBQQ__Quote__c = : quoteId
        ];
        
        Map<String,Object> salesforceItems = null;
        
        if (quoteLines != null) {
            for (SBQQ__QuoteLine__c quoteLine : quoteLines) {
                if (quoteLine.SBQQ__Product__r.HP_KW_Mapping_Table__r.HP__c != null 
                    && quoteLine.SBQQ__Product__r.mg4__c != null 
                    && quoteLine.SBQQ__Product__r.pressure_capability_psig__c != null) {
                        salesforceItems = new Map<String,Object>();
                        
                        salesforceItems.put('mg4', Integer.valueOf(quoteLine.SBQQ__Product__r.mg4__c));
                        salesforceItems.put('pressure', Integer.valueOf(quoteLine.SBQQ__Product__r.pressure_capability_psig__c));
                        salesforceItems.put('horsepower', Integer.valueOf(quoteLine.SBQQ__Product__r.HP_KW_Mapping_Table__r.HP__c));
                        
                        break;
                    }
            }
        }
        
        EsaverData postData = new EsaverData( customer, distributor, quote, salesforceItems );
        String jsonBody = JSON.serialize(postData);
        System.debug(LoggingLevel.ERROR, 'postData');
        System.debug(LoggingLevel.ERROR, postData);
        System.debug(LoggingLevel.ERROR, 'jsonBody');
        System.debug(LoggingLevel.ERROR, jsonBody);
        
        HttpRequest postCallToEsaver = new HttpRequest();
        
        postCallToEsaver.setMethod('POST');
        postCallToEsaver.setEndpoint(ESAVER_START_URL);
        postCallToEsaver.setHeader('Content-Type', 'application/json');
        postCallToEsaver.setHeader('Authorization', 'Bearer ' + getAccessToken());
        postCallToEsaver.setBody(jsonBody);
        
        HttpResponse esaverPostCallResponse = (new HTTP()).send(postCallToEsaver);
        
        if(esaverPostCallResponse.getStatusCode() == 200){
            
            System.debug(LoggingLevel.ERROR, '=====================================');
            System.debug(LoggingLevel.ERROR, esaverPostCallResponse.getBody());
            System.debug(LoggingLevel.ERROR, '=====================================');
            
            EsaverResponse responseData = (EsaverResponse)JSON.deserialize(esaverPostCallResponse.getBody(), EsaverResponse.class);
            
            if(responseData.Success){
                System.debug(LoggingLevel.ERROR, 'redirecting to(\"' + responseData.url + '\"');
                String encodedURL = EncodingUtil.URLEncode(responseData.url,'UTF-8');
                result = new PageReference(responseData.url);
                result.setRedirect(true);
            }
            else {
                
                if(responseData.Errors.size() > 0){
                    for(String error : responseData.Errors.keySet()){
                        
                        ApexPages.addMessage(new ApexPages.Message(
                            ApexPages.Severity.ERROR,
                            responseData.Errors.get(error)[0])
                                            );
                    }
                }
                else {
                    ApexPages.addMessage(
                        new ApexPages.Message(ApexPages.Severity.ERROR,
                                              'Unknown error with Esaver application')
                    );
                }
            }
        }
        else{
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Error connecting with Esaver application')
                                );
        }
        
        return result;
    }
    
    private class EsaverData {
        
        public String email;
        public Comparison comparison;
        public String sales_force_quote_id;
        
        public EsaverData(Account customer, Account distributor, SBQQ__Quote__c quote, Map<String,Object> salesforceItems){
            
            this.email = UserInfo.getUserEmail();
            this.sales_force_quote_id = quote.Id;
            this.comparison = new Comparison(customer, distributor, quote.Name, salesforceItems);
        }
    }
    
    private class Comparison {
        
        public String customer_address;
        public String customer_city;
        public String customer_name;
        public String customer_phone;
        public String customer_state;
        public String customer_zip;
        
        public String distributor_address;
        public String distributor_city;
        public String distributor_contact;
        public String distributor_email;
        public String distributor_fax;
        public String distributor_name;
        public String distributor_phone;
        public String distributor_state;
        public String distributor_zip;
        
        public String title;
        
        public Map<String,Object> items_from_salesforce;
        
        public Comparison(Account customer, Account distributor, String quoteNumber, Map<String,Object> salesforceItems) {
            
            String portalUserContactEmail = [SELECT Email FROM User WHERE Id = :UserInfo.getUserId()][0].Email;
            
            this.customer_name = customer.Name;
            this.title = quoteNumber;
            
            this.customer_address = customer.BillingStreet;
            this.customer_city = customer.BillingCity;
            this.customer_state = customer.BillingState;
            this.customer_zip = customer.BillingPostalCode;
            this.customer_phone = customer.Phone;
            
            this.distributor_name = distributor.Name;
            this.distributor_contact = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
            this.distributor_address = distributor.BillingStreet;
            this.distributor_city = distributor.BillingCity;
            this.distributor_state = distributor.BillingState;
            this.distributor_zip = distributor.BillingPostalCode;
            this.distributor_phone = distributor.Phone;
            this.distributor_fax = distributor.Fax;
            this.distributor_email = portalUserContactEmail;
            
            
            this.items_from_salesforce = new Map<String,Object>();
            
            this.items_from_salesforce = salesforceItems;
        }
    }
    
    @TestVisible private class EsaverResponse {
        
        public Boolean Success;
        public Map<String, List<String>> Errors;
        public String url;
    }
    
    public class GDInsideOAuthInfo {
        
        public String token_type;
        public String expires_in;
        public String access_token;
        public String refresh_token;
    }
}