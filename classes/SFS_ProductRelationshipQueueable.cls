/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 28/01/2022
* @description: Trigger on Product Relationship called from SFS_ProductRelationshipTrigger
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001    SIF-69     28/01/2022      Queueable class on Product Relationship
Rahul Chauhan		 M-002	  SIF-4859	  25/07/2023	  Updated the logic for "Related" relation ship
============================================================================================================*/
public class SFS_ProductRelationshipQueueable implements Queueable{
    private List<SFS_Product_Relationship__c> newProdRelationshipList;
    private Map<Id,SFS_Product_Relationship__c> oldMap;
    public String replacingProductName;
    public SFS_ProductRelationshipQueueable(List<SFS_Product_Relationship__c> newProdrelationships,Map<Id,SFS_Product_Relationship__c> oldRecordsMap){
        this.newProdRelationshipList=newProdrelationships;
        this.oldMap=oldRecordsMap;
    }
    public void execute(QueueableContext context) {
        System.debug('Debug from Queue NewList-->'+newProdRelationshipList);
        Set<Id> productIds=new Set<Id>();
        Set<String> productCodes=new Set<String>();
        for(SFS_Product_Relationship__c prodRelationship:newProdRelationshipList){
            if(prodRelationship.SFS_Product__c!=null){
                productIds.add(prodRelationship.SFS_Product__c);
            }
            if(prodRelationship.SFS_Related_Item__c!=null){
                productCodes.add(prodRelationship.SFS_Related_Item__c);
                replacingProductName = prodRelationship.SFS_Related_Item__c;
            }
        }
        //To get existing product relationship records for the product
        Map<Id,List<SFS_Product_Relationship__c>> existingProdRelMap= new Map<Id,List<SFS_Product_Relationship__c>>();
        //To get Asset parts Required records for the product
        Map<Id,List<CAP_IR_Asset_Part_Required__c>> partsRequiredMap= new Map<Id,List<CAP_IR_Asset_Part_Required__c>>();
        //To get Product Request line items for the product
        Map<Id,List<ProductRequestLineItem>> prliMap= new Map<Id,List<ProductRequestLineItem>>();
        //To get charges related to the product
        Map<Id,List<CAP_IR_Charge__c>> chargesMap= new Map<Id,List<CAP_IR_Charge__c>>();
        //To get products required in worktype for the product
        Map<Id,List<ProductRequired>> productsRequiredMap= new Map<Id,List<ProductRequired>>();
        Map<Id,String> productToProductCode=new Map<Id,String>();
        Map<Id,Boolean> productToSuperseded=new Map<Id,Boolean>();
        List<Product2> productList=[Select id,Name,ProductCode,SFS_Superceded__c,(Select Id,Name, SFS_Product__c,SFS_Related_Item__c from SFS_Product_Relationships__r where SFS_Relationship_Type__c='Supersession' OR SFS_Relationship_Type__c='Related'),
                                    (Select Id,Name, Product__c from CAP_IR_Asset_Product_Required__r),
                                    (Select Id,Product2Id,ProductRequestLineItemNumber,Parent.WorkOrderId,Parent.WorkOrder.MaintenancePlanId,parent.OwnerId 
                                     from ProductRequestLineItems where Parent.WorkOrder.MaintenancePlanId=null and status='Open'),
                                    (Select Id, CAP_IR_Status__c, SFS_Product__c, SFS_Product__r.name,CAP_IR_Work_Order__r.OwnerId from Charges__r where CAP_IR_Status__c='New'),
                                    (Select Id,ParentRecordType,Product2.name from ProductsRequired)
                                    from Product2 where Id IN :productIds];
        for(Product2 prod : productList)
        {
            existingProdRelMap.put(prod.Id,prod.SFS_Product_Relationships__r);
            for(SFS_Product_Relationship__c rel:prod.SFS_Product_Relationships__r){
                productCodes.add(rel.SFS_Related_Item__c);
              
            }
            partsRequiredMap.put(prod.Id,prod.CAP_IR_Asset_Product_Required__r);
            prliMap.put(prod.Id,prod.ProductRequestLineItems);
            chargesMap.put(prod.Id,prod.Charges__r);
            productsRequiredMap.put(prod.Id,prod.ProductsRequired);
            productToProductCode.put(prod.Id,prod.ProductCode);
            productToSuperseded.put(prod.Id,prod.SFS_Superceded__c);
        }
        //Map to get product from product code
        Map<String,Id> productCodeToProductMap=new Map<String,Id>();
        Map<String,String> productCodeToProduct=new Map<String,String>();
        if(productCodes.size()>0){
            List<Product2> allProducts=[Select Id,Name,ProductCode,SFS_Oracle_Orderable__c from Product2 where ProductCode IN :productCodes and SFS_Oracle_Orderable__c='Y'];
            for(Product2 prod:allProducts){
                productCodeToProductMap.put(prod.ProductCode,prod.Id);
                productCodeToProduct.put(prod.ProductCode,prod.Name);
            } 
        }
        System.debug('26--'+productCodeToProductMap);
        System.debug('38--'+productsRequiredMap);
        List<groupmember> careTeamQueue=[Select Id,Group.Type,Group.Name,GroupId,UserOrGroupId,group.DeveloperName from GroupMember 
                                         where Group.developerName=:System.label.Care_Service_Agreement_queue_name and Group.Type='Queue'];
        Set<Id> groupIds=new Set<Id>();
        Set<Id> userIds=new Set<Id>();
        For(groupmember member:careTeamQueue){
            String userOrGroupId=member.UserOrGroupId;
            if(userOrGroupId.substring(0,3)=='005'){
                userIds.add(member.UserOrGroupId);
            }else if(userOrGroupId.substring(0,3)=='00G'){
                groupIds.add(member.UserOrGroupId);
            }
        }
        List<groupmember> CareTeamGroupMembers=[Select Id,Group.Type,Group.Name,GroupId,UserOrGroupId,group.DeveloperName from GroupMember 
                                                where groupId IN :groupIds];
        For(groupmember member:CareTeamGroupMembers){
            userIds.add(member.UserOrGroupId);
        }
        system.debug('care team users '+userIds);
        List<CAP_IR_Asset_Part_Required__c> newPartsRequiredList=new List<CAP_IR_Asset_Part_Required__c>();
        List<ProductRequestLineItem> newPrliList=new List<ProductRequestLineItem>();
        List<CAP_IR_Charge__c> newChargeList=new List<CAP_IR_Charge__c>();
        List<ProductRequired> newProductRequiredList=new List<ProductRequired>();
        List<FeedItem> feedItemList=new List<FeedItem>();
        Map<Id,Set<Id>> productToUserMap=new Map<Id,Set<Id>>();
        For(SFS_Product_Relationship__c prodRelationship:newProdRelationshipList){
            List<SFS_Product_Relationship__c> oldProdRelList= existingProdRelMap.get(prodRelationship.SFS_Product__c);
            system.debug('55- old prodRel '+oldProdRelList.size());
            if(oldProdRelList.size() > -1){
                Id newProductId=productCodeToProductMap.get(prodRelationship.SFS_Related_Item__c);
                System.debug('Product from related item '+newProductId);
                For(CAP_IR_Asset_Part_Required__c partsRequired:partsRequiredMap.get(prodRelationship.SFS_Product__c)){
                    
                    partsRequired.Product__c=newProductId;
                    newPartsRequiredList.add(partsRequired);
                    System.debug('Debug from Queue newPartsRequiredList-->'+newPartsRequiredList);
                }
                For(ProductRequestLineItem prli:prliMap.get(prodRelationship.SFS_Product__c)){
                    userIds.add(prli.parent.OwnerId);
                    prli.Product2Id=newProductId;                 
                    newPrliList.add(prli);
                }
                For(CAP_IR_Charge__c charge:chargesMap.get(prodRelationship.SFS_Product__c)){
                    userIds.add(charge.CAP_IR_Work_Order__r.OwnerId);
                    charge.SFS_Product__c=newProductId;
                    newChargeList.add(charge);
                }
               // System.debug('BEFORE PREQ');
                String currentRecord;
                For(ProductRequired record:productsRequiredMap.get(prodRelationship.SFS_Product__c))
                {
                    record.Product2Id=newProductId;
                    currentRecord = record.Id;
                    newProductRequiredList.add(record);

                }
               // System.debug('AFTER PREQ ' + currentRecord);
                    if(userIds.size()>0){
                    productToUserMap.put(prodRelationship.SFS_Product__c,userIds);
                }
         
            }
        }
        system.debug('135--'+productToUserMap);
        system.debug('1001--'+ newProductRequiredList);
        for(String key:productToUserMap.keyset()){
            Set<Id> userSet=productToUserMap.get(key);
            for(Id userId:userSet){
                FeedItem fi = new FeedItem();
                fi.IsRichText=true;
                fi.Body='<b>Product # '+productToProductCode.get(key)+' has been superseded by ' + replacingProductName + '</b><p>&nbsp;</p>The Product has been Superseded by multiple Products, please check all Asset Parts Required, Product Request or Charges using the Product and update using the Product’s Notes details';
                fi.ParentId =userId;
                fi.Visibility = 'AllUsers';
                feedItemList.add(fi);
                System.debug('Debug from Queue feedItemList-->'+feedItemList);
            }
        }
        List<Product2> updatedProductList=new List<Product2>();
        for(Id productId:productIds){
            List<SFS_Product_Relationship__c> prodRelList= existingProdRelMap.get(productId);
            String notes='';
            System.debug('157--'+prodRelList.size());
            if(prodRelList.size() != 0){
                for(SFS_Product_Relationship__c rel:prodRelList){
                    system.debug('159--'+productCodeToProduct.get(rel.SFS_Related_Item__c));
                    if(notes==''){
                        String prodName=productCodeToProduct.get(rel.SFS_Related_Item__c);
                        if(prodName!=null){
                            notes=prodName;
                        }
                    }else{
                        String prodName=productCodeToProduct.get(rel.SFS_Related_Item__c);
                        if(prodName!=null){
                          notes=notes+';'+prodName;                        
                        }
                    }
                }
            }
            Product2 prod=new Product2(Id=productId,SFS_Notes__c=notes);
            System.debug('167--'+productId);
            updatedProductList.add(prod);
        }
        if(newProdRelationshipList.size()==1){
            if(productList.size()>0){
                update updatedProductList;
            }
            if(newPartsRequiredList.size()>0){
                update newPartsRequiredList;
                
            }
            if(newPrliList.size()>0){
                update newPrliList;
            }
            if(newChargeList.size()>0){
                update newChargeList;
            }
            If(newProductRequiredList.size()>0){
                update newProductRequiredList;
            }
        }
        if(feedItemList.size()>0){
            insert feedItemList;
        }
        System.debug('59--'+newProductRequiredList);
    }
}