@isTest
public class ensxtx_TSTE_VCCharacteristicValues
{
    @isTest static void testGetSBOAllowedValuesForModel ()
    {
        Test.startTest();
        ensxtx_ENSX_VCCharacteristicValues characteristicValue = new ensxtx_ENSX_VCCharacteristicValues();
        characteristicValue.ValueDescription = 'ValueDescription';
        characteristicValue.Value = 'Value';
        characteristicValue.CharacteristicId = 'CharacteristicId';
        characteristicValue.CharacteristicDescription = 'CharacteristicDescription';
        ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES allowedValue = ensxtx_ENSX_VCCharacteristicValues.getSBOAllowedValuesForModel(characteristicValue);
        System.assert(allowedValue.CharacteristicValueDescription == 'ValueDescription');
        Test.stopTest();
    }

    @isTest static void testGetSBOASelectedValuesForModel ()
    {
        Test.startTest();
        ensxtx_ENSX_VCCharacteristicValues characteristicValue = new ensxtx_ENSX_VCCharacteristicValues();
        characteristicValue.ValueDescription = 'ValueDescription';
        characteristicValue.Value = 'Value';
        characteristicValue.CharacteristicId = 'CharacteristicId';
        characteristicValue.CharacteristicDescription = 'CharacteristicDescription';
        ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES selectedValue = ensxtx_ENSX_VCCharacteristicValues.getSBOASelectedValuesForModel(characteristicValue);
        System.assert(selectedValue.CharacteristicValue == 'Value');
        Test.stopTest();
    }
}