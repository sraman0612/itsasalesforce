public class C_UpdateParent{
    @InvocableMethod
    public static void updateCase(List<Id> recordIds){     
        
        List<Case> clist = new List<Case>();
        
        for(Case c : [Select id, status, Action_Item_Status_Rollup__c,recordtypeid ,(select id from Cases where status!='Closed') from Case Where Id IN :recordIds]){         
           
           system.debug('****Action_Item_Status_Rollup__c  '+c.Action_Item_Status_Rollup__c );

           system.debug('****subquery '+c.Cases.size());
           system.debug('****recordtype id '+c.recordtypeid);
           
           if(c.Cases.IsEmpty()){            
               c.Status ='Open'; 
               cList.add(c);                  
            } 
        }
        system.debug('****  clist'+clist.size()); 
        if(!clist.IsEmpty()){ 
            update clist; 
        }        
    }
    
}