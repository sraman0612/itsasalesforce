/*=========================================================================================================
* @author Ryan Reddish, Capgemini
* @date 13/07/2022
* @description: Test class for .

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

===========================================================================================================*/
@isTest
public class SFS_Service_Agreement_w_Line_Int_Test {
    @isTest
    public static void CallProjectCreationClass(){
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        
		Division__c div =SFS_TestDataFactory.getDivision();
        Account billToAcc = SFS_TestDataFactory.getAccount();
        billToAcc.RecordTypeId = rtAcc;
        Update billToAcc;
        Account acc = SFS_TestDataFactory.getAccount();
        acc.Bill_To_Account__c=billToAcc.Id;
        acc.ShippingCountry = 'United States';
        acc.Type ='Prospect';
        Update acc;
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(2,div.Id,false);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='00250506';
        update proList[0];
        List<ServiceContract> svc = SFS_TestDataFactory.createServiceAgreement(1, acc.Id,true);
        System.debug('@@@@PB: ' + svc[0].Pricebook2Id);
        List<ContractLineItem> cliRec = SFS_TestDataFactory.createServiceAgreementLineItem(1, svc[0].Id,false);
      	List<PricebookEntry> pbe = SFS_TestDataFactory.CreatePriceBookEntry(1, true);
        System.debug('@@@PBECREATE: ' + pbe[0]);
            //[SELECT Id FROM PriceBookEntry WHERE Pricebook2Id =: svc[0].Pricebook2Id LIMIT 1];
        ContractLineItem cli = new ContractLineItem();
        cli.EndDate = System.today().addYears(2);
        cli.StartDate =System.today();
        cli.SFS_Billing_Method__c = 'Schedule Based';
        //cli.Product2Id= '01t3a000005AY74AAG';
        //cli.sfs_product_code__c= '00250506';
        //cli.sfs_product_description__c='TEST';
        cli.ServiceContractId = svc[0].id;
        cli.SFS_Ready_To_Bill__c= true;
        cli.SFS_Ready_to_Distribute1__c= true;
        cli.Quantity = 1;
        cli.UnitPrice = 100;
        cli.PricebookEntryId = pbe[0].id;
        //SFS_ServiceAgreementOutboundHandler.Run(svc);
        //System.assertEquals(expected, actual)
        System.debug('@@@@PBE: ' + cli.PricebookEntry.Pricebook2Id);
       database.insert(cli);
        
       SFS_ServiceAgreementOutboundHandler.Run(svc);
       String IdString = String.ValueOf(svc[0].id); 
       String interfaceLabel = 'Service Agreement with SALI|' + IdString;
        
       //System.assert(svc.SFS_Integration_Status__c = 'SYNCING');
        try{
            SFS_ServiceAgreementOutboundHandler.InitialResponse(res, interfaceLabel);
        }catch(system.Exception e){
            
        }
    }
}