/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 30/05/2022
* @description: Test class for SFS_WorkOrderWithSvgOutboundHandler

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

===========================================================================================================*/
@isTest
public class SFS_WorkOrderWithSvgOutboundHandler_Test {
    @isTest
    public static void workOrderWithSvgOutboundHandlerTestMethod(){
        Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        
        res.setStatusCode(200);
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        /*Account billToAcc = SFS_TestDataFactory.getAccount();
billToAcc.RecordTypeId = rtAcc;
Update billToAcc;
Account acc = SFS_TestDataFactory.getAccount();
acc.Bill_To_Account__c=billToAcc.Id;
acc.RecordTypeId = rtAcc;
acc.IRIT_Payment_Terms__c='NET 30';
acc.Type ='Prospect';
Update acc;*/
        String accRecID1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList1=SFS_TestDataFactory.createAccounts(2, false);
        accountList1[0].IRIT_Customer_Number__c='123';
        accountList1[0].CurrencyIsoCode='USD';
        accountList1[0].RecordTypeId=accRecID1;
        insert accountList1[0];
        accountList1[1].IRIT_Customer_Number__c='123';
        accountList1[1].type = 'ship to';
        accountList1[1].Bill_To_Account__c = accountList1[0].Id;
        accountList1[1].CurrencyIsoCode='USD';
        insert accountList1[1];
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,true);
        List<PricebookEntry> pEntry = SFS_TestDataFactory.createPricebookEntry(1,true);
        //system.debug('pEntry Currency '+pEntry.currCurrencyIsoCodeency);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='1234';
        update proList[0];
        /*
List<ServiceContract> svc = SFS_TestDataFactory.createServiceAgreement(1, acc.Id,false);
svc[0].SFS_Bill_To_Account__c = billToAcc.Id;
svc[0].SFS_Status__c = 'APPROVED';
//svc[0].CurrencyIsoCode = 'USA';

insert svc[0];
svc[0].SFS_External_Id__c=svc[0].id;
List<ContractLineItem> cliRec = SFS_TestDataFactory.createServiceAgreementLineItem(1, svc[0].Id,false);
cliRec[0].SFS_Product__c = proList[0].Id;
cliRec[0].PricebookEntryId = pEntry[0].id;

//cliRec[0].SFS_Product_Code__c = '006';
//cliRec[0].Product2Id = proList[0].id;
insert cliRec[0];
*/
        List<ServiceContract> svc=SFS_TestDataFactory.createServiceAgreement(1,accountList1[1].id,false);
        svc[0].SFS_Invoice_Frequency__c = 'Annually';
        svc[0].Service_Charge_Frequency__c = 'Annually';
        //svc[0].SFS_Status__c = 'APPROVED';
        svc[0].SFS_Invoice_Format__c = 'Detail';
        svc[0].SFS_PO_Number__c = '2';
        svc[0].SFS_Invoice_Type__c = 'Receivable';
        svc[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svc[0].SFS_Renewal_Escalator_Start_Date__c=system.today().addDays(-10);
        svc[0].SFS_Recurring_Adjustment__c=3;
        svc[0].SFS_Type__c='PlannedCare';
        insert svc;
        svc[0].SFS_Status__c = 'APPROVED';
        svc[0].SFS_Integration_Status__c='APPROVED';
        update svc;
        List<ContractLineItem> cliRec=SFS_TestDataFactory.createServiceAgreementLineItem(1,svc[0].Id,false);
        insert cliRec[0];
        
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
        insert lc; 
        
        CTS_IOT_Frame_Type__c ft = new CTS_IOT_Frame_Type__c();
        ft.Product_Code__c = cliRec[0].product2.productCode;
        ft.CTS_IOT_Type__c = 'Blower';
        insert ft;
        
        List<Asset> assets = SFS_TestDataFactory.createAssets(1, false);
        Assets[0].Product2Id =  proList[0].id;
        Assets[0].AccountId = accountList1[0].Id;
        assets[0].CTS_Frame_Type__c = ft.id;
        insert Assets[0];
        
        List<Entitlement> entitle = SFS_TestDataFactory.createEntitlement(1,accountList1[0].Id,null,svc[0].Id,cliRec[0].Id,false);
        entitle[0].SFS_X1st_PM_Month__c = 'October';
        entitle[0].ServiceContractId = svc[0].id;
        // entitle[0].ContractLineItemId = cliRec[0].id;
        entitle[0].AssetId__c = Assets[0].id;
        insert entitle;
        
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, accountList1[0].Id,null, null,  svc[0].Id, false);
        woList[0].SFS_Bill_To_Account__c = accountList1[1].Id;
        Wolist[0].SFS_Invoice_Submitted__c=true;
        WoList[0].ServiceContractId=svc[0].id;
        woList[0].SFS_PO_Number__c = '1';
        WoList[0].EntitlementId = entitle[0].id;
        WoList[0].AssetId = assets[0].id;
        woList[0].SFS_Requested_Payment_Terms__c='NET 30';
        insert WoList[0];
        
        WoList[0].Status='Open';
        
        woList[0].SFS_Integration_Status__c ='Error';
        Update WoList[0];
        
        test.startTest();        
        SFS_WorkOrderWithSvgOutboundHandler.Run(WoList);        
        String interfaceLabel = 'Work Order with Service Agreement!' + String.ValueOf(woList[0].Id);        
        SFS_WorkOrderWithSvgOutboundHandler.IntegrationResponse(res, interfaceLabel);
        res.setStatusCode(500);
        woList[0].SFS_External_Id__c = '12aees31';
        //svc[0].SFS_External_Id__c = '12aees3g1';
        update woList[0];
        //update svc[0];
        SFS_WorkOrderWithSvgOutboundHandler.Run(WoList);
        
        try{
            SFS_WorkOrderWithSvgOutboundHandler.IntegrationResponse(res, interfaceLabel);
        }catch(system.Exception e){
            
        } 
        test.stopTest();
    }
    @isTest
    public static void workOrderWithSvgOutboundHandlerNegativeTestMethod(){
        Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        
        res.setStatusCode(200);
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        
        //List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        Account billToAcc = SFS_TestDataFactory.getAccount();
        billToAcc.RecordTypeId = rtAcc;
        Update billToAcc;
        Account acc = SFS_TestDataFactory.getAccount();
        acc.Bill_To_Account__c=billToAcc.Id;
        acc.RecordTypeId = rtAcc;
        acc.IRIT_Payment_Terms__c='NET 30';
        acc.shippingcountry = 'CAD';
        acc.Type ='Prospect';
        Update acc;
        //List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,true);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='002';
        update proList[0];
        List<ServiceContract> svc = SFS_TestDataFactory.createServiceAgreement(1, acc.Id,false);
        svc[0].SFS_Bill_To_Account__c = billToAcc.Id;
        //svc[0].	SFS_Status__c = 'APPROVED';
        insert svc[0];
        svc[0].SFS_Status__c = 'APPROVED';
        svc[0].SFS_Integration_Status__c='APPROVED';       
        svc[0].SFS_External_Id__c=svc[0].id;
        update svc[0];
        List<ContractLineItem> cliRec = SFS_TestDataFactory.createServiceAgreementLineItem(1, svc[0].Id,false);
        cliRec[0].SFS_Product__c = proList[0].Id;
        insert cliRec[0];
        /*Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
insert lc; 

CTS_IOT_Frame_Type__c ft = new CTS_IOT_Frame_Type__c();
ft.Product_Code__c = prolist[0].ProductCode;
ft.CTS_IOT_Type__c = 'Blower';
insert ft;


List<Asset> assets = SFS_TestDataFactory.createAssets(1, false);
Assets[0].Product2Id =  prolist[0].id;
Assets[0].AccountId = acc.Id;
Assets[0].CTS_Frame_Type__c = ft.id;
insert Assets[0];*/
        
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acc.Id,null,null, null, false);
        
        woList[0].SFS_Bill_To_Account__c = billToAcc.Id;
        woList[0].SFS_PO_Number__c = '1';
        Wolist[0].SFS_Invoice_Submitted__c=true;
        WoList[0].ServiceContractId=svc[0].id;
        woList[0].SFS_Requested_Payment_Terms__c='NET 30';
        insert WoList[0];
        WoList[0].Status='Open';
        
        woList[0].SFS_Integration_Status__c ='Error';
        
        Update WoList[0];
        
        test.startTest();
        List<Id>WOIdList = new List<Id>();
        WoIdList.add(wolist[0].id);
        
        SFS_WorkOrderWithSvgOutboundHandler.Run(WoList);
        
        String interfaceLabel = 'Work Order with Service Agreement!' + String.ValueOf(woList[0].Id);
        
        
        SFS_WorkOrderWithSvgOutboundHandler.IntegrationResponse(res, interfaceLabel);
        res.setStatusCode(500);
        woList[0].SFS_External_Id__c = '12aees31';
        svc[0].SFS_External_Id__c = '12aees3g1';
        update woList[0];
        update svc[0];
        SFS_WorkOrderWithSvgOutboundHandler.Run(WoList);
        
        try{
            SFS_WorkOrderWithSvgOutboundHandler.IntegrationResponse(res, interfaceLabel);
        }catch(system.Exception e){
            
        } 
        test.stopTest();
    }
    
}