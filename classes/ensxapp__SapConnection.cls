/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SapConnection {
    global SapConnection() {

    }
    global static System.HttpRequest createRequest(Map<String,Object> urlParameters) {
        return null;
    }
    global static System.HttpResponse processRequest(System.HttpRequest req, Map<String,String> traceOptions) {
        return null;
    }
}
