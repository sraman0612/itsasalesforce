public class C_Embedded_Email_Extension {

    public List<EmailMessage> messages {get; private set;}

    private Case theCase;

    public C_Embedded_Email_Extension(ApexPages.StandardController controller) {
        this.theCase = (Case)controller.getRecord();
        this.messages = [Select e.ToAddress, e.TextBody, e.SystemModstamp, e.Subject, e.Status, e.ReplyToEmailMessageId, e.ParentId, e.MessageDate, e.LastModifiedDate, e.LastModifiedById, e.IsExternallyVisible, e.IsDeleted, e.Incoming, e.Id, e.HtmlBody, e.Headers, e.HasAttachment, e.FromName, e.FromAddress, e.CreatedDate, e.CreatedById, e.CcAddress, e.BccAddress, e.ActivityId From EmailMessage e WHERE ParentId =: theCase.Id ORDER BY MessageDate DESC];
    }

}