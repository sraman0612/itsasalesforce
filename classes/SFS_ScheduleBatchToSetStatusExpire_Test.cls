@isTest
public class SFS_ScheduleBatchToSetStatusExpire_Test {
@istest  public static void setStatusExpired() {
      
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        Id rtSCId = [Select Id, Name, SObjectType FROM RecordType where DeveloperName ='SFS_PackageCARE' AND SObjectType = 'ServiceContract'].Id;
        ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Service_Invoice'].id;
        ID NaAirProdRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Product2' AND DeveloperName = 'NA_Air'].id;
        
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].AccountSource='Web';
        accountList[0].RecordTypeId = rtAcc;
        insert accountList[0]; 
        
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].AccountSource='Web';
        insert accountList[1];
        
        ID testStandardPBId = Test.getStandardPricebookId();

        Product2 naProd = new Product2 (Name = 'TestBatchProduct', IsActive = TRUE, RecordTypeId = NaAirProdRecordTypeId, ProductCode = 'TEST');
        insert naProd;
            
        PriceBookEntry naPBE = new PriceBookEntry(PriceBook2Id = testStandardPBId, Product2Id = naProd.Id, UnitPrice = 1, IsActive = TRUE);
        insert naPBE;
        
        ServiceContract sc = new ServiceContract(AccountId=accountList[1].Id,SFS_Agreement_Value__c=78,SFS_Invoice_Format__c='Detail',
                                                SFS_Bill_To_Account__c=accountList[1].Id,StartDate=System.today().addDays(-5),EndDate=System.today().addDays(-2),
                                                SFS_Type__c='PackageCARE',recordTypeId=rtSCId,SFS_Invoice_Type__c='Prepaid',
                                                Name = '1234',SFS_PO_Number__c='123',SFS_PO_Expiration__c=system.today(),SFS_Portable_Required__c='Yes',
                                                SFS_Status__c='APPROVED',SFS_Invoice_Frequency__c='Annually',SFS_Special_Invoice_Note__c='',SFS_Requested_Payment_Terms__c = 'BANKCARD');
        insert sc;
        
         
        
        
         test.startTest();
         String cronTime= '0 0 22 1/1 * ? *';
         SFS_ScheduleBatchToSetStatusExpire sch = new SFS_ScheduleBatchToSetStatusExpire();
         String jobId = system.schedule('setStatusExiredTest', cronTime, sch); 
         test.stopTest();
         system.assertEquals(true,sc.SFS_Invoice_Frequency__c!=null);
           
    }
}