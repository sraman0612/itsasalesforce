global class C_GDIiConnInventoryTriggerSyncBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {

    global final String query;
    global set<id> AssIDlist;
    
    global C_GDIiConnInventoryTriggerSyncBatch(List<id> assid){
        
        if(assid.size()==0){
             query ='SELECT id, name,IMEI__c,Cumulocity_ID__c FROM Asset WHERE IMEI__c !=Null LIMIT 1';
           }
         else{
             //iConn_c8y_H_total__c,iConn_c8y_H_load__c,iConn_c8y_H_service__c,iConn_c8y_H_stal_1__c
             AssIDlist = new Set<id>(assid);
                query ='SELECT id, name,IMEI__c,Cumulocity_ID__c FROM Asset WHERE id IN: AssIDlist';
            }
            
    }
    global Database.QueryLocator start(Database.BatchableContext bacthC){
        
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext batchC,List<Asset>scope){
        
        list<id> AstID = new list<id>();
        for(Asset a : scope){
            AstID.add(a.id);
        }
        C_GDIiconnInventoryTrigger.iConnInventorySync(AstID);
    }
    
    global void finish(Database.BatchableContext batchC){
        
    }
}