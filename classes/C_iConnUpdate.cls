@RestResource(urlMapping='/iConn-IMEI/*')
global class C_iConnUpdate {
    private class Asset_s {
        Asset Asset_s;
    }
    
    @httpPost
    global static void doPost()
    {
        system.debug(RestContext.response + ' \n'+ RestContext.request.requestBody.toString());
        RestContext.response.addHeader('Content-Type', 'application/json');
        JSONGenerator gen = JSON.createGenerator(false);
        Boolean noSN = false;
        
        Asset_s container = (Asset_s)System.JSON.deserialize(RestContext.request.requestBody.toString(),
                                                                             Asset_s.class);
        Map<String, Object> a = (Map<String, Object>) JSON.deserializeUntyped(RestContext.request.requestBody.toString());
        system.debug(a);
        List<Object> results = (List<Object>) a.get('Asset');
        Map<String, Object> p = new Map<String, Object>();
        for (Object tmpKey : results) {
            p=(Map<String, Object>) tmpKey;
            System.debug('>>> ' + p);
        }
        Asset newRec = container.Asset_s;
        System.debug('New record: ' + container + ' **'+results + ' '+a);
        // Note: try with valid serial, if not catch and return error that serial doesn't exist
        
            // Find valid serial
            // Example serial model num: 'RO18200X-9CDL18NZ', its the serial + model num (separated by -)  
            List<Asset> relAsset;
            String record ='';
            try {

                if (!string.isBlank(string.valueOf(p.get('Serial_Number_Model_Number__c')))) {
                    relAsset = [SELECT Id, IMEI__c,iConn_Retro_Fit_Kit__c FROM Asset WHERE Serial_Number_Model_Number__c =:(string)p.get('Serial_Number_Model_Number__c')];
                    if(relAsset.size()>0)
                    {
                    relAsset[0].IMEI__c = (string)p.get('IMEI__c');
                    relAsset[0].iConn_Retro_Fit_Kit__c = (boolean)p.get('iConn_Retro_Fit_Kit__c');
                    update relAsset;
                    system.debug(relAsset+' ***'+ (string)p.get('IMEI__c'));
                    noSN = true;
                    }
                }
                else{
                    gen.writeStartObject();
                    gen.writeNumberField('statusCode', 404);
                    gen.writeStringField('error', 'Error updating Asset: No Serial Number available, check JSON message.');
                    gen.writeEndObject();
                    RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
                }
            } catch (Exception e) {
                
                gen.writeStartObject();
                gen.writeNumberField('statusCode', 404);
                gen.writeStringField('error', 'Error updating Asset: '+e.getMessage());
                gen.writeEndObject();
                RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
            } 
            
            
            
            if (noSN) {
                // Simply return for now
                record = 'Message successfully received, Asset record updated. Record Id: ' + relAsset[0].Id;
                gen.writeStartObject();
                gen.writeNumberField('statusCode', 200);
                gen.writeStringField('message', record);
                gen.writeEndObject();
                RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
                return;
            }
            
       
    }
        
}