public with sharing class CTRL_ShipToSearch implements I_SearchController
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_ShipToSearch.class);

    public SBO_SFCIPartner_Search.SEARCHPARAMS searchParams { get; set; }
    private boolean searchResultsLoaded = false;
    public I_SearchController searchController { get { return this; } }
    public SBO_SFCIPartner_Search.SFCIPartner_SC searchContext { get; set; }
    public Boolean hasRecords { get; private set; }
    public UTIL_PagingOptionsWrapper pagingOptionsWrapper { get; private set; }

    public String dropShipType {get;set;}
    public String newName {get;set;}
    public String newStreet {get;set;}
    public String newCity {get;set;}
    public String newRegion {get;set;}
    public String newPostalCode {get;set;}
    public String newPhoneNumber {get;set;}
    public String newCountry {get;set;}
    public String newStreetDS {get;set;}
    public String newCityDS {get;set;}
    public String newRegionDS {get;set;}
    public String newPostalCodeDS {get;set;}
    public String newCountryDS {get;set;}
    public String newStreetTDS {get;set;}
    public String newCityTDS {get;set;}
    public String newRegionTDS {get;set;}
    public String newPostalCodeTDS {get;set;}
    public String newCountryTDS {get;set;}

    public List<ShipToSearchResult> searchResults
    {
        get; private set;
    }

    public ShipToSearchResult selectedAddress { get; set; }
    private Boolean isInitialLoad { get; set; }
    private Boolean isSoldToPartyNumberSet = false;
    private Boolean isSalesOrganizationSet = false;
    private Boolean isDistributionChannelSet = false;
    private Boolean isDivisionSet = false;
    private Boolean isAutoSearchEnabledSet = false;
    private String soldToPartyNumberSave = null;
    private String salesOrganizationSave = null;
    private String distributionChannelSave = null;
    private String divisionSave = null;

    public String soldToPartyNumber
    {
        get;
        set
        {
            this.soldToPartyNumber = value;
            this.isSoldToPartyNumberSet = true;
            if (value != this.soldToPartyNumberSave)
            {
                this.isInitialLoad = true;
                this.soldToPartyNumberSave = value;
            }
            initialLoad();
        }
    }

    public String salesOrganization
    {
        get;
        set
        {
            this.salesOrganization = value;
            this.isSalesOrganizationSet = true;
            if (value != this.salesOrganizationSave)
            {
                this.isInitialLoad = true;
                this.salesOrganizationSave = value;
            }
            initialLoad();
        }
    }

    public String distributionChannel
    {
        get;
        set
        {
            this.distributionChannel = value;
            this.isDistributionChannelSet = true;
            if (value != this.distributionChannelSave)
            {
                this.isInitialLoad = true;
                this.distributionChannelSave = value;
            }
            initialLoad();
        }
    }

    public String division
    {
        get;
        set
        {
            this.division = value;
            this.isDivisionSet = true;
            if (value != this.divisionSave)
            {
                this.isInitialLoad = true;
                this.divisionSave = value;
            }
            initialLoad();
        }
    }

    public String previousScreen { get; set; }

    public Boolean isAutoSearchEnabled
    {
        get;
        set
        {
            this.isAutoSearchEnabled = value;
            this.isAutoSearchEnabledSet = true;
        }
    }

    public CB_ShipToSearchReceiver searchReceiver
    {
        get;
        set
        {
            this.searchReceiver = value;
        }
    }

    public List<SelectOption> CountryCodes
    {
        get
        {
            List<SelectOption> result = new List<SelectOption>();
            result.add(new SelectOption('US','	USA'));
            result.add(new SelectOption('CA','	Canada'));
            result.add(new SelectOption('MX','	Mexico'));
            result.add(new SelectOption('AR','	Argentina'));
            result.add(new SelectOption('BO','	Bolivia'));
            result.add(new SelectOption('BR','	Brazil'));
            result.add(new SelectOption('BS','	Bahamas'));
            result.add(new SelectOption('BZ','	Belize'));
            result.add(new SelectOption('CL','	Chile'));
            result.add(new SelectOption('CO','	Colombia'));
            result.add(new SelectOption('CR','	Costa Rica'));
            result.add(new SelectOption('DM','	Dominica'));
            result.add(new SelectOption('DO','	Dominican Rep.'));
            result.add(new SelectOption('EC','	Ecuador'));
            result.add(new SelectOption('GT','	Guatemala'));
            result.add(new SelectOption('GU','	Guam'));
            result.add(new SelectOption('GY','	Guyana'));
            result.add(new SelectOption('HN','	Honduras'));
            result.add(new SelectOption('HT','	Haiti'));
            result.add(new SelectOption('JM','	Jamaica'));
            result.add(new SelectOption('NI','	Nicaragua'));
            result.add(new SelectOption('PA','	Panama'));
            result.add(new SelectOption('PE','	Peru'));
            result.add(new SelectOption('PR','	Puerto Rico'));
            result.add(new SelectOption('PY','	Paraguay'));
            result.add(new SelectOption('SR','	Suriname'));
            result.add(new SelectOption('SV','	El Salvador'));
            result.add(new SelectOption('UY','	Uruguay'));
            result.add(new SelectOption('VE','	Venezuela'));

            return result;
        }
    }

    public class ShipToSearchResult
    {
        public Integer index { get; set; }
        public String shipToNumber { get; set; }
        public String salesOrg { get; set; }
        public String distChannel { get; set; }
        public String division { get; set; }
        public String name { get; set; }
        public String address { get; set; }
        public String country { get; set; }
        public String phoneNumber { get; set; }
        public String street { get; set; }
        public String city { get; set; }
        public String region { get; set; }
        public String postalCode { get; set; }
    }

    // ctor()
    public CTRL_ShipToSearch()
    {
        this.searchContext = new SBO_SFCIPartner_Search.SFCIPartner_SC();
        this.selectedAddress = new ShipToSearchResult();
        resetSearchContext();
        this.isInitialLoad = true;
        this.searchParams = new SBO_SFCIPartner_Search.SEARCHPARAMS();
        this.dropShipType = '';
        this.newName = '';
        this.newStreet = '';
        this.newCity = '';
        this.newRegion = '';
        this.newPostalCode = '';
        this.newPhoneNumber = '';
        this.newCountry = '';
        this.newStreetDS = '';
        this.newCityDS = '';
        this.newRegionDS = '';
        this.newPostalCodeDS = '';
        this.newCountryDS = '';
        this.newStreetTDS = '';
        this.newCityTDS = '';
        this.newRegionTDS = '';
        this.newPostalCodeTDS = '';
        this.newCountryTDS = '';
    }

    // initialLoad()
    //
    // Method to check if all assignTo attribute is set
    private void initialLoad()
    {
        if (this.isSoldToPartyNumberSet && this.isAutoSearchEnabledSet)
        {
            initialize();
        }
    }

    public void initialize()
    {
        if (this.isInitialLoad && this.isAutoSearchEnabled)
        {
            runSearch();
            searchResultsLoaded = true;
        }
        this.isInitialLoad = false;
    }

    // resetSearchResults()
    //
    // Action method to search shipTo address
    public void actionSearch()
    {
        resetSearchContext();
        runSearch();
    }

    private void resetSearchContext()
    {
        Integer pageSize = UTIL_ViewHelper.SearchDefaultPageSize;
        if (this.searchContext != null)
        {
            // page size is the only thing not reset
            pageSize = this.searchContext.pagingOptions.pageSize;
        }
        this.searchContext = new SBO_SFCIPartner_Search.SFCIPartner_SC();
        this.pagingOptionsWrapper = new UTIL_PagingOptionsWrapper(this.searchContext.pagingOptions);
        this.searchContext.pagingOptions.pageSize = pageSize;
        this.searchContext.pagingOptions.pageNumber = 1;
        this.hasRecords = false;
    }

    // searchShipTo()
    //
    // Action method to search shipTo address
    public void runSearch()
    {
        List<ShipToSearchResult> results = null;
        List<SBO_SFCIPartner_Search.SEARCHRESULT> partners = UTIL_Customer.getPartners(
            this.soldToPartyNumber, 
            UTIL_Customer.SHIP_TO_PARTNER_CODE,
            this.salesOrganization,
            this.distributionChannel,
            this.division,
            true);

        this.hasRecords = false;
        Integer pTot = partners.size();
        if (pTot > 0)
        {
            this.hasRecords = true;
            results = new List<ShipToSearchResult>();
            Integer index = 0;
            for (Integer pCnt = 0 ; pCnt < pTot ; pCnt++)
            {
                SBO_SFCIPartner_Search.SEARCHRESULT sr = partners[pCnt];
                results.add(addShipToResult(sr, index));
                index++;
            }
        }
        this.searchResults = results;
    }

    // addShipToResult(sr)
    //
    // Add shipTo to the searchResults
    private ShipToSearchResult addShipToResult(SBO_SFCIPartner_Search.SEARCHRESULT sr, Integer index)
    {
        ShipToSearchResult shipTo = new ShipToSearchResult();
        shipTo.index = index;
        //Replaces the leading 0s with an empty string
        shipTo.shipToNumber = sr.PartnerNumber.replaceFirst('^0+(?!$)','');
        shipTo.salesOrg = sr.SalesOrganization;
        shipTo.distChannel = sr.DistributionChannel;
        shipTo.division = sr.Division;
        shipTo.name = sr.PartnerName;
        shipTo.street = sr.Street;
        shipTo.city = sr.City;
        shipTo.region = sr.Region;
        shipTo.postalCode = sr.PostalCode;
        shipTo.address = buildShipToAddress(sr.Street, sr.City, sr.Region, sr.PostalCode);
        shipTo.country = sr.Country;

        return shipTo;
    }

    public void updateDropShipAddr() {
        Integer selectedIndex = Integer.valueOf(System.currentPageReference().getParameters().get('selectedIndex'));

        ShipToSearchResult shipTo = null;
        Integer srTot = this.searchResults.size();
        for (Integer srCnt = 0 ; srCnt < srTot ; srCnt++)
        {
            ShipToSearchResult result = this.searchResults[srCnt];
            if (result.index == selectedIndex)
            {
                dropShipType = System.currentPageReference().getParameters().get('shipToNumber');
                result.name = System.currentPageReference().getParameters().get('name');
                String newStreet = System.currentPageReference().getParameters().get('street');
                String newCity = System.currentPageReference().getParameters().get('city');
                String newRegion = System.currentPageReference().getParameters().get('region');
                String newPostalCode = System.currentPageReference().getParameters().get('postalcode');
                result.address = buildShipToAddress(newStreet, newCity, newRegion, newPostalCode);
                result.country = System.currentPageReference().getParameters().get('country');
                result.phoneNumber = System.currentPageReference().getParameters().get('phonenumber');
                result.street = newStreet;
                result.city = newCity;
                result.region = newRegion;
                result.postalCode = newPostalCode;
                break;
            }
        }
    }

    // buildShipToAddress
    //
    // Build the address
    private String buildShipToAddress(String street, String city, String region, String postalCode)
    {
        // May need to add Country here as well
        String address = String.isNotEmpty(street) ? street : '';
        address += '\n';
        address += String.isNotEmpty(city) ? city: '';
        address += ', ';
        address += String.isNotEmpty(region) ? region: '';
        address += ' ';
        address += String.isNotEmpty(postalCode) ? postalCode: '';
        return address;
    }

    // selectAddress()
    //
    // Action when selecting the address
    public void actionSelectedAddressToReceiver()
    {
        Integer selectedIndex = Integer.valueOf(System.currentPageReference().getParameters().get('selectedIndex'));

        Integer srTot = this.searchResults.size();
        for (Integer srCnt = 0 ; srCnt < srTot ; srCnt++)
        {
            ShipToSearchResult result = this.searchResults[srCnt];
            if (result.index == selectedIndex)
            {
                this.searchReceiver.onReceiveShipToSearch(result);
                break;
            }
        }
    }

    //goToCloneNewShipTo()
    //
    // Action to go to create new ShipTo page
    public PageReference goToCloneNewShipTo()
    {
        Integer selectedIndex = Integer.valueOf(System.currentPageReference().getParameters().get('selectedIndex'));
        ShipToSearchResult shipTo = null;

        Integer srTot = this.searchResults.size();
        for (Integer srCnt = 0 ; srCnt < srTot ; srCnt++)
        {
            ShipToSearchResult result = this.searchResults[srCnt];
            if (result.index == selectedIndex)
            {
                shipTo = result;
                break;
            }
        }

        UTIL_PageState.current.sapPreviousScreen = this.previousScreen;
        UTIL_PageState.current.sapAddPartnerTo = this.soldToPartyNumber;
        UTIL_PageState.current.sapShipToNum = shipTo.shipToNumber;
        UTIL_PageState.current.sapSalesArea = shipTo.salesOrg + ',' + shipTo.distChannel + ',' + shipTo.division;
        UTIL_PageState.current.sapPartnerType = 'SH';

        return UTIL_PageFlow.redirectTo(UTIL_PageFlow.VFP_CustomerCreateUpdate, UTIL_PageState.current);
    }
}