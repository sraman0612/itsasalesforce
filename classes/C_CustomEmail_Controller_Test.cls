@isTest
private class C_CustomEmail_Controller_Test {

    @testSetup
    static void setupData(){
    
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Contact cont1 = new Contact();
        cont1.firstName = 'Can';
        cont1.lastName = ' Pango';
        cont1.email = 'service-gdi@canpango.com';
        cont1.Other_Email__c = 'service-gdi2@canpango.com';
        cont1.Relationship__c = 'Customer';
        insert cont1;
        
        Contact cont2 = new Contact();
        cont2.firstName = 'Can';
        cont2.lastName = ' Pango';
        cont2.email = 'service-gdi3@canpango.com';
        cont2.Other_Email__c = 'service-gdi4@canpango.com';
        cont2.Relationship__c = 'Customer';
        insert cont2;
        
        Case cse1 = new Case();
        cse1.Account = acct;
        cse1.Contact = cont1;
        cse1.ContactId = cont1.Id;
        cse1.Type = 'CSE1';
        cse1.Subject = 'Test Case';
        
        // need to associate an org-wide email address to this case for testing
        OrgWideEmailAddress owea = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        System.debug(owea);
        cse1.Org_Wide_Email_Address_ID__c = ' ' + owea.Id;
        insert cse1;
        System.debug(cse1);
    
        Case c = new Case();
        c.Account = acct;
        c.Type = 'CSE2';
        c.Subject = 'REFW';
        c.Org_Wide_Email_Address_ID__c = ' ' + owea.Id;
        insert c;
        
        Email_Template_Admin__c eta = new Email_Template_Admin__c();
        eta.Name = 'Test Email_Template_Admin__c';
        insert eta;

        for(integer i = 0; i < 3; ++i){
            Attachment att = new Attachment();
            att.Name = 'Attachment Test #' + i.format();
            att.ParentId = cse1.Id;
            att.Body = Blob.valueOf('VGVzdFN0cmluZw==');
            att.ContentType = 'image/jpg';
            insert att;
        }

        EmailMessage em = new EmailMessage();
            em.Incoming = true;
            em.FromName = 'Canpango';
            em.ToAddress = 'salesforce@gardnerdenver.com';
        	em.BccAddress = 'service-gdi@canpango.com';
            em.TextBody = 'Body';
            em.Subject = 'Subject';
            em.MessageDate = System.today();
            em.ParentId = cse1.Id;
            em.HtmlBody = '<p><script>Body</script></p>';
            em.FromAddress = 'service-gdi@canpango.com';
        insert em;
        
        EmailMessage em1 = new EmailMessage();
            em1.Incoming = false;
            em1.FromName = 'Canpango';
            em1.ToAddress = 'salesforce@gardnerdenver.com';
        	em1.BccAddress = 'service-gdi@canpango.com';
            em1.TextBody = 'Body';
            em1.Subject = 'Subject Outgoing';
            em1.MessageDate = System.today();
            em1.ParentId = cse1.Id;
            em1.HtmlBody = '<p><script>Body</script></p>';
            em1.FromAddress = 'service-gdi@canpango.com';
        insert em1;
        
        EmailMessage em2 = new EmailMessage();
            em2.Incoming = false;
            em2.FromName = 'Canpango';
            em2.ToAddress = 'salesforce@gardnerdenver.com';
        	em2.BccAddress = 'service-gdi@canpango.com';
            em2.TextBody = '';
            em2.Subject = 'No Body';
            em2.MessageDate = System.today();
            em2.ParentId = cse1.Id;
            em2.HtmlBody = '<p><script>Body</script></p>';
            em2.FromAddress = 'service-gdi@canpango.com';
        insert em2;

        EmailMessage emREFW = new EmailMessage();
            emREFW.Incoming = true;
            emREFW.FromName = 'Canpango';
            emREFW.ToAddress = 'salesforce@gardnerdenver.com';
            emREFW.CcAddress = 'service-gdi@canpango.com';
            emREFW.BccAddress = 'service-gdi@canpango.com';
            emREFW.TextBody = 'Body';
            emREFW.Subject = 'RE: REFW';
            emREFW.MessageDate = System.today();
            emREFW.ParentId = c.Id;
            emREFW.FromAddress = 'service-gdi3@canpango.com';
        insert emREFW;
    }
    
    static testmethod void testC_CustomEmail_Controller(){
    
        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'Test Template';
        validEmailTemplate.DeveloperName = 'Test_Template';
        validEmailTemplate.TemplateType = 'Text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        
        insert validEmailTemplate;
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

        EmailMessage em = [Select e.Incoming,e.FromName, e.ToAddress, e.TextBody, e.Subject, e.MessageDate, e.ParentId, e.HtmlBody, e.FromAddress, e.CcAddress, e.BccAddress From EmailMessage e WHERE Subject = 'Subject'];
        Case c = [SELECT OwnerName__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c, Email_Signature_Special__c, Email_Signature_Preamble__c, Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, CaseNumber, Subject FROM Case WHERE Subject = 'Test Case'];
        Contact con = [SELECT Id, Email, Other_Email__c FROM Contact LIMIT 1];
        System.debug(c);
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Customer Care' Limit 1];

        Test.startTest();
            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);

            C_CustomEmail_Controller ceCont = new C_CustomEmail_Controller();
            ceCont.editorContent = 'test';
            ceCont.signatureTemplate();
            ceCont.addBCC();
            ceCont.addCC();
            ceCont.ccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.bccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.theCase = c;
            ceCont.theCase.ContactId = con.Id;
            ceCont.theCase.Contact = con;
            ceCont.correctToAddress = con.Email;
            ceCont.selectedTemplate = template.Id;
            List<C_CustomEmail_Controller.attachmentWrapper> attachments = ceCont.attachmentsWrapper;
            ceCont.attachmentsWrapper[0].include = true;

            C_CustomEmail_Controller.saveImage('test','VGVzdFN0cmluZw==');
            try{
                ceCont.send();
                ceCont.addNewAttachment();
            }
            catch(exception e){
                System.debug(e);
            }
            //ceCont.cancel();

            ceCont.freeFormBCC += '';
            ceCont.freeFormCC += '';
            ceCont.emailSubject += '';
            ceCont.templateSubject += '';

            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);
            System.currentPageReference().getParameters().put('type', 'reply');
            System.currentPageReference().getParameters().put('emailid', em.Id);

            ceCont = new C_CustomEmail_Controller();
            ceCont.selectedTemplate = template.Id;
            ceCont.addNewAttachment();
            ceCont.attachmentsWrapper[0].include = true;
            ceCont.sendContent();
        	
        Test.stopTest();
    }
    
    static testmethod void testC_CustomEmail_Controller1(){
    
        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'Test Template';
        validEmailTemplate.DeveloperName = 'Test_Template';
        validEmailTemplate.TemplateType = 'Text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        
        insert validEmailTemplate;
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

        EmailMessage em = [Select e.Incoming,e.FromName, e.ToAddress, e.TextBody, e.Subject, e.MessageDate, e.ParentId, e.HtmlBody, e.FromAddress, e.CcAddress, e.BccAddress From EmailMessage e WHERE Subject = 'Subject Outgoing'];
        Case c = [SELECT OwnerName__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c, Email_Signature_Special__c, Email_Signature_Preamble__c, Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, CaseNumber, Subject FROM Case WHERE Subject = 'Test Case'];
        Contact con = [SELECT Id, Email, Other_Email__c FROM Contact LIMIT 1];
        System.debug(c);
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Customer Care' Limit 1];

        Test.startTest();
            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);

            C_CustomEmail_Controller ceCont = new C_CustomEmail_Controller();
            ceCont.editorContent = 'test';
            ceCont.signatureTemplate();
            ceCont.addBCC();
            ceCont.addCC();
            ceCont.ccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.bccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.theCase = c;
            ceCont.theCase.ContactId = con.Id;
            ceCont.theCase.Contact = con;
            ceCont.correctToAddress = con.Email;
            ceCont.selectedTemplate = template.Id;
            List<C_CustomEmail_Controller.attachmentWrapper> attachments = ceCont.attachmentsWrapper;
            ceCont.attachmentsWrapper[0].include = true;

            C_CustomEmail_Controller.saveImage('test','VGVzdFN0cmluZw==');
            try{
                ceCont.send();
                ceCont.addNewAttachment();
            }
            catch(exception e){
                System.debug(e);
            }
            //ceCont.cancel();

            ceCont.freeFormBCC += '';
            ceCont.freeFormCC += '';
            ceCont.emailSubject += '';
            ceCont.templateSubject += '';

            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);
            System.currentPageReference().getParameters().put('type', 'reply');
            System.currentPageReference().getParameters().put('emailid', em.Id);

            ceCont = new C_CustomEmail_Controller();
            ceCont.selectedTemplate = template.Id;
            ceCont.addNewAttachment();
            ceCont.attachmentsWrapper[0].include = true;
            ceCont.sendContent();
        Test.stopTest();
    }

    static testmethod void testC_CustomEmail_Controller2(){

        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'Test Template';
        validEmailTemplate.DeveloperName = 'Test_Template';
        validEmailTemplate.TemplateType = 'Text';
        validEmailTemplate.FolderId = UserInfo.getUserId();

        insert validEmailTemplate;
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

        EmailMessage em = [Select e.Incoming,e.FromName, e.ToAddress, e.TextBody, e.Subject, e.MessageDate, e.ParentId, e.HtmlBody, e.FromAddress, e.CcAddress, e.BccAddress From EmailMessage e WHERE Subject = 'Subject'];
        Case c = [SELECT OwnerName__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c, CaseNumber, Email_Signature_Special__c, Email_Signature_Preamble__c, Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, Subject FROM Case WHERE Subject = 'Test Case'];
        Contact con = [SELECT Id, Email, Other_Email__c FROM Contact LIMIT 1];
        System.debug(c);
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Customer Care' Limit 1];

        Test.startTest();
            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);

            C_CustomEmail_Controller ceCont = new C_CustomEmail_Controller();
            ceCont.editorContent = 'test';
            ceCont.signatureTemplate();
            ceCont.addBCC();
            ceCont.addCC();
            ceCont.ccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.bccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.theCase = c;
            ceCont.theCase.ContactId = con.Id;
            ceCont.theCase.Contact = con;
            ceCont.correctToAddress = con.Email;
            ceCont.selectedTemplate = template.Id;
            List<C_CustomEmail_Controller.attachmentWrapper> attachments = ceCont.attachmentsWrapper;
            ceCont.attachmentsWrapper[0].include = true;

            C_CustomEmail_Controller.saveImage('test','VGVzdFN0cmluZw==');
            try{
                ceCont.send();
                ceCont.addNewAttachment();
            }
            catch(exception e){
                System.debug(e);
            }
            ceCont.cancel();

            ceCont.freeFormBCC += '';
            ceCont.freeFormCC += '';
            ceCont.emailSubject += '';
            ceCont.templateSubject += '';

            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);
            System.currentPageReference().getParameters().put('type', 'replyall');
            System.currentPageReference().getParameters().put('emailid', em.Id);

            ceCont = new C_CustomEmail_Controller();
            ceCont.selectedTemplate = template.Id;
            ceCont.addNewAttachment();
            ceCont.attachmentsWrapper[0].include = true;
        Test.stopTest();
    }
    
    static testmethod void testC_CustomEmail_Controller22(){

        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'Test Template';
        validEmailTemplate.DeveloperName = 'Test_Template';
        validEmailTemplate.TemplateType = 'Text';
        validEmailTemplate.FolderId = UserInfo.getUserId();

        insert validEmailTemplate;
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

        EmailMessage em = [Select e.Incoming,e.FromName, e.ToAddress, e.TextBody, e.Subject, e.MessageDate, e.ParentId, e.HtmlBody, e.FromAddress, e.CcAddress, e.BccAddress From EmailMessage e WHERE Subject = 'Subject Outgoing'];
        Case c = [SELECT OwnerName__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c, CaseNumber, Email_Signature_Special__c, Email_Signature_Preamble__c, Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, Subject FROM Case WHERE Subject = 'Test Case'];
        Contact con = [SELECT Id, Email, Other_Email__c FROM Contact LIMIT 1];
        System.debug(c);
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Customer Care' Limit 1];

        Test.startTest();
            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);

            C_CustomEmail_Controller ceCont = new C_CustomEmail_Controller();
            ceCont.editorContent = 'test';
            ceCont.signatureTemplate();
            ceCont.addBCC();
            ceCont.addCC();
            ceCont.ccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.bccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.theCase = c;
            ceCont.theCase.ContactId = con.Id;
            ceCont.theCase.Contact = con;
            ceCont.correctToAddress = con.Email;
            ceCont.selectedTemplate = template.Id;
            List<C_CustomEmail_Controller.attachmentWrapper> attachments = ceCont.attachmentsWrapper;
            ceCont.attachmentsWrapper[0].include = true;

            C_CustomEmail_Controller.saveImage('test','VGVzdFN0cmluZw==');
            try{
                ceCont.send();
                ceCont.addNewAttachment();
            }
            catch(exception e){
                System.debug(e);
            }
            ceCont.cancel();

            ceCont.freeFormBCC += '';
            ceCont.freeFormCC += '';
            ceCont.emailSubject += '';
            ceCont.templateSubject += '';

            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);
            System.currentPageReference().getParameters().put('type', 'replyall');
            System.currentPageReference().getParameters().put('emailid', em.Id);

            ceCont = new C_CustomEmail_Controller();
            ceCont.selectedTemplate = template.Id;
            ceCont.addNewAttachment();
            ceCont.attachmentsWrapper[0].include = true;
        Test.stopTest();
    }

    static testmethod void testC_CustomEmail_Controller3(){

        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'Test Template';
        validEmailTemplate.DeveloperName = 'Test_Template';
        validEmailTemplate.TemplateType = 'Text';
        validEmailTemplate.FolderId = UserInfo.getUserId();

        insert validEmailTemplate;
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

        EmailMessage em = [Select e.Incoming,e.FromName, e.ToAddress, e.TextBody, e.Subject, e.MessageDate, e.ParentId, e.HtmlBody, e.FromAddress, e.CcAddress, e.BccAddress From EmailMessage e WHERE Subject = 'Subject'];
        Case c = [SELECT OwnerName__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c,Email_Signature_Special__c, Email_Signature_Preamble__c, Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, CaseNumber, Subject FROM Case WHERE Subject = 'Test Case'];
        Contact con = [SELECT Id, Email, Other_Email__c FROM Contact LIMIT 1];
        System.debug(c);
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Customer Care' Limit 1];

        Test.startTest();
            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);

            C_CustomEmail_Controller ceCont = new C_CustomEmail_Controller();
            ceCont.editorContent = 'test';
            ceCont.signatureTemplate();
            ceCont.addBCC();
            ceCont.addCC();
            ceCont.ccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.bccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.theCase = c;
            ceCont.theCase.ContactId = con.Id;
            ceCont.theCase.Contact = con;
            ceCont.correctToAddress = con.Email;
            ceCont.selectedTemplate = template.Id;
            List<C_CustomEmail_Controller.attachmentWrapper> attachments = ceCont.attachmentsWrapper;
            ceCont.attachmentsWrapper[0].include = true;

            C_CustomEmail_Controller.saveImage('test','VGVzdFN0cmluZw==');
            try{
                ceCont.send();
                ceCont.addNewAttachment();
            }
            catch(exception e){
                System.debug(e);
            }
            //ceCont.cancel();

            ceCont.freeFormBCC += '';
            ceCont.freeFormCC += '';
            ceCont.emailSubject += '';
            ceCont.templateSubject += '';

            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);
            System.currentPageReference().getParameters().put('type', 'forward');
            System.currentPageReference().getParameters().put('emailid', em.Id);

            ceCont = new C_CustomEmail_Controller();
            ceCont.selectedTemplate = template.Id;
            ceCont.addNewAttachment();
            ceCont.attachmentsWrapper[0].include = true;
        Test.stopTest();
    }
    
    static testmethod void testC_CustomEmail_Controller33(){

        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'Test Template';
        validEmailTemplate.DeveloperName = 'Test_Template';
        validEmailTemplate.TemplateType = 'Text';
        validEmailTemplate.FolderId = UserInfo.getUserId();

        insert validEmailTemplate;
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

        EmailMessage em = [Select e.Incoming,e.FromName, e.ToAddress, e.TextBody, e.Subject, e.MessageDate, e.ParentId, e.HtmlBody, e.FromAddress, e.CcAddress, e.BccAddress From EmailMessage e WHERE Subject = 'No Body'];
        Case c = [SELECT OwnerName__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c,Email_Signature_Special__c, Email_Signature_Preamble__c, Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, CaseNumber, Subject FROM Case WHERE Subject = 'Test Case'];
        Contact con = [SELECT Id, Email, Other_Email__c FROM Contact LIMIT 1];
        System.debug(c);
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Customer Care' Limit 1];

        Test.startTest();
            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);

            C_CustomEmail_Controller ceCont = new C_CustomEmail_Controller();
            ceCont.editorContent = 'test';
            ceCont.signatureTemplate();
            ceCont.addBCC();
            ceCont.addCC();
            ceCont.ccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.bccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
            ceCont.theCase = c;
            ceCont.theCase.ContactId = con.Id;
            ceCont.theCase.Contact = con;
            ceCont.correctToAddress = con.Email;
            ceCont.selectedTemplate = template.Id;
            List<C_CustomEmail_Controller.attachmentWrapper> attachments = ceCont.attachmentsWrapper;
            ceCont.attachmentsWrapper[0].include = true;

            C_CustomEmail_Controller.saveImage('test','VGVzdFN0cmluZw==');
            try{
                ceCont.send();
                ceCont.addNewAttachment();
            }
            catch(exception e){
                System.debug(e);
            }
            //ceCont.cancel();

            ceCont.freeFormBCC += '';
            ceCont.freeFormCC += '';
            ceCont.emailSubject += '';
            ceCont.templateSubject += '';

            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);
            System.currentPageReference().getParameters().put('type', 'forward');
            System.currentPageReference().getParameters().put('emailid', em.Id);

            ceCont = new C_CustomEmail_Controller();
            ceCont.selectedTemplate = template.Id;
            ceCont.addNewAttachment();
            ceCont.attachmentsWrapper[0].include = true;
        Test.stopTest();
    }

    static testmethod void testC_CustomEmail_Controller4(){

        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'Test Template';
        validEmailTemplate.DeveloperName = 'Test_Template';
        validEmailTemplate.TemplateType = 'Text';
        validEmailTemplate.FolderId = UserInfo.getUserId();

        insert validEmailTemplate;
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

        EmailMessage em = [Select e.Incoming,e.FromName, e.ToAddress, e.TextBody, e.Subject, e.MessageDate, e.ParentId, e.HtmlBody, e.FromAddress, e.CcAddress, e.BccAddress From EmailMessage e WHERE Subject = 'Subject'];
        Case c = [SELECT OwnerName__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c,Email_Signature_Special__c, Email_Signature_Preamble__c, Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, CaseNumber, Subject FROM Case WHERE Subject = 'Test Case'];
        Contact con = [SELECT Id, Email, Other_Email__c FROM Contact LIMIT 1];
        System.debug(c);
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and Name='Customer Care' Limit 1];

        Test.startTest();
        Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
        System.currentPageReference().getParameters().put('CaseId', c.Id);

        C_CustomEmail_Controller ceCont = new C_CustomEmail_Controller();
        ceCont.editorContent = 'test';
        ceCont.signatureTemplate();
        ceCont.addBCC();
        ceCont.addCC();
        ceCont.ccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
        ceCont.bccWrappers.add(new C_CustomEmail_Controller.addressWrapper(con));
        ceCont.theCase = c;
        ceCont.theCase.ContactId = con.Id;
        ceCont.theCase.Contact = con;
        ceCont.correctToAddress = con.Email;
        ceCont.selectedTemplate = template.Id;
        List<C_CustomEmail_Controller.attachmentWrapper> attachments = ceCont.attachmentsWrapper;
        ceCont.attachmentsWrapper[0].include = true;

        C_CustomEmail_Controller.saveImage('test','VGVzdFN0cmluZw==');
        try{
            ceCont.send();
            ceCont.addNewAttachment();
        }
        catch(exception e){
            System.debug(e);
        }
        ceCont.cancel();

        ceCont.freeFormBCC += '';
        ceCont.freeFormCC += '';
        ceCont.emailSubject += '';
        ceCont.templateSubject += '';

        Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
        System.currentPageReference().getParameters().put('CaseId', c.Id);
        System.currentPageReference().getParameters().put('emailid', em.Id);

        ceCont = new C_CustomEmail_Controller();
        ceCont.selectedTemplate = template.Id;
        ceCont.addNewAttachment();
        ceCont.attachmentsWrapper[0].include = true;
        Test.stopTest();
    }
    
    static testmethod void testC_CustomEmail_Controller5(){

        Case c = [SELECT OwnerName__c, Brand_W_Out_Department__c, Owner_Title__c, Owner_Phone__c, Owner_Fax__c, Assigned_From_Address_Picklist__c, Email_Signature_Special__c, Email_Signature_Preamble__c,Email_Ref_Id__c, Id, ContactId, Contact.Email, Org_Wide_Email_Address_ID__c, ParentId, Parent.Full_Case_Description__c, CaseNumber, Subject  FROM Case WHERE Subject = 'REFW'];
        Attachment a = [Select Id FROM Attachment LIMIT 1];
        System.debug(c);
        
        Test.startTest();
            Test.setCurrentPageReference(new PageReference('C_CustomEmail'));
            System.currentPageReference().getParameters().put('CaseId', c.Id);

            C_CustomEmail_Controller ceCont = new C_CustomEmail_Controller();
            ceCont.theCase = c;
            ceCont.theCase.ContactId = null;
            ceCont.theCase.Contact = null;

            ceCont.attchID = a.Id;
            ceCont.createNewFiles_Stage();

            ceCont.CCWrappers = new List<C_CustomEmail_Controller.addressWrapper>();
            ceCont.BCCWrappers = new List<C_CustomEmail_Controller.addressWrapper>();
            ceCont.ToWrappers = new List<C_CustomEmail_Controller.addressWrapper>();
        Test.stopTest();
    }
}