global class ATG_ServiceDateCalculatorBatch implements Database.Batchable<sObject>, System.Schedulable{
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'SELECT Id, Last_Logged_Service_Visit__c, Avg_Yearly_Run_Hours_use__c, Start_up_Date__c, '+ 
            'iConn_c8y_H_total__c,iConn_c8y_H_service__c, iConn_Burn_Down_Service_Date__c, Service_Interval_Date__c,'+ 
            'Calculated_Next_Service_Date__c,Expected_Next_Service_Date__c, Hours__c, Reason_Service_Due__c, Average_Hours_Service_Date__c FROM Asset WHERE Expected_Next_Service_Date__c != null ' +
            'AND (Calculated_Next_Service_Date__c = NULL OR Calculated_Next_Service_Date__c <= TODAY)';
        return Database.getQueryLocator(query);
    } 
    global void execute(Database.BatchableContext bc, List<Asset> scope){
        Set<Id> serialNumberIds = new Set<Id>();
        //Collate the Ids for the serial numbers
        for(Asset serialNumber: scope){
            serialNumberIds.add(serialNumber.Id);
        }
        //Retrieve the related Service Contracts
        List<Service_Contract__c> serviceContracts = [
            SELECT Id, Serial_Number__c,Service_Interval__c,Contract_End_Date__c
            FROM Service_Contract__c
            WHERE Serial_Number__c IN :serialNumberIds
            AND Status__c = 'Active'
        ];
        
        //Map the Service contracts to the Serial Number
        Map<Id,Service_Contract__c> serviceContractMap = ATG_ServiceDateCalculator.mapServiceContractToSerialNumber(serviceContracts);
        //Update the service interval date field and the burndown field
        for(Asset serialNumber: scope){
            
            //Service Interval
            if(serviceContractMap.containsKey(serialNumber.Id)){
                //Service_Contract__c serviceContract = serviceContractMap.get(serialNumber.Id);
                //serialNumber.Service_Interval_Date__c = ATG_ServiceDateCalculator.getDistributorServiceIntervalDate(serialNumber, serviceContract);
            }
            
            //iConn or Average Yearly Run Hours
            if(serialNumber.iConn_c8y_H_total__c != null){
                serialNumber.iConn_Burn_Down_Service_Date__c = ATG_ServiceDateCalculator.getIConnBurnDownDate(serialNumber);
                
            }
            else{
                serialNumber.Average_Hours_Service_Date__c = ATG_ServiceDateCalculator.getAverageHoursBurnDownDate(serialNumber);
            }
            
            ATG_ServiceDateCalculator.ReasonDueTuple calculatedNextServiceDate = ATG_ServiceDateCalculator.calculateSoonestDate(serialNumber);
            
            
            if(calculatedNextServiceDate != null){
                serialNumber.Calculated_Next_Service_Date__c = calculatedNextServiceDate.dueDate;
                serialNumber.Reason_Service_Due__c = calculatedNextServiceDate.reasonDue;
            }
            
        }
        
        //update the Serial numbers with the soonest service date
        
        Database.SaveResult[] saveResultList = Database.update(scope,false);
        
        
    }
    global void execute(SchedulableContext SC){
        Database.executeBatch(new ATG_ServiceDateCalculatorBatch());
    }
    global void finish(Database.BatchableContext BC) {
        //Database.executeBatch(new ATG_DistributorSummaryEmailBatch(),100);
    }
}