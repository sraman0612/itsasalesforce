@isTest
global class SFS_ATPInterfaceOutboundIntegrationTest{ 
    @isTest
    public static void ATPInterfaceOutboundIntegrationTest()
    {
        test.setMock(HttpCalloutMock.class, new mockCallout());
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        List<Product2> pro = SFS_TestDataFactory.createProduct(1,false);    	
        pro[0].Part_Number__c = '23231772';
        pro[0].CurrencyIsoCode = 'USD';
        insert pro[0];

        Account billToAcc = SFS_TestDataFactory.getAccount();
        billToAcc.RecordTypeId = rtAcc;
        Update billToAcc;
        Account acc = SFS_TestDataFactory.getAccount();
        acc.Oracle_Site_Use_Id__c = '23895|GREENSBORO';
        acc.Oracle_Site_Use_Id__c = '23895|ELKHART';
        acc.Oracle_Number__c = '23895';
        acc.Bill_To_Account__c=billToAcc.Id;
        update acc;
        List<Division__c> div= SFS_TestDataFactory.createDivisions(1,false);
        div[0].SFS_Org_Code__c = 'DLC';
        insert div[0];
       
        Account acct1 = new Account();
        acct1.name = 'test account1';
        acct1.Currency__c='USD';
        insert acct1;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Bill_To_Account__c=billToAcc.Id;
        acct.Currency__c='USD';
        acct.type='Prospect';
        insert acct;
        
       
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1,acc.Id,loc[0].Id,div[0].Id,sc[0].Id,true);
        List<WorkType> wt = SFS_TestDataFactory.createWorkType(1,true);
        List<WorkOrderLineItem> woli = SFS_TestDataFactory.createWorkOrderLineItem(1,wo[0].Id,wt[0].Id,true);
        List<ProductRequest> pr = SFS_TestDataFactory.createProductRequest(1,wo[0].Id,woli[0].Id,true);
        List<ProductRequestLineItem> prli = SFS_TestDataFactory.createPRLI(2,pr[0].Id,pro[0].Id,false);
        //prli[0].SFS_Requested_Shipment_Date__c = Date.parse('2022-03-29T17:00:00.000+0000');
        prli[0].SFS_Requested_Shipment_Date__c = system.Today();
        prli[0].SFS_External_Id__c = 'PR-0025-PRLI-0001';
        prli[0].SFS_Source_Location__c = div[0].Id;
        prli[0].Product2Id =  pro[0].Id;
        prli[0].QuantityRequested = 10.00;
        prli[0].AccountId = acc.Id;
        insert prli[0];
        prli[1].SFS_Requested_Shipment_Date__c = system.Today();
        prli[1].SFS_External_Id__c = 'PR-0025-PRLI-0002';
        prli[1].SFS_Source_Location__c = div[0].Id;
        prli[1].Product2Id =  pro[0].Id;
        prli[1].QuantityRequested = 10.00;
        prli[1].AccountId = acc.Id;
        insert prli[1];
        List<List<Id>> idCol = new List<List<Id>>();
        List<Id> prliId = new List<Id>();
        prliId.add(prli[0].Id);
        idCol.add(prliId);
        try{
        SFS_ATPInterfaceOutboundIntegration.Run(idCol);
        
       //SFS_ATPInterfaceOutboundIntegration.parseXMLErrHandler(prli[0].id,'Test Message');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-01-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-02-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-03-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-04-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-05-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-06-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-07-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-08-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-01-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-01-2022');
        SFS_ATPInterfaceOutboundIntegration.ParsedDate('12-01-2022');
        }
        catch(exception e){
            
        }
        try{
        SFS_ATPInterfaceOutboundIntegration.parseXMLErrHandler(prli[1].id,'Test');
        SFS_ATPInterfaceOutboundIntegration.parseXMLError(null,prli[1].id);
        }
        catch(exception e){
            
        }
    }
   /* public static void ATPInterfaceOutboundIntegration_Test()
    {
        test.setMock(HttpCalloutMock.class, new mockCallout());
    }*/
    global class mockCallout implements HttpCalloutMock
    {
        global HttpResponse respond(HttpRequest request)
        {
            string body='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://www.w3.org/2005/08/addressing"> <env:Header> <wsa:Action>execute</wsa:Action> <wsa:MessageID>urn:06dbca19-9650-11ec-b011-02001708e866</wsa:MessageID> <wsa:ReplyTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> <wsa:ReferenceParameters> <instra:tracking.ecid xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">efdbe64f-8c4f-4d51-970a-9a23aca858b4-019cdae0</instra:tracking.ecid> <instra:tracking.FlowEventId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">65910440</instra:tracking.FlowEventId> <instra:tracking.FlowId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">10236104</instra:tracking.FlowId> <instra:tracking.CorrelationFlowId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">0000NwlWN346qIB_vXh8iX1Y5u^f00000k</instra:tracking.CorrelationFlowId> <instra:tracking.quiescing.SCAEntityId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">3760029</instra:tracking.quiescing.SCAEntityId> </wsa:ReferenceParameters> </wsa:ReplyTo> <wsa:FaultTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> </wsa:FaultTo> </env:Header> <env:Body> <ATPResponse xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns="http://xmlns.irco.com/ATPPriceOutResponseABCSImpl"> <ControlArea xmlns=""> <PartnerCode>CPQ</PartnerCode> <RequestType>QUERY</RequestType> <ExternalMessageID>CPQ TestArjun Test Opty 10012021_CTS-76421645803040608</ExternalMessageID> <Database>ISOADEV1</Database> <SourceSystem>EBS</SourceSystem> <Status>SUCCESS</Status> <Messsage>Request Processed Successfully</Messsage> <MessageID>D5DF380550575F90E0540021280BE0EF</MessageID> </ControlArea> <DataArea xmlns=""> <GItem> <Item> <ItemNumber>23231806</ItemNumber> <BestWHSE>DLC (DCL)</BestWHSE> <GAvailability> <Availability> <WHSE>DLC (DCL)</WHSE> <ResultCode>SUCCESS</ResultCode> <AvailableDate>25-FEB-2022</AvailableDate> </Availability> </GAvailability> </Item> </GItem> </DataArea> </ATPResponse> </env:Body> </env:Envelope>';
            HttpResponse res = new HttpResponse();
            res.setBody(body);
            res.setStatusCode(200);
            return res;
        } 
    }
}