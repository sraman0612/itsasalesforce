public class iConn_Measurement_Controller {

    @AuraEnabled
    public static Asset queryAsset(Id assetId){
        return [SELECT Id, Name, IMEI__c, iConn_c8y_H_total__c, iConn_c8y_H_load__c, iConn_Measurement_Last_Change__c, iConn_c8y_H_service__c, iConn_Opt_Out_Reason__c, iConn_Opt_Out__c, Hours__c,iConn_Retro_Fit_Kit__c FROM Asset WHERE Id =: assetId];
    
    }
}