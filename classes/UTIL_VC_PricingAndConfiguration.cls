global class UTIL_VC_PricingAndConfiguration
{
    @testVisible
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_VC_PricingAndConfiguration.class);

    public static String getProductIdForSAPMaterialNumber(string sapMaterialNumber)
    {
        String productId = null;
        Product2 prod = UTIL_SFProduct.getProductByMaterialNumber(sapMaterialNumber);
        if(prod != null)
        {
            productId = prod.Id;
        }
        return productId;
    }
    public static String getSAPMaterialNumberFromProductId(String productId)
    {
        String productMaterialNumber = null;
        if (String.isNotEmpty(productId)) {
            Product2 pdt = UTIL_SFProduct.getProductById(productId);
            productMaterialNumber = UTIL_SFProduct.getMaterialNumberFromProduct(pdt);
        }
        return productMaterialNumber;
    }

    public static String stripLeadingZeros(String s)
    {
        if (s.length()<=1)
            return s;

        return s.replaceFirst('^0+','');
    }

     public static ENSX_VCConfiguration getInitialConfigFromMaterialAndPricing(String material,ENSX_VCPricingConfiguration pricing)
    {
        SBO_EnosixVC_Detail sbo = new SBO_EnosixVC_Detail();
        SBO_EnosixVC_Detail.EnosixVC config = sbo.getDetail(material);
        ApplyPricingConfigurationToSBO(config, pricing);
        config = sbo.initialize(config);

        System.debug('init config=' + config);

        UTIL_PageMessages.addFrameworkMessages(config.getMessages());

        ENSX_VCConfiguration serializableCfg = ENSX_VCConfiguration.getConfigurationFromSBOModel(config);
        return serializableCfg;
    }

    /// Takes a material number, a pricing configuration and any custom kingspan settings.
    /// We use the pricing configuration for the moment since it wraps up in a nice little package all of the
    /// Sales Area information we need, and for kingspan the way they do their defaults is based
    /// in part upon their sales area information.
    public static DS_VCMaterialConfiguration getInitialConfigFromMaterialAndPricingAndCustomConfig(String material
        , ENSX_VCPricingConfiguration pricing)
    {
        logger.enter('getInitialConfigFromMaterialAndPricingAndCustomConfig', new Map<String, Object> {
            'material' => material,
            'pricing' => pricing
        });
        try
        {
            SBO_EnosixVC_Detail sbo = new SBO_EnosixVC_Detail();
            SBO_EnosixVC_Detail.EnosixVC config = new SBO_EnosixVC_Detail.EnosixVC();

            config.Material = material;
            // The pricing config is a carrier for most of the sales area information that is
            // needed by kingspan to properly return which VC options are available.
            config = applyPricingConfigurationToSBO(config, pricing);
            config = sbo.initialize(config);

            UTIL_PageMessages.addFrameworkMessages(config.getMessages());

            return new DS_VCMaterialConfiguration(config);
        }
        catch(Exception ex)
        {
            logger.log(LoggingLevel.ERROR, 'There was an exception thrown when attempting to getInitialConfigFromMaterialAndPricingAndCustomConfig', ex);
        }
        finally
        {
            logger.exit();
        }
        return null;
    }

    // Long term this needs a cleaner refactor. I'd like to remove some of these ENSX classes
    // They don't mesh with our current naming strategy and wind up being more wonky serialization
    // wrappers than actually useful utility classes. --MWK 5-14-2018
    @testVisible
    private static SBO_EnosixVC_Detail.EnosixVC applyPricingConfigurationToSBO(SBO_EnosixVC_Detail.EnosixVC sboConfig, ENSX_VCPricingConfiguration pricingConfiguration)
    {
        if (null !=  PricingConfiguration)
        {
            sboConfig.ObjectKey = pricingConfiguration.ObjectKey;
            sboConfig.ConfigDate = Date.today();
            // We're disabling all pricing calculation at this point. The "Pricing Configuration" is still
            // the object that is a carrier for all of the header info. --MWK 7-27-2018
            sboConfig.CalculatePrice = false;
            sboConfig.SalesDocumentType = pricingConfiguration.SalesDocumentType;
            sboConfig.SalesOrganization = pricingConfiguration.SalesOrganization;
            sboConfig.DistributionChannel = pricingConfiguration.DistributionChannel;
            sboConfig.Division = pricingConfiguration.Division;
            sboConfig.SoldToParty = pricingConfiguration.SoldToParty;
            sboConfig.Plant = pricingConfiguration.Plant;
            sboConfig.SalesDocumentCurrency = pricingConfiguration.SalesDocumentCurrency;
            sboConfig.OrderQuantity = pricingConfiguration.OrderQuantity;
            sboConfig.SalesDocumentType = pricingConfiguration.SalesDocumentType;
        }

        return sboConfig;
    }

    /// Initializes a VC configuration from an existing BOM
    public static DS_VCMaterialConfiguration getInitializedConfigSBOModelFromBOM(string material, ENSX_VCPricingConfiguration pricing, List<DS_VCCharacteristicValues> BOM)
    {
        SBO_EnosixVC_Detail sbo = new SBO_EnosixVC_Detail();
        SBO_EnosixVC_Detail.EnosixVC config = new SBO_EnosixVC_Detail.EnosixVC();

        config.Material = material;
        // The pricing config is a carrier for most of the sales area information that is
        // needed by kingspan to properly return which VC options are available.
        config = applyPricingConfigurationToSBO(config, pricing);

        Integer bomTot = BOM.size();
        for (Integer bomCnt = 0 ; bomCnt < bomTot ; bomCnt++)
        {
            DS_VCCharacteristicValues selectedValue = BOM[bomCnt];
            SBO_EnosixVC_Detail.SELECTEDVALUES val = selectedValue.getSBOASelectedValuesForModel();
            config.SELECTEDVALUES.add(val);
        }
        
        config = sbo.command('CMD_INITIALIZE_FROM_DATA', config);

        if(!config.isSuccess())
        {
            System.debug('Initialize failed');
            System.debug(config.getMessages());
        }
        
        UTIL_PageMessages.addFrameworkMessages(config.getMessages());

        return new DS_VCMaterialConfiguration(config);
    }
}