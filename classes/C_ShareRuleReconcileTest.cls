@isTest
public class C_ShareRuleReconcileTest {
    static list<string> uId = new List<string>();
    static list<string> cId = new List<string>();
    
        
    @TestSetup static void setup()
    {
        string profileid = [select id from profile where name='Partner Community User'].id;
        List<contact> cList = new List<contact>();
        
        Account ap1 = new account(name='test3');
        insert ap1;
        
        Account ap2 = new account(name='test4');
        insert ap2;
        
        Account a1 = new account(name='test1', ParentId = ap2.id,Floor_Plan_Account__c = ap1.id);
        insert a1;
        Account a = new account(name='test',ParentId = ap1.id, Floor_Plan_Account__c=a1.id);
        insert a;
        
        Account a3 = new account(name='test1', ParentId = ap2.id,Floor_Plan_Account__c=a1.id);
        insert a3;
        a3.ParentId = ap1.id;
        a3.Floor_Plan_Account__c = a.id;
        update a3;
        
        contact con = new contact(firstname='j', lastname='b', email='jbtest1@test.com', accountid =a.id);
        //insert con;
        clist.add(con);
        
        contact con1 = new contact(firstname='j2', lastname='b2', email='jbtest2@test.com', accountid =a1.id);
        //insert con1;
        clist.add(con1);
        
        contact con2 = new contact(firstname='j2', lastname='b2', email='jbtest3@test.com', accountid =a1.id);
        //insert con2;
        clist.add(con2);
        
        contact con3 = new contact(firstname='j2', lastname='b2', email='jbtest4@test.com', accountid =a1.id);
        //insert con3;
        clist.add(con3);
        
        contact con4 = new contact(firstname='j', lastname='b', email='jbtest5@test.com', accountid =a.id);
        //insert con4;
        clist.add(con4);
        
        contact con5 = new contact(firstname='j2', lastname='b2', email='jbtest7@test.com', accountid =a1.id);
        //insert con5;
        cList.add(con5);
        
        contact con6 = new contact(firstname='j', lastname='b', email='jbtest6@test.com', accountid =a.id);
        //insert con6;
        clist.add(con6);
        
        insert clist;
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        List<user> uaList = new List<user>();
        User ua1 = new User(
            ProfileId = [select id from profile where name='Partner Community User'].id,
            Firstname='First',
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con6.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Single Account'
        );
        User ua2 = new User(
            ProfileId = [select id from profile where name='Partner Community User'].id,
            Firstname='First2',
            LastName = 'last2',
            Email = 'puser0001@amamama.com',
            Username = 'puser0001@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con5.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Global Account'
        );
        uaList.add(ua1);
        uaList.add(ua2);
        insert uaList;
        User u = new User(
            ProfileId = profileid,
            Firstname='First',
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Single Account'
        );
        
        User u2 = new User(
            ProfileId = profileid,
            Firstname='First2',
            LastName = 'last2',
            Email = 'puser0001@amamama.com',
            Username = 'puser0001@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con1.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Global Account'
        );
        
        User u3 = new User(
            ProfileId = profileid,
            Firstname='First3',
            LastName = 'last3',
            Email = 'puser0002@amamama.com',
            Username = 'puser0002@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            ContactId = con2.Id,
            community_user_type__c='Single Account'
        );
        
        User u4 = new User(
            ProfileId = profileid,
            Firstname='First4',
            LastName = 'last4',
            Email = 'puser0003@amamama.com',
            Username = 'puser0030@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con3.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Global Account'
        );
        
        User u5 = new User(
            ProfileId = profileid,
            Firstname='First5',
            LastName = 'last5',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con4.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Global Account'
        );
        
        List<asset> aList = new List<asset>();
        List<Serial_Number_Contact__c> snList = new List<Serial_Number_Contact__c>();
        List<Service_History__c> shList = new List<Service_History__c>();
        Asset serial = new Asset();
        serial.SerialNumber = '1234567';
        serial.Name = 'test';
        serial.AccountId=a.id;
        serial.current_servicer__c = a1.id;
        //insert serial;
        aList.add(serial);
        Asset serial2 = new Asset();
        serial2.SerialNumber = '1234568';
        serial2.Name = 'test';
        serial2.AccountId=a1.id;
        serial2.current_servicer__c = a.id;
        //insert serial2;
        aList.add(serial2);
        Asset serial3 = new Asset();
        serial3.SerialNumber = '1234569';
        serial3.Name = 'test';
        serial3.AccountId=a1.id;
        serial3.current_servicer__c = a.id;
        //insert serial3;
        aList.add(serial3);
        insert aList;
        Serial_Number_Contact__c sc = new Serial_Number_Contact__c();
        sc.First_Name__c ='j';
        sc.Last_Name__c = 'b';
        sc.Relationship__c = 'End User';
        sc.Email_Address__c  = 'jbtest3@test.com';
        sc.Serial_Number__c  = serial2.id;
        //insert sc;
        
        Serial_Number_Contact__c sc1 = new Serial_Number_Contact__c();
        sc1.First_Name__c ='j';
        sc1.Last_Name__c = 't';
        sc1.Relationship__c = 'Distributor';
        sc1.Email_Address__c  = 'jbtest4@test.com';
        sc1.Serial_Number__c  = serial.id;

        //insert sc1;   
        snList.add(sc);
        snList.add(sc1);
        insert snList;
        test.starttest();
        serial.Current_Servicer__c = ap1.id;
        //update serial;
        serial2.Current_Servicer__c = ap1.id;
        serial3.Current_Servicer__c = a1.id;
        //update serial2;
        update aList;
        Service_History__c sh = new Service_History__c();
        sh.Serial_Number__c = serial.id;
        sh.Technician_Email__c ='jbtest1@test.com';
        sh.account__c = a.id;
        insert sh; 
        
        Service_History__c sh2 = new Service_History__c();
        sh2.Serial_Number__c = serial2.id;
        sh2.Technician_Email__c ='jbtest2@test.com';
        sh2.account__c = a1.id;
        //insert sh2; 
        List<user> uList = new List<user>(); 
        insert u;//uList.add(u);
        insert u2;//uList.add(u2);
        insert u3;//uList.add(u3);
        insert u4;//uList.add(u4);
        //insert u5;//uList.add(u5);
        //insert uList;
        
        System.RunAs(usr)
        {
            
            //Test.startTest();
            
            //myFunc2(u4.id);
            
            Test.stopTest();
        }
    }
    
    
   
    @isTest static void test1()
    {
        test.startTest();
        C_ShareRuleReconcile test1 = new C_ShareRuleReconcile();
        Id batchJobId = Database.executeBatch(test1);
        
        C_AssetShareReconcile testy = new C_AssetShareReconcile();
        batchJobId = Database.executeBatch(testy);
        
        C_createAssetShareOnContactBatch testy1 = new C_createAssetShareOnContactBatch(null,cId);
        batchJobId = Database.executeBatch(testy1,20);
        test.stopTest();
        //C_createAssetShareGlobalUsrBatch testy2 = new C_createAssetShareGlobalUsrBatch(null,uid);
        //batchJobId = Database.executeBatch(testy2,20);
    }
}