/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 28/01/2022
* @description: Test class for SFS_ProductRelationshipQueueable
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001    SIF-112     28/01/2022      Test class for SFS_ProductRelationshipQueueable
============================================================================================================*/
@isTest
public class SFS_ProductRelationshipQueueableTest {
    @testSetup static void setup() {
	
		List<Product2> productsList=SFS_TestDataFactory.createProduct(2,false);
        insert productsList;
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1,false);
        insert assetList;
        List<CAP_IR_Asset_Part_Required__c> partsRequiredList=SFS_TestDataFactory.createPartsRequired(1,assetList[0].Id,productsList[0].Id,false);
        insert partsRequiredList;
        
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        List<Account> accList=SFS_TestDataFactory.createAccounts(2, false);
        accList[0].IRIT_Customer_Number__c='2222';
        accList[1].RecordTypeId=rtAcc;
        //insert accList;
        Database.DMLOptions opts = new Database.DMLOptions();       
        opts.DuplicateRuleHeader.AllowSave = true;        
        Database.insert(accList, opts);
        
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].Type='Prospect';
        update accList;
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        insert svcAgreementList;
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,null,divisionsList[0].Id,svcAgreementList[0].Id,false);
        insert woList;
        List<CAP_IR_Charge__c> chargeList=SFS_TestDataFactory.createCharge(1,woList[0].Id,false);
        chargeList[0].SFS_Product__c=productsList[0].Id;
        insert chargeList;
        List<ProductRequest> requestList= SFS_TestDataFactory.createProductRequest(1,woList[0].Id,null,false);
        insert requestList;
        List<WorkType> workTypeRecord=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeRecord;
        List<ProductRequired> preqList= SFS_TestDataFactory.createProductRequired(1,woList[0].Id,productsList[0].Id,false);
        preqList[0].ParentRecordId=workTypeRecord[0].Id;
        insert preqList;
        List<ProductRequestLineItem> prliList= SFS_TestDataFactory.createPRLI(1,requestList[0].Id,productsList[0].Id,false);
        insert prliList;
	    }
        Static testmethod void testOneToOne(){
        List<Product2> prod1=[Select Id,Name,productCode from Product2];
        List<SFS_Product_Relationship__c> prList=SFS_TestDataFactory.createProductRelationships(1,prod1[0].Id,prod1[1].productCode,false);
        Test.startTest();
        insert prList[0];
        prList[0].SFS_Relationship_Type__c='';
        update prList[0];      
        prList[0].SFS_Relationship_Type__c='Supersession';
        update prList[0];
        Test.stopTest();
    }
        Static testmethod void testOneToMany(){
        List<Product2> prod1=[Select Id,Name,productCode from Product2];
        List<SFS_Product_Relationship__c> prList=SFS_TestDataFactory.createProductRelationships(2,prod1[0].Id,prod1[1].productCode,false);
        Test.startTest();
        insert prList[0];        
        Test.stopTest();
    }
}