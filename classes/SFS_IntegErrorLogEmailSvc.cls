/**
 * @author Armando Vingochea
 * Handle email log errors from CPI middleware for SFS project
 */
global class SFS_IntegErrorLogEmailSvc implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelop) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

        try {
            List<SFS_Integration_Log__c> logs = new List<SFS_Integration_Log__c>();
            List<List<Map<String, String>>> csvData = getData(email);

            // 'ISOAQA XXONT1451 SFS-NA Errors'
            String subject = email.subject.removeStartIgnoreCase('FW:').replaceAll('\\s+', ' ').trim();
            String[] subjectParts = subject.split(' ');
            String oracleEnv = '';
            String riceId;
            if (subjectParts.size() == 4) {
                oracleEnv = subjectParts[0];
                riceId = subjectParts[1];
            }

            for (Integer fileNum = 0; fileNum < csvData.size(); fileNum++) {
                for (Integer recNum = 0; recNum < csvData[fileNum].size(); recNum++) {
                    Map<String, String> rec = csvData[fileNum][recNum];

                    SFS_Integration_Log__c log = new SFS_Integration_Log__c();
                    log.SFS_Integration_Status__c = 'Fail';
                    log.SFS_Integration_Status_Code__c = '400';
                    log.GUID__c = rec.get('GUID');                                // '0208F20845A258FBE0640021280E8E4F'
                    log.Partner__c = rec.get('PARTNER');                          // 'SFS-NA'
                    log.Current_Phase__c = rec.get('CURRENT_PHASE');              // 'CLOSED'
                    log.Entity_Id__c = rec.get('ENTITY_ID');                      // '20454--'
                    log.Entity_Name__c = rec.get('ENTITY_NAME');                  // 'ORDER_NUMBER-LINE_NUMBER-ITEM'
                    log.Occurred_At__c = parseDate(rec.get('ERROR_DATE'));        // '03-AUG-23 01.16.53'
                    log.SFS_Label__c = rec.get('ERROR_MESSAGE').abbreviate(254);  // 'This Part is not Orderable'
                    log.SFS_Label_Full_Length__c = rec.get('ERROR_MESSAGE');      // 'This Part is not Orderable'
                    log.Error_Status__c = rec.get('ERROR_STATUS');                // 'NEW'
                    log.Debug_Step__c = rec.get('DEBUG_STEP');                    // 'CPI Failure'
                    log.RICE_ID__c = riceId;                                      // 'XXONT1451'
                    log.Interface_Destination__c = 'Oracle ' + oracleEnv;         // 'ISOAQA'
                    log.SFS_Integration_Response__c = JSON.serialize(rec);
                    logs.add(log);
                }
            }

            insert logs;
            result.success = true;
        } catch (Exception e) {
            System.debug(e);
            result.success = false;
            result.message = e.getMessage();
        }
        return result;
    }


    // 03-AUG-23 01.16.53 (It's in EST)
    private DateTime parseDate(String dateStr) {
        String[] parts = dateStr.split(' ');
        String[] dayParts = parts[0].split('-');
        String[] timeParts = parts[1].split('\\.');
        return CXUtils.dateTimeForTimezone(
            Integer.valueOf('20' + dayParts[2]),
            CXUtils.getMonthNum(dayParts[1]),
            Integer.valueOf(dayParts[0]),
            Integer.valueOf(timeParts[0]),
            Integer.valueOf(timeParts[1]),
            Integer.valueOf(timeParts[2]),
            'America/New_York'
        );
    }


    /**
     * Read csv attachments & parse
     */
    private List<List<Map<String, String>>> getData(Messaging.InboundEmail email) {
        List<List<Map<String, String>>> data = new List<List<Map<String, String>>>();
        if (email.binaryAttachments != null) {
            for (Messaging.InboundEmail.BinaryAttachment att : email.binaryAttachments) {
                if (att.fileName.containsIgnoreCase('.csv') || att.fileName.containsIgnoreCase('.txt')) {
                    List<Map<String, String>> parsed = CXUtils_CSVReader.parseToMapList(att.body.toString(), ',');
                    data.add(parsed);
                }
            }
        }
        return data;
    }
}