@isTest(SeeAllData=true)
public class SFS_InvLIFlowController_Test {
    
    @isTest
    public static void SFS_InvLIFlowControllerTestMethod(){
        List <Invoice__c> lstAccount = new List<Invoice__c>();
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        Account acct1 = new Account();
        acct1.name = 'test account1';
        acct1.IRIT_Customer_Number__c='123';
        acct1.Currency__c='USD';
        acct1.AccountSource='Web';
        acct1.RecordTypeId=accRecID;
        insert acct1;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.IRIT_Customer_Number__c='123';
        acct.Bill_To_Account__c=acct1.id;
        acct.Currency__c='USD';
        acct.AccountSource='Web';
        acct.Type='Prospect';
        insert acct;
        
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, true);
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct.Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,true);
        String WOinvoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('WO Invoice').getRecordTypeId();
        //get invoice
        String SAinvoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Agreement Invoice').getRecordTypeId();
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(2,false);
        inv[0].RecordTypeId=WOinvoiceRecordTyeId;
        inv[1].RecordTypeId=SAinvoiceRecordTyeId;
        inv[0].SFS_Work_Order__c = wo[0].Id;
        inv[1].SFS_Service_Agreement__c = sc[0].Id;
        inv[0].SFS_Billing_Period_Start_Date__c=System.today();
        inv[0].SFS_Billing_Period_End_Date__c=System.today().addYears(2);
        inv[1].SFS_Billing_Period_Start_Date__c=System.today();
        inv[1].SFS_Billing_Period_End_Date__c=System.today().addYears(2);
        insert inv;
        
        List<SFS_Invoice_Line_Item__c> inlilist = new List<SFS_Invoice_Line_Item__c>();
        SFS_Invoice_Line_Item__c inli = new SFS_Invoice_Line_Item__c(SFS_Work_Order__c = wo[0].Id,SFS_Invoice__c = inv[0].Id);
        inlilist.add(inli);
        insert inlilist;  
        Map<String, Invoice__c> mape = new Map<String, Invoice__c>([select id from Invoice__c limit 1]);
        
        
        PageReference pref = Page.SFS_InvLI_Ids;       
        Test.setCurrentPage(pref);
        
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(inlilist);
        stdSetController.setSelected(inlilist);
        SFS_InvLIFlowController ext = new SFS_InvLIFlowController(stdSetController);
        Test.startTest();      
        ext.getOID();
        ext.getmyID();
        ext.getmyOldID();
        ext.backMethod();
        Test.stopTest();       
    }    
}