/* Author: Srikanth P, Capgemin
* Date: 08/23/2023
* Description: Apex Trigger handler for Credit Limit Object
-------------------------------------------------------------------
*/
public class SFS_CreditLimitHandler {
    
    public static void updateRelatedAccount(List<SFS_Credit_Limit__c> credLimitList){
        
        Map<String,SFS_Credit_Limit__c> siteUseIdMap = new Map<String,SFS_Credit_Limit__c>();
        Map<String,Account> AccountMap = new Map<String,Account>();
        
        for(SFS_Credit_Limit__c cl : credLimitList){
            if(cl.SFS_Account__c==Null){
                siteUseIdMap.put(cl.SFS_Oracle_Site_Use_Id__c,cl);
            }
        }
        //10/10- Nivetha - Added Org Channel to filter only IR accounts
       List<Account> accList = [select Id,SFS_Oracle_Site_Use_Id__c from Account where SFS_Oracle_Site_Use_Id__c IN:siteUseIdMap.keySet() AND CG_Org_Channel__c='IR'];
       for(Account acc : accList){    
           if(siteUseIdMap.containskey(acc.SFS_Oracle_Site_Use_Id__c)){
                AccountMap.put(acc.SFS_Oracle_Site_Use_Id__c,acc);     
           }
        }
       for(SFS_Credit_Limit__c cl : credLimitList){
           if(AccountMap.containsKey(cl.SFS_Oracle_Site_Use_Id__c)){
               cl.SFS_Account__c=AccountMap.get(cl.SFS_Oracle_Site_Use_Id__c).Id;
           }
        }
               
    }
       
    public static void updateRelatedCreditLimit(List<SFS_Credit_Limit__c> credLimitList){
        Map<String,SFS_Credit_Limit__c> siteUseIdMap = new Map<String,SFS_Credit_Limit__c>();
        List<Account> AccountToUpdate = new List<Account>();
        for(SFS_Credit_Limit__c cl : credLimitList){
           // Datetime lastmodified = credLimitOldMap.get(cl.Id).lastmodifieddate;
            if(cl.SFS_Account__c!= NULL){
                siteUseIdMap.put(cl.SFS_Oracle_Site_Use_Id__c,cl);
            }
        }
        //10/10- Nivetha - Added Org Channel to filter only IR accounts
        List<Account> accList = [select Id,SFS_Account_Credit_Limit__c,SFS_Oracle_Site_Use_Id__c from Account where SFS_Oracle_Site_Use_Id__c IN:siteUseIdMap.keySet() AND CG_Org_Channel__c='IR'];
        
        for(Account acc: accList){
            if(siteUseIdMap.containsKey(acc.SFS_Oracle_Site_Use_Id__c) && acc.SFS_Account_Credit_Limit__c==Null){
                acc.SFS_Account_Credit_Limit__c=siteUseIdMap.get(acc.SFS_Oracle_Site_Use_Id__c).Id;
                AccountToUpdate.add(acc);
            }
        }
        
        if(!AccountToUpdate.isEmpty()){
            update AccountToUpdate;
        }
        
    }    

}