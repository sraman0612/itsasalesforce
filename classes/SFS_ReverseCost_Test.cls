/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 25/07/2022
* @description: Test class for SFS_ReverseCost Apex class 
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
========================================================================================================================*/
@isTest
public class SFS_ReverseCost_Test {
    @isTest
    public static void reverseCostTestMethod(){
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[1].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        accList[1].IRIT_Payment_Terms__c='BANKCARD';
        insert accList[1];
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].Type='Prospect';
        accList[0].ShippingPostalCode='28759';
        accList[0].ShippingCity ='Montreat2';
        insert accList[0];
        Contact con = SFS_TestDataFactory.getContact();
        
        RecordType rtSC = [Select Id, Name, SObjectType FROM RecordType where Name ='Rental' AND SObjectType = 'ServiceContract'];
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        svcAgreementList[0].SFS_Invoice_Frequency__c = 'Quarterly';
        svcAgreementList[0].SFS_Bill_To_Account__c=accList[1].Id;
        insert svcAgreementList;
        
        OperatingHours op= new OperatingHours(Name='Normal',TimeZone='America/Chicago');
        insert op;
        
        ServiceTerritory territory=new ServiceTerritory(Name='abc',OperatingHoursId=op.Id,
                                                        SFS_Service_Report_Name__c='test service report',SFS_Service_Report_Phone__c='12364778',
                                                        IsActive=true);
        insert territory;
        
        List<Product2> productsList=SFS_TestDataFactory.createProduct(2,false);
        insert productsList;
        Pricebook2 pb=SFS_TestDataFactory.getPricebook2();
        
        List<PricebookEntry> priceBookEntryList= SFS_TestDataFactory.createPricebookEntry(1,false);
        // priceBookEntryList[0].Pricebook2Id= pb.Id;
        insert priceBookEntryList;
        
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,null,divisionsList[0].Id,svcAgreementList[0].Id,false);
        woList[0].Status='Open';
        woList[0].SFS_Requested_Payment_Terms__c='BANKCARD';
        woList[0].SFS_Bill_to_Account__c=accList[1].Id;
        System.debug('Bill to Account -->'+accList[1].Id);
        woList[0].SFS_PO_Number__c='1234';
        insert woList;
        
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,workTypeList[0].Id,false);
        insert woliList;
        
        List<Expense> expenseList = SFS_TestDataFactory.createExpense(2,null,woliList[0].Id,false);
        expenseList[0].ExpenseType='Air Fare';
        insert expenseList;   
        expenseList[1].ExpenseType ='MISC';
        expenseList[1].SFS_Category__c ='EHS Certifications';
        update expenseList[1];
        
        List<CAP_IR_Labor__c> laborHourList= SFS_TestDataFactory.createLaborHours(1,null,woliList[0].Id,false);
        insert laborHourList;
        
        List<CAP_IR_Charge__c> chargeList = SFS_TestDataFactory.createCharge(3,woList[0].Id,false);
        for(CAP_IR_Charge__c c :chargeList){ 
            c.CAP_IR_Work_Order_Line_Item__c=woliList[0].Id; 
            c.CAP_IR_Date__c=system.Today();
        }   
        insert chargeList;
        
        chargeList[0].SFS_Charge_Type__c ='Time';
        update chargeList[0];
        chargeList[1].SFS_Charge_Type__c ='Expense';
        chargeList[1].SFS_Expense_Type__c = 'Air Fare';
        update chargeList[1];
        chargeList[2].SFS_Charge_Type__c ='Expense';
        chargeList[2].SFS_Expense_Type__c = 'MISC';
        chargeList[2].SFS_Expense__c = expenseList[1].id;
        update chargeList[2];
        
        List<Invoice__c> invList =SFS_TestDataFactory.createInvoice(1,true);
        SFS_Invoice_Line_Item__c inli = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=invList[0].Id,SFS_Work_Order__c=woList[0].Id,SFS_Work_Order_Line_Item__c=woliList[0].Id,SFS_Amount__c=123);
        insert inli;
        
        List<List<String>> lHIdsList = new List<List<String>>();
        List<String> Ids = new List<String>();
        Ids.add(laborHourList[0].Id);
        Ids.add(expenseList[0].Id);
        Ids.add(expenseList[1].Id);
        lHIdsList.add(Ids);
        SFS_ReverseCost.reverseCost(lHIdsList);
        
        system.assertEquals('MISC', chargeList[2].SFS_Expense_Type__c);
    }     
}