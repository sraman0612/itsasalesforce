public class SFS_LaborHourResubmit {
	@invocableMethod(label = 'Outbound Labor Resubmit' description = 'Send to CPI' Category = 'Labor Hour')
    public static void Resubmit(List<Id>WoliId){
        List<CAP_IR_Labor__c> lh = [SELECT SFS_Project_Number__c,SFS_Created_Date__c,CAP_IR_QUantity__c, SFS_External_Id__c,CAP_IR_Status__c, CAP_IR_Work_Order__c,Name,sfs_oracle_end_of_week_date__c,  CAP_IR_Description__c,SFS_Oracle_Expenditure_Type__c,  OwnerId, SFS_Reverse_Transaction_Reference__c, SFS_Transaction_Type__c, SFS_Negative_Transaction_Flag__c, CAP_IR_Labor_Type__c, SFS_Oracle_Action_Type__c, SFS_Oracle_Division_Name__c,sfs_oracle_start_date__c, sfs_batch_name__c,elapsed_time__c FROM CAP_IR_Labor__c WHERE Id =: woliId];
        String xmlString = '';
        Map<Double,String> laborHourXmlMap = new Map<Double,String>();
        //system.debug('@newWorkOrderId'+newWorkOrderId);        
        SFS_LaborHourOutboundHandler.ProcessData(lh, laborHourXmlMap, xmlString);
    }
}