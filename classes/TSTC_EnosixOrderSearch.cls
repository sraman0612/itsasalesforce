@isTest
private class TSTC_EnosixOrderSearch
{
    public class ThisException extends Exception {}
    
    public class MockSBO_EnosixSO_Search implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        Boolean isSuccess;
        
        public MockSBO_EnosixSO_Search(Boolean success) {
            isSuccess = success;
        }
        
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            SBO_EnosixSO_Search.EnosixSO_SC searchContext = (SBO_EnosixSO_Search.EnosixSO_SC)sc;
            SBO_EnosixSO_Search.EnosixSO_SR searchResult =
                new SBO_EnosixSO_Search.EnosixSO_SR();
            
            if (isSuccess)
            {
                SBO_EnosixSO_Search.SEARCHRESULT result = new SBO_EnosixSO_Search.SEARCHRESULT();
                
                result.CustomerPONumber = 'X';
                result.NetOrderValue = 0;
                result.OrderStatus = 'X';
                result.SalesDocument = 'X';
                result.SalesDocumentCurrency = 'X';
                result.SalesDocumentType = 'X';
                result.SalesDocumentTypeDescription = 'X';
                result.SalesDocumentVersionNumber = 'X';
                result.ShipToCity = 'X';
                result.ShipToName = 'X';
                result.ShipToParty = 'X';
                result.ShipToPurchaseOrderNumber = 'X';
                result.ShipToRegion = 'X';
                result.SoldToCity = 'X';
                result.SoldToName = 'X';
                result.SoldToParty = 'X';
                result.SoldToRegion = 'X';
                result.TaxAmount = 0;
                result.YourReference = 'X';
                
                searchResult.SearchResults.add(result);
            }
            
            searchResult.setSuccess(isSuccess);
            searchContext.baseResult = searchResult;
            
            return searchContext;
        }
    }
    
    @isTest
    public static void testSearchSAPSuccess() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Search.class, new MockSBO_EnosixSO_Search(true));
        
        CTRL_EnosixOrderSearch.searchSAP('X', 'open', 'X', 'X', System.today(), System.today(), 'CreateDate', 'asc', 100, 0);
    }
    
    @isTest
    public static void testSearchSAPFail() {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Search.class, new MockSBO_EnosixSO_Search(false));
        
        CTRL_EnosixOrderSearch.searchSAP('X', 'complete', 'X', 'X', System.today(), System.today(), 'X', 'X', 100, 0);
    }
    
    @isTest
    public static void testGetScreenDataSuccess() {
        User user1;
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = 'testerson' + System.now().millisecond() + '@test.com.noway',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        
        System.runAs ( portalAccountOwner1 ) {
	        Account parentAccount = new Account(
                Name = 'ParentAccount'
            );
            Database.insert(parentAccount);
            
            Account repAccount = new Account(
                Name = 'RepAccount',
                OwnerId = portalAccountOwner1.Id,
                ParentId = parentAccount.Id
            );
            Database.insert(repAccount);
            
            Account portalAccount = new Account(
                Name = 'PortalAccount',
                OwnerId = portalAccountOwner1.Id,
                Rep_Account2__c = repAccount.Id,
                ParentId = parentAccount.Id
            );
            Database.insert(portalAccount);
            
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'GlobalUser',
                AccountId = repAccount.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
            Database.insert(contact1);
            
            Profile portalProfile = [SELECT Id FROM Profile where Id =: CTRL_EnosixOrderSearch.GLOBAL_USER_PROFILE_ID Limit 1];
            user1 = new User(
                Username = System.now().millisecond() + 'test@test.com.beatsme',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'GlobalUser',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
        }
        
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Search.class, new MockSBO_EnosixSO_Search(true));
        
        System.runAs(user1) {
            CTRL_EnosixOrderSearch.getScreenData();
        }
    }
}