/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AuraUtilities {
    global AuraUtilities(System.Type t) {

    }
global class Response {
    global Object data {
        get;
    }
    global List<ensxsdk.MessageUtilities.Message> messages {
        get;
    }
    global Object pagingOptions {
        get;
    }
    global Response(System.Type t) {

    }
    global Response(Object data, Object pagingOptions, List<ensxsdk.MessageUtilities.Message> messages) {

    }
}
}
