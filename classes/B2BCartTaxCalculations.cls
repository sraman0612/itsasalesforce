/**
* @author           Amit Datta
* @description      To calculate tax that needs to be added to the cart items.
*
* Modification Log
* ------------------------------------------------------------------------------------------
*         Developer                   Date                Description
* ------------------------------------------------------------------------------------------
*         Amit Datta                  21/02/2024          Original Version
**/

global with sharing class B2BCartTaxCalculations implements sfdc_checkout.CartTaxCalculations {
    
    private static final String ADDRESS_SEPERATOR = ', ';
    private class ApplicationException extends Exception {}
    global sfdc_checkout.IntegrationStatus startCartProcessAsync(sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
        sfdc_checkout.IntegrationStatus integStatus = new sfdc_checkout.IntegrationStatus();
        try {
            
            Map<Id, CartItem> cartItemsMap = new Map<Id, CartItem>();
            B2BTaxCalculator.TaxAddressWrapper addressWrapper = getAddressWrapperByCartId(cartId);
            Decimal cartSalesTax = B2BTaxCalculator.getSalesTaxByAddress(addressWrapper);
            if(cartSalesTax == null) {
                System.debug ('Sales Tax not found');
                return integrationStatusFailedWithCartValidationOutputError(
                    integStatus,
                    Label.B2B_TaxCalc_Error,
                    jobInfo,
                    cartId
                );
            }
            System.debug ('cartSalesTax>>>'+cartSalesTax);
            for (CartItem cartItem : [SELECT Sku, TotalPrice, Type FROM CartItem WHERE CartId = :cartId WITH SECURITY_ENFORCED]) {
                cartItemsMap.put(cartItem.Id,cartItem);
            }
            deleteOldCartTax(cartItemsMap.keySet());
            
            CartTax[] cartTaxestoInsert = new CartTax[]{};
                for (CartItem cItem : cartItemsMap.values()) {
                    CartTax tax = new CartTax( 
                        Amount = cItem.TotalPrice * cartSalesTax,
                        CartItemId = cItem.Id,
                        Name = 'SalesTax',
                        TaxCalculationDate = Date.today(),
                        TaxRate = cartSalesTax,
                        TaxType = 'Estimated'
                    );
                    cartTaxestoInsert.add(tax);
                }
            insert(cartTaxestoInsert);
            integStatus.status = sfdc_checkout.IntegrationStatus.Status.SUCCESS;
        } catch(Exception e) {
            System.debug('exception:' + e.getTypeName() + '>>>' +e.getMessage() );
            return integrationStatusFailedWithCartValidationOutputError(
                integStatus,
                Label.B2B_TaxCalc_Error,
                jobInfo,
                cartId
            );
        }
        return integStatus;
    }
    
    
    private sfdc_checkout.IntegrationStatus integrationStatusFailedWithCartValidationOutputError(
        sfdc_checkout.IntegrationStatus integrationStatus, String errorMessage, sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
            integrationStatus.status = sfdc_checkout.IntegrationStatus.Status.FAILED;
            // For the error to be propagated to the user, we need to add a new CartValidationOutput record.
            // The following fields must be populated:
            // BackgroundOperationId: Foreign Key to the BackgroundOperation
            // CartId: Foreign key to the WebCart that this validation line is for
            // Level (required): One of the following - Info, Error, or Warning
            // Message (optional): Message displayed to the user (maximum 255 characters)
            // Name (required): The name of this CartValidationOutput record. For example CartId:BackgroundOperationId
            // RelatedEntityId (required): Foreign key to WebCart, CartItem, CartDeliveryGroup
            // Type (required): One of the following - SystemError, Inventory, Taxes, Pricing, Shipping, Entitlement, Other
            CartValidationOutput cartValidationError = new CartValidationOutput(
                BackgroundOperationId = jobInfo.jobId,
                CartId = cartId,
                Level = 'Error',
                Message = errorMessage?.left(255),
                Name = (String)cartId + ':' + jobInfo.jobId,
                RelatedEntityId = cartId,
                Type = 'Taxes'
            );
            insert(cartValidationError);
            return integrationStatus;
        }
    
    private B2BTaxCalculator.TaxAddressWrapper getAddressWrapperByCartId(Id cartId ) {
        B2BTaxCalculator.TaxAddressWrapper returnAddressWrapper = new B2BTaxCalculator.TaxAddressWrapper();
        Id cartDeliveryGroupId = [SELECT CartDeliveryGroupId FROM CartItem WHERE CartId = :cartId WITH SECURITY_ENFORCED][0].CartDeliveryGroupId;
        CartDeliveryGroup deliveryGroup = [SELECT DeliverToStreet,DeliverToCity,DeliverToState,DeliverToPostalCode, DeliverToCountry FROM CartDeliveryGroup WHERE Id = :cartDeliveryGroupId WITH SECURITY_ENFORCED][0];
        returnAddressWrapper.fullAddress = deliveryGroup.DeliverToStreet + ADDRESS_SEPERATOR + deliveryGroup.DeliverToCity + ADDRESS_SEPERATOR + deliveryGroup.DeliverToState + ADDRESS_SEPERATOR +deliveryGroup.DeliverToPostalCode;
        return returnAddressWrapper;
    }
    
    private static void deleteOldCartTax(Set<Id> cartItemIdSet) {
        List<CartTax> cartTaxList = [SELECT Id FROM CartTax WHERE CartItemId IN :cartItemIdSet WITH SECURITY_ENFORCED];
        if(!cartTaxList.isEmpty()) {
            delete cartTaxList;
        }
    }
}