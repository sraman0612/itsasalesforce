public with sharing class CTRL_ServiceTechnician {
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_ServiceTechnician.class);

    @AuraEnabled
    public static UTIL_Aura.Response getServiceTechnicians(String customerNumber, String salesOrg) {
        if (customerNumber == null) {
            return null;
        }
        
        if (String.isEmpty(salesOrg)) {
            salesOrg = 'GDMI';
        } else if (salesOrg.length() > 4) {
            salesOrg = salesOrg.substring(0,4);
        }
        
        RFC_Z_ENSX_TECHNICIAN_LIST rfc = new RFC_Z_ENSX_TECHNICIAN_LIST();

        rfc.PARAMS.I_KUNNR = customerNumber.leftPad(10, '0');
        rfc.PARAMS.I_VKORG = salesOrg;

        RFC_Z_ENSX_TECHNICIAN_LIST.RESULT result = rfc.execute();

        List<SERVICE_TECHNICIAN> retVal = new List<SERVICE_TECHNICIAN>();

        if (result.E_TECHNICIAN_LIST_List != null) {
            for (RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST technician : result.E_TECHNICIAN_LIST_List) {
                SERVICE_TECHNICIAN cur = new SERVICE_TECHNICIAN();
                retval.add(cur);
                cur.partnerNumber = technician.PARNR;
                cur.technicianName = technician.NAMEV + ' ' + technician.NAME1;
            }
        }

        return UTIL_Aura.createResponse(JSON.serialize(retVal));
    }

    public class SERVICE_TECHNICIAN {
        public String partnerNumber {get; set;}
        public String technicianName {get; set;}
    }
}