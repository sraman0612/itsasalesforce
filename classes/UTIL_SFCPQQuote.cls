public with sharing class UTIL_SFCPQQuote implements I_SFSObjectDoc
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_SFCPQQuote.class);

    private SObject loadedSObject;
    private string loadedId;
    private string loadedSapType;
    private string loadedSapDocNum;
    private Map<Id, SObject> loadedLineMap;

	public SObject getSObject(String id)
	{
        return getSObject(id, null, null);
    }

	public SObject getSObject(String sapType, String sapDocNum)
	{
        return getSObject(null, sapType, sapDocNum);
    }

	private SObject getSObject(String id, String sapType, String sapDocNum)
	{
		SObject result = null;

        Boolean isIdMatch = (loadedId == id || (String.isNotEmpty(Id) && String.isNotEmpty(loadedId) && (loadedId.startsWith(id) || id.startsWith(loadedId))));
        if (loadedSObject != null && (isIdMatch || (loadedSapType == sapType && loadedSapDocNum == sapDocNum)))
        {
            return loadedSObject;
        }
        loadedId = id;
        loadedSapType = sapType;
        loadedSapDocNum = sapDocNum;
        loadedLineMap = null;

        string errorString = '';
        if (String.isNotEmpty(id) || (String.isNotEmpty(sapType) && String.isNotEmpty(sapDocNum)))
        {
            try
            {
                string selectCmd = 'SELECT Id, SBQQ__Opportunity2__c, SBQQ__PriceBook__c, SBQQ__Account__c, FLD_SAP_Quote_Number__c, SAP_Configuration__c, FLD_SAP_Order_Type__c, FLD_enosixPricingSimulationEnabled__c' +
                            ' FROM SBQQ__Quote__c';

                if (String.isNotEmpty(id))
                {
                    selectCmd += ' WHERE Id = \'' + id + '\'';
                    errorString = ' for the provided Id ' + id;
                }
                else
                {
                    if (sapType == 'Quote')
                    {
                        selectCmd += ' WHERE FLD_SAP_Quote_Number__c = \'' + sapDocNum + '\'';
                        errorString = ' for the provided SAP Quote Number ' + sapDocNum;
                    }
                    else
                    {
                        selectCmd = null;
                    }
                }
                if (selectCmd != null)
                {
                    selectCmd += ' Limit 1';

                    List<SObject> resultList = Database.query(selectCmd);

                    if (resultList.size() > 0)
                    {
                        result = resultList[0];
                    }
                    loadedId = result.Id;
                }
            }
            catch (Exception e)
            {
                System.debug(e);
            }
        }

        if (result == null && String.isNotEmpty(id))
        {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING,
                'There was an issue retrieving a Quote record' + errorString));
        }

        loadedSObject = result;
		return result;
	}

	public Map<Id, SObject> getSObjectLineItems(String id)
	{
        Boolean isIdMatch = (loadedId == id || (String.isNotEmpty(Id) && String.isNotEmpty(loadedId) && (loadedId.startsWith(id) || id.startsWith(loadedId))));
        if (loadedLineMap != null && isIdMatch)
        {
            return loadedLineMap;
        }

        loadedId = id;

		Map<Id, SObject> lineMap = new Map<Id, SObject>();

		try
		{
            string selectCmd = 'SELECT Id, Name, Concession_Request_Name__c, SBQQ__Quote__c, SBQQ__PricebookEntryId__c,' +
                        ' SBQQ__AdditionalDiscount__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__NetPrice__c,' +
                        ' SBQQ__Description__c, SAP_Configuration__c, SBQQ__ProductCode__c, SBQQ__Product__c, ZCON_Percentage__c' +
                        ' FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = \'' + id + '\' AND Use_Internal_Inventory__c = false' +
                        ' order by SBQQ__Number__c asc';
            List<SObject> lineList = Database.query(selectCmd);

            Integer lineTot = lineList.size();
            for (Integer lineCnt = 0 ; lineCnt < lineTot ; lineCnt++)
			{
                SObject lineItem = lineList[lineCnt];
				lineMap.put(lineItem.Id, lineItem);
			}

		}
		catch (Exception e)
		{
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'There was an issue retrieving the Quote line items'));
		}
        loadedLineMap = lineMap;
        return lineMap;
	}

	public String getAccountId(SObject sfSObject)
	{
        return sfSObject == null || sfSObject.getSObjectType().getDescribe().getName() != 'SBQQ__Quote__c' ? null : ((SBQQ__Quote__c) sfSObject).SBQQ__Account__c;
	}

	public String getName(SObject sfSObject)
	{
        return '';
	}

    public String getQuoteNumber(SObject sfSObject)
    {
        return sfSObject == null || sfSObject.getSObjectType().getDescribe().getName() != 'SBQQ__Quote__c' ? '' : ((SBQQ__Quote__c) sfSObject).FLD_SAP_Quote_Number__c;
    }

    public String getOrderNumber(SObject sfSObject)
    {
        return '';
    }

    public Opportunity getOpportunity(SObject sfSObject)
    {
        UTIL_SFOpportunity sfOpportunity = new UTIL_SFOpportunity();
        return sfSObject == null ? null : (Opportunity) sfOpportunity.getSObject(((SBQQ__Quote__c) sfSObject).SBQQ__Opportunity2__c);
    }

    public Id getPriceBookId(SObject sfSObject)
    {
        return sfSObject == null || sfSObject.getSObjectType().getDescribe().getName() != 'SBQQ__Quote__c' ? null : ((SBQQ__Quote__c) sfSObject).SBQQ__PriceBook__c;
    }

    public Id getProductId(SObject sfsObjectLine)
    {
        return sfsObjectLine == null || sfsObjectLine.getSObjectType().getDescribe().getName() != 'SBQQ__QuoteLine__c' ? null : ((SBQQ__QuoteLine__c) sfsObjectLine).SBQQ__Product__c;
    }

    private Map<Id, String> productSAPMaterialNumberList;

    public String getMaterial(SObject sfSObject, SObject sfsObjectLine)
    {
        return sfsObjectLine == null || sfsObjectLine.getSObjectType().getDescribe().getName() != 'SBQQ__QuoteLine__c' ? null : ((SBQQ__QuoteLine__c) sfsObjectLine).SBQQ__ProductCode__c;
    }

    public String getItemNumber(SObject sfsObjectLine)
    {
        return '';
    }

    public void initializeQuoteFromSfSObject(
        String calledFrom,
        SObject sfSObject,
        SBO_EnosixQuote_Detail.EnosixQuote quoteDetail,
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap,
        Map<String, UTIL_Quote.QuoteLineValue> quoteLineValueMap,
        Integer itemIncrement)
    {
		SBQQ__Quote__c cpqQuote = (SBQQ__Quote__c) sfSObject;
        List<SObject> solList = getSObjectLineItems(sfsObject.Id).values();
        Integer solTot = solList.size();
        for (Integer solCnt = 0 ; solCnt < solTot ; solCnt++)
        {
            SObject sfsObjectLine = solList[solCnt];
            SBQQ__QuoteLine__c quoteLine = (SBQQ__QuoteLine__c) sfsObjectLine;
            string materialNumber = getMaterial(sfSObject, sfsObjectLine);
            SBO_EnosixQuote_Detail.ITEMS quoteItem = translateLineItemToQuoteItem(quoteLine, materialNumber);
            if (quoteItem != null)
            {
                UTIL_Quote.addItemToQuote(quoteDetail, quoteItem, itemIncrement);
                addItemsConfig(quoteDetail, quoteItem, quoteLine);
                UTIL_Quote.QuoteLineValue quoteLineValue = new UTIL_Quote.QuoteLineValue();
                addItemsConditions(quoteDetail, quoteItem, quoteLine, quoteLineValue);
                quoteLineValueMap.put(quoteItem.ItemNumber.replaceFirst('^0+(?!$)', ''), quoteLineValue);
                sfSObjectLineIdMap.put(quoteItem.ItemNumber.replaceFirst('^0+(?!$)', ''), new UTIL_SFSObjectDoc.SfSObjectItem(sfsObjectLine.Id));
            }
        }
		SBO_EnosixQuote_Detail.PARTNERS soldToPartner = UTIL_Quote.getPartnerFromQuote(quoteDetail, UTIL_Customer.SOLD_TO_PARTNER_CODE, true);
		soldToPartner.CustomerNumber = (String) getSAPConfigField(cpqQuote.SAP_Configuration__c, 'soldToParty');
        quoteDetail.SALES.SalesOrganization = (String) getSAPConfigField(cpqQuote.SAP_Configuration__c, 'salesOrg');
        quoteDetail.SALES.DistributionChannel = (String) getSAPConfigField(cpqQuote.SAP_Configuration__c, 'distChannel');
        quoteDetail.SALES.Division = (String) getSAPConfigField(cpqQuote.SAP_Configuration__c, 'division');
    }

    public static Object getSAPConfigField(String sapConfigString, String fieldId)
    {
		if (sapConfigString == null) return null;
        Map<String, Object> sapConfigMap = (Map<String, Object>)JSON.deserializeUntyped(sapConfigString);
        return sapConfigMap.get(fieldId);
    }

    @testVisible
    private SBO_EnosixQuote_Detail.ITEMS translateLineItemToQuoteItem(SBQQ__QuoteLine__c quoteLine, string materialNumber)
    {
        SBO_EnosixQuote_Detail.ITEMS item = null;
        if (String.isNotBlank(materialNumber))
        {
            item = new SBO_EnosixQuote_Detail.ITEMS();
            item.Material = materialNumber;
            item.ItemDescription = String.isEmpty(quoteLine.SBQQ__Description__c) ? '' : quoteLine.SBQQ__Description__c.replace('</div>','\n').replaceAll('<[^>]+>',' ').unescapeHtml4();
            //item.OrderQuantity = (Decimal) getSAPConfigField(quoteLine.SAP_Configuration__c, 'OrderQuantity');
            item.OrderQuantity = quoteLine.SBQQ__Quantity__c;
            item.NetItemPrice = quoteLine.SBQQ__ListPrice__c;
			item.Plant = (String) getSAPConfigField(quoteLine.SAP_Configuration__c, 'plant');
        }
        return item;
    }

    @testVisible
    private void addItemsConfig(SBO_EnosixQuote_Detail.EnosixQuote quoteDetail, SBO_EnosixQuote_Detail.ITEMS quoteItem, SBQQ__QuoteLine__c quoteLine)
    {
        List<Object> SAPList = (List<Object>) getSAPConfigField(quoteLine.SAP_Configuration__c, 'selectedCharacteristics');
        if (SAPList != null)
        {
            Integer cTot = SAPList.size();
            for (Integer cCnt = 0 ; cCnt < cTot ; cCnt++)
            {
                Object sapObj = SAPList[cCnt];
                Map<String, Object> sapMap = (Map<String, Object>) sapObj;
                SBO_EnosixQuote_Detail.ITEMS_CONFIG itemConfig = new SBO_EnosixQuote_Detail.ITEMS_CONFIG();
                itemConfig.ItemNumber = quoteItem.ItemNumber;
                itemConfig.CharacteristicID = (String) sapMap.get('CharacteristicID');
                itemConfig.CharacteristicName = (String) sapMap.get('CharacteristicName');
                itemConfig.CharacteristicValue = (String) sapMap.get('CharacteristicValue');
                quoteDetail.ITEMS_CONFIG.add(itemConfig);
            }
        }
    }

    @testVisible
    private void addItemsConditions(SBO_EnosixQuote_Detail.EnosixQuote quoteDetail, SBO_EnosixQuote_Detail.ITEMS quoteItem, SBQQ__QuoteLine__c quoteLine,
        UTIL_Quote.QuoteLineValue quoteLineValue)
    {
        quoteLineValue.ListUnitPrice = quoteLine.SBQQ__ListPrice__c;
        if (quoteLine.SBQQ__AdditionalDiscount__c != null && quoteLine.SBQQ__AdditionalDiscount__c != 0)
        {
        	quoteLineValue.ManualPrice = quoteLine.SBQQ__NetPrice__c;
        }
    }

    public void finalizeQuoteAndUpdateSfsObject(
        String calledFrom,
        SObject sfSObject,
        SBO_EnosixQuote_Detail.EnosixQuote quoteDetail,
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap,
        Id pricebookId,
        List<SBO_EnosixQuote_Detail.ITEMS> quoteItems,
        Map<string, Id> materialToProductIdMap,
        Map<Id, PricebookEntry> productToPricebookEntryMap)
    {
        // Initial save of quote to get Id if new from Quick Quote
        SBQQ__Quote__c quote = (SBQQ__Quote__c) sfSObject;
        quote.FLD_SAP_Quote_Number__c = quoteDetail.SalesDocument;
        upsert quote;

        // Add quote lines
        if (UTIL_Quote.isAddMaterial || UTIL_Quote.isEditMaterial || UTIL_Quote.isCloneMaterial || UTIL_Quote.isMoveMaterial)
        {
            upsertLineItemsFromQuoteItems(quote, quoteItems, materialToProductIdMap, productToPricebookEntryMap, sfSObjectLineIdMap);
        }
    }

    // upsertLineItemsFromQuoteItems()
    //
    // Map the given quote items to new quote lines and insert them into the db
    @testVisible
    private void upsertLineItemsFromQuoteItems(
        SBQQ__Quote__c quote,
        List<SBO_EnosixQuote_Detail.ITEMS> quoteItems,
        Map<string, Id> materialToProductIdMap,
        Map<Id, PricebookEntry> productToPricebookEntryMap,
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap)
    {
        // List<SBQQ__QuoteLine__c> lineItems = new List<SBQQ__QuoteLine__c>();

        // for (SBO_EnosixQuote_Detail.ITEMS item : quoteItems)
        // {
        //     Id productId = materialToProductIdMap.get(item.Material);
        //     PricebookEntry price = productToPricebookEntryMap.get(productId);

        //     SBQQ__QuoteLine__c qli = new SBQQ__QuoteLine__c();

        //     if (sfSObjectLineIdMap.containsKey(item.ItemNumber.replaceFirst('^0+(?!$)', ''))
        //         && !sfSObjectLineIdMap.get(item.ItemNumber.replaceFirst('^0+(?!$)', '')).isDeleted)
        //     {
        //         qli.id = sfSObjectLineIdMap.get(item.ItemNumber.replaceFirst('^0+(?!$)', '')).id;
        //         System.debug('Updating Quote Line Item');
        //     }
        //     else
        //     {
        //         qli.SBQQ__Quote__c = quote.Id;
        //         qli.SBQQ__PricebookEntryId__c = price.Id;
        //         System.debug('Adding Quote Line Item');
        //     }
        //     qli.SBQQ__Description__c = item.ItemDescription;
        //     qli.SBQQ__Quantity__c = item.OrderQuantity;
        //     qli.SBQQ__NetPrice__c = item.NetItemPrice;
        //     qli.FLD_SAP_Item_Number__c = item.ItemNumber;

        //     System.debug(qli);

        //     lineItems.add(qli);
        // }

        // upsert lineItems;
    }

    public void initializeOrderFromSfSObject(
        String calledFrom,
        SObject sfSObject,
        SBO_EnosixSO_Detail.EnosixSO orderDetail,
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap,
        Map<String, UTIL_Order.OrderLineValue> orderLineValueMap,
        Integer itemIncrement)
    {
        orderDetail.CustomerPurchaseOrderNumber = getName(sfSObject);

		SBQQ__Quote__c cpqQuote = (SBQQ__Quote__c) sfSObject;
        List<SObject> solList = getSObjectLineItems(sfsObject.Id).values();
        Integer solTot = solList.size();
        for (Integer solCnt = 0 ; solCnt < solTot ; solCnt++)
        {
            SObject sfsObjectLine = solList[solCnt];
            SBQQ__QuoteLine__c quoteLine = (SBQQ__QuoteLine__c) sfsObjectLine;
            string materialNumber = getMaterial(sfSObject, sfsObjectLine);
            SBO_EnosixSO_Detail.ITEMS orderItem = translateLineItemToOrderItem(quoteLine, materialNumber);
            if (orderItem != null)
            {
                UTIL_Order.addItemToOrder(orderDetail, orderItem, itemIncrement);
                addItemsConfig(orderDetail, orderItem, quoteLine);
                UTIL_Order.OrderLineValue orderLineValue = new UTIL_Order.OrderLineValue();
                addItemsConditions(orderDetail, orderItem, quoteLine, orderLineValue);
                orderLineValueMap.put(orderItem.ItemNumber.replaceFirst('^0+(?!$)', ''), orderLineValue);
                sfSObjectLineIdMap.put(orderItem.ItemNumber.replaceFirst('^0+(?!$)', ''), new UTIL_SFSObjectDoc.SfSObjectItem(sfsObjectLine.Id));
            }
        }

        if (ApexPages.currentPage() != null && cpqQuote.FLD_enosixPricingSimulationEnabled__c != null && !cpqQuote.FLD_enosixPricingSimulationEnabled__c)
        {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 
                'SAP Pricing Simulation not turned on for CPQ Quote'));
        }

        orderDetail.SALES.SalesDocumentType = cpqQuote.FLD_SAP_Order_Type__c;
        orderDetail.SALES.SalesOrganization = (String) getSAPConfigField(cpqQuote.SAP_Configuration__c, 'salesOrg');
        orderDetail.SALES.DistributionChannel = (String) getSAPConfigField(cpqQuote.SAP_Configuration__c, 'distChannel');
        orderDetail.SALES.Division = (String) getSAPConfigField(cpqQuote.SAP_Configuration__c, 'division');

        SBO_EnosixSO_Detail.PARTNERS soldToPartner = UTIL_Order.getPartnerFromOrder(orderDetail, UTIL_Customer.SOLD_TO_PARTNER_CODE, true);
		soldToPartner.CustomerNumber = (String) getSAPConfigField(cpqQuote.SAP_Configuration__c, 'soldToParty');

        SBO_EnosixSO_Detail.PARTNERS carrierPartner = UTIL_Order.getPartnerFromOrder(orderDetail, UTIL_Customer.CARRIER_PARTNER_CODE, true);
		carrierPartner.CustomerNumber = '600012';
    }

    @testVisible
    private void addItemsConfig(SBO_EnosixSO_Detail.EnosixSO orderDetail, SBO_EnosixSO_Detail.ITEMS orderItem, SBQQ__QuoteLine__c quoteLine)
    {
        List<Object> SAPList = (List<Object>) getSAPConfigField(quoteLine.SAP_Configuration__c, 'selectedCharacteristics');
        if (SAPList != null)
        {
            Integer cTot = SAPList.size();
            for (Integer cCnt = 0 ; cCnt < cTot ; cCnt++)
            {
                Object sapObj = SAPList[cCnt];
                Map<String, Object> sapMap = (Map<String, Object>) sapObj;
                SBO_EnosixSO_Detail.ITEMS_CONFIG itemConfig = new SBO_EnosixSO_Detail.ITEMS_CONFIG();
                itemConfig.ItemNumber = orderItem.ItemNumber;
                itemConfig.CharacteristicID = (String) sapMap.get('CharacteristicID');
                itemConfig.CharacteristicName = (String) sapMap.get('CharacteristicName');
                itemConfig.CharacteristicValue = (String) sapMap.get('CharacteristicValue');
                orderDetail.ITEMS_CONFIG.add(itemConfig);
            }
        }
    }

    @testVisible
    private void addItemsConditions(SBO_EnosixSO_Detail.EnosixSO orderDetail, SBO_EnosixSO_Detail.ITEMS orderItem, SBQQ__QuoteLine__c quoteLine,
        UTIL_Order.OrderLineValue orderLineValue)
    {
        orderLineValue.ListUnitPrice = quoteLine.SBQQ__ListPrice__c;
        if (quoteLine.SBQQ__AdditionalDiscount__c != null && quoteLine.SBQQ__AdditionalDiscount__c != 0)
        {
        	orderLineValue.ManualPrice = quoteLine.SBQQ__NetPrice__c;
        }
        if (quoteLine.ZCON_Percentage__c != null)
        {
            orderLineValue.ZCONpercent = quoteLine.ZCON_Percentage__c;
        }
    }

    @testVisible
    private SBO_EnosixSO_Detail.ITEMS translateLineItemToOrderItem(SBQQ__QuoteLine__c quoteLine, string materialNumber)
    {
        SBO_EnosixSO_Detail.ITEMS item = null;
        if (String.isNotBlank(materialNumber))
        {
            item = new SBO_EnosixSO_Detail.ITEMS();
            item.Material = materialNumber;
            item.ItemDescription = String.isEmpty(quoteLine.SBQQ__Description__c) ? '' : quoteLine.SBQQ__Description__c.replace('</div>','\n').replaceAll('<[^>]+>',' ').unescapeHtml4();
            item.OrderQuantity = quoteLine.SBQQ__Quantity__c;
            //item.OrderQuantity = (Decimal) getSAPConfigField(quoteLine.SAP_Configuration__c, 'OrderQuantity');

            if (quoteLine.Concession_Request_Name__c != null) {
                item.SalesConcessionAppNumber = quoteLine.Concession_Request_Name__c;
            }

            item.NetItemPrice = quoteLine.SBQQ__ListPrice__c;
			item.Plant = (String) getSAPConfigField(quoteLine.SAP_Configuration__c, 'plant');
        }
        return item;
    }

    public void finalizeOrderAndUpdateSfsObject(
        String calledFrom,
        SObject sfSObject,
        SBO_EnosixSO_Detail.EnosixSO orderDetail,
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap,
        Id pricebookId,
        List<SBO_EnosixSO_Detail.ITEMS> orderItems,
        Map<string, Id> materialToProductIdMap,
        Map<Id, PricebookEntry> productToPricebookEntryMap)
    {
        SBQQ__Quote__c quote = (SBQQ__Quote__c) sfSObject;
        if (string.isNotEmpty(orderDetail.SalesDocument))
        {
            // quote.ENSX_EDM__OrderNumber__c = orderDetail.SalesDocument;
            quote.FLD_SAP_Order_Number__c = orderDetail.SalesDocument;
        }
        upsert quote;

        // Add quote lines
        if (UTIL_Order.isAddMaterial || UTIL_Order.isEditMaterial || UTIL_Order.isCloneMaterial || UTIL_Order.isMoveMaterial)
        {
            upsertLineItemsFromOrderItems(quote, orderItems, materialToProductIdMap, productToPricebookEntryMap, sfSObjectLineIdMap);
        }
    }

    // upsertLineItemsFromOrderItems()
    //
    // Map the given order items to new quote lines and insert them into the db
    @testVisible
    private void upsertLineItemsFromOrderItems(
        SBQQ__Quote__c quote,
        List<SBO_EnosixSO_Detail.ITEMS> orderItems,
        Map<string, Id> materialToProductIdMap,
        Map<Id, PricebookEntry> productToPricebookEntryMap,
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap)
    {
        // List<SBQQ__QuoteLine__c> lineItems = new List<SBQQ__QuoteLine__c>();

        // for (SBO_EnosixSO_Detail.ITEMS item : orderItems)
        // {
        //     Id productId = materialToProductIdMap.get(item.Material);
        //     PricebookEntry price = productToPricebookEntryMap.get(productId);

        //     SBQQ__QuoteLine__c qli = new SBQQ__QuoteLine__c();

        //     if (sfSObjectLineIdMap.containsKey(item.ItemNumber.replaceFirst('^0+(?!$)', ''))
        //         && !sfSObjectLineIdMap.get(item.ItemNumber.replaceFirst('^0+(?!$)', '')).isDeleted)
        //     {
        //         qli.id = sfSObjectLineIdMap.get(item.ItemNumber.replaceFirst('^0+(?!$)', '')).id;
        //         System.debug('Updating Quote Line Item');
        //     }
        //     else
        //     {
        //         qli.SBQQ__Quote__c = quote.Id;
        //         qli.SBQQ__PricebookEntryId__c = price.Id;
        //         System.debug('Adding Quote Line Item');
        //     }
        //     qli.SBQQ__Description__c = item.ItemDescription;
        //     qli.SBQQ__Quantity__c = item.OrderQuantity;
        //     qli.SBQQ__NetPrice__c = item.NetItemPrice;
        //     qli.FLD_SAP_Item_Number__c = item.ItemNumber;

        //     System.debug(qli);

        //     lineItems.add(qli);
        // }

        // upsert lineItems;
    }
}