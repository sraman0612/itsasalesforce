/// enosiX Inc. Generated Apex Model
/// Generated On: 11/3/2020 5:47:45 PM
/// SAP Host: From REST Service On: https://gdi--enosixdev2.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev2.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class ensxtx_TST_EnosixMaterialBOM_Search
{

    public class Mockensxtx_SBO_EnosixMaterialBOM_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixMaterialBOM_Search.class, new Mockensxtx_SBO_EnosixMaterialBOM_Search());
        ensxtx_SBO_EnosixMaterialBOM_Search sbo = new ensxtx_SBO_EnosixMaterialBOM_Search();
        System.assertEquals(ensxtx_SBO_EnosixMaterialBOM_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SC sc = new ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SC();
        System.assertEquals(ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS childObj = new ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS();
        System.assertEquals(ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.Material = 'X';
        System.assertEquals('X', childObj.Material);

        childObj.Plant = 'X';
        System.assertEquals('X', childObj.Plant);

        childObj.QuantityForKit = 1.5;
        System.assertEquals(1.5, childObj.QuantityForKit);

        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);


    }

    @isTest
    static void testEnosixMaterialBOM_SR()
    {
        ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SR sr = new ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SR();

        sr.registerReflectionForClass();

        System.assertEquals(ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT childObj = new ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT();
        System.assertEquals(ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT_COLLECTION childObjCollection = new ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.SoldSeparately = true;
        System.assertEquals(true, childObj.SoldSeparately);

        childObj.BOMItemNumber = 'X';
        System.assertEquals('X', childObj.BOMItemNumber);

        childObj.BOMComponent = 'X';
        System.assertEquals('X', childObj.BOMComponent);

        childObj.ItemDescription = 'X';
        System.assertEquals('X', childObj.ItemDescription);

        childObj.ComponentQuantity = 1.5;
        System.assertEquals(1.5, childObj.ComponentQuantity);

        childObj.ComponentUnitOfMeasure = 'X';
        System.assertEquals('X', childObj.ComponentUnitOfMeasure);


    }

}