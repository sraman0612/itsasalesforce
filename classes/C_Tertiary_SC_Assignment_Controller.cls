public class C_Tertiary_SC_Assignment_Controller{
    
    private Id thisCaseId;
    private Id thisAccountId;
    private Account thisAccount;
    @TestVisible
    private List <COR_GMapsDistanceFinder.GMapsDistance> accountsOrderedByDistance;
    
    public C_Tertiary_SC_Assignment_Controller(ApexPages.StandardSetController controller) {
        
    }
    
    public C_Tertiary_SC_Assignment_Controller(ApexPages.StandardController controller) {
        
        Case thisCase = (Case)controller.getRecord();
        
        if (thisCase != null) {
            this.thisCaseId = thisCase.Id;
            this.thisAccountId = thisCase.Account.Id;
        }
        
        if (this.thisCaseId == null || this.thisAccountId == null) {
            this.thisCaseId     = ApexPages.currentPage().getParameters().get('CaseID');
            this.thisAccountId  = ApexPages.currentPage().getParameters().get('AccountID');
        }
        
        
        if (this.thisCaseId == null || this.thisAccountId == null) {
            if (flDemo == null) System.debug('FLOW IS NULL');
            else System.debug('FLOW IS NOT NULL!');
            this.thisCaseId     = (Id)flDemo.CaseID;
            this.thisAccountId  = (Id)flDemo.AccountID;
        }
        
        if (this.thisAccount == null) {
            getAccount();
        }
    }
    
    //Allows for the 'Finish' button in the 'Service Center Assignment' Flow to redirect back to the Case when finished
    // Instantiate the Flow for use by the Controller - linked by VF "interview" attribute
    public Flow.Interview.Service_Center_Wizard flDemo {get;set;}
    
    // Factor PageReference as a full GET/SET
    public PageReference prFinishLocation {
        get {
            PageReference prRef = new PageReference('/' + strOutputVariable);
            prRef.setRedirect(true);
            return prRef;
        }
        set { prFinishLocation = value; }
    }
    
    // Factor Flow output variable pull as a full GET / SET
    public String strOutputVariable {
        get {
            String strTemp = '';
            
            if(flDemo != null) {
                strTemp = string.valueOf(flDemo.getVariableValue('CaseID'));
            }
            else System.debug('FLOW IS NULL HERE TOO!');
            
            return strTemp;
        }
        
        set { strOutputVariable = value; }
    } 

    public ApexPages.StandardSetController setCon {
        get {
            if (thisAccount == null) getAccount();
            
            if(setCon == null) { 
                try {
                    string latlongstring = thisAccount.Location__latitude__s+','+thisAccount.Location__longitude__s;
                    string maxDistance = '1000';
                    string units = 'mi';
                    string rowLimit = '10';
                    string query = 'SELECT Id, Managed_Care_Survey_Average__c, Service_Center_Notes__c, Name, Phone, After_Hours_Phone__c, After_Hours__c, Stocking_Service_Center__c, Labor__c, Travel__c, Mileage__c, Tertiary_Estimated_Visit_Date__c, Tertiary_Disposition__c, Tertiary_Decline_Reason__c, Tertiary_Declined_Reason_PL__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Inactive__c = FALSE AND BillingCountry = \'' + this.thisAccount.BillingCountry + '\' AND RecordType.Name = \'Service Center\' and DISTANCE(Location__c, GEOLOCATION('+latlongString+'), \''+units+'\') < '+maxDistance+' order by DISTANCE(Location__c, GEOLOCATION('+latlongString+'), \''+units+'\') asc LIMIT '+rowLimit;
                     
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(query));
                }
                catch (Exception ex) {
                    throw new COR_GMapsException('The account in question may not have latitude / longitude coordinates assigned to it. Please correct this and try again.');
                }
            }
            return setCon;
        }
        set;
    }
    
    // Initialize setCon and return a list of records
    public List<Account> getaccountList() {
        return (List<Account>) setCon.getRecords();
    }
    
    
    List <COR_GMapsDistanceFinder.GMapsDistance> preferredAccountsOrderedByDistance;
    List <COR_GMapsDistanceFinder.GMapsDistance> otherAccountsOrderedByDistance;
    
    public List <COR_GmapsDistanceFinder.GMapsDistance> getNearbyAccounts () {
        
        if (this.accountsOrderedByDistance == null) {
            System.debug('GETTING NEARBY SERVICE CENTERS');
            if (this.thisAccount == null) {
                getAccount();
            }
            
            Map <Id,COR_GMapsDistanceFinder.GMapsDistance> acctMap = new Map<ID,COR_GMapsDistanceFinder.GMapsDistance>();
            
            List <Account> serviceCenterAccounts = getaccountList();
            otherAccountsOrderedByDistance = COR_GMapsDistanceFinder.orderServiceCentersByDistance(this.thisAccount, serviceCenterAccounts);
            
            for(COR_GMapsDistanceFinder.GMapsDistance gmd : otherAccountsOrderedByDistance){
                acctMap.put(gmd.account.Id, gmd);
            }
            
            List <Account> preferredServiceCenters = new List <Account> ([SELECT Id, Managed_Care_Survey_Average__c, Service_Center_Notes__c, Name, Phone, After_Hours__c, After_Hours_Phone__c, Stocking_Service_Center__c, Labor__c, Travel__c, Mileage__c, Tertiary_Estimated_Visit_Date__c, Tertiary_Disposition__c, Tertiary_Decline_Reason__c, Tertiary_Declined_Reason_PL__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Inactive__c = FALSE AND RecordType.Name = 'Service Center' and Id in (Select Service_Center__c From Account_Association__c Where Customer__c = :this.thisAccount.Id AND Active__c = true)]);
            List <Account_Association__c> associations = [SELECT Id, Service_Center__c, Priority__c, Hours__c FROM Account_Association__c WHERE Active__c = TRUE AND Service_Center__c IN :preferredServiceCenters AND Customer__c = :this.thisAccount.Id AND Service_Center__r.Inactive__c = FALSE];                        
            
            preferredAccountsOrderedByDistance = COR_GMapsDistanceFinder.orderServiceCentersByDistance(this.thisAccount, preferredServiceCenters);
            COR_GMapsDistanceFinder.GMapsDistance[] prefOrder = new COR_GMapsDistanceFinder.GMapsDistance[4];
            for(COR_GMapsDistanceFinder.GMapsDistance gmd: preferredAccountsOrderedByDistance){
                gmd.setNameBold();
                for(Account_Association__c aa : associations){
                    if(aa.Service_Center__c == gmd.Account.Id){
                    
                        if(String.isEmpty(gmd.designation)){
                        	
                            gmd.designation = (aa.Priority__c == 'Primary' ? 'Prim' : 'Sec')  + ' - ' + (aa.Hours__c == 'Business Hours' ? 'Bus' : 'After');  
                            
                            if(gmd.designation == 'Prim - Bus'){
                                prefOrder[0] = gmd;
                            }
                            else if(gmd.designation == 'Sec - Bus'){
                                prefOrder[1] = gmd;
                            }
                            else if(gmd.designation == 'Prim - After'){
                                prefOrder[2] = gmd;
                            }
                            else{
                                prefOrder[3] = gmd;
                            }
                        }
                        else{
                            gmd.designation += '<br/>' + (aa.Priority__c == 'Primary' ? 'Prim' : 'Sec')  + ' - ' + (aa.Hours__c == 'Business Hours' ? 'Bus' : 'After');  
                        }
                        
                        
                    }
                }
            }
            Set <COR_GMapsDistanceFinder.GMapsDistance> finalAccountList = new Set <COR_GMapsDistanceFinder.GMapsDistance> ();
            for(integer i = 0; i < 4; i++){
                if(prefOrder[i] != null){
                    finalAccountList.add(prefOrder[i]);
                }
            }
            for(COR_GMapsDistanceFinder.GMapsDistance gmd : finalAccountList){
                acctMap.remove(gmd.account.Id);
            }
            finalAccountList.addAll(acctMap.values());            
            /*
            for(COR_GMapsDistanceFinder.GMapsDistance gmd : preferredAccountsOrderedByDistance){
                acctMap.put(gmd.account.Id, gmd);
            }
            */
            
            /*
            finalAccountList.addAll(preferredAccountsOrderedByDistance);      
            if (otherAccountsOrderedByDistance != null) {
                finalAccountList.addAll(otherAccountsOrderedByDistance);
            }
            */
            
            this.accountsOrderedByDistance = new List<COR_GMapsDistanceFinder.GMapsDistance>(finalAccountList);
        }
        
        
        return this.accountsOrderedByDistance;
    }
    
    
    private void getAccount () {
        if (this.thisAccountId == null) {
            if (this.thisCaseId == null) {
                throw new COR_GMapsException ('This controller needs either a case ID or an account ID input.');
            }
            else {
                Case thisCase = [Select Id, AccountId From Case Where Id = :this.thisCaseId Limit 1];
                this.thisAccountId = thisCase.AccountId;
            }
        }
        this.thisAccount = [Select Id, Managed_Care_Survey_Average__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, Location__latitude__s, Location__longitude__s From Account Where Id = :thisAccountId Limit 1];
    }
    
    
    
    public PageReference saveRecords() {
        List <COR_GmapsDistanceFinder.GMapsDistance> updatedAccounts   = getUpdatedAccounts();
        List <Case_Association__c>  updatedCaseAssociationList         = new List <Case_Association__c> ();
        List <Site_Visit__c>        updatedSiteVisitList               = new List <Site_Visit__c> ();
        
        integer countAccepted = 0;
        
        for (COR_GmapsDistanceFinder.GMapsDistance distanceMetric : updatedAccounts) {
            
            Account a = distanceMetric.account;
            
            // For the old value:
            //distanceMetric.newDisposition
            
            if(distanceMetric.account.Tertiary_Disposition__c == 'Declined'){
                
                Case_Association__c thisCS = new Case_Association__c ();
                
                thisCS.Case__c = thisCaseId;
                thisCS.Selected_Account__c = thisAccountId;
                thisCS.Disposition__c = distanceMetric.account.Tertiary_Disposition__c;
                thisCS.Declined_Not_Contacted_Reason__c = distanceMetric.account.Tertiary_Decline_Reason__c;
                thisCS.Declined_Service_Center__c = distanceMetric.account.Id;
                thisCS.Declined_Reason__c = distanceMetric.account.Tertiary_Declined_Reason_PL__c;
                
                
                updatedCaseAssociationList.add(thisCS);
                
            }
            else if(distanceMetric.account.Tertiary_Disposition__c == 'Accepted')  {
                
                Case_Association__c thisCS = new Case_Association__c ();
                
                thisCS.Case__c = thisCaseId;
                thisCS.Selected_Account__c = thisAccountId;
                thisCS.Disposition__c = distanceMetric.account.Tertiary_Disposition__c;
                thisCS.Declined_Not_Contacted_Reason__c = distanceMetric.account.Tertiary_Decline_Reason__c;
                thisCS.Selected_Service_Center__c = distanceMetric.account.Id;
                thisCS.Declined_Reason__c = distanceMetric.account.Tertiary_Declined_Reason_PL__c;
                
                
                
                Site_Visit__c thisSV = new Site_Visit__c ();
                
                thisSV.Case__c = thisCaseId;
                thisSV.Customer__c = thisAccountId;
                thisSV.Estimated_Visit_Date__c = distanceMetric.account.Tertiary_Estimated_Visit_Date__c;
                thisSV.Service_Center__c = distanceMetric.account.Id;
                thisSV.Travel_Distance__c = distanceMetric.distanceInMiles * 2;
                thisSV.Travel_Time__c = distanceMetric.durationInMinutes * 2;
                
                
                
                Case caseUpdate = [SELECT Id, Active_Service_Center__c, Travel_Distance__c, Travel_Distance_Number__c, Travel_Time__c, Travel_Time_Number__c FROM Case WHERE Id = :thisCaseId];
                caseUpdate.Active_Service_Center__c     = distanceMetric.account.Id;
                caseUpdate.Travel_Distance__c           = distanceMetric.distanceText;
                caseUpdate.Travel_Time__c               = distanceMetric.durationText;
                caseUpdate.Travel_Distance_Number__c    = distanceMetric.distanceInMiles * 2;
                caseUpdate.Travel_Time_Number__c        = distanceMetric.durationInMinutes * 2;
                
                updatedSiteVisitList.add(thisSV);
                updatedCaseAssociationList.add(thisCS);
                insert updatedCaseAssociationList;
                update caseUpdate;
                insert updatedSiteVisitList;
                
                
                countAccepted++;
                
                PageReference pg = new PageReference('/'+thisCaseId);
                return pg;
                
            } else {
                
                return null;
                
            }   
            
        }
        
        if(countAccepted > 1){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Only one Account can be marked as Approved');
            ApexPages.addMessage(errorMsg);
        }
        else{
            insert updatedCaseAssociationList;
            insert updatedSiteVisitList;
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Records Saved'));
        }
        
        return null;
    }
    
    
    //public List <COR_GmapsDistanceFinder.GMapsDistance> updatedAccounts {get; set;}
    private List <COR_GmapsDistanceFinder.GMapsDistance> getUpdatedAccounts () {
        List <COR_GmapsDistanceFinder.GMapsDistance> updatedAccounts = new List <COR_GmapsDistanceFinder.GMapsDistance> ();
        
        if (this.accountsOrderedByDistance != null) {
            for (COR_GMapsDistanceFinder.GMapsDistance distanceMetric : accountsOrderedByDistance) {
                if (distanceMetric.account.Tertiary_Disposition__c != distanceMetric.newDisposition || distanceMetric.account.Tertiary_Decline_Reason__c != distanceMetric.newDeclineReason) {
                    updatedAccounts.add(distanceMetric);
                }
            }
        }
        
        return updatedAccounts;
    }
}