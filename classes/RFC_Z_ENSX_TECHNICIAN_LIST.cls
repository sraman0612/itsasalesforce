/// enosiX Inc. Generated Apex Model
/// Generated On: 3/10/2020 10:20:55 AM
/// SAP Host: From REST Service On: https://gdi--Dev.my.salesforce.com
/// CID: From REST Service On: https://gdi--Dev.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class RFC_Z_ENSX_TECHNICIAN_LIST extends ensxsdk.EnosixFramework.RFC
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('RFC_Z_ENSX_TECHNICIAN_LIST_Meta', new Type[] {
            RFC_Z_ENSX_TECHNICIAN_LIST.RESULT.class
            , RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST.class
            , RFC_Z_ENSX_TECHNICIAN_LIST.RETURN.class
            } 
        );
    }

    public RFC_Z_ENSX_TECHNICIAN_LIST()
    {
        super('Z_ENSX_TECHNICIAN_LIST', RFC_Z_ENSX_TECHNICIAN_LIST.RESULT.class);
    }

    public override Type getType() { return RFC_Z_ENSX_TECHNICIAN_LIST.class; }

    public RESULT PARAMS
    {
        get { return (RESULT)this.getParameterContext(); }
    }

    public RESULT execute()
    {
        return (RESULT)this.executeFunction();
    }
    
    public class RESULT extends ensxsdk.EnosixFramework.FunctionObject
    {    	
        public RESULT()
        {
            super(new Map<string,type>
            {
                'E_TECHNICIAN_LIST' => RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST.class
        ,'RETURN' => RFC_Z_ENSX_TECHNICIAN_LIST.FieldRETURN.class
            });	
        }
        
        public override Type getType() { return RFC_Z_ENSX_TECHNICIAN_LIST.RESULT.class; }

        public override void registerReflectionForClass()
        {
            RFC_Z_ENSX_TECHNICIAN_LIST.registerReflectionInfo();
        }

        public List<E_TECHNICIAN_LIST> E_TECHNICIAN_LIST_List
    {
        get 
        {
            List<E_TECHNICIAN_LIST> results = new List<E_TECHNICIAN_LIST>();
            this.getCollection(RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST.class).copyTo(results);
            return results;
        }
    }
    public List<FieldRETURN> FieldRETURN_List
    {
        get 
        {
            List<FieldRETURN> results = new List<FieldRETURN>();
            this.getCollection(RFC_Z_ENSX_TECHNICIAN_LIST.FieldRETURN.class).copyTo(results);
            return results;
        }
    }
        @AuraEnabled public String I_KUNNR
        { 
            get { return this.getString ('I_KUNNR'); } 
            set { this.Set (value, 'I_KUNNR'); }
        }

        @AuraEnabled public String I_VKORG
        { 
            get { return this.getString ('I_VKORG'); } 
            set { this.Set (value, 'I_VKORG'); }
        }

    	
    }
    public class E_TECHNICIAN_LIST extends ensxsdk.EnosixFramework.ValueObject
    {
        public E_TECHNICIAN_LIST()
        {
            super('E_TECHNICIAN_LIST', new Map<string,type>());
        }

        public override Type getType() { return RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST.class; }

        public override void registerReflectionForClass()
        {
            RFC_Z_ENSX_TECHNICIAN_LIST.registerReflectionInfo();
        }

                    @AuraEnabled public String PARNR
        { 
            get { return this.getString ('PARNR'); } 
            set { this.Set (value, 'PARNR'); }
        }

        @AuraEnabled public String NAMEV
        { 
            get { return this.getString ('NAMEV'); } 
            set { this.Set (value, 'NAMEV'); }
        }

        @AuraEnabled public String Name1
        { 
            get { return this.getString ('NAME1'); } 
            set { this.Set (value, 'NAME1'); }
        }

        @AuraEnabled public String PAFKT
        { 
            get { return this.getString ('PAFKT'); } 
            set { this.Set (value, 'PAFKT'); }
        }

        @AuraEnabled public String ABTNR
        { 
            get { return this.getString ('ABTNR'); } 
            set { this.Set (value, 'ABTNR'); }
        }

            
        }
    public class FieldRETURN extends ensxsdk.EnosixFramework.ValueObject
    {
        public FieldRETURN()
        {
            super('FieldRETURN', new Map<string,type>());
        }

        public override Type getType() { return RFC_Z_ENSX_TECHNICIAN_LIST.FieldRETURN.class; }

        public override void registerReflectionForClass()
        {
            RFC_Z_ENSX_TECHNICIAN_LIST.registerReflectionInfo();
        }

                    @AuraEnabled public String FieldTYPE
        { 
            get { return this.getString ('TYPE'); } 
            set { this.Set (value, 'TYPE'); }
        }

        @AuraEnabled public String FieldID
        { 
            get { return this.getString ('ID'); } 
            set { this.Set (value, 'ID'); }
        }

        @AuraEnabled public String FieldNUMBER
        { 
            get { return this.getString ('NUMBER'); } 
            set { this.Set (value, 'NUMBER'); }
        }

        @AuraEnabled public String MESSAGE
        { 
            get { return this.getString ('MESSAGE'); } 
            set { this.Set (value, 'MESSAGE'); }
        }

        @AuraEnabled public String LOG_NO
        { 
            get { return this.getString ('LOG_NO'); } 
            set { this.Set (value, 'LOG_NO'); }
        }

        @AuraEnabled public String LOG_MSG_NO
        { 
            get { return this.getString ('LOG_MSG_NO'); } 
            set { this.Set (value, 'LOG_MSG_NO'); }
        }

        @AuraEnabled public String MESSAGE_V1
        { 
            get { return this.getString ('MESSAGE_V1'); } 
            set { this.Set (value, 'MESSAGE_V1'); }
        }

        @AuraEnabled public String MESSAGE_V2
        { 
            get { return this.getString ('MESSAGE_V2'); } 
            set { this.Set (value, 'MESSAGE_V2'); }
        }

        @AuraEnabled public String MESSAGE_V3
        { 
            get { return this.getString ('MESSAGE_V3'); } 
            set { this.Set (value, 'MESSAGE_V3'); }
        }

        @AuraEnabled public String MESSAGE_V4
        { 
            get { return this.getString ('MESSAGE_V4'); } 
            set { this.Set (value, 'MESSAGE_V4'); }
        }

        @AuraEnabled public String PARAMETER
        { 
            get { return this.getString ('PARAMETER'); } 
            set { this.Set (value, 'PARAMETER'); }
        }

        @AuraEnabled public String ROW
        { 
            get { return this.getString ('ROW'); } 
            set { this.Set (value, 'ROW'); }
        }

        @AuraEnabled public String FIELD
        { 
            get { return this.getString ('FIELD'); } 
            set { this.Set (value, 'FIELD'); }
        }

        @AuraEnabled public String FieldSYSTEM
        { 
            get { return this.getString ('SYSTEM'); } 
            set { this.Set (value, 'SYSTEM'); }
        }

            
        }
}