public with sharing class UTIL_WarrantyClaimExpenseItemCleanup implements Database.Batchable<Warranty_Claim_Expenses__c> {

    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_WarrantyClaimExpenseItemCleanup.class);

    public List<Warranty_Claim_Expenses__c> start(Database.BatchableContext context) {
        return [SELECT Id, SAP_Item_Number__c, Warranty_Claim__c FROM Warranty_Claim_Expenses__c WHERE SAP_Item_Number__c = '000020' AND isDeleted = false];
    }

    public void execute(Database.BatchableContext context, List<Warranty_Claim_Expenses__c> expenseItem20s) {
        Map<String,Warranty_Claim_Expenses__c> warrantyClaimItem20Map = new Map<String,Warranty_Claim_Expenses__c>();
        List<Warranty_Claim_Expenses__c> toDelete = new List<Warranty_Claim_Expenses__c>();

        for (Warranty_Claim_Expenses__c expenseItem20 : expenseItem20s) {
            warrantyClaimItem20Map.put(expenseItem20.Warranty_Claim__c, expenseItem20);
            toDelete.add(expenseItem20);
        }

        List<Warranty_Claim_Expenses__c> expenseItem10s = [SELECT Id, SAP_Item_Number__c, Warranty_Claim__c FROM Warranty_Claim_Expenses__c WHERE SAP_Item_Number__c = '000010' AND Warranty_Claim__c in : warrantyClaimItem20Map.keySet()];

        for (Warranty_Claim_Expenses__c expenseItem10 : expenseItem10s) {
            toDelete.remove(toDelete.indexOf(warrantyClaimItem20Map.get(expenseItem10.Warranty_Claim__c)));
        }

        System.debug('Deleting ' + toDelete.size() + ' expense items.');

        if (!toDelete.isEmpty()) {
            delete toDelete;
        }
    }

    public void finish(Database.BatchableContext BC) {
        UTIL_SyncFailureNotifier.syncFinished(BC);
    }
}