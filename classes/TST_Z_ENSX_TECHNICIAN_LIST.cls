/// enosiX Inc. Generated Apex Model
/// Generated On: 3/10/2020 10:20:55 AM
/// SAP Host: From REST Service On: https://gdi--Dev.my.salesforce.com
/// CID: From REST Service On: https://gdi--Dev.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
public with sharing class TST_Z_ENSX_TECHNICIAN_LIST
{
    public class MockRFC_Z_ENSX_TECHNICIAN_LIST implements ensxsdk.EnosixFramework.RFCMock
    {
        public ensxsdk.EnosixFramework.FunctionObject executeFunction() 
        {
            return null;
        }
    }

    @isTest
    static void testExecute()
    {
        ensxsdk.EnosixFramework.setMock(RFC_Z_ENSX_TECHNICIAN_LIST.class, new MockRFC_Z_ENSX_TECHNICIAN_LIST());
        RFC_Z_ENSX_TECHNICIAN_LIST rfc = new RFC_Z_ENSX_TECHNICIAN_LIST();
        RFC_Z_ENSX_TECHNICIAN_LIST.RESULT params = rfc.PARAMS;
        System.assertEquals(null, rfc.execute());
    }

    @isTest
    static void testRESULT()
    {
        RFC_Z_ENSX_TECHNICIAN_LIST.RESULT funcObj = new RFC_Z_ENSX_TECHNICIAN_LIST.RESULT();

        funcObj.registerReflectionForClass();

        System.assertEquals(RFC_Z_ENSX_TECHNICIAN_LIST.RESULT.class, funcObj.getType(), 'getType() does not match object type.');

        funcObj.I_KUNNR = 'X';
        System.assertEquals('X', funcObj.I_KUNNR);

        funcObj.I_VKORG = 'X';
        System.assertEquals('X', funcObj.I_VKORG);

        //Check all the collections
        funcObj.getCollection(RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST.class).add(new RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST());
        System.assertEquals(1,funcObj.E_TECHNICIAN_LIST_List.size());

        funcObj.getCollection(RFC_Z_ENSX_TECHNICIAN_LIST.FieldRETURN.class).add(new RFC_Z_ENSX_TECHNICIAN_LIST.FieldRETURN());
        System.assertEquals(1,funcObj.FieldRETURN_List.size());

    }

    @isTest
    static void testE_TECHNICIAN_LIST()
    {
        RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST funcObj = new RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST();

        funcObj.registerReflectionForClass();

        System.assertEquals(RFC_Z_ENSX_TECHNICIAN_LIST.E_TECHNICIAN_LIST.class, funcObj.getType(), 'getType() does not match object type.');
        funcObj.PARNR = 'X';
        System.assertEquals('X', funcObj.PARNR);

        funcObj.NAMEV = 'X';
        System.assertEquals('X', funcObj.NAMEV);

        funcObj.Name1 = 'X';
        System.assertEquals('X', funcObj.Name1);

        funcObj.PAFKT = 'X';
        System.assertEquals('X', funcObj.PAFKT);

        funcObj.ABTNR = 'X';
        System.assertEquals('X', funcObj.ABTNR);

    }
    @isTest
    static void testFieldRETURN()
    {
        RFC_Z_ENSX_TECHNICIAN_LIST.FieldRETURN funcObj = new RFC_Z_ENSX_TECHNICIAN_LIST.FieldRETURN();

        funcObj.registerReflectionForClass();

        System.assertEquals(RFC_Z_ENSX_TECHNICIAN_LIST.FieldRETURN.class, funcObj.getType(), 'getType() does not match object type.');
        funcObj.FieldTYPE = 'X';
        System.assertEquals('X', funcObj.FieldTYPE);

        funcObj.FieldID = 'X';
        System.assertEquals('X', funcObj.FieldID);

        funcObj.FieldNUMBER = 'X';
        System.assertEquals('X', funcObj.FieldNUMBER);

        funcObj.MESSAGE = 'X';
        System.assertEquals('X', funcObj.MESSAGE);

        funcObj.LOG_NO = 'X';
        System.assertEquals('X', funcObj.LOG_NO);

        funcObj.LOG_MSG_NO = 'X';
        System.assertEquals('X', funcObj.LOG_MSG_NO);

        funcObj.MESSAGE_V1 = 'X';
        System.assertEquals('X', funcObj.MESSAGE_V1);

        funcObj.MESSAGE_V2 = 'X';
        System.assertEquals('X', funcObj.MESSAGE_V2);

        funcObj.MESSAGE_V3 = 'X';
        System.assertEquals('X', funcObj.MESSAGE_V3);

        funcObj.MESSAGE_V4 = 'X';
        System.assertEquals('X', funcObj.MESSAGE_V4);

        funcObj.PARAMETER = 'X';
        System.assertEquals('X', funcObj.PARAMETER);

        funcObj.ROW = 'X';
        System.assertEquals('X', funcObj.ROW);

        funcObj.FIELD = 'X';
        System.assertEquals('X', funcObj.FIELD);

        funcObj.FieldSYSTEM = 'X';
        System.assertEquals('X', funcObj.FieldSYSTEM);

    }
}