public with sharing class CTRL_EnosixInventorySearch
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_ContactList.class);

    @AuraEnabled
    public static UTIL_Aura.Response doSearch(String salesOrg, String distributionChannel) {
        if ((Boolean)UTIL_AppSettings.getValue('CTRL_EnosixInventorySearch.LocalSearch', false)) {
            return doSearchLocal(salesOrg, distributionChannel);
        }

        return doSearchSAP(salesOrg, distributionChannel);
    }

    @AuraEnabled
    public static UTIL_Aura.Response doSearchLocal(String salesOrg, String distributionChannel) {
        if (String.isEmpty(salesOrg) || String.isEmpty(distributionChannel)) {
            UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.InvSearch_Error_No_SalesOrg);
            return UTIL_Aura.createResponse(null);
        }

        List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT> stockList = new List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT>();

        List<Product2> products = [Select ProductCode, Family, Name, Description, SAP_Stock__c, SAP_Scheduled__c from Product2 where IsActive = true AND FLD_Distribution_Channel__c = : distributionChannel AND (SAP_Stock__c > 0 OR SAP_Scheduled__c > 0) ORDER BY ProductCode ASC LIMIT 1000];

        for (Product2 product : products) {
            SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT result = new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();

            result.Material = product.ProductCode;
            result.MaterialFamily = product.Family;
            result.MaterialDescription = product.Description != null && product.Description != '' ? product.Description : product.Name;
            result.Scheduled = product.SAP_Scheduled__c;
            result.Stock = product.SAP_Stock__c;

            stockList.add(result);
        }

        return UTIL_Aura.createResponse(JSON.serialize(stockList));
    }

    @AuraEnabled
    public static UTIL_Aura.Response doSearchSAP(String salesOrg, String distributionChannel) {
        if (String.isEmpty(salesOrg) || String.isEmpty(distributionChannel)) {
            UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, System.Label.InvSearch_Error_No_SalesOrg);
            return UTIL_Aura.createResponse(null);
        }

        logger.enterAura('doSearch', new Map<String, Object> {
            'salesOrg' => salesOrg,
            'distributionChannel' => distributionChannel
        });

        SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC searchContext = new SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC();

        searchContext.SEARCHPARAMS.SalesOrganization = salesOrg;
        searchContext.SEARCHPARAMS.DistributionChannel = distributionChannel;
        searchContext.pagingOptions.pageSize = 9999;
        searchContext.pagingOptions.pageNumber = 0;

        SBO_EnosixMaterialInventorySync_Search sbo = new SBO_EnosixMaterialInventorySync_Search();

        try {
            searchContext = sbo.search(searchContext);
        } catch (Exception e) {
            UTIL_PageMessages.addExceptionMessage(e);
        } finally {
            logger.exit();
        }

        if (searchContext.result != null) {
            List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT> results = searchContext.result.getResults();
            List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT> retVal = new List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT>();
    
            for (SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT result : results) {
                if ((result.Stock != null && result.Stock > 0) || (result.Scheduled != null && result.Scheduled > 0)) {
                    retVal.add(result);
                }
            }
    
            return UTIL_Aura.createResponse(JSON.serialize(retVal));
        }

        return UTIL_Aura.createResponse(null);
    }

    private static COMMUNITY_USER_INFO getCommunityUserInfoImpl(String recordId) {
        COMMUNITY_USER_INFO retVal = new COMMUNITY_USER_INFO();

        if (recordId != null) {
            String sobjectName = getSobjectNameFromId(recordId);

            if ('Account'.equals(sobjectName)) {
                retVal.AccountId = recordId;
            } else if ('Opportunity'.equals(sobjectName)) {
                retVal.OpportunityId = recordId;


                Opportunity opp = [Select Id, AccountId from Opportunity where id =: retVal.OpportunityId];

                if (opp != null) {
                    retVal.AccountId = opp.AccountId;
                }
            }
        // We don't have a record Id when we are on a contact in the community so this should already be working as expected
        } else {
            String contactId = [Select contactid from User where id =: Userinfo.getUserid()].contactId;

            if (contactId != null) {
                retVal.AccountId = [Select AccountId from Contact where id =: contactid].AccountId;
            }
        }

        if (retVal.AccountId == null) {
            return retVal;
        }

        Account account = null;
        
        try {
            account = [Select Id, DC__c, Child_DCs__c, Parent_Account__c from Account where Id =: retVal.AccountId LIMIT 1];
        } catch (Exception e) {
            return retVal;
        }

        List<Distribution_Channel__mdt> dcs = [SELECT DeveloperName, MasterLabel from Distribution_Channel__mdt];
        Map<String,String> dcMap = new Map<String,String>();

        for (Distribution_Channel__mdt dc : dcs) {
            dcMap.put(dc.DeveloperName, dc.MasterLabel);
        }

        List<LABEL_VALUE_PAIR> distributionChannelList = new List<LABEL_VALUE_PAIR>();

        if (account.Child_DCs__c != null) {
            List<String> childDCs = account.Child_DCs__c.split(',');

            for (String childDC : childDCs) {
                if (String.isNotBlank(dcMap.get(childDC))) {
                    distributionChannelList.add(new LABEL_VALUE_PAIR(dcMap.get(childDC), childDC));
                }
            }
        } else if (account.DC__c != null && account.DC__c.length() > 3) {
            String dcValue = account.DC__c.substring(0,2);

            distributionChannelList.add(new LABEL_VALUE_PAIR(dcMap.get(dcValue), dcValue));
        }

        retVal.DistributionChannels = distributionChannelList;

        return retVal;
    }

    @AuraEnabled
    public static UTIL_Aura.Response getCommunityUserInfo(String recordId) {
        return UTIL_Aura.createResponse(JSON.serialize(getCommunityUserInfoImpl(recordId)));
    }

    @AuraEnabled
    public static UTIL_Aura.Response doCreateQuote(String accountId, String distributionChannel, List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT> selectedRows) {
        if (accountId == null) {
            UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, 'No account supplied for current user.');
            return UTIL_Aura.createResponse(null);
        }

        COMMUNITY_USER_INFO userInfo = getCommunityUserInfoImpl(null);
        Account account = [SELECT Id, Name from Account where Id =: accountId LIMIT 1];

        Date closeDate = System.today().addDays(30);

        Id pricebookId = (Test.isRunningTest()?Test.getStandardPricebookId():[SELECT Id from Pricebook2 where Name = 'Standard Price Book' LIMIT 1].Id);

        RecordType recordType = [SELECT Id from RecordType where DeveloperName = 'Sales' AND SobjectType = 'Opportunity' LIMIT 1];
        RecordType quoteRecordType = [SELECT Id from RecordType where DeveloperName = 'Quote_Draft' AND SobjectType = 'SBQQ__Quote__c' LIMIT 1];

        Opportunity opp = new Opportunity();

        opp.Name = 'Quick Order for ' + account.Name +  ' ' + closeDate.format();
        opp.StageName = 'Stage 1 - Qualified Lead';
        opp.CloseDate = closeDate;
        opp.AccountId = (Test.isRunningTest()?accountId:userInfo.AccountId);
        opp.End_User_Account__c = accountId;
        opp.Quick_Order__c = true;
        opp.SBQQ__QuotePricebookId__c = pricebookId;
        opp.RecordTypeId = recordType.Id;
        opp.Sales_Channel__c = distributionChannel;

        insert opp;

        SBQQ__Quote__c quote = new SBQQ__Quote__c();

        quote.SBQQ__Account__c = (Test.isRunningTest()?accountId:userInfo.AccountId);
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__Primary__c = true;
        quote.RecordTypeId = quoteRecordType.Id;
        quote.End_Customer_Account__c = accountId;
        quote.Distribution_Channel__c = distributionChannel;

        insert quote;

        if (selectedRows != null) {
            List<String> productCodes = new List<String>();

            for (SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT row: selectedRows) {
                productCodes.add(row.Material);
                System.debug('row.Material=' + row.Material);
                System.debug('row.Quantity=' + row.Quantity);
            }

            List<Product2> products = [SELECT Id, ProductCode from Product2 where ProductCode in : productCodes];
            Map<String,Product2> productMap = new Map<String,Product2>();

            for (Product2 product : products) {
                productMap.put(product.ProductCode, product);
            }

            List<SBQQ__QuoteLine__c> quoteLines = new List<SBQQ__QuoteLine__c>();
            for (SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT row: selectedRows) {
                SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
                Id productId = (productMap.get(row.Material)==null?null:productMap.get(row.Material).Id);

                quoteLine.SBQQ__Quote__c = quote.Id;
                quoteLine.SBQQ__Quantity__c = row.Quantity;
                quoteLine.SBQQ__Product__c = productId;

                quoteLines.add(quoteLine);
            }

            insert quoteLines;
        }

        CPQ_URL__c cpqURL = CPQ_URL__c.getInstance();

        return UTIL_Aura.createResponse(cpqURL.CPQ_URL__c + quote.Id);
    }

    public class COMMUNITY_USER_INFO {
        public String AccountId {get;set;}
        public String OpportunityId {get;set;}
        public List<LABEL_VALUE_PAIR> DistributionChannels {get;set;}
    }

    public class LABEL_VALUE_PAIR {
        public String label { get; set; }
        public String value { get; set; }

        public LABEL_VALUE_PAIR(String label) {
            this.label = label;
            this.value = label;
        }

        public LABEL_VALUE_PAIR(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    public static String getSobjectNameFromId(String id) {
        if (id == null) {
            return null;
        }

        String objectName;

        try {
            Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe();

            for (Schema.SObjectType stype : gd.values()) {
                Schema.DescribeSObjectResult r = stype.getDescribe();
                String prefix = r.getKeyPrefix();

                if (prefix!=null && id.startsWith(prefix)) {
                    objectName = r.getName();
                    break;
                }
            }
        } catch (Exception e) {
            System.debug(e);
        }

        return objectName;
    }
}