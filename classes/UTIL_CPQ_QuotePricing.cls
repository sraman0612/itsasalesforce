public with sharing class UTIL_CPQ_QuotePricing 
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_CPQ_QuotePricing.class);

    // TODO: Rip out defaults here and instead require them to be passed in
    @testVisible
    private static String conditionType = (String)UTIL_AppSettings.getValue('CPQSimulation.ConditionType', '');
    @testVisible
    private static String defaultSalesDocType = (String)UTIL_AppSettings.getValue('CPQSimulation.DefaultSalesDocType', '');
    @testVisible
    private static String defaultSalesOrg = (String)UTIL_AppSettings.getValue('CPQSimulation.DefaultSalesOrg', '');
    @testVisible
    private static String defaultDistributionChannel = (String)UTIL_AppSettings.getValue('CPQSimulation.DefaultDistributionChannel', '');
    @testVisible
    private static String defaultDivision = (String)UTIL_AppSettings.getValue('CPQSimulation.DefaultDivision', '');
    @testVisible
    private static String defaultCustomerNumber = (String)UTIL_AppSettings.getValue('CPQSimulation.DefaultCustomerNumber', '');
    @testVisible
    private static String defaultSoldToPartnerFunction = (String)UTIL_AppSettings.getValue('CPQSimulation.DefaultSoldToPartnerFunction', '');
    @testVisible
    private static String defaultMaterialgroup1 = (String)UTIL_AppSettings.getValue('CPQSimulation.DefaultMaterialgroup1', '');
    @testVisible
    private static String defaultMaterialgroup2 = (String)UTIL_AppSettings.getValue('CPQSimulation.DefaultMaterialgroup2', '');
    @testVisible
    public static Integer QuoteLineIncrement {
        public get
        {
            if (null == QuoteLineIncrement)
            {
                QuoteLineIncrement = (Integer)UTIL_AppSettings.getValue('CPQSimulation.DefaultQuoteLineIncrement', 10);
            }
            if (null == QuoteLineIncrement || QuoteLineIncrement < 1)
            {
                QuoteLineIncrement = getItemIncrement(defaultSalesDocType);
            }
            return QuoteLineIncrement;
        }
        private set;
    }

    // Runs a pricing simulate for a specific material number (with or without variant config)
    // This is primarily used to pull back whether any additional items get added to the BoM when pricing a single material.
    public static SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing getSBOForVC_Config(
        string materialNumber,
        ENSX_VCPricingConfiguration pricingConfig, 
        List<DS_VCCharacteristicValues> vcConfig)
    {
        logger.enterAura('getSBOForVC_Config', new Map<String, Object> {
            'materialNumber' => materialNumber
            , 'pricingConfig' => pricingConfig
            , 'vcConfig' => vcConfig
        });
        SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing oppPricingDetail = new SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing();

        try
        {
            if (String.isBlank(materialNumber))
            {
                throw new ENSX_CPQ_Exceptions.SimulationException('A Material Number must be passed in for pricing simulation to occur.');
            }
            
            oppPricingDetail.SoldToParty = pricingConfig.soldToParty;

            oppPricingDetail.SALES.SalesDocumentType = pricingConfig.SalesDocumentType != null ? pricingConfig.SalesDocumentType : defaultSalesDocType;

            oppPricingDetail.SALES.SalesOrganization = pricingConfig.SalesOrganization != null ? pricingConfig.SalesOrganization : defaultSalesOrg;
            oppPricingDetail.SALES.DistributionChannel = pricingConfig.DistributionChannel != null ? pricingConfig.DistributionChannel : defaultDistributionChannel;
            oppPricingDetail.SALES.Division = pricingConfig.Division != null ? pricingConfig.Division : defaultDivision;

            SBO_EnosixOpportunityPricing_Detail.PARTNERS soldToPartner = new SBO_EnosixOpportunityPricing_Detail.PARTNERS();
            soldToPartner.PartnerFunction = defaultSoldToPartnerFunction;
            soldToPartner.CustomerNumber =  pricingConfig.soldToParty != null ? pricingConfig.soldToParty : defaultCustomerNumber;

            oppPricingDetail.PARTNERS.add(soldToPartner);

            if (String.isNotBlank(materialNumber))
            {
                SBO_EnosixOpportunityPricing_Detail.ITEMS itm = new SBO_EnosixOpportunityPricing_Detail.ITEMS();
                itm.ItemNumber = '10';
                itm.Material = materialNumber;
                itm.Plant = pricingConfig.Plant;
                itm.OrderQuantity = 1;

                itm.Materialgroup1 = defaultMaterialgroup1;
                itm.Materialgroup2 = defaultMaterialgroup2;

                oppPricingDetail.Items.add(itm);

                // Variant Configuration
                Integer vcTot = vcConfig.size();
                for (Integer vcCnt = 0 ; vcCnt < vcTot ; vcCnt++)
                {
                    DS_VCCharacteristicValues characteristic = vcConfig[vcCnt];
                    SBO_EnosixOpportunityPricing_Detail.ITEMS_CONFIG cfg = new SBO_EnosixOpportunityPricing_Detail.ITEMS_CONFIG();
                    cfg.ItemNumber = '10';
                    cfg.CharacteristicID = characteristic.CharacteristicID;
                    cfg.CharacteristicName = characteristic.CharacteristicName;
                    cfg.CharacteristicValue = characteristic.CharacteristicValue;
                    oppPricingDetail.ITEMS_CONFIG.add(cfg);
                }
            }

            oppPricingDetail = simulatePricing(oppPricingDetail);
            
        } catch (Exception ex) {
            logger.log(LoggingLevel.ERROR, 'There was an exception thrown when attempting to getSBOForVC_Config ', ex);
        } finally { 
            logger.exit();
        }

        return oppPricingDetail;
    }

    // getSBOForENSX_Quote
    //
    // Performs the SAP pricing simulation
    public static SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing getSBOForENSX_Quote(
        ENSX_Quote quote, Map<Integer, ENSX_QuoteLineMapping> preCalculateState, Map<Id, Product2> productMap)
    {
        logger.enterAura('getSBOForENSX_Quote', new Map<String, Object> {
            'quote' => quote
        });
        SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing oppPricingDetail = new SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing();
        try
        {
            string soldToParty = String.isNotBlank(quote.soldToParty) ? quote.soldToParty : defaultCustomerNumber;
            if (String.isBlank(soldToParty))
            {
                throw new ENSX_CPQ_Exceptions.SimulationException(System.Label.Account_Sold_To_Blank);
            }
            oppPricingDetail.SoldToParty = soldToParty;

            oppPricingDetail.SALES.SalesDocumentType = quote.salesDocType !=null ? quote.salesDocType: defaultSalesDocType;

            oppPricingDetail.SALES.SalesOrganization = quote.SalesOrg!=null?quote.SalesOrg: defaultSalesOrg;
            oppPricingDetail.SALES.DistributionChannel = quote.distChannel!= null?quote.distChannel:defaultDistributionChannel;
            oppPricingDetail.SALES.Division = quote.Division != null?quote.Division: defaultDivision;

            SBO_EnosixOpportunityPricing_Detail.PARTNERS soldToPartner = new SBO_EnosixOpportunityPricing_Detail.PARTNERS();
            soldToPartner.PartnerFunction = defaultSoldToPartnerFunction;
            soldToPartner.CustomerNumber = soldToParty;

            oppPricingDetail.PARTNERS.add(soldToPartner);

            Integer qlTot = quote.LinkedQuoteLines.size();
            Boolean hasConfigLines = false;
            for (Integer qlCnt = 0 ; qlCnt < qlTot ; qlCnt++)
            {
                ENSX_QuoteLine line = quote.LinkedQuoteLines[qlCnt];
                Product2 lineProduct = productMap.get(line.Product);
                if (UTIL_SFProduct.isProductLinkedToMaterial(lineProduct))
                {
                    SBO_EnosixOpportunityPricing_Detail.ITEMS itm = new SBO_EnosixOpportunityPricing_Detail.ITEMS();
                    itm.ItemNumber = String.valueOf(preCalculateState.get(line.LineItem).SAPLineItem);
                    itm.Material = UTIL_SFProduct.getMaterialNumberFromProduct(lineProduct);
                    itm.Plant = line.ItemConfiguration.plant;
                    itm.OrderQuantity = line.Quantity;
                    itm.Materialgroup1 = defaultMaterialgroup1;
                    itm.Materialgroup2 = defaultMaterialgroup2;
                    oppPricingDetail.Items.add(itm);

                    // Variant Configuration
                    if (null != line.ItemConfiguration && null != line.ItemConfiguration.selectedCharacteristics && !line.ItemConfiguration.selectedCharacteristics.isEmpty())
                    {
                        Integer scTot = line.ItemConfiguration.selectedCharacteristics.size();
                        for (Integer scCnt = 0 ; scCnt < scTot ; scCnt++)
                        {
                            hasConfigLines = true;
                            ENSX_Characteristic c = line.ItemConfiguration.selectedCharacteristics[scCnt];
                            SBO_EnosixOpportunityPricing_Detail.ITEMS_CONFIG cfg = new SBO_EnosixOpportunityPricing_Detail.ITEMS_CONFIG();
                            cfg.ItemNumber = itm.ItemNumber;
                            cfg.CharacteristicID = c.CharacteristicID;
                            cfg.CharacteristicName = c.CharacteristicName;
                            cfg.CharacteristicValue = c.CharacteristicValue;
                            oppPricingDetail.ITEMS_CONFIG.add(cfg);
                        }
                    }
                }
            }
            if (hasConfigLines && oppPricingDetail.SALES.SalesDocumentType == 'ZE') oppPricingDetail.SALES.SalesDocumentType = 'OR';
            
            oppPricingDetail = simulatePricing(oppPricingDetail);
            
        } catch (Exception ex) {
            logger.log(LoggingLevel.ERROR, 'There was an exception thrown when attempting to getSBOForENSX_Quote ', ex);
            throw ex;
        } finally { 
            logger.exit();
        }

        return oppPricingDetail;
    }

    /// Actually executes a pricing simulation for a a configured SBO.
    @testVisible
    private static SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing simulatePricing(SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing oppPricingDetail)
    {
        logger.enter('simulatePricing', new Map<String, Object> {
            'oppPricingDetail' => oppPricingDetail
        });

        SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing result = new SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing();

        try
        {            
            SBO_EnosixOpportunityPricing_Detail sbo = new SBO_EnosixOpportunityPricing_Detail();
            System.debug('about to run command against SAP');
            System.debug('Initial item counts:' + oppPricingDetail.ITEMS.size());
            result = sbo.command('CMD_SIMULATE_PRICING', oppPricingDetail);

            if (result.isSuccess())
            {
                System.debug('simulation was a success');
                System.debug(result.getMessages());
                System.debug('fetched ' + result.ITEMS.size() +' items from SAP');
            }
            else
            {
                System.debug('simulation failure');
                System.debug(result.getMessages());
                string exceptionMessage = '';
                integer messageSize = result.getMessages() == null ? 0 : result.getMessages().size();
                if (messageSize == 0 )
                {
                    exceptionMessage = 'Quote Calculation Failed';
                }
                else
                {
                    for (integer i = 0; i < messageSize; i++ ) {
                        exceptionMessage += result.getMessages()[i].Text;
                        if (i != messageSize - 1) exceptionMessage += ', ';
                    }
                }
                throw new ENSX_CPQ_Exceptions.SimulationException(exceptionMessage);
            }
        } catch (Exception ex) {
            logger.log(LoggingLevel.ERROR, 'There was an exception thrown when attempting to simulatePricing.', ex);
            throw ex;
        } finally { 
            logger.exit();
        }

        return result;
    }

    @testVisible
    private static RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT getOrderMasterData(string orderTypeKey)
    {
        List<RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT> otList = UTIL_RFC.getDocTypeMaster().ET_OUTPUT_List;
        Integer otTot = otList.size();
        for (Integer otCnt = 0 ; otCnt < otTot ; otCnt++)
        {
            RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT orderType = otList[otCnt];
            if (orderType.DocumentType == orderTypeKey) return orderType;
        }
        system.Debug('Was unable to locate Master Data matching key: ' + orderTypeKey);
        return null;
    }

    /// Gets the increment multiplier for each line item on opportunity pricing based upon the doc type
    /// This is all configured inside of SAP so needs to be pulled from there.
    private static Integer getItemIncrement(string docType)
    {
        //Default increment if nothing has been configured.
        Integer increment = 10;

        RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT orderMasterData = getOrderMasterData(docType);

        if (null != orderMasterData && string.isNotBlank(orderMasterData.INCPO))
        {
            Integer docIncrement = Integer.valueOf(orderMasterData.INCPO);
            if (docIncrement > 0)
            {
                increment = docIncrement;
            }
        }
        return increment;
    }
}