public class SFS_AssetDataIntegration {
    
    
    @Future(callout=true)
    public static void HttpRESTMethod(String interfaceDetail, Set<Id> asstIds, String DMLAction){
        //Http Method to send xml and recieve success or failure code.
        SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT Label,SFS_Endpoint_URL__c, SFS_Username__c, SFS_Password__c,SFS_ContentType__c
                                                      FROM SFS_SFS_Integration_Endpoint__mdt
                                                      WHERE Label =: interfaceDetail];
        
        //Encoding credentials for Authorization Header
        Blob headervalue = Blob.valueOf(endpoint.SFS_Username__c + ':' + endpoint.SFS_Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        String endpointUrl = ''; 
       // List<Asset> lstOfAssetRecordToUpdate = (List<Asset>)JSON.deserialize(stringOfAssetRecords, List<Asset>.class);
        List<Asset> AsslistToUpdate = new List<asset>();

        System.debug('authorizationHeader:'+authorizationHeader);
        
        //Create Asset JSON Data
        for(Asset ast: [SELECT Id,CPQ_Record_Id__c,CPQ_Part_Number__c,IRIT_Frame_Type__c,Asset_product_name__c,SFS_Rental_Division__r.Name, SFS_Oracle_Frame_Type__c, Name,Rental_Type__c,Area__c,Division__c,Account_Name__c,SFS_ShippingCountry__c,District__c,Branch__c,Service_Location__c,Primary_Account_Asset__c,SFS_ShippingStreet__c,SFS_ShippingCity__c,SFS_ShippingState__c,SFS_ShippingPostalCode__c,
                                           Ship_To_ID__c,Status,SFS_Serial_Number__c,Manufacturer__c,Model_Name__c,ProductDescription,Parent_Frame__c,Level_3__c,Compressor1_Type__c,Dryer_Type__c,Product_Line__c,SFS_Part_Number__c,CTS_Voltage__c,PSI__c,CTS_CFM_Rental__c,
                                           AMPS__c,Air_Dis_Inch_NPT__c,Cable_Wire_Size__c,Hose_Size_Inches__c,Length_in__c,Width_in__c,Height_in__c,Dimensions__c,No_Of_Hose_Kits__c,No_Of_Cable_Kits__c,Weekly_Rate__c,Monthly_Rate__c,IR_Rental_Contact__c,IR_Rental_Con_Phone__c,IR_Rental_Contact_Email__c,Outdoor_Mod__c,
                                           Quick_Connects__c,CableKits__c,Weight__c,Skid_Mount__c,Rental_Part_Number__c, HP__c,SFS_Account_Division__c,Operating_Status__c,CTS_CFM__c, Account.ShippingLongitude, Account.ShippingLatitude
                                           FROM asset WHERE Id in: asstIds]){
            system.debug('-> ' + ast);
            string ProductDescrptn='';
            if(DMLAction !='Delete')
            DMLAction = String.isBlank(ast.CPQ_Record_Id__c) ? 'create': 'update';
            
            string modelName = ast.Model_Name__c;
            String primaryAssetValue = 'No';
            if(modelName != null && !modelName.contains('\'')){
                if(modelName.contains('"')){
                    modelName = modelName.replaceAll('"','\\"');
                }}
            if(ast.ProductDescription != null){
                if(ast.ProductDescription.contains('"')){
                    ProductDescrptn = ast.ProductDescription.replaceAll('"','\"');
                }}
            
            if(ast.Primary_Account_Asset__c){
                primaryAssetValue = 'Yes';
            }
            
            string productLine = ast.Product_Line__c;
            
            string rentalPartNumberNew = (productLine != null ? 'RENTAL'+productLine.substring(0, 3) : null);
            String modlNo = ast.Model_Name__c == null?'':ast.Model_Name__c.replace('"', '\\"');
            decimal amps = ast.AMPS__c==null?0:ast.AMPS__c;
            String jsonResponse=        '{'+
                '  "documents": {'+
                '    "items": ['+
                '      {'+
                '        "id":"'+ast.CPQ_Record_Id__c+'",'+
                '        "Unique_ID": "'+ast.id+'",'+
                '        "Asset_ID": "'+ast.Name+'",'+
                '        "Serial_Number": "'+ast.SFS_Serial_Number__c+'",'+
                '        "Rental_Type": "'+ast.Rental_Type__c+'",'+
                '        "Area": "'+ast.Area__c +'",'+
                '        "Division": "'+ast.SFS_Account_Division__c +'",'+
                '        "Rental_Division": "'+ast.SFS_Rental_Division__r.Name+'",'+
                '        "Corp": "'+ast.Account_Name__c +'",'+
                '        "Account": "'+ast.Account_Name__c +'",'+
                '        "Country": "'+ast.SFS_ShippingCountry__c +'",'+
                '        "District": "'+ast.District__c +'",'+
                // '        "Branch": "'+ast.Branch__c +'",'+
                // '        "Service_Location": "'+ast.Service_Location__c +'",'+
                '        "Primary": "'+primaryAssetValue +'",'+
                '        "Street": "'+ast.SFS_ShippingStreet__c +'",'+
                '        "City": "'+ast.SFS_ShippingCity__c +'",'+
                '        "State": "'+ast.SFS_ShippingState__c +'",'+
                '        "Zip": "'+ast.SFS_ShippingPostalCode__c +'",'+
                // '        "Ship_To_ID": "'+ast.Ship_To_ID__c  +'",'+
                '        "Status": "'+ast.Operating_Status__c+'",'+
                // '        "Site_Use_ID": "'+ast.GES_Site__c+'",'+
                '        "Manufacturer": "'+ast.Manufacturer__c+'",'+
                //'        "Model": "'+modelName+'",'+
                '        "Product": "'+ast.Asset_product_name__c+'",'+
                '        "Product_Description": "'+ProductDescrptn+'",'+
                '        "Parent_Frame": "'+ast.Parent_Frame__c+'",'+
                '        "Frame_Type": "'+ast.IRIT_Frame_Type__c+'",'+
                // '        "Product_Type": "'+ast.Level_3__c+'",'+
                '        "Compressor_Type": "'+ast.Compressor1_Type__c+'",'+
                '        "Dryer_Type": "'+ast.Dryer_Type__c+'",'+
                '        "Product_Line": "'+ast.Product_Line__c+'",'+
                '        "SFS_Part_Number": "'+ast.SFS_Part_Number__c+'",'+
                '        "Part_Number": "'+ast.CPQ_Part_Number__c+'",'+
                '        "Part_Number_New": "'+rentalPartNumberNew+'",'+
                '        "HP": '+ast.HP__c+','+
                '        "Voltage": '+ast.CTS_Voltage__c+','+
                '        "PSI": '+ast.PSI__c+','+
                '        "CFM": '+ast.CTS_CFM__c+','+
                //'        "AMPS": '+ast.AMPS__c+','+
                '        "Air_Dis_Inch_NPT": '+ast.Air_Dis_Inch_NPT__c+','+
                '        "Cable_Wire_Size": "'+ast.Cable_Wire_Size__c+'",'+
                '        "Hose_Size_Inches": '+ast.Hose_Size_Inches__c+','+
                '        "Length": '+ast.Length_in__c+','+
                '        "Width": '+ast.Width_in__c+','+
                '        "Height": '+ast.Height_in__c+','+
                '        "No_Of_Hose_Kits": "'+ast.No_Of_Hose_Kits__c+'",'+
                '        "No_Of_Cable_Kits": "'+ast.No_Of_Cable_Kits__c+'",'+
                '        "Weekly_Rate": '+ast.Weekly_Rate__c+','+
                '        "Monthly_Rate": '+ast.Monthly_Rate__c+','+
                '        "IR_Rental_Contact": "'+ast.IR_Rental_Contact__c+'",'+
                '        "IR_Rental_Con_Phone": "'+ast.IR_Rental_Con_Phone__c+'",'+
                '        "IR_Rental_Con_Email": "'+ast.IR_Rental_Contact_Email__c+'",'+
                '        "Outdoor_Mod": "'+ast.Outdoor_Mod__c+'",'+
                '        "Quick_Connects": "'+ast.Quick_Connects__c+'",'+
                // '        "CableKits": "'+ast.CableKits__c+'",'+
                '        "Dimensions": "'+ast.Dimensions__c+'",'+
                '        "Weight": '+ast.Weight__c+','+
                '        "Skid_Mount":"'+ast.Skid_Mount__c+'",'+
                '        "Active":"Y",'+
                '        "Model":"'+modlNo+'",'+
                '        "AMPS":"'+amps+'",'+
                '        "Shipping_Latitude":"'+ ast.Account.ShippingLatitude +'",'+
                '        "Shipping_Longitude":"'+ ast.Account.ShippingLongitude +'",'+
                '        "sync_action": "'+DMLAction+'"'+
                '      }'+
                '    ]'+
                '  }'+
                '}';
               system.debug('122 jsonResponse-> ' + jsonResponse);
            AssetCPQFieldMapJsonClass obj = AssetCPQFieldMapJsonClass.parse(jsonResponse); 
            String JSONString = JSON.serialize(obj);
            system.debug('@@JSONString@@--->'+ JSONString);
            JSONString = JSONString.replaceAll('sync_action', '_sync_action');
            JSONString = JSONString.replaceAll('"null"', '" "');
            JSONString = JSONString.replaceAll('@@', ' ');
            JSONString = JSONString.replaceAll('##', '/');
            
            
            //Http Request
            if(endpoint.Label =='RentalSelectionOracleDataTable') endpointUrl = endpoint.SFS_Endpoint_URL__C;
            
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpointUrl);
            request.setMethod('POST');
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('Content-Type',endpoint.SFS_ContentType__c);
            request.setBody(JSON.serialize(JSON.deserializeUntyped(JSONString)));
            request.setTimeout(120000);
            
            //Get Http Response
          //  If(!Test.isRunningTest()){                
                HttpResponse response = http.send(request);
                System.debug('STATUS:======== ' + response.getStatusCode() + ' == RESPONSE BODY: ' +response.getBody());
                map<String,object> result =(map<String,object>)JSON.deserializeUntyped(response.getBody());
                String CPQRecordId,AssetId;
                Map<String, Object> getDocuments = (Map<String, Object>)result.get('documents');
                Map<String,object> ItemMap = new  Map<String,object>();
                Map<String , object>ItemsMap  = new Map<String , object>();
                Map<String , String> CPQAssetIdMap = new Map<String , String>();
                if(getDocuments !=null){
                    List<Object> ItemList = (List<Object>)getDocuments.get('items');
                    For(Object obj1 : ItemList){
                        ItemMap = (Map<String , Object>)obj1 ;
                    }
                }
                if(ItemMap !=null && !ItemMap.isEmpty()){
                    CPQRecordId = String.valueOf(ItemMap.get('id'));
                    AssetId= String.valueOf(ItemMap.get('Unique_ID'));
                    if(ast.CPQ_Record_Id__c == null || string.isEmpty(ast.CPQ_Record_Id__c)){
                        ast.CPQ_Record_Id__c = CPQRecordId;
                        AsslistToUpdate.add(ast) ;
                    }
                }
           // }   
            
        }
        if(AsslistToUpdate !=null && AsslistToUpdate.size()>0){
            shouldIRun.stopTrigger();
            update AsslistToUpdate;
        }
    } 
    
}