public class iConn_Data_Creation_Batch implements Database.Batchable<sObject>{
 
   public final String Query = 'SELECT Id, Cumulocity_Id__c, IMEI__c FROM Asset WHERE Cumulocity_Id__c != null AND IMEI__c != null AND iConn_Data__c = null AND Inactive__c = FALSE';
 
   public Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }
 
   public void execute(Database.BatchableContext BC, List<Asset> scope){
        for(Asset a : scope){
            iConn_Data__c iData = new iConn_Data__c();
            iData.IMEI__c = a.IMEI__c;
            insert iData;
            a.iConn_Data__c = iData.Id;
        }
        update scope;
    }
 
   public void finish(Database.BatchableContext BC){
       
   }
}