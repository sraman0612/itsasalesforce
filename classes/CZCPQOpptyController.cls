public class CZCPQOpptyController {

   Public final Opportunity opp;
   Public CZCPQOpptyController(ApexPages.StandardController controller) {
       this.opp = (Opportunity)controller.getRecord();
       }
 
   public ID getAccountId = System.currentPagereference().getParameters().get('id');
   public Flow.Interview.CZ_New_Quote flNewOpp{get;set;}
   public String getAccountId(){ return getAccountId; }
   public ID returnId = getAccountId;
    
   public PageReference getfinishLocation(){
        if(flNewOpp != null) returnId = (ID) flNewOpp.getVariablevalue('recordId');
        system.debug('returnId: '+ returnId);
        String sObjName = 'Opportunity';
       if (returnId != null) {
           sObjName = returnId.getSObjectType().getDescribe().getName();
       }
        // get the object type of the id in returnId     
        system.debug('sobjName '+ sObjName);
       // get the current URL and replace '--c' with '--sbqq' for proper url to quoteline editor
        String sfUrl=URL.getSalesforceBaseUrl().getHost().replace('--c','--sbqq');
        system.debug('sfURL: '+ sfURL);
       // check if returnId is a Quote, redirect to quote in quoteline editor, otherwise go to record     
        If(returnId != null && sobjName == 'SBQQ__Quote__c'){
          PageReference send = new PageReference('https://' + sfURL +'/apex/sb?scontrolCaching=1&id=' + returnId + '#/product/lookup?qId=' + returnId );
          send.setRedirect(true);  
          return send;
           } else{
               PageReference send = new PageReference('/' + returnId);
               return send;  
            }          
      }
    
}