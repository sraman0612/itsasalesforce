@isTest
private class TSTC_EnosixDeliverySearch {

    public class MockSBO_SFCIDelivery_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock
    {
        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) {

            SBO_SFCIDelivery_Detail sbo = new SBO_SFCIDelivery_Detail();
            SBO_SFCIDelivery_Detail.SFCIDelivery result = new SBO_SFCIDelivery_Detail.SFCIDelivery();
            List<SBO_SFCIDelivery_Detail.PARTNERS> partners = new List<SBO_SFCIDelivery_Detail.PARTNERS>();

            result.registerReflectionForClass();

            result.Delivery = '11114444';
            result.DeliveryDate = system.today();
            result.DeliveryStatus = 'Open';
            result.BillofLading = '065123471119419';

            SBO_SFCIDelivery_Detail.PARTNERS ptnr1 = new SBO_SFCIDelivery_Detail.PARTNERS();

            ptnr1.PartnerFunction = 'CR';
            ptnr1.PartnerFunctionName = 'Carrier';
            ptnr1.Vendor = '6000012';
            ptnr1.PartnerName = 'GARDNER DENVER CHOICE';

            result.PARTNERS.add(ptnr1);

            SBO_SFCIDelivery_Detail.PARTNERS ptnr2 = new SBO_SFCIDelivery_Detail.PARTNERS();

            ptnr2.PartnerFunction = 'SH';
            ptnr2.PartnerFunctionName = 'Ship-to Party';
            ptnr2.Vendor = '';
            ptnr2.PartnerName = 'Accurate Air Engineering Inc';

            result.PARTNERS.add(ptnr2);
            partners = result.PARTNERS.getAsList();

            return result;
        }
    }

    @isTest
    static void testDeliverySearchSuccess()
	{
		ensxsdk.EnosixFramework.setMock(SBO_SFCIDelivery_Detail.class, new MockSBO_SFCIDelivery_Detail());

        GDI_Delivery_Vendors__c vend = new GDI_Delivery_Vendors__c();
        vend.FLD_Tracking_URL_Label__c = 'Track Your Shipment';
        vend.FLD_Tracking_URL__c = 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=';
        vend.FLD_Vendor_Code__c = 'FEDEX';
        vend.FLD_Vendor_Description__c = 'Federal Express';
        vend.FLD_Vendor_Num__c = '6000012';
        vend.Name = 'FEDEX';
        insert vend;

        Test.startTest();
        UTIL_Aura.Response resp1 = CTRL_EnosixDeliverySearch.doSearch('11114444');
        system.debug('resp1 ===> '+resp1);
        Test.stopTest();
	}

    @isTest
    static void testDeliverySearchFailure()
	{
        UTIL_Aura.Response resp2 = CTRL_EnosixDeliverySearch.doSearch('');
    }

}