@isTest
public class CreateQuoteControllerTest {
    @IsTest
    public static void unitTest(){
        Trigger_Soft_Disable__c custSetting = new Trigger_Soft_Disable__c(Name = 'Account',disable_insert__c = false,
                                                                          disable_update__c = false,
                                                                          disable_delete__c = false,
                                                                          disable_undelete__c = false);
        insert custSetting;
        
        User usr1 = new User(LastName = 'LIVESTON',
                             FirstName='JASON',
                             Alias = 'jliv',
                             Email = 'jason.liveston@asdf.com',
                             Username = 'jason.liveston1234@qwerty.com',
                             ProfileId = UserInfo.getProfileId(),
                             TimeZoneSidKey = 'GMT',
                             LanguageLocaleKey = 'en_US',
                             EmailEncodingKey = 'UTF-8',
                             LocaleSidKey = 'en_US'
                            );
        insert usr1;
        
       Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'Site_Account_NA_Air' 
                             and SobjectType='Account' limit 1].Id;
        
        List<Account> acclist = new List<Account>();
        Account acc1 = new Account();
        acc1.Name = 'srs_2001';
        acc1.BillingCity = 'rsr_!city2';
        acc1.BillingCountry = 'USA';
        acc1.BillingPostalCode = '1234557858898';
        acc1.BillingState = 'CA';
        acc1.BillingStreet = '123, rsr_!!street2';
        acc1.ShippingCity = 'city3';
        acc1.ShippingCountry  = 'United States';
        acc1.ShippingState = 'CA';
        acc1.ShippingStreet = '132, street2';
        acc1.ShippingPostalCode = '1234';
        acc1.County__c = 'testCounty2';
        acc1.RecordTypeId = AirNAAcctRT_ID; 
        acc1.AccountSource='Data.com';
        acc1.IRIT_Dealer_Approved_By_text__c=usr1.id;
        acc1.CG_Org_Channel__c = 'IR';
        acc1.WasConverted__c = false;
        acc1.Oracle_Number__c = '12345';
        acc1.Quote_Type__c = 'Service';
        acc1.Service_Quote_Type__c = 'Diagnostic';
        acclist.add(acc1);
        insert acclist; 
        System.debug('acclist[0]:'+acclist[0]);
        
        List<Contact> Conlist = new List<Contact>();
        Contact con = new Contact();
        con.Lastname = 'Test';
        con.FirstName = 'Demo';
        con.Title = 'BABA';
        con.Phone = '12323';
        con.Email = 'abc@gmail.com';
        con.Primary__c=true;
        con.AccountId = acclist[0].ID;
        Conlist.add(con);
        insert Conlist;
        System.debug('Conlist[0].Id:'+ Conlist[0].Id);
        
        
        Opportunity Opp = new opportunity();
        Opp.name = 'Anu Test';
        opp.StageName = 'Closed Lost';
        opp.CloseDate = Date.today();
        opp.OwnerId = usr1.Id;
        String oppOwnerID = usr1.Id;
        insert opp;
        ID oppId = opp.ID;
        ID conID = Conlist[0].Id;
        String Name = opp.Name;
        
        
        
        List<cafsl__Oracle_User__c> oracleUserList = [SELECT Id, cafsl__User__c, cafsl__Embedded_CPQ_Settings__c, 
                                                      cafsl__Allow_Quote_Creation__c FROM cafsl__Oracle_User__c ];
        if(oracleUserList.size()>0){ 
            cafsl__Oracle_User__c cafsl = new cafsl__Oracle_User__c();
            cafsl.id = oracleUserList[0].id;
            cafsl.cafsl__User__c = usr1.Id;
            update cafsl;
            Boolean  allowQuoteCreation= cafsl.cafsl__Allow_Quote_Creation__c;
            String siteSettingsId = cafsl.cafsl__Embedded_CPQ_Settings__c;
        }
        
        Asset ast = new Asset();
        ast.Name = 'Test Asset';
        String assetserialNumber = ast.Name;
        insert ast;
        
        
        Test.startTest();
        CreateQuoteController.getAccountDetails(acclist[0].Id); //10
        CreateQuoteController.getRelatedBillToAccounts(acclist[0].Oracle_Number__c);
        CreateQuoteController.deleteOpportunityUponPartialSave(oppId);
        CreateQuoteController.getRelatedAssetList(acclist[0].Id);
        CreateQuoteController.getQuoteType();
        CreateQuoteController.getWorkQuoteType();
        CreateQuoteController.getType1();
        CreateQuoteController.getStatus1();
        CreateQuoteController.getPrimaryContactDetails(acclist[0].Id); 
        CreateQuoteController.saveAccount(acclist[0], oppOwnerID, assetserialNumber, Conlist[0].Id, acclist[0].Id );
        Test.stopTest();
        
    }
    @IsTest (SeeAllData=true)
    public static  void unitTest1(){
        Trigger_Soft_Disable__c custSetting = new Trigger_Soft_Disable__c(Name = 'Account',disable_insert__c = false,
                                                                          disable_update__c = false,
                                                                          disable_delete__c = false,
                                                                          disable_undelete__c = false);
        insert custSetting;
        
        User usr1 = new User(LastName = 'LIVESTON',
                             FirstName='JASON',
                             Alias = 'jliv',
                             Email = 'jason.liveston@asdf.com',
                             Username = 'jason.liveston1234@qwerty.com',
                             ProfileId = UserInfo.getProfileId(),
                             TimeZoneSidKey = 'GMT',
                             LanguageLocaleKey = 'en_US',
                             EmailEncodingKey = 'UTF-8',
                             LocaleSidKey = 'en_US'
                            );
        insert usr1;
        
       Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'Site_Account_NA_Air' 
                             and SobjectType='Account' limit 1].Id;
        
        List<Account> acclist = new List<Account>();
        Account acc1 = new Account();
        acc1.Name = 'srs_2';
        acc1.BillingCity = 'rsr_!city2';
        acc1.BillingCountry = 'USA';
        acc1.BillingPostalCode = '1234557858898';
        acc1.BillingState = 'CA';
        acc1.BillingStreet = '123, rsr_!!street2';
        acc1.ShippingCity = 'city3';
        acc1.ShippingCountry  = 'United States';
        acc1.ShippingState = 'CA';
        acc1.ShippingStreet = '132, street2';
        acc1.ShippingPostalCode = '1234';
        acc1.County__c = 'testCounty2';
        acc1.RecordTypeId = AirNAAcctRT_ID; 
        acc1.AccountSource='Data.com';
        acc1.IRIT_Dealer_Approved_By_text__c=usr1.id;
        acc1.CG_Org_Channel__c = 'IR';
        acc1.WasConverted__c = false;
        acc1.Oracle_Number__c = '12345';
        acc1.Quote_Type__c = 'Service';
        acc1.Service_Quote_Type__c = 'Diagnostic';
        acclist.add(acc1);
        insert acclist; 

        
        List<Contact> Conlist = new List<Contact>();
        Contact con = new Contact();
        con.FirstName = 'Demo';
        con.Lastname = 'Test';
        con.Title = 'BABA';
        con.Phone = '12323';
        con.Email = 'abc@gmail.com';
        con.Primary__c=true;
        con.AccountId = acclist[0].ID;
        Conlist.add(con);
        insert Conlist;
        
        
        Opportunity Opp = new opportunity();
        Opp.name = 'Anu Test';
        opp.StageName = 'Closed Lost';
        opp.CloseDate = Date.today();
        opp.OwnerId = usr1.ID;
        String oppOwnerID = usr1.ID;
        insert opp;
        ID oppId = opp.ID;
        ID conID = Conlist[0].Id;
        String Name = opp.Name;
        String currentUserId = UserInfo.getUserId();
        List<cafsl__Oracle_User__c> oracleUserList = [SELECT Id, cafsl__User__c, cafsl__Embedded_CPQ_Settings__c,
                                                      cafsl__Allow_Quote_Creation__c FROM cafsl__Oracle_User__c
                                                       ];
        System.debug('size of the oracleUserList is--->'+oracleUserList);
        if(oracleUserList.size()>0){
            cafsl__Oracle_User__c cafsl = new cafsl__Oracle_User__c();
            cafsl.id = oracleUserList[0].id;
            cafsl.cafsl__User__c = usr1.Id;
            update cafsl;
            Boolean allowQuoteCreation= cafsl.cafsl__Allow_Quote_Creation__c;
            String siteSettingsId = cafsl.cafsl__Embedded_CPQ_Settings__c;
        }
        
        Asset ast = new Asset();
        ast.Name = 'Test Asset';
        insert ast;
        
        
        Test.startTest();
            CreateQuoteController.genQuote(acclist[0].Id, oppOwnerID, oppId);
        Test.StopTest();
        
    }
    @IsTest
    public static  void getQuoteTypeBySFSServiceTechnicianProfileTest() {
        Id profileId = [SELECT Id,Name FROM Profile WHERE Name=:'SFS Service Technician'].Id;
        User usr1 = new User(LastName = 'LIVESTON',
                FirstName='JASON',
                Alias = 'jliv',
                Email = 'jason.liveston@asdf.com',
                Username = 'jason.liveston1234@qwerty.com',
                ProfileId = profileId,
                TimeZoneSidKey = 'GMT',
                LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US'
        );
        insert usr1;
        Map<String, String> quoteTypes  = new Map<String, String>();
        System.runAs(usr1){
            Test.StartTest();
                quoteTypes = CreateQuoteController.getQuoteType();
            Test.StopTest();
        }
        System.assertNotEquals(null,quoteTypes);
    }
    @IsTest
    public static  void getQuoteTypeByIRCompSalesProfileTest() {
        Id profileId = [SELECT Id,Name FROM Profile WHERE Name=:'IR Comp Sales'].Id;
        User usr1 = new User(LastName = 'LIVESTON',
                FirstName='JASON',
                Alias = 'jliv',
                Email = 'jason.liveston@asdf.com',
                Username = 'jason.liveston1234@qwerty.com',
                ProfileId = profileId,
                TimeZoneSidKey = 'GMT',
                LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US'
        );
        insert usr1;
        Map<String, String> quoteTypes  = new Map<String, String>();
        System.runAs(usr1){
            Test.StartTest();
            quoteTypes = CreateQuoteController.getQuoteType();
            Test.StopTest();
        }
        System.assertNotEquals(null,quoteTypes);
    }
    @IsTest
    public static  void getQuoteTypeByRandomProfileTest() {
        Id profileId = [SELECT Id,Name FROM Profile WHERE Name=:'IR Comp TechSupport'].Id;
        User usr1 = new User(LastName = 'LIVESTON',
                FirstName='JASON',
                Alias = 'jliv',
                Email = 'jason.liveston@asdf.com',
                Username = 'jason.liveston1234@qwerty.com',
                ProfileId = profileId,
                TimeZoneSidKey = 'GMT',
                LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US'
        );
        insert usr1;
        Map<String, String> quoteTypes  = new Map<String, String>();
        System.runAs(usr1){
            Test.StartTest();
            quoteTypes = CreateQuoteController.getQuoteType();
            Test.StopTest();
        }
        System.assertNotEquals(null,quoteTypes);
    }
}