/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Logger {
    global Logger(System.Type t) {

    }
    global void debug(Object message) {

    }
    global void debug(Object message, Object detail) {

    }
    global void enterAura(String the_method, Map<String,Object> the_params) {

    }
    global void enterQueueable(String the_method, Map<String,Object> the_params) {

    }
    global void enterVfpAction(String the_method, Map<String,Object> the_params) {

    }
    global void enterVfpAjax(String the_method, Map<String,Object> the_params) {

    }
    global void enterVfpConstructor(String the_method, Map<String,Object> the_params) {

    }
    global void enterVfpProperty(String the_method, Map<String,Object> the_params) {

    }
    global void enter(String the_method, Map<String,Object> the_params) {

    }
    global void error(Object message) {

    }
    global void error(Object message, Object detail) {

    }
    global void exit() {

    }
    global void fine(Object message) {

    }
    global void fine(Object message, Object detail) {

    }
    global void finer(Object message) {

    }
    global void finer(Object message, Object detail) {

    }
    global void finest(Object message) {

    }
    global void finest(Object message, Object detail) {

    }
    global void info(Object message) {

    }
    global void info(Object message, Object detail) {

    }
    global void internal(Object message) {

    }
    global void internal(Object message, Object detail) {

    }
    global void log(System.LoggingLevel level, Object message, Object detail) {

    }
    global void warn(Object message) {

    }
    global void warn(Object message, Object detail) {

    }
}
