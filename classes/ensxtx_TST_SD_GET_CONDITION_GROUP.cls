/// enosiX Inc. Generated Apex Model
/// Generated On: 8/5/2020 5:50:36 PM
/// SAP Host: From REST Service On: https://platform-force-5128-dev-ed.cs95.my.salesforce.com
/// CID: From REST Service On: https://platform-force-5128-dev-ed.cs95.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
public with sharing class ensxtx_TST_SD_GET_CONDITION_GROUP
{
    public class Mockensxtx_RFC_SD_GET_CONDITION_GROUP implements ensxsdk.EnosixFramework.RFCMock
    {
        public ensxsdk.EnosixFramework.FunctionObject executeFunction() 
        {
            return null;
        }
    }

    @isTest
    static void testExecute()
    {
        ensxsdk.EnosixFramework.setMock(ensxtx_RFC_SD_GET_CONDITION_GROUP.class, new Mockensxtx_RFC_SD_GET_CONDITION_GROUP());
        ensxtx_RFC_SD_GET_CONDITION_GROUP rfc = new ensxtx_RFC_SD_GET_CONDITION_GROUP();
        ensxtx_RFC_SD_GET_CONDITION_GROUP.RESULT params = rfc.PARAMS;
        System.assertEquals(null, rfc.execute());
    }

    @isTest
    static void testRESULT()
    {
        ensxtx_RFC_SD_GET_CONDITION_GROUP.RESULT funcObj = new ensxtx_RFC_SD_GET_CONDITION_GROUP.RESULT();

        funcObj.registerReflectionForClass();

        System.assertEquals(ensxtx_RFC_SD_GET_CONDITION_GROUP.RESULT.class, funcObj.getType(), 'getType() does not match object type.');

        //Check all the collections
        funcObj.getCollection(ensxtx_RFC_SD_GET_CONDITION_GROUP.ET_COND_GROUP.class).add(new ensxtx_RFC_SD_GET_CONDITION_GROUP.ET_COND_GROUP());
        System.assertEquals(1,funcObj.ET_COND_GROUP_List.size());

    }

    @isTest
    static void testET_COND_GROUP()
    {
        ensxtx_RFC_SD_GET_CONDITION_GROUP.ET_COND_GROUP funcObj = new ensxtx_RFC_SD_GET_CONDITION_GROUP.ET_COND_GROUP();

        funcObj.registerReflectionForClass();

        System.assertEquals(ensxtx_RFC_SD_GET_CONDITION_GROUP.ET_COND_GROUP.class, funcObj.getType(), 'getType() does not match object type.');
        funcObj.KDKGR = 'X';
        System.assertEquals('X', funcObj.KDKGR);

        funcObj.VTEXT = 'X';
        System.assertEquals('X', funcObj.VTEXT);

    }
}