@isTest
public class TSTE_VCPricingConfiguration
{
    @isTest static void testClassVariables ()
    {
        Test.startTest();
        ENSX_VCPricingConfiguration ensxVcPricingConfguration = new ENSX_VCPricingConfiguration();
        ensxVcPricingConfguration.ConfigDate = 'ConfigDate';
        ensxVcPricingConfguration.SalesDocumentType = 'SalesDocumentType';
        ensxVcPricingConfguration.SalesOrganization = 'SalesOrganization';
        ensxVcPricingConfguration.DistributionChannel = 'DistributionChannel';
        ensxVcPricingConfguration.Division = 'Division';
        ensxVcPricingConfguration.SoldToParty = 'SoldToParty';
        ensxVcPricingConfguration.Plant = 'Plant';
        ensxVcPricingConfguration.SalesDocumentCurrency = 'SalesDocumentCurrency';
        ensxVcPricingConfguration.OrderQuantity = 0;
        ensxVcPricingConfguration.ObjectKey = 'ObjectKey';
        Test.stopTest();
    }
}