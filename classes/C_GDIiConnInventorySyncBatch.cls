global class C_GDIiConnInventorySyncBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {

    global final String query;
    
    global C_GDIiConnInventorySyncBatch(String q){
        
        if(!string.isBlank(q)){
            query=q;
           }
         else{
             //iConn_c8y_H_total__c,iConn_c8y_H_load__c,iConn_c8y_H_service__c,iConn_c8y_H_stal_1__c
                query ='SELECT id, name,IMEI__c,Cumulocity_ID__c FROM Asset WHERE IMEI__c!=NULL';
            }
            
    }
    global Database.QueryLocator start(Database.BatchableContext bacthC){
        
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext batchC,List<Asset>scope){
        
        list<id> AstID = new list<id>();
        for(Asset a : scope){
            AstID.add(a.id);
        }
        C_GDIiConnInventorySync.iConnInventorySync(AstID);
    }
    
    global void finish(Database.BatchableContext batchC){
        
    }
}