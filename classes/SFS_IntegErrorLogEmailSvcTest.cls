@isTest
public class SFS_IntegErrorLogEmailSvcTest {

    @isTest
    static void testEmail() {
        String csvString = '';
        csvString += '"GUID","PARTNER","CURRENT_PHASE","ENTITY_ID","ENTITY_NAME","ERROR_DATE","ERROR_MESSAGE","ERROR_STATUS","DEBUG_STEP"\n';
        csvString += '"01FAABAC3FC77204E0640021280E8E4F","SFS-NA","CLOSED","3-D FARMS-73707","CUSTOMER_NUMBER-RELATED_CUSTOMER_NUMBER","02-AUG-23 08.15.12","External Id: more than one record found for external id field: [aA2DT000000D5WF0A0, aA2DT000000D4TV0A0]","NEW","CPI Failure"\n';
        csvString += '"01FAABAC3FC77204E0640021280E8E4F","SFS-NA","CLOSED","BIG 5 SPORTING GOODS-73707","CUSTOMER_NUMBER-RELATED_CUSTOMER_NUMBER","02-AUG-23 08.15.12","Record rolled back because not all records were valid and the request was using AllOrNone header","NEW","CPI Failure"\n';
        csvString += '"01FAABAC3FC77204E0640021280E8E4F","SFS-NA","CLOSED","LOVE YOU A LATTE%-73707","CUSTOMER_NUMBER-RELATED_CUSTOMER_NUMBER","02-AUG-23 08.15.12","Record rolled back because not all records were valid and the request was using AllOrNone header","NEW","CPI Failure"\n';

        Messaging.InboundEmail.BinaryAttachment att = new Messaging.InboundEmail.BinaryAttachment();
        att.fileName = 'XXONT1451_SFS_NA_ERRORS_211735718_20230802.csv';
        att.mimeTypeSubType = 'text/csv';
        att.body = Blob.valueOf(csvString);

        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        env.fromAddress = 'test@test.com';

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.subject = 'ISOAQA XXONT1451 SFS-NA Errors';
        email.plainTextBody = 'Document(s) attached OR if no file is present, no data was found';
        email.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[] { att };

        SFS_IntegErrorLogEmailSvc emailSvc = new SFS_IntegErrorLogEmailSvc();
        emailSvc.handleInboundEmail(email, env);

        SFS_Integration_Log__c[] log = [SELECT Id FROM SFS_Integration_Log__c];
        System.assert(log.size() == 3, 'Three logs should have been created');
    }
}