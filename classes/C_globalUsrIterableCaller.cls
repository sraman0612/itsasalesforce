global class C_globalUsrIterableCaller implements iterable<assetShare>{
    List<assetShare> ashRecs = new List<assetShare>();
    public C_globalUsrIterableCaller(List<assetShare> asRecs){
        ashRecs = asRecs;
    }
   global Iterator<assetShare> Iterator(){
      return new C_createAssetShareGlobalUsrIterable(ashRecs);
   }
}