public with sharing class AssignLead {

    @AuraEnabled public static List<Account> fetchOptions(Id leadId){

        System.assertNotEquals(null, leadId);
        Lead theLead = [select FLD_Customer_Numbers__c from Lead where Id = :leadId][0];

        List<String> accountNumbers = new List<String>();

        if(theLead.FLD_Customer_Numbers__c != null){
            accountNumbers.addAll(theLead.FLD_Customer_Numbers__c.split(','));
        }

        return [
            SELECT  Name, AccountNumber, BillingCity
            FROM    Account
            WHERE   AccountNumber IN :accountNumbers
                AND AccountNumber != NULL
            	AND Account_Type__c = 'Parent'
        ];
    }

    @AuraEnabled public static void assignLead(String distAccId, Id leadId){

        System.assertNotEquals(null, distAccId);
        System.assertNotEquals(null, leadId);

        Lead theLead = [
            SELECT  Assigned_Distributor__c, Assigned_to_Distributor_Date__c
            FROM    Lead
            WHERE   Id = :leadId
        ][0];

        if(theLead.Assigned_Distributor__c == NULL || (theLead.Assigned_Distributor__c != null && theLead.Assigned_Distributor__c != distAccId)){

            theLead.Assigned_Distributor__c = distAccId;
            theLead.Assigned_to_Distributor_Date__c = System.today();
            update theLead;
        }
    }
}