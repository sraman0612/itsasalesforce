@isTest
public class CreateMaintenancePlanAssetTest {
    private static testMethod void createMaintenancePlanTest(){        
        List<List<MaintenancePlan>> mpLists = new List<List<MaintenancePlan>>();
        List<Account> acc = SFS_TestDataFactory.createAccounts(1, true);
        List<ServiceContract> sa = SFS_TestDataFactory.createServiceAgreement(1,acc[0].Id,true);
        List<MaintenancePlan> mpList = SFS_TestDataFactory.createMaintenancePlan(1,sa[0].Id,true);
        List<Asset> ast = SFS_TestDataFactory.createAssets(1,true);
        List<MaintenanceAsset> ma = SFS_TestDataFactory.createMaintenanceAsset(1,mpList[0].Id,ast[0].Id,false);
        ma[0].SFS_Service_Agreement__c = sa[0].Id;
        insert ma;
        List<ServiceContract> sa2 = SFS_TestDataFactory.createServiceAgreement(1,acc[0].Id,false); 
        sa2[0].ParentServiceContractId = sa[0].Id;
        insert sa2[0];
        List<MaintenancePlan> mpList2 = SFS_TestDataFactory.createMaintenancePlan(1,sa2[0].Id,false); 
        mpList2[0].Parent_Service_Agreement__c = sa[0].Id;
        mpList2[0].Clone_From__c = mpList[0].Id;
        insert mpList2;
        List<MaintenanceAsset> ma2 = SFS_TestDataFactory.createMaintenanceAsset(1,mpList2[0].Id,ast[0].Id,false);
        ma2[0].SFS_Service_Agreement__c = sa2[0].Id;
        ma2[0].Clone_From__c = ma[0].Id;
        //insert ma2;
        
        mpLists.add(mpList2);
        Test.startTest();  
        CreateMaintenancePlanAsset.createMaintenancePlan(mpLists);        
        Test.stopTest(); 
    }   
}