public without sharing class ShareMyInventory_Ctrl {

    @AuraEnabled public static Account doInit(Account testAccount){

        User currentUser = [Select AccountId from User where Id = :UserInfo.getUserId()];

        String accountId = currentUser.AccountId;

        if(testAccount != null){
            accountId = testAccount.Id;
        }

        return [
            SELECT  Share_Assets__c
            FROM    Account
            WHERE   Id = :accountId
        ];
    }

    @AuraEnabled public static Account updateAccount(Account account){
        update account;
        return account;
    }
}