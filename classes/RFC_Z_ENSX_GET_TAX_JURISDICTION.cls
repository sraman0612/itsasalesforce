/// enosiX Inc. Generated Apex Model
/// Generated On: 9/15/2020 12:31:34 PM
/// SAP Host: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev1.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class RFC_Z_ENSX_GET_TAX_JURISDICTION extends ensxsdk.EnosixFramework.RFC
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('RFC_Z_ENSX_GET_TAX_JURISDICTION_Meta', new Type[] {
            RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT.class
            , RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS.class
            } 
        );
    }

    public RFC_Z_ENSX_GET_TAX_JURISDICTION()
    {
        super('Z_ENSX_GET_TAX_JURISDICTION', RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT.class);
    }

    public override Type getType() { return RFC_Z_ENSX_GET_TAX_JURISDICTION.class; }

    public RESULT PARAMS
    {
        get { return (RESULT)this.getParameterContext(); }
    }

    public RESULT execute()
    {
        return (RESULT)this.executeFunction();
    }
    
    public class RESULT extends ensxsdk.EnosixFramework.FunctionObject
    {    	
        public RESULT()
        {
            super(new Map<string,type>
            {
                'ET_LOCATION_RESULTS' => RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS.class
            });	
        }
        
        public override Type getType() { return RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT.class; }

        public override void registerReflectionForClass()
        {
            RFC_Z_ENSX_GET_TAX_JURISDICTION.registerReflectionInfo();
        }

        @AuraEnabled public List<ET_LOCATION_RESULTS> ET_LOCATION_RESULTS_List
    {
        get 
        {
            List<ET_LOCATION_RESULTS> results = new List<ET_LOCATION_RESULTS>();
            this.getCollection(RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS.class).copyTo(results);
            return results;
        }
    }
        @AuraEnabled public String IV_CITY
        { 
            get { return this.getString ('IV_CITY'); } 
            set { this.Set (value, 'IV_CITY'); }
        }

        @AuraEnabled public String IV_COUNTRY
        { 
            get { return this.getString ('IV_COUNTRY'); } 
            set { this.Set (value, 'IV_COUNTRY'); }
        }

        @AuraEnabled public String IV_COUNTY
        { 
            get { return this.getString ('IV_COUNTY'); } 
            set { this.Set (value, 'IV_COUNTY'); }
        }

        @AuraEnabled public String IV_REGION
        { 
            get { return this.getString ('IV_REGION'); } 
            set { this.Set (value, 'IV_REGION'); }
        }

        @AuraEnabled public String IV_ZIPCODE
        { 
            get { return this.getString ('IV_ZIPCODE'); } 
            set { this.Set (value, 'IV_ZIPCODE'); }
        }

    	
    }
    public class ET_LOCATION_RESULTS extends ensxsdk.EnosixFramework.ValueObject
    {
        public ET_LOCATION_RESULTS()
        {
            super('ET_LOCATION_RESULTS', new Map<string,type>());
        }

        public override Type getType() { return RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS.class; }

        public override void registerReflectionForClass()
        {
            RFC_Z_ENSX_GET_TAX_JURISDICTION.registerReflectionInfo();
        }

                    @AuraEnabled public String Country
        { 
            get { return this.getString ('COUNTRY'); } 
            set { this.Set (value, 'COUNTRY'); }
        }

        @AuraEnabled public String STATE
        { 
            get { return this.getString ('STATE'); } 
            set { this.Set (value, 'STATE'); }
        }

        @AuraEnabled public String COUNTY
        { 
            get { return this.getString ('COUNTY'); } 
            set { this.Set (value, 'COUNTY'); }
        }

        @AuraEnabled public String CITY
        { 
            get { return this.getString ('CITY'); } 
            set { this.Set (value, 'CITY'); }
        }

        @AuraEnabled public String ZIPCODE
        { 
            get { return this.getString ('ZIPCODE'); } 
            set { this.Set (value, 'ZIPCODE'); }
        }

        @AuraEnabled public String TXJCD_L1
        { 
            get { return this.getString ('TXJCD_L1'); } 
            set { this.Set (value, 'TXJCD_L1'); }
        }

        @AuraEnabled public String TXJCD_L2
        { 
            get { return this.getString ('TXJCD_L2'); } 
            set { this.Set (value, 'TXJCD_L2'); }
        }

        @AuraEnabled public String TXJCD_L3
        { 
            get { return this.getString ('TXJCD_L3'); } 
            set { this.Set (value, 'TXJCD_L3'); }
        }

        @AuraEnabled public String TXJCD_L4
        { 
            get { return this.getString ('TXJCD_L4'); } 
            set { this.Set (value, 'TXJCD_L4'); }
        }

        @AuraEnabled public String TXJCD
        { 
            get { return this.getString ('TXJCD'); } 
            set { this.Set (value, 'TXJCD'); }
        }

        @AuraEnabled public String OUTOF_CITY
        { 
            get { return this.getString ('OUTOF_CITY'); } 
            set { this.Set (value, 'OUTOF_CITY'); }
        }

            
        }
}