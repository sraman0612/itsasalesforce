@isTest
public class ensxtx_TSTE_VCConfiguration
{
    @isTest static void testGetConfigurationFromSBOModel ()
    {
        Test.startTest();
        ensxtx_SBO_EnosixVC_Detail.EnosixVC enosixVc = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
        enosixVc.Material = 'Material';
        ensxtx_ENSX_VCConfiguration ensxVcConfiguration = ensxtx_ENSX_VCConfiguration.getConfigurationFromSBOModel(enosixVc);
        System.assert(ensxVcConfiguration.MaterialId == 'Material');
        Test.stopTest();
    }

    @isTest static void testGetSBOModelFromConfig ()
    {
        Test.startTest();
        ensxtx_ENSX_VCConfiguration ensxVcConfiguration = new ensxtx_ENSX_VCConfiguration();
        ensxtx_ENSX_VCCharacteristicValues ensxVcCharacteristicValue = new ensxtx_ENSX_VCCharacteristicValues();
        ensxVcConfiguration.SelectedValues = new List<ensxtx_ENSX_VCCharacteristicValues>();
        ensxVcConfiguration.SelectedValues.add(ensxVcCharacteristicValue);
        ensxtx_ENSX_VCCharacteristic ensxVcCharacteristic = new ensxtx_ENSX_VCCharacteristic();
        ensxVcCharacteristic.PossibleValues = new List<ensxtx_ENSX_VCCharacteristicValues>();
        ensxVcCharacteristic.PossibleValues.add(ensxVcCharacteristicValue);
        ensxVcConfiguration.Characteristics = new List<ensxtx_ENSX_VCCharacteristic>();
        ensxVcConfiguration.Characteristics.add(ensxVcCharacteristic);
        ensxVcConfiguration.PricingConfiguration = new ensxtx_ENSX_VCPricingConfiguration();
        ensxVcConfiguration.MaterialId = 'MaterialId';
        ensxVcConfiguration.ConfigurationIsValid = true;
        ensxtx_SBO_EnosixVC_Detail.EnosixVC enosixVc = ensxtx_ENSX_VCConfiguration.getSBOModelFromConfig(ensxVcConfiguration, true);
        System.assert(enosixVc.Material == 'MaterialId');
        Test.stopTest();
    }

    @isTest static void testGetCharacteristicsFromSBOModel ()
    {
        Test.startTest();
        ensxtx_SBO_EnosixVC_Detail.EnosixVC enosixVc = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
        ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS characteristic = new ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS();
        enosixVc.CHARACTERISTICS.add(characteristic);
        List<ensxtx_ENSX_VCCharacteristic> testList = ensxtx_ENSX_VCConfiguration.getCharacteristicsFromSBOModel(enosixVc);
        System.assert(testList.size() == 1);
        Test.stopTest();
    }

    @isTest static void testGetSelectedValuesFromSBOModel ()
    {
        Test.startTest();
        ensxtx_SBO_EnosixVC_Detail.EnosixVC enosixVc = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
        ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES selectedValue = new ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES();
        enosixVc.SELECTEDVALUES.add(selectedValue);
        List<ensxtx_ENSX_VCCharacteristicValues> testList = ensxtx_ENSX_VCConfiguration.getSelectedValuesFromSBOModel(enosixVc);
        System.assert(testList.size() == 1);
        Test.stopTest();
    }
}