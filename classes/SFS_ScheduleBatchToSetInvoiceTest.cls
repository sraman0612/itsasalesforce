/*=========================================================================================================
* @author Arati Yadav
* @date 12/10/2021
* @description:TestClass for SFS_ScheduleBatchToSetInvoiceTest and SFS_ScheduleBatchToSetStatus
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------


=======================================================================================================*/


@IsTest
public class SFS_ScheduleBatchToSetInvoiceTest {
  
    @istest  public static void setStatusToSubmittTest1() {
      
       	ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        Id rtSCId = [Select Id, Name, SObjectType FROM RecordType where DeveloperName ='SFS_Rental' AND SObjectType = 'ServiceContract'].Id;
       	ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Service_Invoice'].id;
       	ID NaAirProdRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Product2' AND DeveloperName = 'NA_Air'].id;
        String sA_RECORDTYPEID = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('PackageCARE').getRecordTypeId();
        
       	List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].AccountSource='Web1';
        accountList[0].IRIT_Customer_Number__c='1234';
        accountList[0].RecordTypeId = rtAcc;
       	//insert accountList[0]; 
        
       	accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].AccountSource='Web';
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Type = 'Ship To';
       	//insert accountList[1];
        
        ID testStandardPBId = Test.getStandardPricebookId();

        Product2 naProd = new Product2 (Name = 'TestBatchProduct', IsActive = TRUE, RecordTypeId = NaAirProdRecordTypeId, ProductCode = 'TEST');
        insert naProd;
         	
        PriceBookEntry naPBE = new PriceBookEntry(PriceBook2Id = testStandardPBId, Product2Id = naProd.Id, UnitPrice = 1, IsActive = TRUE);
        insert naPBE;
        
        ServiceContract sc = new ServiceContract(AccountId=accountList[1].Id,SFS_Agreement_Value__c=78,SFS_Invoice_Format__c='Detail',CurrencyIsoCode = 'USD',
                                                SFS_Bill_To_Account__c=accountList[1].Id,StartDate=System.today(),EndDate=System.today().addYears(2),
                                                SFS_Type__c='Rental',recordTypeId=rtSCId,SFS_Invoice_Type__c='Prepaid',
                                                Name = '1234',SFS_PO_Number__c='123',SFS_PO_Expiration__c=system.today(),SFS_Portable_Required__c='Yes',
                                                SFS_Status__c='APPROVED',SFS_Special_Note__c = false,SFS_Invoice_Submission__c = 'Automatic',SFS_Invoice_Frequency__c='Weekly',SFS_Integration_Status__c='APPROVED',SFS_Special_Invoice_Note__c='',SFS_Requested_Payment_Terms__c = 'NET 30');
        insert sc;
        
       ServiceContract sc1 = new ServiceContract(AccountId=accountList[1].Id,SFS_Agreement_Value__c=78,SFS_Invoice_Format__c='Detail',CurrencyIsoCode = 'USD',
                                                SFS_Bill_To_Account__c=accountList[1].Id,StartDate=System.today(),EndDate=System.today().addYears(2),
                                                SFS_Type__c='PackageCARE',recordTypeId=sA_RECORDTYPEID,SFS_Invoice_Type__c='Prepaid',
                                                Name = '1234',SFS_PO_Number__c='123',SFS_PO_Expiration__c=system.today(),SFS_Portable_Required__c='Yes',
                                                SFS_Status__c='APPROVED',SFS_Special_Note__c = false,SFS_Invoice_Submission__c = 'Automatic',SFS_Invoice_Frequency__c='Annually',SFS_Integration_Status__c='APPROVED',SFS_Special_Invoice_Note__c='',SFS_Requested_Payment_Terms__c = 'NET 30');
        insert sc1;
        
        Invoice__c inv = new Invoice__c(recordTypeId = invRecTypeId,
                                       SFS_Service_Agreement__c=sc.Id,
                                       SFS_Status__c='Created',
									  // SFS_Work_Order__c='',
                                       SFS_Invoice_Format__c=sc.SFS_Invoice_Format__c,
                                       SFS_Billing_Period_End_Date__c=system.today(), 
                                       SFS_Billing_Period_Start_Date__c= system.today()-6,
                                       SFS_Charge_Count__c = 1);
        insert inv;
        Invoice__c inv1 = new Invoice__c(recordTypeId = invRecTypeId,
                                       SFS_Service_Agreement__c=sc1.Id,
                                       SFS_Status__c='Created',
									   //SFS_Work_Order__c='',
                                       SFS_Invoice_Format__c=sc.SFS_Invoice_Format__c,
                                       SFS_Billing_Period_End_Date__c=system.today(), 
                                       SFS_Billing_Period_Start_Date__c= system.today()-6,
                                        SFS_Charge_Count__c = 1);
         insert inv1;
                
        
        
         test.startTest();
         String cronTimeINV= '0 0 22 1/1 * ? *';
         SFS_ScheduleBatchToSetInvoice sch1 = new SFS_ScheduleBatchToSetInvoice();
         String InvjobId = system.schedule('setInvoiceStatusTest', cronTimeINV, sch1); 
         test.stopTest();
         system.assertEquals(true,sc.SFS_Invoice_Frequency__c!=null);
           
    }
   /*  @istest  public static void setStatusToSubmittTest2() {
      
       	ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
         Id rtSCId = [Select Id, Name, SObjectType FROM RecordType where DeveloperName ='SFS_Rental' AND SObjectType = 'ServiceContract'].Id;
       	ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Service_Invoice'].id;
       	ID NaAirProdRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Product2' AND DeveloperName = 'NA_Air'].id;
        
       	List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
         accountList[0].AccountSource='Web3';
         accountList[0].IRIT_Customer_Number__c='12354';
         accountList[0].RecordTypeId = rtAcc;
       	//insert accountList[0]; 
        
       	accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].AccountSource='Web';
         accountList[1].IRIT_Customer_Number__c='123';
         accountList[1].Type = 'Ship To';
       	//insert accountList[1];
        
        ID testStandardPBId = Test.getStandardPricebookId();

        Product2 naProd = new Product2 (Name = 'TestBatchProduct', IsActive = TRUE, RecordTypeId = NaAirProdRecordTypeId, ProductCode = 'TEST');
        insert naProd;
         	
        PriceBookEntry naPBE = new PriceBookEntry(PriceBook2Id = testStandardPBId, Product2Id = naProd.Id, UnitPrice = 1, IsActive = TRUE);
        insert naPBE;
        
        ServiceContract sc = new ServiceContract(AccountId=accountList[1].Id,SFS_Agreement_Value__c=78,SFS_Invoice_Format__c='Detail',CurrencyIsoCode = 'USD',
                                                SFS_Bill_To_Account__c=accountList[1].Id,StartDate=System.today(),EndDate=System.today().addYears(2),
                                                SFS_Type__c='Rental',recordTypeId=rtSCId,SFS_Invoice_Type__c='Prepaid',
                                                Name = '1234',SFS_PO_Number__c='123',SFS_PO_Expiration__c=system.today(),SFS_Portable_Required__c='Yes',
                                                SFS_Status__c='APPROVED',SFS_Special_Note__c = false,SFS_Invoice_Submission__c = 'Automatic',SFS_Invoice_Frequency__c='Weekly',SFS_Integration_Status__c='APPROVED',SFS_Special_Invoice_Note__c='',SFS_Requested_Payment_Terms__c = 'BANKCARD');
        insert sc;
        
       ServiceContract sc1 = new ServiceContract(AccountId=accountList[1].Id,SFS_Agreement_Value__c=78,SFS_Invoice_Format__c='Detail',CurrencyIsoCode = 'USD',
                                                SFS_Bill_To_Account__c=accountList[1].Id,StartDate=System.today(),EndDate=System.today().addYears(2),
                                                SFS_Type__c='PackageCARE',recordTypeId=rtSCId,SFS_Invoice_Type__c='Prepaid',
                                                Name = '1234',SFS_PO_Number__c='123',SFS_PO_Expiration__c=system.today(),SFS_Portable_Required__c='Yes',
                                                SFS_Status__c='APPROVED',SFS_Special_Note__c = false,SFS_Invoice_Submission__c = 'Automatic',SFS_Invoice_Frequency__c='Anually',SFS_Integration_Status__c='APPROVED',SFS_Special_Invoice_Note__c='',SFS_Requested_Payment_Terms__c = 'BANKCARD');
        insert sc1;
        
        Invoice__c inv = new Invoice__c(recordTypeId = invRecTypeId,
                                       SFS_Service_Agreement__c=sc.Id,
                                       SFS_Status__c='Created',
									  // SFS_Work_Order__c='',
                                       SFS_Invoice_Format__c=sc.SFS_Invoice_Format__c,
                                       SFS_Billing_Period_End_Date__c=system.today(), 
                                       SFS_Billing_Period_Start_Date__c= system.today()-6,
                                       SFS_Charge_Count__c = 1);
        insert inv;
        Invoice__c inv1 = new Invoice__c(recordTypeId = invRecTypeId,
                                       SFS_Service_Agreement__c=sc1.Id,
                                       SFS_Status__c='Created',
									   //SFS_Work_Order__c='',
                                       SFS_Invoice_Format__c=sc.SFS_Invoice_Format__c,
                                       SFS_Billing_Period_End_Date__c=system.today(), 
                                       SFS_Billing_Period_Start_Date__c= system.today()-6,
                                        SFS_Charge_Count__c = 1);
         insert inv1;
        
        
         test.startTest();
         String cronTimeINV= '0 0 22 1/1 * ? *';
         SFS_ScheduleBatchToSetInvoice sch1 = new SFS_ScheduleBatchToSetInvoice();
         String InvjobId = system.schedule('setInvoiceStatusTest', cronTimeINV, sch1);
         System.abortJob(InvjobId);
         test.stopTest();
         system.assertEquals(true,sc.SFS_Invoice_Frequency__c!=null);
           
    }
*/
}