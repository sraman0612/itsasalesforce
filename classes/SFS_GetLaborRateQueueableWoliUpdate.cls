public class SFS_GetLaborRateQueueableWoliUpdate implements Queueable{

public List<ID> requestIDs;
    public SFS_GetLaborRateQueueableWoliUpdate(List<Id> requestIDs){
        this.requestIDs = requestIDs ;  
    }
    public void execute(QueueableContext context) {  
      SFS_Get_LaborRate_Interface.GetLaborRate(requestIDs);
    }
}