@isTest
public with sharing class TSTU_MaterialSyncBatch
{
    static final string TEST_JSON =
        '{"UTIL_MaterialSyncBatch.SalesOrg": "GDMI",' +
        '"UTIL_MaterialSyncBatch.DChainBlocks": ["87","03","80","88","A3","EX","MR","S0","S1","S2","S3","S4","S5","S7","S8","ZU","OE"],' +
        '"UTIL_MaterialSyncBatch.DistributionChannels": ["CM","CP","ER","SB","DI","GI","DT","GT","FH","MV","WT","DV","MK"],' +
        '"UTIL_MaterialSyncBatch.MaterialTypes":["FERT","ZERT"],' +
        '"UTIL_MaterialSyncBatch.MaterialGroups":["A","*"],' +
        '"UTIL_MaterialSyncBatch.Divisions":["01","02"],' +
        '"UTIL_MaterialSyncBatch.Logging": true,' +
        '"UTIL_MaterialSyncBatch.ExcludedDistributionChannels": ["00", "CU", "CV", "CX", "QT"]}';

    public class MockSyncSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        private boolean success = true;

        public void setSuccess(boolean success)
        {
            this.success = success;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext)
        {
            if (this.throwException)
            {
                throw new UTIL_SyncHelper.SyncException('');
            }

            SBO_EnosixMatSync_Search.EnosixMatSync_SR searchResult =
                new SBO_EnosixMatSync_Search.EnosixMatSync_SR();

            // New Account
            SBO_EnosixMatSync_Search.SEARCHRESULT result1 =
                new SBO_EnosixMatSync_Search.SEARCHRESULT();

            result1.DeleteIndicator = 'X';
            result1.ConfigurableMaterialFlag = 'X';
            result1.MaterialType = 'MaterialType';
            result1.MaterialTypeDescription = 'MaterialTypeDescription';
            result1.Division = 'Division';
            result1.DivisionDescription = 'DivisionDescription';
            result1.MaterialGroup = 'MaterialGroup';
            result1.MaterialGroupDescription = 'MaterialGroupDescription';
            result1.Material = 'Material1';
            result1.MaterialDescription = 'MaterialDescription';
            result1.ProductAttribute6 = 'X';
            result1.BaseUnitOfMeasure = 'BaseUnitOfMeasure';
            result1.SalesUnit = 'SalesUnit';
            result1.SalesOrganization = 'SalesOrganization';
            result1.DistributionChannel = 'CM';
            result1.DistChainStatus = 'DistChainStatus';
            result1.WeightUnit = 'WeightUnit';
            result1.DistChainStatusDate = System.today();

            searchResult.SearchResults.add(result1);

            // Existing Account
            SBO_EnosixMatSync_Search.SEARCHRESULT result2 =
                new SBO_EnosixMatSync_Search.SEARCHRESULT();

            result2.ConfigurableMaterialFlag = 'X';
            result2.MaterialType = 'MaterialType';
            result2.MaterialTypeDescription = 'MaterialTypeDescription';
            result2.Division = 'Division';
            result2.DivisionDescription = 'DivisionDescription';
            result2.MaterialGroup = 'MaterialGroup';
            result2.MaterialGroup4 = '224';
            result2.MaterialGroupDescription = 'MaterialGroupDescription';
            result2.Material = 'Material2';
            result2.MaterialDescription = 'MaterialDescription';
            result2.BaseUnitOfMeasure = 'BaseUnitOfMeasure';
            result2.SalesUnit = 'SalesUnit';
            result2.SalesOrganization = 'SalesOrganization';
            result2.DistributionChannel = 'CM';
            result2.DistChainStatus = 'DistChainStatus';
            result2.WeightUnit = 'WeightUnit';
            result2.DistChainStatusDate = System.today();

            searchResult.SearchResults.add(result2);

            SBO_EnosixMatSync_Search.SEARCHRESULT result3 =
                new SBO_EnosixMatSync_Search.SEARCHRESULT();

            result3.DeleteIndicator = 'X';
            result3.ConfigurableMaterialFlag = 'X';
            result3.MaterialType = 'MaterialType';
            result3.MaterialTypeDescription = 'MaterialTypeDescription';
            result3.Division = 'Division';
            result3.DivisionDescription = 'DivisionDescription';
            result3.MaterialGroup = 'MaterialGroup';
            result3.MaterialGroup4 = 'C50';
            result3.MaterialGroupDescription = 'MaterialGroupDescription';
            result3.Material = 'Material3';
            result3.MaterialDescription = 'MaterialDescription';
            result3.ProductAttribute6 = 'X';
            result3.BaseUnitOfMeasure = 'BaseUnitOfMeasure';
            result3.SalesUnit = 'SalesUnit';
            result3.SalesOrganization = 'SalesOrganization';
            result3.DistributionChannel = 'DT';
            result3.DistChainStatus = 'DistChainStatus';
            result3.WeightUnit = 'WeightUnit';
            result3.DistChainStatusDate = System.today();

            searchResult.SearchResults.add(result3);

            SBO_EnosixMatSync_Search.SEARCHRESULT result4 =
                new SBO_EnosixMatSync_Search.SEARCHRESULT();

            result4.MaterialType = 'MaterialType';
            result4.MaterialTypeDescription = 'MaterialTypeDescription';
            result4.Division = 'Division';
            result4.DivisionDescription = 'DivisionDescription';
            result4.MaterialGroup = 'MaterialGroup';
            result2.MaterialGroup4 = 'E31';
            result4.MaterialGroupDescription = 'MaterialGroupDescription';
            result4.Material = 'Material4';
            result4.MaterialDescription = 'MaterialDescription';
            result4.ProductAttribute6 = 'X';
            result4.BaseUnitOfMeasure = 'BaseUnitOfMeasure';
            result4.SalesUnit = 'SalesUnit';
            result4.SalesOrganization = 'SalesOrganization';
            result4.DistributionChannel = 'DT';
            result4.DistChainStatus = 'DistChainStatus';
            result4.WeightUnit = 'WeightUnit';
            result4.DistChainStatusDate = System.today();

            searchResult.SearchResults.add(result4);

            searchResult.setSuccess(this.success);
            searchContext.baseResult = searchResult;
            return searchContext;
        }
    }

    public static testMethod void test_MaterialSync()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMatSync_Search.class, new MockSyncSearch());

        UTIL_AppSettings.resourceJson = TEST_JSON;
        createExistingObject();
        Test.startTest();
        Product2 prod1 = new Product2();
        prod1.Name = 'Name1';
        prod1.ProductCode = 'Material1';
        prod1.FLD_Distribution_Channel__c = 'CM';
        prod1.SAP_Sync_Field__c = 'Material1 - CM';
        insert prod1;
        Product2 prod2 = new Product2();
        prod2.Name = 'Name2';
        prod2.ProductCode = 'Material3';
        prod2.FLD_Distribution_Channel__c = 'DT';
        prod2.SAP_Sync_Field__c = 'Material3 - DT';
        insert prod2;
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = UTIL_Pricebook.getStandardPriceBookId();
        pbe.Product2Id = prod1.Id;
        pbe.UnitPrice = 0;
        pbe.isActive = false;
        insert pbe;
        PricebookEntry pbe2 = new PricebookEntry();
        pbe2.Pricebook2Id = UTIL_Pricebook.getStandardPriceBookId();
        pbe2.Product2Id = prod2.Id;
        pbe2.UnitPrice = 0;
        pbe2.isActive = true;
        insert pbe2;
        PricebookEntry pbe3 = new PricebookEntry();
        pbe3.Pricebook2Id = [SELECT Id FROM Pricebook2 WHERE Name = 'OEM Pricebook' AND IsActive = true LIMIT 1].Id;
        pbe3.Product2Id = prod1.Id;
        pbe3.UnitPrice = 0;
        pbe3.UseStandardPrice = false;
        pbe3.isActive = false;
        insert pbe3;
        PricebookEntry pbe4 = new PricebookEntry();
        pbe4.Pricebook2Id = [SELECT Id FROM Pricebook2 WHERE Name = 'OEM Pricebook' AND IsActive = true LIMIT 1].Id;
        pbe4.Product2Id = prod2.Id;
        pbe4.UnitPrice = 0;
        pbe4.UseStandardPrice = true;
        pbe4.isActive = true;
        insert pbe4;
        UTIL_MaterialSyncBatch controller = new UTIL_MaterialSyncBatch();
        UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
        lastSync.retryCnt = 1;
        controller.setBatchParam(lastSync);
        Database.executeBatch(controller);
        Test.stopTest();
    }

    public static testMethod void test_MaterialSyncFailure()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMatSync_Search.class, mockSyncSearch);
        mockSyncSearch.setSuccess(false);

        createExistingObject();
        Test.startTest();
        UTIL_MaterialSyncBatch controller = new UTIL_MaterialSyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
        }
        catch (Exception e) {}
        Test.stopTest();
    }

    public static testMethod void test_MaterialSyncException()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMatSync_Search.class, mockSyncSearch);
        mockSyncSearch.setThrowException(true);

        createExistingObject();
        Test.startTest();
        UTIL_MaterialSyncBatch controller = new UTIL_MaterialSyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
            controller.start(null);
        }
        catch (Exception e) {}
        Test.stopTest();
    }

    private static void createExistingObject()
    {
        MPG4_Code__c currCode = new MPG4_Code__c();
        currCode.MPG4_Number__c = '224';
        currCode.Name = '224 EnviroAire Scroll';
        currCode.Description__c = 'EnviroAire Scroll';
        insert currCode;

        MPG4_Code__c currCode2 = new MPG4_Code__c();
        currCode2.MPG4_Number__c = 'C50';
        currCode2.Name = 'C50 Champion Climate Control Simplex';
        currCode2.Description__c = 'Champion Climate Control Simplex';
        insert currCode2;

        MPG4_Code__c currCode3 = new MPG4_Code__c();
        currCode3.MPG4_Number__c = 'E31';
        currCode3.Name = 'E31 V-Series VLV';
        currCode3.Description__c = 'V-Series VLV';
        insert currCode3;

        MPG4_Code__c currCode4 = new MPG4_Code__c();
        currCode4.MPG4_Number__c = 'E65';
        currCode4.Name = 'E65 S - Series Chem Pumps';
        currCode4.Description__c = 'S - Series Chem Pumps';
        insert currCode4;

        Product2 currentObject = new Product2();
        currentObject.Name = 'Material1';
        currentObject.MPG4_Code__c = currCode.Id;
        currentObject.put(UTIL_MaterialSyncBatch.SFSyncKeyField,'Material1 - CM');
        insert currentObject;

        Product2 currentObject3 = new Product2();
        currentObject3.Name = 'Material3';
        currentObject3.MPG4_Code__c = currCode3.Id;
        currentObject3.put(UTIL_MaterialSyncBatch.SFSyncKeyField,'Material3 - DT');
        insert currentObject3;

        OBJ_SAP_Sync__c sapSync = new OBJ_SAP_Sync__c();
        sapSync.Name = 'UTIL_MaterialSyncSchedule';
        sapSync.FLD_Sync_DateTime__c = System.today().addDays(-1);
        sapSync.FLD_Page_Number__c = 0;
        upsert sapSync;

        Pricebook2 oemPB = new Pricebook2();
        oemPB.Name = 'OEM Pricebook';
        oemPB.isActive = true;
        insert oemPB;
    }
}