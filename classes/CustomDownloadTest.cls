@isTest
public class CustomDownloadTest {

    @isTest
    public static void attTest() {
        
        Account acc = new Account(Name='test1',Phone='56766238',Industry='Agriculture',Type='Applicator');
        insert acc;
        Contact co = new Contact(LastName='test',FirstName='test',Email='test2@gdi.com',Phone='56766238',Accountid=acc.id);
        insert co;
        Case c = new Case(Accountid=acc.id,Contactid=co.id,Type='Problem',Origin='Phone',Status='New',Subject='test');
        insert c;
        Attachment att = new Attachment(Body=Blob.valueOf(''), ContentType='png', parentid=c.id,Name='test');
        insert att;

        test.startTest();
            CustomDownload.loadAttachment(att.id);
        test.stopTest();

    }

    @isTest
    public static void fileTest() {

        ContentVersion contVer1 = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contVer1;

        ContentVersion contVer2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contVer1.Id LIMIT 1];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        test.startTest();
            CustomDownload.loadAttachment(documents[0].Id);
        test.stopTest();

    }
}