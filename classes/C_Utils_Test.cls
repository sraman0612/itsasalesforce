/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class C_Utils_Test {


    @testSetup
    static void setupData(){
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        insert acct;
        
        Contact cont = new Contact();
        cont.firstName = 'Can';
        cont.lastName = ' Pango';
        cont.email = 'service-gdi@canpango.com';
        cont.Account = acct;
        cont.AccountId = acct.Id;
        insert cont;
        
        Case cse = new Case();
        cse.Account = acct;
        cse.Contact = cont;
        cse.AccountId = acct.Id;
        cse.ContactId = cont.Id;
        cse.Subject = 'Test Case';
        insert cse;
        
        Attachment att = new Attachment();
        att.Name = 'Attachment Test';
        att.ParentId = cse.Id;
        att.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att.ContentType = 'image/jpg';
        insert att;
        
        Email_Field_Map__c efm = new Email_Field_Map__c();
        efm.Name = 'Test Email_Field_Map__c';
        insert efm;

    }

    static testMethod void testsaveCase() {       
        Case c = [SELECT Id FROM Case];
        C_Utils.saveCase(c.Id);
    }
    
    static testMethod void testduplicateAttachments(){
        Map<Id, Attachment> attmap = new Map<Id,Attachment>([SELECT Id, Name FROM Attachment]);
        List<Id> idList  = new List<Id>();
        idList.addAll(attMap.keySet());
        C_Utils.duplicateAttachments(idList);
    }
    
    static testmethod void testsendTemplatedEmail(){
        Case c = [SELECT Id FROM Case];
        C_Utils.sendTemplatedEmail(new List<Id>{c.Id});
    }
    
    static testMethod void testUpdateParent() {       
        Case c = [SELECT Id,ContactId, AccountId,Status FROM Case];
                
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='test@testorg.com1', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@testorg.com1');        
        
        System.runAs(u) { 
        
            
                Case cse = new Case();             
                cse.Subject = 'Test Case';
                cse.Send_to_User__c =u.id;
                cse.ParentId = c.id;
                insert cse;        
                
                Case newCase = [select status,parentId from Case where Id =: cse.Id];
                Case pendingResponseCase = [select Status from Case where Id =:newCase.ParentId];
                
                //when new case is created, default to new
                System.assertEquals('New',newCase.Status);     
                          
                Case cse1 = new Case();
                cse1.id = cse.id;
                cse1.AccountId= c.AccountId;
                cse1.Status='Closed'; 
                cse1.GDI_Department__c='Customer Service';
                cse1.Level_2__c='Compressor - CS';
                cse1.Brand__c ='Champion - CS';
                cse1.Inquiry_Call_Type_List__c ='test';
                cse1.Assigned_From_Address_Picklist__c='champion.cs.qcy@gardnerdenver.com';
                update cse1;
                
                Case updatedCase = [select status from Case where Id=:newCase.Parentid];
               // When all related child cases are closed, change parent case to open
               // System.assertEquals('Open',updatedCase.Status);
           
        }
    }    
    
    static testmethod void testCaseLoopTrigger(){
        Account acct = [SELECT Id FROM Account WHERE Name = 'Canpango Test Account'];
        Contact cont = [SELECT Id, Email FROM Contact WHERE Email = 'service-gdi@canpango.com'];
        RecordType rt = [select Id,Name from RecordType where SobjectType='Case' and DeveloperName='Customer_Care' Limit 1];
        
        for(integer i = 0; i < 5; i++){
            System.debug('i : ' + i);
            Case cse = new Case();
            cse.Account = acct;
            cse.Contact = cont;
            cse.AccountId = acct.Id;
            cse.ContactId = cont.Id;
            cse.SuppliedEmail = cont.Email;
            cse.Subject = 'Test Case';
            cse.Description = 'Test Case Desc';
            cse.RecordTypeId = rt.Id;
            insert cse;
            System.debug(cse);
        }
        
    }
}