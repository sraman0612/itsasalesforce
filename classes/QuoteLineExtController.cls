public with sharing class QuoteLineExtController {
    private Id quoteLineId;
    public QuoteLineExtController(ApexPages.StandardController stdController) {
        quoteLineId = stdController.getId();
    }
    public PageReference onSubmit() {
        if (quoteLineId != null) {
            SBAA.ApprovalAPI.submit(quoteLineId, SBAA__Approval__c.Quote_Line__c);
        }
        return new PageReference('/' + quoteLineId);
    }
    public PageReference onRecall() {
        if (quoteLineId != null) {
            SBAA.ApprovalAPI.recall(quoteLineId, SBAA__Approval__c.Quote_Line__c);
        }
        return new PageReference('/' + quoteLineId);
    }
}