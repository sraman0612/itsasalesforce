/*
Paste the below code into the Anonymous Apex Window
To know how many total records are there to sync
========== START CODE ==========
new ensxsdk.Logger(null);

SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC searchContext = new SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC();
searchContext.pagingOptions.pageSize = 1;
searchContext.pagingOptions.pageNumber = 1;
searchContext.SEARCHPARAMS.DateFrom = Date.parse('01/01/2000');
SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE notifType = new SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE();
notifType.NotificationType = 'Z1';
searchContext.NOTIF_TYPE.add(notifType);

SBO_EnosixServiceNotificationSync_Search sbo = new SBO_EnosixServiceNotificationSync_Search();
sbo.search(searchContext);
System.debug(searchContext.result.isSuccess());
System.debug(searchContext.pagingOptions.totalRecords);
========== END CODE ==========
*/
public with sharing class UTIL_WarrantyClaimSyncBatch
    implements Database.Batchable<Object>,
    Database.AllowsCallouts,
    Database.Stateful,
    I_ParameterizedSync
{
    @TestVisible
    private static String SFSyncKeyField = 'SAP_Service_Notification_Number__c';
    @TestVisible
    private static String BatchClassName = 'UTIL_WarrantyClaimSyncBatch';
    private static String ScheduleClassName = 'UTIL_WarrantyClaimSyncSchedule';

    private Set<String> SAPServiceNotificationNumbers = new Set<String>();
    private Map<String,Warranty_Claim__c> currentWarrantyClaimsMap = new Map<String,Warranty_Claim__c>();

    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_WarrantyClaimSyncBatch.class);
    public void logCallouts(String location)
    {
        if ((Boolean)UTIL_AppSettings.getValue(BatchClassName + '.Logging', false))
        {
            logger.enterVfpConstructor(location, null);
        }
    }

    // jobInfo contains debug information persisted across contexts since start(),
    // execute(), and finish() all run in separate contexts with separate debug logs
    private List<String> jobInfo = new List<String>();

    // In this case, we will store the largest change date/time as the param
    private UTIL_SyncHelper.LastSync fromLastSync = new UTIL_SyncHelper.LastSync();
    private static String ObjectType = 'Warranty_Claim__c';


    /* I_ParameterizedSync methods - setBatchParam() */
    public void setBatchParam(Object value)
    {
        this.fromLastSync = (UTIL_SyncHelper.LastSync) value;
    }
    /* end I_ParameterizedSync methods */

    // Sync Filter collections
    // Get the filters from the AppSettings

    // End Sync filter collections

    /* Database.Batchable methods start(), execute(), and finish() */
    // start()
    //
    // Calls SBO and returns search results of update materials
    public List<Object> start(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.start');
        System.debug(context.getJobId() + ' started');

        SBO_EnosixServiceNotificationSync_Search sbo = new SBO_EnosixServiceNotificationSync_Search();
        SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC searchContext = new SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SC();

        SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE notifType = new SBO_EnosixServiceNotificationSync_Search.NOTIF_TYPE();
        notifType.NotificationType = 'Z1';
        searchContext.NOTIF_TYPE.add(notifType);

        this.fromLastSync = UTIL_SyncHelper.getLastSyncFromTable(
            ScheduleClassName,
            this.fromLastSync);

        this.fromLastSync.pageNumber = this.fromLastSync.pageNumber + 1;

        if (this.fromLastSync.retryCnt == -1)
        {
            UTIL_SyncHelper.resetPage(this.fromLastSync, (Integer) UTIL_AppSettings.getValue(
                BatchClassName + '.SAPPageSize',
                9999));
        }
        if (this.fromLastSync.lastSyncDate != null)
        {
            searchContext.SEARCHPARAMS.DateFrom = this.fromLastSync.lastSyncDate;
        }
        if (this.fromLastSync.lastSyncDate == null)
        {
            searchContext.SEARCHPARAMS.DateFrom = Date.parse('01/01/2018');
        }

        searchContext.pagingOptions.pageSize = this.fromLastSync.pageSize;
        searchContext.pagingOptions.pageNumber = this.fromLastSync.pageNumber;
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());
        System.debug('fromLastSync:' + this.fromLastSync.toString());


        // Execute the search
        SBO_EnosixServiceNotificationSync_Search.EnosixServiceNotificationSync_SR result;
        try
        {
            sbo.search(searchContext);
            result = searchContext.result;
        }
        catch (Exception ex)
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, ex, this.jobInfo);
        }

        // Write any response messages to the debug log
        String errorMessage = UTIL_SyncHelper.buildErrorMessage(BatchClassName, result.getMessages());

        if (!result.isSuccess())
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, new UTIL_SyncHelper.SyncException(errorMessage), this.jobInfo);
        }

        List<Object> searchResults = result.getResults();
        System.debug('Result size: ' + searchResults.size());

        // let finish() know to queue up another instance
        this.fromLastSync.isAnotherBatchNeeded = searchResults.size() > 0;
        this.fromLastSync.retryCnt = -1;

        this.jobInfo.add('searchResultsSize:' + searchResults.size());
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());

        return searchResults;
    }

    // execute()
    //
    // Given the updated search results, does the work of updating the object table.
    public void execute(
        Database.BatchableContext context,
        List<Object> searchResults)
    {
        logCallouts(BatchClassName + '.execute');
        System.debug(context.getJobId() + ' executing');

        if (null == searchResults || 0 == searchResults.size()) return;

        List<SObject> errors = new List<SObject>();
        Map<String, Object> searchResultMap = createObjectKeyMap(searchResults);

        // Get all the ServiceNotification numbers from the searchResults
        for (Object searchResult : searchResults)
        {
            String key = getSboKey(searchResult);
            if (String.isNotEmpty(key))
            {
                SAPServiceNotificationNumbers.add(key);
            }
        }
        // Load currentWarrantyClaimsMap collection with existing records
        for (Warranty_Claim__c claim : [SELECT Id, SAP_Service_Notification_Number__c, SAP_Sales_Order_Number__c FROM Warranty_Claim__c WHERE SAP_Service_Notification_Number__c IN : SAPServiceNotificationNumbers]) {
            currentWarrantyClaimsMap.put(claim.SAP_Service_Notification_Number__c, claim);
        }

        // First, update matching existing objects
        List<SObject> currentObjectList = UTIL_SyncHelper.getCurrentObjects(ObjectType, SFSyncKeyField, searchResultMap.keySet());
        List<SObject> updateObjectList = updateExistingObjects(searchResultMap, currentObjectList, errors);
        Set<Id> savedIdSet = new Set<Id>();

        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Update', errors, savedIdSet, updateObjectList, BatchClassName, SFSyncKeyField);

        UTIL_SyncHelper.insertUpdateResults('Error', 'Insert', errors, savedIdSet, errors, BatchClassName, null);
    }

    // finish()
    //
    // queues up another batch when isAnotherBatchNeeded is true
    public void finish(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.finish');
        System.debug(context.getJobId() + ' finished');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);
        if (this.fromLastSync.retryCnt >= 0)
        {
            System.debug('Retry=' + this.fromLastSync.retryCnt + ' ' + System.Now());
        }

        UTIL_SyncFailureNotifier.syncFinished(context);

        UTIL_SyncHelper.launchAnotherBatchIfNeeded(
            this.fromLastSync.isAnotherBatchNeeded, ScheduleClassName, this.fromLastSync);
    }

    private SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT getSboResult(Object searchResult)
    {
        return (SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT) searchResult;
    }

    // createObjectKeyMap()
    //
    // create map of product key / search result.
    private Map<String, Object> createObjectKeyMap(
        List<Object> searchResults)
    {
        Map<String, Object> result =
            new Map<String, Object>();

        for (Object searchResult : searchResults)
        {
            String key = getSboKey(searchResult);
            if (String.isNotEmpty(key))
            {
                result.put(key, searchResult);
            }
        }

        return result;
    }

    private List<SObject> updateExistingObjects(
        Map<String, Object> searchResultMap,
        List<SObject> currentObjectList,
        List<SObject> errors)
    {
        List<SObject> updateObjectList = new List<SObject>();

        for (SObject currentObject : currentObjectList)
        {
            String syncKey = (String) currentObject.get(SFSyncKeyField);
            Object searchResult = searchResultMap.get(syncKey);

            // Updates fields and adds to objectList list for later commit
            syncObject(currentObject, searchResult, errors, updateObjectList);
        }

        System.debug('Existing Object Size: ' + updateObjectList.size());

        return updateObjectList;
    }

    private void syncObject(
        SObject currentObject,
        Object searchResult,
        List<SObject> errors,
        List<SObject> objectList)
    {
        SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);

        if (!currentWarrantyClaimsMap.containsKey(sboResult.NotificationNumber)) return;
        if (currentObject == null) currentObject = new Warranty_Claim__c();

        Warranty_Claim__c claim = (Warranty_Claim__c) currentObject;
        String key = getSboKey(searchResult);
        claim.put(SFSyncKeyField,key);

        claim.SAP_Sales_Order_Number__c = sboResult.SalesOrderNumber;
        claim.SAP_Warranty_Code__c = sboResult.WarrantyCode;
        claim.SAP_Closed_Date__c = sboResult.DispositionDate;
        if (sboResult.DispositionDate != null) claim.Disposition_Status__c = 'Approved';
        claim.Date_Return_Requested__c = sboResult.DateReturnRequested;
        claim.Date_Material_Received__c = sboResult.DateMaterialReceived;
        claim.Date_Inspected__c = sboResult.DateInspected;
        claim.Credit_Memo__c = sboResult.CreditMemo;
        claim.Credit_Memo_Date__c = sboResult.CreditMemoDate;
        claim.Credit_Memo_Amount__c = sboResult.CreditMemoAmount;

        objectList.add(claim);
    }

    private String getSboKey(Object searchResult)
    {
        SBO_EnosixServiceNotificationSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        return sboResult == null ? '' : sboResult.NotificationNumber;
    }
}