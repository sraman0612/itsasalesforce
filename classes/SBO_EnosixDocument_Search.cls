/// enosiX Inc. Generated Apex Model
/// Generated On: 10/14/2019 1:24:03 PM
/// SAP Host: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixDocument_Search extends ensxsdk.EnosixFramework.SearchSBO 
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixDocument_Search_Meta', new Type[] {
            SBO_EnosixDocument_Search.EnosixDocument_SC.class
            , SBO_EnosixDocument_Search.EnosixDocument_SR.class
            , SBO_EnosixDocument_Search.SEARCHRESULT.class
            , SBO_EnosixDocument_Search.SEARCHPARAMS.class
            , SBO_EnosixDocument_Search.SEARCHRESULT.class
            } 
        );
    }

    public SBO_EnosixDocument_Search() 
    {
        super('EnosixDocument', SBO_EnosixDocument_Search.EnosixDocument_SC.class, SBO_EnosixDocument_Search.EnosixDocument_SR.class);
    }
    
    public override Type getType() { return SBO_EnosixDocument_Search.class; }

    public EnosixDocument_SC search(EnosixDocument_SC sc) 
    {
        return (EnosixDocument_SC)super.executeSearch(sc);
    }

    public EnosixDocument_SC initialize(EnosixDocument_SC sc) 
    {
        return (EnosixDocument_SC)super.executeInitialize(sc);
    }

    public class EnosixDocument_SC extends ensxsdk.EnosixFramework.SearchContext 
    { 		
        public EnosixDocument_SC() 
        {		
            super(new Map<string,type>		
                {		
                    'SEARCHPARAMS' => SBO_EnosixDocument_Search.SEARCHPARAMS.class		
                });		
        }

        public override Type getType() { return SBO_EnosixDocument_Search.EnosixDocument_SC.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixDocument_Search.registerReflectionInfo();
        }

        public EnosixDocument_SR result { get { return (EnosixDocument_SR)baseResult; } }


        @AuraEnabled public SBO_EnosixDocument_Search.SEARCHPARAMS SEARCHPARAMS 
        {
            get
            {
                return (SBO_EnosixDocument_Search.SEARCHPARAMS)this.getStruct(SBO_EnosixDocument_Search.SEARCHPARAMS.class);
            }
        }
        
        }

    public class EnosixDocument_SR extends ensxsdk.EnosixFramework.SearchResult 
    {
        public EnosixDocument_SR() 
        {
            super(new Map<string,type>{'SEARCHRESULT' => SBO_EnosixDocument_Search.SEARCHRESULT.class } );
        }
        
        public ensxsdk.EnosixFramework.FrameworkCollection SearchResults
        {
            get { return super.getCollection(SBO_EnosixDocument_Search.SEARCHRESULT.class); }
        }
        
        public List<SEARCHRESULT> getResults() 
        {
            List<SEARCHRESULT> results = new List<SEARCHRESULT>();
            SearchResults.copyTo(results);
            return results;
        }

        public override Type getType() { return SBO_EnosixDocument_Search.EnosixDocument_SR.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixDocument_Search.registerReflectionInfo();
        }
    }

    public class SEARCHPARAMS extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixDocument_Search.SEARCHPARAMS.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixDocument_Search.registerReflectionInfo();
        }
        @AuraEnabled public String Material
        { 
            get { return this.getString ('MATNR'); } 
            set { this.Set (value, 'MATNR'); }
        }

        @AuraEnabled public String GdDocumentType
        { 
            get { return this.getString ('GDDOCTYPE'); } 
            set { this.Set (value, 'GDDOCTYPE'); }
        }

        @AuraEnabled public String Language
        { 
            get { return this.getString ('SPRAS'); } 
            set { this.Set (value, 'SPRAS'); }
        }

    }

    public class SEARCHRESULT extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixDocument_Search.SEARCHRESULT.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixDocument_Search.registerReflectionInfo();
        }
        @AuraEnabled public Date StartDate
        { 
            get { return this.getDate ('BEGDA'); } 
            set { this.Set (value, 'BEGDA'); }
        }

        @AuraEnabled public Date EndDate
        { 
            get { return this.getDate ('ENDDA'); } 
            set { this.Set (value, 'ENDDA'); }
        }

        @AuraEnabled public String EnosixObjKey
        { 
            get { return this.getString ('EnosixObjKey'); } 
            set { this.Set (value, 'EnosixObjKey'); }
        }

        @AuraEnabled public String DocumentNumber
        { 
            get { return this.getString ('DOCID'); } 
            set { this.Set (value, 'DOCID'); }
        }

        @AuraEnabled public String DocumentVersion
        { 
            get { return this.getString ('DOCVER'); } 
            set { this.Set (value, 'DOCVER'); }
        }

        @AuraEnabled public String GdDocumentType
        { 
            get { return this.getString ('GDDOCTYPE'); } 
            set { this.Set (value, 'GDDOCTYPE'); }
        }

        @AuraEnabled public String DocumentType
        { 
            get { return this.getString ('DOKAR'); } 
            set { this.Set (value, 'DOKAR'); }
        }

        @AuraEnabled public String DocumentPart
        { 
            get { return this.getString ('DOKTL'); } 
            set { this.Set (value, 'DOKTL'); }
        }

        @AuraEnabled public String DocumentDescription
        { 
            get { return this.getString ('DOCDESC'); } 
            set { this.Set (value, 'DOCDESC'); }
        }

    }

    public class SEARCHRESULT_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public SEARCHRESULT_COLLECTION()
        {
            super('SEARCHRESULT', SBO_EnosixDocument_Search.SEARCHRESULT.class, null);
        }

        public List<SBO_EnosixDocument_Search.SEARCHRESULT> getAsList()
        {
            return (List<SBO_EnosixDocument_Search.SEARCHRESULT>)this.buildList(List<SBO_EnosixDocument_Search.SEARCHRESULT>.class);
        }
    }


}