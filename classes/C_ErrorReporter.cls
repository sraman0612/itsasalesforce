public class C_ErrorReporter{
    
    public static void sendErrorReport(String subject, String origin, String message, String trace, String additionalInfo){
        
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //String[] toAddresses = new String[]{'service-gdi@canpango.com','nathan.holthaus@gardnerdenver.com'};
        mail.setToAddresses(getEmailAddresses2());
        mail.setUseSignature(false);
        mail.setSenderDisplayName('GDI Error Notification');
        mail.setSubject(subject);      
        String body = '<h4>An error has occurred in the following process: {0}</h4><h5>Message:</h5>{1}<br/><h5>Trace:</h5>{2}<h5>Additional Information:</h5>{3}';          
        mail.setHTMLBody(String.format(body,new String[]{origin,message,trace,additionalInfo}));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
    }
    
    public static void sendInfoReport(String subject, String origin, String message, String additionalInfo){
        
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //String[] toAddresses = new String[]{'service-gdi@canpango.com'};
        mail.setToAddresses(getEmailAddresses());
        mail.setUseSignature(false);
        mail.setSenderDisplayName('GDI Email Loop Notification');
        mail.setSubject(subject);      
        String body = '<h4>Email Subject: {0}</h4><h5>Message:</h5>{1}<br/><h5>Email Body:</h5>{2}';          
        mail.setHTMLBody(String.format(body,new String[]{origin,message,additionalInfo}));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
    }
    
    private static List<String> getEmailAddresses() {
        List<String> idList = new List<String>();
        List<String> mailToAddresses = new List<String>();
        Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE DeveloperName = 'Case_Loop_Notification'];
        for (GroupMember gm : g.groupMembers) {
            idList.add(gm.userOrGroupId);
        }
        User[] usr = [SELECT email FROM user WHERE id IN :idList];
        for(User u : usr) {
            mailToAddresses.add(u.email);
        }
        System.debug(mailToAddresses);
        return mailToAddresses;
    }
    
    private static List<String> getEmailAddresses2() {
        List<String> idList = new List<String>();
        List<String> mailToAddresses = new List<String>();
        Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE DeveloperName = 'Error_Notification_List'];
        for (GroupMember gm : g.groupMembers) {
            idList.add(gm.userOrGroupId);
        }
        User[] usr = [SELECT email FROM user WHERE id IN :idList];
        for(User u : usr) {
            mailToAddresses.add(u.email);
        }
        System.debug(mailToAddresses);
        return mailToAddresses;
    }
    
}