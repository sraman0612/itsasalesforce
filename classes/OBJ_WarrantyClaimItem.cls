global with sharing class OBJ_WarrantyClaimItem
{
    @AuraEnabled
    public String ItemNo {get; set;}
    @AuraEnabled
    public String PartNo {get; set;}
    @AuraEnabled
    public Decimal Quantity {get; set;}
    @AuraEnabled
    public String Unit {get; set;}
    @AuraEnabled
    public String GDInvoiceNumber {get; set;}
    @AuraEnabled
    public String GDInvoiceItemNumber {get; set;}
    @AuraEnabled
    public Decimal Price {get; set;} 
    @AuraEnabled
    public Decimal DiscountPercent {get; set;}
}