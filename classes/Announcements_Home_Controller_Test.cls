@isTest
public class Announcements_Home_Controller_Test {
    static testMethod void getAnnouncementsTest () {
        Date expDt = datetime.now().addDays(2).date();
        System.debug('expDt: ' + expDt);
        
        Date postDt = datetime.now().date();
        System.debug('postDt: ' + postDt);
        
        insert new Announcements__c(Name = 'Test 1', Active__c = true, Expire_Date__c = expDt, Posted_Date__c = postDt);
        
        List<Announcements__c> ann = Announcements_Home_Controller.getAnnouncements();
        System.debug('First announcement ' + ann[0]);
        System.assertEquals(ann[0].Name, 'Test 1');
    }
}