@IsTest
public class TSTC_EnosixServiceNotificationDetail {
    public class MOC_EnosixSO_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock,
            ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock,
            ensxsdk.EnosixFramework.DetailSBOInitMock
    {
        public boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }
        
        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            if (throwException)
            {
                throw new CalloutException();
            }

            SBO_EnosixSO_Detail.EnosixSO result = new SBO_EnosixSO_Detail.EnosixSO();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixSO_Detail.EnosixSO result = (SBO_EnosixSO_Detail.EnosixSO) obj;
            result.SalesDocument = '12345';
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string command, ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixSO_Detail.EnosixSO result = (SBO_EnosixSO_Detail.EnosixSO) obj;
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject  obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixSO_Detail.EnosixSO result = (SBO_EnosixSO_Detail.EnosixSO) obj;
            result.setSuccess(success);
            return result;
        }
    }

    public class MOC_EnosixServNotifUpdate_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOInitMock
    {
        public boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }
        
        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            if (throwException)
            {
                throw new CalloutException();
            }

            SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate result = new SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate result = (SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate) obj;
            result.SalesDocument = '12345';
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string command, ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate result = (SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate) obj;
            result.setSuccess(success);
            return result;
        }

        public  ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject  obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate result = (SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate) obj;
            result.setSuccess(success);
            return result;
        }
    }

    public class MOC_EnosixServiceNotification_Detail implements ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock, ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOInitMock
    {
        public boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }
        
        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key)
        {
            if (throwException)
            {
                throw new CalloutException();
            }

            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification();
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = (SBO_EnosixServiceNotification_Detail.EnosixServiceNotification) obj;
            result.SalesDocument = '12345';
            result.setSuccess(success);
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeCommand(string command, ensxsdk.EnosixFramework.DetailObject obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = (SBO_EnosixServiceNotification_Detail.EnosixServiceNotification) obj;
            result.setSuccess(success);
            return result;
        }

        public  ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject  obj)
        {
            if (throwException)
            {
                throw new CalloutException();
            }
            SBO_EnosixServiceNotification_Detail.EnosixServiceNotification result = (SBO_EnosixServiceNotification_Detail.EnosixServiceNotification) obj;
            result.setSuccess(success);
            return result;
        }
    }
    
    @IsTest
    public static void test_simulateOrder() {
        MOC_EnosixSO_Detail mocEnosixSODetail = new MOC_EnosixSO_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Detail.class, mocEnosixSODetail);

        CTRL_EnosixServiceNotificationDetail.simulateOrder(new SBO_EnosixSO_Detail.EnosixSO());

        mocEnosixSODetail.setThrowException(true);

        try {
            CTRL_EnosixServiceNotificationDetail.simulateOrder(new SBO_EnosixSO_Detail.EnosixSO());
        } catch (Exception e) {}
    }

    @IsTest
    public static void test_saveSalesOrder() {
        MOC_EnosixSO_Detail mocEnosixSODetail = new MOC_EnosixSO_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixSO_Detail.class, mocEnosixSODetail);

        CTRL_EnosixServiceNotificationDetail.saveSalesOrder(new SBO_EnosixSO_Detail.EnosixSO());

        mocEnosixSODetail.setThrowException(true);

        try {
            CTRL_EnosixServiceNotificationDetail.saveSalesOrder(new SBO_EnosixSO_Detail.EnosixSO());
        } catch (Exception e) {}
    }

    @IsTest
    public static void test_updateServiceNotification() {
        MOC_EnosixServNotifUpdate_Detail mocEnosixServNotifUpdateDetail = new MOC_EnosixServNotifUpdate_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServNotifUpdate_Detail.class, mocEnosixServNotifUpdateDetail);

        CTRL_EnosixServiceNotificationDetail.updateServiceNotification(new SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate());

        mocEnosixServNotifUpdateDetail.setThrowException(true);

        try {
            CTRL_EnosixServiceNotificationDetail.updateServiceNotification(new SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate());
        } catch (Exception e) {}
    }

    @IsTest
    public static void test_createServiceNotificationImpl() {
        MOC_EnosixServiceNotification_Detail mocEnosixServiceNotificationDetail = new MOC_EnosixServiceNotification_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotification_Detail.class, mocEnosixServiceNotificationDetail);

        CTRL_EnosixServiceNotificationDetail.createServiceNotificationImpl(new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification());

        mocEnosixServiceNotificationDetail.setThrowException(true);

        try {
            CTRL_EnosixServiceNotificationDetail.createServiceNotificationImpl(new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification());
        } catch (Exception e) {}
    }

    @IsTest
    public static void test_verifyServiceNotification() {
        MOC_EnosixServiceNotification_Detail mocEnosixServiceNotificationDetail = new MOC_EnosixServiceNotification_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixServiceNotification_Detail.class, mocEnosixServiceNotificationDetail);

        CTRL_EnosixServiceNotificationDetail.verifyServiceNotification(new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification());

        mocEnosixServiceNotificationDetail.setThrowException(true);

        try {
            CTRL_EnosixServiceNotificationDetail.verifyServiceNotification(new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification());
        } catch (Exception e) {}
    }
}