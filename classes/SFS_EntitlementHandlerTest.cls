/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 22/09/2021
* @description: SFS_EntitlementHandlerTest Apex Trigger.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001   SIF-47   20/12/2021 Test class for SFS_EntitlementHandler
====================================================================================================================================================*/
@isTest
public class SFS_EntitlementHandlerTest {
    @testSetup static void setup() {
        List<CTS_IOT_Frame_Type__c> frameTypeList=SFS_TestDataFactory.createFrameTypes(3,false,'Filter');
        frameTypeList[0].CTS_IOT_Type__c = 'Compressor';
        frameTypeList[1].CTS_IOT_Type__c = 'Dryer';
        insert frameTypeList;
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(4,false);
        assetList[0].CTS_Frame_Type__c=frameTypeList[0].Id;
        assetList[1].CTS_Frame_Type__c=frameTypeList[1].Id;
        assetList[2].CTS_Frame_Type__c=frameTypeList[2].Id;
        assetList[3].CTS_Frame_Type__c=frameTypeList[0].Id;
        insert assetList;
        
        List<WorkType> workTypeList = SFS_TestDataFactory.createWorkType(6,false);
        workTypeList[1].CTS_Frame_Type__c=frameTypeList[0].Id;
        workTypeList[0].SFS_Work_Scope1__c='4';
        workTypeList[0].SFS_Work_Order_Record_Type__c='Service';
        workTypeList[0].CTS_Frame_Type__c=frameTypeList[0].Id;
        workTypeList[2].CTS_Frame_Type__c=frameTypeList[1].Id;
        workTypeList[2].SFS_Work_Scope1__c='8';
        workTypeList[2].SFS_Work_Order_Record_Type__c='Service';
        workTypeList[3].CTS_Frame_Type__c=frameTypeList[1].Id;
        workTypeList[4].CTS_Frame_Type__c=frameTypeList[2].Id;
        workTypeList[4].SFS_Work_Scope1__c='8';
        workTypeList[4].SFS_Work_Order_Record_Type__c='Service';
        workTypeList[5].CTS_Frame_Type__c=frameTypeList[2].Id;
        insert workTypeList;
        
       String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accList = SFS_TestDataFactory.createAccounts(2,false);
        accList[1].Currency__c='USD';
        accList[1].Type='Prospect';
        accList[1].RecordTypeId=accRecID;
        accList[0].Type='Prospect';
        accList[0].AccountSource='Web';
        accList[1].AccountSource='Web';
        accList[0].IRIT_Customer_Number__c='2313';
        accList[1].IRIT_Customer_Number__c='1233';
        accList[1].CurrencyIsoCode='USD';
        accList[1].Currency__c='USD';
        accList[0].CurrencyIsoCode='USD';
        accList[0].Currency__c='USD';
        //insert accList;
        Database.DMLOptions opts = new Database.DMLOptions();       
        opts.DuplicateRuleHeader.AllowSave = true;        
        Database.insert(accList, opts);
        accList[0].Bill_To_Account__c=accList[1].Id;
        update accList[0];
		
        Pricebook2 priceBook = SFS_TestDataFactory.getPricebook2();
        
        List<ServiceContract> scList = SFS_TestDataFactory.createServiceAgreement(2, accList[0].Id ,false);
        scList[0].SFS_Status__c='APPROVED';
        insert scList;
        
        List<ContractLineItem> cliList =SFS_TestDataFactory.createServiceAgreementLineItem(2, scList[0].Id ,false);
        cliList[1].ServiceContractId = scList[1].Id;
        insert cliList;
    }
    
    Static testmethod void generateMwrTest(){
        List<Asset> assetList=[Select Id,RecordTypeId,Name,CurrencyIsoCode from Asset];
       
        List<Account> accountList = [Select Id,Name,Currency__c,type,Bill_To_Account__c from Account];
        
        List<ServiceContract> agreementList=[Select Id,Name,AccountId,SFS_Type__c,SFS_Portable_Required__c,StartDate,EndDate,SFS_Freight_Terms__c,recordTypeId from ServiceContract];
        
        List<ContractLineItem> lineItemList=[Select Id,ServiceContractId,Quantity,UnitPrice from ContractLineItem];
        
        List<WorkType> workTypeList=[Select Id,Name,EstimatedDuration,DurationType,SFS_Work_Scope1__c,SFS_Work_Order_Type__c from WorkType 
                                     where CTS_Frame_Type__r.CTS_IOT_Type__c = 'Compressor'];
        
        //get MaintenancePlan
        /*List<MaintenancePlan> mp = SFS_TestDataFactory.createMaintenancePlan(1,scList[1].Id,false);
        insert mp[0];
        
        //get MaintenanceAsset
        List<MaintenanceAsset> ma = SFS_TestDataFactory.createMaintenanceAsset(1,mp[0].Id,assetList[2].Id,true);*/
		
        List<Entitlement> entList = new List<Entitlement>();
        List<Entitlement> entFil = SFS_TestDataFactory.createEntitlement(1, accountList[0].Id, assetList[2].Id, agreementList[1].Id, lineItemList[1].Id ,false);
        entFil[0].SFS_CreateMWR__c = true;
        entList.add(entFil[0]);
        
        //get Entitlement
        List<Entitlement> enDry = SFS_TestDataFactory.createEntitlement(1, accountList[0].Id, assetList[1].Id, agreementList[0].Id, lineItemList[0].Id ,false);
        enDry[0].StartDate=System.today().addYears(2);
        enDry[0].EndDate=System.today().addYears(4);
        enDry[0].SFS_Contract_Hours__c = 12000;
        enDry[0].SFS_X1st_PM_Month__c = 'October';
        enDry[0].SFS_CreateMWR__c = true;
        entList.add(enDry[0]);

        List<Entitlement> enCom = SFS_TestDataFactory.createEntitlement(1, accountList[0].Id, assetList[0].Id, agreementList[0].Id, lineItemList[0].Id ,false);
        enCom[0].StartDate=System.today().addYears(1);
        enCom[0].EndDate=System.today().addYears(3);
        enCom[0].SFS_Contract_Hours__c = 12000;
        enCom[0].SFS_X1st_PM_Month__c = 'May';
        enCom[0].SFS_X1st_Work_Scope__c = '4';
        enCom[0].SFS_CreateMWR__c = true;
        enCom[0].SFS_Work_Type__c=workTypeList[0].Id;
        entList.add(enCom[0]);
        
        Test.startTest();
        
        insert entList;
        entList[0].AssetId=assetList[1].Id;
        entList[1].AssetId=assetList[0].Id;
        entList[2].AssetId=assetList[2].Id;
		List<Id> EntId=new List<Id>();
        EntId.add(entList[1].Id);
        List<Id> EntId2=new List<Id>();
        EntId2.add(entList[0].Id);
        List<Id> EntId3=new List<Id>();
        EntId3.add(entList[2].Id);
        SFS_EntitlementHandler.EntitlementHandlerUpdate(EntId);
        SFS_EntitlementHandler.EntitlementHandlerUpdate(EntId2);
        SFS_EntitlementHandler.EntitlementHandlerUpdate(EntId3);
        
        Test.stopTest();
    }
    Static testmethod void generateMwrTest1(){
        List<Asset> assetList=[Select Id,RecordTypeId,Name,CurrencyIsoCode from Asset];
       
        List<Account> accountList = [Select Id,Name,Currency__c,type,Bill_To_Account__c from Account];
        
        List<ServiceContract> agreementList=[Select Id,Name,AccountId,SFS_Type__c,SFS_Portable_Required__c,StartDate,EndDate,SFS_Freight_Terms__c,recordTypeId from ServiceContract];
        
        List<ContractLineItem> lineItemList=[Select Id,ServiceContractId,Quantity,UnitPrice from ContractLineItem];
        
        List<WorkType> workTypeList=[Select Id,Name,EstimatedDuration,DurationType,SFS_Work_Scope1__c,SFS_Work_Order_Type__c from WorkType 
                                     where CTS_Frame_Type__r.CTS_IOT_Type__c = 'Compressor'];

        List<Entitlement> entitlementList = SFS_TestDataFactory.createEntitlement(3, accountList[0].Id, assetList[0].Id, agreementList[0].Id, lineItemList[0].Id ,false);
        Integer numberOfDays = Date.daysInMonth(System.today().year(), System.today().month());
        Date startDate=Date.newInstance(System.today().year(), System.today().month(), 01);
        
        entitlementList[0].StartDate=startDate;
        entitlementList[0].EndDate=startDate.addMonths(11);
        entitlementList[0].SFS_Contract_Hours__c = 1500;
        entitlementList[0].SFS_X1st_PM_Month__c = 'May';
        entitlementList[0].SFS_CreateMWR__c = true;
        entitlementList[0].AssetId=assetList[3].Id;
        entitlementList[0].SFS_Work_Type__c=workTypeList[0].Id;
        
        entitlementList[1].StartDate=startDate;
        entitlementList[1].EndDate=startDate.addMonths(11);
        entitlementList[1].SFS_Contract_Hours__c = 1500;
        entitlementList[1].SFS_X1st_PM_Month__c = 'May';
        entitlementList[1].SFS_CreateMWR__c = true;
        entitlementList[1].AssetId=assetList[1].Id;

        Test.startTest();
        
        insert entitlementList;        
        entitlementList[1].AssetId=assetList[3].Id;
        entitlementList[0].AssetId=assetList[1].Id;
        List<Id> EntId=new List<Id>();
        EntId.add(entitlementList[1].Id);
        List<Id> EntId2=new List<Id>();
        EntId2.add(entitlementList[0].Id);
        SFS_EntitlementHandler.EntitlementHandlerUpdate(EntId);
        SFS_EntitlementHandler.EntitlementHandlerUpdate(EntId2);
        
        Test.stopTest();
    }
}