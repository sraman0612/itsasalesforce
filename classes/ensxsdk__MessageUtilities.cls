/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MessageUtilities {
    global static String ERROR;
    global static String INFO;
    global static String SUCCESS;
    global static String WARNING;
    global MessageUtilities(System.Type t) {

    }
global class Message {
    global String message {
        get;
        set;
    }
    global String messageType {
        get;
        set;
    }
    global Message() {

    }
    global Message(System.Type t) {

    }
    global Message(ensxsdk.EnosixFramework.Message f) {

    }
    global Message(String messageType, String message) {

    }
}
}
