public class CTRL_PausedClaimInterviews {
    @AuraEnabled
    public static UTIL_Aura.Response getPausedClaims() {
        SCREEN_DATA screenData = new SCREEN_DATA();
        String profileName = [Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;

        if (profileName == 'Warranty Administration' || profileName == 'System Administrator') {
            screenData.warrantyAdmin = true;
            screenData.interviews = [ SELECT Id, Name, CurrentElement, PauseLabel, CreatedDate, CreatedBy.Name, InterviewLabel FROM FlowInterview WHERE (InterviewLabel LIKE 'Machine Startup%' OR InterviewLabel LIKE 'Warranty Claim Form%' OR InterviewLabel LIKE 'Startup%') ORDER BY CreatedDate DESC ];
        } else {
            screenData.warrantyAdmin = false;
            screenData.interviews = [ SELECT Id, Name, CurrentElement, PauseLabel, CreatedDate, CreatedBy.Name, InterviewLabel FROM FlowInterview WHERE CreatedById = :UserInfo.getUserId() AND (InterviewLabel LIKE 'Machine Startup%' OR InterviewLabel LIKE 'Warranty Claim Form%' OR InterviewLabel LIKE 'Startup%') ORDER BY CreatedDate DESC ];
        }

        return UTIL_Aura.createResponse(screenData);
    }

    @AuraEnabled
    public static UTIL_Aura.Response deleteInterview(String interviewId) {
        delete [SELECT Id from FlowInterview where Id = : interviewId];

        return UTIL_Aura.createResponse(null);
    }

    public class SCREEN_DATA {
        @AuraEnabled
        public List<FlowInterview> interviews;
        @AuraEnabled
        public Boolean warrantyAdmin;
    }
}