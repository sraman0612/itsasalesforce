@isTest
public class CZCPQOpptyControllerTest {
    
    @isTest
    public static void test1() {
        Account testAccount = TSTU_SFAccount.createTestAccount();
        testAccount.Name = 'test';
        upsert testAccount;

        Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'TEST OPP';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.AccountId = testAccount.Id;
        opp.Pricebook2Id = pricebookId;

        TSTU_SFOpportunity.upsertOpportunity(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        TSTU_SFOpportunity.upsertOpportunity(opp);
        
        PageReference pageRef = Page.CZNewQuote;
        Test.setCurrentPage(pageRef);
      
        CZCPQOpptyController controller = new CZCPQOpptyController(new ApexPages.StandardController(opp));
        
        controller.getfinishLocation();
        controller.getAccountId();
    }

}