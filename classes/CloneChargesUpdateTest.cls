@isTest
public class CloneChargesUpdateTest {
    
    private static testMethod void chargeUpdateTest() {
        List<List<CAP_IR_Charge__c>> chLists = new List<List<CAP_IR_Charge__c>>();
        List<CAP_IR_Charge__c> chList = new List<CAP_IR_Charge__c>();
        List<Account> acc = SFS_TestDataFactory.createAccounts(1,true);
        List<ServiceContract> sa = SFS_TestDataFactory.createServiceAgreement(1,acc[0].Id,true);
        List<ContractLineItem> li = SFS_TestDataFactory.createServiceAgreementLineItem(1,sa[0].Id,true);
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(2,true);        
        CAP_IR_Charge__c ch = new CAP_IR_Charge__c(SFS_Serial_Number__c = 'Test Charge',
                                                   SFS_Expense_Type__c = 'Test Expense',
                                                   SFS_Manual_Charge__c = True,
                                                   SFS_Invoice__c = inv[0].Id,
                                                   CAP_IR_Contract_Line_Item__c = li[0].Id);
        insert ch;
        CAP_IR_Charge__c ch2 = new CAP_IR_Charge__c(SFS_Serial_Number__c = 'Test Charge',
                                                    SFS_Expense_Type__c = 'Test Expense',
                                                    SFS_Manual_Charge__c = True,
                                                    SFS_Invoice__c = inv[0].Id,
                                                    CAP_IR_Contract_Line_Item__c = li[0].Id,
                                                    Clone_From__c = ch.Id);
        chList.add(ch2);
        insert ch2;
        chLists.add(chList);
		Test.startTest();        
        CloneChargesUpdate.updateCharges(chLists);        
        Test.stopTest();
    }    
}