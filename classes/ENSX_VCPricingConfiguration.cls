public with sharing class ENSX_VCPricingConfiguration
{
    @AuraEnabled
    public String ConfigDate { get; set;  }
    @AuraEnabled
    public String SalesDocumentType { get; set; }
    @AuraEnabled
    public String SalesOrganization { get; set; }
    @AuraEnabled
    public String DistributionChannel { get; set; }
    @AuraEnabled
    public String Division { get; set; }
    @AuraEnabled
    public String SoldToParty{ get; set; }
    @AuraEnabled
    public String Plant { get; set; }
    @AuraEnabled
    public String SalesDocumentCurrency { get; set;}
    @AuraEnabled
    public Decimal OrderQuantity { get; set;}
    @AuraEnabled
    public String ObjectKey{ get;set; }
}