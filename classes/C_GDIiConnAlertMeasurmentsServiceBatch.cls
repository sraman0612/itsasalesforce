/*
 * Author   : Nocks Emmanuel Mulea 
 * Company  : Canpango LLC
 * Email    : emmanuel.mulea@canpango.com
 * 
 * 
 * 
 */


global class C_GDIiConnAlertMeasurmentsServiceBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    global final String query;
    
    global C_GDIiConnAlertMeasurmentsServiceBatch(){
         query ='SELECT id, name,IMEI__c,Cumulocity_ID__c FROM Asset LIMIT 1';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bacthC){
        
        return Database.getQueryLocator(query);
        
    }
    
    
    global void execute(Database.BatchableContext batchC,List<Asset>scope){
  
       // System.debug('everyMinute');
        C_GDIiConnMeasurmentsService.alarmEvreyFiveMins();
    }
 
    global void finish(Database.BatchableContext batchC){
        Datetime dt = system.now().addMinutes(3);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = '00';
        String year = string.valueOf(dt.year());
        String jobName ='C_GDIiConnAlertServiceSchedule-'+year+'-'+month+'-'+day+'-'+hour+'-'+minute+'-'+second;
        String cronExp = '00 '+minute+' '+hour+' '+day+' '+month+' ?'+' '+year;
        System.schedule(jobName, cronExp, new C_GDIiConnAlertServiceSchedule());
    }

}