@isTest
public with sharing class ensxtx_TSTU_ShippingInfo
{
    public class MOC_ensxtx_RFC_SD_GET_SHIP_INFO implements ensxsdk.EnosixFramework.RFCMock
    {
        public boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        public ensxsdk.EnosixFramework.FunctionObject executeFunction()
        {
            ensxtx_RFC_SD_GET_SHIP_INFO.RESULT result = new ensxtx_RFC_SD_GET_SHIP_INFO.RESULT();
            ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND shippingCondition = new ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND();
            shippingCondition.ShippingConditions = 'ShippingConditions';
            shippingCondition.VTEXT = 'VTEXT';
            result.getCollection(ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND.class).add(shippingCondition);
            result.setSuccess(this.success);
            return result;
        }
    }

    @isTest
    public static void test_filterShippingConditions()
    {
        MOC_ensxtx_RFC_SD_GET_SHIP_INFO mocRfcSdGetShipInfo = new MOC_ensxtx_RFC_SD_GET_SHIP_INFO();
        ensxsdk.EnosixFramework.setMock(ensxtx_RFC_SD_GET_SHIP_INFO.class, mocRfcSdGetShipInfo);

        Test.startTest();
        List<SelectOption> result = ensxtx_UTIL_SelectOption.buildOptionsFromList(
            new ensxtx_UTIL_ShippingInfo.ShippingConditionOptionBuilder(),
            ensxtx_UTIL_ShippingInfo.filterShippingConditions(ensxtx_UTIL_ShippingInfo.getShippingMaster())
        );
        mocRfcSdGetShipInfo.setSuccess(false);
        ensxtx_UTIL_ShippingInfo.filterShippingConditions(ensxtx_UTIL_ShippingInfo.getShippingMaster());
        Test.stopTest();
    }
}