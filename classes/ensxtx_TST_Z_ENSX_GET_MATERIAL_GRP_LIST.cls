/// enosiX Inc. Generated Apex Model
/// Generated On: 11/4/2020 9:54:30 AM
/// SAP Host: From REST Service On: https://gdi--enosixdev2.my.salesforce.com
/// CID: From REST Service On: https://gdi--enosixdev2.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
public with sharing class ensxtx_TST_Z_ENSX_GET_MATERIAL_GRP_LIST
{
    public class Mockensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST implements ensxsdk.EnosixFramework.RFCMock
    {
        public ensxsdk.EnosixFramework.FunctionObject executeFunction() 
        {
            return null;
        }
    }

    @isTest
    static void testExecute()
    {
        ensxsdk.EnosixFramework.setMock(ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.class, new Mockensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST());
        ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST rfc = new ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST();
        ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.RESULT params = rfc.PARAMS;
        System.assertEquals(null, rfc.execute());
    }

    @isTest
    static void testRESULT()
    {
        ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.RESULT funcObj = new ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.RESULT();

        funcObj.registerReflectionForClass();

        System.assertEquals(ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.RESULT.class, funcObj.getType(), 'getType() does not match object type.');

        funcObj.IV_SPART = 'X';
        System.assertEquals('X', funcObj.IV_SPART);

        funcObj.IV_VKORG = 'X';
        System.assertEquals('X', funcObj.IV_VKORG);

        funcObj.IV_VTWEG = 'X';
        System.assertEquals('X', funcObj.IV_VTWEG);

        //Check all the collections
        funcObj.getCollection(ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS.class).add(new ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS());
        System.assertEquals(1,funcObj.ET_MATERIAL_GROUPS_List.size());

    }

    @isTest
    static void testET_MATERIAL_GROUPS()
    {
        ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS funcObj = new ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS();

        funcObj.registerReflectionForClass();

        System.assertEquals(ensxtx_RFC_Z_ENSX_GET_MATERIAL_GRP_LIST.ET_MATERIAL_GROUPS.class, funcObj.getType(), 'getType() does not match object type.');
        funcObj.MANDT = 'X';
        System.assertEquals('X', funcObj.MANDT);

        funcObj.SPRAS = 'X';
        System.assertEquals('X', funcObj.SPRAS);

        funcObj.MATKL = 'X';
        System.assertEquals('X', funcObj.MATKL);

        funcObj.WGBEZ = 'X';
        System.assertEquals('X', funcObj.WGBEZ);

        funcObj.WGBEZ60 = 'X';
        System.assertEquals('X', funcObj.WGBEZ60);

    }
}