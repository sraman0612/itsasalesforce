public with sharing class CaseAddToCalendar {
    
    @AuraEnabled
    public static Case getCase(Id caseId){
        //make your own SOQL here from your desired object where you want to place your lightning action
        return [SELECT Id, Status, ContactId, Primary_Ser_Tech_Contact__c, OwnerName__c, OwnerId, Current_Schedule_Date__c,
                Current_Schedule_End_Date__c, Serial_Number_Owner_Information_Name__c, Serial_Number_Owner_Information_C_S__c, 
                Belliss_Service_Type__c, Actual_Type_of_Maintenance__c, Serial_Number_CC__c, Technician__c
                FROM Case WHERE Id = :caseId];
    }

}