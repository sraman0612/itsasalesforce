/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 20/10/2021
* @description:  Apex Handler class for InvoiceLineItemTrigger
Modification Log:
------------------------------------------------------------------------------------
Developer       	  Mod Number  		Date        		Description
------------------------------------------------------------------------------------
Bhargavi Nerella						02/09/2022		SIF-1840 assignChargeAmount method
Bhargavi Nerella						02/09/2022		SIF-1840 assignSellPrice
============================================================================================================================================================*/

public class SFS_ChargeTriggerHandler {
    
    //Updates Invoice amount based on the charges amount (Custom roll up summary logic)
    //Updated on 29/08/2022 to consider service agreement related charges
    public static void invoiceAmountRollUpSummary(List<CAP_IR_Charge__c> invLineItemList,Map<Id,CAP_IR_Charge__c>invLineItemMap){
        Set<Id> invIDs = new Set<Id>();
        Map<Id,Invoice__c> invMap = new Map<Id,Invoice__c>();
        Integer d=0;//SIF-651
        Id agrChargeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByDeveloperName().get('SFS_Agreement_Charge').getRecordTypeId();
        Id agrCreditId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByDeveloperName().get('SFS_Agreement_Credit').getRecordTypeId();
        Id careChargeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByDeveloperName().get('SFS_CARE_Charge').getRecordTypeId(); 
        Id chrgeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByDeveloperName().get('SFS_Charge').getRecordTypeId();
        try{
            if(invLineItemList.size()>0){
                for(CAP_IR_Charge__c invLine : invLineItemList){
                    if(invLine.recordTypeId == agrChargeId || invLine.recordTypeId == agrCreditId || invLine.recordTypeId == careChargeId || invLine.recordTypeId == chrgeId){                        
                        if(invLineItemMap==Null){
                            invIDs.add(invLine.SFS_Invoice__c);
                        }  
                        else {
                            CAP_IR_Charge__c oldinvLine = invLineItemMap.get(invLine.Id);
                            if(invLine.CAP_IR_Amount__c != oldinvLine.CAP_IR_Amount__c ||invLine.SFS_Original_Amount__c != oldinvLine.SFS_Original_Amount__c   )
                            {
                                invIDs.add(invLine.SFS_Invoice__c);
                            }
                        }
                    }
                } 
                if(invIDs.size()>0){
                    for(Id invoiceId : invIDs){
                        invMap.put(invoiceId, new Invoice__c(Id =invoiceId,SFS_Invoice_Amount__c=0,SFS_Total_Price__c=0));       
                    }                    
                }
                if(invIDs.size()>0){
                    for(CAP_IR_Charge__c iLi : [select Id, CAP_IR_Amount__c,SFS_Invoice__c,SFS_Invoice4Multiples__c,SFS_Original_Amount__c from CAP_IR_Charge__c where SFS_Invoice__c IN :invIDs  
                                                AND (RecordType.DeveloperName = 'SFS_Agreement_Charge' OR RecordType.DeveloperName = 'SFS_Agreement_Credit' OR RecordType.DeveloperName = 'SFS_CARE_Charge'OR RecordType.DeveloperName = 'SFS_Charge')]){
                                                    if(iLi.SFS_Invoice4Multiples__c == true){//SIF-651
                                                        invMap.get(iLi.SFS_Invoice__c).SFS_Invoice_Amount__c += 0.00; //adds invoice line item amount //When Rental Agreement is active, we create weekly Invoices, but every 4th week is 0$.
                                                    }
                                                    else{
                                                        invMap.get(iLi.SFS_Invoice__c).SFS_Invoice_Amount__c += iLi.CAP_IR_Amount__c;
                                                    }                 
                                                    invMap.get(iLi.SFS_Invoice__c).SFS_Total_Price__c += iLi.SFS_Original_Amount__c;
                                                    
                                                }
                }
                
                if(invMap.size()>0 && invMap!=Null){               
                    database.update(invMap.values()); 
                }
            }
        }
        catch(Exception e){
            system.debug('Exception'+e.getMessage() + e.getLineNumber());
        }
    }
    
    //Count no of Charges on invoice
    public static void updateChargeCount(List<CAP_IR_Charge__c> chargeList,Map<Id,CAP_IR_Charge__c> oldMap){
        Set<Id> invoiceIds=new Set<Id>(); 
        Map<Id,Invoice__c> invMap = new Map<Id,Invoice__c>();
        for(CAP_IR_Charge__c charge:chargeList){
            if(oldMap==null && charge.SFS_Invoice__c!=null){
                invoiceIds.add(charge.SFS_Invoice__c);
            }
            else if(oldMap!=null){
                CAP_IR_Charge__c oldRecord = oldMap.get(charge.Id);                       
                if((oldRecord.SFS_Invoice__c != charge.SFS_Invoice__c)&& charge.SFS_Invoice__c != null){
                    invoiceIds.add(charge.SFS_Invoice__c);                              
                }
            }
        }
        if(invoiceIds.size()>0){
            for(Id invoiceId : invoiceIds){
                invMap.put(invoiceId, new Invoice__c(Id =invoiceId,SFS_Charge_Count__c=0));       
            }
            for(CAP_IR_Charge__c charge : [select Id,SFS_Invoice__c from CAP_IR_Charge__c where SFS_Invoice__c IN :invoiceIds ]){
                invMap.get(charge.SFS_Invoice__c).SFS_Charge_Count__c += 1;
            }
        }
        if(invMap.size()>0 && invMap!=Null){           
            database.update(invMap.values()); 
        }
    }
    
    public static void rollUpAmountToWorkOrder(List<CAP_IR_Charge__c> newList, Map<Id,CAP_IR_Charge__c> oldMap){
        try{
            Set<Id> woIds = new Set<Id>();
            Set<Id> chargeIds = new Set<Id>();
            for(CAP_IR_Charge__c charge : newList){
                chargeIds.add(charge.Id);
                if(charge.CAP_IR_Work_Order__c == null){
                    CAP_IR_Charge__c oldCharge = oldMap.get(charge.Id);
                    woIds.add(oldCharge.CAP_IR_Work_Order__c);
                }else{
                    woIds.add(charge.CAP_IR_Work_Order__c);
                }
                
            }
            List<WorkOrder> newWoList =new List<WorkOrder>();
            if(woIds.size()>0){
                newWoList=[Select Id,(Select Id,CAP_IR_Work_Order__c,CAP_IR_Description__c,SFS_PO_Number__c,CAP_IR_Amount__c,SFS_Sell_Price__c 
                                      from Charges__r ) from WorkOrder where Id IN:woIds];
            }
            List<WorkOrder> updatedWoList=new List<WorkOrder>();
            if(newWoList.size()>0){
                for(WorkOrder wo : newWoList){
                    Double Amount=0;
                    for(CAP_IR_Charge__c charge : wo.Charges__r){
                        Amount+=charge.SFS_Sell_Price__c;
                    }
                    wo.SFS_Total_Charges__c=Amount;
                    updatedWoList.add(wo);
                }
            }
            update updatedWoList;
        }Catch(Exception e){}
    }
    
    /*Purpose : In a WO charge, when Invoice or Amount is updated OR Charge is deleted OR Undeleted, roll up 
the Sell Price and Original Price on Charge to the Invoice.*/
    public static void assignChargeAmount(List<CAP_IR_Charge__c> chargeList,Map<Id,CAP_IR_Charge__c> oldMap){
        
        try{            
            Id woChargeTypeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByDeveloperName().get('SFS_WO_Charge').getRecordTypeId();
            Id woCreditTypeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByDeveloperName().get('SFS_WO_Credit').getRecordTypeId();
            Id chrgeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByDeveloperName().get('SFS_Charge').getRecordTypeId();
            Set<Id> invoiceIds = new Set<Id>();
            for(CAP_IR_Charge__c charge : chargeList){
                if(charge.RecordTypeId == woChargeTypeId || charge.RecordTypeId == woCreditTypeId || charge.RecordTypeId == chrgeId){
                    if(oldMap == null && charge.SFS_Invoice__c != null){
                        invoiceIds.add(charge.SFS_Invoice__c);                        
                    }else{
                        CAP_IR_Charge__c oldRecord = oldMap.get(charge.Id);                       
                        if((oldRecord.SFS_Invoice__c != charge.SFS_Invoice__c)  || (oldRecord.SFS_Sell_Price__c != charge.SFS_Sell_Price__c && charge.SFS_Invoice__c != null)){
                            invoiceIds.add(charge.SFS_Invoice__c);                               
                        }
                    }
                }
            }
            List<Invoice__c> invoiceList= new List<Invoice__c>();
            if(invoiceIds.size()>0){
                invoiceList = [Select Id,SFS_Invoice_Amount__c, SFS_Total_Price__c, SFS_Total_Charge_Amount__c,
                               (Select Id,CAP_IR_Amount__c, SFS_Sell_Price__c,SFS_Original_Price__c from Invoice_Line_Items__r 
                                where recordTypeId=:woChargeTypeId OR recordTypeId=:woCreditTypeId OR recordTypeId=:chrgeId) 
                               from Invoice__c where Id IN : invoiceIds];
            }
            
            Map<Id,List<CAP_IR_Charge__c>> chargeMap = new Map<Id,List<CAP_IR_Charge__c>>();
            List<Invoice__c> updatedList = new List<Invoice__c>();
            if(invoiceList.size()>0){
                for(Invoice__c inv : invoiceList){
                    Double totalOriginalSellPrice = 0.00;
                    Double totalSellPrice = 0.00;                    
                    for(CAP_IR_Charge__c charge : inv.Invoice_Line_Items__r){                                               
                        if(charge.SFS_Original_Price__c!=null){
                            totalOriginalSellPrice+= charge.SFS_Original_Price__c;
                        }
                        if(charge.SFS_Sell_Price__c!=null){
                            totalSellPrice+= charge.SFS_Sell_Price__c;
                        }
                        
                    }                   
                    inv.SFS_Total_Price__c = totalOriginalSellPrice;
                    inv.SFS_Total_Charge_Amount__c = totalSellPrice;                   
                    updatedList.add(inv);
                }
            }
            update updatedList;
        }catch(exception e){
            system.debug('Exception'+e.getMessage() + e.getLineNumber());
        }
    }
    /*Purpose : When the amount is changed, assign Sell Price based on the charge type and work order division*/
    public static void assignSellPrice(List<CAP_IR_Charge__c> chargeList, Map<Id,CAP_IR_Charge__c> oldMap){
        try{
            Set<Id> woIds = new Set<Id>();
            Decimal amnt;
            Decimal roundAmnt;
            for(CAP_IR_Charge__c charge : chargeList){
                if(charge.CAP_IR_Work_Order__c!=null ){
                    woIds.add(charge.CAP_IR_Work_Order__c);
                }
            }
            List<WorkOrder> woList = new List<WorkOrder>();
            if(woIds.size()>0){
                woList = [Select Id,SFS_Division__r.SFS_Margin__c, SFS_Division__r.SFS_Expense_margin__c 
                          from WorkOrder where Id IN : woIds];
            }
            Map<Id,WorkOrder> woMap = new Map<Id,WorkOrder>();
            for(WorkOrder wo : woList){
                woMap.put(wo.Id,wo);
            }
            for(CAP_IR_Charge__c charge : chargeList){
                CAP_IR_Charge__c oldRecord = oldMap.get(charge.Id);
                if(charge.CAP_IR_Amount__c != oldRecord.CAP_IR_Amount__c){
                    WorkOrder relatedWO = woMap.get(charge.CAP_IR_Work_Order__c);
                    if(charge.SFS_Charge_Type__c=='Part Movement' || charge.SFS_Charge_Type__c=='Time'){
                        charge.SFS_Sell_Price__c = charge.CAP_IR_Amount__c;
                    }
                    else if(charge.SFS_Charge_Type__c=='Ordered Item' || charge.SFS_Charge_Type__c=='Expense'){
                        amnt=charge.CAP_IR_Amount__c/(1-(relatedWO.SFS_Division__r.SFS_Expense_margin__c/100));
                        roundAmnt = amnt.setScale(2);
                        charge.SFS_Sell_Price__c = roundAmnt;
                    }
                    else if(charge.SFS_Charge_Type__c=='Freight / S&H' && charge.CAP_IR_Description__c=='Freight / S&H'){
                        amnt=charge.CAP_IR_Amount__c/(1-(relatedWO.SFS_Division__r.SFS_Margin__c/100));
                        roundAmnt = amnt.setScale(2);
                        charge.SFS_Sell_Price__c = roundAmnt;
                    }
                }
            }
        }catch(Exception e){
            
        }
    }
    
    //Jacob 11Aug
     public static void handleChargeTrigger(List<CAP_IR_Charge__c> newCharges, Map<Id, CAP_IR_Charge__c> oldMap, Boolean isDelete) {
        Set<Id> parentIdsToUpdate = new Set<Id>();

        for (CAP_IR_Charge__c charge : newCharges) {
            if (charge.Invoice_Line_Item__c != null) {
                parentIdsToUpdate.add(charge.Invoice_Line_Item__c);
            }
        }

        if (isDelete && oldMap != null && !oldMap.isEmpty()) {
            for (CAP_IR_Charge__c charge : oldMap.values()) {
                if (charge.Invoice_Line_Item__c != null) {
                    parentIdsToUpdate.add(charge.Invoice_Line_Item__c);
                }
            }
        }

        List<SFS_Invoice_Line_Item__c> parentsToUpdate = new List<SFS_Invoice_Line_Item__c>();
        Map<Id, Integer> parentIdToChargeCount = new Map<Id, Integer>();

        if (!parentIdsToUpdate.isEmpty()) {
            for (AggregateResult ar : [SELECT Invoice_Line_Item__c, COUNT(Id) chargeCount
                                       FROM CAP_IR_Charge__c
                                       WHERE Invoice_Line_Item__c IN :parentIdsToUpdate
                                       GROUP BY Invoice_Line_Item__c]) {
                Id parentId = (Id) ar.get('Invoice_Line_Item__c');
                Integer chargeCount = (Integer) ar.get('chargeCount');
                parentIdToChargeCount.put(parentId, chargeCount);
            }
        }

        for (Id parentId : parentIdsToUpdate) {
            Integer chargeCount = parentIdToChargeCount.containsKey(parentId) ? parentIdToChargeCount.get(parentId) : 0;
            parentsToUpdate.add(new SFS_Invoice_Line_Item__c(Id = parentId, Number_of_Charges__c = chargeCount));
        }

        if (!parentsToUpdate.isEmpty()) {
            update parentsToUpdate;
        }
    }
}