/*=========================================================================================================
* @author : Naresh P, Capgemini
* @date : 07/09/2022
* @description: 
==============================================================================================================*/
@isTest
public class SFS_Billing_WorkOrder_Outbound_Test {
@isTest
    public static void SFS_Billing_WorkOrder_Outbound_Handler_Test()
    {
        //try{
            List<Account> accList=new List<Account>();
            Account acct = new Account();
        	acct.name = 'test account1';
        	acct.Currency__c='USD';
            acct.BillingCountry='USA';
            acct.type = 'ship to';
            acct.AccountSource='Web';
            acct.BillingState='AP';
            acct.IRIT_Customer_Number__c = '123';
        	insert acct;
            accList.add(acct);

         List<Division__c> div = new List<Division__c> ();
        Division__c div1 = new Division__c( 
                                 Name = 'TestLoc',
                                 SFS_Org_Code__c = 'ABC',
                                 Division_Type__c='Customer Center' ,
                                 EBS_System__c='Oracle 11i');
        insert div1;
        div.add(div1);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct.Id ,loc[0].Id, div[0].Id, sc[0].Id, false);
        wo[0].StartDate = date.today();
        wo[0].endDate = date.today() + 14;
        insert wo;
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,true);
        
        //get invoice
         List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,acct.Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Annually';
           svcAgreementList[0].SFS_Status__c = 'APPROVED';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Vendor_Invoice'].id;
         Invoice__c inv = new Invoice__c(recordTypeId = invRecTypeId
                                        //SFS_Service_Agreement__c=svcAgreementList[0].Id,
                                       	//SFS_Status__c='Created',
                                       //SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                       //SFS_Billing_Period_End_Date__c=system.today(),
                                       //SFS_Work_Order__c=wo[0].id,
                                       //SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                       //SFS_Billing_Period_Start_Date__c= system.today()
                                       );
         insert inv;
                
           //List<Invoice__c> invList =SFS_TestDataFactory.createInvoice(1,true);
            SFS_Invoice_Line_Item__c inli = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=inv.Id,SFS_Work_Order__c=wo[0].Id/*,SFS_Work_Order_Line_Item__c=woliList[0].Id*/,SFS_Amount__c=123);
            insert inli;
        inli.SFS_Type__c = 'Time';
        update inli;

            List<Id> Ids = new List<id>();
            Ids.add(inv.Id);
       
            SFS_Billing_WorkOrder_Outbound_Handler.Run(Ids);
      /*}
        catch(exception e) {} */
    }  
    @isTest
    public static void SFS_Billing_WorkOrder_Outbound_Handler_Fail1()
    {
        //try{
            List<Account> accList=new List<Account>();
            Account acct = new Account();
        	acct.name = 'test account1';
        	acct.Currency__c='USD';
            acct.BillingCountry='USA';
         acct.type = 'ship to';
            acct.AccountSource='Web';
            acct.BillingState='AP';
            acct.IRIT_Customer_Number__c = '123';
        	insert acct;
            accList.add(acct);

         List<Division__c> div = new List<Division__c> ();
        Division__c div1 = new Division__c( 
                                 Name = 'TestLoc',
                                 SFS_Org_Code__c = 'ABC',
                                 Division_Type__c='Customer Center' ,
                                 EBS_System__c='Oracle 11i');
        insert div1;
        div.add(div1);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct.Id ,loc[0].Id, div[0].Id, sc[0].Id, false);
        wo[0].StartDate = date.today();
        wo[0].endDate = date.today() + 14;
        
        insert wo;
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(2,wo[0].Id,false);
        charge[0].SFS_Integration_Status__c = 'N/A';
        charge[0].SFS_Integration_Response__c = Null;
        charge[1].SFS_Integration_Status__c = 'N/A';
        charge[1].SFS_Integration_Response__c = Null;
        insert charge;
        
        //get invoice
         List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,acct.Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Annually';
           svcAgreementList[0].SFS_Status__c = 'APPROVED';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Vendor_Invoice'].id;
         Invoice__c inv = new Invoice__c(recordTypeId = invRecTypeId,
                                       //SFS_Service_Agreement__c=svcAgreementList[0].Id,
                                       	//SFS_Status__c='Created',
                                       //SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                       //SFS_Billing_Period_End_Date__c=system.today(),
                                       //SFS_Work_Order__c=wo[0].id,
                                       //SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                       //SFS_Billing_Period_Start_Date__c= system.today()
                                       SFS_Integration_Status__c = 'New',
                                       SFS_Integration_Response__c = null
                                       );
         insert inv;
        Invoice__c inv2 = new Invoice__c(recordTypeId = invRecTypeId,
                                       //SFS_Service_Agreement__c=svcAgreementList[0].Id,
                                       	//SFS_Status__c='Created',
                                       //SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                       //SFS_Billing_Period_End_Date__c=system.today(),
                                       //SFS_Work_Order__c=wo[0].id,
                                       //SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                       //SFS_Billing_Period_Start_Date__c= system.today()
                                       SFS_Integration_Status__c = 'New',
                                       SFS_Integration_Response__c = null
                                       );
         insert inv2;
                
           //List<Invoice__c> invList =SFS_TestDataFactory.createInvoice(1,true);
            SFS_Invoice_Line_Item__c inli = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=inv.Id,SFS_Work_Order__c=wo[0].Id/*,SFS_Work_Order_Line_Item__c=woliList[0].Id*/,SFS_Amount__c=123);
            insert inli;
        	//charge[0].SFS_Invoice__c = inv.Id;
        SFS_Invoice_Line_Item__c inli2 = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=inv2.Id,SFS_Work_Order__c=wo[0].Id/*,SFS_Work_Order_Line_Item__c=woliList[0].Id*/,SFS_Amount__c=123);
            insert inli2;
        	//charge[1].SFS_Invoice__c = inv2.Id;

            List<Id> Ids = new List<id>();
            Ids.add(inv.Id);
        	String idString = String.ValueOf(inv.Id);
        	String InterfaceLabel = 'WorkorderBilling|'+IdString;
        	string idString2 = String.ValueOf(inv2.Id);
        	String InterfaceLabel2 = 'WorkorderBilling|'+IdString2;
       
            httpResponse res = new httpResponse();
        	res.setStatusCode(401);
        	res.setBody('Server Error');
        	SFS_Billing_WorkOrder_Outbound_Handler.getResponse(res, interfaceLabel);
        	System.AssertEquals(true,inv.SFS_Integration_Status__c != Null);

        	
        
        	httpResponse res1 = new httpResponse();
        	res.setStatusCode(00);
        	res.setBody('Ok');
        	SFS_Billing_WorkOrder_Outbound_Handler.getResponse(res, interfaceLabel2);
        	System.AssertEquals(true, inv2.SFS_Integration_Status__c != null);

      /*}
        catch(exception e) {} */
    } 
    @isTest
    public static void SFS_Billing_WorkOrder_Outbound_Handler_Fail()
    {
        //try{
            List<Account> accList=new List<Account>();
            Account acct = new Account();
        	acct.name = 'test account1';
        	acct.Currency__c='USD';
            acct.BillingCountry='USA';
         acct.type = 'ship to';
            acct.AccountSource='Web';
            acct.BillingState='AP';
            acct.IRIT_Customer_Number__c = '123';
        	insert acct;
            accList.add(acct);

         List<Division__c> div = new List<Division__c> ();
        Division__c div1 = new Division__c( 
                                 Name = 'TestLoc',
                                 SFS_Org_Code__c = 'ABC',
                                 Division_Type__c='Customer Center' ,
                                 EBS_System__c='Oracle 11i');
        insert div1;
        div.add(div1);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct.Id ,loc[0].Id, div[0].Id, sc[0].Id, false);
        wo[0].StartDate = date.today();
        wo[0].endDate = date.today() + 14;
        insert wo;
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(2,wo[0].Id,false);
        charge[0].SFS_Integration_Status__c = 'N/A';
        charge[0].SFS_Integration_Response__c = Null;
        charge[1].SFS_Integration_Status__c = 'N/A';
        charge[1].SFS_Integration_Response__c = Null;
        insert charge;
        
        //get invoice
         List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,acct.Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Annually';
           svcAgreementList[0].SFS_Status__c = 'APPROVED';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Vendor_Invoice'].id;
         Invoice__c inv = new Invoice__c(recordTypeId = invRecTypeId,
                                       //SFS_Service_Agreement__c=svcAgreementList[0].Id,
                                       	//SFS_Status__c='Created',
                                       //SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                       //SFS_Billing_Period_End_Date__c=system.today(),
                                       //SFS_Work_Order__c=wo[0].id,
                                       //SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                       //SFS_Billing_Period_Start_Date__c= system.today()
                                       SFS_Integration_Status__c = 'New',
                                       SFS_Integration_Response__c = null
                                       );
         insert inv;
        Invoice__c inv2 = new Invoice__c(recordTypeId = invRecTypeId,
                                       //SFS_Service_Agreement__c=svcAgreementList[0].Id,
                                       	//SFS_Status__c='Created',
                                       //SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                       //SFS_Billing_Period_End_Date__c=system.today(),
                                       //SFS_Work_Order__c=wo[0].id,
                                       //SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                       //SFS_Billing_Period_Start_Date__c= system.today()
                                       SFS_Integration_Status__c = 'New',
                                       SFS_Integration_Response__c = null
                                       );
         insert inv2;
                
           //List<Invoice__c> invList =SFS_TestDataFactory.createInvoice(1,true);
            SFS_Invoice_Line_Item__c inli = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=inv.Id,SFS_Work_Order__c=wo[0].Id/*,SFS_Work_Order_Line_Item__c=woliList[0].Id*/,SFS_Amount__c=123);
            insert inli;
        	//charge[0].SFS_Invoice__c = inv.Id;
        SFS_Invoice_Line_Item__c inli2 = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=inv2.Id,SFS_Work_Order__c=wo[0].Id/*,SFS_Work_Order_Line_Item__c=woliList[0].Id*/,SFS_Amount__c=123);
            insert inli2;
        	//charge[1].SFS_Invoice__c = inv2.Id;

            List<Id> Ids = new List<id>();
            Ids.add(inv.Id);
        	String idString = String.ValueOf(inv.Id);
        	String InterfaceLabel = 'WorkorderBilling|'+IdString;
        	string idString2 = String.ValueOf(inv2.Id);
        	String InterfaceLabel2 = 'WorkorderBilling|'+IdString2;
       
            httpResponse res = new httpResponse();
        	res.setStatusCode(401);
        	res.setBody('Server Error');
        	SFS_Billing_WorkOrder_Outbound_Handler.getResponse(res, interfaceLabel);
        	System.AssertEquals(true,inv.SFS_Integration_Status__c != Null);

        	
        
        	httpResponse res1 = new httpResponse();
        	res.setStatusCode(200);
        	res.setBody('Ok');
        	SFS_Billing_WorkOrder_Outbound_Handler.getResponse(res, interfaceLabel2);
        	System.AssertEquals(true, inv2.SFS_Integration_Status__c != null);

      /*}
        catch(exception e) {} */
    }     
}