public inherited sharing class ensxtx_UTIL_VC_Perf {
     public static Map<String, String> RunConfiguration(ensxtx_ENSX_VCPricingConfiguration pricingConfig, Map<String, String> values) {
        System.assertNotEquals(null, pricingConfig, 'pricingConfig required');
        System.assertNotEquals(null, values, 'values required');
        
         Map<String, Long> processsTimes = new Map<String, Long>();

        Long startTime = SW();

        ensxtx_DS_VCMaterialConfiguration initialConfig = ensxtx_UTIL_VC_PricingAndConfiguration.getInitialConfigFromMaterialAndPricingAndCustomConfig(pricingConfig.Material, pricingConfig);

        //System.debug(initialConfig);
        List<ensxtx_DS_VCCharacteristicValues> selectedValues = new List<ensxtx_DS_VCCharacteristicValues>();
        List<String> charactoristics = new List<String>();
       
        List<string> keyList = new List<string>(values.keySet());
        Integer keyTot = keyList.size();
        Integer charTot = initialConfig.characteristics.size();
        for (Integer keyCnt = 0 ; keyCnt < keyTot ; keyCnt++) {
            string key = keyList[keyCnt];
            ensxtx_DS_VCCharacteristicValues addedValue = null;

            for (Integer charCnt = 0 ; charCnt < charTot ; charCnt++) {
                ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS c = initialConfig.characteristics[charCnt];
                if (key == c.CharacteristicName || key == c.CharacteristicDescription) {
                    charactoristics.add(c.CharacteristicName);
                }
            }
        }

        Long valuesStart = SW();
        initialConfig = ensxtx_UTIL_VC_PricingAndConfiguration.proccessAndLogVCConfiguration(initialConfig, selectedValues, charactoristics);
        Long valuesEnd = SW();
        processsTimes.put('LoadAllowValues'+charactoristics.size(), valuesEnd - valuesStart);

        for (Integer keyCnt = 0 ; keyCnt < keyTot ; keyCnt++) {
            string key = keyList[keyCnt];
            string value = values.get(key);
            ensxtx_DS_VCCharacteristicValues addedValue = CreateValueForCharAndValue(initialConfig, key, value);
            selectedValues = new List<ensxtx_DS_VCCharacteristicValues>{ addedValue };
            
            if (null == addedValue) {
                system.debug('Could not add characteristic (' + key + ') value ' + value);
            }

            //System.assertNotEquals(null, addedValue, 'Could not add characteristic (' + key + ') value ' + value);

            Long configStart = SW();
            ensxtx_DS_VCMaterialConfiguration newConfig = ensxtx_UTIL_VC_PricingAndConfiguration.proccessAndLogVCConfiguration(initialConfig, selectedValues, charactoristics);
            Long configEnd = SW();
            processsTimes.put(key, configEnd - configStart);

            //AssertSelectedValueSet(newConfig, key, value);
            
            initialConfig = newConfig; // Commit
        }

        Long endTime = SW();

        Map<String, String> result = new Map<String, String>{
            'TotalTime' => (endTime - startTime) + 'ms'
            //,'SessionData' => ensxtx_UTIL_VC_PricingAndConfiguration.SessionData
        };
        Long Sum = 0;
        keyList = new List<string>(processsTimes.keySet());
        keyTot = keyList.size();
        for (Integer keyCnt = 0 ; keyCnt < keyTot ; keyCnt++) {
            string key = keyList[keyCnt];
            result.put(key+'_Time', processsTimes.get(key)+'ms');
            Sum += processsTimes.get(key);
        }

        result.put('Avg_Time', (Sum / Math.Max(1, processsTimes.size()) )+'ms');

        return result;
    }

    private static Long SW() { return datetime.now().getTime(); }

    @testVisible
    private static ensxtx_DS_VCCharacteristicValues CreateValueForCharAndValue(ensxtx_DS_VCMaterialConfiguration initialConfig, string key, string value) {

        Integer charTot = initialConfig.characteristics.size();
        for (Integer charCnt = 0 ; charCnt < charTot ; charCnt++) {
            ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS c = initialConfig.characteristics[charCnt];
            if (key == c.CharacteristicName || key == c.CharacteristicDescription) {
                List<ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES> avList = initialConfig.indexedAllowedValues.get(c.CharacteristicName);
                Integer avTot = avList.size();
                for (Integer avCnt = 0 ; avCnt < avTot ; avCnt++)  {
                    ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES av = avList[avCnt];
                    if(value == av.CharacteristicValueDescription || value == av.CharacteristicValue) {
                        return CreateValueFromCharAndValue(c, av);
                    }
                }
                return CreateValueFromCharAndValue(c, value);
            }
        }

        return null;
    }

    private static ensxtx_DS_VCCharacteristicValues CreateValueFromCharAndValue(ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS c, string value) {
        ensxtx_DS_VCCharacteristicValues result = new ensxtx_DS_VCCharacteristicValues();
        result.CharacteristicID = c.CharacteristicID; 
        result.CharacteristicValue = value;
        result.CharacteristicName = c.CharacteristicName;
        result.UserModified = true;
        return result;
    }

    private static ensxtx_DS_VCCharacteristicValues CreateValueFromCharAndValue(ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS c, ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES av) {
        ensxtx_DS_VCCharacteristicValues result = new ensxtx_DS_VCCharacteristicValues();
        result.CharacteristicID = c.CharacteristicID; 
        result.CharacteristicValue = av.CharacteristicValue;
        result.CharacteristicName = av.CharacteristicName;
        result.CharacteristicValueDescription = av.CharacteristicValueDescription;
        result.UserModified = true;
        return result;
    }

    @testVisible
    private static void AssertSelectedValueSet(ensxtx_DS_VCMaterialConfiguration newConfig, string key, string value) {
        List<ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES> indexedSelectedValues = newConfig.indexedSelectedValues.get(key);

        System.assert(indexedSelectedValues.size() > 0, 'Value Not Set in result from process ' + key);
        if (null != indexedSelectedValues) {
            Integer svTot = indexedSelectedValues.size();
            for (Integer svCnt = 0 ; svCnt < svTot ; svCnt++) {
                ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES v = indexedSelectedValues[svCnt];
                System.debug('Value '+ v.CharacteristicValueDescription + '(' + v.CharacteristicValue + ') selected for ' + key);
            }
        }
    }
}