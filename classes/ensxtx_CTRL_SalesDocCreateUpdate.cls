public class ensxtx_CTRL_SalesDocCreateUpdate
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_CTRL_SalesDocCreateUpdate.class);

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response initSFObject(String recordId, String sapDocType)
    {
        logger.enterAura('initSFObject', new Map<String, Object>{
            'recordId' => recordId,
            'sapDocType' => sapDocType
        });

        ensxtx_UTIL_SalesDoc.SFObject result = ensxtx_UTIL_SalesDoc.getSFObjectInfo(recordId, sapDocType);

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response initSalesDocDetailFromSFObject(ensxtx_DS_Document_Detail salesDocDetail,
        ensxtx_UTIL_SalesDoc.SFObject sfObject, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        logger.enterAura('initSalesDocDetailFromSFObject', new Map<String, Object>{
            'salesDocDetail' => salesDocDetail,
            'sfObject' => sfObject,
            'appSettings' => appSettings
        });

        try
        {
            ensxtx_DS_Document_Detail newSalesDocDetail = ensxtx_UTIL_SalesDoc.mapSalesDocDetailFromSFObject(salesDocDetail, sfObject, appSettings);
            if (newSalesDocDetail != null) salesDocDetail = newSalesDocDetail;
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(salesDocDetail);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getSalesDocDetail(String salesDocNumber, ensxtx_DS_SalesDocAppSettings appSettings,
        ensxtx_UTIL_SalesDoc.SFObject sfObject)
    {
        logger.enterAura('getSalesDocDetail', new Map<String, Object>{
            'salesDocNumber' => salesDocNumber,
            'appSettings' => appSettings,
            'sfObject' => sfObject
        });

        ensxsdk.EnosixFramework.DetailSBO sbo = initializeSBODetail(appSettings.SBODetailType);
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();

        try
        {
            ensxsdk.EnosixFramework.DetailObject result = sbo.executeGetDetail(salesDocNumber);
            ensxtx_UTIL_Document_Detail.convertToObject(result, salesDocDetail, true, appSettings);

            SObject sfSObject = sfObject.sfMainObject;

            if (sfSObject != null && sfObject.sfLineItems != null)
            {
                Integer solTot = sfObject.sfLineItems.size();
                Integer itemTot = salesDocDetail.ITEMS.size();
                for (Integer solCnt = 0 ; solCnt < solTot ; solCnt++)
                {
                    SObject sfsObjectLine = sfObject.sfLineItems[solCnt];
                    String itemNumber = ensxtx_UTIL_SFSObjectDoc.getItemNumber(sfSObject, sfsObjectLine);
                    boolean isFound = false;
                    string materialNumber = ensxtx_UTIL_SFSObjectDoc.getMaterial(sfSObject, sfsObjectLine);
                    for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
                    {
                        ensxtx_DS_Document_Detail.ITEMS item = salesDocDetail.ITEMS[itemCnt];
                        if (itemNumber != null && 
                            itemNumber.replaceFirst('^0+(?!$)', '') == item.ItemNumber.replaceFirst('^0+(?!$)', '') && 
                            materialNumber == item.Material)
                        {
                            item.SFId = sfsObjectLine.Id;
                            break;
                        }                    
                    }
                }
            }

            ensxtx_UTIL_PageMessages.addFrameworkMessages(result.getMessages());
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(salesDocDetail);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getCustomerDetail(String customerNumber)
    {
        logger.enterAura('getCustomerDetail', new Map<String, Object>{
            'customerNumber' => customerNumber
        });

        ensxtx_SBO_EnosixCustomer_Detail.EnosixCustomer customerDetail = new ensxtx_SBO_EnosixCustomer_Detail.EnosixCustomer();
        ensxtx_SBO_EnosixCustomer_Detail sbo = new ensxtx_SBO_EnosixCustomer_Detail();

        try
        {
            customerDetail = sbo.getDetail(customerNumber);
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(customerDetail);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getShipInfo()
    {        
        logger.enterAura('getShipInfo', null);
        
        ensxtx_RFC_SD_GET_SHIP_INFO.RESULT result = new ensxtx_RFC_SD_GET_SHIP_INFO.RESULT();
        try
        {           
            result = ensxtx_UTIL_ShippingInfo.getShippingMaster();
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getPricingStat()
    {        
        logger.enterAura('getPricingStat', null);

        ensxtx_RFC_SD_GET_PRICING_STAT.RESULT result;
        try
        {
            ensxtx_RFC_SD_GET_PRICING_STAT rfc = new ensxtx_RFC_SD_GET_PRICING_STAT();            
            result = rfc.execute();
        }        
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getGroupOffice()
    {        
        logger.enterAura('getGroupOffice', null);

        ensxtx_RFC_SD_GET_GROUP_OFFICE.RESULT result;
        try
        {
            ensxtx_RFC_SD_GET_GROUP_OFFICE rfc = new ensxtx_RFC_SD_GET_GROUP_OFFICE();            
            result = rfc.execute();
        }        
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getConditionTypes(Boolean isHeader, String pricingProcedure)
    {
        logger.enterAura('getConditionTypes', new Map<String, Object>{
            'isHeader' => isHeader,
            'pricingProcedure' => pricingProcedure
        });

        ensxtx_RFC_Z_ENSX_GET_CONDITION_TYPES.RESULT result;
        try
        {
            ensxtx_RFC_Z_ENSX_GET_CONDITION_TYPES rfc = new ensxtx_RFC_Z_ENSX_GET_CONDITION_TYPES();
            rfc.PARAMS.IV_HEADER = isHeader ? 'B' : 'A';
            rfc.PARAMS.IV_KALSM = pricingProcedure;
            result = rfc.execute();
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getRejectionReasons()
    {
        logger.enterAura('getRejectionReasons', null);

        ensxtx_RFC_SD_GET_REJECTION_REASONS.RESULT result;
        try
        {
            ensxtx_RFC_SD_GET_REJECTION_REASONS rfc = new ensxtx_RFC_SD_GET_REJECTION_REASONS();            
            result = rfc.execute();
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getBillingPlans()
    {
        logger.enterAura('getBillingPlans', null);

        ensxtx_RFC_SD_GET_PERIO.RESULT result;
        try
        {
            ensxtx_RFC_SD_GET_PERIO rfc = new ensxtx_RFC_SD_GET_PERIO();            
            result = rfc.execute();
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getCountries()
    {
        logger.enterAura('getCountries', null);

        ensxtx_RFC_SD_GET_COUNTRIES.RESULT result;
        try
        {
            ensxtx_RFC_SD_GET_COUNTRIES rfc = new ensxtx_RFC_SD_GET_COUNTRIES();
            result = rfc.execute();
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getTaxJurisdictions(String city, String country, String region, String postalCode)
    {
        logger.enterAura('getTaxJurisdictions', null);

        ensxtx_RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT result;
        try
        {
            ensxtx_RFC_Z_ENSX_GET_TAX_JURISDICTION rfc = new ensxtx_RFC_Z_ENSX_GET_TAX_JURISDICTION();
            rfc.PARAMS.IV_CITY = city;
            rfc.PARAMS.IV_COUNTRY = country;
            rfc.PARAMS.IV_REGION = region;
            rfc.PARAMS.IV_ZIPCODE = postalCode;
            result = rfc.execute();
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getItemCategories(ensxtx_DS_Document_Detail.SALES salesDetail, String material)
    {
        logger.enterAura('getItemCategories', null);

        ensxtx_RFC_SD_GET_ITEMCAT_VALUES.RESULT result;
        try
        {
            ensxtx_RFC_SD_GET_ITEMCAT_VALUES rfc = new ensxtx_RFC_SD_GET_ITEMCAT_VALUES();            
            rfc.PARAMS.IV_AUART = salesDetail.SalesDocumentType;
            rfc.PARAMS.IV_VKORG = salesDetail.SalesOrganization;
            rfc.PARAMS.IV_VTWEG = salesDetail.DistributionChannel;
            rfc.PARAMS.IV_MATNR = material;
            result = rfc.execute();
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(result);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response validateProductsInSalesforce(List<String> materials, String pricebookId)
    {
        logger.enterAura('validateProductsInSalesforce', new Map<String, Object>{
            'materials' => materials,
            'pricebookId' => pricebookId
        });

        Set<String> materialNumbers = new Set<String>(materials);

        Map<String, PricebookEntry> materialNumberPbe = new Map<String, PricebookEntry>();

        try
        {
            ensxtx_UTIL_SalesDoc.validateMaterials(materialNumbers, pricebookId, materialNumberPbe);
        }
        catch (ensxtx_UTIL_SalesDoc.MissingMaterialsException ex) 
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }

        return ensxtx_UTIL_Aura.createResponse(materialNumberPbe.keySet());
    }    

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getMaterialsDetail(List<String> materials, String salesOrg, String distChannel)
    {
        logger.enterAura('getMaterialsDetail', new Map<String, Object>{
            'materials' => materials,
            'salesOrg' => salesOrg,
            'distChannel' => distChannel
        });

        Map<String, MaterialDetail> materialsDetail = new Map<String, MaterialDetail>();
        ensxtx_SBO_EnosixMaterial_Detail sbo = new ensxtx_SBO_EnosixMaterial_Detail();
        ensxtx_SBO_EnosixMaterial_Detail.EnosixMaterial result = new ensxtx_SBO_EnosixMaterial_Detail.EnosixMaterial();
        
        Integer matTot = materials.size();
        for (Integer matCnt = 0 ; matCnt < matTot ; matCnt++)
        {        
            String material = materials[matCnt];    
            try
            {                
                result = sbo.getDetail(material);

                if (result.isSuccess() != null && !result.isSuccess()) {
                    ensxtx_UTIL_PageMessages.addFrameworkMessages(result.getMessages());
                    continue;
                }
                MaterialDetail matDetail = new MaterialDetail(result.MaterialDescription, result.BASIC_DATA_2.ConfigurableMaterial);

                List<ensxtx_SBO_EnosixMaterial_Detail.PLANT_DATA> plantList = result.PLANT_DATA.getAsList();
                Integer plantTot = plantList.size();
                for (Integer plantCnt = 0 ; plantCnt < plantTot ; plantCnt++)
                {         
                    ensxtx_SBO_EnosixMaterial_Detail.PLANT_DATA plant = plantList[plantCnt];           
                    MaterialPlant newPlant = new MaterialPlant();
                    newPlant.Plant = plant.Plant;
                    newPlant.PlantName = plant.Name;
                    newPlant.SalesOrganization = plant.SalesOrganization;
                    newPlant.DistributionChannel = plant.DistributionChannel;
                    matDetail.Plants.add(newPlant);
                }

                materialsDetail.put(material, matDetail);
            }
            catch(Exception ex)
            {
                System.debug('Invalid Material: ' + ex);
            }
        }

        return ensxtx_UTIL_Aura.createResponse(materialsDetail);
    }

    // Simple object for Material Detail
    public class MaterialDetail {

        public MaterialDetail(String MaterialDescription, Boolean ConfigurableMaterial) 
        {
            this.MaterialDescription = MaterialDescription;
            this.ConfigurableMaterial = ConfigurableMaterial;
            this.Plants = new List<MaterialPlant>();
        }

        @AuraEnabled public String MaterialDescription { get; set; }
        @AuraEnabled public Boolean ConfigurableMaterial { get; set; }
        @AuraEnabled public List<MaterialPlant> Plants { get; set; }
    }

    public class MaterialPlant {
        @AuraEnabled public String Plant { get; set; }
        @AuraEnabled public String PlantName { get; set; }
        @AuraEnabled public String SalesOrganization { get; set; }
        @AuraEnabled public String DistributionChannel { get; set; }
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getReferenceDocument(String salesDocNumber, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        logger.enterAura('getReferenceDocument', new Map<String, Object>{
            'salesDocNumber' => salesDocNumber,
            'appSettings' => appSettings
        });

        ensxsdk.EnosixFramework.DetailSBO sbo = initializeSBODetail(appSettings.SBODetailType);
        ensxtx_DS_Document_Detail salesDocDetail = new ensxtx_DS_Document_Detail();

        try
        {
            ensxsdk.EnosixFramework.DetailObject result = ensxtx_UTIL_Document_Detail.buildSBOForReference(
                salesDocNumber, appSettings.DefaultDocType, appSettings.SBODetailType);

            String referenceCommand = ensxtx_UTIL_Document_Detail.getReferenceDocumentCommand(appSettings.SBODetailType);

            result = sbo.executeCommand(referenceCommand, result);

            if (result.isSuccess()) {
                ensxtx_UTIL_Document_Detail.convertToObject(result, salesDocDetail, true, appSettings);
            }

            ensxtx_UTIL_PageMessages.addFrameworkMessages(result.getMessages());
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
            return ensxtx_UTIL_Aura.createResponse(null);
        }

        return ensxtx_UTIL_Aura.createResponse(salesDocDetail);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response simulateSalesDoc(ensxtx_DS_Document_Detail salesDocDetail, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        // Log size can be too big
        // logger.enterAura('simulateSalesDoc', new Map<String, Object>{
        //     'salesDocDetail' => salesDocDetail,
        //     'appSettings' => appSettings
        // });

        ensxsdk.EnosixFramework.DetailSBO sbo = initializeSBODetail(appSettings.SBODetailType);

        try
        {
            Integer startCpu = Limits.getCpuTime();
            Integer startHeap = Limits.getHeapSize();
            System.debug('Start getCpuTime(): '+ startCpu);
            System.debug('Start getHeapSize(): '+ startHeap);

            if (salesDocDetail == null) salesDocDetail = new ensxtx_DS_Document_Detail();
            ensxsdk.EnosixFramework.DetailObject result = ensxtx_UTIL_Document_Detail.convertToSBO(null, salesDocDetail, false, appSettings);
            String simulateCommand = ensxtx_UTIL_Document_Detail.getSimulateCommand(appSettings.SBODetailType);

            Boolean is3RD2 = false;
            if (salesDocDetail.SALES.IncotermsPart1 == '3RD2') {
                is3RD2 = true;
            }

            DateTime startTimer = System.now();
            System.debug('Start Timer: '+ startTimer);

            result.setBoolean(true,'useTextSerialize');
            result = sbo.executeCommand(simulateCommand, result);

            DateTime endTimer = System.now();
            System.debug('End Timer: '+ endTimer);
            System.debug('MilliSecond Simulate: ' + (endTimer.getTime() - startTimer.getTime()));

            if (result.isSuccess()) {
                //result = getPricingScalesAndMaterialAvailabity(sbo, result, salesDocDetail, appSettings);
                ensxtx_UTIL_Document_Detail.convertToObject(result, salesDocDetail, false, appSettings);
            }

            if (is3RD2) {
                // Map back to 3RD2 after simulation
                salesDocDetail.SALES.IncotermsPart1 = '3RD2';
            }

            ensxtx_UTIL_PageMessages.addFrameworkMessages(result.getMessages());

            Integer endCpu = Limits.getCpuTime();
            Integer endHeap = Limits.getHeapSize();
            System.debug('End getHeapSize(): '+ endHeap);
            System.debug('End getCpuTime(): '+ endCpu);
            System.debug('Heap diff: ' + (endHeap - startHeap));
            System.debug('CPU diff: ' + (endCpu - startCpu));
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }

        return ensxtx_UTIL_Aura.createResponse(salesDocDetail);
    }

    // private static ensxsdk.EnosixFramework.DetailObject getPricingScalesAndMaterialAvailabity(ensxsdk.EnosixFramework.DetailSBO sbo, ensxsdk.EnosixFramework.DetailObject detailObj,
    //     ensxtx_DS_Document_Detail salesDocDetail, ensxtx_DS_SalesDocAppSettings appSettings)
    // {
    //     String materialAvailabilityCommand = ensxtx_UTIL_Document_Detail.getMaterialAvailabilityCommand(appSettings.SBODetailType);
    //     String priceScaleCommand = ensxtx_UTIL_Document_Detail.getPriceScaleCommand(appSettings.SBODetailType);

    //     if (String.isNotEmpty(materialAvailabilityCommand) && String.isNotEmpty(priceScaleCommand) && salesDocDetail.ITEMS.size() > 0)
    //     {
    //         ensxtx_DS_Document_Detail documentDetail = new ensxtx_DS_Document_Detail();
    //         documentDetail.SoldToParty = salesDocDetail.SoldToParty;
    //         documentDetail.SALES = new ensxtx_DS_Document_Detail.SALES();
    //         documentDetail.SALES.SalesOrganization = salesDocDetail.SALES.SalesOrganization;
    //         documentDetail.SALES.DistributionChannel = salesDocDetail.SALES.DistributionChannel;
    //         documentDetail.SALES.Division = salesDocDetail.SALES.Division;

    //         documentDetail.ITEMS = new List<ensxtx_DS_Document_Detail.ITEMS>();
    //         for (ensxtx_DS_Document_Detail.ITEMS item : salesDocDetail.ITEMS)
    //         {
    //             ensxtx_DS_Document_Detail.ITEMS newItem = new ensxtx_DS_Document_Detail.ITEMS();
    //             newItem.ItemNumber = item.ItemNumber;
    //             newItem.Material = item.Material;
    //             newItem.OrderQuantity = item.OrderQuantity;
    //             documentDetail.ITEMS.add(newItem);
    //         }

    //         ensxsdk.EnosixFramework.DetailObject sboDetail = ensxtx_UTIL_Document_Detail.convertToSBO(null, documentDetail, false, appSettings);

    //         ensxsdk.EnosixFramework.DetailObject materialAvailabilityResult = sbo.executeCommand(materialAvailabilityCommand, sboDetail);
    //         ensxsdk.EnosixFramework.DetailObject priceScaleResult = sbo.executeCommand(priceScaleCommand, sboDetail);

    //         if (materialAvailabilityResult.isSuccess() && priceScaleResult.isSuccess()) {
    //             detailObj = ensxtx_UTIL_Document_Detail.getMatAvailabilityAndPriceScale(detailObj, materialAvailabilityResult, priceScaleResult, appSettings.SBODetailType);
    //         }
    //     }

    //     return detailObj;
    // }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response saveToSObject(ensxtx_UTIL_SalesDoc.SFObject sfObject, ensxtx_DS_Document_Detail salesDocDetail, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        logger.enterAura('createSAPDocument', new Map<String, Object>{
            'sfObject' => sfObject,
            'salesDocDetail' => salesDocDetail,
            'appSettings' => appSettings
        });

        Boolean isSuccess = true;
        try
        {
            ensxtx_UTIL_SalesDoc.saveHeaderAndLineItems(sfObject, salesDocDetail, appSettings);
        }
        catch (Exception ex)
        {
            isSuccess = false;
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }
        return ensxtx_UTIL_Aura.createResponse(isSuccess);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response createSAPDocument(ensxtx_UTIL_SalesDoc.SFObject sfObject,
        ensxtx_DS_Document_Detail salesDocDetail, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        // Log size can be too big
        // logger.enterAura('createSAPDocument', new Map<String, Object>{
        //     'sfObject' => sfObject,
        //     'salesDocDetail' => salesDocDetail,
        //     'appSettings' => appSettings
        // });

        if (salesDocDetail == null) return ensxtx_UTIL_Aura.createResponse(null);

        ensxsdk.EnosixFramework.DetailSBO sbo = initializeSBODetail(appSettings.SBODetailType);

        try
        {
            Integer startCpu = Limits.getCpuTime();
            Integer startHeap = Limits.getHeapSize();
            System.debug('Start getCpuTime(): '+ startCpu);
            System.debug('Start getHeapSize(): '+ startHeap);

            // Only set during create
            // Same logic from old order create
            for (ensxtx_DS_Document_Detail.TEXTS text : salesDocDetail.TEXTS) {
                if (salesDocDetail.SALES.IncotermsPart1 == '3RD2' && text.TextID == 'ZBOL') {
                    text.Text = 'Bill Freight To: ' + salesDocDetail.FreightBillingName + ' ' +
                        salesDocDetail.FreightBillingNAddress + ' ' + salesDocDetail.FreightBillingCityStatsZip + ' ' + 
                        salesDocDetail.FreightBillingNotes + ' ' + (String.isNotEmpty(text.Text) ? text.Text : '');
                }
                else if (salesDocDetail.SALES.IncotermsPart1 == '3RD' && text.TextID == 'ZBOL') {
                    text.Text = 'Bill Freight To: ' + salesDocDetail.BillingAddressInfo + ' ' +
                        salesDocDetail.FreightBillingNotes + ' ' + (String.isNotEmpty(text.Text) ? text.Text : '');
                }
            }
            Boolean is3RD2 = false;
            if (salesDocDetail.SALES.IncotermsPart1 == '3RD2') {
                is3RD2 = true;
            }

            ensxsdk.EnosixFramework.DetailObject result = ensxtx_UTIL_Document_Detail.convertToSBO(null, salesDocDetail, false, appSettings);

            DateTime startTimer = System.now();
            System.debug('Start Timer: '+ startTimer);

            result.setBoolean(true,'useTextSerialize');
            result = sbo.executeSave(result);

            DateTime endTimer = System.now();
            System.debug('End Timer: '+ endTimer);
            System.debug('MilliSecond Create: ' + (endTimer.getTime() - startTimer.getTime()));

            ensxtx_UTIL_Document_Detail.convertToObject(result, salesDocDetail, false, appSettings);
            ensxtx_UTIL_PageMessages.addFrameworkMessages(result.getMessages());

            if (salesDocDetail.IsSuccess && sfObject.initFromSObject)
            {
                ensxtx_UTIL_SalesDoc.saveHeaderAndLineItems(sfObject, salesDocDetail, appSettings);
            }

            if (is3RD2) {
                // Map back to 3RD2 after simulation
                salesDocDetail.SALES.IncotermsPart1 = '3RD2';
            }

            Integer endCpu = Limits.getCpuTime();
            Integer endHeap = Limits.getHeapSize();
            System.debug('End getHeapSize(): '+ endHeap);
            System.debug('End getCpuTime(): '+ endCpu);
            System.debug('Heap diff: ' + (endHeap - startHeap));
            System.debug('CPU diff: ' + (endCpu - startCpu));
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }

        return ensxtx_UTIL_Aura.createResponse(salesDocDetail);
    }

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response updateSAPDocument(ensxtx_UTIL_SalesDoc.SFObject sfObject,
        ensxtx_DS_Document_Detail salesDocDetail, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        // Log size can be too big
        // logger.enterAura('updateSAPDocument', new Map<String, Object>{
        //     'sfObject' => sfObject,
        //     'salesDocDetail' => salesDocDetail,
        //     'appSettings' => appSettings
        // });

        ensxsdk.EnosixFramework.DetailSBO sbo = initializeSBODetail(appSettings.SBODetailType);

        try
        {
            Integer startCpu = Limits.getCpuTime();
            Integer startHeap = Limits.getHeapSize();
            System.debug('Start getCpuTime(): '+ startCpu);
            System.debug('Start getHeapSize(): '+ startHeap);

            ensxsdk.EnosixFramework.DetailObject result = sbo.executeGetDetail(salesDocDetail.SalesDocument);
            result = ensxtx_UTIL_Document_Detail.removeAllConditions(result, appSettings.SBODetailType);
            result = ensxtx_UTIL_Document_Detail.convertToSBO(result, salesDocDetail, true, appSettings);

            // Need to call Simulate before update because there are new added items
            if (salesDocDetail.callSimulateBeforeUpdate == true) {
                System.debug('Calling simulate before update');
                String simulateCommand = ensxtx_UTIL_Document_Detail.getSimulateCommand(appSettings.SBODetailType);
                result.setBoolean(true,'useTextSerialize');
                result = sbo.executeCommand(simulateCommand, result);
                result = ensxtx_UTIL_Document_Detail.updateTextFields(result, salesDocDetail, appSettings.SBODetailType);
            }

            DateTime startTimer = System.now();
            System.debug('Start Timer: '+ startTimer);

            result.setBoolean(true,'useTextSerialize');
            result = sbo.executeSave(result);

            DateTime endTimer = System.now();
            System.debug('End Timer: '+ endTimer);
            System.debug('MilliSecond Updated: ' + (endTimer.getTime() - startTimer.getTime()));

            ensxtx_UTIL_Document_Detail.convertToObject(result, salesDocDetail, false, appSettings);
            ensxtx_UTIL_PageMessages.addFrameworkMessages(result.getMessages());

            if (salesDocDetail.IsSuccess && sfObject.initFromSObject)
            {
                ensxtx_UTIL_SalesDoc.saveHeaderAndLineItems(sfObject, salesDocDetail, appSettings);
            }

            Integer endCpu = Limits.getCpuTime();
            Integer endHeap = Limits.getHeapSize();
            System.debug('End getHeapSize(): '+ endHeap);
            System.debug('End getCpuTime(): '+ endCpu);
            System.debug('Heap diff: ' + (endHeap - startHeap));
            System.debug('CPU diff: ' + (endCpu - startCpu));
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }

        return ensxtx_UTIL_Aura.createResponse(salesDocDetail);
    }

    private static ensxsdk.EnosixFramework.DetailSBO initializeSBODetail(String sboDetailType)
    {
        String className = 'ensxtx_SBO_Enosix' + sboDetailType + '_Detail';
        System.Type detailType = Type.forName(className);
        return (ensxsdk.EnosixFramework.DetailSBO) detailType.newInstance();
    }
}