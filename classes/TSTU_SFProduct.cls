/*
*Test class for UTIL_SFProduct
*/
@isTest
public class TSTU_SFProduct
{
    @isTest
    static void test_getProductById()
    {
        Product2 prod = new Product2();
        prod.Name = 'TestProduct';
        upsert prod;

        Test.startTest();
        Product2 fetched1 = UTIL_SFProduct.getProductById(prod.Id);
        Product2 fetched2 = UTIL_SFProduct.getProductById(prod.Id, new List<String> { 'Name', '', null });
        Product2 fetched3 = UTIL_SFProduct.getProductById(null);
        Test.stopTest();

        System.assertEquals(prod.Name, fetched1.Name);
        System.assertEquals(prod.Id, fetched1.Id);

        System.assertEquals(prod.Name, fetched2.Name);
        System.assertEquals(prod.Id, fetched2.Id);

        System.assertEquals(null, fetched3);
    }

    @isTest
    static void test_isProductLinkedToMaterial()
    {
        Product2 prod = new Product2();
        prod.Name = 'Dummy Prod';
        prod.put(UTIL_SFProduct.MaterialFieldName, '1234');
        upsert prod;

        Product2 prod2 = new Product2();
        prod2.Name = 'Dummy Prod';
        upsert prod2;

        System.assertEquals(false, UTIL_SFProduct.isProductLinkedToMaterial(null));
        System.assertEquals(false, UTIL_SFProduct.isProductLinkedToMaterialByProductId(null));
        System.assert(UTIL_SFProduct.isProductLinkedToMaterial(prod));
        System.assertEquals(false, UTIL_SFProduct.isProductLinkedToMaterial(prod2));
    }

    @isTest
    static void test_getProductByMaterialNumber()
    {
        Product2 prod = new Product2();
        prod.Name = 'Dummy Prod';
        prod.put(UTIL_SFProduct.MaterialFieldName, '1234');
        upsert prod;

        Product2 fetched1 = UTIL_SFProduct.getProductByMaterialNumber('1234');

        System.assertEquals(prod.Id, fetched1.Id);
    }

    @isTest
    static void test_getValueFromProductField()
    {
        Product2 prod = new Product2();
        prod.Name = 'Dummy Prod';
        UTIL_SFProduct.getValueFromProductField(null, '', 'Error');
        UTIL_SFProduct.getValueFromProductField(prod, 'BadFieldName', 'Error');
        UTIL_SFProduct.getValueFromProductField(prod, 'Name', 'Error');
        prod.Name = null;
        UTIL_SFProduct.getValueFromProductField(prod, 'Name', 'Error');
    }

    @isTest
    static void test_setProductMaterialNumber()
    {
        Product2 prod = new Product2();
        prod.Name = 'Dummy Prod';
        upsert prod;

        UTIL_SFProduct.setProductMaterialNumber(prod, '1234');

        System.assertEquals('1234', prod.get(UTIL_SFProduct.MaterialFieldName));
    }

    @isTest
    static void test_getProductByFields() {
        Product2 product = createTestProduct2();
        insert product;
        string sField = 'Name';
        List<String> vals = new List<String> {product.Name};
        List<STring> fields = new List<String> {
            'name',
            'productcode',
            'description'};
        Map<Id, Product2> prods = UTIL_SFProduct.getProductsByField(sField, vals, fields);
    }

    @isTest
    public static void test_getSAPMaterialNumbersFromProductList()
    {
        Test.startTest();

        // Create a fake product and do an insert
        Product2 product1 = new Product2();
        product1.IsActive = true;
        product1.Name = 'FakeProd';
        product1.ProductCode = '2WJD83';
        String materialFieldValue = '12345';
        UTIL_SFProduct.setProductMaterialNumber(product1, materialFieldValue);
        insert product1;
        System.debug('product1='+JSON.serialize(product1));

        SET<Id> testIds = new SET<Id>();
        testIds.add(product1.Id);
        MAP<Id, String> result2 = UTIL_SFProduct.getSAPMaterialNumbersFromProductList(testIds);
        System.assertEquals(materialFieldValue, result2.get(product1.Id));

        Test.stopTest();
    }

    public static Product2 createTestProduct2() {
        Product2 product = new Product2(
            Name = 'Mock Product',
            ProductCode = 'TEST'
        );
        return product;
    }
}