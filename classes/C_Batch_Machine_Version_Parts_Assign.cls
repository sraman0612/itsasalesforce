global class C_Batch_Machine_Version_Parts_Assign implements Database.Batchable<sObject> {
    
    global final String Query = 'SELECT Id, Machine_Version_Parts_List__c, MPG4_Code__c, DelDate__c, Model__c From Asset WHERE MPG4_Code__c IN (SELECT MPG4_Code_Lookup__c FROM Machine_Version_Parts_List__c) AND Machine_Version_Parts_List__c = null AND DelDate__c >= 1998-01-01 AND DChl__c=\'CM\'';
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Asset> scope){
        List<Id> mpg4Codes = new List<Id>();
        for(Asset a : scope){
            mpg4Codes.add(a.MPG4_Code__c);
        }        
        List<Machine_Version_Parts_List__c> mvpls = [SELECT Id, Start_Date__c, End_Date__c, MPG4_Code__c, MPG4_Code_Lookup__c, Configuration__c FROM Machine_Version_Parts_List__c WHERE MPG4_Code_Lookup__c IN :mpg4Codes ORDER BY Start_Date__c ASC];
        
        if(mvpls.size() == 0){
            return;
        }
        
        Map<Id, List<Machine_Version_Parts_List__c>> mvplMap = new Map<Id, List<Machine_Version_Parts_List__c>>();
        for(Machine_Version_Parts_List__c mvpl : mvpls){
            if(mvplMap.containsKey(mvpl.MPG4_Code_Lookup__c)){
                System.debug('Adding Version : ' + mvpl.MPG4_Code_Lookup__c);
                mvplMap.get(mvpl.MPG4_Code_Lookup__c).add(mvpl);
            }
            else{
                System.debug('Adding Key : ' + mvpl.MPG4_Code_Lookup__c);
                mvplMap.put(mvpl.MPG4_Code_Lookup__c, new List<Machine_Version_Parts_List__c>{mvpl});                
            }
        }
        
        for(Asset a : scope){
            System.debug(a);
            if(mvplMap.containsKey(a.MPG4_Code__c)){
                for(Machine_Version_Parts_List__c mvpl : mvplMap.get(a.MPG4_Code__c)){
                    if(a.Model__c == mvpl.Configuration__c){
                        if(a.DelDate__c >= mvpl.Start_Date__c){
                            if(mvpl.End_Date__c != null){
                                if(a.DelDate__c <= mvpl.End_Date__c){
                                    a.Machine_Version_Parts_List__c = mvpl.Id;
                                }
                            }
                            else{
                                a.Machine_Version_Parts_List__c = mvpl.Id;
                            }
                        }                
                    }
                }
            }
            else{
                System.debug('Map did not contain the following Key : ' + a.MPG4_Code__c);
            }
            if(a.Machine_Version_Parts_List__c == null){
                //For the current time this is OK, as not all MVPLs are in for the entire Product lineup
                System.debug('No MVPL found for Serial Number : ' + a.Id);
            }
            
        }
        
        update scope;
        
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
    
}