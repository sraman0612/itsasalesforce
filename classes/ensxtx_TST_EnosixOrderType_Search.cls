/// enosiX Inc. Generated Apex Model
/// Generated On: 8/5/2020 5:36:05 PM
/// SAP Host: From REST Service On: https://platform-force-5128-dev-ed.cs95.my.salesforce.com
/// CID: From REST Service On: https://platform-force-5128-dev-ed.cs95.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class ensxtx_TST_EnosixOrderType_Search
{

    public class Mockensxtx_SBO_EnosixOrderType_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixOrderType_Search.class, new Mockensxtx_SBO_EnosixOrderType_Search());
        ensxtx_SBO_EnosixOrderType_Search sbo = new ensxtx_SBO_EnosixOrderType_Search();
        System.assertEquals(ensxtx_SBO_EnosixOrderType_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        ensxtx_SBO_EnosixOrderType_Search.EnosixOrderType_SC sc = new ensxtx_SBO_EnosixOrderType_Search.EnosixOrderType_SC();
        System.assertEquals(ensxtx_SBO_EnosixOrderType_Search.EnosixOrderType_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        ensxtx_SBO_EnosixOrderType_Search.SEARCHPARAMS childObj = new ensxtx_SBO_EnosixOrderType_Search.SEARCHPARAMS();
        System.assertEquals(ensxtx_SBO_EnosixOrderType_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.SalesDocumentType = 'X';
        System.assertEquals('X', childObj.SalesDocumentType);

        childObj.SO = true;
        System.assertEquals(true, childObj.SO);

        childObj.QT = true;
        System.assertEquals(true, childObj.QT);

        childObj.ARM = true;
        System.assertEquals(true, childObj.ARM);


    }

    @isTest
    static void testEnosixOrderType_SR()
    {
        ensxtx_SBO_EnosixOrderType_Search.EnosixOrderType_SR sr = new ensxtx_SBO_EnosixOrderType_Search.EnosixOrderType_SR();

        sr.registerReflectionForClass();

        System.assertEquals(ensxtx_SBO_EnosixOrderType_Search.EnosixOrderType_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        ensxtx_SBO_EnosixOrderType_Search.SEARCHRESULT childObj = new ensxtx_SBO_EnosixOrderType_Search.SEARCHRESULT();
        System.assertEquals(ensxtx_SBO_EnosixOrderType_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        ensxtx_SBO_EnosixOrderType_Search.SEARCHRESULT_COLLECTION childObjCollection = new ensxtx_SBO_EnosixOrderType_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.SalesDocumentType = 'X';
        System.assertEquals('X', childObj.SalesDocumentType);

        childObj.SalesDocumentTypeDescription = 'X';
        System.assertEquals('X', childObj.SalesDocumentTypeDescription);


    }

}