@isTest
public class SFS_Get_LaborRate_Interface_Test {
    @isTest
    public static void SFS_Get_LaborRate_Interface_Test()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
		
        System.debug('response code' + res.getStatusCode());
        List<Account> acct=SFS_TestDataFactory.createAccounts(2, false);
        acct[0].AccountSource='Web';
        acct[0].IRIT_Customer_Number__c='1234';
        acct[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acct[0].IRIT_Payment_Terms__c='BANKCARD';
        acct[0].name = 'Test Account';
        acct[0].ShippingCountry='USA';
        acct[0].shippingStreet='1412 Fox Run Parkway';
        acct[0].ShippingPostalCode='36804';
        acct[0].ShippingState='AL';
        insert acct[0]; 
        
        acct[1].Bill_To_Account__c = acct[0].Id;
        acct[1].AccountSource='Web';
        acct[1].name = 'test account';
        acct[1].CurrencyIsoCode = 'USD';
        acct[1].IRIT_Customer_Number__c='1234';
        acct[1].Type='Prospect';
        acct[1].Currency__c='USD';
        acct[1].Account_Region__c ='Oracle 11i';
        insert acct[1];
        
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct[1].Id ,true);
        sc[0].SFS_External_Id__c='1234';
        update sc[0];
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct[1].Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        wo[0].SFS_External_Id__c='1234';
        update wo;
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        List<ID> woliIdList=new List<ID>();
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId=wo[0].Id,
                                                       SFS_Quote_Number__c='1236589');
        insert woli;
        woliList.add(woli);
        woliIdList.add(woli.Id);
        /*List<SFS_Get_LaborRate_Interface.getLaborRate> sg1=new List<SFS_Get_LaborRate_Interface.getLaborRate>();
        SFS_Get_LaborRate_Interface.getLaborRate sg=new SFS_Get_LaborRate_Interface.getLaborRate();
        sg.currencyCode='USD';
        sg.recId=woliList[0].ID;
        sg1.add(sg);*/
        SFS_Get_LaborRate_Interface.GetLaborRate(woliIdList);
       // try{
       System.debug('response code' + res.getStatusCode());
        SFS_Get_LaborRate_Interface.getLaborRateResponse(res,'test!'+woliList[0].id);
        
        //}
       // catch(exception e){
            
       // }
    }
}