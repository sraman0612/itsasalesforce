/* Author: Srikanth P - Capgemini
 * Date: 19/1/22
 * Description: Outbound integration to CPI. Service Agreement with Work Order.
 * TODO:
 */
public class SFS_WorkOrderWithSvgOutboundHandler {
   @invocableMethod(label = 'Outbound WorkOrder With Service Agreement' description = 'Send to CPI' Category = 'WorkOrder')
    public static void Run(List<WorkOrder> newWorkOrderId){
       //Main Method
        String xmlString = '';
        Map<Double, String> WoxmlMap = new Map<Double, String>();        
        ProcessData(newWorkOrderId, WoxmlMap,xmlString);

    }
    public static void ProcessData(List<WorkOrder> newWorkOrderId, Map<Double, String> WoxmlMap, String xmlString){
        //Retrieves and transforms data. 
        List<String> XMLList = new List<String>();
        //Get XML lines from SFS_Service_Agreement_With_WO_XML__mdt. 
        SFS_Service_Agreement_With_WO_XML__mdt[] woSvgMaps = [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                         FROM SFS_Service_Agreement_With_WO_XML__mdt Order By SFS_Node_Order__c asc];
        //Query the record from flow. Used to get record field names.
        //String query = 'SELECT Id,sfs_project_type__c,sfs_chargeable_flag_wo__c,sfs_oracle_billing_method_wo__c,Entitlement.ServiceContract.AccountId,Entitlement.ServiceContractId,SFS_Division__c,AccountId,ServiceContractId,SFS_Bill_to_Account__c,SFS_External_Id__c,SFS_Oracle_End_Date__c,SFS_Oracle_Start_Date__c,SFS_Chargeable__c,SFS_Oracle_Ready_To_Bill__c,SFS_Oracle_Billing_Method__c,Description,SFS_Ready_to_Distibute__c, AssetId,SFS_Party_Site_Use_Id__c, SFS_Integration_Status__c, SFS_Integration_Response__c,SFS_WorkOrder_Ready_To_Distribute__c from WorkOrder WHERE ID =: newWorkOrderId';
        //String query2 = 'SELECT Id,SFS_External_Id__c,sfs_chargeable_flag_wo__c,sfs_oracle_billing_method_wo__c,Entitlement.ServiceContract.AccountId,Entitlement.ServiceContractId,SFS_Division__c,AccountId,ServiceContractId,SFS_Bill_to_Account__c,SFS_External_Id__c,SFS_Oracle_End_Date__c,SFS_Oracle_Start_Date__c,SFS_Chargeable__c,SFS_Oracle_Ready_To_Bill__c,SFS_Oracle_Billing_Method__c,Description,SFS_Ready_to_Distibute__c, AssetId,SFS_Party_Site_Use_Id__c, SFS_Integration_Status__c, SFS_Integration_Response__c from WorkOrder WHERE ID =: newWorkOrderId';        
        WorkOrder wo = new WorkOrder();
        Map<String, Object> workOrderFields = new Map <String, Object>();
        try{
            wo =  newWorkOrderId[0];
            workOrderFields = wo.getPopulatedFieldsAsMap();
            
        }catch(System.Exception e){ 
           wo.SFS_Integration_Status__c = 'ERROR';
           wo.SFS_Integration_Response__c = String.ValueOf(e);
           update wo;
        }
        System.debug('@@@WORKORDERENTITLEMENT: ' + wo.Entitlement.ServiceContractId);
        //System.debug('@@@@@EntitlementId   ' + wo.EntitlementId.ServiceContractId);
        
        
        ServiceContract svg = new ServiceContract();
        Map<String, Object> ServiceContractFields = new Map <String, Object>();
        try{
          //  
          		List<Entitlement> contrId = [SELECT Id, ServiceContractId FROM Entitlement WHERE Id =: wo.EntitlementId LIMIT 1];
                svg = [SELECT Id,SFS_External_id__c,SFS_Oracle_Type__c, AccountId,SFS_Oracle_Start_Date__c, SFS_Oracle_End_Date__c,SFS_Oracle_Status__c,
                             CurrencyIsoCode,SFS_Agreement_Value__c,Description,SFS_PO_Number__c, sfs_ship_to_party_site_use__c,sfs_ship_to_site_id__c,sfs_sales_order__c
                             from ServiceContract WHERE ID =: contrId[0].ServiceContractId];  
          //  }else{
            /*    svg = [SELECT Id,SFS_Siebel_Id__c,SFS_Oracle_Type__c, AccountId,SFS_Oracle_Start_Date__c, SFS_Oracle_End_Date__c,SFS_Oracle_Status__c,
                             CurrencyIsoCode,SFS_Agreement_Value__c,Description,SFS_PO_Number__c, sfs_ship_to_party_site_use__c
                             from ServiceContract WHERE ID =: wo.Entitlement.ServiceContractId];  
            }*/
            
            
            ServiceContractFields = svg.getPopulatedFieldsAsMap();
        }catch(System.Exception e){
            wo.SFS_Integration_Status__c = 'ERROR';
            wo.SFS_Integration_Response__c = String.ValueOf(e);
            System.debug('SVG: ' + e);
            update wo;
        }
     
        
        Account workOrderAcc = new Account();
        Map<String, Object> AccountFields = new Map <String, Object>(); 
        try{
        workOrderAcc = [SELECT Id,SFS_Oracle_Site_Use_Id__c,IRIT_Customer_Number__c, CTS_Global_Shipping_Address_1__c,ShippingStreet,ShippingCity, CTS_Global_Shipping_Address_2__c, County__c, CTS_Global_Shipping_City__c,
                                   CTS_Global_Shipping_State_Province__c,shippingstate,shippingpostalcode, shippingcountry, Country__c, CTS_Global_Shipping_Postal_Code__c, IRIT_Payment_Terms__c, SFS_Oracle_Country__c,Siebel_ID__c
                                   FROM Account WHERE Id =: Wo.AccountId];         
        
        
            AccountFields = workOrderAcc.getPopulatedFieldsAsMap();
        }catch(System.Exception e){
            wo.SFS_Integration_Status__c = 'ERROR';
            wo.SFS_Integration_Response__c = String.ValueOf(e);
            System.debug('ACC: ' + e);
            update wo;
        }
        
        double dropNode = 0;
        if (workOrderAcc.shippingcountry != 'USA' && workOrderAcc.ShippingCountry != 'US' && workOrderAcc.ShippingCountry != 'United States') {
            SFS_Service_Agreement_With_WO_XML__mdt state = SFS_Service_Agreement_With_WO_XML__mdt.getInstance('SFS_Customer_State');  
            dropNode = double.ValueOf(state.SFS_Node_Order__c);
            //CountryPointer = String.ValueOf(state.SFS_XML_Full_Name__c);
        } else {
            SFS_Service_Agreement_With_WO_XML__mdt prov = SFS_Service_Agreement_With_WO_XML__mdt.getInstance('SFS_Customer_Province');  
            dropNode = double.ValueOf(prov.SFS_Node_Order__c);
        }
         Map<String, Object> BillToAccountFields = new Map <String, Object>(); 
        Account workOrderBillToAcc = new Account();
        try{
        workOrderBillToAcc = [SELECT Id,Oracle_Id__c,SFS_Oracle_Site_Use_Id__c,IRIT_Customer_Number__c
                                   FROM Account WHERE Id =: wo.SFS_Bill_to_Account__c];
        
       
        BillToAccountFields = workOrderBillToAcc.getPopulatedFieldsAsMap();
        }catch(System.Exception e){
            wo.SFS_Integration_Status__c = 'ERROR';
            wo.SFS_Integration_Response__c = String.ValueOf(e);
            update wo;
        }
        Map<String, Object> AssetFields = new Map<String, Object>();
        Asset a = new Asset();
        try{
        a = [SELECT Product_Line__c
                  FROM Asset 
                  WHERE Id =: wo.AssetId];
        
        AssetFields = a.getPopulatedFieldsAsMap();
        }Catch(System.Exception e){
            wo.SFS_Integration_Status__c = 'ERROR';
            wo.SFS_Integration_Response__c = String.ValueOf(e);
            update wo;
        }
        
         Division__c workOrderDiv = new Division__c();
        Map<String, Object> DivisionFields = new Map <String, Object>(); 
        try{
             workOrderDiv = [SELECT Id,SFS_Org_Code__c, SFS_Category__c, SFS_Employee_Number__c,SFS_Class_Code__c,SFS_External_System_Value__c, SFS_Sales_Branch__c, EBS_System__c
                                   FROM Division__c WHERE Id =:wo.SFS_Division__c];
               DivisionFields = workOrderDiv.getPopulatedFieldsAsMap(); 
        }catch(System.Exception e){
            wo.SFS_Integration_Status__c = 'ERROR';
            wo.SFS_Integration_Response__c = String.ValueOf(e);
            update wo;
        }
       
        
     
        
        Map<String, Object> ScFieldValues = new Map<String, Object>();
        for(String field : workOrderFields.keyset()){
            //try{
                    if(field == 'SFS_External_Id__c'){
                    	scFieldValues.put('workordernumber', wo.get(field));
                        System.debug('TRUE WO');
                    }
            		
            		System.debug('TESTTT::: ' + field.toLowerCase()+ ':' + wo.get(field));
                    ScFieldValues.put(field.toLowerCase(), wo.get(field));
                 	
           // }catch(SObjectException e){
             //  System.debug('EXCEPTION:: ' + e);
            //}
        }
     //try{
        for(String field : ServiceContractFields.keyset()){
                if(field == 'SFS_External_Id__c'){
                    	scFieldValues.put('contractnumber', svg.get(field));
                    System.debug('contractNumber' + svg.get(field));
                    System.debug('TRUE SC');

                    }
             	
            		System.debug('TEST:::::::: ' + field.toLowerCase() + ' , ' + svg.get(field));
                    ScFieldValues.put(field.toLowerCase(), svg.get(field));            
            }
        for(String field : AccountFields.keyset()){
                ScFieldValues.put(field.toLowerCase(), workOrderAcc.get(field));                
            }
        for(String field : BillToAccountFields.keyset()){
                //ScFieldValues.put(field.toLowerCase(), workOrderBillToAcc.get(field)); 
                if(field == 'irit_Customer_Number__c'){
                  ScFieldValues.put('SFS_BillTo_Customer_Number', workOrderBillToAcc.get(field));
                  }
               else{
                 ScFieldValues.put(field.toLowerCase(), workOrderBillToAcc.get(field));
                }               
            }
        for(String field : DivisionFields.Keyset()){
                ScFieldValues.put(field.toLowerCase(), workOrderDiv.get(field));                
            }
        
        for(String field : AssetFields.Keyset()){
                ScFieldValues.put(field.toLowerCase(), a.get(field));               
            }
       //  }catch(Exception e){
               //  System.debug('Exception :' +e.getMessage());
        //        }
        List<Double> nodeList = new List<Double>();
        for(SFS_Service_Agreement_With_WO_XML__mdt m: woSvgMaps){
            //Some data will need to be hardcoded. We will check for fields that do not need to be hardcoded first.
            if(m.SFS_Node_Order__c != dropNode){
                nodeList.add(m.SFS_Node_Order__c);
                if(m.SFS_HardCoded_Flag__c == false ){
                    String sfField = m.SFS_Salesforce_Field__c;
                    if(SCFIeldValues.containsKey(sfField)){
                        double nodeOrder = m.SFS_Node_Order__c;
                        if(SCFieldValues.get(sffield) != null){
                            //Making sure sffield != null. If not replace with values in SA map, else add the m.SFS_XML_Full_Name value to the map.
                            String xmlfullName = m.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(ScFieldValues.get(sffield));
                            replacement = replacement.escapeXml();
                            String newpa = XMLFullName.replace(sffield,replacement); 
                            WoxmlMap.put(nodeOrder,newpa);
                        }  
                        else if(SCFieldValues.get(sffield) == null){
                            WoxmlMap.put(m.SFS_Node_Order__c, m.SFS_XML_Full_Name__c);
                        }                   
                    }
                    else if(!SCFieldValues.ContainsKey(sffield)){
                        String empty = '';
                        String replacement = m.SFS_XML_Full_Name__c.replace(sffield, empty);
                        WoxmlMap.put(m.SFS_Node_Order__c, replacement);
                    }
                }
                else if (m.SFS_HardCoded_Flag__c == true){                    
                    WoxmlMap.put(m.SFS_Node_Order__c, m.SFS_XML_Full_Name__c);
                    //XMLList.add(m.SFS_XML_Full_Name__c);
                }
            }
            
        }
        
        nodeList.Sort();
        List<String> finalXML = new List<String>();
        for(Double n : nodeList){
            if(n != dropNode){
                finalXML.add(WoxmlMap.get(n));   
            } 
            
        }            
        for(String s : finalXML){
            System.debug('XMLString ' + s);
            xmlString = xmlString + s;
            
        }
        String interfaceDetail = 'XXPA2381';
        String interfaceLabel = 'Work Order with Service Agreement';
        String WorkOrderId = wo.id;
        
        if(!test.isRunningTest()){
           InterfaceLabel = InterfaceLabel + '!' + workOrderId;
           SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail,interfaceLabel);
        }else{
            SFS_Outbound_Integration_Callout.HttpCalloutTest('WorkOrder', WorkOrderId, xmlString, interfaceDetail, interfaceLabel);
        }
      
    }

    //Process the response from the callout. Store the results on the records sent to Oracle.
    public static void IntegrationResponse(HttpResponse res, String interfaceLabel){
        System.debug('@@@INTERFACE LABEL:  ' + interfaceLabel);
        System.debug('@@@@SPLIT LOGIC: ' + interfaceLabel.Split('!'));
        List<String> woId = interfaceLabel.split('!');
        
        WorkOrder wo = [SELECT SFS_Integration_Status__c, SFS_Integration_Response__c, ServiceContractId FROM WorkOrder WHERE Id =: woId];
        //ServiceContract sc = [SELECT Id FROM ServiceContract WHERE Id =: wo.EntitlementId];
        
        if(res.getStatusCode() != 200){
            wo.SFS_Integration_Status__c = 'ERROR';
            wo.SFS_Integration_Response__c = 'Project Creation - Work Order with Service Agreement'+ '\nIntegration Response:\t' + res.getBody();
           // sc.SFS_Integration_Status__c = 'Error';
            //sc.SFS_Integration_Response__c = 'Project Creation - Work Order with Service Agreement' + '\nIntegration Response:\t' + res.getBody();
        }else{
            wo.SFS_Integration_Status__c = 'SYNCING';
            wo.SfS_Integration_Response__c = 'Project Creation - Work Order with Service Agreement' + '\nIntegration Response:\t' + res.getBody();
           //	sc.SFS_Integration_Status__c = 'SYNCING';
            //sc.SFS_Integration_Response__c = 'Project Creation - Work Order with Service Agreement' + '\nIntegration Response:\t' + res.getBody();
        }
        
        update wo;
    }
  
}