/**
* @author           Amit Datta
* @description      Test Class for B2BCommerceUtils.
*
* Modification Log
* ------------------------------------------------------------------------------------------
*         Developer                   Date                Description
* ------------------------------------------------------------------------------------------
*         Amit Datta                  14/01/2024          Original Version
**/

public with sharing class B2BCommerceUtils {
    // A cache to map a string to ConnectApi.CartItemSortOrder
    private static Map<String, ConnectApi.CartItemSortOrder> sortOrderCache = new Map<String, ConnectApi.CartItemSortOrder>();
    
    // Function to lookup the webstore ID associated with the community
    public static String resolveCommunityIdToWebstoreId(String communityId) {
        if (communityId == null || communityId == '') {
            return null;
        }
        String webstoreId = null;
        if (Schema.sObjectType.WebStoreNetwork.fields.WebStoreId.isAccessible() && Schema.sObjectType.WebStoreNetwork.fields.NetworkId.isAccessible()) {
            List<WebStoreNetwork> wsnList = [SELECT WebStoreId FROM WebStoreNetwork WHERE NetworkId = :communityId];
            if (!wsnList.isEmpty()) {
                WebStoreNetwork wsn = wsnList.get(0);
                webstoreId = wsn.WebStoreId;
            }
        }
        return webstoreId;
    }
    
    // Function to lookup the webstore ID associated with the community
    public static String getWebStoreIdByUrlPathPrefix(String urlPathPrefix) {
        if (urlPathPrefix == null || urlPathPrefix == '') {
            return null;
        }
        String webstoreId = null;
        if (Schema.sObjectType.WebStoreNetwork.fields.WebStoreId.isAccessible() && Schema.sObjectType.WebStoreNetwork.fields.NetworkId.isAccessible()) {
            List<WebStoreNetwork> wsnList = [SELECT WebStoreId FROM WebStoreNetwork WHERE Network.UrlPathPrefix = :urlPathPrefix];
            if (!wsnList.isEmpty()) {
                WebStoreNetwork wsn = wsnList.get(0);
                webstoreId = wsn.WebStoreId;
            }
        }
        return webstoreId;
    }

    public static String getAccountIdFromUser(){
        List<User> userContext= [Select Contact.AccountId from User where Id = :System.UserInfo.getUserId()];
        return userContext[0]?.Contact?.AccountId;
    }

    public static String getCartId(String accountId, String userId) {
        List<WebCart> carts = [Select Id, TotalAmount, GrandTotalAmount, Content_Document_Id__c, Status, OwnerId, AccountId FROM WebCart
                               WHERE AccountId = :accountId AND (Status = 'Checkout' OR Status = 'Active') AND OwnerId = :userId];

        return carts.size() > 0 ? carts[0]?.Id : '';
    }
}