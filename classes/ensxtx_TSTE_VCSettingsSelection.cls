@isTest
public class ensxtx_TSTE_VCSettingsSelection
{
    @isTest static void testClassVariables ()
    {
        Test.startTest();
        ensxtx_ENSX_VCSettingsSelection ensxVcSettingsSelection = new ensxtx_ENSX_VCSettingsSelection();
        ensxVcSettingsSelection.Value = 'Value';
        ensxVcSettingsSelection.ValueDescription = 'ValueDescription';
        System.assert(ensxVcSettingsSelection.Value == 'Value');
        Test.stopTest();
    }
}