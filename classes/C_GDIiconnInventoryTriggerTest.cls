@isTest
global class C_GDIiconnInventoryTriggerTest {
  
  @isTest
    static void posTest(){
        
        list<id> AstID = new list<id>();
        Test.setMock(HttpCalloutMock.class, new C_GDIHttpMockTest());
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://example.com/example/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        Asset newAst1 = new Asset(name='testing11',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c=Null);
        // Asset newAst2 = new Asset(name='testing22',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c='');
        insert newAst1;
        newAst1.IMEI__c = '359804083674640';
        //C_GDIiconnInventoryTrigger.iConnInventorySync(AstID); 
        Test.startTest();
        try{
        update newAst1;
        }
        catch(Exception e){
            system.debug('error: ' +e);
        }
       // C_GDIiConnInventoryTriggerSyncBatch btch = new C_GDIiConnInventoryTriggerSyncBatch(AstID);
       //id batchprocessid = Database.executebatch(btch,1);
        Test.stopTest();

    }
   
     @isTest
    static void negTest(){
        
        list<id> AstID = new list<id>();
        Asset newAst1 = new Asset(name='testing11',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId());
        // Asset newAst2 = new Asset(name='testing22',RecordtypeId=Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Location Equipment').getRecordTypeId(),IMEI__c='');
        insert newAst1;
        newAst1.IMEI__c = '359804083674640';
        
        AstID.add(newAst1.id);
        Test.startTest();    
       	update newAst1;
        Test.stopTest();
    }
    
}