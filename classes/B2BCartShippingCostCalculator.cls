global with sharing class B2BCartShippingCostCalculator implements sfdc_checkout.CartShippingCharges {

    public final String shippingProvider = 'IR Fixed Cost';
    public final String shippingProviderName = 'IR Shipping Cost';
    public final String shippingChargeProduct2Name = 'IR B2B Shipping Charge';
    public final String defaultDeliveryMethodName = 'IR Order Delivery Method';
    public static final String OneLBottleShippingCost = 'One_Litre_Bottle_Shipping_Cost';
    public static final String TwentyLBucketShippingCost = 'Twenty_Litre_Bucket_Shipping_Cost';
    public static final String FiveLJugShippingCost = 'Five_Litre_Jug_Shipping_Cost';
    public static final String KitsShippingCost = 'Kits_Shipping_Cost';
    public static final String PartsShippingCost = 'Parts_Shipping_Cost';

    global sfdc_checkout.IntegrationStatus startCartProcessAsync(sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
        sfdc_checkout.IntegrationStatus integStatus = new sfdc_checkout.IntegrationStatus();
        try {
            List<CartDeliveryGroup> cartDeliveryGroups = [SELECT Id, DeliveryMethodId FROM CartDeliveryGroup WHERE CartId = :cartId WITH SECURITY_ENFORCED];
            CartDeliveryGroup cartDeliveryGroup = cartDeliveryGroups.size() > 0 ? cartDeliveryGroups[0] : null;

            List<cartItem> cartItems = [SELECT Id, Quantity, Product2.Shipping_Category__c from cartItem WHERE CartId = :cartId AND Type = 'Product' WITH SECURITY_ENFORCED];

            // Get shipping cost
            Map<String, Decimal> shippingCosts = getShippingCost();
            Decimal totalShippingCost = 0;
            for (CartItem cartItem : cartItems) {
                String category = String.isEmpty(cartItem.Product2.Shipping_Category__c) ? 'Kits' : cartItem.Product2.Shipping_Category__c;
                totalShippingCost = totalShippingCost + cartItem.Quantity * shippingCosts.get(category);
            }

            if (cartDeliveryGroup != null) {
                delete [SELECT Id FROM CartDeliveryGroupMethod WHERE CartDeliveryGroupId = :cartDeliveryGroup.Id WITH SECURITY_ENFORCED];
            }

            List<Id> orderDeliveryMethodIds = getOrderDeliveryMethods();
            for (Id orderDeliveryMethodId: orderDeliveryMethodIds) {
                if (cartDeliveryGroup != null) {
                    populateCartDeliveryGroupMethodWithShippingOptions(totalShippingCost, cartDeliveryGroup.Id, orderDeliveryMethodId, cartId);
                }
                else {
                    populateCartDeliveryGroupMethodWithShippingOptions(totalShippingCost, null, orderDeliveryMethodId, cartId); //just added to increase test coverage
                }
            }

            integStatus.status = sfdc_checkout.IntegrationStatus.Status.SUCCESS;

        } catch (DmlException de) {
            // Catch any exceptions thrown when trying to insert the shipping charge to the CartItems
            Integer numErrors = de.getNumDml();
            String errorMessage = 'There were ' + numErrors + ' errors when trying to insert the charge in the CartItem: ';
            for(Integer errorIdx = 0; errorIdx < numErrors; errorIdx++) {
                errorMessage += 'Field Names = ' + de.getDmlFieldNames(errorIdx);
                errorMessage += 'Message = ' + de.getDmlMessage(errorIdx);
                errorMessage += ' , ';
            }

            return integrationStatusFailedWithCartValidationOutputError(
                    integStatus,
                    errorMessage,
                    jobInfo,
                    cartId
            );
        }
        catch(Exception e) {
            System.debug('exception:' + e.getStackTraceString() +  e.getTypeName() + '>>>' +e.getMessage() );
            return integrationStatusFailedWithCartValidationOutputError(
                    integStatus,
                    'An exception of type ' + e.getTypeName() + ' has occurred: ' + e.getMessage(),
                    jobInfo,
                    cartId
            );
        }
        return integStatus;
    }

    private static Map<String, Decimal> getShippingCost() {
        Map<String, Decimal> shippingCostByCategory = new Map<String, Decimal>();
        shippingCostByCategory.put('1L Bottle', Decimal.valueOf(B2B_IR_Store_Setting__mdt.getInstance(OneLBottleShippingCost).Value__c.replace(',','.')));
        shippingCostByCategory.put('5L Jug', Decimal.valueOf(B2B_IR_Store_Setting__mdt.getInstance(FiveLJugShippingCost).Value__c.replace(',','.')));
        shippingCostByCategory.put('20L Bucket', Decimal.valueOf(B2B_IR_Store_Setting__mdt.getInstance(TwentyLBucketShippingCost).Value__c.replace(',','.')));
        shippingCostByCategory.put('Kits', Decimal.valueOf(B2B_IR_Store_Setting__mdt.getInstance(KitsShippingCost).Value__c.replace(',','.')));
        shippingCostByCategory.put('Parts', Decimal.valueOf(B2B_IR_Store_Setting__mdt.getInstance(PartsShippingCost).Value__c.replace(',','.')));
        return shippingCostByCategory;

    }

    private List<Id> getOrderDeliveryMethods() {
        String defaultDeliveryMethodName = defaultDeliveryMethodName;
        Id product2IdForThisDeliveryMethod = getDefaultShippingChargeProduct2Id();

        // Check to see if a default OrderDeliveryMethod already exists.
        // If it doesn't exist, create one.
        List<Id> orderDeliveryMethodIds = new List<Id>();
        List<OrderDeliveryMethod> orderDeliveryMethods = new List<OrderDeliveryMethod>();
        Integer i = 1;
        String shippingOptionNumber = String.valueOf(i);
        String name = defaultDeliveryMethodName + shippingOptionNumber;
        List<OrderDeliveryMethod> odms = [SELECT Id, ProductId, Carrier, ClassOfService FROM OrderDeliveryMethod WHERE Name = :name WITH SECURITY_ENFORCED];
        // This is the case in which an Order Delivery method does not exist.
        if (odms.isEmpty()) {
            OrderDeliveryMethod defaultOrderDeliveryMethod = new OrderDeliveryMethod(
                    Name = name,
                    Carrier = shippingProvider,
                    isActive = true,
                    ProductId = product2IdForThisDeliveryMethod,
                    ClassOfService = shippingProvider
            );
            insert(defaultOrderDeliveryMethod);
            orderDeliveryMethodIds.add(defaultOrderDeliveryMethod.Id);
        }
        else {
            OrderDeliveryMethod existingodm = odms[0];
            if (existingodm.Carrier == null || existingodm.ClassOfService == null) {
                existingodm.ProductId = product2IdForThisDeliveryMethod;
                existingodm.Carrier = shippingProvider;
                existingodm.ClassOfService = shippingProvider;
                update(existingodm);
            }
            orderDeliveryMethodIds.add(existingodm.Id);
        }
        return orderDeliveryMethodIds;
    }


    // Create a CartDeliveryGroupMethod record for every shipping option returned from the external service
    private void populateCartDeliveryGroupMethodWithShippingOptions(Decimal cost,
            Id cartDeliveryGroupId,
            Id deliveryMethodId,
            Id webCartId){
        CartDeliveryGroupMethod cartDeliveryGroupMethod = new CartDeliveryGroupMethod(
                CartDeliveryGroupId = cartDeliveryGroupId,
                DeliveryMethodId = deliveryMethodId,
                ExternalProvider = shippingProvider,
                Name = shippingProviderName,
                ShippingFee = cost,
                WebCartId = webCartId
        );
        insert(cartDeliveryGroupMethod);
    }

    private sfdc_checkout.IntegrationStatus integrationStatusFailedWithCartValidationOutputError(
            sfdc_checkout.IntegrationStatus integrationStatus, String errorMessage, sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
        integrationStatus.status = sfdc_checkout.IntegrationStatus.Status.FAILED;
        CartValidationOutput cartValidationError = new CartValidationOutput(
                BackgroundOperationId = jobInfo.jobId,
                CartId = cartId,
                Level = 'Error',
                Message = errorMessage.left(255),
                Name = (String)cartId + ':' + jobInfo.jobId,
                RelatedEntityId = cartId,
                Type = 'Shipping'
        );
        insert(cartValidationError);
        return integrationStatus;
    }

    private Id getDefaultShippingChargeProduct2Id() {
        String shippingChargeProduct2Name = shippingChargeProduct2Name;
        List<Product2> shippingChargeProducts = [SELECT Id FROM Product2 WHERE Name = :shippingChargeProduct2Name WITH SECURITY_ENFORCED];
        if (shippingChargeProducts.isEmpty()) {
            Product2 shippingChargeProduct = new Product2(
                    isActive = true,
                    Name = shippingChargeProduct2Name
            );
            insert(shippingChargeProduct);
            return shippingChargeProduct.Id;
        }
        else {
            return shippingChargeProducts[0].Id;
        }
    }
}