@RestResource(urlMapping='/service-parts/')
global class ServiceHistory_Parts {
@HttpGet
    global static void getParts() {
        RestRequest request = RestContext.request;
        String query = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
        RestContext.response.addHeader('Content-Type', 'application/json');
        List<Part__c> record = [SELECT Part_Number__c, Description__c, Part_Family__c FROM Part__c];
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(record));
    }
}