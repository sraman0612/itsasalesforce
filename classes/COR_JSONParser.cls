public with sharing class COR_JSONParser {
	
	private String JSONToParse;
	public List <JSONObject> objects {get; private set;}
	
	
	public COR_JSONParser (String JSONToParse) {
		this.JSONToParse = JSONToParse;
		beginParsingJSON(this.JSONToParse);
	}
	
	
	public void beginParsingJSON (String JSONToParse) {
		// Create a new list of objects.
		objects = new List <JSONObject> ();
		
		// Initialize the native parser.
		JSONParser parser = JSON.createParser(JSONToParse);
   		
   		// Being parsing from start to end.
   		while (parser.nextToken() != null) {
   			JSONToken token = parser.getCurrentToken();
   			
   			if (token == JSONToken.START_OBJECT) {
   				JSONObject newObject = new JSONObject();
   				parseObject(parser, newObject);
   				objects.add(newObject);
   			}
   		}
	}
	
	
	
	private void parseObject (JSONParser parser, JSONObject newObject) {
		while (parser.nextToken() != null) {
			JSONToken token = parser.getCurrentToken();
			
			if (token == JSONToken.FIELD_NAME) {
				String fieldName = parser.getText();
				token = parser.nextToken();
				if (token != JSONToken.START_OBJECT && token != JSONToken.START_ARRAY) {
					String fieldValue = parser.getText();
					newObject.addFieldValue(fieldName, fieldValue);
				}
				else if (token == JSONToken.START_OBJECT) {
					JSONObject childObject = new JSONObject();
					parseObject(parser, childObject);
					newObject.addChildObject(fieldName, childObject);
				}
				else if (token == JSONToken.START_ARRAY) {
					parseArrays(fieldName, parser, newObject);
				}
			}
			else if (token == JSONToken.END_OBJECT) {
				return;
			}
		}
	}
	
	
	private void parseArrays (String fieldName, JSONParser parser, JSONObject newObject) {
		List <JSONObject> 	childArrayOfObjects = new List <JSONObject> ();
		List <String> 		childArrayOfValues 	= new List <String> ();
		
		while (parser.nextToken() != null) {
			JSONToken token = parser.getCurrentToken();
			
			if (token == JSONToken.START_OBJECT) {
				JSONObject childObject = new JSONObject();
				parseObject(parser, childObject);
				childArrayOfObjects.add(childObject);
			}
			else if (token != JSONToken.START_OBJECT  && token != JSONToken.START_ARRAY && token != JSONToken.END_OBJECT && token != JSONToken.END_ARRAY) {
				String value = parser.getText();
				childArrayOfvalues.add(value);
			}
			else if (token == JSONToken.END_ARRAY) {
				if (childArrayOfObjects.size() > 0) newObject.addChildArrayOfObjects(fieldName, childArrayOfObjects);
				if (childArrayOfValues.size() > 0)	newObject.addChildArrayOfValues(fieldName, childArrayOfValues);
				return;
			}
		}
	}
	
	
	
	public void printStructure () {
		for (JSONObject job : objects) {
			job.printStructure(0);
		}
	}
	
	
	
	
	public class JSONObject {
		public Map <String, String> 			values			{get; private set;}
		public Map <String, JSONObject> 		objects			{get; private set;}
		public Map <String, List <JSONObject>> 	arraysOfObjects	{get; private set;}
		public Map <String, List <String>>		arraysOfValues	{get; private set;}
		
		public JSONObject () {
			values 			= new Map <String, String> ();
			objects 		= new Map <String, JSONObject> ();
			arraysOfObjects = new Map <String, List <JSONObject>> ();
			arraysOfValues 	= new Map <String, List <String>> ();
		}
		
		public void addFieldValue (String fieldName, String fieldValue) {
			values.put(fieldName, fieldValue);
		}
		
		public void addChildObject (String fieldName, JSONObject childObject) {
			objects.put(fieldName, childObject);
		}
		
		public void addChildArrayOfObjects (String fieldName, List <JSONObject> childArray) {
			arraysOfObjects.put(fieldName, childArray);
		}
		
		public void addChildArrayOfValues (String fieldName, List <String> childArray) {
			arraysOfValues.put(fieldName, childArray);
		}
		
		
		public void printStructure (Integer level) {
			String tabs = '';
			for (Integer i = 0; i < level; i++) {
				tabs += '___';
			}
			//System.debug(tabs+'OBJECT');
			for (String field : values.keySet()) {
				System.debug(tabs+'. '+ field +' - '+ values.get(field));
			}
			//System.debug(tabs+'CHILD OBJECTS');
			for (String field : objects.keySet()) {
				System.debug(tabs+'CO '+ field);
				objects.get(field).printStructure(level+1);
			}
			//System.debug(tabs+'CHILD ARRAYS OF OBJECTS');
			for (String field : arraysOfObjects.keySet()) {
				System.debug(tabs+'COA '+ field);
				List <JSONObject> childArray = arraysOfObjects.get(field);
				for (JSONObject aob : childArray) {
					aob.printStructure(level+1);
				}
			}
			//System.debug(tabs+'CHILD ARRAYS OF VALUES');
			for (String field : arraysOfValues.keySet()) {
				System.debug(tabs+'CVA '+ field);
				List <String> childArray = arraysOfValues.get(field);
				for (String value : childArray) {
					System.debug(tabs+'___'+value);
				}
			}
		}
	}
}