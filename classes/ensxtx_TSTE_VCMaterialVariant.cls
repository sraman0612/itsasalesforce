@isTest
public class ensxtx_TSTE_VCMaterialVariant
{
    @isTest static void testClassVariables ()
    {
        Test.startTest();
        ensxtx_ENSX_VCMaterialVariant ensxVcMaterialVariant = new ensxtx_ENSX_VCMaterialVariant();
        ensxVcMaterialVariant.VariantDescription = 'VariantDescription';
        ensxVcMaterialVariant.VariantId = 'VariantId';
        ensxVcMaterialVariant.CharacteristicValues = null;
        System.assert(ensxVcMaterialVariant.VariantDescription == 'VariantDescription');
        Test.stopTest();
    }
}