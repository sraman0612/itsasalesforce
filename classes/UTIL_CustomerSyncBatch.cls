/*
Paste the below code into the Anonymous Apex Window
To know how many total records are there to sync
========== START CODE ==========
new ensxsdk.Logger(null);
List<String> accountGroups = (List<String>)UTIL_AppSettings.getList(
    'UTIL_CustomerSyncBatch.AccountGroups', String.class, new List<String>{});

SBO_EnosixCustSync_Search.EnosixCustSync_SC searchContext = new SBO_EnosixCustSync_Search.EnosixCustSync_SC();
searchContext.pagingOptions.pageSize = 1;
searchContext.pagingOptions.pageNumber = 1;
if (accountGroups.size() > 0)
{
    for (String acctGroup : accountGroups)
    {
        SBO_EnosixCustSync_Search.ACCOUNT_GROUPS newAcctGroup = new SBO_EnosixCustSync_Search.ACCOUNT_GROUPS();
        newAcctGroup.CustomerAccountGroup = acctGroup;
        searchContext.ACCOUNT_GROUPS.add(newAcctGroup);
    }
}
SBO_EnosixCustSync_Search sbo = new SBO_EnosixCustSync_Search();
sbo.search(searchContext);
System.debug(searchContext.result.isSuccess());
System.debug(searchContext.pagingOptions.totalRecords);
========== END CODE ==========
*/
public with sharing class UTIL_CustomerSyncBatch
    implements Database.Batchable<Object>,
    Database.AllowsCallouts,
    Database.Stateful,
    I_ParameterizedSync
{
    @TestVisible
    private static String SFSyncKeyField = UTIL_SFAccount.CustomerFieldName;
    @TestVisible
    private static String BatchClassName = 'UTIL_CustomerSyncBatch';
    private static String ScheduleClassName = 'UTIL_CustomerSyncSchedule';

    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_CustomerSyncBatch.class);
    public void logCallouts(String location)
    {
        if ((Boolean)UTIL_AppSettings.getValue(BatchClassName + '.Logging', false))
        {
            logger.enterVfpConstructor(location, null);
        }
    }

    // jobInfo contains debug information persisted across contexts since start(),
    // execute(), and finish() all run in separate contexts with separate debug logs
    private List<String> jobInfo = new List<String>();

    // In this case, we will store the largest change date/time as the param
    private UTIL_SyncHelper.LastSync fromLastSync = new UTIL_SyncHelper.LastSync();
    private static String ObjectType = 'Account';
    private static List<String> SAP_SalesOrgs = (List<String>)UTIL_AppSettings.getList(BatchClassName + '.SalesOrgs', String.class, new List<String>{});

    /* I_ParameterizedSync methods - setBatchParam() */
    public void setBatchParam(Object value)
    {
        this.fromLastSync = (UTIL_SyncHelper.LastSync) value;
    }
    /* end I_ParameterizedSync methods */

    private static List<String> accountGroups
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                BatchClassName + '.AccountGroups', String.class, new List<String>{});
        }
    }

    private static List<String> distributionChannels
    {
        get
        {
            return (List<String>)UTIL_AppSettings.getList(
                BatchClassName + '.DistributionChannels', String.class, new List<String>{});
        }
    }

    /* Database.Batchable methods start(), execute(), and finish() */
    // start()
    //
    // Calls SBO and returns search results of update customers
    public List<Object> start(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.start');
        System.debug(context.getJobId() + ' started');

        SBO_EnosixCustSync_Search sbo = new SBO_EnosixCustSync_Search();
        SBO_EnosixCustSync_Search.EnosixCustSync_SC searchContext =
            new SBO_EnosixCustSync_Search.EnosixCustSync_SC();

        this.fromLastSync = UTIL_SyncHelper.getLastSyncFromTable(
            ScheduleClassName,
            this.fromLastSync);

        this.fromLastSync.pageNumber = this.fromLastSync.pageNumber + 1;

        if (this.fromLastSync.retryCnt == -1)
        {
            UTIL_SyncHelper.resetPage(this.fromLastSync, (Integer) UTIL_AppSettings.getValue(
                BatchClassName + '.SAPPageSize',
                512));
        }
        if (this.fromLastSync.lastSyncDate != null)
        {
            searchContext.SEARCHPARAMS.DateFrom = this.fromLastSync.lastSyncDate;
            // searchContext.SEARCHPARAMS.FromChangeTime = this.fromLastSync.lastSyncTime;
        }
        else
        {
            searchContext.SEARCHPARAMS.InitialLoad = true;
        }

        searchContext.pagingOptions.pageSize = this.fromLastSync.pageSize;
        searchContext.pagingOptions.pageNumber = this.fromLastSync.pageNumber;
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());
        System.debug('fromLastSync:' + this.fromLastSync.toString());

        if (accountGroups.size() > 0)
        {
            for (String acctGroup : accountGroups)
            {
                SBO_EnosixCustSync_Search.ACCOUNT_GROUPS newAcctGroup = new SBO_EnosixCustSync_Search.ACCOUNT_GROUPS();
                newAcctGroup.CustomerAccountGroup = acctGroup;
                searchContext.ACCOUNT_GROUPS.add(newAcctGroup);
            }
        }

        // Execute the search
        SBO_EnosixCustSync_Search.EnosixCustSync_SR result;
        try
        {
            sbo.search(searchContext);
            result = searchContext.result;
        }
        catch (Exception ex)
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, ex, this.jobInfo);
        }

        // Write any response messages to the debug log
        String errorMessage = UTIL_SyncHelper.buildErrorMessage(BatchClassName, result.getMessages());

        if (!result.isSuccess())
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, new UTIL_SyncHelper.SyncException(errorMessage), this.jobInfo);
        }

        List<Object> searchResults = result.getResults();
        System.debug('Result size: ' + searchResults.size());

        // let finish() know to queue up another instance
        this.fromLastSync.isAnotherBatchNeeded = searchResults.size() > 0;
        this.fromLastSync.retryCnt = -1;

        this.jobInfo.add('searchResultsSize:' + searchResults.size());
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());

        return searchResults;
    }

    // execute()
    //
    // Given the updated search results, does the work of updating the object table.
    public void execute(
        Database.BatchableContext context,
        List<Object> searchResults)
    {
        logCallouts(BatchClassName + '.execute');
        System.debug(context.getJobId() + ' executing');

        if (null == searchResults || 0 == searchResults.size()) return;

        List<SObject> errors = new List<SObject>();
        Map<String, Object> searchResultMap = createObjectKeyMap(searchResults);

        // First, update matching existing objects
        List<SObject> currentObjectList = UTIL_SyncHelper.getCurrentObjects(
            ObjectType,
            SFSyncKeyField,
            searchResultMap.keySet());
        Map<String, SObject> currentObjectMap = new Map<String, SObject>();
        for (SObject currentObject : currentObjectList)
        {
            currentObjectMap.put((String) currentObject.get(SFSyncKeyField),currentObject);
        }

        Map<String, SObject> updateObjectMap = updateExistingObjects(
            searchResultMap,
            currentObjectMap,
            errors);

        Map<String, SObject> insertObjectMap = createNewObjects(
            searchResultMap,
            currentObjectMap,
            errors);

        Set<Id> savedIdSet = new Set<Id>();

        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Update', errors, savedIdSet, updateObjectMap.values(), BatchClassName, SFSyncKeyField);
        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Insert', errors, savedIdSet, insertObjectMap.values(), BatchClassName, SFSyncKeyField);
        UTIL_SyncHelper.insertUpdateResults('Error', 'Insert', errors, savedIdSet, errors, BatchClassName, null);
    }

    // finish()
    //
    // queues up another batch when isAnotherBatchNeeded is true
    public void finish(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.finish');
        System.debug(context.getJobId() + ' finished');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);
        if (this.fromLastSync.retryCnt >= 0)
        {
            System.debug('Retry=' + this.fromLastSync.retryCnt + ' ' + System.Now());
        }

        UTIL_SyncHelper.launchAnotherBatchIfNeeded(
            this.fromLastSync.isAnotherBatchNeeded, ScheduleClassName, this.fromLastSync);

        if (!this.fromLastSync.isAnotherBatchNeeded) Database.executeBatch(new UTIL_CustomerSyncBatchCleanup());
    }

    private SBO_EnosixCustSync_Search.SEARCHRESULT getSboResult(Object searchResult)
    {
        return (SBO_EnosixCustSync_Search.SEARCHRESULT) searchResult;
    }

    // createObjectKeyMap()
    //
    // create map of objecy key / search result.
    private Map<String, Object> createObjectKeyMap(
        List<Object> searchResults)
    {
        Map<String, Object> result =
            new Map<String, Object>();

        for (Object searchResult : searchResults)
        {
            String key = getSboKey(searchResult);
            if (String.isNotEmpty(key) && distributionChannels.contains((getSboResult(searchResult).DistributionChannel)))
            {
                result.put(key, searchResult);
            }
        }

        return result;
    }

    private Map<String, SObject> updateExistingObjects(
        Map<String, Object> searchResultMap,
        Map<String, SObject> currentObjectMap,
        List<SObject> errors)
    {
        Map<String, SObject> updateObjectMap = new Map<String, SObject>();

        for (Object searchResult : searchResultMap.values())
        {
            String currentSboKey = getSboKey(searchResult);
            if (currentObjectMap.containsKey(currentSboKey))
            {
                SObject currentObject = currentObjectMap.get(currentSboKey);

                // Updates fields and adds to objectList list for later commit
                syncObject(
                    currentObject,
                    searchResult,
                    updateObjectMap,
                    errors);
            }
        }

        findRepAccount(updateObjectMap, errors);
        System.debug('Existing Object Size: ' + updateObjectMap.size());

        return updateObjectMap;
    }

    private Map<String, SObject> createNewObjects(
        Map<String, Object> searchResultMap,
        Map<String, SObject> currentObjectMap,
        List<SObject> errors)
    {
        Map<String, SObject> insertObjectMap = new Map<String, SObject>();

        for (Object searchResult : searchResultMap.values())
        {
            if (!currentObjectMap.containsKey(getSboKey(searchResult)))
            {
                syncObject(
                    null,
                    searchResult,
                    insertObjectMap,
                    errors);
            }
        }

        createObjectParent(insertObjectMap, errors);
        findRepAccount(insertObjectMap, errors);
        System.debug('New Object Size: ' + insertObjectMap.size());

        return insertObjectMap;
    }

    private void createObjectParent(Map<String, SObject> objectMap, List<SObject> errors) {
        Set<String> sapAccountNums = new Set<String>();
        Set<String> newSAPacctNums = new Set<String>();

        for (SObject curObj : objectMap.values()) {
            Account curAcct = (Account)curObj;
            try {
                if (String.isNotBlank(curAcct.Customer_group__c) && String.isNotBlank(curAcct.ENSX_EDM__SAP_Customer_Number__c) && String.isNotBlank(curAcct.SOrg__c)) {
                    sapAccountNums.add(curAcct.ENSX_EDM__SAP_Customer_Number__c);
                }
            } catch (Exception e) {
                String key = (String) curAcct.get(SFSyncKeyField);
                String message = 'Account - SAP Customer Number ' + curAcct.ENSX_EDM__SAP_Customer_Number__c + ' is not a valid number from Key ' + key;
                System.debug(message);
                UTIL_SyncHelper.addLog(errors, 'ERROR', key, Json.serialize(curAcct), BatchClassName + ' ' + ObjectType + ' Name missing from Key ' + key);
            }
        }

        Map<String, Account> sapAccountNumMap = new Map<String, Account>();
        List<Account> newParentAccounts = new List<Account>();
        List<Account> sapParentAccounts = [SELECT Id, Account_Number__c from Account where Account_Number__c in : sapAccountNums AND Account_Type__c = 'Parent'];

        for (Account sapParentAccount : sapParentAccounts) {
            sapAccountNumMap.put(sapParentAccount.Account_Number__c, sapParentAccount);
        }

        for (SObject curObj2 : objectMap.values()) {
            Account curAcct = (Account)curObj2;
            Account parentAccount = null;
            if (sapAccountNumMap.containsKey(curAcct.ENSX_EDM__SAP_Customer_Number__c)) parentAccount = sapAccountNumMap.get(curAcct.ENSX_EDM__SAP_Customer_Number__c);

            if (parentAccount != null) {
                curAcct.ParentId = parentAccount.Id;
            }
            else if (!newSAPacctNums.contains(curAcct.ENSX_EDM__SAP_Customer_Number__c)) {
                Account newParent = new Account();
                newParent.Account_Number__c = curAcct.ENSX_EDM__SAP_Customer_Number__c;
				newParent.AccountNumber = curAcct.ENSX_EDM__SAP_Customer_Number__c;
                newParent.BillingCity = curAcct.BillingCity;
                newParent.BillingCountry = curAcct.BillingCountry;
                newParent.BillingPostalCode = curAcct.BillingPostalCode;
                newParent.BillingState = curAcct.BillingState;
                newParent.BillingStreet = curAcct.BillingStreet;
                newParent.Email_address__c = curAcct.Email_address__c;
                newParent.Name = curAcct.Name;
                newParent.Phone = curAcct.Phone;

                newParentAccounts.add(newParent);
                newSAPacctNums.add(curAcct.ENSX_EDM__SAP_Customer_Number__c);

				// Debug for new Parent accounts
                String key = (String) curAcct.get(SFSyncKeyField);
            }
        }

        if (newParentAccounts.size() > 0) upsert newParentAccounts;
    }

    private void findRepAccount(Map<String, SObject> objectMap, List<SObject> errors) {
        Set<String> sapRepAccountNums = new Set<String>();

        for (SObject curObj : objectMap.values()) {
            Account curAcct = (Account)curObj;

            try {
                if (String.isNotBlank(curAcct.Rep_Account__c)) {
                    sapRepAccountNums.add(curAcct.Rep_Account__c);
                }
            } catch (Exception e) {
                String key = (String) curAcct.get(SFSyncKeyField);
                String message = 'Account - Rep Account ' + curAcct.Rep_Account__c + ' is not a valid number from Key ' + key;
                System.debug(message);
                UTIL_SyncHelper.addLog(errors, 'ERROR', key, Json.serialize(curAcct), BatchClassName + ' ' + ObjectType + ' Name missing from Key ' + key);
            }
        }

        Map<String, Account> sapRepAccountNumMap = new Map<String, Account>();
        List<Account> RepAccounts = [SELECT Id, AccountNumber from Account where AccountNumber in : sapRepAccountNums];

        for (Account RepAccount : RepAccounts) {
            sapRepAccountNumMap.put(RepAccount.AccountNumber, RepAccount);
        }

        for (SObject currObj : objectMap.values()) {
            Account currAcct = (Account)currObj;
            Account repAccount = null;
            if (sapRepAccountNumMap.containsKey(currAcct.AccountNumber)) repAccount = sapRepAccountNumMap.get(currAcct.AccountNumber);

            if (repAccount != null && currAcct.Rep_Account2__c == null) {
                currAcct.Rep_Account2__c = repAccount.Id;

                // Debug for found Rep accounts
                String key = (String) currAcct.get(SFSyncKeyField);
            }
        }
    }

    private void syncObject(
        SObject currentObject,
        Object searchResult,
        Map<String, SObject> objectMap,
        List<SObject> errors)
    {
        SBO_EnosixCustSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        if (sboResult.CentralDeletionIndicator == 'X')
        {
            return;
        }
        
        if (!SAP_SalesOrgs.contains(sboResult.SalesOrganization))
        {
            return;
        }

        if (currentObject == null)
        {
            currentObject = new Account();
        }

        Account acct = (Account) currentObject;
        String key = getSboKey(searchResult);
        acct.put(SFSyncKeyField,key);
        acct.FLD_SAP_Account_Group__c = sboResult.CustomerAccountGroup;
        acct.AAG__c = sboResult.AccountAssignmentGroup +' - '+ sboResult.AccountAsgnGrpText;
        acct.Account_Number__c = sboResult.CustomerNumber +' - '+ sboResult.DistributionChannel;
        acct.ENSX_EDM__SAP_Customer_Number__c = sboResult.CustomerNumber;
        acct.Name = UTIL_SyncHelper.combineStrings(sboResult.Name1, sboResult.Name2, '; ');
        if (String.isEmpty(acct.Name))
        {
            System.debug(BatchClassName + ' ' + ObjectType + ' Name missing from Key ' + key);
            UTIL_SyncHelper.addLog(errors, 'ERROR', key, Json.serialize(acct), BatchClassName + ' ' + ObjectType + ' Name missing from Key ' + key);
            return;
        }
        acct.BillingStreet = UTIL_SyncHelper.combineStrings(sboResult.HouseNumber, sboResult.Street, ' ');
        acct.BillingStreet = UTIL_SyncHelper.combineStrings(acct.BillingStreet, sboResult.Street2, '\n');
        acct.BillingStreet = UTIL_SyncHelper.combineStrings(acct.BillingStreet, sboResult.Street3, '\n');
        acct.BillingStreet = UTIL_SyncHelper.combineStrings(acct.BillingStreet, sboResult.Street4, '\n');
        acct.BillingStreet = UTIL_SyncHelper.combineStrings(acct.BillingStreet, sboResult.Street5, '\n');
        if (String.isNotEmpty(sboResult.POBox))
        {
            acct.BillingStreet = UTIL_SyncHelper.combineStrings(acct.BillingStreet, 'PO BOX ' + sboResult.POBox, '\n');
        }
        acct.BillingPostalCode = sboResult.POBoxPostalCode;
        if (String.isNotEmpty(sboResult.CityPostalCode))
        {
            acct.BillingPostalCode = sboResult.CityPostalCode;
        }
        acct.BillingCity = sboResult.City;
        acct.BillingState = sboResult.Region;
        // acct.BillingStateCode = sboResult.Region;
        acct.BillingCountry = sboResult.CountryKey;
        // acct.BillingCountryCode = sboResult.Country;
        acct.Phone = sboResult.TelephoneNo;
        acct.FLD_SAP_Is_Deleted__c = sboResult.CentralDeletionIndicator == 'X';
        acct.CG__c = sboResult.CustomerGroup;
        acct.CG1__c = sboResult.CustomerGroup1;
        acct.CG2__c = sboResult.CustomerGroup2;
        acct.Grp3__c = sboResult.CustomerGroup3;
        acct.Customer_group__c = sboResult.CustomerGroup +' - '+ sboResult.CustomerGroupText;
        acct.Customer_group_1__c = sboResult.CustomerGroup1 +' - '+ sboResult.CustomerGroup1Text;
        acct.Customer_group_2__c = sboResult.CustomerGroup2 +' - '+ sboResult.CustomerGroup2Text;
        acct.Customer_group_3__c = sboResult.CustomerGroup3 +' - '+ sboResult.CustomerGroup3Text;
        acct.DC__c = sboResult.DistributionChannel +' - '+ sboResult.DistributionChannelDescription;

        switch on sboResult.CustomerAccountGroup {
            when '0001' {
                acct.PN__c = 'SP - ' + sboResult.AccountGroupName;
            }
            when '0002' {
                acct.PN__c = 'SH - ' + sboResult.AccountGroupName;
            }
            when '0003' {
                acct.PN__c = 'PY - ' + sboResult.AccountGroupName;
            }
            when '0004' {
                acct.PN__c = 'BP - ' + sboResult.AccountGroupName;
            }
            when else {
                acct.PN__c = sboResult.CustomerCassification + ' - ' + sboResult.AccountGroupName;
            }
        }

        acct.Central_Order_Block_for_Customer__c = sboResult.CentralOrderBlockForCustomer +'-'+ sboResult.CentralOrderBlockText;
        acct.Customer_Order_Block_Sales_Area__c = sboResult.CustomerOrderBlockSalesArea +'-'+ sboResult.SalesAreaOrderBlockText;
        acct.CoCd__c = sboResult.CompanyCode;
        acct.Rep_Partner__c = sboResult.PartnerFunction + ' - ' + sboResult.Description;
        acct.Rep_Account__c = sboResult.Partner;
        acct.Rep_Name__c = sboResult.PartnerName;
        acct.Sales_district__c = sboResult.SalesDistrict + ' - ' + sboResult.SalesDistrictText;
        acct.Sales_Office__c = sboResult.SalesOffice + ' - ' + sboResult.SalesOfficeText;
        acct.SOrg__c = sboResult.SalesOrganization;

        // Find the required stock level for the DC associated with the current account
        switch on sboResult.DistributionChannel {
            when 'CM' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelCM;
            }
            when 'SB' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelSB;
            }
            when 'DI' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelDI;
            }
            when 'GI' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelGI;
            }
            when 'GT' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelGT;
            }
            when 'DT' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelDT;
            }
            when 'MV' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelMV;
            }
            when 'DV' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelDV;
            }
            when 'FH' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelFH;
            }
            when 'WT' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelWT;
            }
            when 'ER' {
                acct.RequiredStockLevel_DC__c = sboResult.ReqStockLevelER;
            }
            when else {
                acct.RequiredStockLevel_DC__c = '';
            }
        }

        if (String.isNotEmpty(sboResult.EMailAddress))
        {
            if (UTIL_SyncHelper.validateEmailAddress(sboResult.EMailAddress))
            {
                acct.FLD_Email__c = sboResult.EMailAddress;
                acct.Email_address__c = sboResult.EMailAddress;
            }
            else
            {
                UTIL_SyncHelper.addLog(errors, 'ERROR', key, Json.serialize(acct),
                    BatchClassName + '; Invalid email found: ' + sboResult.EMailAddress + '; Customer Number: ' + sboResult.CustomerNumber);
            }
        }

        objectMap.put(key, acct);
    }

    private String getSboKey(Object searchResult)
    {
        SBO_EnosixCustSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        return sboResult == null ? '' : sboResult.CustomerNumber.replaceFirst('^0+(?!$)', '') +' - '+sboResult.DistributionChannel;
    }
}