/*=========================================================================================================
* @author : Mahesh Raj, Capgemini
* @date : 07/21/2023
* @description: Queueable apex to update invoke WOwithSA interface.

Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number               Date          Description
------------------------------------------------------------------------------------

============================================================================================================*/
public class SFS_WOforNAStatusQueueable implements Queueable {
public List<WorkOrder> WOToUpdate;
    public SFS_WOforNAStatusQueueable(List<WorkOrder> WOToUpdate){
        this.WOToUpdate = WOToUpdate ;  
        
    }
    public void execute(QueueableContext context) {
       for(WorkOrder Wo : WOToUpdate){
       		if(Wo.SFS_Integration_Status__c == 'N/A' && Wo.EntitlementId != NULL && Wo.Entitlement.ServiceContractId != NULL && Entitlement.ServiceContract.Id != Null ){
           		Wo.SFS_Integration_Resubmit__c =true;
                Wo.SFS_Work_Order_Resubmit__c = true;
            }
    	}
     update   WOToUpdate; 
	}
}