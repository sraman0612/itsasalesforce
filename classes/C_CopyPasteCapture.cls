global class C_CopyPasteCapture {

    public string editorContent {get; set;}
    public Case theCase {get; private set;}
    
    
    public C_CopyPasteCapture (ApexPages.StandardController controller) {
        this.theCase = (Case) controller.getRecord();
        this.editorContent = 'test';
    }
    
    
    
    public PageReference sendContent () {
        System.debug('Editor content: '+ editorContent);
        this.theCase.Email_Body__c = editorContent;
        update this.theCase;
        return null;
    }
    
    
    
    @RemoteAction global static RemoteSaveResult saveImage (String filename, String imageBody) {
        
        // Get the ID of the folder we wish to keep these pasted images in. The folder should be public if images are to be viewed in external emails.
        Id attachmentsFolderId = [Select Id From Folder Where Name = 'Attachments' Limit 1][0].Id;
        
        // Create the document that keeps the image.
        Document doc = new Document();
        doc.FolderId = attachmentsFolderId;
        doc.Name = filename+'_'+DateTime.now();
        doc.Description = filename;
        doc.ContentType = 'image/png';
        doc.Type = 'png';
        doc.Body = EncodingUtil.base64Decode(imageBody);
        doc.IsPublic = true;
        
        // Save the document.
        Database.saveResult result = Database.insert(doc, false);
        
        // Create the URL to the image being saved. This should be org-agnostic.
        String baseOrgURL = System.URL.getSalesforceBaseUrl().toExternalForm();
        Integer firstIndex = baseOrgURL.indexOf('.')+1;
        Integer secondIndex = baseOrgURL.indexOf('.', firstIndex);
        String orgBase = baseOrgURL.substring(firstIndex, secondIndex);
        String contentBaseURL = 'https://c.'+ orgBase +'.content.force.com/servlet/servlet.ImageServer?id=';
        String docID = ((String)doc.Id).substring(0, 15);
        String orgID = ((String)[Select Id, Name From Organization Limit 1][0].Id).substring(0, 15);
        String imageURL = contentBaseURL+docID+'&oid='+orgID;
        
        // Put the results of this operation in a RemoteSaveResult object.
        RemoteSaveResult newResult = new RemoteSaveResult();
        newResult.success = result.isSuccess();
        newResult.successMessage = result.isSuccess() ? imageURL : '';
        newResult.errorMessage = result.isSuccess() ? '' : result.getErrors()[0].getMessage();
        
        return newResult;
    } 
    

    global class RemoteSaveResult {
        public Boolean success;
        public String errorMessage;
        public String successMessage;
    }
}