@isTest
global class C_UserAfterTrggerTest {
    @isTest static void test1()
    {
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
		string profileid = [select id from profile where name='Partner Community User'].id;
        List<contact> cList = new List<Contact>();
        List<user> uUpdt = new List<user>();
        
        Account ap1 = new account(name='test3');
        insert ap1;
        Account ap2 = new account(name='test4');
        insert ap2;
        Account a1 = new account(name='test1', ParentId = ap2.id,Floor_Plan_Account__c = ap1.id);
        insert a1;
        Account a = new account(name='test',ParentId = ap1.id, Floor_Plan_Account__c=a1.id);
        insert a;
        
        contact con = new contact(firstname='j', lastname='b', email='jbtest1@test.com', accountid =a.id);
        //insert con;
        clist.add(con);
        
        contact con1 = new contact(firstname='j2', lastname='b2', email='jbtest2@test.com', accountid =a1.id);
        contact con2 = new contact(firstname='j3', lastname='b3', email='jbtest3@test.com', accountid =a1.id);
        //insert con1;
        clist.add(con1);
        clist.add(con2);
        insert cList;
        
        List<asset> updtAss = new List<asset>();
        Asset ass1 = new Asset();
        ass1.accountID = a1.id;
        ass1.Name = 'test1';
        
        Asset ass2 = new Asset();
        ass2.Current_Servicer__c = a.id;
        ass2.Name = 'test2';
        
        Asset ass3 = new Asset();
        ass3.accountID = a.Floor_Plan_Account__c;
        ass3.Name = 'test3';
        
        updtAss.add(ass1);
        updtAss.add(ass2);
        updtAss.add(ass3);
        insert updtAss;
        
        User u = new User(
            ProfileId = profileid,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Single Account'
        );
         User u2 = new User(
            ProfileId = profileid,
            LastName = 'last1',
            Email = 'puser001@amamama.com',
            Username = 'puser001@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con1.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Global Account'
        );
        User u3 = new User(
            ProfileId = profileid,
            LastName = 'last2',
            Email = 'puser002@amamama.com',
            Username = 'puser002@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ContactId = con2.Id,
            LocaleSidKey = 'en_US',//UserRoleId = r.Id,
            community_user_type__c='Single Account'
        );
        uUpdt.add(u); 
        uUpdt.add(u2);
        uUpdt.add(u3);
        insert uUpdt;
        list<id> uID = new List<id>();
        uID.add(u.id);
        uID.add(u2.id);
        uID.add(u3.id);
        System.RunAs(usr)
        {
            
            Test.startTest();
            
            myFunc2(uID);
            
            Test.stopTest();
            
        }
        //u.Community_User_Type__c='Global Account';
        //u2.Community_User_Type__c='Single Account';
        //update uUpdt;
    }
    
    //@future
	private static void myFunc2(List<id> uUpdt)
   	{
       user[] uList = [select id, Community_User_Type__c,Email from User where id in :uUpdt];
       for(User u : uList)
       {
           if(u.Community_User_Type__c =='Single Account' && u.Email != 'puser002@amamama.com')
               u.Community_User_Type__c = 'Global Account';
           else if(u.Community_User_Type__c =='Global Account')
               u.Community_User_Type__c = 'Single Account';
           else if(u.Email == 'puser002@amamama.com')
               u.Community_User_Type__c = null;
       }
       update uList;
    }

}