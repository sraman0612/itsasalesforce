global with sharing class ENSX_Quote
{
    @AuraEnabled
    public List<ENSX_QuoteLine> LinkedQuoteLines{get;set;}
    @AuraEnabled        
    public String instanceUrl {get;set;}
    @AuraEnabled
    public String QuoteId{get;set;}
    @AuraEnabled
    public String salesOrg{get;set;}
    @AuraEnabled
    public String distChannel{get;set;}
    @AuraEnabled
    public String division{get;set;}
    @AuraEnabled
    public String soldToParty{get;set;}
    @AuraEnabled
    public String shipToParty{get;set;}
    @AuraEnabled
    public String salesDocType{get;set;}
}