global class C_createAssetShareGlobalUsrBatch implements Database.batchable<assetshare>{ 
      
    global final String Query ;
    global final List<string> uIDs;
    List<assetShare> assetShareToCreate = new List<assetShare>();
    
    global C_createAssetShareGlobalUsrBatch(List<assetShare> shareRecs, List<string> usrIDs)
    {
        uIDs = usrIDs;
        assetShareToCreate = shareRecs;
        //delete all asset share records for user associate to contact whose account just changed
        delete [Select id from assetshare where userorgroupid in (Select id from user where id in :uIDs)];
        //if(uIDs != null)
          //  Query='Select id,contact.account.parentID,contact.account.Floor_Plan_Account_ID__c,name, Partner_Account_Parent_ID__c,Community_User_Type__c from User where ID IN :uIDs';
    }
    
    /*global Iterable<assetShare> start(Database.BatchableContext bc)//Database.QueryLocator
    {
      return assetShareToCreate;
    }*/
    global Iterable<assetshare> start(Database.batchableContext bc){ 
        system.debug(assetShareToCreate.size());
        List<assetShare> newShares = assetShareToCreate;
       return newShares;//new C_globalUsrIterableCaller(assetShareToCreate); 
   }  
    
    global void execute(Database.BatchableContext BC, List<assetshare> scope){
        //List<assetShare> assetShareToCreates = new List<assetShare>();
        //assetShareToCreates = scope;
        /*for(User U : scope)
        {
            system.debug(u);
            if(!string.isBlank(u.Partner_Account_Parent_ID__c )){
                for(Asset a : [Select id,name,Parent_AccountID__c from Asset 
                               where Parent_AccountID__c = :u.Partner_Account_Parent_ID__c ])
                {
                    //system.debug('with parent ID: '+a + ': users parentID: '+u.contact.account.parentID);
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
                system.debug(Logginglevel.INFO ,'SIZE TO CREATE ASSET SHARE FOR GLOBAL USER***'+assetShareToCreate.size());
                //continue;
            }
            if(!string.isBlank(u.contact.account.Floor_Plan_Account_ID__c )){
                for(Asset a : [Select id,name,accountID from Asset where accountID = :u.contact.account.Floor_Plan_Account_ID__c])
                {
                    //system.debug('with account ID: '+a+': users floorpan ID: '+u.contact.account.Floor_Plan_Account_ID__c);
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
                //continue;
            }
            if(!string.isBlank(u.Partner_Account_Parent_ID__c)){
                for(Asset a : [Select id,name,Current_Servicer_ID__c from Asset where Current_Servicer_Parent_ID__c = :u.Partner_Account_Parent_ID__c ])
                {
                    //system.debug('with current servicer ID: '+a+': users parentID: '+u.contact.account.Floor_Plan_Account_ID__c);
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
        }
        system.debug(Logginglevel.INFO , 'SIZE TO CREATE ASSET SHARE FOR GLOBAL USER***'+assetShareToCreate.size());*/
        //create Asset Sharing
        Database.SaveResult[] srList = Database.insert(scope, false);
        
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
        system.debug('Finished creating rights!!!');
    }
}