@isTest
private class QuoteLineExtControllerTests {
    
    testMethod static void testSubmit() {
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware', ProductCode = 'ABCDE');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        SBQQ__Quote__c quote = createQuote();
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__Quote__c = quote.Id;
        quoteLine.SBQQ__Product__c = prod.Id;
        quoteLine.SBQQ__Quantity__c = 1;
        insert quoteLine;
        
        Test.startTest();
        QuoteLineExtController con = new QuoteLineExtController(new ApexPages.StandardController(quoteLine));
        con.onSubmit();
        quoteLine = [SELECT ApprovalStatus__c FROM SBQQ__QuoteLine__c WHERE Id = :quoteLine.Id LIMIT 1];
        Test.stopTest();
        
        System.assertEquals('Approved', quoteLine.ApprovalStatus__c);
    }
    
    testMethod static void testRecall() {
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware', ProductCode = 'ABCDE');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        SBQQ__Quote__c quote = createQuote();
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__Quote__c = quote.Id;
        quoteLine.SBQQ__Product__c = prod.Id;
        quoteLine.SBQQ__Quantity__c = 1;
        insert quoteLine;
        
        Test.startTest();
        QuoteLineExtController con = new QuoteLineExtController(new ApexPages.StandardController(quoteLine));
        con.onRecall();
        quoteLine = [SELECT ApprovalStatus__c FROM SBQQ__QuoteLine__c WHERE Id = :quoteLine.Id LIMIT 1];
        Test.stopTest();
        
        System.assertEquals('Recalled', quoteLine.ApprovalStatus__c);
    }
    
    public static SBQQ__Quote__c createQuote() {
        String distributionChannel = 'CM';
        RecordType recordType = [SELECT Id from RecordType where DeveloperName = 'Sales' AND SobjectType = 'Opportunity' LIMIT 1];
        RecordType quoteRecordType = [SELECT Id from RecordType where DeveloperName = 'Quote_Draft' AND SobjectType = 'SBQQ__Quote__c' LIMIT 1];
        
		Account acct = new Account();
        acct.Name = 'Test Account';
        acct.Child_DCs__c = 'XX,YY,';
        acct.Account_Number__c='1234';
        acct.DC__c = 'CP - XXXXXXX';

        insert acct;
        
        Opportunity opp = new Opportunity();

        opp.Name = 'test';
        opp.StageName = 'Stage 1 - Qualified Lead';
        opp.CloseDate = System.today().addMonths(1);
        opp.AccountId = acct.Id;
        opp.End_User_Account__c = acct.Id;
        opp.SBQQ__QuotePricebookId__c = Test.getStandardPricebookId();
        opp.RecordTypeId = recordType.Id;
        opp.Sales_Channel__c = distributionChannel;

        insert opp;

        SBQQ__Quote__c quote = new SBQQ__Quote__c();

        quote.SBQQ__Account__c = acct.Id;
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__Primary__c = true;
        quote.RecordTypeId = quoteRecordType.Id;
        quote.End_Customer_Account__c = acct.Id;
        quote.Distribution_Channel__c = distributionChannel;

        insert quote;
        
        return quote;
	}
}