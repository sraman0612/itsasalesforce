public class UTIL_ServiceNotification {
    public static SBO_EnosixServiceNotification_Detail.EnosixServiceNotification populateWithMachineStartupForm(SBO_EnosixServiceNotification_Detail.EnosixServiceNotification notification, String startupFormId) {
        Machine_Startup_Form__c startupForm = getStartupForm(startupFormId);
        
        Asset serialNumber = getSerialNumber(startupForm.Serial_Number__c);
        Account assetAccount = null;

        if (!String.isBlank(startupForm.Distributor__c)) {
            assetAccount = getAccount(startupForm.Account__c);
        }

        notification.NotificationType = 'Z2';
        notification.NotificationDescription = 'Default';
        notification.CustomerNumber = assetAccount.AccountNumber;
        notification.SalesOrganization = serialNumber.SOrg__c;
        notification.DistributionChannel = serialNumber.DChl__c;
        notification.Division = '00';
        notification.PartNo = serialNumber.Model_Number__c;
        notification.Serial = serialNumber.Name;
        notification.EquipmentNumber = serialNumber.Equipment_Number__c;
        notification.MALFUNCTION_BREAKDOWN.EndDateofMalfunction = startupForm.Date__c;

        notification.EQUIPMENT_ADDRESS.NameLine1 = serialNumber.End_User_Account__r.Name;
        notification.EQUIPMENT_ADDRESS.Street = serialNumber.End_User_Account__r.BillingStreet;
        notification.EQUIPMENT_ADDRESS.City = serialNumber.End_User_Account__r.BillingCity;
        notification.EQUIPMENT_ADDRESS.Region = serialNumber.End_User_Account__r.BillingState;
        notification.EQUIPMENT_ADDRESS.Country = serialNumber.End_User_Account__r.BillingCountry;
        notification.EQUIPMENT_ADDRESS.CountryDistrict = serialNumber.End_User_Account__r.County__c;
        notification.EQUIPMENT_ADDRESS.PostalCode = serialNumber.End_User_Account__r.BillingPostalCode;

        return notification;
    }

    public static String generateClaimText(String startupFormId) {
        Machine_Startup_Form__c startupForm = getStartupForm(startupFormId);

        String claimText = '';

        if (!String.isBlank(startupForm.NOTES__c)) {
            claimText += startupForm.NOTES__c;
        }

        if (!String.isBlank(startupForm.Contact_Email__c)) {
            claimText += '\n\nClaim contact email: ' + startupForm.Contact_Email__c;
        }

        if (!String.isBlank(startupForm.SAP_Messages__c)) {
            claimText += '\n\n~Messages\n' + startupForm.SAP_Messages__c;
        }

        return claimText;
    }

    public static SBO_EnosixSO_Detail.EnosixSO populateSalesOrderForStartupForm(SBO_EnosixSO_Detail.EnosixSO salesOrder, String formId, String serviceNotificationId, List<SBO_EnosixServiceNotification_Detail.PARTNERS> partners) {
        Machine_Startup_Form__c startupForm = getStartupForm(formId);

        Asset serialNumber = getSerialNumber(startupForm.Serial_Number__c);
        Account assetAccount = null;

        if (!String.isBlank(startupForm.Distributor__c)) {
            assetAccount = getAccount(startupForm.Account__c);
        }

        salesOrder.SoldToParty = assetAccount.AccountNumber;
        salesOrder.NotificationNumber = serviceNotificationId;
        salesOrder.Material = serialNumber.Model_Number__c;
        salesOrder.SerialNumber = serialNumber.Name;
        salesOrder.Material = serialNumber.Model_Number__c;

        salesOrder.SALES.SalesDocumentType = 'ZIDW';
        salesOrder.SALES.SalesOrganization = serialNumber.SOrg__c;
        salesOrder.SALES.DistributionChannel = serialNumber.DChl__c;
        salesOrder.SALES.Division = '00';
        salesOrder.BILLING.DateOnWhichServicesRendered = startupForm.Date__c;

        if (String.isBlank(startupForm.Customer_Ref_Number__c)) {
            salesOrder.CustomerPurchaseOrderNumber = 'None';
        } else {
            salesOrder.CustomerPurchaseOrderNumber = startupForm.Customer_Ref_Number__c;
        }

        if (String.isNotEmpty(startupForm.Startup_Performed_By_Number__c)) {
            SBO_EnosixSO_Detail.PARTNERS soPartner = new SBO_EnosixSO_Detail.PARTNERS();
            salesOrder.PARTNERS.add(soPartner);
            soPartner.PartnerFunction = 'ZW';
            soPartner.ContactPersonNumber = startupForm.Startup_Performed_By_Number__c;
            soPartner.CustomerNumber = startupForm.Startup_Performed_By_Number__c;
        }
        
        User reportedBy = null;
        try {
            reportedBy = [SELECT Id, Name from User where Id = : UserInfo.getUserId() LIMIT 1];
            
            salesOrder.ORDERDATA.YourReference = reportedBy.Name;
        } catch (Exception e) {
            
        }
        
        if (String.isNotEmpty(startupForm.Startup_Performed_By_Number__c)) {
            SBO_EnosixSO_Detail.PARTNERS soPartner = new SBO_EnosixSO_Detail.PARTNERS();
            salesOrder.PARTNERS.add(soPartner);
            soPartner.PartnerFunction = 'ZW';
            soPartner.ContactPersonNumber = startupForm.Startup_Performed_By_Number__c;
            soPartner.CustomerNumber = startupForm.Startup_Performed_By_Number__c;
        }
        
        Integer itemNumber = 10;

        //Item 10 model
        SBO_EnosixSO_Detail.ITEMS item = new SBO_EnosixSO_Detail.ITEMS();
        item.ItemNumber = String.valueOf(itemNumber);
        item.Material = serialNumber.Model_Number__c;
        item.SerialNumber = serialNumber.Name;
        item.OrderQuantity = 1;
        String hoursInService = startupForm.Operating_hours__c;
            
        if (String.isBlank(hoursInService)) {
            hoursInService = '';
        }
        
        item.ItemDescription = serialNumber.Name + '*' + hoursInService.leftPad(6);


        salesOrder.ITEMS.add(item);
        
        if (String.isNotBlank(startupForm.Company_Name__c)) {
            String county = null;

            RFC_Z_ENSX_GET_TAX_JURISDICTION rfc = new RFC_Z_ENSX_GET_TAX_JURISDICTION();
            rfc.PARAMS.IV_CITY = serialNumber.End_User_Account__r.BillingCity;
            rfc.PARAMS.IV_COUNTRY = serialNumber.End_User_Account__r.BillingCountry;
            rfc.PARAMS.IV_REGION = serialNumber.End_User_Account__r.BillingState;
            rfc.PARAMS.IV_ZIPCODE = serialNumber.End_User_Account__r.BillingPostalCode;

            RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT rfcResult = rfc.execute();
            
            if (rfcResult != null) {
                List<RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS> jurisdictions = rfcResult.ET_LOCATION_RESULTS_List;
    
                if (jurisdictions != null && !jurisdictions.isEmpty()) {
                    county = jurisdictions.get(0).COUNTY;
                }
            }
            
            SBO_EnosixSO_Detail.PARTNERS shipToPtnr = new SBO_EnosixSO_Detail.PARTNERS();
            shipToPtnr.PartnerFunction = UTIL_Customer.SHIP_TO_PARTNER_CODE;
            shipToPtnr.CustomerNumber = 'DROP-SHIP';
            shipToPtnr.PartnerName = serialNumber.End_User_Account__r.Name;
            shipToPtnr.Street = serialNumber.End_User_Account__r.BillingStreet;
            shipToPtnr.City = serialNumber.End_User_Account__r.BillingCity;
            shipToPtnr.Region = serialNumber.End_User_Account__r.BillingState;
            shipToPtnr.PostalCode = serialNumber.End_User_Account__r.BillingPostalCode;
            shipToPtnr.Country = serialNumber.End_User_Account__r.BillingCountry;
            shipToPtnr.TelephoneNumber = serialNumber.End_User_Account__r.Phone;
            shipToPtnr.District = county;
            salesOrder.PARTNERS.add(shipToPtnr);
        }

        return salesOrder;
    }

    private static Account getAccount(String accountId) {
        return [SELECT Id, AccountNumber from Account where Id = : accountId LIMIT 1];
    }

    private static Asset getSerialNumber(String serialNumberId) {
        if (serialNumberId == null) {
            return null;
        }

        Asset retVal = [Select Id,
                Name,
                SOrg__c,
                DChl__c,
                End_User_Account__r.Name,
                End_User_Account__r.BillingStreet,
                End_User_Account__r.BillingState,
                End_User_Account__r.BillingCity,
                End_User_Account__r.BillingPostalCode,
                End_User_Account__r.BillingCountry,
                End_User_Account__r.County__c,
                End_User_Account__r.Phone,
                End_User_Account__r.Fax,
                Equipment_Number__c,
                Model_Number__c
            From Asset where Id = : serialNumberId LIMIT 1];

            return retVal;
    }

    private static Machine_Startup_Form__c getStartupForm(String startupFormId) {
                Machine_Startup_Form__c startupForm = [SELECT Id, 
                                                    Account__c,
                                                    Advisory_Information__c,
                                                    Airend_High_Pressure_Serial_Number__c,
                                                    Airend_Serial_Number__c,
                                                    All_connections_checked_for_tightness_an__c,
                                                    All_pipe_fittings_and_connections_are_ch__c,
                                                    Ampere_1__c,
                                                    Ampere_2__c,
                                                    Ampere_3__c,
                                                    Ampere_4__c,
                                                    Ampere_5__c,
                                                    Ample_space_is_provided_around_the_packa__c,
                                                    Auto_pressure__c,
                                                    Capture_IMEI_number_from_iConn_modem__c,
                                                    Cat_No__c,
                                                    Champion_Line_Reactor_Model_No_if_such__c,
                                                    Champion_Premium_Warranty_Registration__c,
                                                    Choose_a_Startup_Form_Type_below__c,
                                                    City__c,
                                                    Comments_on_customer_water_system__c,
                                                    Company_Name__c,
                                                    Connect_the_wire_to_the_modem__c,
                                                    Contact__c,
                                                    Contact_Email__c,
                                                    Contact_Name__c,
                                                    Country__c,
                                                    CreatedById,
                                                    CreatedDate,
                                                    Customer_Signee_Name__c,
                                                    Customer_Ref_Number__c,
                                                    Date__c,
                                                    Describe_application_and_installation_co__c,
                                                    Describe_Operating_Environment__c,
                                                    Differential_press__c,
                                                    Discharge_temp__c,
                                                    Disconnect_lockout_and_tagout_power_sup__c,
                                                    Distributor__c,
                                                    Distributor_Name__c,
                                                    Ensure_customer_has_access_to_the_manual__c,
                                                    Exchange_any_two_phases_of_the_incoming__c,
                                                    GD_Line_Reactor_Model_Number__c,
                                                    Grounding_code_used_for_installation__c,
                                                    Hertz_1__c,
                                                    Hertz_2__c,
                                                    Hertz_3__c,
                                                    Hold_down_bolts_cap_screws_are_properl__c,
                                                    I_D_No_1__c,
                                                    I_D_No_2__c,
                                                    Identify_Target_Pressure__c,
                                                    If_equipped_with_cooling_duct_work_is_i__c,
                                                    If_the_customer_does_not_wish_to_activat__c,
                                                    IL1_On__c,
                                                    IL2_On__c,
                                                    IL3_On__c,
                                                    Incoming_Wire_Size__c,
                                                    Incoming_wiring_properly_sized_installed__c,
                                                    Inlet_temp__c,
                                                    Inlet_water_temperature_between_60_90_F__c,
                                                    Input_f_Volts_H_1__c,
                                                    Input_f_Volts_H_2__c,
                                                    Input_f_Volts_H_3__c,
                                                    Interstage_Press_1__c,
                                                    Interstage_Press_2__c,
                                                    Interstage_press__c,
                                                    Interstage_temp__c,
                                                    Is_plant_pressure_stable__c,
                                                    IsDeleted,
                                                    kW_HP_1__c,
                                                    kW_HP_2__c,
                                                    Length_of_Incoming_Wire__c,
                                                    Load_percentage__c,
                                                    Manual_Items_Reviewed__c,
                                                    Model_No_1__c,
                                                    Model_No_2__c,
                                                    Model_No_3__c,
                                                    Motor_1_current__c,
                                                    Motor_1_freq__c,
                                                    Motor_1_power__c,
                                                    Motor_1_speed__c,
                                                    Motor_2_current__c,
                                                    Motor_2_freq__c,
                                                    Motor_2_power__c,
                                                    Motor_2_speed__c,
                                                    Motor_3_current__c,
                                                    Motor_3_freq__c,
                                                    Motor_3_power__c,
                                                    Motor_3_speed__c,
                                                    Motor_Rating_1__c,
                                                    Motor_Rating_2__c,
                                                    Motor_Rating_3__c,
                                                    Name,
                                                    No_Champion_Warranty_Registration__c,
                                                    No_Warranty_Registration__c,
                                                    NOTES__c,
                                                    Oil_level_is_adequate__c,
                                                    Oil_press_manifold__c,
                                                    Operating_hours__c,
                                                    Overall_appearance_and_condition_is_good__c,
                                                    Owner_Contact_Email__c,
                                                    Owner_Contact_Name__c,
                                                    Owner_Contact_Phone__c,
                                                    Package_inlet_water_temp__c,
                                                    Package_outlet_water_temp__c,
                                                    Piping_sized_right_for_adequater_flow_ra__c,
                                                    Plant_press__c,
                                                    Plant_temp__c,
                                                    Platinum_Extended_Warranty_Registration__c,
                                                    Pressure_relief_valve_s_are_provided_wh__c,
                                                    Proper_operating_procedures__c,
                                                    Pull_the_antenna_out_of_its_box_attach__c,
                                                    Recommended_routine_maintenance__c,
                                                    RecordTypeId,
                                                    Remarks__c,
                                                    Reservoir_press__c,
                                                    Review_Air_Filter__c,
                                                    Review_Compressor_Lubrication_Separati__c,
                                                    Review_Controls_and_Instrumentation__c,
                                                    Review_Foreword__c,
                                                    Review_Heat_Exchangers_Oil_Air__c,
                                                    Review_Inlet_Control_Valve__c,
                                                    Review_Installation__c,
                                                    Review_Maintenance_Schedule__c,
                                                    Review_Minimum_Pressure_Valve__c,
                                                    Review_Motor_Lubrication__c,
                                                    Review_Pressure_Relief_Valve__c,
                                                    Review_Shaft_Coupling__c,
                                                    Review_Starting_and_Operating_Procedures__c,
                                                    Review_Troubleshooting__c,
                                                    Review_Ventilation_Fans__c,
                                                    Safety_Instructions__c,
                                                    SAP_Messages__c,
                                                    SAP_Status__c,
                                                    SAP_Service_Notification_Number__c,
                                                    SAP_Sales_Order_Number__c,
                                                    Separator_press__c,
                                                    Separator_temp__c,
                                                    Serial_No_1__c,
                                                    Serial_No_2__c,
                                                    Serial_Number__c,
                                                    Signature_Capture__c,
                                                    Startup_Performed_By_Number__c,
                                                    State__c,
                                                    Street__c,
                                                    Supply_Voltage__c,
                                                    Surface_makes_100_contact_with_bottom_o__c,
                                                    Surface_supports_package_weight__c,
                                                    SystemModstamp,
                                                    Target_pressure__c,
                                                    TOTAL_MOTOR_CURRENT__c,
                                                    TOTAL_MOTOR_POWER__c,
                                                    Type_of_Business__c,
                                                    Unit_started_and_performing_well__c,
                                                    Unload_pressure__c,
                                                    V1_dc_main_motor_1__c,
                                                    V1_freq_main_motor_1__c,
                                                    V1_temp_vfd_sink_2__c,
                                                    V1_temp_vfd_sink__c,
                                                    V1_version_firmware__c,
                                                    V2_dc_main_motor_2__c,
                                                    V2_freq_main_motor_2__c,
                                                    V2_temp_vfd_sink_2__c,
                                                    V2_temp_vfd_sink__c,
                                                    V3_dc_main_motor_3__c,
                                                    V3_freq_main_motor_3__c,
                                                    V3_temp_vfd_sink_2__c,
                                                    V3_temp_vfd_sink__c,
                                                    Verify_that_a_set_of_manuals_and_diagram__c,
                                                    VL1_L2_Off__c,
                                                    VL1_L2_On__c,
                                                    VL2_L3_Off__c,
                                                    VL2_L3_On__c,
                                                    VL3_L1_Off__c,
                                                    VL3_L1_On__c,
                                                    Volts_1__c,
                                                    Volts_2__c,
                                                    Water_pressure__c,
                                                    Water_PSIG_between_40_75_PSIG_at_full_ra__c,
                                                    Water_shutoff_valve_installed__c,
                                                    Will_ambient_cond_get_below_freezing__c,
                                                    Yes_Champion_Warranty_Registration__c,
                                                    Yes_Warranty_Registration__c,
                                                    Zip_Postal_Code__c
                                             from Machine_Startup_Form__c where Id = : startupFormId LIMIT 1];

        return startupForm;
    }
}