@isTest
public class C_OilSample_SHTest {
	@isTest static void testOilSample()
    {
        Warranty_Compliance__c CS = Warranty_Compliance__c.getOrgDefaults();
        cs.Oil_Sample_Date_Range__c = 1.10;
        cs.Early_Visit_Threshold_Days__c =45;
        cs.Oil_Filter_Hours__c=1000;
        insert cs;
        
        account ap = new account(name='test1 AP');
        insert ap;
        
        account a = new account(name='test1', parentID=ap.id);
        insert a;
        
        contact c = new contact(LastName='SF', FirstName='F');
        c.AccountId = a.Id;
        insert c;
        
        contact c2 = new contact(LastName='SF2', FirstName='F2');
        c2.AccountId = a.Id;
        insert c2;
        
        Id p = [select id from profile where name='Partner Community User'].id;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = c.Id,community_user_type__c='Global Account',
                timezonesidkey='America/Los_Angeles', username='j.knitter@gmail.com');
       
        insert user;
        
        User user2 = new User(alias = 'test1234', email='test1234@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = c2.Id,community_user_type__c='Single Account',
                timezonesidkey='America/Los_Angeles', username='j.brooks@gmail.com');
       
        insert user2;
        
        User u = [select id, partner_account_id__c from user where username='j.knitter@gmail.com'];
        Oil_Sample__c os = new Oil_Sample__c(Serial_Number_Text__c='1234C',Collect_Date__c=system.today().addDays(-1),Report_Number__c='',Collecting_Distributor__c='',Distributor__c=a.Id );
        insert os;
        system.debug('****: '+ os + ' '+u+ ' ' + c);
        
        Asset aset = new Asset(name = os.Serial_Number_Text__c,SerialNumber='1234C',Current_Servicer__c=a.id,Serial_Number_Model_Number__c='1234C');
        aset.AccountId = a.Id;
        aset.ContactId = c.Id;
        insert aset;
        
        Service_History__c sh = new Service_History__c(Serial_Number_MD__c =aset.id,Serial_Number__c=aset.id,Date_of_Service__c=system.today());
        
        insert sh;
        
        
        C_OilSample_SH cosSH= new C_OilSample_SH(os);
        os.Report_Number__c = null;
        update OS;
        C_OilSample_SH cosSH1= new C_OilSample_SH(os);
    }
    
}