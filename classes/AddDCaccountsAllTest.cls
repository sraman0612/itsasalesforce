@IsTest
private class AddDCaccountsAllTest {

    class MockSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        Boolean isSuccess;
        public MockSearch(Boolean isSuccess)
        {
            this.isSuccess = isSuccess;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            SBO_EnosixZAPR_Search.EnosixZAPR_SC searchContext = (SBO_EnosixZAPR_Search.EnosixZAPR_SC)sc;
            SBO_EnosixZAPR_Search.EnosixZAPR_SR searchResult =
                new SBO_EnosixZAPR_Search.EnosixZAPR_SR();

            if (isSuccess)
            {
                SBO_EnosixZAPR_Search.SEARCHRESULT result = new SBO_EnosixZAPR_Search.SEARCHRESULT();
                result.SalesOrganization = searchContext.SEARCHPARAMS.SalesOrganization;
                result.DistributionChannel = searchContext.SEARCHPARAMS.DistributionChannel;
                result.Country = searchContext.SEARCHPARAMS.Country;
                result.Region = searchContext.SEARCHPARAMS.Region;
                result.County = searchContext.SEARCHPARAMS.County;
                result.CustomerNumber = '12345';
                result.Sequence = '1';
                result.SalesDistrict = '43';
                result.SalesDistrictName = 'McTester,Testy';

                searchResult.SearchResults.add(result);

                result = new SBO_EnosixZAPR_Search.SEARCHRESULT();
                result.SalesOrganization = searchContext.SEARCHPARAMS.SalesOrganization;
                result.DistributionChannel = searchContext.SEARCHPARAMS.DistributionChannel;
                result.Country = searchContext.SEARCHPARAMS.Country;
                result.Region = searchContext.SEARCHPARAMS.Region;
                result.County = searchContext.SEARCHPARAMS.County;
                result.CustomerNumber = '32355';
                result.Sequence = '2';
                result.SalesDistrict = '43';
                result.SalesDistrictName = 'Nasty McRudey';

                searchResult.SearchResults.add(result);
            }

            searchResult.setSuccess(isSuccess);
            searchContext.baseResult = searchResult;

			return searchContext;
        }
    }

    @testSetup
    static void setup() {
        List<Lead> leads = new List<Lead>();
        // add test leads
        //for (Integer i = 0; i < 50; i++) {
            leads.add(new Lead(
                FirstName = 'Test',
                LastName = 'Lead', // '+i,
                Company = 'TestCo',
                Country = 'US',
                State = 'IL',
                ProductCategory__c = 'Blower'
            ));
        //}
        insert leads;
    }

    static testmethod void testQueueable1() {

        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSearch(true));

        // Set up test data to pass to queueable class
        Lead currLead = [SELECT Id,FLD_Sales_Organization__c,FLD_Customer_Numbers__c FROM Lead LIMIT 1];
        String salesOrg = 'GDMI';
        String dcVal = 'DV';
        String country = 'US';
        String region = 'NY';
        String county = null;
        // Create our Queueable instance
        AddDCaccounts adca = new AddDCaccounts(currLead, salesOrg, dcVal, country, region, county);
        // startTest/stopTest block to force async processes to run
        Test.startTest();
        System.enqueueJob(adca);
        Test.stopTest();
        // Validate the job ran. Check if the correct number of leads were created
        System.assertEquals(1, [SELECT count() FROM Lead WHERE FLD_Sales_Organization__c = :currLead.FLD_Sales_Organization__c]);
    }

    static testmethod void testQueueable2() {

        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSearch(true));

        // Set up test data to pass to queueable class
        Lead currLead = [SELECT Id,FLD_Sales_Organization__c,FLD_Customer_Numbers__c FROM Lead LIMIT 1];
        String salesOrg = 'GDMI';
        String dcVal = 'FH';
        String country = 'US';
        String region = 'NY';
        String county = null;
        // Create our Queueable instance
        AddDCaccounts2 adca = new AddDCaccounts2(currLead, salesOrg, dcVal, country, region, county);
        // startTest/stopTest block to force async processes to run
        Test.startTest();
        System.enqueueJob(adca);
        Test.stopTest();
        // Validate the job ran. Check if the correct number of leads were created
        System.assertEquals(1, [SELECT count() FROM Lead WHERE FLD_Sales_Organization__c = :currLead.FLD_Sales_Organization__c]);
    }

    static testmethod void testQueueable3() {

        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSearch(true));

        // Set up test data to pass to queueable class
        Lead currLead = [SELECT Id,FLD_Sales_Organization__c,FLD_Customer_Numbers__c FROM Lead LIMIT 1];
        String salesOrg = 'GDMI';
        String dcVal = 'GT';
        String country = 'US';
        String region = 'NY';
        String county = null;
        // Create our Queueable instance
        AddDCaccounts3 adca = new AddDCaccounts3(currLead, salesOrg, dcVal, country, region, county);
        // startTest/stopTest block to force async processes to run
        Test.startTest();
        System.enqueueJob(adca);
        Test.stopTest();
        // Validate the job ran. Check if the correct number of leads were created
        System.assertEquals(1, [SELECT count() FROM Lead WHERE FLD_Sales_Organization__c = :currLead.FLD_Sales_Organization__c]);
    }

    static testmethod void testQueueable4() {

        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSearch(true));

        // Set up test data to pass to queueable class
        Lead currLead = [SELECT Id,FLD_Sales_Organization__c,FLD_Customer_Numbers__c FROM Lead LIMIT 1];
        String salesOrg = 'GDMI';
        String dcVal = 'MV';
        String country = 'US';
        String region = 'NY';
        String county = null;
        // Create our Queueable instance
        AddDCaccounts4 adca = new AddDCaccounts4(currLead, salesOrg, dcVal, country, region, county);
        // startTest/stopTest block to force async processes to run
        Test.startTest();
        System.enqueueJob(adca);
        Test.stopTest();
        // Validate the job ran. Check if the correct number of leads were created
        System.assertEquals(1, [SELECT count() FROM Lead WHERE FLD_Sales_Organization__c = :currLead.FLD_Sales_Organization__c]);
    }

    static testmethod void testQueueable5() {

        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSearch(true));

        // Set up test data to pass to queueable class
        Lead currLead = [SELECT Id,FLD_Sales_Organization__c,FLD_Customer_Numbers__c FROM Lead LIMIT 1];
        String salesOrg = 'GDMI';
        String dcVal = 'WT';
        String country = 'US';
        String region = 'NY';
        String county = null;
        // Create our Queueable instance
        AddDCaccounts5 adca = new AddDCaccounts5(currLead, salesOrg, dcVal, country, region, county);
        // startTest/stopTest block to force async processes to run
        Test.startTest();
        System.enqueueJob(adca);
        Test.stopTest();
        // Validate the job ran. Check if the correct number of leads were created
        System.assertEquals(1, [SELECT count() FROM Lead WHERE FLD_Sales_Organization__c = :currLead.FLD_Sales_Organization__c]);
    }
}