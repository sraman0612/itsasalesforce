/**=========================================================================================================
* @author: Bhargavi Nerella, Capgemini
* @date: 04/05/2022
* @description: Apex Class for SFS_ProductRequiredTrigger.
  Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

=============================================================================================================*/
public class SFS_ProductRequiredTriggerHandler {
//SIF-48 Delete Products Required in WOLI if the related WOLI's Asset's Asset parts required exists with same product and quantity 0
    public static void deleteRecords(List<ProductRequired> productReqList){
        Set<Id> woliIds=new Set<Id>();
        for(ProductRequired record:productReqList){
            if(record.ParentRecordType=='Work Order Line Item'){
                woliIds.add(record.ParentRecordId);
            }
        }
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        if(woliIds.size()>0){
            woliList=[Select Id,AssetId from WorkOrderLineItem where Id IN:woliIds];
        }
        Map<Id,Id> woliToAssetId=new Map<Id,Id>();
        Set<Id> assetIds=new Set<Id>();
        if(woliList.size()>0){
            for(WorkOrderLineItem woli:woliList){
                assetIds.add(woli.AssetId);
                woliToAssetId.put(woli.Id,woli.AssetId);
            }
        }
        Map<Id,List<CAP_IR_Asset_Part_Required__c>> partRequiredMap=new Map<Id,List<CAP_IR_Asset_Part_Required__c>>();
        List<Asset> assetList=[Select Id,Name,(Select Id,Name,Product__c,SFS_Quantity__c from CAP_IR_Asset_Product_Required__r) 
                               from Asset where Id IN:assetIds];
        for(Asset rec:assetList){
            partRequiredMap.put(rec.Id,rec.CAP_IR_Asset_Product_Required__r);
        }
        Set<Id> deletedIds=new Set<Id>();
        for(ProductRequired record:productReqList){
            System.debug('parent type'+record.ParentRecordType);
            if(record.ParentRecordType=='Work Order Line Item'){
                Id assetId=woliToAssetId.get(record.ParentRecordId);
                List<CAP_IR_Asset_Part_Required__c> partRequiredList = new List<CAP_IR_Asset_Part_Required__c>();
                if(partRequiredMap.containsKey(assetId)){
                    partRequiredList=partRequiredMap.get(assetId);
                }
                if(partRequiredList.size()>0){
                    for(CAP_IR_Asset_Part_Required__c part:partRequiredList){
                        if(part.Product__c==record.Product2Id && part.SFS_Quantity__c==0){
                            deletedIds.add(record.Id);
                        }
                    }
                }
            }
        }
        if(deletedIds.size()>0){ //added the condition for SOQL error
            List<ProductRequired> deletedList=[Select Id from ProductRequired where Id IN:deletedIds];
            system.debug('39--'+deletedList.size());
            delete deletedList;
        }
    }
    public static void createAssetRecords(List<ProductRequired> productReqList){
        Set<Id> PRIds=new Set<Id>();
        Id WorkTypeIds,Prodt;
        Boolean k1=false;
        Boolean k2=false;
        Boolean k4=false;
        Boolean k6=false;
        Boolean k8=false;
        Boolean k12=false;
        Boolean k16=false;
        Boolean k24=false;
        Boolean k48=false;
        Decimal Qunatity;
        String RecordTypeName;
        if(productReqList.size()>0){
            for(ProductRequired record:productReqList){
          		PRIds.add(record.Id);
            	WorkTypeIds=record.ParentRecordId;
            	Prodt = record.Product2Id;
            	Qunatity = record.QuantityRequired;
                RecordTypeName= record.ParentRecordType;
            	k1 = record.SFS_1k__c;
            	k2 = record.SFS_2k__c;
            	k4 = record.SFS_4k__c;
            	k6 = record.SFS_6k__c;
            	k8 = record.SFS_8k__c;
            	k12 = record.SFS_12k__c;
            	k16 = record.SFS_16k__c;
            	k24 = record.SFS_24k__c;
            	k48 = record.SFS_48k__c;
            
        	}
        	List<CAP_IR_Asset_Part_Required__c> createdList=new List<CAP_IR_Asset_Part_Required__c>();
            List<Asset> assetList=new List<Asset>();
            if(WorkTypeIds!=null && (RecordTypeName=='WorkType' || RecordTypeName=='Work Type')){
                WorkType WorkTypeList=[Select Id,CTS_Frame_Type__c from WorkType where Id =:WorkTypeIds];
                if(WorkTypeList.CTS_Frame_Type__c!=null)
                	assetList=[Select Id,CTS_Frame_Type__c from Asset where CTS_Frame_Type__c =:WorkTypeList.CTS_Frame_Type__c];
            }
            if(assetList.size()>0){
                for(Asset asst:assetList){
            		CAP_IR_Asset_Part_Required__c newRecord=new CAP_IR_Asset_Part_Required__c();
                	newRecord.SFS_Quantity__c=Qunatity;
                	newRecord.Product__c=Prodt;
                	newRecord.SFS_1k__c=k1;
                	newRecord.SFS_2k__c=k2;
                	newRecord.SFS_4k__c=k4;
                	newRecord.SFS_6k__c=k6;
                	newRecord.SFS_8k__c=k8;
                	newRecord.SFS_12k__c=k12;
                	newRecord.SFS_16k__c=k16;
                	newRecord.SFS_24k__c=k24;
                	newRecord.SFS_48k__c=k48;
                	newRecord.Asset__c=asst.Id;
                	createdList.add(newRecord);
        	}
      	}        
        if(createdList.size()>0){
          insert createdList;      
       	}		
        
        }
    }
    public static void updateAssetRecords(List<ProductRequired> productReqList){
        Set<Id> PRIds=new Set<Id>();
        Id WorkTypeIds,Prodt;
        Boolean k1=false;
        Boolean k2=false;
        Boolean k4=false;
        Boolean k6=false;
        Boolean k8=false;
        Boolean k12=false;
        Boolean k16=false;
        Boolean k24=false;
        Boolean k48=false;
        Decimal Qunatity;
        String RecordTypeName;
        if(productReqList.size()>0){
            for(ProductRequired record:productReqList){
          	PRIds.add(record.Id);
            WorkTypeIds=record.ParentRecordId;
            Prodt = record.Product2Id;
            Qunatity = record.QuantityRequired;
            RecordTypeName= record.ParentRecordType;
            k1 = record.SFS_1k__c;
            k2 = record.SFS_2k__c;
            k4 = record.SFS_4k__c;
            k6 = record.SFS_6k__c;
            k8 = record.SFS_8k__c;
            k12 = record.SFS_12k__c;
            k16 = record.SFS_16k__c;
            k24 = record.SFS_24k__c;
            k48 = record.SFS_48k__c;
            
        	}
        	List<CAP_IR_Asset_Part_Required__c> updatedList=new List<CAP_IR_Asset_Part_Required__c>();
            List<Asset> assetList=new List<Asset>();   
            List<CAP_IR_Asset_Part_Required__c> APRList =new List<CAP_IR_Asset_Part_Required__c>();
        	if(WorkTypeIds!=null && (RecordTypeName=='WorkType' || RecordTypeName=='Work Type')){
                WorkType WorkTypeList=[Select Id,CTS_Frame_Type__c from WorkType where Id =:WorkTypeIds];
                if(WorkTypeList.CTS_Frame_Type__c!=null)
            		assetList=[Select Id,CTS_Frame_Type__c from Asset where CTS_Frame_Type__c =:WorkTypeList.CTS_Frame_Type__c];
        	}
        	if(assetList.size()>0){
        		APRList =[Select Id,SFS_Quantity__c,Product__c,SFS_1k__c,SFS_2k__c,SFS_4k__c,SFS_6k__c,
                                                      SFS_8k__c,SFS_12k__c,SFS_16k__c,SFS_24k__c,SFS_48k__c
                                                      from CAP_IR_Asset_Part_Required__c where Asset__c =:assetList and Product__c=:Prodt];
        	}
        	if(APRList.size()>0){
           		for(CAP_IR_Asset_Part_Required__c apr:APRList){
                	apr.SFS_Quantity__c=Qunatity;
                	apr.Product__c=Prodt;
                	apr.SFS_1k__c=k1;
                	apr.SFS_2k__c=k2;
                	apr.SFS_4k__c=k4;
                	apr.SFS_6k__c=k6;
                	apr.SFS_8k__c=k8;
                	apr.SFS_12k__c=k12;
                	apr.SFS_16k__c=k16;
                	apr.SFS_24k__c=k24;
                	apr.SFS_48k__c=k48;
                	updatedList.add(apr);
        		} 
        	}
        	if(updatedList.size()>0){
        		update updatedList;
        	}
        }
        
    }
}