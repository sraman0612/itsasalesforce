global class C_createAssetShareOnContactBatch implements Database.Batchable<sObject> {
    
    global final String Query ;
    global final List<string> cIDs;
    
    global C_createAssetShareOnContactBatch(string q, List<string> contIDs)
    {
        cIDs = contIDs;
        
        //delete all asset share records for user associate to contact whose account just changed
        delete [Select id from assetshare where userorgroupid in (Select id from user where contactID in :cIDs)];
        if(q != null)
            Query=q;
        else
            Query='Select id,account.parentID,accountID,account.Floor_Plan_Account_ID__c from contact where ID IN: cIDs';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
      return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> scope){
        List<assetShare> assetShareToCreate = new List<assetShare>();
        
        
        for(Contact c : scope)
        {
            //global
            for(User u : [Select id from user where contactID in :cIDs and toLabel(community_user_type__c) = 'Global Account'])
            {
                if(!string.isBlank(string.valueOf(c.account.parentID).substring(0,15)) || !string.isBlank(c.accountID))
                {
                    for(Asset a : [Select id from Asset where (Parent_AccountID__c  = :string.valueOf(c.account.parentID).substring(0,15) OR accountID = :c.accountid)])
                    {                
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                }
                if(!string.isBlank(c.account.Floor_Plan_Account_ID__c))
                {
                    for(Asset a : [Select id from Asset where accountID =:string.valueOf(c.account.Floor_Plan_Account_ID__c).substring(0,15)])
                    {                    
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                }
                if(!string.isBlank(string.valueOf(c.account.parentID).substring(0,15)))
                {
                    for(Asset a : [Select id from Asset where Current_Servicer_Parent_ID__c = :string.valueOf(c.account.parentID).substring(0,15)])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);                   
                    }
                }
            }
            
            //primary
            for(User u : [Select id from user where contactID IN :cIDs and toLabel(community_user_type__c) = 'Single Account'])
            {
                for(Asset a : [Select id from Asset where accountID = :string.valueOf(c.accountID).substring(0,15)])
                {                   
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
                if(!string.isBlank(c.account.Floor_Plan_Account_ID__c)){
                    for(Asset a : [Select id from Asset where Floor_Plan_AccountID__c = :c.account.Floor_Plan_Account_ID__c ])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                }
                
                for(Asset a : [Select id from Asset where Current_Servicer_ID__c = :string.valueOf(c.accountid).substring(0,15)])
                {                   
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
        }
        
        //insert assetShareToCreate;
        Database.saveResult [] cr = Database.insert(assetShareToCreate, false);
    }
    
    global void finish(Database.BatchableContext BC)
    {
        system.debug('Finished creating rights!!!');
    }
}