@isTest
public class SFS_callQueueableInvoiceBillable_Test {
    
    @isTest
    public static void SFS_callQueueableInvoiceBillable(){
         List<Account> accList = SFS_TestDataFactory.createAccounts(2, false);
        accList[1].AccountSource = 'Web';
        accList[1].IRIT_Customer_Number__c='1234';
        insert accList[1];
        
        //get Asset
        List<Asset> ast = SFS_TestDataFactory.createAssets(1, true);
        
        //get PriceBook
        Pricebook2 standardPricebook = new Pricebook2(Name = 'Standard Price Book',IsActive = true);
        insert standardPricebook;
        
        //get PriceBookEntry
        List<PricebookEntry> Pe = SFS_TestDataFactory.createPricebookEntry(1,true);
        
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false); 
        
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, false);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        
        //get ServiceContractLineItem
        List<ContractLineItem> scli = SFS_TestDataFactory.createServiceAgreementLineItem(1, sc[0].Id ,false);
        
        //get Entitlement
        List<Entitlement> ent = SFS_TestDataFactory.createEntitlement(1,accList[0].Id,ast[0].Id,sc[0].Id,scli[0].Id,false);
        
        //get Invoice
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(1, false);
        inv[0].SFS_Service_Agreement__c = sc[0].Id;
        inv[0].SFS_Status__c = 'Submitted';
        inv[0].SFS_Billing_Period_Start_Date__c = System.today();
        inv[0].SFS_Billing_Period_End_Date__c = System.today().addYears(2);
        inv[0].SFS_Credit_Card_Authorization_Status__c='Accept';
        insert inv[0];
        
        List<Id> invId = new List<Id>();
        invId.add(inv[0].Id);
        
         Test.startTest();
        //SFS_callQueueableInvoiceBillable invQue = new SFS_callQueueableInvoiceBillable(invId);
        //system.enqueueJob(invQue,1);
        SFS_callQueueableInvoiceBillable.SFS_callQueueableInvoiceBillable(invId);
        Test.stopTest();
        
    }     

}