/**
 * Created by tejaswini on 26/03/2024.
 */

public with sharing class B2BCartController {

    private static final String IRB2BWEBSTOREURLPATHPREFIX = 'IRB2BWebstoreUrlPathPrefix';
    private static final String permissionSetName = 'B2B_Buyer_Additional_Access';

    @AuraEnabled
    public static ConnectApi.CartItemCollection getCartItems(String userId, String effectiveAccountId) {
        B2B_IR_Store_Setting__mdt customPermissionIR = B2B_IR_Store_Setting__mdt.getInstance(IRB2BWEBSTOREURLPATHPREFIX);
        String webStoreId = B2BCommerceUtils.getWebStoreIdByUrlPathPrefix(customPermissionIR.value__c);
        ConnectApi.CartItemCollection  cartItemCollection = null;

        if(!Test.isRunningTest()) {
            String activeCartId = B2BCommerceUtils.getCartId(effectiveAccountId, userId);
            cartItemCollection = activeCartId != '' ? ConnectApi.CommerceCart.getCartItems(webStoreId, effectiveAccountId, activeCartId) : null;
        }
        return cartItemCollection;
    }

    @AuraEnabled
    public static Boolean checkBuyerPermission(String userId, String frameTypeId) {
        List<PermissionSetAssignment> psas = [SELECT Id FROM PermissionSetAssignment
                                                WHERE PermissionSet.Name = :permissionSetName AND AssigneeId = :userId];
        if (String.isEmpty(frameTypeId)) {
            return psas.size() > 0;
        }
        List<RecommendedProducts.CartItemWrapper> cartItemWrappers = new List<RecommendedProducts.CartItemWrapper>();
        if (psas.size() > 0) {
            cartItemWrappers = RecommendedProducts.getCSRecommendationProducts(frameTypeId);
        }
        return cartItemWrappers.size() > 0;
    }

}