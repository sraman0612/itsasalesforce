/* utility class for Sales Area information */
// TODO: I think this class shouldn't exist. These methods could go in ensxtx_UTIL_Order, right?
public class ensxtx_UTIL_ShippingInfo
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_UTIL_ShippingInfo.class);

    // getShippingMaster()
    //
    // Get the Shipping master from the RFC
    public static ensxtx_RFC_SD_GET_SHIP_INFO.RESULT getShippingMaster()
    {
        ensxtx_RFC_SD_GET_SHIP_INFO rfc = new ensxtx_RFC_SD_GET_SHIP_INFO();
        ensxtx_RFC_SD_GET_SHIP_INFO.RESULT result = rfc.execute();

        if (!result.isSuccess())
        {
            ensxtx_UTIL_ViewHelper.displayResultMessages(result.getMessages(), ensxsdk.EnosixFramework.MessageType.INFO);
        }

        return result;
    }

    // filterShippingConditions()
    //
    // Filter the shipping conditions from the allowedShippingConditions
    public static List<ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND> filterShippingConditions(
        ensxtx_RFC_SD_GET_SHIP_INFO.RESULT shippingConditionMaster)
    {
        List<ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND> filteredList = new List<ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND>();

        if (null != shippingConditionMaster)
        {
            List<ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND> shipList = shippingConditionMaster.ET_SHIP_COND_List;
            Integer shipTot = shipList.size();
            for (Integer shipCnt = 0 ; shipCnt < shipTot ; shipCnt++)
            {
                ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND shippingCondition = shipList[shipCnt];
                if (allowedShippingConditions.contains(shippingCondition.ShippingConditions)
                    || allowedShippingConditions.contains('*'))
                {
                    filteredList.add(shippingCondition);
                }
            }
        }

        return filteredList;
    }

    @testVisible
    private static Set<String> allowedShippingConditions
    {
        get
        {
            return (Set<String>)ensxtx_UTIL_AppSettings.getSet(
                ensxtx_UTIL_AppSettings.EnterpriseApp, 'Order.AllowedShippingConditions', String.class, new Set<String>{'*'});
        }
    }

    public class ShippingConditionOptionBuilder extends ensxtx_UTIL_SelectOption.OptionBuilder
    {
        public override string getItemValue(object item)
        {
            return ((ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND)item).ShippingConditions;
        }

        public override string getItemLabel(object item)
        {
            return string.format('{0} - {1}', new string[]{getItemValue(item), getItemDescription(item)});
        }

        public override string getItemDescription(object item)
        {
            return ((ensxtx_RFC_SD_GET_SHIP_INFO.ET_SHIP_COND)item).VTEXT;
        }
    }
}