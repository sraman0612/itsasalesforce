public with sharing class DistiAsset {
    public static void createChild(Map<Id, Asset> newTriggerAssets, Map<Id, Asset> oldTriggerAssets) { 

        List<Child_Asset__c> existingChildren = [SELECT Id, Parent_Asset__c, Current_Servicer__c, Current_Servicer__r.Parent_Account2__c FROM Child_Asset__c WHERE Parent_Asset__c IN :newTriggerAssets.values()];

        Map<String, List<Child_Asset__c>> existingChildrenMap = new Map<String, List<Child_Asset__c>>();

        for(Child_Asset__c childAsset : existingChildren) {
            String mapKey = childAsset.Current_Servicer__r.Parent_Account2__c + '' + childAsset.Parent_Asset__c;

            if(existingChildrenMap.containsKey(mapKey)) {
                existingChildrenMap.get(mapKey).add(childAsset);
            }
            else {
                existingChildrenMap.put(mapKey, new List<Child_Asset__c>{childAsset});
            }
        }

        List<Child_Asset__c> childrenToUpsert = new List<Child_Asset__c>();
        System.debug('inside disti'); 

        for(Asset triggerAsset : newTriggerAssets.values()) {
            if(trigger.isUpdate && String.isNotBlank(triggerAsset.Current_Servicer__c)) {
                if(oldTriggerAssets.get(triggerAsset.Id).Current_Servicer__c != triggerAsset.Current_Servicer__c) {
                    String mapKey = triggerAsset.Current_Servicer_Parent_ID__c + '' + triggerAsset.Id;

                    if(existingChildrenMap.containsKey(mapKey)) {
                        System.debug('inside existing check');
                        for(Child_Asset__c existingChild : existingChildrenMap.get(mapKey)) {
                            existingChild.Current_Servicer__c = triggerAsset.Current_Servicer__c;
                            childrenToUpsert.add(existingChild);
                        }
                    }
                    else {
                        Child_Asset__c childAsset = new Child_Asset__c(Parent_Asset__c = triggerAsset.Id, Current_Servicer__c = triggerAsset.Current_Servicer__c);
                        childrenToUpsert.add(childAsset);
                    }
                }
            }
            else {
                if(String.isNotBlank(triggerAsset.Current_Servicer__c)) {
                    String mapKey = triggerAsset.Current_Servicer_Parent_ID__c + '' + triggerAsset.Id;

                    if(existingChildrenMap.containsKey(mapKey)) {
                        System.debug('inside existing check');
                        for(Child_Asset__c existingChild : existingChildrenMap.get(mapKey)) {
                            existingChild.Current_Servicer__c = triggerAsset.Current_Servicer__c;
                            childrenToUpsert.add(existingChild);
                        }
                    }
                    else {
                        Child_Asset__c childAsset = new Child_Asset__c(Parent_Asset__c = triggerAsset.Id, Current_Servicer__c = triggerAsset.Current_Servicer__c);
                        childrenToUpsert.add(childAsset);
                    }
                }
            }
        }

        for(Child_Asset__c childAsset : childrenToUpsert) {
            childAsset.Customer_Street_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Customer_Street_Community__c;
            childAsset.Customer_City_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Customer_City_Community__c;
            childAsset.Owner_Contact_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Owner_Contact_Community__c;
            childAsset.Customer_Country_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Customer_Country_Community__c;
            childAsset.Customer_County_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Customer_County_Community__c;
            childAsset.Owner_Email_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Owner_Email_Community__c;
            childAsset.Owner_Name_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Owner_Name_Community__c;
            childAsset.Owner_Phone_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Owner_Phone_Community__c;
            childAsset.Customer_Postal_Code_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Customer_Postal_Code_Community__c;
            childAsset.Customer_State_Community__c = newTriggerAssets.get(childAsset.Parent_Asset__c).Customer_State_Community__c;
        }

        System.debug('childrenToUpsert' + childrenToUpsert);

        upsert childrenToUpsert;
    }
}