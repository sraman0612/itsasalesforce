/*=========================================================================================================
* @author : Ryan Reddish, Capgemini
* @date : 01/03/2022
* @description: XXPA2589 Activity Cost with lhenses
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
============================================================================================================*/


public class SFS_LaborHourOutboundHandler {
    @invocableMethod(label = 'Outbound Labor Hour' description = 'Send to CPI' Category = 'Labor Hour')
    public static void Run(List<Id> woliId){
        //Main Method
        //
        String xmlString = '';
        Map<Double,String> laborHourXmlMap = new Map<Double,String>();
        String [] aliasStatus = new String[]{'N/A','Error'};
            List<CAP_IR_Labor__c> lh = new List<CAP_IR_Labor__c>();
            Try{
              lh= [SELECT SFS_Project_Number__c,CAP_IR_Status__c,Elapsed_Time__c, SFS_External_Id__c, CAP_IR_Work_Order__c,CAP_IR_Quantity__c, Name,sfs_oracle_end_of_week_date__c,  CAP_IR_Description__c,  OwnerId, SFS_Reverse_Transaction_Reference__c, SFS_Transaction_Type__c, SFS_Negative_Transaction_Flag__c, SFS_Oracle_Expenditure_Type__c, SFS_Oracle_Action_Type__c, SFS_Oracle_Division_Name__c,sfs_oracle_start_date__c, sfs_batch_name__c,sfs_system_linkage__c FROM CAP_IR_Labor__c WHERE CAP_IR_Work_Order_Line_Item__c =: woliId AND SFS_Integration_Status__c !='SYNCING'];

            }catch(System.Exception e){
                System.debug('EXCEPTION: ' + e);
            }
		System.debug(lh);
        System.debug('RUNNNN');
          
        ProcessData(lh, laborHourXmlMap,xmlString); 
       
    }
     public static void ProcessData(List<CAP_IR_Labor__c> lh, Map<Double, String> laborHourXmlMap, String xmlString){
        List<String>finalXML = new List<String>();
         SFS_Activity_Cost_Labor_Hour_XML_Struct__mdt[] labrHourMdt = [SELECT label, SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c 
                                                        FROM SFS_Activity_Cost_Labor_Hour_XML_Struct__mdt Order By SFS_Node_Order__c asc];
         String headerFields = '';
         String footerFields = '';
         for(SFS_Activity_Cost_Labor_Hour_XML_Struct__mdt x : labrHourMdt){
             if(x.label == 'Header'){
               headerFields = x.SFS_XML_Full_Name__c;
                 
             }else if(x.label == 'Footer'){
                 footerFields = x.SFS_XMl_Full_Name__c;
             }
             
         }
      	
         System.debug('Last Line Execute' + lh);
         For(Cap_Ir_Labor__c l : lh){
        
             Map<String, Object> laborHourFieldMap = new Map<String, Object>();
             laborHourFieldMap = l.getPopulatedFieldsAsMap();
             System.debug(laborHourFieldMap);
             User us = new User();
             Map<String, Object> userName = new Map<String, Object>();
             try{
                  us = [SELECT Id, federationIdentifier FROM User WHERE ID =: l.OwnerId];
                  userName = us.getPopulatedFieldsAsMap();
              }catch(System.Exception e){
             System.debug('EXCEPTION: ' + e);
         }
        
         
         WorkOrder wo = [SELECT WorkOrderNumber, SFS_Division__c,SFS_External_Id__c FROM WorkOrder WHERE Id =: l.CAP_IR_Work_Order__c];
         Map<String, Object> workOrderFieldMap = new Map<String, Object>();
         workOrderFieldMap = wo.getPopulatedFieldsAsMap();
         
         Division__c div = [SELECT Organization_Name__c, SFS_Org_Code__c, SFS_Oracle_Division_Name__c, SFS_Employee_Number__c FROM Division__c WHERE Id =: wo.SFS_Division__c];
         Map<String, Object> divFieldMap = new Map<String, Object>();
         divFieldMap = div.getPopulatedFieldsAsMap();
        
        Map<String, Object> FinalMap = new Map<String, Object>();
         for(String e : laborHourFieldMap.keyset()){
             try{
                 FinalMap.put(e.toLowerCase(), l.get(e));
             }Catch(System.Exception x){
                 
             }   
         }
         for(String e : WorkOrderFieldMap.keyset()){
             try{
                 if(e == 'SFS_External_Id__c'){
                     FinalMap.put('workordernumber', wo.get(e));
                     System.debug('TRUE WO');
                 }else{             
                     FinalMap.put(e.toLowerCase(), wo.get(e));
                 }
             }catch(System.Exception x){
                 
             }
         }
         for(String e : divFieldMap.keyset()){
             try{
                 FinalMap.put(e.toLowerCase(), div.get(e));
             }catch(System.Exception x){
                 
             }
         }
         for(String e : userName.keySet()){
             try{
                 FinalMap.put(e.toLowerCase(), us.get(e));
             }catch(System.Exception X){
                 
             }
         }         
             Boolean add = false;
             Boolean Fadd = False;
             Map<Double, String> convertedValues = new Map <Double, String>();
             List<String> usedFieldList = new List<String>();
             List<Double> newNodeList = new List<Double>();
             
             for(SFS_Activity_Cost_Labor_Hour_XML_Struct__mdt xmlStructure : labrHourMdt){
                 if(xmlStructure.Label != 'Header' && XmlStructure.Label != 'Footer'){
                     newNodeList.add(xmlStructure.SFS_Node_Order__c);
                     //check for hardcoded flag. If false, replace data.
                     if(xmlStructure.SFS_Hardcoded_Flag__c == false){
                         System.debug('ENTRY');
                         String sfField = xmlStructure.SFS_Salesforce_Field__c;
                         if(FinalMap.containsKey(sfField)){
                             double nodeOrder = xmlStructure.SFS_Node_Order__c;
                             //check if value from query is null
                             if(FinalMap.get(sfField) != null){
                                 String xmlFullName = xmlStructure.SFS_XML_Full_Name__c;
                                 String replacement = String.ValueOf(FinalMap.get(sfField));
                                 replacement = replacement.escapeXml();
                                 String newLine = xmlFullName.replace(sfField, replacement);
                                 FinalXML.Add(newLine);
                             }
                             else if(FinalMap.get(sfField) == null){
                                 finalXML.Add(xmlStructure.SFS_XML_Full_Name__c);
                             }
                         }
                         //if SCFieldMap does not contain sffield, the field value is null. Replace null value with just tags.
                         else if(!FinalMap.ContainsKey(sfField)){
                             String empty = '';
                             String replacement = xmlStructure.SFS_XML_Full_Name__c.replace(sfField, empty);
                             FinalXML.Add(replacement);
                         }
                     }
                     else if(xmlStructure.SFS_Hardcoded_Flag__c == true){
                             FinalXML.Add(xmlStructure.SFS_XML_Full_Name__c);
                      }
                     
                 }
             }
         }  
             for(String s : finalXML){
                 xmlString = xmlString + s;
                 System.debug(XMLString);
             }
         
         xmlstring = xmlString + footerFields;
         xmlString = headerFields + xmlString;
         //Use the new node list to build xml string.
        //newNodeList.sort();
        
       /* for(String s : finalXML)
        for(Double n : newNodeList){
            finalXML.add(convertedValues.get(n));
        }*/
         
         String InterfaceDetail = 'XXPA2589';
         String InterfaceLabel= 'Activity Cost Labor Hour';
         For(Cap_IR_Labor__c l : lh){
             InterfaceLabel = interfaceLabel + '!' + String.ValueOf(l.Id);
         }
         
         SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, interfaceLabel);
    }
    
    public static void IntegrationRespone(HttpResponse res, String InterfaceLabel){
        List<String> idList = interfaceLabel.Split('!');
        
        List<CAP_IR_Labor__c> laborHour = [SELECT SFS_Integration_Response__c, SFS_Integration_Status__c FROm CAP_IR_Labor__c WHERE Id =: idList];
        
        if(res.getStatusCode() != 200){
            for(CAP_IR_Labor__c l : laborHour){
                l.SFS_Integration_Status__c = 'ERROR';
                l.SFS_Transit_Status__c = 'Rejected';
                l.SFS_Integration_Response__c = res.getBody();
            }
        }
        else{
            for(CAP_IR_Labor__c l : laborHour){
                l.SFS_Integration_Status__c = 'SYNCING';
            }
        }
        update laborHour;
    }
    
}