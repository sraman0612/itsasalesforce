/// enosiX Inc. Generated Apex Model
/// Generated On: 10/9/2019 10:07:34 AM
/// SAP Host: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDan.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

@isTest
private class TST_EnosixZAPR_Search
{

    public class MockSBO_EnosixZAPR_Search implements ensxsdk.EnosixFramework.SearchSBOInitMock, ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public ensxsdk.EnosixFramework.SearchContext executeInitialize(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            return null;
        }
    }

    @isTest
    static void testSBO()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSBO_EnosixZAPR_Search());
        SBO_EnosixZAPR_Search sbo = new SBO_EnosixZAPR_Search();
        System.assertEquals(SBO_EnosixZAPR_Search.class, sbo.getType(), 'getType() does not match object type.');

        System.assertEquals(null, sbo.initialize(null));
        System.assertEquals(null, sbo.search(null));

        SBO_EnosixZAPR_Search.EnosixZAPR_SC sc = new SBO_EnosixZAPR_Search.EnosixZAPR_SC();
        System.assertEquals(SBO_EnosixZAPR_Search.EnosixZAPR_SC.class, sc.getType(), 'getType() does not match object type.');

        sc.registerReflectionForClass();
        System.assertEquals(null, sc.result);

        System.assertNotEquals(null, sc.SEARCHPARAMS);

    }
    
    
    @isTest
    static void testSEARCHPARAMS()
    {
        SBO_EnosixZAPR_Search.SEARCHPARAMS childObj = new SBO_EnosixZAPR_Search.SEARCHPARAMS();
        System.assertEquals(SBO_EnosixZAPR_Search.SEARCHPARAMS.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);

        childObj.Country = 'X';
        System.assertEquals('X', childObj.Country);

        childObj.Region = 'X';
        System.assertEquals('X', childObj.Region);

        childObj.County = 'X';
        System.assertEquals('X', childObj.County);

        childObj.AppSegCode = 'X';
        System.assertEquals('X', childObj.AppSegCode);


    }

    @isTest
    static void testEnosixZAPR_SR()
    {
        SBO_EnosixZAPR_Search.EnosixZAPR_SR sr = new SBO_EnosixZAPR_Search.EnosixZAPR_SR();

        sr.registerReflectionForClass();

        System.assertEquals(SBO_EnosixZAPR_Search.EnosixZAPR_SR.class, sr.getType(), 'getType() does not match object type.');

        System.assertNotEquals(null, sr.getResults());
    }


    @isTest
    static void testSEARCHRESULT()
    {
        SBO_EnosixZAPR_Search.SEARCHRESULT childObj = new SBO_EnosixZAPR_Search.SEARCHRESULT();
        System.assertEquals(SBO_EnosixZAPR_Search.SEARCHRESULT.class, childObj.getType(),'getType() does not match object type.');

        childObj.registerReflectionForClass();
        SBO_EnosixZAPR_Search.SEARCHRESULT_COLLECTION childObjCollection = new SBO_EnosixZAPR_Search.SEARCHRESULT_COLLECTION();
        System.assertNotEquals(null, childObjCollection.getAsList());

        childObj.SalesOrganization = 'X';
        System.assertEquals('X', childObj.SalesOrganization);

        childObj.DistributionChannel = 'X';
        System.assertEquals('X', childObj.DistributionChannel);

        childObj.Country = 'X';
        System.assertEquals('X', childObj.Country);

        childObj.Region = 'X';
        System.assertEquals('X', childObj.Region);

        childObj.County = 'X';
        System.assertEquals('X', childObj.County);

        childObj.AppSegCode = 'X';
        System.assertEquals('X', childObj.AppSegCode);

        childObj.AppSegCodeDesc = 'X';
        System.assertEquals('X', childObj.AppSegCodeDesc);

        childObj.Sequence = 'X';
        System.assertEquals('X', childObj.Sequence);

        childObj.CustomerNumber = 'X';
        System.assertEquals('X', childObj.CustomerNumber);

        childObj.SalesDistrict = 'X';
        System.assertEquals('X', childObj.SalesDistrict);

        childObj.SalesDistrictName = 'X';
        System.assertEquals('X', childObj.SalesDistrictName);


    }

}