public with sharing class UTIL_EnosixOutput_Search {
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_EnosixOutput_Search.class);

    @AuraEnabled
    public static SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC getBOLSearchContext(String SalesDocument)
    {
        SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC searchContext =
                new SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC();

        if (String.isNotBlank(SalesDocument)) {
            searchContext.SEARCHPARAMS.SalesDocument = SalesDocument;
        }

        searchContext.SEARCHPARAMS.OutputApplication = 'V7';

        SBO_EnosixSalesDocOutput_Search.OUTPUT_TYPE outputType = new SBO_EnosixSalesDocOutput_Search.OUTPUT_TYPE();
        outputType.ConditionType = 'ZBOL';
        outputType.X_GetPDF = true;

        searchContext.OUTPUT_TYPE.add(outputType);

        return searchContext;
    }

    @AuraEnabled
    public static SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC getInvoiceSearchContext(String SalesDocument)
    {
        SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC searchContext =
                new SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC();

        if (String.isNotBlank(SalesDocument)) {
            searchContext.SEARCHPARAMS.SalesDocument = SalesDocument;
        }

        searchContext.SEARCHPARAMS.OutputApplication = 'V3';

        SBO_EnosixSalesDocOutput_Search.OUTPUT_TYPE outputType = new SBO_EnosixSalesDocOutput_Search.OUTPUT_TYPE();
        outputType.ConditionType = 'ZINV';
        outputType.X_GetPDF = true;

        searchContext.OUTPUT_TYPE.add(outputType);

        return searchContext;
    }
}