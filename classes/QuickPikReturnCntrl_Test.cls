@isTest
public with sharing class QuickPikReturnCntrl_Test {
    @testSetup
    static void createData() {

        Id profileId = [
            SELECT Id
            FROM Profile
            WHERE Name = 'Partner Community - Global Account User'
        ].Id;
        
         Account a = new Account(
            Name = 'testname1',
            Account_Number__c='12344'
        );
        insert a;
        
        Account ac = new Account(
            Name = 'testname',
            parentID=a.id,
           Account_Number__c='12345'
        );
        insert ac;

        Contact con = new Contact(
            LastName = 'testCon',
            AccountId = ac.Id
        );
        insert con;
            
        User user = new User(
            Alias = 'test123',
            Email = 'test123@noemail.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Testing',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = profileId,
            Country = 'United States',
            IsActive = true,
            ContactId = con.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            Username = 'tester@nokhjyrljhfdrsr3email.com'
        );
         User usr = [Select id from User where Id = :UserInfo.getUserId()];
     System.RunAs(usr)

     {
        insert user;
     

        con.LastName = user.Id;
        update con;

        insert new Product2(
            IsActive = true,
            Name = 'TestProduct',
            FLD_Distribution_Channel__c = 'CM'
        );

        insert new Pricebook2(
            Name = 'Standard Price Book',
            IsActive = true
        );
         }
    }

    @isTest
    static void doRedirect() {

        PageReference pr = Page.QuickPikReturn;
        pr.getParameters().put('Prod', [SELECT Id FROM Product2 WHERE FLD_Distribution_Channel__c != NULL LIMIT 1][0].Id);

        User usr = [
            SELECT  Id
            FROM    User
            WHERE   Contact.AccountId != NULL
                AND IsActive = TRUE
            LIMIT   1
        ];

        System.runAs(usr) {
            System.Test.setCurrentPage(pr);
            QuickPikReturnCntrl controller = new QuickPikReturnCntrl();
            controller.doRedirect();
        }
    }
}