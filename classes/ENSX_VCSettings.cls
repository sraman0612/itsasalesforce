public with sharing class ENSX_VCSettings
{
    // @AuraEnabled
    // public Boolean AutoSelectWhenSingleValue { get; set; }
   
    // @AuraEnabled
    // public Boolean AutoSelectFirstValue { get; set; }
   
    @AuraEnabled
    public Boolean DisplayCost { get; set; }

    @AuraEnabled
    public Boolean DisplayPrice { get; set; }
     
    @AuraEnabled
    public String FetchConfigurationFrequency { get; set; }

    @AuraEnabled
    public List<ENSX_VCSettingsSelection> FetchConfigurationFrequencyPossibilities{ get; set;}
}