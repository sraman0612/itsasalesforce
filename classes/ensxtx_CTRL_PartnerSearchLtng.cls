public class ensxtx_CTRL_PartnerSearchLtng
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_CTRL_PartnerSearchLtng.class);

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response searchPartners(
        String customerNumber, String partnerFunction, String salesOrg, String distChannel, String division,
        String incoterm, List<String> filterPartnerNumbers, Map<String, Object> pagingOptions)
    {
        logger.enterAura('searchPartners', new Map<String, Object>{
            'customerNumber' => customerNumber,
            'partnerFunction' => partnerFunction,
            'salesOrg' => salesOrg,
            'distChannel' => distChannel,
            'division' => division,
            'incoterm' => incoterm,
            'filterPartnerNumbers' => filterPartnerNumbers,
            'paginOptions' => pagingOptions
        });

        ensxtx_SBO_SFCIPartner_Search sbo = new ensxtx_SBO_SFCIPartner_Search();
        String results = '';
        Object responsePagingOptions = null; 

        try
        {
            ensxtx_SBO_SFCIPartner_Search.SFCIPartner_SC searchContext = new ensxtx_SBO_SFCIPartner_Search.SFCIPartner_SC();
            Boolean isCarrier = partnerFunction == 'CR';
            searchContext.SEARCHPARAMS.CustomerNumber = isCarrier ? 'CARRIER' : customerNumber;
            searchContext.SEARCHPARAMS.PartnerFunction = partnerFunction;
            searchContext.SEARCHPARAMS.SalesOrganization = salesOrg;
            searchContext.SEARCHPARAMS.DistributionChannel = distChannel;
            searchContext.SEARCHPARAMS.Division = division;
            if (isCarrier && incoterm == 'ADD' ) {
                pagingOptions = new Map<String, Object>{
                    'pageSize' => 200,
                    'pageNumber' => 1
                };
            }
            ensxtx_UTIL_Aura.setSearchContextPagingOptions(searchContext, pagingOptions);

            sbo.search(searchContext);
            List<ensxtx_SBO_SFCIPartner_Search.SEARCHRESULT> searchResults = searchContext.result.getResults();

            if (incoterm == 'ADD' && isCarrier && filterPartnerNumbers != null && filterPartnerNumbers.size() > 0) {
                searchResults = filterPartners(new Set<String>(filterPartnerNumbers), searchResults);
            }

            if (searchResults.size() > 0) results = JSON.serialize(searchResults);
            responsePagingOptions = searchContext.pagingOptions;
        }
        catch (Exception ex)
        {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        }

        return ensxtx_UTIL_Aura.createResponse(results, responsePagingOptions);
    }

    private static List<ensxtx_SBO_SFCIPartner_Search.SEARCHRESULT> filterPartners(Set<String> filterPartnerNumbers, List<ensxtx_SBO_SFCIPartner_Search.SEARCHRESULT> searchResults)
    {
        System.debug('filter: ' + filterPartnerNumbers);
        List<ensxtx_SBO_SFCIPartner_Search.SEARCHRESULT> newSearchResults = new List<ensxtx_SBO_SFCIPartner_Search.SEARCHRESULT>();

        for (ensxtx_SBO_SFCIPartner_Search.SEARCHRESULT result : searchResults)
        {
            if (filterPartnerNumbers.contains(result.VendorNumber)) {
                newSearchResults.add(result);
            }
        }

        return newSearchResults;
    }
}