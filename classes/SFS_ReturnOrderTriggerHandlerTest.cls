/*=========================================================================================================
* @author : Geethanjali SP
* @description: Test Class for SFS_ReturnOrderTriggerHandler
*/
@isTest
public class SFS_ReturnOrderTriggerHandlerTest {   
    Static testmethod void testCreateCase(){
        
        List<Division__C> divList=SFS_TestDataFactory.createDivisions(1, false);
       	insert divList[0];
        
        List<Schema.Location> locationList=SFS_TestDataFactory.createLocations(1,divList[0].Id, false);
        insert locationList[0];
        
        List<Account> accountList=SFS_TestDataFactory.createAccounts(1, false);
        insert accountList[0];    
         
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,accountList[0].Id,locationList[0].Id,null ,null,false);
        insert woList[0];
        
        List<ReturnOrder> returnOrderList=SFS_TestDataFactory.createReturnOrders(1,woList[0].Id,false);
        insert returnOrderList[0];
        
        List<Id> roIds=new List<Id>();
        roIds.add(returnOrderList[0].Id);
        
        Test.startTest();
		SFS_ReturnOrderTriggerHandler.createCase(roIds);
        Test.stopTest();
    }
}