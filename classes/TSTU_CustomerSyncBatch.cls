@isTest
public with sharing class TSTU_CustomerSyncBatch
{
    static final string TEST_JSON = '{"UTIL_CustomerSyncBatch.SalesOrgs":["GDMI"], "UTIL_CustomerSyncBatch.DistributionChannels":["CM","DT","GT","SB"], "UTIL_CustomerSyncBatch.AccountGroups":["0001","0002"], "UTIL_CustomerSyncBatch.Logging": true}';

    public class MockSyncSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        private boolean success = true;

        public void setSuccess(boolean success)
        {
            this.success = success;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext)
        {
            if (this.throwException)
            {
                throw new UTIL_SyncHelper.SyncException('');
            }

            SBO_EnosixCustSync_Search.EnosixCustSync_SR searchResult =
                new SBO_EnosixCustSync_Search.EnosixCustSync_SR();

            SBO_EnosixCustSync_Search.SEARCHRESULT result1 =
                new SBO_EnosixCustSync_Search.SEARCHRESULT();

            result1.CustomerNumber = '1';
            result1.CustomerAccountGroup = '0001';
            result1.DistributionChannel = 'CM';
            result1.Name1 = 'Name';
            result1.Name2 = 'Name2';
            result1.Street = 'Street';
            result1.Street2 = 'Street2';
            result1.Street3 = 'Street3';
            result1.Street4 = 'Street4';
            result1.Street5 = 'AssetLocation';
            result1.HouseNumber = '111';
            result1.CityPostalCode = '22244';
            result1.City = 'City';
            result1.CountryKey = 'Country';
            result1.Region = 'Region';
            result1.POBox = 'POBox';
            result1.POBoxPostalCode = 'POBoxPostalCode';
            result1.TelephoneNo = 'TelephoneNumber';
            result1.EMailAddress = 'valid@valid.com';
            result1.SalesOrganization = 'GDMI';

            searchResult.SearchResults.add(result1);


            SBO_EnosixCustSync_Search.SEARCHRESULT result11 =
                new SBO_EnosixCustSync_Search.SEARCHRESULT();

            result11.CustomerNumber = '11';
            result11.DistributionChannel = 'SB';
            result11.Name1 = 'Name';
            result11.Name2 = 'Name2';
            result11.Street = 'Street';
            result11.Street2 = 'Street2';
            result11.Street3 = 'Street3';
            result11.Street4 = 'Street4';
            result11.Street5 = 'AssetLocation';
            result11.HouseNumber = '1111';
            result11.CityPostalCode = '22244';
            result11.City = 'City';
            result11.CountryKey = 'Country';
            result11.Region = 'Region';
            result11.POBox = 'POBox';
            result11.POBoxPostalCode = 'POBoxPostalCode';
            result11.TelephoneNo = 'TelephoneNumber';
            result11.EMailAddress = 'valid11@valid.com';
            result11.SalesOrganization = 'GDMI';

            searchResult.SearchResults.add(result11);


            // Will be created as a new Account
            SBO_EnosixCustSync_Search.SEARCHRESULT result12 =
                new SBO_EnosixCustSync_Search.SEARCHRESULT();

            result12.CustomerNumber = '12';
            result12.CustomerAccountGroup = '0002';
            result12.CustomerGroup = 'U1';
            result12.CustomerGroupText = 'END USER 1';
            result12.Name1 = 'Name';
            result12.Name2 = 'Name2';
            result12.Street = 'Street';
            result12.Street2 = 'Street2';
            result12.Street3 = 'Street3';
            result12.Street4 = 'Street4';
            result12.Street5 = 'AssetLocation';
            result12.HouseNumber = '1112';
            result12.CityPostalCode = '22244';
            result12.City = 'City';
            result12.CountryKey = 'Country';
            result12.Region = 'Region';
            result12.POBox = 'POBox';
            result12.POBoxPostalCode = 'POBoxPostalCode';
            result12.TelephoneNo = 'TelephoneNumber';
            result12.EMailAddress = 'valid12@valid.com';

            searchResult.SearchResults.add(result12);


            SBO_EnosixCustSync_Search.SEARCHRESULT result2 =
                new SBO_EnosixCustSync_Search.SEARCHRESULT();

            result2.CustomerNumber = '2';
            result2.CustomerAccountGroup = '0002';
            result2.DistributionChannel = 'DT';
            result2.Name1 = 'Name';
            result2.Name2 = 'Name2';
            result2.Street = 'Street';
            result2.Street2 = 'Street2';
            result2.Street3 = 'Street3';
            result2.Street4 = 'Street4';
            result2.Street5 = 'AssetLocation';
            result2.HouseNumber = '222';
            result2.CityPostalCode = '33355';
            result2.City = 'City';
            result2.CountryKey = 'Country';
            result2.Region = 'Region';
            result2.POBox = 'POBox';
            result2.POBoxPostalCode = 'POBoxPostalCode';
            result2.TelephoneNo = 'TelephoneNumber';
            result2.EMailAddress = 'valid2@valid.com';
            result2.SalesOrganization = 'GDMI';

            searchResult.SearchResults.add(result2);


            SBO_EnosixCustSync_Search.SEARCHRESULT result22 =
                new SBO_EnosixCustSync_Search.SEARCHRESULT();

            result22.CustomerNumber = '22';
            result22.CustomerAccountGroup = '0003';
            result22.Name1 = 'Name';
            result22.Name2 = 'Name2';
            result22.Street = 'Street';
            result22.Street2 = 'Street2';
            result22.Street3 = 'Street3';
            result22.Street4 = 'Street4';
            result22.Street5 = 'AssetLocation';
            result22.HouseNumber = '2222';
            result22.CityPostalCode = '33355';
            result22.City = 'City';
            result22.CountryKey = 'Country';
            result22.Region = 'Region';
            result22.POBox = 'POBox';
            result22.POBoxPostalCode = 'POBoxPostalCode';
            result22.TelephoneNo = 'TelephoneNumber';
            result22.EMailAddress = 'valid22@valid.com';
            result22.SalesOrganization = 'GDMI';

            searchResult.SearchResults.add(result22);


            SBO_EnosixCustSync_Search.SEARCHRESULT result3 =
                new SBO_EnosixCustSync_Search.SEARCHRESULT();

            result3.CustomerNumber = '3';
            result3.CustomerGroup = 'R2 - RESALER/DEALER 2';
            result3.CustomerAccountGroup = '0004';
            result3.DistributionChannel = 'GT';
            result3.Name1 = 'Name';
            result3.Name2 = 'Name2';
            result3.Street = 'Street';
            result3.Street2 = 'Street2';
            result3.Street3 = 'Street3';
            result3.Street4 = 'Street4';
            result3.Street5 = 'AssetLocation';
            result3.HouseNumber = '333';
            result3.CityPostalCode = '30043';
            result3.City = 'City';
            result3.CountryKey = 'Country';
            result3.Region = 'Region';
            result3.POBox = 'POBox';
            result3.POBoxPostalCode = 'POBoxPostalCode';
            result3.TelephoneNo = 'TelephoneNumber';
            result3.EMailAddress = 'InvalidEmail';
            result3.SalesOrganization = 'GDMI';
            result3.Partner = '111333';

            searchResult.SearchResults.add(result3);


            SBO_EnosixCustSync_Search.SEARCHRESULT result33 =
                new SBO_EnosixCustSync_Search.SEARCHRESULT();

            result33.CustomerNumber = '33';
            result33.CustomerAccountGroup = '0003';
            result33.DistributionChannel = 'CM';
            result33.Name1 = 'Name';
            result33.Name2 = 'Name2';
            result33.Street = 'Street';
            result33.Street2 = 'Street2';
            result33.Street3 = 'Street3';
            result33.Street4 = 'Street4';
            result33.Street5 = 'AssetLocation';
            result33.HouseNumber = '333';
            result33.CityPostalCode = '30043';
            result33.City = 'City';
            result33.CountryKey = 'Country';
            result33.Region = 'Region';
            result33.POBox = 'POBox';
            result33.POBoxPostalCode = 'POBoxPostalCode';
            result33.TelephoneNo = 'TelephoneNumber';
            result33.EMailAddress = 'valid33@valid.com';
            result33.CentralDeletionIndicator = 'X';
            result33.SalesOrganization = 'GDMI';

            searchResult.SearchResults.add(result33);


            SBO_EnosixCustSync_Search.SEARCHRESULT result4 =
                new SBO_EnosixCustSync_Search.SEARCHRESULT();

            result4.CustomerNumber = '4';

            searchResult.SearchResults.add(result4);

            searchResult.setSuccess(this.success);
            searchContext.baseResult = searchResult;
            return searchContext;
        }
    }

    public static testMethod void test_CustomerSync()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixCustSync_Search.class, new MockSyncSearch());

        UTIL_AppSettings.resourceJson = TEST_JSON;
        createExistingObject();
        Test.startTest();
        UTIL_CustomerSyncBatch controller = new UTIL_CustomerSyncBatch();
        UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
        lastSync.retryCnt = 1;
        controller.setBatchParam(lastSync);
        Database.executeBatch(controller);
        Test.stopTest();
    }

    public static testMethod void test_CustomerSyncFailure()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixCustSync_Search.class, mockSyncSearch);
        mockSyncSearch.setSuccess(false);

        createExistingObject();
        Test.startTest();
        UTIL_CustomerSyncBatch controller = new UTIL_CustomerSyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
        }
        catch (Exception e) {}
        Test.stopTest();
    }

    public static testMethod void test_CustomerSyncException()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixCustSync_Search.class, mockSyncSearch);
        mockSyncSearch.setThrowException(true);

        createExistingObject();
        Test.startTest();
        UTIL_CustomerSyncBatch controller = new UTIL_CustomerSyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
            controller.start(null);
        }
        catch (Exception e) {}
        Test.stopTest();
    }

    private static void createExistingObject()
    {
        Account ParentAccount = new Account();
        ParentAccount.Name = 'Parent Account';
        ParentAccount.Customer_group__c = '0001';
        ParentAccount.ENSX_EDM__SAP_Customer_Number__c = '55599';
        ParentAccount.Account_Number__c = '55599';
        ParentAccount.SOrg__c = 'GDMI';
        ParentAccount.ParentId = null;
        insert ParentAccount;

        Account RepAccount = new Account();
        RepAccount.Name = 'Rep Account';
        RepAccount.Customer_group__c = '0001';
        RepAccount.ENSX_EDM__SAP_Customer_Number__c = '99955';
        RepAccount.SOrg__c = 'GDMI';
        RepAccount.ParentId = null;
        insert RepAccount;

        Account currentObject = TSTU_SFAccount.createTestAccount();
        currentObject.Name = 'account1';
        currentObject.put(UTIL_CustomerSyncBatch.SFSyncKeyField,'1');
        insert currentObject;

        Account currentObject2 = TSTU_SFAccount.createTestAccount();
        currentObject2.Name = 'account2';
        currentObject2.put(UTIL_CustomerSyncBatch.SFSyncKeyField,'2');
        currentObject2.Rep_Account__c = RepAccount.ENSX_EDM__SAP_Customer_Number__c;
        currentObject2.Rep_Account2__c = RepAccount.Id;
        insert currentObject2;

        Account currentObject22 = TSTU_SFAccount.createTestAccount();
        currentObject22.Name = 'account22';
        currentObject22.put(UTIL_CustomerSyncBatch.SFSyncKeyField,'22');
        insert currentObject22;

        Account currentObject3 = TSTU_SFAccount.createTestAccount();
        currentObject3.Name = 'account3';
        currentObject3.put(UTIL_CustomerSyncBatch.SFSyncKeyField,'3');
        currentObject3.Rep_Account__c = RepAccount.ENSX_EDM__SAP_Customer_Number__c;
        insert currentObject3;

        OBJ_SAP_Sync__c sapSync = new OBJ_SAP_Sync__c();
        sapSync.Name = 'UTIL_CustomerSyncSchedule';
        sapSync.FLD_Sync_DateTime__c = System.today().addDays(-1);
        sapSync.FLD_Page_Number__c = 0;
        upsert sapSync;
    }
}