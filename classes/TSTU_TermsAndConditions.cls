@IsTest
public class TSTU_TermsAndConditions {

    @IsTest
    public static void test() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Account_Number__c='1234';
        insert acc;
        
        Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = new Opportunity();
        opp.Name = 'TEST OPP';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.Pricebook2Id = pricebookId;
        opp.AccountId = acc.Id;
        TSTU_SFOpportunity.upsertOpportunity(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        TSTU_SFOpportunity.upsertOpportunity(opp);

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__PriceBook__c = pricebookId;
        quote.SBQQ__Account__c = acc.Id;
        quote.SBQQ__Status__c = 'Draft';
        quote.My_Quote_Terms_and_Conditions__c = 'first';
        upsert quote;
        quote = [SELECT Id, SBQQ__PriceBook__c, SBQQ__Opportunity2__c, SBQQ__Account__c, My_Quote_Terms_and_Conditions__c FROM SBQQ__Quote__c WHERE Id = :quote.Id LIMIT 1];
        quote.SBQQ__PriceBook__c = pricebookId;
        upsert quote;

        Company_Brand__c brand = new Company_Brand__c();
        brand.AccountId__c = acc.Id;
        brand.Distributor_Terms_and_Conditions__c = 'second';
        
        Test.startTest();
        
        insert brand;
        
        quote = [SELECT Id, SBQQ__PriceBook__c, SBQQ__Opportunity2__c, SBQQ__Account__c, My_Quote_Terms_and_Conditions__c FROM SBQQ__Quote__c WHERE Id = :quote.Id LIMIT 1];
        
        //System.assert(quote.My_Quote_Terms_and_Conditions__c == 'second');

		brand.Distributor_Terms_and_Conditions__c = 'third';
        
        update brand;
        
        quote = [SELECT Id, SBQQ__PriceBook__c, SBQQ__Opportunity2__c, SBQQ__Account__c, My_Quote_Terms_and_Conditions__c FROM SBQQ__Quote__c WHERE Id = :quote.Id LIMIT 1];
        
        Test.stopTest();
        
        //System.assert(quote.My_Quote_Terms_and_Conditions__c == 'third');
    }
}