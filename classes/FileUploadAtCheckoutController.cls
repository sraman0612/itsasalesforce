public with sharing class FileUploadAtCheckoutController {

    @AuraEnabled
    public static Decimal getCartTotalAmount(Id cartId) {
        Decimal amount = 0;
        List<WebCart> carts = [SELECT Id, GrandTotalAmount FROM WebCart WHERE Id = :cartId ];
        if (carts.size() > 0) {
            amount = carts[0].GrandTotalAmount;
        }

        return amount;
    }

    @AuraEnabled
    public static void uploadFile(String base64, String filename, String recordId) {
        ContentVersion cv = createContentVersion(base64, filename);
        updateCart(recordId, cv.Id);
    }

    @TestVisible
    private static void updateCart(String recordId, String contentVersionId) {
        if (String.isNotEmpty(recordId)) {
            WebCart webCart = new WebCart();
            webCart.Id = recordId;
            webCart.Content_Document_Id__c = contentVersionId;

            update webCart;
        }
        else {
            System.debug('cart Id not provided');
        }
    }

    @TestVisible
    private static ContentVersion createContentVersion(String base64, String filename) {
        try {
            ContentVersion cv = new ContentVersion();
            cv.VersionData = EncodingUtil.base64Decode(base64);
            cv.Title = filename;
            cv.PathOnClient = filename;
            insert cv;
            return cv;
        } catch(DMLException e) {
            System.debug(e);
            return null;
        }
    }

    @Future
    public static void createContentLink(String contentVersionId, String recordId) {
        if (contentVersionId != null && recordId != null) {
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [ SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionId].ContentDocumentId;
            cdl.LinkedEntityId = recordId;
            // ShareType is either 'V', 'C', or 'I'(V = Viewer, C = Collaborator, I = Inferred)
            cdl.ShareType = 'V';
            try {
                insert cdl;
            } catch (DMLException e) {
                System.debug(e);
            }
        }
    }

}