public class CloneChargesUpdate{
    @invocableMethod(label = 'Update Charges' description = 'Apex Call from ChildCloneSA' Category = 'Charges')
    public static void updateCharges(List<List<CAP_IR_Charge__c>> chargesList){
        system.debug('cList=>'+chargesList.size());        
        List<CAP_IR_Charge__c> cList = new List<CAP_IR_Charge__c>();
        Map<Id, CAP_IR_Charge__c> chargeInvoiceMAP = new Map<Id, CAP_IR_Charge__c>(); 
        Map<Id, CAP_IR_Charge__c> chargeLineMAP = new Map<Id, CAP_IR_Charge__c>(); 
        Map<String,CAP_IR_Charge__c> oldChargeIds = new Map<String,CAP_IR_Charge__c>();
        if(chargesList[0] != NULL){            
            cList = chargesList[0];
            system.debug('cList=>'+cList.size());
            for(CAP_IR_Charge__c ch:cList){                
                oldChargeIds.put(ch.Clone_From__c,ch);
            }
            for(CAP_IR_Charge__c chr : [Select id,Text_Lookup_Invoice__c,Text_Lookup_Line_Item__c from CAP_IR_Charge__c where Id in:oldChargeIds.keyset()]){
                if(chr.Text_Lookup_Invoice__c != NULL){
                    chargeInvoiceMAP.put(chr.Text_Lookup_Invoice__c,oldchargeIds.get(chr.id));
                }
                if(chr.Text_Lookup_Line_Item__c != NULL){
                   chargeLineMAP.put(chr.Text_Lookup_Line_Item__c,oldchargeIds.get(chr.id)); 
                }
            }
        }
        Map<Id, Id> newInvoiceIdsMap = new Map<Id, Id>();
        Map<Id, Id> newLineIdsMap = new Map<Id, Id>();
        List<Invoice__c> invoiceList = [Select id, Clone_From__c from Invoice__c where Clone_From__c IN :chargeInvoiceMAP.keySet()];
        List<ContractLineItem> lineItemList = [Select id, Clone_From__c from ContractLineItem where Clone_From__c IN :chargeLineMAP.keySet()];
        for(Invoice__c inv:invoiceList){
            newInvoiceIdsMap.put(inv.Clone_From__c,inv.Id);
            system.debug('inv'+inv);
        }
        for(ContractLineItem line:lineItemList){
            newLineIdsMap.put(line.Clone_From__c,line.Id);
            system.debug('line'+line);
        }        
        for(CAP_IR_Charge__c ch:cList){
            if(newInvoiceIdsMap.containsKey(ch.Invoice_of_Original_Charge__c)){
                ch.SFS_Invoice__c = newInvoiceIdsMap.get(ch.Invoice_of_Original_Charge__c);
            }
            if(newLineIdsMap.containsKey(ch.CAP_IR_Contract_Line_Item__c)){
                ch.CAP_IR_Contract_Line_Item__c = newLineIdsMap.get(ch.CAP_IR_Contract_Line_Item__c);  
            }
        }
        UPDATE cList;  
    }    
}