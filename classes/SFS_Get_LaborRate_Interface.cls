/*=========================================================================================================
* @author Naresh Ponneri, Capgemini
* @date 26/07/2022
* @description:Labor Rate Interface
=============================================================================================================*/
global class SFS_Get_LaborRate_Interface {
    public static List<WorkOrderLineItem> woliLIST = new List<WorkOrderLineItem>();
   
    @invocableMethod(label = 'GetLaborRate')
    public static void GetLaborRate(List<Id> woliIDList){
        system.debug('woliIDList----------'+woliIDList);
        
       for(ID WoliID:woliIDList){
           
            SYSTEM.Debug('@@@@@@@@@@@@');
            WorkOrderLineItem WOLI=[SELECT ID,WorkOrderId,asset.product2.SFS_Product_Code__c,WorkType.Name,CreatedBy.Name from WorkOrderLineItem where id=:WoliID];
            workOrder wo=[select id,SFS_Division__r.Name,SFS_Work_Order_Type__c,CurrencyIsoCode,SFS_WOLI_Count__c,
                          Account.ShippingLatitude,Account.ShippingLongitude,Account.ShippingState,
                          Account.ShippingCountry,Account.County__c,
                          
                          asset.product2.Part_Number__c,asset.product2.SFS_Product_Code__c from WorkOrder where id=:WOLI.WorkOrderId];
           
            laborRateHeader wrapper = new laborRateHeader();
            wrapper.currencyCode =wo.CurrencyIsoCode;
            wrapper.headerAttributeValues.serviceType ='LaborRateServices';
            wrapper.headerAttributeValues.divisionH=wo.SFS_Division__r.Name;
            wrapper.headerAttributeValues.shipToStateH=wo.Account.ShippingState;
            wrapper.headerAttributeValues.workOrderTypeH=wo.SFS_Work_Order_Type__c;
           
            wrapper.headerAttributeValues.shippingLatitudeH= (wo.Account != NULL && wo.Account.shippingLatitude!=null) ? string.valueOf((wo.Account.shippingLatitude).setScale(6)) : '0.0';
            wrapper.headerAttributeValues.shippingLongitudeH= (wo.Account != NULL && wo.Account.ShippingLongitude!=null) ? string.valueOf((wo.Account.ShippingLongitude).setScale(6)) : '0.0';
            wrapper.headerAttributeValues.countyH=wo.Account.County__c;
            wrapper.headerAttributeValues.distanceUnitsH='Miles';
            
            itemAttributeValues ia=new itemAttributeValues();
            ia.productCodeLine=WOLI.asset.product2.SFS_Product_Code__c;
            ia.rateTypeL='';
            ia.workTypeL= WOLI.WorkType!=Null?WOLI.WorkType.Name:'';
            ia.WOLIIdL=WOLI.Id;
            
            Items itemWrap = new Items();
            itemWrap.itemIdentifier='1';
            // Note: This value has been hardcoded, CPQ is only looking at the product code -- part number does not matter
            // -Yasaswini and Arjun
            itemWrap.partNumber = 'COMP26610';
            itemWrap.itemAttributeValues=ia;
            
            wrapper.items.add(itemWrap);
            
            String jsonString = json.serialize(wrapper);
            system.debug('@@@jsonString'+jsonString); 
            String interfaceLabel = 'Labor Rate Interface';
            String WoLiIdLabel = WOLI.Id;
            InterfaceLabel = InterfaceLabel + '!' + WoLiIdLabel;
            system.debug('count------'+InterfaceLabel); 
            SFS_Outbound_Integration_Callout.HttpMethod(jsonString,'Labor Rate',InterfaceLabel);
            
        }

    }
    

    public static void getLaborRateResponse(HttpResponse response,String interfaceLabel){
        List<String> woliIDfromResponse = interfaceLabel.split('\\!');
        String Woliids=woliIDfromResponse[1];
        WorkOrderLineItem woliIDNEW=new WorkOrderLineItem();
                woliIDNEW.id=Woliids;
      
        if(response.getStatusCode() == 200){
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            system.debug('LaborRateResponse******'+results);
            Map<String,Map<String,string>> calculationsMap=new Map<String,Map<String,string>>();
            Map<string,string> calculationmap=new Map<string,string>();
            Map<string,string> woliWithValues=new Map<string,string>();
            List<WorkOrder> listOfWorkOrders=new  List<WorkOrder>();
            List<ServiceContract> listOfServiceContract=new  List<ServiceContract>();
            List<Invoice__c> listOfInvoice=new  List<Invoice__c>();
            List<Object> lstCalculationInfo =new List<Object>();
            for(Object items : (List<Object>)results.get('items')){
                Map<String,Object> item = (Map<String,Object>)items; 
                lstCalculationInfo=(List<Object>)item.get('calculationInfo');
                system.debug('calculationInfo2'+lstCalculationInfo);
            }
            for(Object calculation : lstCalculationInfo){
                Map<String, Object> calculationAttributes = (Map<String, Object>)calculation;
                for (String attributeName : calculationAttributes.keyset()) {
                    calculationmap.put(attributeName,string.valueof(calculationAttributes.get(attributeName)));
                }
                system.debug('calculation@@@@@@@@@@@'+calculationmap.get('WOLIId'));
                
                WorkOrderLineItem woli=new WorkOrderLineItem();
                woli.id=calculationmap.get('WOLIId');
                
                woli.SFS_Standard_Labor_Rate__c=Decimal.valueof(calculationmap.get('Standard'));
                woli.SFS_Overtime_Labor_Rate__c=Decimal.valueof(calculationmap.get('Overtime'));
                woli.SFS_Double_Labor_Rate__c=Decimal.valueof(calculationmap.get('Double'));
                woli.SFS_Round_Trip_Distance_Mi__c=Decimal.valueof(calculationmap.get('RoundTripDistance'));
                woli.SFS_Round_Trip_Time_Hrs__c=Decimal.valueof(calculationmap.get('RoundTripDuration'));  
                Update woli;

                    
               
            }
            woliIDNEW.SFS_Labor_Rate_Triggered__c = True;
            woliIDNEW.SFS_Labor_Rate_Resubmit__c = false;
                Update woliIDNEW;
        }
        else{
            List<String> woLiId = interfaceLabel.split('!');
            system.debug('@@@@@@@@@@@@Woli'+woLiId);
            if(!test.isRunningTest()){
            WorkOrderlineitem woli = [SELECT SFS_Integration_Status__c, SFS_Integration_Response__c FROM WorkOrderlineitem WHERE Id =: woLiId];
            woli.SFS_Integration_Status__c = 'ERROR';
            woli.SFS_Integration_Response__c = response.getBody();
             woli.SFS_Labor_Rate_Resubmit__c = false;
            update woli;
            }
        }
        
    }
    
    
    
   
    public class laborRateHeader{
        public String currencyCode; 
        public headerAttributeValues headerAttributeValues;
        public List<items> items;
        public laborRateHeader(){
            headerAttributeValues = new headerAttributeValues();
            items = new List<items>();
        }
    }
    public class headerAttributeValues {
        public String serviceType;  
        public String divisionH;    
        public String workOrderTypeH;    
        public String distanceUnitsH;
        public String shipToStateH;
        public String countyH;
        public String shippingLatitudeH;
        public String shippingLongitudeH;
    }
    public class items {
        public String itemIdentifier;   
        public String partNumber;   
        public itemAttributeValues itemAttributeValues;
    }
    public class itemAttributeValues {
        public String productCodeLine;  
        public String rateTypeL;    
        public String workTypeL;    
        public String WOLIIdL;  
    }
    
}