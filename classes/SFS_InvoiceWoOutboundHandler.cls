/*=========================================================================================================
* @author : Naresh Ponneri, Capgemini
* @date : 11/8/2022
* @description:
Modification Log:
================================================================================================================*/
public class SFS_InvoiceWoOutboundHandler {
    
    @invocableMethod(label = 'wORK Order Invoice' description = 'Send to CPI' Category = 'Invoice')
    public static void Run(List<ID> chargeId){
        SFS_WO_Invoice_XML_Structure__mdt[] revenueMetaMap = [SELECT SFS_Salesforce_Field__c, QualifiedAPIName,SFS_Salesforce_Object__c,SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c, SFS_Charge_Type__c 
                                                              FROM SFS_WO_Invoice_XML_Structure__mdt Order By SFS_Node_Order__c asc];    
        List<CAP_IR_Charge__c> chargeList = [Select ID,Name,SFS_Invoice_Date__c,SFS_Invoice_Format__c,SFS_Submitted_Date__c,SFS_Activity_Id__c,SFS_External_Id__c,CAP_IR_Description__c,SFS_Transaction_Id__c,CurrencyIsoCode,SFS_Integration_Status__c,
                                             CAP_IR_Status__c,SFS_Charge_Type__c,CAP_IR_Type__c,SFS_Invoice_Code__c,CAP_IR_Date__c,CTS_Quanity__c,SFS_Sell_Price__c,
                                             CreatedBy.Name,SFS_Integration_Id__c,CAP_IR_Amount__c,
                                             CAP_IR_Product_Consumed__r.SFS_Part_Number__c,SFS_Product__r.Part_Number__c,SFS_Product__r.SFS_Product_Code__c,SFS_Invoice__r.SFS_Invoice_Print_Code__c,CAP_IR_Product_Consumed__r.ProductConsumedNumber,CAP_IR_Product_Consumed__r.SFS_Product_Code__c,
                                             SFS_Expense__r.ExpenseNumber,SFS_Expense__r.ExpenseType,SFS_WOorAgreementWO__c,SFS_Project_Invoice__c,SFS_WO_PO_Number__c,SFS_Work_Order_Number__c,
                                             CAP_IR_Labor__r.Name,CAP_IR_Labor__r.SFS_Federation_Identifier__c,SFS_Related_Charge__r.SFS_Charge_Type__c,SFS_Related_Charge__r.CAP_IR_Type__c,SFS_Related_Charge_Expense_Type__c,
                                             SFS_Invoice__r.SFS_Invoice_Type__c ,CAP_IR_Work_Order__r.WorkOrderNumber,CAP_IR_Work_Order__r.SFS_PO_Number__c,CAP_IR_Work_Order__r.EndDate,CAP_IR_Work_Order__r.SFS_External_Id__c, CAP_IR_Work_Order__r.SFS_WOorAgreementWO__c
                                             FROM CAP_IR_Charge__c WHERE id =:chargeId];
        System.debug('***SubmittedCharges***'+chargeList);
        
        Map<Integer,Map<String,Object>> lineItemsMap = new Map<Integer,Map<String,Object>>();
        for(CAP_IR_Charge__c c:chargeList){
            
            Map<String,Object> chargFields = new Map <String, Object>();
            Map<String,Object> cInvoiceFields = new Map<String,Object>();
            Map<String,Object> cwoFields = new Map<String,Object>();
            Map<String,Object> cUserFields = new Map<String,Object>();
            Map<String,Object> cAgreeFields = new Map<String,Object>();
            Map<String,Object> cPConsumedFields = new Map<String,Object>();
            Map<String,Object> cExpenseFields = new Map<String,Object>();
            Map<String,Object> cLaborFields = new Map<String,Object>();
            Map<String,Object> cProductFields = new Map<String,Object>();
            chargFields = c.getPopulatedFieldsAsMap();
            try{
                Invoice__c cinv = (Invoice__c)chargFields.get('SFS_Invoice__r');
                cInvoiceFields = cinv.getPopulatedFieldsAsMap();
                System.debug('cInvoiceFields: '+CInvoiceFields);
            }catch(exception e){}
            try{
                WorkOrder cwo = (WorkOrder)chargFields.get('CAP_IR_Work_Order__r');
                cwoFields = cwo.getPopulatedFieldsAsMap();
                System.debug('cwoFields: '+cwoFields);
            }catch(exception e){}
            try{
                Product2 prd = (Product2)chargFields.get('SFS_Product__r');
                cProductFields = prd.getPopulatedFieldsAsMap();
                System.debug('cProductFields: '+cProductFields);
            }catch(exception e){}
            try{
                User cuser = (User)chargFields.get('CreatedBy');
                cUserFields = cuser.getPopulatedFieldsAsMap();
                System.debug('cUserFields: '+cUserFields);
            }catch(exception e){}
            /*try{
                ServiceContract cAgree = (ServiceContract)chargFields.get('CAP_IR_Service_Contract__r');
                cAgreeFields = cAgree.getPopulatedFieldsAsMap();
            }catch(exception e){}*/
            try{
                ProductConsumed  cPConsumed = (ProductConsumed)chargFields.get('CAP_IR_Product_Consumed__r');
                cPConsumedFields = cPConsumed.getPopulatedFieldsAsMap();
            }catch(exception e){}
            try{
                Expense cexpense = (Expense)chargFields.get('SFS_Expense__r');
                cExpenseFields = cexpense.getPopulatedFieldsAsMap();
            }catch(exception e){}
            
            try{
                CAP_IR_Labor__c cLabor = (CAP_IR_Labor__c)chargFields.get('CAP_IR_Labor__r');
                cLaborFields = cLabor.getPopulatedFieldsAsMap();
            }catch(exception e){} 
            Map<String,Object> finalChargeFields = new Map<String,Object>();
            for(String field : chargFields.keyset()){
                if(!field.contains('SFS_Invoice__r')&& !field.contains('SFS_Product__r') && !field.contains('CreatedBy') && !field.contains('CAP_IR_Work_Order__r') /*&& !field.contains('CAP_IR_Service_Contract__r')*/ && !field.contains('SFS_Expense__r')  && !field.contains('CAP_IR_Product_Consumed__r') && !field.contains('CAP_IR_Labor__r')){
                    /*if(field=='SFS_External_Id__c'){
                        string externalId=string.valueOf(c.get(field));
                        if(externalId!=null && externalId!=''){
                            string IntegrationId=externalId+'-1';
                            finalChargeFields.put(field,IntegrationId);
                        }
                    }
                    else{
                        finalChargeFields.put(field,c.get(field));
                    }*/
                    finalChargeFields.put(field,c.get(field));
                }
                /*if(field.contains('RecordType.DeveloperName')){
                    if(chargFields.get(field) == 'SFS_WO_Credit__c' || chargFields.get(field) == 'SFS_Credit'){
                        finalChargeFields.put('Invoice-SFS_External_Id__c', cInvoiceFields.get('SFS_Invoice__r.SFS_External_Id__c'));
                        finalChargeFields.put('RROnly','N');
                    }
                    else{
                        finalChargeFields.put('Invoice-SFS_External_Id__c', cInvoiceFields.get('Name'));
                        finalChargeFields.put('RROnly','Y');
                    }
                }*/
   
            }
            
            for(String field : cInvoiceFields.keyset()){
                string fieldstring = 'SFS_Invoice__r'+'-'+field;
                finalChargeFields.put(fieldstring, cInvoiceFields.get(field));
                System.debug('KEY: ' + fieldString + ', Value: ' + cInvoiceFields.get(field));
            }
            for(String field : cwoFields.keyset()){
                string fieldstring = 'CAP_IR_Work_Order__r'+'-'+field;
                try{
                        if(field == 'SFS_External_Id__c'){
                            finalChargeFields.put(fieldstring,cwoFields.get(field));
                            System.debug('TRUE WO');
                        }else{             
                            finalChargeFields.put(fieldstring, cwoFields.get(field));
                        }                        
                    }catch(System.Exception x){}
                System.debug('KEY: ' + fieldString + ', Value: ' + cwoFields.get(field));
            }
            for(String field : cProductFields.keyset()){
                string fieldstring = 'SFS_Product__r'+'-'+field;
                finalChargeFields.put(fieldstring, cProductFields.get(field));
                System.debug('KEY: ' + fieldString + ', Value: ' + cProductFields.get(field));
            }
            for(String field : cUserFields.keyset()){
                string fieldstring = 'CreatedBy'+'-'+field;
                finalChargeFields.put(fieldstring, cUserFields.get(field));
                System.debug('KEY: ' + fieldString + ', Value: ' + cUserFields.get(field));
            }
            /*for(String field : cAgreeFields.keyset()){
                string fieldstring = 'CAP_IR_Service_Contract__r'+'-'+field;
                finalChargeFields.put(fieldstring, cAgreeFields.get(field));
            }*/
            for(String field : cAgreeFields.keyset()){
                string fieldstring = 'SFS_Expense__r'+'-'+field;
                finalChargeFields.put(fieldstring, cAgreeFields.get(field));
            }
            for(String field : cAgreeFields.keyset()){
                string fieldstring = 'CAP_IR_Product_Consumed__r'+'-'+field;
                finalChargeFields.put(fieldstring, cAgreeFields.get(field));
            }
            for(String field : cAgreeFields.keyset()){
                string fieldstring = 'CAP_IR_Labor__r'+'-'+field;
                finalChargeFields.put(fieldstring, cAgreeFields.get(field));
            }
            
           // finalChargeFields.put('Count',chargeList.indexOf(c)+1);
            //lineItemsMap.put(chargeList.indexOf(c),finalChargeFields);
        
        List<String> chargeXMLList = new List<String>();
        List<String> reXMLList = new List<String>(); 
        String chargeType = '';
//        Map<String,Object> lineItemsFieldsMap = (Map<String, Object>)lineItemsMap.get(chargeList.indexOf(c)); 
        chargeType = string.valueOf(finalChargeFields.get('SFS_Charge_Type__c'));
   
        SFS_InvoiceWoOutboundHandler revenue=new SFS_InvoiceWoOutboundHandler();
        chargeXMLList = revenue.buildXmlStructure(revenueMetaMap,finalChargeFields,'Charge',chargeType);     
        system.debug('@reXMLList'+reXMLList);
      //  reXMLList.addAll(chargeXMLList);    
        SFS_WO_Invoice_XML_Structure__mdt re = SFS_WO_Invoice_XML_Structure__mdt.getInstance('SFS_End_Tag');
        chargeXMLList.add(re.SFS_XML_Full_Name__c);
        String xmlString = '';
        for(String s : chargeXMLList){
            xmlString = xmlString + s;
            System.debug('XML:  ' + s);
        }
        System.debug('XMLString ' + xmlString);
        
        //Callout 
        String IdString = String.ValueOf(ChargeID);
            system.debug('calling integration____________'+IdString);
        String interfaceDetail ='INT-155';
        String InterfaceLabel = 'WorkOrderRevenue|'+c.id;
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail,InterfaceLabel);
        }       
    }
    public List<String> buildXmlStructure(SFS_WO_Invoice_XML_Structure__mdt[] revenueMetaMap,Map<String,Object>fieldsMap,String ObjectName,String ChargeType){
        List<String> xmlStructure = new List<String>();
        integer count = 0;
        for(SFS_WO_Invoice_XML_Structure__mdt m: revenueMetaMap){
            System.debug('Map Meta: ' + m.SFS_XML_Full_Name__c);
            if(m.SFS_Salesforce_Object__c==ObjectName || m.SFS_Charge_Type__c==ChargeType){  
                if(m.SFS_HardCoded_Flag__c == false){
                    String sfField = m.SFS_Salesforce_Field__c;
                    If(sfField=='SFS_Invoice_Date__c'){
                         String xmlfullName = m.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(System.today());
                            replacement = replacement.escapeXml();
                            String newpa = XMLFullName.replace(sffield,replacement);  
                            xmlStructure.add(newpa);
                    }
                    else If(sfField=='SFS_Submitted_Date__c'){
                        if(fieldsMap.get('CAP_IR_Status__c')=='Submitted'){
                          String xmlfullName = m.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(System.today());
                            replacement = replacement.escapeXml();
                            String newpa = XMLFullName.replace(sffield,replacement);  
                            xmlStructure.add(newpa);
                        }
                    }
                    else if(fieldsMap.containsKey(sfField)){
                        if(fieldsMap.get(sffield) != null){
                            String xmlfullName = m.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(fieldsMap.get(sffield));
                            replacement = replacement.escapeXml();
                            String newpa = XMLFullName.replace(sffield,replacement);  
                            xmlStructure.add(newpa);
                        }  
                        else if(fieldsMap.get(sffield) == null){
                            xmlStructure.add(m.SFS_XML_Full_Name__c);
                        }    
                    }
                    else if(!fieldsMap.ContainsKey(sffield)){
                        String empty = '';
                        String replacement = m.SFS_XML_Full_Name__c.replace(sffield,empty);
                        replacement = replacement.unescapeHtml4();
                        xmlStructure.add(replacement);
                    } 
                }
                else if(m.SFS_HardCoded_Flag__c == true){
                    xmlStructure.add(m.SFS_XML_Full_Name__c);
                }
                
                System.debug('@@@@@xmlStructure: ' + m.SFS_Salesforce_Object__c);
                System.debug('@@@@@XMLBuildCount: ' + count);
                count++;
            }                        
        }         
        return xmlStructure;      
    }
    Public static void getResponse(HttpResponse res, String interfaceLabel){
       List<String> ChargeID = interfaceLabel.split('\\|');
        system.debug('ChargeIDs ******'+ChargeID);
        String chargeIds=ChargeID[1];
        system.debug('chargeIds ******'+chargeIds);
        
        chargeIds=chargeIds.replace('(', '');
        chargeIds=chargeIds.replace(')', '');
        chargeIds=chargeIds.replace(' ', '');
        
        List<string> listOfChargeIds=chargeIds.split('\\,');
        //Id ChargeIDs = Id.ValueOf(ChargeID[1]);
        system.debug('in WO revenue response method ******'+listOfChargeIds);
        List<CAP_IR_Charge__c> Charge = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM CAP_IR_Charge__c WHERE Id IN :listOfChargeIds];
                 
        system.debug('Charge 204'+Charge);        
        
        If(res.getStatusCode() != 200){
            for(CAP_IR_Charge__c ch:Charge){
            if(res.getStatusCode() == 0 || res.getBody() == 'Read timed out/nnull')
            {
           
            
            ch.SFS_Integration_Status__c = 'Timed Out';
            ch.SFS_Integration_Response__c = res.getBody();
           
            }
            else{
                ch.SFS_Integration_Status__c = 'Failed';
                ch.SFS_Integration_Response__c = res.getBody();
                
            }
            }
        }
        
        else{
             for(CAP_IR_Charge__c ch:Charge){
            system.debug('in WO revenue response method 2222******');
            ch.SFS_Integration_Status__c = 'Submitted';
            ch.SFS_Integration_Response__c = res.getBody();
             }
        }
                 
        update Charge;
        
       
     
    }
}