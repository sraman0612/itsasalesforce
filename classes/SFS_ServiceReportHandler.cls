/*=========================================================================================================
* @author Manimozhi, Capgemini
* @date 21/06/2021
* @description: SFS_ServiceReportHandler Apex Handler.

------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Manimozhi            M-001    21/06/2021  Apex Handler to call WorkOrderHandler Apex class to send mailto WOContacts

============================================================================================================================================================*/

public class SFS_ServiceReportHandler {
    
    public static void sendServiceReportAttachmentToWOContacts(List<ServiceReport> serReport){ 
        List<WorkOrder> wOrderList =new List<WorkOrder>();
        Set<Id> wOIds=new Set<Id>();
        for(ServiceReport sReport:serReport){
            wOIds.add(sReport.ParentId);
        }      
        if(!wOIds.isEmpty()){
            wOrderList=([Select Id,Status from WorkOrder where Id IN:wOIds]);
        }
        if(!wOrderList.isEmpty()){
            
            SFS_WorkOrderHandler.sendServiceReportAttachmentToMultipleRec(wOrderList);
        }
    }
}