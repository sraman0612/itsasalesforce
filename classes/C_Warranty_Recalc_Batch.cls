public class C_Warranty_Recalc_Batch implements Database.batchable<sObject> {
    

    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id FROM Asset WHERE Id IN (SELECT Serial_Number__c FROM Service_History__c WHERE Recalculation_Results__c != null) ';
        return Database.getQueryLocator(query);
    }
    
    
    public void execute(Database.BatchableContext info, List<Asset> scope){
        
            C_Warranty_Recalc recalc = new C_Warranty_Recalc(scope[0].Id);
            System.debug('Finished Processing');

    }     
    
    public void finish(Database.BatchableContext info){     

    } 
    
}