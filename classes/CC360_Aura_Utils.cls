public with sharing class CC360_Aura_Utils {

    @AuraEnabled
    public static String getCARLRecord(Id assetId){
        String result;
        
        try{
            List<Child_Asset__c> carls = [SELECT Id FROM Child_Asset__c WHERE Parent_Asset__c = :assetId];
            
            if(carls.size() > 0){
            	result = carls[0].Id;    
            }
            
        }
        catch(Exception e){
            System.debug(e);
            result = 'error';
        }
        
        return result;
    }
    
    @AuraEnabled
    public static Asset getAssetRecord(Id assetId){
        return [SELECT Id, User_CurrentServicer__c, User_SoldTo__c FROM Asset WHERE Id= :assetId];
    }
    
}