global class C_Utils{

   
    webservice static String saveCase(String CaseId){
        try{
            Case opp = new Case(Id=CaseId);
            opp.Status = 'Closed';
            update opp;
        }
        catch(DMLException e){
            return e.getDmlMessage(0);
        }
        return '-';
    }   
    
    
    global static void duplicateAttachments(List<Id> attachmentIds){
        System.debug(attachmentIds);
        List<Id> parentIds = new List<Id>();
        List<Attachment> theAttachments = [SELECT Name, ParentId, Body, ContentType FROM Attachment WHERE Id IN : attachmentIds];
        
        for(Attachment a : theAttachments){
            parentIds.add(a.ParentId);
        }
        
        Map<Id, Case> theCases = new Map<Id,Case>([SELECT Id, ParentId FROM Case WHERE Id IN :parentIds AND ParentId != null]);
        
        List<Attachment> clones = new List<Attachment>();
        
        for(Attachment a : theAttachments){
            Attachment newAttachment = a.clone();
            Case c = theCases.get(a.ParentId);
            if(c != null){
                newAttachment.ParentId = c.ParentId;
                clones.add(newAttachment);
            }
        }
        
        if(clones.size() > 0){
            insert clones;
        }
    }
    
    @InvocableMethod
    global static void sendTemplatedEmail(List<String> Ids){
        Case theCase = [SELECT Id, ContactId, Contact.AccountId, SuppliedEmail, Org_Wide_Email_Address_ID__c, New_Case_Notification_Sent_to_Contact__c  FROM Case WHERE Id =: Ids[0]];
        Messaging.SingleEmailMessage theEmail = new Messaging.SingleEmailMessage();
        try{
            EmailMessage em = [SELECT Id, ToAddress, CcAddress FROM EmailMessage WHERE Incoming = TRUE AND ParentId =: theCase.Id ORDER BY CreatedDate ASC LIMIT 1];
            List<String> addresses = new List<String>();
            if(!String.isEmpty(em.CcAddress)){
                addresses.addAll(em.CcAddress.split(';'));
            }      
            theEmail.setCCAddresses(addresses);
        }
        catch(Exception e){
            System.debug(e);
        }
        
        Email_Template_Admin__c ET = Email_Template_Admin__c.getOrgDefaults();
        
        theEmail.setWhatId(theCase.Id);
        
        if(Test.isRunningTest()){
            theEmail.setPlainTextBody('test body');
            theEmail.setToAddresses(new List<String>{'service-gdi@canpango.com'});
        }
        else{
            if(theCase.Contact.AccountId != null && theCase.Contact.AccountId != '001j000000nnpBR'){
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Case_With_Contact__c')));
    
            }
            else{
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Case_Without_Contact__c')));
    
            }
        }
        
        
        
        System.debug('theCase.Org_Wide_Email_Address_ID__c: ' + theCase.Org_Wide_Email_Address_ID__c);
        theEmail.setOrgWideEmailAddressId(theCase.Org_Wide_Email_Address_ID__c);
        theEmail.setUseSignature(false);
        Messaging.SendEmailResult[] theResult = Messaging.sendEmail(new Messaging.Email[]{theEmail}, true);
        
        theCase.New_Case_Notification_Sent_to_Contact__c = true;
        update theCase;
    }
    
    public static void updateParentStatus(String caseId){
        Case parentCase = new Case();
        parentCase.Id = caseId;
        parentCase.Status = 'Pending Response';          
        update parentCase;                        
    }        
    
}