@isTest
public with sharing class TSTU_MaterialVCSyncBatch
{
    public class MockSyncSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        private boolean success = true;

        public void setSuccess(boolean success)
        {
            this.success = success;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext)
        {
            if (this.throwException)
            {
                throw new UTIL_SyncHelper.SyncException('');
            }

            SBO_EnosixVCSync_Search.EnosixVCSync_SR searchResult =
                new SBO_EnosixVCSync_Search.EnosixVCSync_SR();

            // New Product
            SBO_EnosixVCSync_Search.SEARCHRESULT result1 =
                new SBO_EnosixVCSync_Search.SEARCHRESULT();

            result1.Material = 'Material1';
            result1.Plant = 'SED';
            result1.CharacteristicName = 'NEMA_CLASS';
            result1.CharacteristicDescription = 'NEMA';
            result1.CharacteristicValue = '1';
            result1.CharacteristicValueDescription = 'NEMA 1';

            searchResult.SearchResults.add(result1);

            // Existing Product
            SBO_EnosixVCSync_Search.SEARCHRESULT result2 =
                new SBO_EnosixVCSync_Search.SEARCHRESULT();

            result2.Material = 'Material2';
            result2.Plant = 'SED';
            result2.CharacteristicName = 'PRESSURE';
            result2.CharacteristicDescription = 'Pressure';
            result2.CharacteristicValue = '175';
            result2.CharacteristicValueDescription = '175';

            searchResult.SearchResults.add(result2);

            SBO_EnosixVCSync_Search.SEARCHRESULT result3 =
                new SBO_EnosixVCSync_Search.SEARCHRESULT();

            result3.Material = 'Material3';
            result3.Plant = 'SED';
            result3.CharacteristicName = 'TANK_SIZE';
            result3.CharacteristicDescription = 'Air Receiver Selection';
            result3.CharacteristicValue = '120H';
            result3.CharacteristicValueDescription = '120 Gallon Horizontal';

            searchResult.SearchResults.add(result3);

            SBO_EnosixVCSync_Search.SEARCHRESULT result4 =
                new SBO_EnosixVCSync_Search.SEARCHRESULT();

            result4.Material = 'Material4';
            result4.Plant = 'SED';
            result4.CharacteristicName = 'MOTOR_NO';
            result4.CharacteristicDescription = 'Motor Part Number:';
            result4.CharacteristicValue = '24AW27';
            result4.CharacteristicValueDescription = '30HP, 230/460 ODP';

            searchResult.SearchResults.add(result4);

            searchResult.setSuccess(this.success);
            searchContext.baseResult = searchResult;
            return searchContext;
        }
    }

    public static testMethod void test_MaterialVCSync()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixVCSync_Search.class, new MockSyncSearch());

        createExistingObject();
        Test.startTest();
        Product2 prod = new Product2();
        prod.Name = 'Name';
        prod.ProductCode = 'Material1';
        prod.FLD_Variant_Material__c = true;
        insert prod;
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = UTIL_Pricebook.getStandardPriceBookId();
        pbe.Product2Id = prod.Id;
        pbe.UnitPrice = 0;
        pbe.isActive = false;
        insert pbe;
        UTIL_MaterialVCSyncBatch controller = new UTIL_MaterialVCSyncBatch();

        //controller.setBatchParam(lastSync);
        Database.executeBatch(controller);
        Test.stopTest();
    }

    public static testMethod void test_MaterialVCSyncFailure()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixVCSync_Search.class, mockSyncSearch);
        mockSyncSearch.setSuccess(false);

        createExistingObject();
        Test.startTest();
        UTIL_MaterialVCSyncBatch controller = new UTIL_MaterialVCSyncBatch();
        Database.executeBatch(controller);
        Test.stopTest();
    }

    private static void createExistingObject()
    {

        Product2 currentObject = new Product2();
        currentObject.Name = 'Material1';
        currentObject.ENSX_EDM__Material__c = 'Material1';
        currentObject.ProductCode = 'Material1';
        currentObject.Delivering_Plant__c = 'SED';
        currentObject.FLD_Variant_Material__c = true;
        currentObject.FLD_Distribution_Channel__c = 'CM';
        currentObject.IsActive = true;
        currentObject.put(UTIL_MaterialVCSyncBatch.SFSyncKeyField,'Material1 - CM');
        insert currentObject;

        Product2 currentObject3 = new Product2();
        currentObject3.Name = 'Material3';
        currentObject3.ENSX_EDM__Material__c = 'Material3';
        currentObject3.ProductCode = 'Material3';
        currentObject3.Delivering_Plant__c = 'SED';
        currentObject3.FLD_Variant_Material__c = true;
        currentObject3.FLD_Distribution_Channel__c = 'CP';
        currentObject3.IsActive = true;
        currentObject3.put(UTIL_MaterialVCSyncBatch.SFSyncKeyField,'Material3 - CP');
        insert currentObject3;
    }
}