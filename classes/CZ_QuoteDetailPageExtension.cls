public class CZ_QuoteDetailPageExtension {
    public List<CZ_QuoteDetailPageWrapper> wrapperList;
    
    public List<CZ_QuoteDetailPageWrapper> getWrapperList() {
        return wrapperList;
    }
    
    public void showPage() {
        wrapperList = new List<CZ_QuoteDetailPageWrapper>();
        
        Id quoteId = (Id) ApexPages.currentPage().getParameters().get('qid');
        
        List<SBQQ__QuoteLine__c> quoteLineList = [
            SELECT Id,
            SBQQ__Product__r.Image_Selection_Criteria__c, 
            SBQQ__Product__r.ProductCode, 
            SBQQ__Product__r.Product_Type__c, 
            SBQQ__Product__r.Image_URL__c, 
            SBQQ__Product__r.HP_KW__c, 
            SBQQ__Product__r.FLD_Distribution_Channel__c, 
            SBQQ__Product__r.MG1__c, 
            SBQQ__Product__r.MPG4_Code__c, 
            SBQQ__Product__r.Feature_Benefits__c, 
            SBQQ__Product__r.SBQQ__ExternallyConfigurable__c, 
            SBQQ__Product__r.Product_Image__c, 
            SBQQ__Product__r.Technical_Information_Bullet_1__c, 
            SBQQ__Product__r.Distinctive_Benefit_1__c, 
            SBQQ__Product__r.Distinctive_Benefit_2__c, 
            SBQQ__Product__r.Distinctive_Benefit_3__c, 
            SBQQ__Product__r.Technical_Information_Bullet_2__c, 
            SBQQ__Product__r.Technical_Information_Bullet_3__c, 
            SBQQ__Product__r.Technical_Information_Bullet_4__c, 
            SBQQ__Product__r.Technical_Information_Bullet_5__c, 
            SBQQ__Product__r.Technical_Information_Bullet_6__c, 
            SBQQ__Product__r.Name,
            SBQQ__Product__r.MPG4_Code__r.MPG4_Number__c,
            SBQQ__Product__r.Gear_Size__c,
            Distribution_Channel__c,
            Frame_Size__c,
            Horsepower__c,
            Pressure__c,
            Recip_Model__c,
            Voltage__c,
            Mounting_Attribute__c,
            Model_Matrix__c
            FROM    SBQQ__QuoteLine__c
            WHERE   SBQQ__Quote__c = :quoteId
        ];
        
        for(SBQQ__QuoteLine__c quoteLine : quoteLineList){
            
            CZ_QuoteDetailPageWrapper wrapper = new CZ_QuoteDetailPageWrapper();
            
            if(quoteLine.SBQQ__Product__r.SBQQ__ExternallyConfigurable__c){
                
                System.debug(JSON.serializePretty(quoteLine));
                List<Machine_Data_Lookup__c> machineDataLookupList = UTIL_MachineDataLookup.getMachineDataLookupForQuoteLine(quoteLine);
                
                if (machineDataLookupList.isEmpty()) {
                    System.debug('No Data Lookup');
                    continue;
                }

                wrapper.machineDetails = machineDataLookupList.get(0);
                wrapper.productCode = quoteLine.SBQQ__Product__r.ProductCode;
                
                    
                List<Product_Images__c> imageList = [Select Attribute__c, DC__c, Horsepower__c, Image_Selection_Criteria__c, Image_URL__c from Product_Images__c where MPG4_Code__c = :quoteLine.SBQQ__Product__r.MPG4_Code__c];
                system.debug(imageList);
                
                //CZ_QuoteDetailPageWrapper wrapper = new CZ_QuoteDetailPageWrapper();
                
                wrapper.productCode = quoteLine.SBQQ__Product__r.ProductCode;
                
                //   if(detailList.size() > 0) wrapper.machineDetails = detailList[0];
                
                for(Product_Images__c image : imageList){
                    
                    if(image.DC__c.contains(quoteLine.SBQQ__Product__r.FLD_Distribution_Channel__c) 
                       && image.Image_Selection_Criteria__c == quoteLine.SBQQ__Product__r.Image_Selection_Criteria__c 
                       && image.Attribute__c == quoteLine.Mounting_Attribute__c
                       && image.Horsepower__c == quoteLine.Horsepower__c) {//SBQQ__Product__r.HP_KW__c
                           
                           wrapper.mainImage = image.Image_URL__c;
                           system.debug(wrapper.mainImage);
                           break;
                           
                       }
        
                }

                //List<Product_Images__c> imageList = UTIL_MachineDataLookup.getMachineImages(quoteLine);
                
                // if (!imageList.isEmpty()) {
                //     wrapper.mainImage = imageList.get(0).Image_URL__c;
                // }
                
                List<Id> benefitIdList = new List<Id>();
                
                if(quoteLine.SBQQ__Product__r.SBQQ__ExternallyConfigurable__c){ 
                    if(wrapper.machineDetails.Distinct_Benefit_1__c != null) benefitIdList.add(wrapper.machineDetails.Distinct_Benefit_1__c);
                    
                    
                    if(benefitIdList.isEmpty()){
                        continue;  
                    }
                    
                    if(wrapper.machineDetails.Distinct_Benefit_2__c != null) benefitIdList.add(wrapper.machineDetails.Distinct_Benefit_2__c);
                    if(wrapper.machineDetails.Distinct_Benefit_3__c != null) benefitIdList.add(wrapper.machineDetails.Distinct_Benefit_3__c);
                } else {
                    if(quoteLine.SBQQ__Product__r.Distinctive_Benefit_1__c != null) benefitIdList.add(quoteLine.SBQQ__Product__r.Distinctive_Benefit_1__c);
                    
                    
                    if(benefitIdList.isEmpty()){
                        continue;  
                    }
                    
                    if(quoteLine.SBQQ__Product__r.Distinctive_Benefit_2__c != null) benefitIdList.add(quoteLine.SBQQ__Product__r.Distinctive_Benefit_2__c);
                    if(quoteLine.SBQQ__Product__r.Distinctive_Benefit_3__c != null) benefitIdList.add(quoteLine.SBQQ__Product__r.Distinctive_Benefit_3__c);
                }
                
                if(benefitIdList.isEmpty()){
                    continue;  
                }
                
                if (!benefitIdList.isEmpty()) {
                    wrapper.benefitList = [
                        SELECT Id,
                        Image_URL__c,
                        Bullet_Point_1__c,
                        Bullet_Point_2__c,
                        Bullet_Point_3__c,
                        Bullet_Point_4__c,
                        Description__c,
                        Image__c
                        FROM Lookup_Quote_Docs_Distinct_Benefits__c 
                        WHERE Id IN :benefitIdList
                    ];
                }
                
                if (wrapper.machineDetails.Feature_Benefits__c != null) {
                    wrapper.featureBenefits = [
                        SELECT Id, 
                        Benefits_Bullet_1__c, 
                        Benefits_Bullet_2__c,
                        Benefits_Bullet_3__c,
                        Benefits_Bullet_4__c,
                        Benefits_Bullet_5__c,
                        Benefits_Bullet_6__c,
                        Benefits_Bullet_7__c,
                        Benefits_Bullet_8__c,
                        Benefits_Bullet_9__c,
                        Benefits_Bullet_10__c,
                        Benefits_Bullet_11__c,
                        Benefits_Bullet_12__c,
                        Component_Features_Bullet_1__c,
                        Component_Features_Bullet_2__c,
                        Component_Features_Bullet_3__c,
                        Component_Features_Bullet_4__c,
                        Component_Features_Bullet_5__c,
                        Component_Features_Bullet_6__c,
                        Component_Features_Bullet_7__c,
                        Component_Features_Bullet_8__c,
                        Component_Features_Bullet_9__c,
                        Component_Features_Bullet_10__c,
                        Component_Features_Bullet_11__c,
                        Component_Features_Bullet_12__c,
                        Product_Description__c
                        FROM Lookup_Quote_Docs_Machines__c
                        WHERE Id = : wrapper.machineDetails.Feature_Benefits__c
                        LIMIT 1
                    ];
                }
                else{
                    continue;
                }
                
                if (wrapper.machineDetails.Tech_Specs__c != null) {
                    Lookup_Quote_Docs_Tech_Specs__c techspecs = [
                        Select Id, 
                        Technical_Information_Bullet_1__c, 
                        Technical_Information_Bullet_2__c, 
                        Technical_Information_Bullet_3__c, 
                        Technical_Information_Bullet_4__c, 
                        Technical_Information_Bullet_5__c, 
                        Technical_Information_Bullet_6__c 
                        from Lookup_Quote_Docs_Tech_Specs__c
                        where Id = : wrapper.machineDetails.Tech_Specs__c LIMIT 1
                    ];
                    
                    if (techspecs != null) {
                        wrapper.technicalInfo1 = techspecs.Technical_Information_Bullet_1__c;
                        wrapper.technicalInfo2 = techspecs.Technical_Information_Bullet_2__c;
                        wrapper.technicalInfo3 = techspecs.Technical_Information_Bullet_3__c;
                        wrapper.technicalInfo4 = techspecs.Technical_Information_Bullet_4__c;
                        wrapper.technicalInfo5 = techspecs.Technical_Information_Bullet_5__c;
                        wrapper.technicalInfo6 = techspecs.Technical_Information_Bullet_6__c;
                    }
                    
                }
                else{
                    continue;
                }
                
                wrapper.productDescription = wrapper.featureBenefits.Product_Description__c;
                
                System.debug(wrapper.machineDetails);
                
                wrapperList.add(wrapper);
            }
            else {
                //CK - removed check for quoteLine.SBQQ__Product__r.Distinctive_Benefit_1__c == null
                if (quoteLine.SBQQ__Product__r.Technical_Information_Bullet_1__c == null || quoteLine.SBQQ__Product__r.Feature_Benefits__c == null) {
                    continue;
                }
                
                List<Lookup_Quote_Docs_Machines__c> detailList = [
                    SELECT  display__c, Image_URL__c,Benefits_Bullet_1__c, Benefits_Bullet_2__c, Benefits_Bullet_3__c, Benefits_Bullet_4__c,
                    Benefits_Bullet_5__c, Benefits_Bullet_6__c, Benefits_Bullet_7__c, Benefits_Bullet_8__c, Benefits_Bullet_9__c,
                    Benefits_Bullet_10__c, Benefits_Bullet_11__c, Benefits_Bullet_12__c, Component_Features_Bullet_1__c,
                    Component_Features_Bullet_2__c, Component_Features_Bullet_3__c, Component_Features_Bullet_4__c,
                    Component_Features_Bullet_5__c, Component_Features_Bullet_6__c, Component_Features_Bullet_7__c,
                    Component_Features_Bullet_8__c, Component_Features_Bullet_9__c, Component_Features_Bullet_10__c,
                    Component_Features_Bullet_11__c, Component_Features_Bullet_12__c, Product_Description__c, Image__c,
                    Technical_Information_Bullet_1__c, Technical_Information_Bullet_2__c, Technical_Information_Bullet_3__c,
                    Technical_Information_Bullet_4__c, Technical_Information_Bullet_5__c, Technical_Information_Bullet_6__c
                    FROM    Lookup_Quote_Docs_Machines__c where Id = :quoteLine.SBQQ__Product__r.Feature_Benefits__c 
                ];
                
                System.debug(quoteLine.SBQQ__Product__r.Feature_Benefits__c);
                
                
                List<Id> distinctBenefitIdList = new List<Id>();
                
                if(quoteLine.SBQQ__Product__r.Distinctive_Benefit_1__c != null) distinctBenefitIdList.add(quoteLine.SBQQ__Product__r.Distinctive_Benefit_1__c);
                if(quoteLine.SBQQ__Product__r.Distinctive_Benefit_2__c != null) distinctBenefitIdList.add(quoteLine.SBQQ__Product__r.Distinctive_Benefit_2__c);
                if(quoteLine.SBQQ__Product__r.Distinctive_Benefit_3__c != null) distinctBenefitIdList.add(quoteLine.SBQQ__Product__r.Distinctive_Benefit_3__c);
                
                System.debug(distinctBenefitIdList);
                
                List<Lookup_Quote_Docs_Distinct_Benefits__c> benefitList;
                
                if (distinctBenefitIdList.size() > 0){
                    
                    benefitList = [
                        SELECT  Image_URL__c,Bullet_Point_1__c, Bullet_Point_2__c, Bullet_Point_3__c, Bullet_Point_4__c, Description__c, Image__c
                        FROM    Lookup_Quote_Docs_Distinct_Benefits__c where Id in :distinctBenefitIdList
                    ];
                    
                }
                
                
                System.debug('ImageURL - '+quoteLine.SBQQ__Product__r.Image_URL__c);
                if(detailList.size() > 0){
                    wrapper.productDescription = detailList[0].Product_Description__c;
                    wrapper.featureBenefits = detailList[0];
                } 
                wrapper.mainImage = quoteLine.SBQQ__Product__r.Image_URL__c;
                wrapper.productCode = quoteLine.SBQQ__Product__r.ProductCode;
                wrapper.technicalInfo1 = quoteLine.SBQQ__Product__r.Technical_Information_Bullet_1__c;
                wrapper.technicalInfo2 = quoteLine.SBQQ__Product__r.Technical_Information_Bullet_2__c;
                wrapper.technicalInfo3 = quoteLine.SBQQ__Product__r.Technical_Information_Bullet_3__c;
                wrapper.technicalInfo4 = quoteLine.SBQQ__Product__r.Technical_Information_Bullet_4__c;
                wrapper.technicalInfo5 = quoteLine.SBQQ__Product__r.Technical_Information_Bullet_5__c;
                wrapper.technicalInfo6 = quoteLine.SBQQ__Product__r.Technical_Information_Bullet_6__c;
                
                
                if(benefitList != null && benefitList.size() > 0){
                    wrapper.benefitList = benefitList;   
                }
                System.debug(wrapper);
                wrapperList.add(wrapper);                                    
            }
        }
    }     
    
}