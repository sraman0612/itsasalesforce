/*=========================================================================================================
* @author Naresh, Capgemini
* @date 
* @description: Mock Callout class to test rest api Interfaces

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date         Description
------------------------------------------------------------------------------------
Srikanth P        M-001      08/06/2023   Added invokeMockResponseReturnOrder method
===========================================================================================================*/
@isTest
global class SFS_MockCalloutClass {
    global static HttpResponse invokeMockResponse(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.com/test/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        res.setStatusCode(200);
        res.setBody('{"items":[{"itemIdentifier":"1","partNumber":"COMP26610","unitPrice":0.000000,"calculationInfo":[{"Standard":199,"Overtime":299,"Double":398,"RATE_TYPE":"HOURLY","ITEM_NUMBER":"SMALL ROTARY","STANDARD_HOURS":"0","LaborId":"","WOLIId":"1WL3a0000036eO5GAI","RoundTripDistance":37.6,"RoundTripDuration":0.9,"RoundTripError":"NA"}],"_bomItemVariableName":""}]}');
        return res;
    }
    global static HttpResponse invokeMockResponseReturnOrder(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.com/test/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        res.setStatusCode(200);
        res.setBody('<?xml version="1.0"?><!DOCTYPE DocumentExportResponse SYSTEM "http://solutions.sciquest.com/app_docs/dtd/documentExport/DocumentExport.dtd"><DocumentExportResponse version="1.0"><Header><MessageId>020739E66F49638BE0640021280E8E4F</MessageId><Timestamp></Timestamp></Header><Response><Status><StatusCode>200</StatusCode><StatusText><Status><XxintMsgID>020739E66F49638BE0640021280E8E4F</XxintMsgID><code>SUCCESS</code><errorMessage>Out of 1 Incoming Transactions, All Transactions inserted into Oracle Open Interface</errorMessage><G_TRANSACTIONS><MTL_TRANSACTIONS_INTERFACE><UNIQUE_REFERENCE_NO>RO-0001-ROLI-0001</UNIQUE_REFERENCE_NO><ORACLE_TRANSACTION_ID/><code>SUCCESS</code><errorMessage/></MTL_TRANSACTIONS_INTERFACE></G_TRANSACTIONS></Status></StatusText></Status></Response></DocumentExportResponse>');
        return res;
    }
    global static HttpResponse invokeMockResponseWarrantyClaim(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.com/test/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        res.setStatusCode(200);
         res.setBody('<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://www.w3.org/2005/08/addressing"><env:Body><ClaimSubmissionResponse xmlns:ns1="http://www.tavant.com/globalsync/SalesforceITSClaimSubmisionResponse" xmlns="http://www.irco.com/TavantServiceWarrantyReqABCSImpl/response"><Status xmlns="">FAILURE</Status><ErrorCodes xmlns=""><EachErrorCode><errorCode>1</errorCode><errorMessage>Conditions Found Tag is mandatory and the value is blank or empty</errorMessage></EachErrorCode></ErrorCodes></ClaimSubmissionResponse></env:Body></env:Envelope>');       
    return res;
    }
    
    global static HttpResponse invokeMockResponsePOReceipt(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.com/test/test');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        res.setStatusCode(200);
         res.setBody('<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://www.w3.org/2005/08/addressing"> <env:Header> <wsa:Action>execute</wsa:Action> <wsa:MessageID>urn:06dbca19-9650-11ec-b011-02001708e866</wsa:MessageID> <wsa:ReplyTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> <wsa:ReferenceParameters> <instra:tracking.ecid xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">efdbe64f-8c4f-4d51-970a-9a23aca858b4-019cdae0</instra:tracking.ecid> <instra:tracking.FlowEventId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">65910440</instra:tracking.FlowEventId> <instra:tracking.FlowId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">10236104</instra:tracking.FlowId> <instra:tracking.CorrelationFlowId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">0000NwlWN346qIB_vXh8iX1Y5u^f00000k</instra:tracking.CorrelationFlowId> <instra:tracking.quiescing.SCAEntityId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">3760029</instra:tracking.quiescing.SCAEntityId> </wsa:ReferenceParameters> </wsa:ReplyTo> <wsa:FaultTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> </wsa:FaultTo> </env:Header> <env:Body> <ATPResponse xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns="http://xmlns.irco.com/ATPPriceOutResponseABCSImpl"> <ControlArea xmlns=""> <PartnerCode>CPQ</PartnerCode> <RequestType>QUERY</RequestType> <ExternalMessageID>CPQ TestArjun Test Opty 10012021_CTS-76421645803040608</ExternalMessageID> <Database>ISOADEV1</Database> <SourceSystem>EBS</SourceSystem> <Status>SUCCESS</Status> <Messsage>Request Processed Successfully</Messsage> <MessageID>D5DF380550575F90E0540021280BE0EF</MessageID> </ControlArea> <DataArea xmlns=""> <GItem> <Item> <ItemNumber>23231806</ItemNumber> <BestWHSE>DLC (DCL)</BestWHSE> <GAvailability> <Availability> <WHSE>DLC (DCL)</WHSE> <ResultCode>SUCCESS</ResultCode> <AvailableDate>25-FEB-2022</AvailableDate> </Availability> </GAvailability> </Item> </GItem> </DataArea> </ATPResponse> </env:Body> </env:Envelope>');       
    return res;
    }
}