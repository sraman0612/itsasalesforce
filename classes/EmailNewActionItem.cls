public class EmailNewActionItem {
    
    @AuraEnabled
    public static Case getCase(Id emailId){
        String caseId = [SELECT ParentId FROM EmailMessage WHERE Id = :emailId].ParentId;
        
        //make your SOQL here from your desired object where you want to place your lightning action
        return [SELECT Id, OwnerId, Status, AccountId, Org_Wide_Email_Address_ID__c,
                    Subject, CaseNumber, Description FROM Case WHERE Id = :caseId];
    }

    @AuraEnabled
    public static String getCaseRecType (String name){
        //make your SOQL here from your desired object where you want to place your lightning action
        return [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = :name].Id;
    }

}