@isTest
public class TSTE_VCSettings
{
    @isTest static void testClassVariables ()
    {
        Test.startTest();
        ENSX_VCSettings ensxVcSettings = new ENSX_VCSettings();
        ensxVcSettings.DisplayCost = true;
        ensxVcSettings.DisplayPrice = true;
        ensxVcSettings.FetchConfigurationFrequency = 'FetchConfigurationFrequency';
        ensxVcSettings.FetchConfigurationFrequencyPossibilities = null;
        System.assert(ensxVcSettings.FetchConfigurationFrequency == 'FetchConfigurationFrequency');
        Test.stopTest();
    }
}