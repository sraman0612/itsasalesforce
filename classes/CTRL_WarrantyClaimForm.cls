public class CTRL_WarrantyClaimForm {
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_WarrantyClaimForm.class);

    private static Product2 getProduct(String productCode, String distributionChannel) {
        System.debug('productCode=' + productCode);
        System.debug('distributionChannel=' + distributionChannel);
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            WHERE ProductCode = :productCode AND FLD_Distribution_Channel__c = :distributionChannel
        ];

        if (productList == null || productList.isEmpty() || productList.size() > 1) {
            return null;
        }

        return productList.get(0);
    }

    @AuraEnabled
    public static UTIL_Aura.Response getSalesOrderDetail(String sapSalesOrderNumber) {
        logger.enterAura('getSalesOrder', new Map<String, Object>{ 'sapSalesOrderNumber' => sapSalesOrderNumber });

        SBO_EnosixSO_Detail sbo = new SBO_EnosixSO_Detail();

        try {
            SBO_EnosixSO_Detail.EnosixSO salesOrder = sbo.getDetail(sapSalesOrderNumber);

            return UTIL_Aura.createResponse(salesOrder);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    @AuraEnabled
    public static UTIL_Aura.Response createServiceNotificationImpl(String warrantyClaimStr) {
        logger.enterAura('verifyServiceNotification', new Map<String, Object>{ 'warrantyClaimStr' => warrantyClaimStr });

        OBJ_WarrantyClaim warrantyClaim = (OBJ_WarrantyClaim) JSON.deserialize(warrantyClaimStr, OBJ_WarrantyClaim.class);

        SBO_EnosixServiceNotification_Detail.EnosixServiceNotification notification = new SBO_EnosixServiceNotification_Detail.EnosixServiceNotification();

        Account assetAccount = getAccount(warrantyClaim.accountId);
        Asset serialNumber = getSerialNumber(warrantyClaim.serialNumberId);

        notification.NotificationType = 'Z1';
        notification.NotificationDescription = 'Default';

        if (assetAccount != null) {
            notification.CustomerNumber = assetAccount.AccountNumber;
        }

        if (serialNumber != null) {
            notification.SalesOrganization = serialNumber.SOrg__c;
            notification.DistributionChannel = serialNumber.DChl__c;
            notification.Division = '00';
            notification.PartNo = serialNumber.Model_Number__c;
            notification.Serial = serialNumber.Name;
            notification.EquipmentNumber = serialNumber.Equipment_Number__c;
        } else {
            if (String.isNotBlank(warrantyClaim.salesOrg)) {
                notification.SalesOrganization = warrantyClaim.salesOrg;
            }
            if (String.isNotBlank(warrantyClaim.distributionChannel)) {
                notification.DistributionChannel = warrantyClaim.distributionChannel;
                notification.Division = '00';
            }
            if (String.isNotBlank(warrantyClaim.failedPartNumber)) {
                notification.PartNo = warrantyClaim.failedPartNumber;
            }
            if (String.isNotBlank(warrantyClaim.failedSerialNumber)) {
                notification.Serial = warrantyClaim.failedSerialNumber;
            }
        }

        notification.PartsWarranty = warrantyClaim.partsClaim;

        if (warrantyClaim.serviceDate != null) {
            System.debug('warrantyClaim.serviceDate=' + warrantyClaim.serviceDate);
            notification.MALFUNCTION_BREAKDOWN.EndDateofMalfunction = warrantyClaim.serviceDate;
            System.debug(
                'notification.MALFUNCTION_BREAKDOWN.EndDateofMalfunction=' + notification.MALFUNCTION_BREAKDOWN.EndDateofMalfunction
            );
        }

        if (warrantyClaim.dateOfMalfunction != null) {
            notification.MALFUNCTION_BREAKDOWN.StartDateofMalfunction = warrantyClaim.dateOfMalfunction;
        }

        if (String.isNotBlank(warrantyClaim.locationNameLine1)) {
            notification.EQUIPMENT_ADDRESS.NameLine1 = warrantyClaim.locationNameLine1;
        }
        if (String.isNotBlank(warrantyClaim.locationNameLine2)) {
            notification.EQUIPMENT_ADDRESS.NameLine2 = warrantyClaim.locationNameLine2;
        }
        if (String.isNotBlank(warrantyClaim.locationStreet)) {
            notification.EQUIPMENT_ADDRESS.Street = warrantyClaim.locationStreet;
        }
        if (String.isNotBlank(warrantyClaim.locationCity)) {
            notification.EQUIPMENT_ADDRESS.City = warrantyClaim.locationCity;
        }
        if (String.isNotBlank(warrantyClaim.locationPostalCode)) {
            notification.EQUIPMENT_ADDRESS.PostalCode = warrantyClaim.locationPostalCode;
        }
        if (String.isNotBlank(warrantyClaim.locationRegion)) {
            notification.EQUIPMENT_ADDRESS.Region = warrantyClaim.locationRegion;
        }
        if (String.isNotBlank(warrantyClaim.locationCountry)) {
            notification.EQUIPMENT_ADDRESS.Country = warrantyClaim.locationCountry;
        }
        if (String.isNotBlank(warrantyClaim.locationCounty)) {
            notification.EQUIPMENT_ADDRESS.CountryDistrict = warrantyClaim.locationCounty;
        }
        if (String.isNotBlank(warrantyClaim.locationFax)) {
            notification.EQUIPMENT_ADDRESS.FaxNumber = warrantyClaim.locationFax;
        }
        if (String.isNotBlank(warrantyClaim.locationPhone)) {
            notification.EQUIPMENT_ADDRESS.TelephoneNumber = warrantyClaim.locationPhone;
        }

        SBO_EnosixServiceNotification_Detail sbo = new SBO_EnosixServiceNotification_Detail();

        try {
            notification = sbo.command('CMD_SERVICE_NOTN_CHECK', notification);

            if (notification.getMessages() != null) {
                myAddFrameworkMessages(notification.getMessages());
            }

            notification.REFERENCE_OBJECT.ClaimText = warrantyClaim.claimText;

            return saveServiceNotification(notification);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    @AuraEnabled
    public static UTIL_Aura.Response saveServiceNotification(SBO_EnosixServiceNotification_Detail.EnosixServiceNotification notification) {
        logger.enterAura('saveServiceNotification', new Map<String, Object>{ 'notification' => notification });

        SBO_EnosixServiceNotification_Detail sbo = new SBO_EnosixServiceNotification_Detail();

        try {
            notification = sbo.save(notification);

            if (notification.getMessages() != null) {
                myAddFrameworkMessages(notification.getMessages());
            }

            return UTIL_Aura.createResponse(notification);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    @AuraEnabled
    public static UTIL_Aura.Response simulateSalesOrder(String warrantyClaimStr, Boolean saveToo) {
        try {
            logger.enterAura('simulateSalesOrder', new Map<String, Object>{ 'warrantyClaimStr' => warrantyClaimStr, 'saveToo' => saveToo });

            System.debug(warrantyClaimStr);

            OBJ_WarrantyClaim warrantyClaim = (OBJ_WarrantyClaim) JSON.deserialize(warrantyClaimStr, OBJ_WarrantyClaim.class);

            Account assetAccount = getAccount(warrantyClaim.accountId);
            Asset serialNumber = getSerialNumber(warrantyClaim.serialNumberId);

            if (String.isNotBlank(warrantyClaim.failedPartNumber)) {
                Product2 failedPart = getProduct(warrantyClaim.failedPartNumber, warrantyClaim.distributionChannel);

                if (failedPart == null) {
                    UTIL_PageMessages.addMessage(
                        'ERROR',
                        'Failed part ' +
                        warrantyClaim.failedPartNumber +
                        ' does not exist for Distribution Channel ' +
                        warrantyClaim.distributionChannel +
                        '.'
                    );
                    return UTIL_Aura.createResponse(null);
                }
            }

            SBO_EnosixSO_Detail.EnosixSO salesOrder = new SBO_EnosixSO_Detail.EnosixSO();

            salesOrder.SALES.SalesDocumentType = 'ZIDW';
            salesOrder.PartsWarranty = warrantyClaim.partsClaim;

            if (String.isNotBlank(warrantyClaim.salesOrg)) {
                salesOrder.SALES.SalesOrganization = warrantyClaim.salesOrg;
            }
            if (String.isNotBlank(warrantyClaim.distributionChannel)) {
                salesOrder.SALES.DistributionChannel = warrantyClaim.distributionChannel;
                salesOrder.SALES.Division = '00';
            }
            if (String.isNotBlank(warrantyClaim.modelNumber)) {
                salesOrder.Material = warrantyClaim.modelNumber;
            }
            if (String.isNotBlank(warrantyClaim.serialNumber)) {
                salesOrder.SerialNumber = warrantyClaim.serialNumber;
            }

            if (serialNumber != null) {
                salesOrder.EquipmentNumber = serialNumber.Equipment_Number__c;
            }

            salesOrder.SALES.Division = '00';

            if (warrantyClaim.serviceDate != null) {
                salesOrder.BILLING.DateOnWhichServicesRendered = warrantyClaim.serviceDate;
            }

            salesOrder.SoldToParty = assetAccount.AccountNumber;
            salesOrder.SHIPPING.ShipToParty = assetAccount.AccountNumber;

            salesOrder.NotificationNumber = warrantyClaim.sapServiceNotificationNumber;

            if (String.isBlank(warrantyClaim.customerReferenceNumber)) {
                salesOrder.CustomerPurchaseOrderNumber = 'None';
            } else {
                salesOrder.CustomerPurchaseOrderNumber = warrantyClaim.customerReferenceNumber;
            }

            if (String.isNotEmpty(warrantyClaim.serviceTechnicianNumber)) {
                SBO_EnosixSO_Detail.PARTNERS soPartner = new SBO_EnosixSO_Detail.PARTNERS();
                salesOrder.PARTNERS.add(soPartner);
                soPartner.PartnerFunction = 'ZW';
                soPartner.ContactPersonNumber = warrantyClaim.serviceTechnicianNumber;
                soPartner.CustomerNumber = warrantyClaim.serviceTechnicianNumber;
            }

            User reportedBy = null;
            reportedBy = [SELECT Id, Name FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

            salesOrder.ORDERDATA.YourReference = reportedBy.Name;
            salesOrder.ORDERDATA.OrdererName = reportedBy.Name;

            SBO_EnosixSO_Detail.ITEMS item = null;

            if (String.isNotBlank(warrantyClaim.serialNumber) && !warrantyClaim.partsClaim) {
                item = new SBO_EnosixSO_Detail.ITEMS();
                item.ItemNumber = '10';
                item.Material = warrantyClaim.modelNumber;
                item.SerialNumber = warrantyClaim.serialNumber;
                item.ItemDescription = leftPad(warrantyClaim.serialNumber, 7) + '*' + leftPad(warrantyClaim.hoursInService, 6);

                salesOrder.ITEMS.add(item);
            }

            if (String.isNotBlank(warrantyClaim.failedPartNumber)) {
                System.debug('warrantyClaim.failedPartNumber=' + warrantyClaim.failedPartNumber);
                System.debug('warrantyClaim.failedHoursInService=' + warrantyClaim.failedHoursInService);
                System.debug('warrantyClaim.failedStartupDate=' + warrantyClaim.failedStartupDate);
                item = new SBO_EnosixSO_Detail.ITEMS();
                salesOrder.ITEMS.add(item);
                item.ItemNumber = '20';
                item.Material = warrantyClaim.failedPartNumber;
                item.SerialNumber = warrantyClaim.failedSerialNumber;

                String failedHoursInService = '0';
                String failedStartupDate = '0';
                String failedSerialNumber = '';

                if (warrantyClaim.failedHoursInService != null) {
                    failedHoursInService = String.valueOf(warrantyClaim.failedHoursInService);
                }

                if (warrantyClaim.failedSerialNumber != null) {
                    failedSerialNumber = warrantyClaim.failedSerialNumber;
                }

                if (warrantyClaim.failedStartupDate != null) {
                    failedHoursInService = String.valueOf(warrantyClaim.failedHoursInService);
                    Datetime dt = Datetime.newInstance(
                        warrantyClaim.failedStartupDate.year(),
                        warrantyClaim.failedStartupDate.month(),
                        warrantyClaim.failedStartupDate.day()
                    );
                    failedStartupDate = dt.format('yyyyMMdd');
                }

                item.ItemDescription =
                    leftPad(warrantyClaim.failedSerialNumber, 7) +
                    '*' +
                    leftPad(failedHoursInService, 6) +
                    '*' +
                    leftPad(failedStartupDate, 10, '0');
            }

            if (warrantyClaim.laborHours != null && warrantyClaim.laborHours > 0) {
                item = new SBO_EnosixSO_Detail.ITEMS();
                salesOrder.ITEMS.add(item);
                item.ItemNumber = '30';
                item.Material = 'LABOR';
                item.OrderQuantity = warrantyClaim.laborHours;
            }

            if (warrantyClaim.travelHours != null && warrantyClaim.travelHours > 0) {
                item = new SBO_EnosixSO_Detail.ITEMS();
                salesOrder.ITEMS.add(item);
                item.ItemNumber = '40';
                item.Material = 'TRAVEL';
                item.OrderQuantity = warrantyClaim.travelHours;
            }

            if (warrantyClaim.travelMiles != null && warrantyClaim.travelMiles > 0) {
                item = new SBO_EnosixSO_Detail.ITEMS();
                salesOrder.ITEMS.add(item);
                item.ItemNumber = '50';
                item.Material = 'MILEAGE';
                item.OrderQuantity = warrantyClaim.travelMiles;
            }

            if (warrantyClaim.otherExpenses != null && warrantyClaim.otherExpenses > 0) {
                item = new SBO_EnosixSO_Detail.ITEMS();
                salesOrder.ITEMS.add(item);
                item.ItemNumber = '60';
                item.Material = 'EXPENSES';
                item.OrderQuantity = warrantyClaim.otherExpenses;
            }

            if (warrantyClaim.localMaterial != null && warrantyClaim.localMaterial > 0) {
                item = new SBO_EnosixSO_Detail.ITEMS();
                salesOrder.ITEMS.add(item);
                item.ItemNumber = '70';
                item.Material = 'LOCAL-MAT';
                item.OrderQuantity = warrantyClaim.localMaterial;
            }

            if (warrantyClaim.freight != null && warrantyClaim.freight > 0) {
                item = new SBO_EnosixSO_Detail.ITEMS();
                salesOrder.ITEMS.add(item);
                item.ItemNumber = '80';
                item.Material = 'FREIGHT';
                item.OrderQuantity = warrantyClaim.freight;
            }

            if (String.isNotBlank(warrantyClaim.replacementPartNumber)) {
                item = new SBO_EnosixSO_Detail.ITEMS();
                salesOrder.ITEMS.add(item);
                item.ItemNumber = '90';
                item.Material = warrantyClaim.replacementPartNumber;
                item.SerialNumber = warrantyClaim.replacementSerialNumber;
                item.OrderQuantity = 1;

                item.ItemDescription =
                    leftPad(warrantyClaim.replacementSerialNumber, 7) +
                    '*' +
                    leftPad(warrantyClaim.replacementGDInvoiceItemNumber, 6) +
                    '*' +
                    leftPad(warrantyClaim.replacementGDInvoiceNumber, 10, '0');

                if (!String.isBlank(warrantyClaim.replacementGDInvoiceNumber)) {
                    item.ItemReferenceDocument = leftPad(warrantyClaim.replacementGDInvoiceNumber, 10, '0');
                    item.ItemReferenceDocumentItNumber = warrantyClaim.replacementGDInvoiceItemNumber;
                    item.ItemReferenceDocumentCtegory = 'M';
                }
            }

            if (String.isNotBlank(warrantyClaim.itemJSON)) {
                System.debug('itemJSON=' + warrantyClaim.itemJSON);
                if (warrantyClaim.itemJSON.startsWith('"')) {
                    warrantyClaim.itemJSON = warrantyClaim.itemJSON.substring(1, warrantyClaim.itemJSON.length() - 1);
                    System.debug('itemJSON1=' + warrantyClaim.itemJSON);

                    warrantyClaim.itemJSON = warrantyClaim.itemJSON.replaceAll('\\\\', '');
                    System.debug('itemJSON2=' + warrantyClaim.itemJSON);
                }

                List<OBJ_WarrantyClaimItem> items = null;
                items = (List<OBJ_WarrantyClaimItem>) JSON.deserialize(warrantyClaim.itemJSON, List<OBJ_WarrantyClaimItem>.class);

                if (items != null) {
                    Integer itemNumber = 100;
                    for (OBJ_WarrantyClaimItem oItem : items) {
                        if (String.isBlank(oItem.PartNo)) {
                            continue;
                        }

                        item = new SBO_EnosixSO_Detail.ITEMS();
                        salesOrder.ITEMS.add(item);
                        item.ItemNumber = String.valueOf(itemNumber);
                        item.Material = oItem.PartNo;
                        item.OrderQuantity = oItem.Quantity == null ? 1 : oItem.Quantity;

                        if (!String.isBlank(oItem.GDInvoiceNumber)) {
                            item.GDInvoice = leftPad(oItem.GDInvoiceNumber, 10, '0');
                            item.GDInvoiceItem = oItem.GDInvoiceItemNumber;
                            item.ItemReferenceDocument = leftPad(oItem.GDInvoiceNumber, 10, '0');
                            item.ItemReferenceDocumentItNumber = oItem.GDInvoiceItemNumber;
                            item.ItemReferenceDocumentCtegory = 'M';

                            item.ItemDescription = leftPad(oItem.GDInvoiceItemNumber, 6) + '*' + leftPad(oItem.GDInvoiceNumber, 10, '0');
                        }

                        itemNumber += 10;
                    }
                }
            }

            if (String.isNotBlank(warrantyClaim.locationNameLine1)) {
                String county = warrantyClaim.locationCounty;

                if (String.isBlank(warrantyClaim.locationCounty)) {
                    RFC_Z_ENSX_GET_TAX_JURISDICTION rfc = new RFC_Z_ENSX_GET_TAX_JURISDICTION();
                    rfc.PARAMS.IV_CITY = warrantyClaim.locationCity;
                    rfc.PARAMS.IV_COUNTRY = warrantyClaim.locationCountry;
                    rfc.PARAMS.IV_REGION = warrantyClaim.locationRegion;
                    rfc.PARAMS.IV_ZIPCODE = warrantyClaim.locationPostalCode;
                    RFC_Z_ENSX_GET_TAX_JURISDICTION.RESULT rfcResult = rfc.execute();
                    List<RFC_Z_ENSX_GET_TAX_JURISDICTION.ET_LOCATION_RESULTS> jurisdictions = rfcResult.ET_LOCATION_RESULTS_List;

                    if (jurisdictions != null && !jurisdictions.isEmpty()) {
                        county = jurisdictions.get(0).COUNTY;
                    }
                }

                SBO_EnosixSO_Detail.PARTNERS shipToPtnr = new SBO_EnosixSO_Detail.PARTNERS();
                shipToPtnr.PartnerFunction = UTIL_Customer.SHIP_TO_PARTNER_CODE;
                shipToPtnr.CustomerNumber = 'DROP-SHIP';
                shipToPtnr.PartnerName = warrantyClaim.locationNameLine1;
                shipToPtnr.Street = warrantyClaim.locationStreet;
                shipToPtnr.City = warrantyClaim.locationCity;
                if (warrantyClaim.locationCountry == 'US' || warrantyClaim.locationCountry == 'CA') {
                    shipToPtnr.Region = warrantyClaim.locationRegion;
                    shipToPtnr.District = county;
                }
                shipToPtnr.PostalCode = warrantyClaim.locationPostalCode;
                shipToPtnr.Country = warrantyClaim.locationCountry;
                shipToPtnr.TelephoneNumber = warrantyClaim.locationPhone;
                salesOrder.PARTNERS.add(shipToPtnr);
            }

            SBO_EnosixSO_Detail sbo = new SBO_EnosixSO_Detail();

            salesOrder = sbo.command('CMD_SIMULATE_ORDER', salesOrder);

            SBO_EnosixSO_Detail.TEXTS note1 = new SBO_EnosixSO_Detail.TEXTS();
            note1.TextID = 'ZINT';
            note1.TextLanguage = 'E';
            note1.TextIDDescription = 'Internal X-Note';
            if (String.isNotBlank(warrantyClaim.claimText)) {
                note1.Text = warrantyClaim.claimText;
            } else {
                note1.Text = '';
            }
            salesOrder.TEXTS.add(note1);

            if (salesOrder.getMessages() != null) {
                if (myAddFrameworkMessages(salesOrder, salesOrder.getMessages())) {
                    return UTIL_Aura.createResponse(null);
                }
            }

            if (saveToo) {
                if (serialNumber != null) {
                    salesOrder.EquipmentNumber = serialNumber.Equipment_Number__c;
                }

                //fix for truncation
                if (String.isNotBlank(warrantyClaim.customerReferenceNumber)) {
                    salesOrder.CustomerPurchaseOrderNumber = warrantyClaim.customerReferenceNumber;
                }

                return saveSalesOrder(salesOrder);
            }

            return UTIL_Aura.createResponse(salesOrder);
        } catch (Exception e) {
            logger.error(e);

            UTIL_PageMessages.addExceptionMessage(e);
        } finally {
            logger.exit();
        }

        return UTIL_Aura.createResponse(null);
    }

    private static String leftPad(String input, Integer length) {
        return leftPad(input, length, ' ');
    }

    private static String leftPad(String input, Integer length, String padStr) {
        if (input == null) {
            return ''.leftPad(length, padStr);
        }

        return input.leftPad(length, padStr);
    }

    @AuraEnabled
    public static UTIL_Aura.Response saveSalesOrder(SBO_EnosixSO_Detail.EnosixSO salesOrder) {
        logger.enterAura('saveSalesOrder', new Map<String, Object>{ 'salesOrder' => salesOrder });

        SBO_EnosixSO_Detail sbo = new SBO_EnosixSO_Detail();

        try {
            salesOrder = sbo.save(salesOrder);

            if (salesOrder.getMessages() != null) {
                myAddFrameworkMessages(salesOrder.getMessages());
            }

            checkAndNotifyOfSalesOrderCreationErrors(salesOrder);

            return UTIL_Aura.createResponse(salesOrder);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    @AuraEnabled
    public static UTIL_Aura.Response updateServiceNotification(String sapSalesOrderNumber) {
        logger.enterAura('updateServiceNotification', new Map<String, Object>{ 'sapSalesOrderNumber' => sapSalesOrderNumber });

        SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate serviceNotificationUpdate = new SBO_EnosixServNotifUpdate_Detail.EnosixServNotifUpdate();

        serviceNotificationUpdate.SalesDocument = sapSalesOrderNumber;

        SBO_EnosixServNotifUpdate_Detail sbo = new SBO_EnosixServNotifUpdate_Detail();

        try {
            serviceNotificationUpdate = sbo.save(serviceNotificationUpdate);

            if (serviceNotificationUpdate.getMessages() != null) {
                myAddFrameworkMessages(serviceNotificationUpdate.getMessages());
            }

            return UTIL_Aura.createResponse(serviceNotificationUpdate);
        } catch (Exception e) {
            logger.error(e);

            throw e;
        } finally {
            logger.exit();
        }
    }

    private static void checkAndNotifyOfSalesOrderCreationErrors(SBO_EnosixSO_Detail.EnosixSO salesOrder) {
        boolean hasErrors = false;

        for (UTIL_PageMessages.Message message : UTIL_PageMessages.messageList) {
            if (message.messageType == UTIL_PageMessages.ERROR) {
                hasErrors = true;
            }
        }

        if (hasErrors) {
            String userName = UserInfo.getUserName();
            User activeUser = [SELECT Email FROM User WHERE Username = :userName LIMIT 1];
            String userEmail = activeUser.Email;

            String[] toAddresses = new List<String>{
                (String) UTIL_AppSettings.getValue('CTRL_WarrantyClaimForm.GhostClaimEmail', activeUser.Email)
            };

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            mail.setToAddresses(toAddresses);
            mail.setSubject('Possible Ghost Claim');
            String body = 'Here\'s the info:';
            for (UTIL_PageMessages.Message message : UTIL_PageMessages.messageList) {
                body += '\n' + message.messageType + ' : ' + message.message;
            }

            mail.setPlainTextBody(body);

            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });
        }
    }

    @TestVisible
    private static void myAddFrameworkMessages(List<ensxsdk.EnosixFramework.Message> messages) {
        myAddFrameworkMessages(null, messages);
    }

    @TestVisible
    private static Boolean myAddFrameworkMessages(SBO_EnosixSO_Detail.EnosixSO salesOrder, List<ensxsdk.EnosixFramework.Message> messages) {
        if (null == messages)
            return true;

        Boolean hasErrors = false;

        for (ensxsdk.EnosixFramework.Message f : messages) {
            UTIL_PageMessages.Message message = new UTIL_PageMessages.Message(f);
            if (message.messageType == UTIL_PageMessages.ERROR) {
                hasErrors = true;
            }

            UTIL_PageMessages.messageList.add(message);
        }

        //append message from simulate order to claim text
        if (salesOrder != null) {
            SBO_EnosixSO_Detail.TEXTS text = null;
            List<SBO_EnosixSO_Detail.TEXTS> texts = salesOrder.TEXTS.getAsList();
            if (texts.size() == 1) {
                text = texts.get(0);
            }
            if (text != null) {
                for (UTIL_PageMessages.Message message : UTIL_PageMessages.messageList) {
                    text.Text += '\n' + message.messageType + ': ' + message.message;
                }
            }
        }

        return hasErrors;
    }

    private static Account getAccount(String accountId) {
        return [SELECT Id, AccountNumber FROM Account WHERE Id = :accountId LIMIT 1];
    }

    private static Asset getSerialNumber(String serialNumberId) {
        try {
            return [SELECT Id, Name, SOrg__c, DChl__c, Equipment_Number__c, Model_Number__c FROM Asset WHERE Id = :serialNumberId LIMIT 1];
        } catch (Exception e) {
        }

        return null;
    }

    public class OBJ_WarrantyClaim {
        @AuraEnabled
        public Boolean partsClaim;
        @AuraEnabled
        public String accountId;
        @AuraEnabled
        public String serialNumberId;
        @AuraEnabled
        public String serialNumber;
        @AuraEnabled
        public Date dateOfMalfunction;
        @AuraEnabled
        public Date serviceDate;
        @AuraEnabled
        public String modelNumber;
        @AuraEnabled
        public String salesOrg;
        @AuraEnabled
        public String distributionChannel;
        @AuraEnabled
        public String sapServiceNotificationNumber;
        @AuraEnabled
        public String customerReferenceNumber;
        @AuraEnabled
        public String serviceTechnicianNumber;
        @AuraEnabled
        public String hoursInService;
        @AuraEnabled
        public String failedPartNumber;
        @AuraEnabled
        public String failedSerialNumber;
        @AuraEnabled
        public Integer failedHoursInService;
        @AuraEnabled
        public Date failedStartupDate;
        @AuraEnabled
        public Double laborHours;
        @AuraEnabled
        public Double travelHours;
        @AuraEnabled
        public Integer travelMiles;
        @AuraEnabled
        public Double freight;
        @AuraEnabled
        public Double localMaterial;
        @AuraEnabled
        public Double otherExpenses;
        @AuraEnabled
        public String replacementPartNumber;
        @AuraEnabled
        public String replacementSerialNumber;
        @AuraEnabled
        public String replacementGDInvoiceNumber;
        @AuraEnabled
        public String replacementGDInvoiceItemNumber;
        @AuraEnabled
        public String itemJSON;
        @AuraEnabled
        public String claimText;
        @AuraEnabled
        public String locationNameLine1;
        @AuraEnabled
        public String locationNameLine2;
        @AuraEnabled
        public String locationStreet;
        @AuraEnabled
        public String locationCity;
        @AuraEnabled
        public String locationPostalCode;
        @AuraEnabled
        public String locationRegion;
        @AuraEnabled
        public String locationCountry;
        @AuraEnabled
        public String locationCounty;
        @AuraEnabled
        public String locationPhone;
        @AuraEnabled
        public String locationFax;
    }
}