/*=========================================================================================================
* @author Geethanjali SP, Capgemini
* @date 31/05/2022
* @description:  Apex Handler class for ReturnOrder
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/

public class SFS_ReturnOrderTriggerHandler {
    
    @future
    public static void createCase(List<Id> roList){
        
        System.debug('Id:'+roList);
        List<Case> caseList=new List<Case>();
        String caseRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('IR Comp Non-Warranty Claims').getRecordTypeId();
        List<ReturnOrder> returnOrderDetails=[select id,SFS_WorkOrder__r.AccountId,SFS_WorkOrder__r.ContactId, SFS_WorkOrder__r.Account.Account_Division__c,SFS_WorkOrder__r.Account.Country__c, SFS_WorkOrder__r.Account.Region__c 
                                              from ReturnOrder where Id IN:roList];
        
        if(returnOrderDetails.size()>0){ 
            for(ReturnOrder ro : returnOrderDetails){                                
                Case caseRecord=new Case(AccountId=ro.SFS_WorkOrder__r.AccountId,
                	ContactId=ro.SFS_WorkOrder__r.ContactId,RecordTypeId=caseRecordTypeID,Division__c=ro.SFS_WorkOrder__r.Account.Account_Division__c,
                    CTS_OM_Country__c=ro.SFS_WorkOrder__r.Account.Country__c,CTS_Region__c=ro.SFS_WorkOrder__r.Account.Region__c);
        		caseList.add(caseRecord);
            }
        }
        
        if(caseList.size()>0){
                Database.insert(caseList);
        }
        
    }

}