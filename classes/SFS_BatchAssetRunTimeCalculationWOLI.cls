public class SFS_BatchAssetRunTimeCalculationWOLI implements Database.batchable<sObject>{
	public Database.QueryLocator start(database.BatchableContext bc) {
       DateTime yesterdayTime=System.today()-2;
       DateTime todayTime=System.today();      
       String Query = 'Select Id,Name,SFS_WOLI_Last_Completed_Date__c from Asset where (SFS_WOLI_Last_Completed_Date__c>:yesterdayTime and SFS_WOLI_Last_Completed_Date__c <=:todayTime)';    
       return Database.getQueryLocator(Query); 
       
    } 
    public void execute(Database.BatchableContext bc, List<Asset> scope){
        try{
            
            Map<Id,Integer> CountMap=new Map<Id,Integer>();
            Set<Id> assetIds=new Set<Id>();
            for(Asset record:scope){
                assetIds.add(record.Id);
            }
            List<WorkOrderLineItem> woliList=[Select Id,LineItemNumber, AssetId,SFS_Asset_Run_Hours__c,WorkOrder.MaintenancePlanId,SFS_Completed_Date__c
                                              from WorkOrderLineItem where SFS_Asset_Run_Hours__c!=0 AND SFS_Asset_Run_Hours__c!=1 and 
                                              SFS_Completed_Date__c!=null and (Status='Completed' or Status='Closed') and AssetId IN :assetIds order by AssetId,SFS_Completed_Date__c desc];

            List<Asset> assetList=new List<Asset>();
            for(Asset record:scope){
                Integer runRate;
                Date svcCompletedDate1;
                DateTime svcCompleteDateTime;
                Id svcLatest1,checkMP;
                Integer runHours1;
                for(WorkOrderLineItem woli:woliList){
                    if(woli.AssetId==record.Id){
                        Integer countWOLI=CountMap.get(record.Id);
                        if(countWOLI==null)
                            countWOLI=0;
                        countWOLI+=1;
                        CountMap.put(record.Id,countWOLI);
                        if(svcCompletedDate1==null){
                        	svcCompletedDate1=Date.valueOf(woli.SFS_Completed_Date__c);
                            svcCompleteDateTime=woli.SFS_Completed_Date__c;
                        	svcLatest1=woli.Id;
                        	runHours1=Integer.valueOf(woli.SFS_Asset_Run_Hours__c);
                            checkMP=woli.WorkOrder.MaintenancePlanId;
                    	}
                    	if(svcCompletedDate1<Date.valueOf(woli.SFS_Completed_Date__c)){
                        	svcCompletedDate1=Date.valueOf(woli.SFS_Completed_Date__c);
                            svcCompleteDateTime=woli.SFS_Completed_Date__c;
                        	svcLatest1=woli.Id;
                            checkMP=woli.WorkOrder.MaintenancePlanId;
                        	if(woli.SFS_Asset_Run_Hours__c!=null){
                            	runHours1=Integer.valueOf(woli.SFS_Asset_Run_Hours__c);
                        	}
                    	}
                    }
                }
                if(svcCompletedDate1!=null){
                Date checkPrevious11=svcCompletedDate1.addMonths(-12);
                Date svcCompletedDate2,PreviouscomplDate;
                Id svcLatest2,PreviousId;
                Integer runHours2,PreviousrunHours,PreviousrunHours1;
                for(WorkOrderLineItem woli:woliList){
                    if(woli.AssetId==record.Id){
                        Integer countWOLI=CountMap.get(record.Id);
                        if(countWOLI==1)
                            break;
                        if(countWOLI==2){
                            if(woli.Id!=svcLatest1){
                                svcCompletedDate2=Date.valueOf(woli.SFS_Completed_Date__c);
                           		svcLatest2=woli.Id;
                            	runHours2=Integer.valueOf(woli.SFS_Asset_Run_Hours__c);
                            	break;
                            }
                        }
                        else if(Date.valueOf(woli.SFS_Completed_Date__c)<checkPrevious11){
                            Integer DateDiffoNearest1=math.abs((Date.valueOf(checkPrevious11).daysbetween(Date.valueof(woli.SFS_Completed_Date__c))));
                            Integer DateDiffoNearest2=math.abs((Date.valueOf(checkPrevious11).daysbetween(Date.valueof(PreviouscomplDate))));
                            if(DateDiffoNearest1>DateDiffoNearest2){
                                svcCompletedDate2=PreviouscomplDate;
                            	svcLatest2=PreviousId;
                            	runHours2=PreviousrunHours1;
                            }else{
                                svcCompletedDate2=Date.valueOf(woli.SFS_Completed_Date__c);
                            	svcLatest2=woli.Id;
                            	runHours2=Integer.valueOf(woli.SFS_Asset_Run_Hours__c);
                            }
                            break;
                    	}
                        PreviousrunHours1=Integer.valueOf(woli.SFS_Asset_Run_Hours__c);
                        PreviouscomplDate=Date.valueOf(woli.SFS_Completed_Date__c);
                        PreviousId=woli.Id;
                    }
                }
                if(svcLatest2==null){
                   svcCompletedDate2=PreviouscomplDate;
                   svcLatest2=PreviousId;
                   runHours2=PreviousrunHours1;
                }
                 system.debug('WOLI 1 '+svcLatest1 + '  RR1 '+runHours1+' WOLI 2 '+svcLatest2+ '  RR2 '+runHours2);
                if(svcCompletedDate1!=null && svcCompletedDate2!=null && runHours2!=null && runHours1!=null){
                    Integer DateDiff=math.abs((Date.valueOf(svcCompletedDate1).daysbetween(Date.valueof(svcCompletedDate2))));
                    if(DateDiff!=0) 
                      runRate=((runHours1-runHours2)*365)/DateDiff;
                }
                Date svcCompletedDate3,PreviousCompletedDate;
                Id svcLatest3,PreviousWOLI;
                if(svcLatest2==null || (svcLatest2!=null && (runRate<0 || runRate>8760 || runRate==null))){
                    for(WorkOrderLineItem woli:woliList){
                        if(woli.AssetId==record.Id){
                            Integer countWOLI=CountMap.get(record.Id);
                        	if(countWOLI==1)
                            	break;
                            if(Date.valueOf(woli.SFS_Completed_Date__c)>checkPrevious11 && Date.valueOf(woli.SFS_Completed_Date__c)<svcCompletedDate1){
                            	if(svcCompletedDate3==null){
                                	svcCompletedDate3=Date.valueOf(woli.SFS_Completed_Date__c);
                                	svcLatest3=woli.Id;
                                	if(woli.SFS_Asset_Run_Hours__c!=null){
                                    	runHours2=Integer.valueOf(woli.SFS_Asset_Run_Hours__c);    
                                	}
                            	}
                            	if(svcCompletedDate3>Date.valueOf(woli.SFS_Completed_Date__c)){
                                	PreviousCompletedDate=svcCompletedDate3;                               
                                	PreviousWOLI=svcLatest3;
                                	PreviousrunHours=runHours2;
                                	svcCompletedDate3=Date.valueOf(woli.SFS_Completed_Date__c);
                                	svcLatest3=woli.Id;
                                	if(woli.SFS_Asset_Run_Hours__c!=null){
                                    	runHours2=Integer.valueOf(woli.SFS_Asset_Run_Hours__c); 
                                	}                               
                            	}
                        	}
                        }
                    }
                }

                if(svcCompletedDate1 !=null && svcCompletedDate3!=null && runHours2!=null && runHours1!=null){
                    Integer DateDiff1=math.abs((Date.valueOf(svcCompletedDate1).daysbetween(Date.valueof(svcCompletedDate3))));
                    if(DateDiff1!=0)
                       runRate=((runHours1-runHours2)*365)/DateDiff1;
                }
                
                if((runRate<0 || runRate>8760 || runRate==null) && svcCompletedDate1 !=null && PreviousCompletedDate!=null && PreviousrunHours!=null && runHours1!=null){
                    Integer DateDiff2=math.abs((Date.valueOf(svcCompletedDate1).daysbetween(Date.valueof(PreviousCompletedDate))));
                    if(DateDiff2!=0)
                      runRate=((runHours1-PreviousrunHours)*365)/DateDiff2;
                }
                if(runRate>=0 && runRate<=8760){
                    record.IRIT_Run_Rate__c=runRate; 
                }
                record.IRIT_Last_Hours_on_Asset__c=runHours1;
                record.CTS_Last_Hours_on_Asset_Date__c =svcCompletedDate1;
                if(checkMP!=null)
                	record.IRIT_Last_PM_Date__c =svcCompleteDateTime;
               	assetList.add(record);
            }
                
            }
            update assetList; 

        }Catch(Exception e){
        }
    }
    public void finish(Database.BatchableContext bc){
      }
}