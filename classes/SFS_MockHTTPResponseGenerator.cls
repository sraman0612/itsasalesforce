@isTest
global class SFS_MockHTTPResponseGenerator implements HttpCalloutMock{
    
    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);
        return res;
        
    }
    
}