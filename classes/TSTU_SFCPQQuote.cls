/*
* Test cases for Quote utility class
*/
@isTest
public class TSTU_SFCPQQuote
{
    @isTest
    public static void test_getSObject()
    {
        SObject testSObject = createTestObjects();

        Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        utility.getSObject('bad Id');
        utility.getSObject(testSObject.Id);
        utility.getSObject(testSObject.Id);
        utility.getSObject('Quote', 'sapQuoteNumber');
        utility.getSObject('Order', 'sapOrderNumber');
        utility.getSObject('bad SAP Type', 'bad SAP Type');
        Test.stopTest();
    }

   @isTest
    static void test_getSObjectLineItems()
    {
        Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
        utility.getSObjectLineItems('bad Id');
        utility.getSObjectLineItems(testSObject.Id);
        utility.getSObjectLineItems(testSObject.Id);
        Test.stopTest();
    }

    @isTest
    static void test_getAccountId()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
		utility.getAccountId(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getName()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
		utility.getName(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getQuoteNumber()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
		utility.getQuoteNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getOrderNumber()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
		utility.getOrderNumber(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getOpportunity()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
		utility.getOpportunity(testSObject);
		Test.stopTest();
    }

    @isTest
    static void test_getPriceBookId()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
		utility.getPriceBookId(null);
		Test.stopTest();
    }

    @isTest
    static void test_getProductId()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
		utility.getProductId(null);
		Test.stopTest();
    }

    @isTest
    static void test_getMaterial()
    {
    	Test.startTest();
        SObject testSObject = createTestObjects();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
		utility.getMaterial(testSObject, null);
		Test.stopTest();
    }

    @isTest
    static void test_getItemNumber()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
		utility.getItemNumber(null);
		Test.stopTest();
    }

    @isTest
    static void test_initializeQuoteFromSfSObject()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
        SBO_EnosixQuote_Detail.EnosixQuote quoteDetail = createQuote();
		utility.initializeQuoteFromSfSObject('', testSObject, quoteDetail, new Map<String, UTIL_SFSObjectDoc.SfSObjectItem>(), new Map<String, UTIL_Quote.QuoteLineValue>(), 10);
		Test.stopTest();
    }

    @isTest
    static void test_TranslateLineItemToQuoteItem()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SBQQ__QuoteLine__c lineItem = new SBQQ__QuoteLine__c();
		utility.translateLineItemToQuoteItem(lineItem, null);
		utility.translateLineItemToQuoteItem(lineItem, 'MaterialNumber');
		Test.stopTest();
    }

    @isTest
    static void test_finalizeQuoteAndUpdateSfsObject()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
        SBO_EnosixQuote_Detail.EnosixQuote quoteDetail = createQuote();
        quoteDetail.ITEMS.clear();
		utility.finalizeQuoteAndUpdateSfsObject('', testSObject, quoteDetail,
            new Map<String, UTIL_SFSObjectDoc.SfSObjectItem>(), UTIL_Pricebook.getStandardPriceBookId(),
            new List<SBO_EnosixQuote_Detail.ITEMS>(), new Map<string, Id>(), new Map<Id, PricebookEntry>());
		Test.stopTest();
    }

    @isTest public static void test_setOppItemsFromQuote()
    {
        SBQQ__Quote__c testSObject = createTestObjects();

        SBO_EnosixQuote_Detail.EnosixQuote quoteDetail = createQuote();

        Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();

        Map<string, Id> materialToProductIdMap = new Map<string, Id>();
        Map<Id, PricebookEntry> productToPricebookEntryMap = new Map<Id, PricebookEntry>();
        Product2 product1 = new Product2(
            Description = 'Material1',
            Name = 'Material1',
            ProductCode = 'Material1',
            IsActive = true
        );
        UTIL_SFProduct.setProductMaterialNumber(product1, 'Material1');
        insert product1;
        PricebookEntry pbe1 = new PricebookEntry(
            Pricebook2Id = UTIL_Pricebook.getStandardPriceBookId(),
            Product2Id = product1.Id,
            UnitPrice = 0,
            IsActive = true,
            UseStandardPrice = false
        );
        insert pbe1;
        materialToProductIdMap.put('Material1', product1.Id);
        productToPricebookEntryMap.put(product1.Id, pbe1);

        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap = new Map<String, UTIL_SFSObjectDoc.SfSObjectItem>();
        UTIL_SFSObjectDoc.SfSObjectItem sfSobjectItem1 = new UTIL_SFSObjectDoc.SfSObjectItem(lineId);
        sfSobjectItem1.isDeleted = true;
        sfSObjectLineIdMap.put('1',sfSobjectItem1);
        utility.upsertLineItemsFromQuoteItems(testSObject, quoteDetail.ITEMS.getAsList(), materialToProductIdMap, productToPricebookEntryMap, sfSObjectLineIdMap);
        Test.stopTest();
    }

    @isTest
    static void test_initializeOrderFromSfSObject()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
        SBO_EnosixSO_Detail.EnosixSO orderDetail = createOrder();
		utility.initializeOrderFromSfSObject('', testSObject, orderDetail, new Map<String, UTIL_SFSObjectDoc.SfSObjectItem>(), new Map<String, UTIL_Order.OrderLineValue>(), 10);
		Test.stopTest();
    }

    @isTest
    static void test_TranslateLineItemToOrderItem()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SBQQ__QuoteLine__c lineItem = new SBQQ__QuoteLine__c();
		utility.translateLineItemToOrderItem(lineItem, null);
		utility.translateLineItemToOrderItem(lineItem, 'MaterialNumber');
		Test.stopTest();
    }

    @isTest
    static void test_finalizeOrderAndUpdateSfsObject()
    {
    	Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();
        SObject testSObject = createTestObjects();
        SBO_EnosixSO_Detail.EnosixSO orderDetail = createOrder();
        orderDetail.ITEMS.clear();
		utility.finalizeOrderAndUpdateSfsObject('', testSObject, orderDetail,
            new Map<String, UTIL_SFSObjectDoc.SfSObjectItem>(), UTIL_Pricebook.getStandardPriceBookId(),
            new List<SBO_EnosixSO_Detail.ITEMS>(), new Map<string, Id>(), new Map<Id, PricebookEntry>());
		Test.stopTest();
    }

    @isTest public static void test_setOppItemsFromOrder()
    {
        SBQQ__Quote__c testSObject = createTestObjects();

        SBO_EnosixSO_Detail.EnosixSO orderDetail = createOrder();

        Test.startTest();
        UTIL_SFCPQQuote utility = new UTIL_SFCPQQuote();

        Map<string, Id> materialToProductIdMap = new Map<string, Id>();
        Map<Id, PricebookEntry> productToPricebookEntryMap = new Map<Id, PricebookEntry>();
        Product2 product1 = new Product2(
            Description = 'Material1',
            Name = 'Material1',
            ProductCode = 'Material1',
            IsActive = true
        );
        UTIL_SFProduct.setProductMaterialNumber(product1, 'Material1');
        insert product1;
        PricebookEntry pbe1 = new PricebookEntry(
            Pricebook2Id = UTIL_Pricebook.getStandardPriceBookId(),
            Product2Id = product1.Id,
            UnitPrice = 0,
            IsActive = true,
            UseStandardPrice = false
        );
        insert pbe1;
        materialToProductIdMap.put('Material1', product1.Id);
        productToPricebookEntryMap.put(product1.Id, pbe1);

        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap = new Map<String, UTIL_SFSObjectDoc.SfSObjectItem>();
        UTIL_SFSObjectDoc.SfSObjectItem sfSobjectItem1 = new UTIL_SFSObjectDoc.SfSObjectItem(lineId);
        sfSobjectItem1.isDeleted = true;
        sfSObjectLineIdMap.put('1',sfSobjectItem1);
        utility.upsertLineItemsFromOrderItems(testSObject, orderDetail.ITEMS.getAsList(), materialToProductIdMap, productToPricebookEntryMap, sfSObjectLineIdMap);
        Test.stopTest();
    }

    private static SBO_EnosixQuote_Detail.EnosixQuote createQuote()
    {
        SBO_EnosixQuote_Detail.EnosixQuote result = new SBO_EnosixQuote_Detail.EnosixQuote();

        SBO_EnosixQuote_Detail.ITEMS item1 = new SBO_EnosixQuote_Detail.ITEMS();
        item1.ItemNumber = '1';
        item1.ItemDescription = 'Item 1';
        item1.Material = 'Material1';
        item1.OrderQuantity = 1;
        item1.NetItemPrice = 10.00;
        item1.ScheduleLineDate = Date.newInstance(2017, 3, 17);

        result.ITEMS.add(item1);

        return result;
    }

    private static SBO_EnosixSO_Detail.EnosixSO createOrder()
    {
        SBO_EnosixSO_Detail.EnosixSO result = new SBO_EnosixSO_Detail.EnosixSO();

        SBO_EnosixSO_Detail.ITEMS item1 = new SBO_EnosixSO_Detail.ITEMS();
        item1.ItemNumber = '1';
        item1.ItemDescription = 'Item 1';
        item1.Material = 'Material1';
        item1.OrderQuantity = 1;
        item1.NetItemPrice = 10.00;
        item1.ScheduleLineDate = Date.newInstance(2017, 3, 17);

        result.ITEMS.add(item1);

        return result;
    }

    private static string lineId = null;

    private static SBQQ__Quote__c createTestObjects()
    {
        Account acct = TSTU_SFAccount.createTestAccount();
    	acct.Name='Acme';
    	acct.BillingCity='Cincinnatti';
        acct.put(UTIL_SFAccount.CustomerFieldName,'CustNum');
    	insert acct;

        Id pricebookId = UTIL_Pricebook.getStandardPriceBookId();
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'TEST OPP';
        opp.CloseDate = Date.today();
        opp.StageName = 'IsWon';
        opp.Pricebook2Id = pricebookId;
        opp.AccountId = acct.Id;
        TSTU_SFOpportunity.upsertOpportunity(opp);
        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :opp.Id];
        opp.Pricebook2Id = pricebookId;
        TSTU_SFOpportunity.upsertOpportunity(opp);

        SBQQ__Quote__c quote = createTestQuote();
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__PriceBook__c = pricebookId;
        quote.SAP_Configuration__c =
            '{"soldToParty":"soldToParty",' +
            '"salesOrg":"salesOrg",' +
            '"distChannel":"distChannel",' +
            '"division":"division"}';
        // quote.SBQQ__PrimaryContact__c = ;
        quote.SBQQ__Account__c = acct.Id;
        quote.SBQQ__Status__c = 'Draft';
        quote.My_Quote_Terms_and_Conditions__c = 'first';
        // quote.End_Customer_Account__c = ;
        // quote.SBQQ__Partner__c = ;
        // quote.SBQQ__Distributor__c = ;
        upsert quote;
        quote = [SELECT Id, SBQQ__Opportunity2__c, SBQQ__Account__c, FLD_SAP_Quote_Number__c, SAP_Configuration__c, FLD_enosixPricingSimulationEnabled__c, FLD_SAP_Order_Type__c FROM SBQQ__Quote__c WHERE Id = :quote.Id];
        quote.SBQQ__PriceBook__c = pricebookId;
        upsert quote;

        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        newProd.put(UTIL_SFProduct.MaterialFieldName,'materialNumber');
        insert newProd;

        PriceBookEntry standardPbe = new PriceBookEntry();
        standardPbe.UnitPrice = 100;
        standardPbe.Pricebook2Id = pricebookId;
        standardPbe.Product2Id = newProd.Id;
        standardPbe.UseStandardPrice = false;
        standardPbe.IsActive = true;
        insert standardPbe;

       SBQQ__QuoteLine__c qli = createTestQuoteLine();
		qli.SBQQ__Quote__c = quote.Id;
        qli.SBQQ__Product__c = newProd.Id;
		qli.SBQQ__Quantity__c = 10;
		qli.SBQQ__ListPrice__c =0.95;
		qli.SBQQ__Description__c = 'test Desciption';
        qli.SBQQ__PricebookEntryId__c = standardPbe.Id;
        qli.SBQQ__AdditionalDiscountAmount__c = 10;
        qli.SBQQ__MarkupAmount__c = 5;
        qli.SBQQ__NetPrice__c = .95;
        /*qli.SAP_Configuration__c =
            '{"plant":"plant",' +
            '"OrderQuantity":1,' +
            '"selectedCharacteristics":[{"CharacteristicDescription":"Horsepower","CharacteristicValueDescription":"100 HORSEPOWER"}]}';*/
            insert qli;

        lineId = qli.Id;

        return quote;
    }

    public static SBQQ__Quote__c createTestQuote()
    {
        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.Distribution_Channel__c = 'SB';
        return quote;
    }

    public static SBQQ__QuoteLine__c createTestQuoteLine()
    {
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        return quoteLine;
    }
}