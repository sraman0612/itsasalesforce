/**
* @author           Amit Datta
* @description      manage All logic to support the Recommended Products.
*
* Modification Log
* ------------------------------------------------------------------------------------------
*         Developer                   Date                Description
* ------------------------------------------------------------------------------------------
*         Amit Datta                  14/01/2024          Original Version
**/

public with sharing class RecommendedProducts {
    
    private static final String IRB2BWEBSTOREURLPATHPREFIX = 'IRB2BWebstoreUrlPathPrefix';
    private static final String IRB2BWEBSTOREURL = B2B_IR_Store_Setting__mdt.getInstance('IRB2BWebstoreUrl')?.value__c;
    private static final String IRLOGOURL = B2B_IR_Store_Setting__mdt.getInstance('IR_logo_CMSUrl')?.value__c;
    
    public class CartItemWrapper{
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String recId{get;set;}
        @AuraEnabled public String stockKeepingUnit{get;set;}
        @AuraEnabled public String description{get;set;}
        @AuraEnabled public String compressorAirFilter{get;set;}
        @AuraEnabled public String maintenanceInterval{get;set;}
        @AuraEnabled public String fullUrl{get;set;}
        @AuraEnabled public String defaultImageUrl{get;set;}
        @AuraEnabled public String defaultImageTitle{get;set;}
        @AuraEnabled public String unitPrice{get;set;}
        @AuraEnabled public String currencyIsoCode{get;set;}
        @AuraEnabled public String quantity{get;set;}
        @AuraEnabled public String accountId{get;set;}
        @AuraEnabled public String webStoreId{get;set;}
    }
    
    @AuraEnabled(cacheable=true)
    public static List<CartItemWrapper> getCSRecommendationProducts(String frameTypeId) {
        
        if(frameTypeId == null || String.isBlank(frameTypeId)) {
            return null;
        }
        //System.debug('communityBasePath>>>'+communityBasePath);
        //System.debug('frameTypeId>>>'+frameTypeId);
        List<CartItemWrapper> returnList = new List<CartItemWrapper>();
        
        String webStoreId =null;
        String effectiveAccountId = B2BCommerceUtils.getAccountIdFromUser();
        System.debug('effectiveAccountId>>>'+effectiveAccountId);
        List<ConnectApi.ProductDetail> myProductsInformations = new List<ConnectApi.ProductDetail>();
        Set<Id> recomProductIdSet = new Set<Id>();
        
        B2B_IR_Store_Setting__mdt customPermissionIR = B2B_IR_Store_Setting__mdt.getInstance(IRB2BWEBSTOREURLPATHPREFIX);
        webStoreId = B2BCommerceUtils.getWebStoreIdByUrlPathPrefix(customPermissionIR.value__c);
        
        System.debug('webStoreId>>>'+webStoreId);
        for(Cross_Sell_Recommendations__c csr : getCrossSellProductsByFrameType(frameTypeId)){
            recomProductIdSet.add(csr.Recommended_ProductId__c);
        }
        System.debug('recomProductIdSet>>>'+recomProductIdSet);
        ConnectApi.ProductDetail productDetail;
        ConnectApi.ProductPrice productPrice;
        
        for (Id productId : recomProductIdSet) {
            productDetail = RecommendedProducts.getProduct(webstoreID, productId, effectiveAccountID);
            productPrice = RecommendedProducts.getProductPrice(webstoreID, productId, effectiveAccountID);
            returnList.add(getCartItemWrapper(productDetail,productPrice,effectiveAccountID,webstoreID));
            //myProductsInformations.add(productDetail);
        }
        System.debug('returnList>>>'+returnList);
        return returnList;
    }
    
    private static List<Cross_Sell_Recommendations__c> getCrossSellProductsByFrameType(String frameTypeId) {
        return [Select Id,Recommended_ProductId__c from Cross_Sell_Recommendations__c where Recommended_Model_ParentId__c = :frameTypeId Order By createdDate desc limit 6];
    }

    private static List<Cross_Sell_Recommendations__c> getCrossSellProductsByProductId(String productId) {
        return [Select Id,Recommended_ProductId__c from Cross_Sell_Recommendations__c where Recommended_Product_ParentId__c = :productId Order By createdDate desc limit 6];
    }
    
    //@AuraEnabled(cacheable=true)
    public static ConnectApi.ProductDetail getProduct(String webstoreId, String productId, String effectiveAccountId) {
        ConnectApi.ProductDetail pd = null;
        if(!Test.isRunningTest()){
            pd = ConnectApi.CommerceCatalog.getProduct(webstoreId, productId, effectiveAccountID, null, false, null, false, true, false);
        }
        return pd;
    }
    //@AuraEnabled(cacheable=true)
    public static ConnectApi.ProductPrice getProductPrice(String webstoreId, String productId, String effectiveAccountId) {
        ConnectApi.ProductPrice price = null;
        if(!Test.isRunningTest()){
            price = ConnectApi.CommerceStorePricing.getProductPrice(webstoreId, productId, effectiveAccountId);
        }
        return price;
    }
    private static CartItemWrapper getCartItemWrapper(ConnectApi.ProductDetail productDetail, ConnectApi.ProductPrice productPrice, String effectiveAccountID, String webstoreID) {
        CartItemWrapper ciw = new CartItemWrapper();
        ciw.name = productDetail?.fields?.get('Name');
        ciw.recId = productDetail?.Id;
        ciw.stockKeepingUnit = productDetail?.fields?.get('StockKeepingUnit');
        ciw.description = productDetail?.fields?.get('Short_Description__c');
        ciw.compressorAirFilter = productDetail?.fields?.get('Compressor_Air_Filter_type__c');
        ciw.maintenanceInterval = productDetail?.fields?.get('Maintenance_Interval__c');
        ciw.fullUrl = IRB2BWEBSTOREURL + '&RelayState=/product/'+productDetail?.Id;
        ciw.defaultImageUrl = productDetail?.defaultImage.url == '/img/b2b/default-product-image.svg' ? getIRLogoUrl() : productDetail?.defaultImage.url;
        ciw.defaultImageTitle = productDetail?.defaultImage.title;
        ciw.unitPrice = productPrice?.unitPrice;
        ciw.currencyIsoCode = productPrice?.currencyIsoCode;
        ciw.accountId = effectiveAccountID;
        ciw.webStoreId = webstoreID;
        ciw.quantity = '1';
        return ciw; 
    }
    
    @AuraEnabled
    public static ConnectApi.CartItem addToCart(String webStoreId, String productId, String quantity, String effectiveAccountId) {
        ConnectApi.CartItemInput cartInput = new ConnectApi.CartItemInput();
        cartInput.productId = productId;
        cartInput.quantity = quantity;
        cartInput.type = ConnectApi.CartItemType.PRODUCT;
        ConnectApi.CartItem cartItem = null;
        if(!Test.isRunningTest()){
            ConnectApi.CartSummary cartSummary = ConnectApi.CommerceCart.getOrCreateActiveCartSummary(webstoreId, effectiveAccountId, 'active');
            System.debug(cartSummary);
            cartItem = ConnectApi.CommerceCart.addItemToCart(webstoreId, effectiveAccountId, cartSummary.cartId, cartInput);

        }
        return cartItem;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<CartItemWrapper> getCSRecommendationProductsByProductId(String productId) {
        if(productId == null || String.isBlank(productId)) {
            return null;
        }
    List<CartItemWrapper> returnList = new List<CartItemWrapper>();
        
        String webStoreId =null;
        String effectiveAccountId = B2BCommerceUtils.getAccountIdFromUser();

         List<ConnectApi.ProductDetail> myProductsInformations = new List<ConnectApi.ProductDetail>();
        Set<Id> recomProductIdSet = new Set<Id>();
        
        B2B_IR_Store_Setting__mdt customPermissionIR = B2B_IR_Store_Setting__mdt.getInstance(IRB2BWEBSTOREURLPATHPREFIX);
        webStoreId = B2BCommerceUtils.getWebStoreIdByUrlPathPrefix(customPermissionIR.value__c);
        
        for(Cross_Sell_Recommendations__c csr : getCrossSellProductsByProductId(productId)){
            recomProductIdSet.add(csr.Recommended_ProductId__c);
        }
        System.debug('recomProductIdSet>>>'+recomProductIdSet);
        ConnectApi.ProductDetail productDetail;
        ConnectApi.ProductPrice productPrice;

        for (Id prdId : recomProductIdSet) {
            productDetail = RecommendedProducts.getProduct(webstoreID, prdId, effectiveAccountID);
            productPrice = RecommendedProducts.getProductPrice(webstoreID, prdId, effectiveAccountID);
            returnList.add(getCartItemWrapper(productDetail,productPrice,effectiveAccountID,webstoreID));
            //myProductsInformations.add(productDetail);
        }
        System.debug('returnList>>>'+returnList);

        return returnList;
    }
    
    @AuraEnabled(cacheable=true)
    public static String getIRLogoUrl() {
        return IRLOGOURL;
    }

    @AuraEnabled(cacheable=true)
    public static String getWebCartUrl() {
        return IRB2BWEBSTOREURL + '&RelayState=/cart';
    }

    @AuraEnabled(cacheable=true)
    public static Asset getAssetData(Id assetId) {
        Asset asset = [SELECT Name, Asset_Name_Displayed__c, AccountId, CTS_IOT_Operating_Status__c, SerialNumber, Model_Name__c, InstallDate, Ship_Date__c, Helix_Connectivity_Status__c, Manufacturer__c, HP__c, IRIT_Frame_Type__c, IRIT_RMS_Flag__c, Controller_Type_RMS_Compatible__c, CTS_IOT_Frame_Type__c
                        From Asset WHERE Id = :assetId];
        return asset;
    }
    
    
    
}