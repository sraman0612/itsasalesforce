public class UTIL_TermsAndConditions {
    public static void updateDraftStatusQuotes(List<Company_Brand__c> toProcess) {
        System.debug('UTIL_TermsAndConditions');
        Map<String,String> accountTermsAndConditionsMap = new Map<String,String>();
        
        for (Company_Brand__c cur : toProcess) {
            if (cur.Distributor_Terms_and_Conditions__c != null) {
	            accountTermsAndConditionsMap.put(cur.AccountId__c, cur.Distributor_Terms_and_Conditions__c);
            }
        }
        /*
        List<SBQQ__Quote__c> quotes = [SELECT Id, SBQQ__Account__c, My_Quote_Terms_and_Conditions__c from SBQQ__Quote__c where SBQQ__Status__c = 'Draft' AND SBQQ__Account__c in : accountTermsAndConditionsMap.keySet()];
        
        for (SBQQ__Quote__c quote : quotes) {
            quote.My_Quote_Terms_and_Conditions__c = accountTermsAndConditionsMap.get(quote.SBQQ__Account__c);
        }
        
        update quotes;
		*/
        
        for(String acctId : accountTermsAndConditionsMap.keySet()){
            Database.executeBatch(new UTIL_TermsAndConditions_Batch(acctId, accountTermsAndConditionsMap.get(acctId)), 100); 
        }
    }
}