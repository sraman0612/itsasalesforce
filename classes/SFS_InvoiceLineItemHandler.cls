public class SFS_InvoiceLineItemHandler {
    Public static void assignChargeAmount(List<SFS_Invoice_Line_Item__c> newList){
        try{
            Set<Id> invoiceIds = new Set<Id>();
            for(SFS_Invoice_Line_Item__c record : newList){
                invoiceIds.add(record.SFS_Invoice__c);
            }
            List<Invoice__c> invoiceList = [Select Id, SFS_Total_Charge_Amount__c, (Select Id,SFS_Sell_Price__c from Invoice_Line_Items1__r) 
                                            from Invoice__c where Id IN : invoiceIds];
            List<Invoice__c> updatedList = new List<Invoice__c>();
            If(invoiceList.size()>0){
                for(Invoice__c record : invoiceList){
                    Decimal chargeAmount=0;
                    if(record.Invoice_Line_Items1__r.size()>0){
                        for(SFS_Invoice_Line_Item__c lineItem : record.Invoice_Line_Items1__r){
                            if(lineItem.SFS_Sell_Price__c != null){
                                chargeAmount+= lineItem.SFS_Sell_Price__c;
                            }
                        }
                    }
                    if(chargeAmount>0){
                        record.SFS_Total_Charge_Amount__c = chargeAmount;
                        updatedList.add(record);
                    }
                }
                update updatedList;
            }
        }Catch(Exception e){}
    }
}