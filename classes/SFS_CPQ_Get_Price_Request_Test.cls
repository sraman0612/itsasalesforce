/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 24/05/2022
* @description: Test class for SFS_CPQ_Get_Price_Request

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

===========================================================================================================*/
@isTest
public class SFS_CPQ_Get_Price_Request_Test {
        
    @isTest
    public static void CPQGetPriceRequestTestMethod(){
        
        Test.setMock(HttpCalloutMock.class, new SFS_MockCPQGetPriceResponse());
        //HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        HttpResponse res = SFS_MockCPQGetPriceResponse.invokeMockResponse();
        
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        //Account billToAcc = SFS_TestDataFactory.getAccount();
        //Update billToAcc;
        //Account acc = SFS_TestDataFactory.getAccount();
        //acc.Bill_To_Account__c=billToAcc.Id;
        //acc.Type ='Prospect';
        //Update acc;
        
        List<Account> acct=SFS_TestDataFactory.createAccounts(2, false);
        acct[0].AccountSource='Web';
        acct[0].IRIT_Customer_Number__c='1234';
        acct[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acct[0].IRIT_Payment_Terms__c='BANKCARD';
        insert acct[0]; 
        
        acct[1].Bill_To_Account__c = acct[0].Id;
        acct[1].AccountSource='Web';
        acct[1].name = 'test account';
        acct[1].CurrencyIsoCode = 'USD';
        acct[1].IRIT_Customer_Number__c='1234';
        insert acct[1];
        acct[1].Type = 'Prospect';
        update acct[1];        
        
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,false);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='00250506';
        update proList[0];
        List<ServiceContract> svc = SFS_TestDataFactory.createServiceAgreement(1, acct[1].Id, true);
        
        List<ContractLineItem> cliRec = SFS_TestDataFactory.createServiceAgreementLineItem(1, svc[0].Id,true);
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
        insert lc; 
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acct[1].Id,loc[0].Id, div[0].Id,  svc[0].Id, true);
        WorkType wt = new WorkType();
           String wt_RECORDTYPEID = Schema.SObjectType.WorkType.getRecordTypeInfosByName().get('Service').getRecordTypeId();
           wt.Name = 'test';
           wt.SFS_Product__c = proList[0].Id;
           wt.RecordTypeId = wt_RECORDTYPEID;
           wt.EstimatedDuration = 12.00;
           wt.DurationType ='Hours';
           insert wt;
        
        List<WorkOrderLineItem> woliList =  SFS_TestDataFactory.createWorkOrderLineItem(1,WoList[0].Id,wt.Id,true);
        
        List<ProductItem> prItemList = SFS_TestDataFactory.createProductItem(1,loc[0].Id ,proList[0].Id, false);
              
        List<ProductRequest> prList =  SFS_TestDataFactory.createProductRequest(1,WoList[0].Id,woliList[0].Id,false);
        prList[0].DestinationLocationId=loc[0].Id;
        system.debug('pr '+prList);
        insert prList[0];
        
        List<ProductRequestLineItem> prliList =  SFS_TestDataFactory.createPRLI(1,prList[0].Id,proList[0].Id,false);
        prliList[0].Status = 'Shipped';
        prliList[0].QuantityUnitOfMeasure = 'EA';
        prliList[0].SFS_Quantiy_Shipped__c = 2;
        prliList[0].SFS_Quantity_Received__c =2;
        insert prliList[0];
        prliList[0].SFS_External_Id__c='1';
        update prliList[0];
        
        /*Pricebook2 servicePricebook = new Pricebook2();
        servicePricebook.IsActive = true;
        servicePricebook.Name = 'Service Pricebook Test';
            //[SELECT Id, isActive FROM Pricebook2 WHERE Name = 'Service Price Book'];
		System.debug('Pricebook Query: ' + servicePricebook);        
        insert servicePricebook;
        id pricebookx = servicePricebook.id;
        
       
       System.debug('ServPB ' + pricebookx);
        Product2 pro = new Product2(Name = 'testProduct');
        insert pro;
        
        PricebookEntry entry = new PricebookEntry();
        entry.Pricebook2Id = pricebookx;
        	entry.Product2Id = pro.Id;
        	entry.UnitPrice = 2.00;
        	entry.IsActive = true;
        
        List<ProductConsumed> prConList = SFS_TestDataFactory.createProductConsumed(1, woliList[0].Id, prItemList[0].Id, false);
        prConList[0].ProductItemId = prItemList[0].Id;
        prConList[0].PricebookEntryId = entry.id;
        System.debug('prCons '+prConList);
        insert prConList[0];*/
        
        SFS_CPQ_Get_Price_Request.getPriceRequest getPrice = new  SFS_CPQ_Get_Price_Request.getPriceRequest();
        getPrice.recId=prliList[0].Id;
        getPrice.currencyCode ='USD';
        SFS_CPQ_Get_Price_Request.GetPrice(
            new List<SFS_CPQ_Get_Price_Request.getPriceRequest>{getPrice}
        );
        SFS_CPQ_Get_Price_Request.getPriceResponse(res);
        
        system.assertEquals('{"items":[{"itemIdentifier":"1","partNumber":"00250506","unitPrice":102.000000,"calculationInfo":[{"DNCost":81.6,"unitPrice":102}]}]}', res.getBody());
    }
    
    @isTest
    public static void CPQGetPriceRequestWoliTestMethod(){
        
        Test.setMock(HttpCalloutMock.class, new SFS_MockCPQGetPriceResponse());
        //HttpResponse res = SFS_MockCalloutClass.invokeMockResponse();
        HttpResponse res = SFS_MockCPQGetPriceResponse.invokeMockResponse();
        
        List<Division__C>  div =SFS_TestDataFactory.createDivisions(1,true);
        //Account billToAcc = SFS_TestDataFactory.getAccount();
        //Update billToAcc;
        //Account acc = SFS_TestDataFactory.getAccount();
        //acc.Bill_To_Account__c=billToAcc.Id;
        //acc.Type ='Prospect';
        //Update acc;
        
        List<Account> acct=SFS_TestDataFactory.createAccounts(2, false);
        acct[0].AccountSource='Web';
        acct[0].IRIT_Customer_Number__c='1234';
        acct[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acct[0].IRIT_Payment_Terms__c='BANKCARD';
        insert acct[0]; 
        
        acct[1].Bill_To_Account__c = acct[0].Id;
        acct[1].AccountSource='Web';
        acct[1].name = 'test account';
        acct[1].CurrencyIsoCode = 'USD';
        acct[1].IRIT_Customer_Number__c='1234';
        insert acct[1];
        acct[1].Type = 'Prospect';
        update acct[1];
        
        List<Schema.Location> loc = SFS_TestDataFactory.createLocations(1,div[0].Id,false);
        List<Product2> proList = SFS_TestDataFactory.createProduct(1,true);
        proList[0].productCode ='00250506';
        update proList[0];
        List<ServiceContract> svc = SFS_TestDataFactory.createServiceAgreement(1, acct[1].Id,true);
        List<ContractLineItem> cliRec = SFS_TestDataFactory.createServiceAgreementLineItem(1, svc[0].Id,true);
        Schema.Location lc = new  Schema.Location(Name = 'testVan', SFS_Primary_Location__c = true, IsInventoryLocation = true);
        insert lc; 
        List<WorkOrder> WoList = SFS_TestDataFactory.createWorkOrder(1, acct[1].Id,loc[0].Id, div[0].Id,  svc[0].Id, true);
        WorkType wt = new WorkType();
           String wt_RECORDTYPEID = Schema.SObjectType.WorkType.getRecordTypeInfosByName().get('Service').getRecordTypeId();
           wt.Name = 'test';
           wt.SFS_Product__c = proList[0].Id;
           wt.RecordTypeId = wt_RECORDTYPEID;
           wt.EstimatedDuration = 12.00;
           wt.DurationType ='Hours';
           insert wt;
        
        List<WorkOrderLineItem> woliList =  SFS_TestDataFactory.createWorkOrderLineItem(1,WoList[0].Id,wt.Id,true);
                 
        //ProductItem pI = new ProductItem(Product2Id = proList[0].Id, LocationId = loc[0].Id, QuantityOnHand = 4);
        //insert pI;
        
        Product2 pro = new Product2(Name = 'testProduct');
        insert pro;
        
        List<CAP_IR_Labor__c> lh= SFS_TestDataFactory.createLaborHours(1,WoList[0].Id,woliList[0].Id,true);
        lh[0].SFS_Product__c=proList[0].Id;
        lh[0].SFS_External_Id__c='1';
        lh[0].CAP_IR_Unit_Price__c=100.00;
        lh[0].SFS_Product__c = pro.id;
        update  lh[0];
        
        SFS_CPQ_Get_Price_Request.getPriceRequest getPrice = new  SFS_CPQ_Get_Price_Request.getPriceRequest();
        getPrice.recId=woliList[0].Id;
        getPrice.currencyCode ='USD';
        SFS_CPQ_Get_Price_Request.GetPrice(
            new List<SFS_CPQ_Get_Price_Request.getPriceRequest>{getPrice}
        );
        SFS_CPQ_Get_Price_Request.getPriceResponse(res);
    }
    

}