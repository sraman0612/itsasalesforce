/*=========================================================================================================
* @author Geethanjali SP, Capgemini
* @date 16/06/2022
* @Story Number: SIF-647

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/
public class SFS_ServiceAppointmentHandler {

    public static void checkOutsideOperatingHours(List<ServiceAppointment> saList){
        
        Set<Id> saIds=new Set<Id>();
        Set<Id> woliIds=new Set<Id>();
        List<WorkOrderLineItem> woliList = new List<WorkOrderLineItem>();
        List<TimeSlot> tSlotList = new List<TimeSlot>();
        Boolean outsideop = false;
        for(ServiceAppointment sa : saList){
          
              saIds.add(sa.Id);  
              woliIds.add(sa.ParentRecordId); 
                                     
       }
        if(!saIds.isEmpty()){ 
            woliList = [Select Id,WorkOrder.FSL__VisitingHours__c from WorkOrderLineItem where Id IN:woliIds];
        }
        
        if(!woliList.isEmpty()){
        	for(WorkOrderLineItem woli : woliList){
            	if(woli.WorkOrder.FSL__VisitingHours__c!=null){
                    System.debug(woli.WorkOrder.FSL__VisitingHours__c);
                    String strHours = woli.WorkOrder.FSL__VisitingHours__c;
                	tSlotList= [Select Id, StartTime, EndTime, DayOfWeek from TimeSlot where OperatingHoursId =:strHours];    
            	}                    
        	} 
        }
        
        		 
       	DateTime cDT = system.Now();
        Time t =cDT.time();
		String dayOfWeek = cDT.format('EEEE'); 

        if(tSlotList.size()>0){
            for(TimeSlot ts:tSlotList){
                if(outsideop == false){
                	if(ts.DayOfWeek==dayOfWeek && (ts.StartTime>=t || ts.EndTime<=t)){
                    	outsideop = true;
                	}  
                }               
            }  
            
            if(outsideop == true && !saList.isEmpty()){
                for(ServiceAppointment sa1:saList){
                	sa1.SFS_Outside_Operating_Hours__c = true;
            	}

            }    
        }
 
    }
}