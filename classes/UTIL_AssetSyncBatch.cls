/*
Paste the below code into the Anonymous Apex Window
To know how many total records are there to sync
========== START CODE ==========
new ensxsdk.Logger(null);

SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC searchContext = new SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC();
searchContext.pagingOptions.pageSize = 1;
searchContext.pagingOptions.pageNumber = 1;
searchContext.SEARCHPARAMS.InitialLoadFlag = true;

SBO_EnosixEquipmentSync_Search sbo = new SBO_EnosixEquipmentSync_Search();
sbo.search(searchContext);
System.debug(searchContext.result.isSuccess());
System.debug(searchContext.pagingOptions.totalRecords);
========== END CODE ==========
*/
public with sharing class UTIL_AssetSyncBatch
    implements Database.Batchable<Object>,
    Database.AllowsCallouts,
    Database.Stateful,
    I_ParameterizedSync
{
    @TestVisible
    private static String SFSyncKeyField = 'Serial_Number_Model_Number__c'; //'FLD_Serial_Number_Equipment_Number__c'
    @TestVisible
    private static String BatchClassName = 'UTIL_AssetSyncBatch';
    private static String ScheduleClassName = 'UTIL_AssetSyncSchedule';

    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_AssetSyncBatch.class);
    public void logCallouts(String location)
    {
        if ((Boolean)UTIL_AppSettings.getValue(BatchClassName + '.Logging', false))
        {
            logger.enterVfpConstructor(location, null);
        }
    }

    // jobInfo contains debug information persisted across contexts since start(),
    // execute(), and finish() all run in separate contexts with separate debug logs
    private List<String> jobInfo = new List<String>();

    // In this case, we will store the largest change date/time as the param
    private UTIL_SyncHelper.LastSync fromLastSync = new UTIL_SyncHelper.LastSync();
    private static String ObjectType = 'Asset';
    private static String SAP_SalesOrg = (string) UTIL_AppSettings.getValue(BatchClassName + '.SalesOrg');
    private static String SAP_Division = (string) UTIL_AppSettings.getValue(BatchClassName + '.Division');
    private static List<String> List_SAP_ExclDistChannels = (List<String>) UTIL_AppSettings.getList(BatchClassName + '.ExcludedDistributionChannels',String.class);
    private static Set<String> SAP_ExclDistChannels = new Set<String>();
    private static Map<String, MPG4_Code__c> MPG4_Codes_Map = new Map<String, MPG4_Code__c>();

    /* I_ParameterizedSync methods - setBatchParam() */
    public void setBatchParam(Object value)
    {
        this.fromLastSync = (UTIL_SyncHelper.LastSync) value;
    }
    /* end I_ParameterizedSync methods */

    /* Database.Batchable methods start(), execute(), and finish() */
    // start()
    //
    // Calls SBO and returns search results of update materials
    public List<Object> start(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.start');
        System.debug(context.getJobId() + ' started');

        SBO_EnosixEquipmentSync_Search sbo = new SBO_EnosixEquipmentSync_Search();
        SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC searchContext = new SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SC();

        this.fromLastSync = UTIL_SyncHelper.getLastSyncFromTable(
            ScheduleClassName,
            this.fromLastSync);

        this.fromLastSync.pageNumber = this.fromLastSync.pageNumber + 1;

        if (this.fromLastSync.retryCnt == -1)
        {
            UTIL_SyncHelper.resetPage(this.fromLastSync, (Integer) UTIL_AppSettings.getValue(
                BatchClassName + '.SAPPageSize',
                1000));
        }
        if (this.fromLastSync.lastSyncDate != null)
        {
            searchContext.SEARCHPARAMS.DateFrom = this.fromLastSync.lastSyncDate;
        }
        else
        {
            searchContext.SEARCHPARAMS.InitialLoadFlag = true;
        }

        searchContext.pagingOptions.pageSize = this.fromLastSync.pageSize;
        searchContext.pagingOptions.pageNumber = this.fromLastSync.pageNumber;
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());
        System.debug('fromLastSync:' + this.fromLastSync.toString());

        // Execute the search
        SBO_EnosixEquipmentSync_Search.EnosixEquipmentSync_SR result;
        try
        {
            sbo.search(searchContext);
            result = searchContext.result;
        }
        catch (Exception ex)
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, ex, this.jobInfo);
        }

        // Write any response messages to the debug log
        String errorMessage = UTIL_SyncHelper.buildErrorMessage(BatchClassName, result.getMessages());

        if (!result.isSuccess())
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, new UTIL_SyncHelper.SyncException(errorMessage), this.jobInfo);
        }

        List<Object> searchResults = result.getResults();
        System.debug('Result size: ' + searchResults.size());

        // let finish() know to queue up another instance
        this.fromLastSync.isAnotherBatchNeeded = searchResults.size() > 0;
        this.fromLastSync.retryCnt = -1;

        this.jobInfo.add('searchResultsSize:' + searchResults.size());
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());

        // add distribution channels from the static resource file to the SAP_DistChannels <Set> for filtering
        for (String channel : List_SAP_ExclDistChannels) SAP_ExclDistChannels.add(channel);

        return searchResults;
    }

    // execute()
    //
    // Given the updated search results, does the work of updating the object table.
    public void execute(
        Database.BatchableContext context,
        List<Object> searchResults)
    {
        logCallouts(BatchClassName + '.execute');
        System.debug(context.getJobId() + ' executing');

        if (null == searchResults || 0 == searchResults.size()) return;

        // Load map of MPG4 Codes to populate the related lookup field on the Asset object
        for (MPG4_Code__c mpg4Vals : [Select MPG4_Number__c,Id,Level_1__c,Level_2__c,Level_3__c From MPG4_Code__c]) {
            MPG4_Codes_Map.put(mpg4Vals.MPG4_Number__c, mpg4Vals);
        }

        List<SObject> errors = new List<SObject>();
        Set<String> configurableProductIds = new Set<String>();
        Map<String, Object> searchResultMap = createObjectKeyMap(searchResults);

        // First, update matching existing objects
        List<SObject> currentObjectList = UTIL_SyncHelper.getCurrentObjects(ObjectType, SFSyncKeyField, searchResultMap.keySet());
        List<SObject> updateObjectList = updateExistingObjects(searchResultMap, currentObjectList, errors, configurableProductIds);
        List<SObject> insertObjectList = createNewObjects(searchResultMap, currentObjectList, errors, configurableProductIds);
        Set<Id> savedIdSet = new Set<Id>();

        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Update', errors, savedIdSet, updateObjectList, BatchClassName, SFSyncKeyField);
        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Insert', errors, savedIdSet, insertObjectList, BatchClassName, SFSyncKeyField);
        UTIL_SyncHelper.insertUpdateResults('Error', 'Insert', errors, savedIdSet, errors, BatchClassName, null);
    }

    // finish()
    //
    // queues up another batch when isAnotherBatchNeeded is true
    public void finish(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.finish');
        System.debug(context.getJobId() + ' finished');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);
        if (this.fromLastSync.retryCnt >= 0)
        {
            System.debug('Retry=' + this.fromLastSync.retryCnt + ' ' + System.Now());
        }

        UTIL_SyncHelper.launchAnotherBatchIfNeeded(
            this.fromLastSync.isAnotherBatchNeeded, ScheduleClassName, this.fromLastSync);
    }

    private SBO_EnosixEquipmentSync_Search.SEARCHRESULT getSboResult(Object searchResult)
    {
        return (SBO_EnosixEquipmentSync_Search.SEARCHRESULT) searchResult;
    }

    // createObjectKeyMap()
    //
    // create map of product key / search result.
    private Map<String, Object> createObjectKeyMap(
        List<Object> searchResults)
    {
        Map<String, Object> result =
            new Map<String, Object>();

        for (Object searchResult : searchResults)
        {
            String key = getSboKey(searchResult);
            if (String.isNotEmpty(key))
            {
                result.put(key, searchResult);
            }
        }

        return result;
    }

    private List<SObject> updateExistingObjects(
        Map<String, Object> searchResultMap,
        List<SObject> currentObjectList,
        List<SObject> errors,
        Set<String> configurableProductIds)
    {
        List<SObject> updateObjectList = new List<SObject>();

        for (SObject currentObject : currentObjectList)
        {
            String syncKey = (String) currentObject.get(SFSyncKeyField);
            Object searchResult = searchResultMap.get(syncKey);

            // Updates fields and adds to objectList list for later commit
            syncObject(currentObject, searchResult, errors, updateObjectList, configurableProductIds);
        }

        System.debug('Existing Object Size: ' + updateObjectList.size());

        return updateObjectList;
    }

    private List<SObject> createNewObjects(
        Map<String, Object> searchResultMap,
        List<SObject> currentObjectList,
        List<SObject> errors,
        Set<String> configurableProductIds)
    {
        Set<String> existingObjectSyncKeys = new Set<String>();

        for (sObject currentObject : currentObjectList)
        {
            String syncKey = (String) currentObject.get(SFSyncKeyField);
            existingObjectSyncKeys.add(syncKey);
        }

        List<SObject> newObjectList = new List<SObject>();

        for (Object searchResult : searchResultMap.values())
        {
            if (!existingObjectSyncKeys.contains(getSboKey(searchResult)))
            {
                syncObject(null, searchResult, errors, newObjectList, configurableProductIds);
            }
        }

        System.debug('New Object Size: ' + newObjectList.size());

        return newObjectList;
    }

    private void syncObject(
        SObject currentObject,
        Object searchResult,
        List<SObject> errors,
        List<SObject> objectList,
        Set<String> configurableProductIds)
    {
        SBO_EnosixEquipmentSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        if (String.isNotBlank(sboResult.DistributionChannel) && !SAP_ExclDistChannels.contains(sboResult.DistributionChannel) &&
            sboResult.SalesOrganization == SAP_SalesOrg  && sboResult.Division == SAP_Division)
        {
            if (currentObject == null)
            {
                currentObject = new Asset();
            }
        }
        else {
            return;
        }
        Asset asst = (Asset) currentObject;
        String key = getSboKey(searchResult);
        asst.put(SFSyncKeyField,key);
        asst.Name = sboResult.SerialNumber;
        if (String.isEmpty(asst.Name)) asst.Name = key;

        if (null != MPG4_Codes_Map.get(sboResult.Materialgroup4))
        {
            asst.put('MPG4_Code__c',MPG4_Codes_Map.get(sboResult.Materialgroup4).Id);
        }

        asst.Change_Date__c = sboResult.DateChanged;
        asst.DChl__c = sboResult.DistributionChannel +' - '+ sboResult.DistributionChannelText;
        asst.FLD_Distribution_Channel__c = sboResult.DistributionChannel;
        asst.DelDate__c = sboResult.DeliveryDate;
        asst.Material__c = sboResult.Material;
        asst.Material_Description__c = sboResult.MaterialDescription;
        asst.Model_Number__c = sboResult.Material;
        asst.Model__c = sboResult.EquipmentNumberDescription;
        asst.Description = sboResult.EquipmentNumberDescription;
        asst.Equipment_Number__c = sboResult.EquipmentNumber;
        asst.Equipment_Status__c = sboResult.SystemStatus1+' '+sboResult.SystemStatus2+' '+sboResult.SystemStatus3+' '+sboResult.SystemStatus4+' '+sboResult.SystemStatus5;
        asst.Equipment_Status_Description__c = sboResult.SystemStatus1Description+' '+sboResult.SystemStatus2Description+' '+sboResult.SystemStatus3Description+' '+sboResult.SystemStatus4Description+' '+sboResult.SystemStatus5Description;
        asst.IMEI__c = sboResult.IMEISerialNumber;
        asst.MG_4__c = sboResult.Materialgroup4;
        asst.Owner_Address_Line_1__c = sboResult.Street;
        asst.Owner_City__c = sboResult.City;
        asst.Owner_Country__c = sboResult.Country;
        asst.Owner_Name__c = sboResult.Name;
        asst.Owner_State__c = sboResult.Region;
        asst.Owner_Zip_Code__c = sboResult.PostalCode;
        asst.Owner_Phone__c = sboResult.TelephoneNumber;
        asst.Product_Hierarchy__c = sboResult.ProductHierarchy;
        asst.SOrg__c = sboResult.SalesOrganization +' - '+ sboResult.SalesOrgDescription;
        asst.SerialNumber = sboResult.SerialNumber;
        asst.Serial_Number_Model_Number__c = sboResult.SerialNumber +'-'+ sboResult.Material;
        asst.FLD_Serial_Number_Equipment_Number__c = sboResult.SerialNumber+'-'+sboResult.EquipmentNumber;
        asst.Start_up_Date__c = sboResult.TechnicalObjectStartupDate;
        asst.Store_Number__c = sboResult.StroreNo;
        asst.Sales_Order__c = sboResult.SalesOrderNumber;
        asst.Warranty_No__c = sboResult.MasterWarrantyNumber;
        asst.Master_Warranty_Description__c = sboResult.MasterWarrantyText;
        asst.Sold_to__c = sboResult.SoldToPartnerNumber +' - '+ sboResult.SoldToName;
        asst.char1__c = sboResult.characteristic1;
        asst.char2__c = sboResult.characteristic2;
        asst.char3__c = sboResult.characteristic3;
        asst.char4__c = sboResult.characteristic4;
        asst.char5__c = sboResult.characteristic5;
        asst.char6__c = sboResult.characteristic6;
        asst.char7__c = sboResult.characteristic7;
        asst.char8__c = sboResult.characteristic8;
        asst.char9__c = sboResult.characteristic9;
        asst.char10__c = sboResult.characteristic10;
        asst.char11__c = sboResult.characteristic11;
        asst.char12__c = sboResult.characteristic12;
        asst.char13__c = sboResult.characteristic13;
        asst.char14__c = sboResult.characteristic14;
        asst.char15__c = sboResult.characteristic15;
        asst.char16__c = sboResult.characteristic16;
        asst.char17__c = sboResult.characteristic17;
        asst.char18__c = sboResult.characteristic18;
        asst.char19__c = sboResult.characteristic19;
        asst.char20__c = sboResult.characteristic20;
        asst.char21__c = sboResult.characteristic21;
        asst.char22__c = sboResult.characteristic22;
        asst.char23__c = sboResult.characteristic23;
        asst.char24__c = sboResult.characteristic24;
        asst.char25__c = sboResult.characteristic25;
        asst.char_Text1__c = sboResult.ValText1;
        asst.char_Text2__c = sboResult.ValText2;
        asst.char_Text3__c = sboResult.ValText3;
        asst.char_Text4__c = sboResult.ValText4;
        asst.char_Text5__c = sboResult.ValText5;
        asst.char_Text6__c = sboResult.ValText6;
        asst.char_Text7__c = sboResult.ValText7;
        asst.char_Text8__c = sboResult.ValText8;
        asst.char_Text9__c = sboResult.ValText9;
        asst.char_Text10__c = sboResult.ValText10;
        asst.char_Text11__c = sboResult.ValText11;
        asst.char_Text12__c = sboResult.ValText12;
        asst.char_Text13__c = sboResult.ValText13;
        asst.char_Text14__c = sboResult.ValText14;
        asst.char_Text15__c = sboResult.ValText15;
        asst.char_Text16__c = sboResult.ValText16;
        asst.char_Text17__c = sboResult.ValText17;
        asst.char_Text18__c = sboResult.ValText18;
        asst.char_Text19__c = sboResult.ValText19;
        asst.char_Text20__c = sboResult.ValText20;
        asst.char_Text21__c = sboResult.ValText21;
        asst.char_Text22__c = sboResult.ValText22;
        asst.char_Text23__c = sboResult.ValText23;
        asst.char_Text24__c = sboResult.ValText24;
        asst.char_Text25__c = sboResult.ValText25;

        objectList.add(asst);
    }

    private String getSboKey(Object searchResult)
    {
        SBO_EnosixEquipmentSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        return sboResult == null ? '' : sboResult.SerialNumber +'-'+ sboResult.Material;
        //return sboResult == null ? '' : sboResult.SerialNumber+'-'+sboResult.EquipmentNumber;
    }
}