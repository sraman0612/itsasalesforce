@isTest
public with sharing class ManageAccountHierarchyController_Test {
    public static testMethod void doTest() {
    	// Create some data
    	Account blueSun = new Account( Name = 'Blue Sun Corp' );
    	insert blueSun;
    	Account serenity = new Account( Name = 'Serenity' );
    	insert Serenity;
    	Account badger = new Account( Name = 'Badger', ParentId = blueSun.Id );
    	insert badger;

		// Do some stuff
		ManageAccountHierarchyController theController = new ManageAccountHierarchyController();
		theController.accountSearch = 'blah';
		ManageAccountHierarchyController.fetchAccounts( 'blah' );
		
		// Validate
		// again, 100% coverage.  I'm not touching it.
    }
}