public class serialList2Con {

    public serialList2Con(ApexPages.StandardController controller) {

    }

    // ApexPages.StandardSetController must be instantiated
    // for standard list controllers
    public Case currentRecord{get; set;}

    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
            
                currentRecord = [SELECT Id, Account.ParentId, Store_Number__c FROM Case WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id, AccountId, Name, Model__c, Inactive__c, Status, Store_Number__c, Material_Description__c FROM Asset WHERE 
                    Store_Number__c = :currentRecord.Store_Number__c LIMIT 10000
                    //AND AccountId = :currentRecord.Account.ParentId
                    ]));
            }
            return setCon;
        }
        set;
    }

    // Initialize setCon and return a list of records
    public List<Asset> getSerials() {
        return (List<Asset>) setCon.getRecords();
    }
}