@IsTest
public class ExpenseClaimInvocationHandlerTest {
    
    @IsTest
    public static void test() {
        Id wcId = [SELECT Id from Warranty_Claim__c where Name = 'test' LIMIT 1].Id;
        
        List<OBJ_WarrantyClaimItem> items = new List<OBJ_WarrantyClaimItem>();
        OBJ_WarrantyClaimItem item = new OBJ_WarrantyClaimItem();
        items.add(item);
        item.PartNo = '12345';
        item.ItemNo = '12345';
        item.Quantity = 1;
        item.Price = 13.33;
        item.Unit = 'X';
        item.DiscountPercent = 12;
        item.GDInvoiceNumber = '12345';
        item.GDInvoiceItemNumber = '10';
        
        ExpenseClaimInvocationHandler.FlowInput input = new ExpenseClaimInvocationHandler.FlowInput();
        input.warrantyClaimId = wcId;
        input.itemJSON = JSON.serialize(items);
        ExpenseClaimInvocationHandler.createWarrantyExpenseItems(new List<ExpenseClaimInvocationHandler.FlowInput>{input});
    }

    @testSetup
    private static void createData() {
        Warranty_Claim__c wc = new Warranty_Claim__c();
        wc.Name = 'test';
        
        insert wc;
    }
}