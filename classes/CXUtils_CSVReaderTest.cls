@isTest class CXUtils_CSVReaderTest {

	@isTest static void testCSVReader1() {
    String delimiter = ',';
		String csvString = 'fieldName1,fieldName2,fieldName3,fieldName4\r\n' +
		                   '"valu,e a1","value\nb1",value c1,\n' +
		                   'value a2,"value""b2","valu""e c2",\r\n' +
		                   ',value\"b3,value\'c3,\'value d3\'\n' +
		                   '"value,a4","",,\'value d4\'';

		List<List<String>> data = CXUtils_CSVReader.parse(csvString, delimiter);

		System.assertEquals('valu,e a1', data[1][0]);
		System.assertEquals('value\nb1', data[1][1]);
		System.assertEquals('value c1', data[1][2]);
		System.assertEquals('', data[1][3]);

		System.assertEquals('value a2', data[2][0]);
		System.assertEquals('value"b2', data[2][1]);
		System.assertEquals('valu"e c2', data[2][2], 'Expected handling of double quoted quotes');
		System.assertEquals('', data[2][3]);

		System.assertEquals('', data[3][0]);
		System.assertEquals('value"b3', data[3][1]);
		System.assertEquals('value\'c3', data[3][2]);
		System.assertEquals('\'value d3\'', data[3][3]);

		System.assertEquals('value,a4', data[4][0]);
		System.assertEquals('', data[4][1]);
		System.assertEquals('', data[4][2]);
		System.assertEquals('\'value d4\'', data[4][3]);
	}

	@isTest static void testCSVReader2() {
    String delimiter = ',';
		String csvString = 'fieldName1\r\n' +
		                   '"valu,e a1"\n' +
		                   'value a2\n' +
		                   '\n' +
		                   '""\n' +
		                   '"value,a4"';

		List<List<String>> data = CXUtils_CSVReader.parse(csvString, delimiter);

		System.assertEquals('valu,e a1', data[1][0]);
		System.assertEquals('value a2', data[2][0]);
		System.assertEquals('', data[3][0]);
		System.assertEquals('', data[4][0]);
		System.assertEquals('value,a4', data[5][0]);
	}

    @isTest
    static void testParseToMapList() {
        String delimiter = ',';
        String csvString = '';
        csvString += '"GUID","PARTNER","CURRENT_PHASE","ENTITY_ID","ENTITY_NAME","ERROR_DATE","ERROR_MESSAGE","ERROR_STATUS","DEBUG_STEP"\n';
        csvString += '"01FAABAC3FC77204E0640021280E8E4F","SFS-NA","CLOSED","3-D FARMS-73707","CUSTOMER_NUMBER-RELATED_CUSTOMER_NUMBER","02-AUG-23 08.15.12","External Id: more than one record found for external id field: [aA2DT000000D5WF0A0, aA2DT000000D4TV0A0]","NEW","CPI Failure"\n';
        csvString += '"01FAABAC3FC77204E0640021280E8E4F","SFS-NA","CLOSED","BIG 5 SPORTING GOODS-73707","CUSTOMER_NUMBER-RELATED_CUSTOMER_NUMBER","02-AUG-23 08.15.12","Record rolled back because not all records were valid and the request was using AllOrNone header","NEW","CPI Failure"\n';

        List<Map<String, String>> data = CXUtils_CSVReader.parseToMapList(csvString, delimiter);
        System.assertEquals('01FAABAC3FC77204E0640021280E8E4F', data[0].get('GUID'), 'Values not equal');
    }

    @isTest
    static void testParseBOMChar() {
        String delimiter = ',';
        String csvString = '\uFEFF';
        csvString += '"GUID","PARTNER","CURRENT_PHASE","ENTITY_ID","ENTITY_NAME","ERROR_DATE","ERROR_MESSAGE","ERROR_STATUS","DEBUG_STEP"\n';
        csvString += '"01FAABAC3FC77204E0640021280E8E4F","SFS-NA","CLOSED","3-D FARMS-73707","CUSTOMER_NUMBER-RELATED_CUSTOMER_NUMBER","02-AUG-23 08.15.12","External Id: more than one record found for external id field: [aA2DT000000D5WF0A0, aA2DT000000D4TV0A0]","NEW","CPI Failure"\n';
        csvString += '"01FAABAC3FC77204E0640021280E8E4F","SFS-NA","CLOSED","BIG 5 SPORTING GOODS-73707","CUSTOMER_NUMBER-RELATED_CUSTOMER_NUMBER","02-AUG-23 08.15.12","Record rolled back because not all records were valid and the request was using AllOrNone header","NEW","CPI Failure"\n';

        List<Map<String, String>> data = CXUtils_CSVReader.parseToMapList(csvString, delimiter);

        // If BOM char not removed, GUID would have quotes around it \"GUID\"
        System.assertEquals('01FAABAC3FC77204E0640021280E8E4F', data[0].get('GUID'), 'BOM Char not removed');
    }
}