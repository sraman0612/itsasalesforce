/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 08/23/2023
* @description: Test Class for SFS_CreditLimitHandler Apex Handler.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

=========================================================================================================================*/

@isTest
public class SFS_CreditLimitHandler_Test {
    @isTest
    public static void updateRelatedAccountTest(){
        String accRecID1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList1=SFS_TestDataFactory.createAccounts(1, false);
        accountList1[0].IRIT_Customer_Number__c='123';
        accountList1[0].CurrencyIsoCode='USD';
        accountList1[0].RecordTypeId=accRecID1;
        accountList1[0].SFS_Oracle_Site_Use_Id__c='234567';
        insert accountList1[0];
         
        List<SFS_Credit_Limit__c> clList = new List<SFS_Credit_Limit__c>();
        SFS_Credit_Limit__c cl = new SFS_Credit_Limit__c();
        cl.SFS_Oracle_Site_Use_Id__c = '234567';
        cl.SFS_Credit_Limit__c = 2230.23;
        cl.SFS_Current_Available_Amount__c=1000.00;
        cl.SFS_Past_Due_Amount__c =200.00;
        clList.add(cl);
        insert clList;
        clList[0].SFS_Credit_Limit__c=1000.12;
        update clList[0];
            
       }
   
}