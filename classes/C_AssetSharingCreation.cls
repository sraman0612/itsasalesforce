public class C_AssetSharingCreation {
/*Authored By: Jonathan Brooks
*Created: 1/29/2017
*Purpose: Replacing the process builder and flows to create proper sharing on Assets based on Community User Type of User.
*Updated by jonathan brooks 10/16/18 to fix the asset share for global users on conact share line 78
* 
* 
* 
*/
    public static void createAssetShareOnAssetInsert(List<Asset> aList)
    {
        //List that will hold all Asset Sharing until time for insert
        List<AssetShare> assetShareToCreate = new List<AssetShare>();
        
        for(Asset a : aList)
        {
            //Create sharing for all Single account users
            if(!string.isBlank(a.AccountId)){
                for(User u : [Select id,Partner_Account_ID__c from user 
                              where Partner_Account_ID__c = :string.valueOf(a.AccountId).substring(0,15)])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
            
            //Create sharing for Global Account users
            if(!string.isBlank(a.Parent_AccountID__c)){
                for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :a.Parent_AccountID__c])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
            
            //Create sharing for users who have the Current Servicer ID as their Partner Account ID
            if(!string.isBlank(a.Current_Servicer_ID__c)){
                for(User u : [Select id, Partner_Account_ID__c from user where Partner_Account_ID__c = :string.valueOf(a.Current_Servicer_ID__c).substring(0,15) ])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
            if(!string.isBlank(a.Current_Servicer_Parent_ID__c)){
                for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :string.valueOf(a.Current_Servicer_Parent_ID__c).substring(0,15)])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
            
        }
        //Insert sharing after loop
        database.insert(assetShareToCreate, false);
    }  
    
    /*@future 
    public static void createAssetShareOnContact(List<ID> cList)
    {
        //delete all asset share records for user associate to contact whose account just changed
        delete [Select id from assetshare where userorgroupid in (Select id from user where contactID in :cList)];
        
        List<assetShare> assetShareToCreate = new List<assetShare>();
        
        
        for(Contact c : [Select id,account.parentID,accountID,account.Floor_Plan_Account_ID__c from COntact where id IN :cList])
        {
            //global
            for(User u : [Select id from user where contactID in :cList and toLabel(community_user_type__c) = 'Global Account'])
            {
                if(!string.isBlank(string.valueOf(c.account.parentID).substring(0,15)) || !string.isBlank(c.accountID))
                {
                    for(Asset a : [Select id from Asset where (Parent_AccountID__c  = :string.valueOf(c.account.parentID).substring(0,15) OR accountID = :c.accountid)])
                    {                
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                }
                if(!string.isBlank(c.account.Floor_Plan_Account_ID__c))
                {
                    for(Asset a : [Select id from Asset where accountID =:string.valueOf(c.account.Floor_Plan_Account_ID__c).substring(0,15)])
                    {                    
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                }
                if(!string.isBlank(string.valueOf(c.account.parentID).substring(0,15)))
                {
                    for(Asset a : [Select id from Asset where Current_Servicer_Parent_ID__c = :string.valueOf(c.account.parentID).substring(0,15)])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);                   
                    }
                }
            }
            
            //primary
            for(User u : [Select id from user where contactID IN :cList and toLabel(community_user_type__c) = 'Single Account'])
            {
                for(Asset a : [Select id from Asset where accountID = :string.valueOf(c.accountID).substring(0,15)])
                {                   
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
                if(!string.isBlank(c.account.Floor_Plan_Account_ID__c)){
                    for(Asset a : [Select id from Asset where Floor_Plan_AccountID__c = :c.account.Floor_Plan_Account_ID__c ])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                }
                
                for(Asset a : [Select id from Asset where Current_Servicer_ID__c = :string.valueOf(c.accountid).substring(0,15)])
                {                   
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
        }
        
        insert assetShareToCreate;
        
    }*/
    
    public static void createServiceContractShareOnContact(List<ID> cList)
    {
        // Create a new list of sharing objects for Job
        List<Service_History__Share> jobShrs  = new List<Service_History__Share>();
        
        List<Service_History__Share> scShareToDel = [Select id from Service_History__Share where userorgroupid in (Select id from user where contactID in :cList)];
        if(scShareToDel.size() > 0)
            delete scShareToDel;
        Map<string, user> uMap = new Map<string, user>();
        List<string> conAcctID = new List<string>();
        //for(Contact c : [Select id,account.parentID,accountID,account.Floor_Plan_Account_ID__c from COntact where id IN :cList])
        //{           
            for(User u: [Select id,contactID,contact.accountid from user where contactID IN :cList])
            {
                uMap.put(u.id, u);
                conAcctID.add(u.contact.accountID);
                for(Service_History__c sc : [Select id,account__c from Service_History__c where account__c IN :conAcctID])
                {
                    Service_History__Share scShare = new Service_History__Share();
                    
                    // Set the ID of record being shared
                    scShare.ParentId = sc.Id;
                    
                    if(uMap.get(u.id).contact.accountid == sc.account__c )
                    {
                        // Set the ID of user or group being granted access
                        scShare.UserOrGroupId = u.id;
                    }
                    
                    // Set the access level
                    scShare.AccessLevel = 'Read';
                    
                    // Set the Apex sharing reason for hiring manager and recruiter
                    scShare.RowCause = 'Manual';
                    
                    // Add objects to list for insert
                    jobShrs.add(scShare);
                }
            }
        //}
        
        if(jobShrs.size()>0)
            database.insert(jobShrs, false);
    }
    
     @future
    public static void createLubricantSampleShareOnContact(List<ID> cList)
    {
        // Create a new list of sharing objects for Job
        List<Oil_Sample__Share> jobShrs  = new List<Oil_Sample__Share>();
        List<user> uList = [Select id, contact.accountid, Partner_Account_Parent_ID__c  from user where contactID in :cList];
        List<Oil_Sample__Share> scShareToDel = [Select id from Oil_Sample__Share where userorgroupid in (Select id from user where contactID in :cList) ];
        if(scShareToDel.size() > 0)
            database.delete(scShareToDel, false);
        
        //for(Contact c : [Select id,account.parentID,accountID,account.Floor_Plan_Account_ID__c from COntact where id IN :cList])
        //{
            for(User u: uList)
            {
                for(Oil_Sample__c sc : [Select id from Oil_Sample__c where Distributor__c = :u.contact.accountid OR Account_Parent_ID__c = :u.Partner_Account_Parent_ID__c ])
                {
                    Oil_Sample__Share scShare = new Oil_Sample__Share(); 
                    
                    // Set the ID of record being shared
                    scShare.ParentId = sc.Id;
                    
                    // Set the ID of user or group being granted access
                    scShare.UserOrGroupId = u.id;
                    
                    // Set the access level
                    scShare.AccessLevel = 'Read';
                    
                    // Set the Apex sharing reason for hiring manager and recruiter
                    scShare.RowCause = 'Manual';
                    
                    // Add objects to list for insert
                    jobShrs.add(scShare);
                }
            }
        //}
        
        if(jobShrs.size()>0)
            database.insert(jobShrs, false);
    }
    
     @future
    public static void createAssetShareSingleCommUsr(List<id> uList)
    {
        List<assetShare> assetShareToCreate = new List<assetShare>();
        List<Service_History__Share> ServiceHistoryShareToCreate = new List<Service_History__Share>();
        
        for(user u : [select id,contact.account.Floor_Plan_Account_ID__c,Partner_Account_ID__c from user where id in :uList])
        {
            if(!string.isBlank(u.Partner_Account_ID__c))
            {
                for(Asset a : [Select id from Asset 
                               where accountID = :u.Partner_Account_ID__c 
                               OR Current_Servicer_ID__c = :u.Partner_Account_ID__c ])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
                for(Service_History__c a : [Select id from Service_History__c where account__c = :u.Partner_Account_ID__c ])
                {
                    Service_History__Share share = new Service_History__Share();
                    share.AccessLevel='Edit';
                    share.ParentId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    ServiceHistoryShareToCreate.add(share);
                }
            }
            if(!string.isBlank(u.contact.account.Floor_Plan_Account_ID__c ))
            {
                for(Asset a : [Select id from Asset where accountID = :u.contact.account.Floor_Plan_Account_ID__c])
                {
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }
        }
        
        if(assetShareToCreate.size()>0)
            database.insert(assetShareToCreate, false);
        
        if(ServiceHistoryShareToCreate.size()>0)
            database.insert(ServiceHistoryShareToCreate, false);
    }
    
    
    //@future
    public static void createAssetShareGlobalUsr(List<ID> uList)
    {
        List<assetShare> assetShareToCreate = new List<assetShare>();
        for(User U : [Select id,contact.account.parentID,contact.account.Floor_Plan_Account_ID__c,name, Partner_Account_Parent_ID__c,Community_User_Type__c 
                      from User where id in :uList
                     and Partner_Account_Parent_ID__c != null])
        {
            system.debug(u);
            //if(!string.isBlank(u.Partner_Account_Parent_ID__c )){
                for(Asset a : [Select id,name,Parent_AccountID__c from Asset 
                               where Parent_AccountID__c = :u.Partner_Account_Parent_ID__c  
                               OR accountID = :u.contact.account.Floor_Plan_Account_ID__c
                               OR Current_Servicer_Parent_ID__c = :u.Partner_Account_Parent_ID__c])
                {
                    //system.debug('with parent ID: '+a + ': users parentID: '+u.contact.account.parentID);
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
                system.debug(Logginglevel.INFO ,'SIZE TO CREATE ASSET SHARE FOR GLOBAL USER***'+assetShareToCreate.size());
                //continue;
            //}
            /*if(!string.isBlank(u.contact.account.Floor_Plan_Account_ID__c )){
                for(Asset a : [Select id,name,accountID from Asset where accountID = :u.contact.account.Floor_Plan_Account_ID__c])
                {
                    //system.debug('with account ID: '+a+': users floorpan ID: '+u.contact.account.Floor_Plan_Account_ID__c);
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
                //continue;
            }
            if(!string.isBlank(u.Partner_Account_Parent_ID__c)){
                for(Asset a : [Select id,name,Current_Servicer_ID__c from Asset where Current_Servicer_Parent_ID__c = :u.Partner_Account_Parent_ID__c ])
                {
                    //system.debug('with current servicer ID: '+a+': users parentID: '+u.contact.account.Floor_Plan_Account_ID__c);
                    AssetShare share = new AssetShare();
                    share.assetAccessLevel='Edit';
                    share.AssetId=a.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    assetShareToCreate.add(share);
                }
            }*/
        }
        system.debug(Logginglevel.INFO , 'SIZE TO CREATE ASSET SHARE FOR GLOBAL USER***'+assetShareToCreate.size());
        
        globalUsrBatch(assetShareToCreate,uList);
        
        //create Asset Sharing
        //Database.SaveResult[] srList = Database.insert(assetShareToCreate, false);
        
        // Iterate through each returned result
        /*for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug(Logginglevel.INFO,'Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug(Logginglevel.INFO,'The following error has occurred.');                    
                    System.debug(Logginglevel.INFO, err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(Logginglevel.INFO, 'Account fields that affected this error: ' + err.getFields());
                }
            }
        }*/
    }
    
    public static void globalUsrBatch(List<assetshare> asList, List<id> uList)
    {
        List<assetshare> assetShareToCreate =  asList;
        List<id> usrlist = ulist;
        C_createAssetShareGlobalUsrBatch uBatch = new C_createAssetShareGlobalUsrBatch(assetShareToCreate,usrlist);//initialize batch class(passid)
        id processedBatch = Database.executeBatch(uBatch);
    }
    
    @future
    public static void createServiceHistoryShareUsr(List<ID> uList)
    {
        List<Service_History__Share> ServiceHistoryShareToCreate = new List<Service_History__Share>();
        
        for(User U : [Select id,contact.account.parentID,contact.account.Floor_Plan_Account_ID__c,name, Partner_Account_Parent_ID__c,Partner_Account_ID__c  from User where id in :uList])
        {
            system.debug(u);
            
            if(!string.isBlank(u.Partner_Account_ID__c)){
                for(Service_History__c sh : [Select id from Service_History__c where account__c = :u.Partner_Account_ID__c])
                {
                    Service_History__Share share = new Service_History__Share();
                    share.AccessLevel='Edit';
                    share.ParentId=sh.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    ServiceHistoryShareToCreate.add(share);
                }
            }
            
            //Create sharing for Global Account users
            if(!string.isBlank(u.Partner_Account_Parent_ID__c )){
                for(Service_History__c sh : [Select id from Service_History__c where Account_Parent_ID__c = :u.Partner_Account_Parent_ID__c])
                {
                    Service_History__Share share = new Service_History__Share();
                    share.AccessLevel='Edit';
                    share.ParentId=sh.id;
                    share.RowCause='Manual';
                    share.UserOrGroupId=u.id;
                    
                    ServiceHistoryShareToCreate.add(share);
                }
            } 
        }
        system.debug(ServiceHistoryShareToCreate.size());
        //create service history Sharing
        Database.SaveResult[] srList = Database.insert(ServiceHistoryShareToCreate, false);
        
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                //System.debug(Logginglevel.INFO,'Successfully inserted assetshare. assetshare ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug(Logginglevel.INFO,'The following error has occurred.');                    
                    System.debug(Logginglevel.INFO, err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(Logginglevel.INFO, 'assetshare fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
}