@isTest
public class SFS_InvoiceWoOutboundHandler_Test {
    @isTest
    public static void SFS_InvoiceWoOutboundHandler_Test()
    {
        //try{
            List<Account> accList=new List<Account>();
            Account acct = new Account();
        	acct.name = 'test account1';
        	acct.Currency__c='USD';
            acct.BillingCountry='USA';
            acct.AccountSource='Web';
            acct.BillingState='AP';
            acct.type = 'ship to';
            acct.IRIT_Customer_Number__c='123';
        	insert acct;
            accList.add(acct);

         List<Division__c> div = new List<Division__c> ();
        Division__c div1 = new Division__c( 
                                 Name = 'TestLoc',
                                 SFS_Org_Code__c = 'ABC',
                                 Division_Type__c='Customer Center' ,
                                 EBS_System__c='Oracle 11i');
        insert div1;
        div.add(div1);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct.Id ,true);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct.Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        //get charge
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,true);
        charge[0].CAP_IR_Status__c = 'Submitted';
        update charge[0];
        
        //get invoice
         List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,acct.Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Annually';
           svcAgreementList[0].SFS_Status__c = 'APPROVED';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        ID invRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Vendor_Invoice'].id;
         Invoice__c inv = new Invoice__c();
         //inv.recordTypeId = invRecTypeId;
         //inv.SFS_Work_Order__c=wo[0].id;
        //inv.SFS_Invoice_Type__c = 'Receivable';
        //inv.SFS_Invoice_Print_Code__c = '12';
                                       //SFS_Service_Agreement__c=svcAgreementList[0].Id,
                                       	//SFS_Status__c='Created',
                                       //SFS_Invoice_Format__c=svcAgreementList[0].SFS_Invoice_Format__c,
                                       //SFS_Billing_Period_End_Date__c=system.today(),
                                      
                                       //SFS_Service_Agreement_Line_Item__c=cli[0].Id,
                                       //SFS_Billing_Period_Start_Date__c= system.today()
                                      // );
        
         insert inv;
                
           //List<Invoice__c> invList =SFS_TestDataFactory.createInvoice(1,true);
            SFS_Invoice_Line_Item__c inli = new SFS_Invoice_Line_Item__c(SFS_Invoice__c=inv.Id,SFS_Work_Order__c=wo[0].Id/*,SFS_Work_Order_Line_Item__c=woliList[0].Id*/,SFS_Amount__c=123);
            insert inli;

            List<Id> Ids = new List<id>();
            Ids.add(charge[0].Id);
       
            SFS_InvoiceWoOutboundHandler.Run(Ids);
      /*}
        catch(exception e) {} */
    }     
          

}