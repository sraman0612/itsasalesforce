public class GDInsideSync {
    public Account[] Accounts;
    
    public class Account{
        public String first_name {get; set;}
        public String last_name {get; set;}
        public String sap_account_number {get; set;}
        public String username {get; set;}
    }
    
    
    public static GDInsideSync parse(String jsonString) {
        return (GDInsideSync)JSON.deserialize(jsonString, GDInsideSync.class);
    }
}