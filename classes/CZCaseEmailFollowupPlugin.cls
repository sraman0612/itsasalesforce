global class CZCaseEmailFollowupPlugin implements Process.Plugin{
    
    
    public  Process.PluginResult invoke(Process.PluginRequest request) {
        
        String emailResults = '';
        
        Id caseId = (Id) request.inputParameters.get('caseId');
        
        Id contactId = (Id) request.inputParameters.get('contactId');
        
        Id emailTemplateId = (Id) request.inputParameters.get('emailTemplateId');
        
        String ccEmailList = (String) request.inputParameters.get('ccEmailList');
        
        Case caseRecord = [Select Id, ContactId, Org_Wide_Email_Address_ID__c from Case where Id = :caseId];
        
        List<Attachment> attachmentList = [Select Id, Name, Body from Attachment where ParentId = :caseId order by CreatedDate desc];
        
        Attachment attachmentToUse;
        
        for(Attachment att : attachmentList ){
            
            System.debug('**Attachement Name - '+att.Name);
            
            if(att.Name.contains('.pdf') && (att.Name.contains('Quote') || att.Name.contains('quote'))){
                
                attachmentToUse = att;
                
                break;
                
            }
            
        }
        
        if(attachmentToUse != null){
            
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attachmentToUse.Name);
            efa.setBody(attachmentToUse.Body); // PDFBody is of blob data Type 
            
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            
            if(caseRecord.Org_Wide_Email_Address_ID__c != null) msg.setOrgWideEmailAddressId(caseRecord.Org_Wide_Email_Address_ID__c);
            
            if(ccEmailList != null) msg.setCcAddresses(ccEmailList.split(','));
            
            msg.setTemplateId(emailTemplateId);
            msg.setTargetObjectId(contactId); //  Here you can give Id of  User , Lead , Contact 
            msg.setWhatId(caseId); // Here you can give if of account ,asset, campaign ,Case , contract if TragetObject is contact
            
            msg.setFileAttachments(new Messaging.EmailFileattachment[]{efa});
            
            try{
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
                
                emailResults = 'Email Sent Successfuly';
                
            }catch(Exception e){
                
                emailResults = e.getMessage();
                
            }
            
        }else{
            
            emailResults = 'No PDF email attachements found on case to send';
        }
        
        
        Map<String,Object> result = new Map<String,Object>();
        result.put('emailResponse', emailResults);
        return new Process.PluginResult(result);
        
    }
    
   global Process.PluginDescribeResult describe() { 
      Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
      result.Name = 'Belliss Email Followup';
      result.Tag = 'Email Plugin';
      result.inputParameters = new 
         List<Process.PluginDescribeResult.InputParameter>{ 
            new Process.PluginDescribeResult.InputParameter('caseId', 
            Process.PluginDescribeResult.ParameterType.Id, true),
            new Process.PluginDescribeResult.InputParameter('contactId', 
            Process.PluginDescribeResult.ParameterType.Id, true),
            new Process.PluginDescribeResult.InputParameter('emailTemplateId', 
            Process.PluginDescribeResult.ParameterType.Id, true),
            new Process.PluginDescribeResult.InputParameter('ccEmailList', 
            Process.PluginDescribeResult.ParameterType.String, false)    
         }; 
      result.outputParameters = new 
         List<Process.PluginDescribeResult.OutputParameter>{              
            new Process.PluginDescribeResult.OutputParameter('emailResponse', 
            Process.PluginDescribeResult.ParameterType.STRING)
                }; 
      return result; 
   }

}