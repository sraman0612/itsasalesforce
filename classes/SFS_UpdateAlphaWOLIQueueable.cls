public class SFS_UpdateAlphaWOLIQueueable implements Queueable{
   public List<Id> mpList ; 
    
    
    public static void SFS_UpdateAlphaWOLIQueueable( List<Id> mpList){
          Integer delayInMinutes = 1;
         System.enqueueJob(new SFS_UpdateAlphaWOLIQueueable(mpList),delayInMinutes);
    }
    
    public SFS_UpdateAlphaWOLIQueueable(List<Id> mpList){
        this.mpList = mpList ;  
    }
    public void execute(QueueableContext context) {      
			Set<Id> mpIds= new Set<Id>();
            Set<Id> allWOLIIds= new Set<Id>();
        	Set<Id> woIds= new Set<Id>();
            Map<String,Set<Id>> skillReqMap=new Map<String,Set<Id>>();
        	Map<String,Double> sAppMap=new Map<String,Double>();
        	Map<String,Id> woliMap=new Map<String,Id>();
        	Map<Id,Id> woliMap2=new Map<Id,Id>();
        	Map<Id,WorkOrder> woMap=new Map<Id,WorkOrder>();
            Integer count=0;
            String AlphaWOLI,ParentRecord,WorkOrderId,WorkOrderIdCheck,woId,WoIdforPR;
            Double duration=0.00;
        	Id CtsWOLI;
            for(Id mp : mpList){
                mpIds.add(mp);
            }
        	List<WorkOrder> woListUpdate=new List<WorkOrder>();
            List<WorkOrder> woLists=[select id,AssetId,AccountId,Description,Subject from WorkOrder 
                                     where MaintenancePlanId IN:mpIds];
        	DateTime TimeNow = DateTime.now();
			DateTime TimeCheck = TimeNow.addMinutes(-10);
            List<WorkOrderLineItem> woliLists=[select id,WorkOrderId,SFS_Alpha_WOLI__c,Asset.AccountId,AssetId,LineItemNumber,MaintenanceWorkRule.SFS_Month__c from WorkOrderLineItem 
                                     where WorkOrder.MaintenancePlanId IN:mpIds and CreatedDate>:TimeCheck order by WorkOrderId,SFS_Rank__c nulls last];
            List<WorkOrderLineItem> WoliList=new List<WorkOrderLineItem>();
            List<WorkOrderLineItem> WoliListToUpdateAlpha=new List<WorkOrderLineItem>();
        	List<SkillRequirement> sreqList=new List<SkillRequirement>();
        	List<SkillRequirement> NewsreqList=new List<SkillRequirement>();
        	List<SkillRequirement> OldsreqList=new List<SkillRequirement>();
        	List<ServiceAppointment> saList=new List<ServiceAppointment>();
        	Id SARecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Not_Required').getRecordTypeId();
            Id WoliId;
        	Set<Id> skillReqId=new Set<Id>();
        	Set<Id> saId=new Set<Id>();
        	Set<Id> woliIds=new Set<Id>();
        	for(WorkOrder wo:woLists){
            	woMap.put(wo.Id,wo);
        	}
            for(WorkOrderLineItem woli : woliLists){
                if(woli.LineItemNumber=='WL-00000001'){
                    String strDesc='Preventive Maintenance-'+woli.MaintenanceWorkRule.SFS_Month__c;
                    WorkOrder woUpdate=woMap.get(woli.WorkOrderId);                 
                    woUpdate.AssetId=woli.AssetId;
                    woUpdate.Description=strDesc;
                    woUpdate.Subject=strDesc;
                   woListUpdate.add(woUpdate); 
                }
                if(woli.WorkOrderId!=WorkOrderId){
                    woli.SFS_Alpha_WOLI__c=true;
                 	WoliListToUpdateAlpha.add(woli);
                    WorkOrderId=woli.WorkOrderId;
                    woliMap.put(WorkOrderId, woli.Id);
                    woliMap2.put(woli.Id,WorkOrderId);
                }
                else{
                     allWOLIIds.add(woli.Id);
                }
                woliIds.add(woli.Id);
               
              }  
			update woListUpdate;
            update WoliListToUpdateAlpha;

        List<WorkOrderLineItem> woliListforSReq=[Select Id,WorkOrderId,(Select Id,RelatedRecordId,SkillId from SkillRequirements ) 
                                           from WorkOrderLineItem where Id IN:woliIds order by WorkOrderId];
        	for(WorkOrderLineItem sreq:woliListforSReq){
               	for(SkillRequirement sreqt:sreq.SkillRequirements){
                    OldsreqList.add(sreqt);
                	if(WorkOrderIdCheck!=sreq.WorkOrderId && skillReqId.size()>0){
                    	skillReqMap.put(WorkOrderIdCheck,skillReqId);
                    	skillReqId.removeAll(skillReqId);
                	}
                	skillReqId.add(sreqt.SkillId);
                	WorkOrderIdCheck=sreq.WorkOrderId;  
                    woIds.add(sreq.WorkOrderId);
            	}
        	}
        	if(skillReqId.size()>0)
                skillReqMap.put(WorkOrderIdCheck,skillReqId);
        	if(skillReqMap.size()>0){
        	for(Id woCheck:woIds){
                WorkOrderIdCheck=woCheck;
                for(Id oldSReq:skillReqMap.get(WorkOrderIdCheck)){
               		WoliId=woliMap.get(woCheck);
                	SkillRequirement sr=new SkillRequirement(RelatedRecordId=WoliId,SkillId=oldSReq);
                	NewsreqList.add(sr);
                }                
        	}
            if(OldsreqList.size()>0)
                delete OldsreqList;
        	insert NewsreqList;
            }        	
        
        	List<WorkOrderLineItem> woliListforSApp=[Select Id,WorkOrderId,(Select Id, Status, Duration,ParentRecordId from ServiceAppointments) 
                                           from WorkOrderLineItem where Id IN:woliIds order by WorkOrderId];
        	
        	for(WorkOrderLineItem sa:woliListforSApp){
               	for(ServiceAppointment sapp:sa.ServiceAppointments){
                    saList.add(sapp);
                	if(WorkOrderIdCheck!=sa.WorkOrderId && saId.size()>0){
                    	sAppMap.put(WorkOrderIdCheck,duration);
                        saId.removeAll(saId);
                    	duration=0.00;
                	}
                    saId.add(sapp.Id);
                	duration+=sapp.Duration;
                	WorkOrderIdCheck=sa.WorkOrderId;                  
            	}
        	}
            if(saId.size()>0)
                sAppMap.put(WorkOrderIdCheck,duration);
        	if(sAppMap.size()>0){
                for(ServiceAppointment saUpdate:saList){
                    woId=woliMap2.get(saUpdate.ParentRecordId);
                    saUpdate.SFS_Original_Duration__c=saUpdate.Duration;
                    if(woId!=null){
                       Double durUpdate=sAppMap.get(woId);
                       saUpdate.Duration=durUpdate;
                                            
                        if(durUpdate>8)
                            saUpdate.FSL__IsMultiDay__c=true;
                    }else{
                       	saUpdate.Status='Not Required';
                    	saUpdate.CTS_Work_Order_Line_Item__c=null;  
                        saUpdate.RecordTypeId=SARecordTypeId;
                    }                         
                    
                }
        		update saList;
            }
        
        List<WorkOrderLineItem> woliListforPR=[Select Id,WorkOrderId,WorkOrder.Contact.FirstName,WorkOrder.Contact.LastName,WorkOrder.MaintenancePlan.StartDate,
                WorkOrder.ServiceTerritory.SFS_Requested_Ship_Date_Offset__c,WorkOrder.AccountId,WorkOrder.Account.ShippingPostalCode,WorkOrder.Account.ShippingCountry,
                WorkOrder.Account.ShippingState,WorkOrder.Account.ShippingStreet,WorkOrder.Account.ShippingCity,WorkOrder.Contact.Phone,WorkOrder.ServiceContract.SFS_Shipping_Method__c,
                WorkOrder.ServiceContract.SFS_Shipping_Instructions__c,WorkOrder.SFS_Division__c,WorkOrder.ServiceContract.SFS_Division__c,
                (Select Id, WorkOrderId, WorkOrderLineItemId from ProductRequests) 
                from WorkOrderLineItem where Id IN:woliIds order by WorkOrderId];

        List<ProductRequest> prDelete = new List<ProductRequest>();
        List<ProductRequest> prInsert = new List<ProductRequest>();
        List<ProductRequest> prCheck = new List<ProductRequest>();
        List<ProductRequestLineItem> prliDelete = new List<ProductRequestLineItem>();
        List<ProductRequestLineItem> prliInsert = new List<ProductRequestLineItem>();
        List<ProductRequestLineItem> prliInsert2 = new List<ProductRequestLineItem>();
        List<ProductRequestLineItem> prliCheck = new List<ProductRequestLineItem>();
        Id PRUpdateWoId,PRUpdateWoliId; 
        String PRUpdateWoStr;
        Map<String,Id> prMap=new Map<String,Id>();
        
        for(WorkOrderLineItem wolipr:woliListforPR){
            PRUpdateWoStr=wolipr.Id;
         	for(ProductRequest prt:wolipr.ProductRequests){
              	prMap.put(PRUpdateWoStr,prt.Id);                 
           	}
        }
       	Date requestedShipDate;
       	Integer offset;
        Boolean shipDate=false;
        Id ShippingMethod,Division;
        for(WorkOrderLineItem wolipr:woliListforPR){
            PRUpdateWoStr=wolipr.Id;
            ShippingMethod=wolipr.WorkOrder.ServiceContract.SFS_Shipping_Method__c;
            Division=wolipr.WorkOrder.ServiceContract.SFS_Division__c;
            Id PRCheckId=prMap.get(PRUpdateWoStr);
            WoIdforPR=woliMap2.get(wolipr.Id);
            if(PRCheckId==null && WoIdforPR!=null){
                PRUpdateWoliId=woliMap.get(wolipr.WorkOrder.Id);
                PRUpdateWoId=woliMap2.get(wolipr.Id);
               	String contactName= wolipr.WorkOrder.Contact.FirstName+' '+wolipr.WorkOrder.Contact.LastName;
                if(wolipr.WorkOrder.ServiceTerritory.SFS_Requested_Ship_Date_Offset__c != null){
                  	offset=(1) * Integer.valueOf(wolipr.WorkOrder.ServiceTerritory.SFS_Requested_Ship_Date_Offset__c);
                   	requestedShipDate=System.today().addDays(offset);
                    shipDate=true;
                }
                if(shipDate==false){
                    requestedShipDate=System.today();
                }
                Id prRecordTypeId = Schema.SObjectType.ProductRequest.getRecordTypeInfosByDeveloperName().get('Service').getRecordTypeId();
                ProductRequest partRequest=new ProductRequest(AccountId=wolipr.WorkOrder.AccountId,
                    		SFS_Consumables_Ship_To_Account__c = wolipr.WorkOrder.AccountId,RecordTypeId=prRecordTypeId,
                   			SFS_Freight_Terms__c='Prepaid',SFS_No_Partials__c=true,SFS_Priority__c='250',
                    		SFS_Shipping_Instructions__c=wolipr.WorkOrder.ServiceContract.SFS_Shipping_Instructions__c,
                    		SFS_Shipping_Method__c=wolipr.WorkOrder.ServiceContract.SFS_Shipping_Method__c,
                    		SFS_Division__c=wolipr.WorkOrder.ServiceContract.SFS_Division__c,Status='Open',SFS_Contact_Name__c=contactName,
                    		SFS_Contact_Phone__c=wolipr.WorkOrder.Contact.Phone,WorkOrderId=PRUpdateWoId,WorkOrderLineItemId=PRUpdateWoliId,
                    		ShipToStreet=wolipr.WorkOrder.Account.ShippingStreet,ShipToCity=wolipr.WorkOrder.Account.ShippingCity,
                    		ShipToState=wolipr.WorkOrder.Account.ShippingState,ShipToCountry=wolipr.WorkOrder.Account.ShippingCountry,
                    		ShipToPostalCode=wolipr.WorkOrder.Account.ShippingPostalCode,SFS_Requested_Ship_Date__c=requestedShipDate);   
                prInsert.add(partRequest);
                System.debug('Debug PR Queue '+partRequest);
            }
        }
        if(prInsert.size()>0)
        	insert prInsert;
        
		List<ProductRequest> prList=[select Id,WorkOrderId,WorkOrderLineItemId,
    		(select Id,WorkOrderId,WorkOrderLineItemId,SFS_Asset__c,RecordTypeId,QuantityRequested,Product2Id from productrequestlineitems order by Id) 
             from ProductRequest where WorkOrderLineItemId IN:woliIds order by WorkOrderId,Id];
        
		List<ProductRequestLineItem> allPrliList= [select Id,WorkOrderId,WorkOrderLineItemId,SFS_Asset__c,RecordTypeId,QuantityRequested,Product2Id from ProductRequestLineItem 
              where WorkOrderLineItemId IN:allWOLIIds];
        
        for(ProductRequest pr:prList){
            if(PRUpdateWoId!=pr.WorkOrderId && allPrliList.size()>0){
                for(ProductRequestLineItem prliUpdate:allPrliList){
                    ProductRequestLineItem prli=new ProductRequestLineItem(WorkOrderId=prliUpdate.WorkOrderId,
                            WorkOrderLineItemId=prliUpdate.WorkOrderLineItemId,SFS_Asset__c=prliUpdate.SFS_Asset__c,
                            RecordTypeId=prliUpdate.RecordTypeId,QuantityRequested=prliUpdate.QuantityRequested,
                            Product2Id=prliUpdate.Product2Id,Status='Open');
                      prliInsert.add(prli);
                }
                allPrliList.clear();
            }               
            WoIdforPR=woliMap2.get(pr.WorkOrderLineItemId);
            if(WoIdforPR==null){
              	prDelete.add(pr);
                for(ProductRequestLineItem prlii:pr.productrequestlineitems){
                    prliDelete.add(prlii);   
                } 
             }else{
               	prCheck.add(pr);
                PRUpdateWoliId=pr.WorkOrderLineItemId;
          	}
            PRUpdateWoId=pr.WorkOrderId;            
        }
        if(allPrliList.size()>0){
        	for(ProductRequestLineItem prliUpdate:allPrliList){
                    ProductRequestLineItem prli=new ProductRequestLineItem(WorkOrderId=prliUpdate.WorkOrderId,
                            WorkOrderLineItemId=prliUpdate.WorkOrderLineItemId,SFS_Asset__c=prliUpdate.SFS_Asset__c,
                            RecordTypeId=prliUpdate.RecordTypeId,QuantityRequested=prliUpdate.QuantityRequested,
                            Product2Id=prliUpdate.Product2Id,Status='Open');
                      prliInsert.add(prli);
                }
                allPrliList.clear();
        }
        for(ProductRequest pr: prCheck){
        	for(ProductRequestLineItem prli : prliInsert){
         		if(pr.WorkOrderId == prli.WorkOrderId){                    
             		prli.ParentId=pr.Id;
                    prliInsert2.add(prli);
                    
          		}
      		} 
        }         
		if(prliInsert2.size()>0)
        	insert prliInsert2;
        if(prDelete.size()>0)
        	delete prDelete; 
        List<ProductRequest> prList1=[select Id,SFS_PRLI_Count__c,WorkOrder.Status from ProductRequest where WorkOrderLineItemId IN:woliIds];
        List<ProductRequest> prListUpdate=new List<ProductRequest>();
        List<ProductRequest> prListDel=new List<ProductRequest>();
        for(ProductRequest pr:prList1){
            if(pr.SFS_PRLI_Count__c==0){
                prListDel.add(pr);
            }else{
                if(pr.WorkOrder.Status== 'Open'){
                    pr.Status='Submitted';
                }
                pr.SFS_Shipping_Method__c=ShippingMethod;
                pr.SFS_Division__c=Division;
                prListUpdate.add(pr); 
            }
        }
        if(prListUpdate.size()>0)
            if(Test.isRunningTest()==False)
        		update prListUpdate;
        if(prListDel.size()>0)
        	delete prListDel;
    }
}