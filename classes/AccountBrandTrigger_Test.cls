@isTest
public with sharing class AccountBrandTrigger_Test {
	
	public static testMethod void doTest() {
		// Create some Data
		Account serenity = new Account( Name = 'Serenity' );
		insert serenity;
		ContentVersion cv = new ContentVersion( Title='testData', PathOnClient = 'serenity.jpg', VersionData = Blob.valueOf('This is test data'), isMajorVersion = true );
		insert cv;
		ContentVersion cvRetrieve = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];
		ContentAsset ca = [SELECT Id FROM ContentAsset LIMIT 1];
		//ContentAsset ca = new ContentAsset( ContentDocumentId = cvRetrieve.Id, DeveloperName = 'Firefly_class_ship', MasterLabel = 'Firefly_class_ship' );
		//insert ca;
		AccountBrand ab = new AccountBrand( AccountId = serenity.Id, Name = 'Serenity');
		
		// Do some stuff
		insert ab;
		
		// for coverage
		update ab;
		delete ab;
		undelete ab;
		
		// Validate
		Account verifyLogo = [SELECT Id, Distributor_Logo_URL__c FROM Account WHERE Id = :serenity.Id];
		System.assertEquals( ab.LogoUrl, verifyLogo.Distributor_Logo_URL__c );
	}    
}