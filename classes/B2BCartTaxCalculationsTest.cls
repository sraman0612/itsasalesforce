/**
* @author           Amit Datta
* @description      Test Class for B2BCartTaxCalculations.
*
* Modification Log
* ------------------------------------------------------------------------------------------
*         Developer                   Date                Description
* ------------------------------------------------------------------------------------------
*         Amit Datta                  21/02/2024          Original Version
**/

@isTest
public with sharing class B2BCartTaxCalculationsTest {
    
    @testSetup static void setup() {
        Account account = new Account(Name='TestAccount');
        insert account;
        WebStore webStore = new WebStore(Name='TestWebStore');
        insert webStore;
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id);
        insert cart;
        CartDeliveryGroup cartDeliveryGroup = new CartDeliveryGroup(CartId=cart.Id, Name='Default Delivery', DeliverToStreet= 'Valtech Detroit W 4th St. Royal Oak', DeliverToCity = 'Royal Oak', DeliverToState = 'MI', DeliverToPostalCode= '48067');
        insert cartDeliveryGroup;
        insertCartItemAndTax(cart.Id, cartDeliveryGroup.Id);
    }
    @isTest static void startCartProcessAsyncWhenSalesTaxAvailableTest() {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('zipTaxAPITestResponse');
        mock.setStatusCode(200);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        B2BCartTaxCalculations cartTax = new B2BCartTaxCalculations();
        sfdc_checkout.IntegrationStatus integrationResult = cartTax.startCartProcessAsync(integInfo, webCart.Id);
        System.assertEquals(sfdc_checkout.IntegrationStatus.Status.SUCCESS, integrationResult.status);
        Test.stopTest();
    }
    @isTest static void startCartProcessAsyncWhenSalesTaxNotAvailableTest() {
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new COR_HTTP_Mock());
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        B2BCartTaxCalculations cartTax = new B2BCartTaxCalculations();
        sfdc_checkout.IntegrationStatus integrationResult = cartTax.startCartProcessAsync(integInfo, webCart.Id);
        System.assertEquals(sfdc_checkout.IntegrationStatus.Status.FAILED, integrationResult.status);
        Test.stopTest();
    }

    @isTest static void startCartProcessAsyncWhenCalloutException() {
        
        Test.startTest();
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        B2BCartTaxCalculations cartTax = new B2BCartTaxCalculations();
        sfdc_checkout.IntegrationStatus integrationResult = cartTax.startCartProcessAsync(integInfo, webCart.Id);
        System.assertEquals(sfdc_checkout.IntegrationStatus.Status.FAILED, integrationResult.status);
        Test.stopTest();
    }
    
    
    
    // Inserts a cart item that matches the cart and cart delivery group
    static void insertCartItemAndTax(String cartId, String cartDeliveryGroupId) {
        CartItem cartItem = new CartItem(
            CartId=cartId, 
            Sku='SKU_Test1', 
            Quantity=3.0, 
            Type='Product', 
            Name='TestProduct', 
            TotalPrice = 5000,
            CartDeliveryGroupId=cartDeliveryGroupId
        );
        insert cartItem;
        CartTax tax = new CartTax( 
            Amount = 100,
            CartItemId = cartItem.Id,
            Name = 'SalesTax',
            TaxCalculationDate = Date.today(),
            TaxType = 'Estimated'
        );
        insert tax;
    }
    
}