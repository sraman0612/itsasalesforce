@IsTest 
public class SFS_createReturnOrderTest {
    @testSetup static void setup(){
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType='Account'].id;
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[1].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        accList[1].IRIT_Payment_Terms__c='BANKCARD';
        insert accList[1];
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].Account_Division__c=divisionsList[0].Id;
        accList[0].ShippingPostalCode='28759';
        accList[0].Type='Prospect';
        accList[0].ShippingCity ='Montreat2';
        insert accList[0];
        Contact con = SFS_TestDataFactory.getContact();
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        insert svcAgreementList;
        
        OperatingHours op= new OperatingHours(Name='Normal',TimeZone='America/Chicago');
        insert op;
        
        String svcPriceBookId=System.Label.SFS_Default_Price_Book;
        
        List<Product2> productsList=SFS_TestDataFactory.createProduct(2,false);
        insert productsList;
        System.debug('29 prod Id '+productsList[0].Id);
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = productsList[0].Id, UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        //PricebookEntry customEntry = new PricebookEntry(Pricebook2Id = svcPriceBookId, Product2Id = productsList[0].Id, UnitPrice = 10000, IsActive = true);
        //insert customEntry;
        
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,null,divisionsList[0].Id,svcAgreementList[0].Id,false);
        woList[0].Pricebook2Id=pricebookId;
        insert woList;
        WorkOrder wo=[Select Id,priceBook2Id from WorkOrder where id=:woList[0].Id];
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,workTypeList[0].Id,false);
        woliList[0].PricebookEntryId=standardPrice.id;
        insert woliList;        
        List<ProductRequest> prList=SFS_TestDataFactory.createProductRequest(1,null,woliList[0].Id,false);
        insert prList;
        
        List<ProductRequestLineItem> prliList=SFS_TestDataFactory.createPRLI(2,prList[0].Id,null,false);
        prliList[0].Product2Id=productsList[0].Id;
        prliList[1].Product2Id=productsList[1].Id;
        insert prliList;
        
        String LocationRecordTypeId = Schema.SObjectType.Location.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
        
        List<Schema.Location> locationList=SFS_TestDataFactory.createLocations(1, divisionsList[0].Id, false);
        locationList[0].IsInventoryLocation=true;
        locationList[0].RecordTypeId = LocationRecordTypeId;
        insert locationList;
        
        List<ServiceAppointment> saList= SFS_TestDataFactory.createServiceAppointment(1,woliList[0].Id,false);
        saList[0].OwnerId=userinfo.getUserId();
        insert saList;
        
        ServiceResource sr = new ServiceResource(Name = 'testSR', IsActive = true,RelatedRecordId = saList[0].OwnerId);
        insert sr;
        
        List<ProductTransfer> prodTransferList=SFS_TestDataFactory.createProductTransfer(1,productsList[0].Id,locationList[0].Id,false);
        prodTransferList[0].ProductRequestLineItemId=prliList[0].Id;
        prodTransferList[0].QuantityReceived=1;
        insert prodTransferList;
        
        ProductItem pI = new ProductItem(Product2Id = productsList[0].Id, LocationId = locationList[0].Id, QuantityOnHand = 4);
        insert pI;
        
        /*List<ProductConsumed> productConsumedList= SFS_TestDataFactory.createProductConsumed(1,woliList[0].Id,pI.Id,false);
productConsumedList[0].PricebookEntryId=customEntry.Id;
productConsumedList[0].WorkOrderId=woList[0].Id;
productConsumedList[0].SFS_Serial_Number__c='4627';
insert productConsumedList;*/
    }
    Static testmethod void testcreateReturnOrder(){
        Test.startTest();
        ProductItem pItem=[Select Id from ProductItem];
        List<WorkOrderLineItem> woliList=[Select Id from WorkOrderLineItem];
        List<WorkOrder> woList=[Select Id from WorkOrder];
        List<ProductConsumed> productConsumedList= SFS_TestDataFactory.createProductConsumed(1,woliList[0].Id,pItem.Id,false);
        //productConsumedList[0].PricebookEntryId=customEntry.Id;
        productConsumedList[0].WorkOrderId=woList[0].Id;
        productConsumedList[0].SFS_Serial_Number__c='4627';
        //insert productConsumedList;
        List<ServiceAppointment> saList=[Select Id from ServiceAppointment];
        
        List<Id> idsList = new List<Id>();
        idsList.add(saList[0].Id);
        
        SFS_createReturnOrder.createReturnOrders(idsList);
        Test.stopTest();
    } 
}