/**
 * @author           Amit Datta
 * @description      To get tax from the external service based on the address provided.
 *
 * Modification Log
 * ------------------------------------------------------------------------------------------
 *         Developer                   Date                Description
 * ------------------------------------------------------------------------------------------
 *         Amit Datta                  21/02/2024          Original Version
 **/

public with sharing class B2BTaxCalculator {

    private static final String QUERY_INDICATOR = '?';
    private static final String QUERY_SEPERATOR = '&';
    private static final String EQUAL_STRING = '=';
    private static final String ZIP_TAX_KEY_VALUE= 'ZipTaxKey';
    private static final String ZIP_TAX_KEY= 'key';
    private static final String ZIP_TAX_ADDRESS = 'address';
    private static final String ZIP_TAX_POSTAL_CODE = 'postalcode';
    private static final String ZIP_TAX_STATE_CODE = 'state';
    private static final String ZIP_TAX_CITY = 'city';

    public class TaxAddressWrapper {
        public String fullAddress;
        public String postalCode;
        public String stateCode;
        public String city;
        public String format;
    }
    
    public static Double getSalesTaxByAddress(TaxAddressWrapper addressWrapper) {
        String queryString = getZipTaxQueryString(addressWrapper);
        String respBody = '';
            respBody = doZipTaxCallout(queryString);
            Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(respBody);
            if(String.valueOf(m?.get('rCode')) == '100' ) {
                List<Object> results = (List<Object> )m.get('results');
                Map<String, Object> result =  (Map<String, Object>)results[0];
                return (Double)result.get('taxSales');
            }
        return null;
    }

    private static String getZipTaxQueryString(TaxAddressWrapper addressWrapper) {
        String queryString= '';
        B2B_IR_Store_Setting__mdt zipTaxkey = B2B_IR_Store_Setting__mdt.getInstance(ZIP_TAX_KEY_VALUE);
        queryString=QUERY_INDICATOR + ZIP_TAX_KEY + EQUAL_STRING + zipTaxkey.value__c;

        if(!String.isEmpty(addressWrapper?.fullAddress)) {
            queryString += QUERY_SEPERATOR + ZIP_TAX_ADDRESS + EQUAL_STRING + EncodingUtil.urlEncode(addressWrapper.fullAddress, 'UTF-8');
        }
        if(!String.isEmpty(addressWrapper.postalCode)) {
            queryString += QUERY_SEPERATOR + ZIP_TAX_POSTAL_CODE + EQUAL_STRING + addressWrapper.postalCode;
        }
        if(!String.isEmpty(addressWrapper.stateCode)) {
            queryString += QUERY_SEPERATOR + ZIP_TAX_STATE_CODE + EQUAL_STRING + addressWrapper.stateCode;
        }
        if(!String.isEmpty(addressWrapper.city)) {
            queryString += QUERY_SEPERATOR + ZIP_TAX_CITY + EQUAL_STRING +  EncodingUtil.urlEncode(addressWrapper.city, 'UTF-8');
        }
        return queryString;
    }

    private static String doZipTaxCallout(String queryString) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:zipTax'+queryString);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res.getBody();
    }
}