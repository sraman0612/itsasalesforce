@isTest
public class ensxtx_TSTC_MaterialBOM
{
    public class MOC_ensxtx_SBO_EnosixMaterialBOM_Search implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        private boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext)
        {
            if (throwException)
            {
                throw new CalloutException();
            }

            ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SR sr = new ensxtx_SBO_EnosixMaterialBOM_Search.EnosixMaterialBOM_SR();

            ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT result = new ensxtx_SBO_EnosixMaterialBOM_Search.SEARCHRESULT();
            result.SoldSeparately = true;
            result.BOMItemNumber = '10';
            result.BOMComponent = 'Material';
            result.ItemDescription = 'Description';
            result.ComponentQuantity = 1;
            sr.SearchResults.add(result);
            sr.setSuccess(this.success);
            searchContext.baseResult = sr;
			return searchContext;
        }
    }

    @isTest
    public static void test_getMaterialBOM()
    {
        MOC_ensxtx_SBO_EnosixMaterialBOM_Search mocSBO = new MOC_ensxtx_SBO_EnosixMaterialBOM_Search();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_EnosixMaterialBOM_Search.class, mocSBO);

        Map<String, Object> searchParams = new Map<String, Object>{
            'material' => 'material',
            'plant' => 'plant',
            'salesOrganization' => '1111',
            'distributionChannel' => '123'
        };
        Map<String, Object> pagingOptions = new Map<String, Object>();
        ensxtx_UTIL_Aura.Response response = ensxtx_CTRL_MaterialBOM.getMaterialBOM(searchParams, pagingOptions);

        mocSBO.setThrowException(true);
        response = ensxtx_CTRL_MaterialBOM.getMaterialBOM(searchParams, pagingOptions);
    }
}