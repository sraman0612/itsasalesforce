@isTest
public with sharing class TSTU_MaterialInventorySyncBatch
{
    static final string TEST_JSON =
        '{"UTIL_MaterialInventorySyncBatch.SalesOrg": "GDMI",' +
        '"UTIL_MaterialInventorySyncBatch.DistributionChannel": "CM",' +
        '"UTIL_MaterialInventorySyncBatch.CustomerNumber": "",' +
        '"UTIL_MaterialInventorySyncBatch.Logging": true}';

    public class MockSyncSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        private boolean success = true;

        public void setSuccess(boolean success)
        {
            this.success = success;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext)
        {
            if (this.throwException)
            {
                throw new UTIL_SyncHelper.SyncException('');
            }

            SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR searchResult =
                new SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR();

            // New Account
            SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT result1 =
                new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();

            result1.Material = 'Material1';
            result1.DistributionChannel = 'CM';
            result1.Scheduled = 10;
            result1.Stock = 15;

            searchResult.SearchResults.add(result1);

            // Existing Account
            SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT result2 =
                new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();

            result2.Material = 'Material2';
            result2.DistributionChannel = 'CM';
            result2.Scheduled = 5;
            result2.Stock = 10;

            searchResult.SearchResults.add(result2);

            SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT result3 =
                new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();

            result3.Material = 'Material3';
            result3.DistributionChannel = 'CM';
            result3.Scheduled = 1;
            result3.Stock = 0;

            searchResult.SearchResults.add(result3);

            SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT result4 =
                new SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT();

            result4.Material = 'Material4';
            result4.DistributionChannel = 'DT';
            result4.Scheduled = 4;
            result4.Stock = 2;

            searchResult.SearchResults.add(result4);

            searchResult.setSuccess(this.success);
            searchContext.baseResult = searchResult;
            return searchContext;
        }
    }

    public static testMethod void test_MaterialInventorySync()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMaterialInventorySync_Search.class, new MockSyncSearch());

        UTIL_AppSettings.resourceJson = TEST_JSON;
        createExistingObject();
        Test.startTest();
        Product2 prod = new Product2();
        prod.Name = 'Name';
        prod.ProductCode = 'Material1';
        prod.FLD_Distribution_Channel__c = 'CM';
        insert prod;
        UTIL_MaterialInventorySyncBatch controller = new UTIL_MaterialInventorySyncBatch();
        UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
        lastSync.retryCnt = 1;
        controller.setBatchParam(lastSync);
        Database.executeBatch(controller);
        Test.stopTest();
    }

    public static testMethod void test_MaterialInventorySyncFailure()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMaterialInventorySync_Search.class, mockSyncSearch);
        mockSyncSearch.setSuccess(false);

        createExistingObject();
        Test.startTest();
        UTIL_MaterialInventorySyncBatch controller = new UTIL_MaterialInventorySyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
        }
        catch (Exception e) {}
        Test.stopTest();
    }

    public static testMethod void test_MaterialInventorySyncException()
    {
        MockSyncSearch mockSyncSearch = new MockSyncSearch();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixMaterialInventorySync_Search.class, mockSyncSearch);
        mockSyncSearch.setThrowException(true);

        createExistingObject();
        Test.startTest();
        UTIL_MaterialInventorySyncBatch controller = new UTIL_MaterialInventorySyncBatch();
        Database.executeBatch(controller);
        try
        {
            UTIL_SyncHelper.LastSync lastSync = new UTIL_SyncHelper.LastSync();
            lastSync.retryCnt = 11;
            controller.setBatchParam(lastSync);
            controller.start(null);
        }
        catch (Exception e) {}
        Test.stopTest();
    }
    
    public static testMethod void test_UTIL_MaterialInventorySyncSchedule()
    {
     	Test.StartTest();
		UTIL_MaterialInventorySyncSchedule sh1 = new UTIL_MaterialInventorySyncSchedule();
		String sch = '0 0 23 * * ?'; 
        system.schedule('Test Material Inventory Sync Schedule', sch, sh1); 
        Test.stopTest();
    }   

    private static void createExistingObject()
    {
        Product2 currentObject = new Product2();
        currentObject.Name = 'Material1';
        currentObject.put(UTIL_MaterialInventorySyncBatch.SFSyncKeyField,'Material1 - CM');
        insert currentObject;

        Product2 currentObject3 = new Product2();
        currentObject3.Name = 'Material3';
        currentObject3.put(UTIL_MaterialInventorySyncBatch.SFSyncKeyField,'Material3 - CM');
        insert currentObject3;

        OBJ_SAP_Sync__c sapSync = new OBJ_SAP_Sync__c();
        sapSync.Name = 'UTIL_MaterialInventorySyncSchedule';
        sapSync.FLD_Sync_DateTime__c = System.today().addDays(-1);
        sapSync.FLD_Page_Number__c = 0;
        upsert sapSync;
    }
}