public class CTRL_SAP_StartupClaimStatus {

    @AuraEnabled
    public static UTIL_Aura.Response getStatus(String startupClaimId) {
        if (String.isBlank(startupClaimId)) {
            return UTIL_Aura.createResponse(null);
        }
        
        try {
            Machine_Startup_Form__c startupClaim = [SELECT Id, RecordType.DeveloperName, SAP_Status__c, SAP_Messages__c from Machine_Startup_Form__c where Id = : startupClaimId LIMIT 1];
            
            return UTIL_Aura.createResponse(startupClaim);
        } catch (Exception e) {
            
        }
        
        return UTIL_Aura.createResponse(null);
    }

}