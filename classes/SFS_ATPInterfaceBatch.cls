global class SFS_ATPInterfaceBatch implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts {
    
    global List<id> selectedList;
    
    public SFS_ATPInterfaceBatch(List<id> selectedList) {
        this.selectedList = selectedList;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT SFS_External_Id__c,SFS_Target_System__c,QuantityRequested,SFS_OrganizationCode__c,SFS_Requested_Warehouse__c,SFS_RequestShipmentDate__c,SFS_Part_Number__c,SFS_Oracle_Number__c,SFS_Ship_To_Site__c,SFS_WO_Ship_To_Site__c,Id from ProductRequestLineItem where Id IN:selectedList';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<ProductRequestLineItem> scope) {
        Map<String,Object> productRequestLineItemFields = new Map <String, Object>();
        Map<String, String> divMap = new Map<String, String>(); 
         List<Division__c> divList = [SELECT Id,Name, SFS_Org_Code__c from Division__c where Name!= null AND SFS_Org_Code__c != null]; 
        for(Division__c div : divList){
            divMap.put(div.Name, div.Id);
        }
        for(ProductRequestLineItem prli : scope){
           
            productRequestLineItemFields = prli.getPopulatedFieldsAsMap();
            System.debug('@@@@@productRequestLineItemFields'+productRequestLineItemFields);
            System.debug('@@@@@prli'+prli);
            SFS_ATPInterfaceOutboundIntegration.xmlStructure(productRequestLineItemFields,prli,divMap);
        }       
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}