/*=========================================================================================================
* @author Mahesh Raj, Capgemini
* @date 21/07/2023
* @description: Batch Class to trigger integration for WO with NA Status 

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

===========================================================================================================*/

public class SFS_batchtorunWOwithSAStatusNA implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful {
 	public List<WorkOrder> WONA = new List<WorkOrder>();
 	    public Database.QueryLocator start(database.BatchableContext bc) {
 		String Query =  'Select Id, SFS_Integration_Status__c,AccountId,EntitlementId,Entitlement.Name,Entitlement.ServiceContractId,AssetId,SFS_Division__c,WorkOrderNumber,SFS_External_Id__c,IsGeneratedFromMaintenancePlan,SFS_Bill_to_Account__c,ServiceContractId from WorkOrder where SFS_Integration_Status__c = \'N/A\' and  MaintenancePlanId != Null  and Entitlement.ServiceContractId != Null and EntitlementId != Null and Status =\'Open\'' ;
            return Database.getQueryLocator(Query);
        }
        public void execute(Database.BatchableContext bc, List<WorkOrder> scope){
        List<WorkOrder> WOToUpdate = New List<WorkOrder>();
          for(WorkOrder Wo : scope){
            if(Wo.SFS_Integration_Status__c == 'N/A'){
            	Wo.SFS_Integration_Resubmit__c =true; 
                WOToUpdate.add(Wo);                  
                System.enqueueJob(new SFS_WOforNAStatusQueueable(WOToUpdate));
            }
          }
        }
    public void finish(Database.BatchableContext bc){ 
                
    }
    }