/*
 * Author   : Nocks Emmanuel Mulea 
 * Company  : Canpango LLC
 * Email    : emmanuel.mulea@canpango.com
 * 
 * 
 * 
 */


public class C_GDIiConnMeasurmentsService {
    
    static Map<string,contact> conMap = new Map<string,contact>();
    static Map<String, GDInsideSync.Account> gdiMap = new Map<String, GDInsideSync.Account>();
    static map<string, GDInsideSync.Account> contactsToCreate = new map<string, GDInsideSync.Account>();
    static Map<string,string> sfAcctIds = new Map<string,string>();
    static List<string> sapAcctNumbs = new List<string>();
    static List<Contact> createC = new List<Contact>();
    static Contact c;
    static String response;
    static string endPointToUse = '';
    static String gdiUserName;
    static String gdiPassword;
    static String baseURL;
    
    public static void syncMeasurements(string endPoint,list<id> AstID){

        iConn_Admin__c iconnAdmin = iConn_Admin__c.getOrgDefaults();
        gdiUserName = iconnAdmin.Username__c;
        gdiPassword = iconnAdmin.Password__c;
        baseURL = iconnAdmin.API_URL__c;

        
        try{
           
            List<Asset> ast = [SELECT id, name,IMEI__c,Cumulocity_ID__c FROM Asset WHERE id IN :AstID FOR UPDATE];
            List<Asset> updateAst = new List<Asset>();

            for(Asset a:ast){

                    endPointToUse = baseURL + '/inventory/managedObjects/'+String.valueOf(a.Cumulocity_ID__c);
                    HTTPRequest req = new HTTPRequest();
                    req.setEndpoint(endPointToUse);
                    req.setMethod('GET');
                    
                    Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
                    String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                    req.setHeader('authorization', authorizationHeader);
                    req.setHeader('Accept', '*/*');
                    req.setHeader('Content-Type','application/json');
                    
                    HTTP h = new HTTP();
                    HTTPResponse resp = h.send(req);

                    iConn_Inventory.IConn_InventoryResult icon = iConn_Inventory.parse(resp.getBody());
                    Asset newAst = new Asset(Id = a.Id);

                    //Hours
                    newAst.iConn_c8y_H_total__c = icon.c8y_H_Total;
                    newAst.iConn_c8y_H_load__c = icon.c8y_H_Load;
                    newAst.iConn_c8y_H_service__c = icon.c8y_H_Service;
                    newAst.iConn_c8y_H_stal_1__c = null;

                    //Controller
                    newAst.iConn_Controller_Type__c = icon.type_Z;
                    newAst.iConn_Controller_ID__c = icon.c8y_Hardware_serialNumber;
                    if(icon.lastUpdated != null){
                        try{
                            String lDate = icon.lastUpdated;
                            lDate = lDate.replace('T', ' ');
                            lDate = lDate.replace('Z', '');
                            newAst.iConn_Controller_Last_Updated__c = Datetime.valueOfGMT(lDate);
                        }
                        catch(exception e){
                            System.debug('Error Parsing Date: ==>'+e.getMessage());
                        }
                    }
                
                	if(icon.creationTime != null){
                        try{
                            String lDate = icon.creationTime;
                            lDate = lDate.replace('T', ' ');
                            lDate = lDate.replace('Z', '');
                            newAst.iConn_Creation_Time__c = Datetime.valueOfGMT(lDate);
                        }
                        catch(exception e){
                            System.debug('Error Parsing Date: ==>'+e.getMessage());
                        }
                    }


                    //Firmware
                    if(icon.c8y_Firmware != null){
                        newAst.iConn_Module_Version__c = icon.c8y_Firmware.moduleVersion;
                        newAst.iConn_Firmware_Name__c = icon.c8y_Firmware.name;
                        newAst.iConn_Firmware_Version__c = icon.c8y_Firmware.version;

                        //Dates - values are GMT
                        if(icon.c8y_Firmware.generationDate != null){
                            try{
                            String gDate = icon.c8y_Firmware.generationDate;
                            gDate = gDate.replace('T', ' ');
                            gDate = gDate.replace('Z', '');
                            newAst.iConn_Generation_Date__c = Datetime.valueOfGMT(gDate);
                            }
                            catch(exception e){
                                System.debug('Error Parsing Date: ==>'+e.getMessage());
                            }
                        }

                        if(icon.c8y_Firmware.installationDate != null){
                            try{
                                String iDate = icon.c8y_Firmware.installationDate;
                                iDate = iDate.replace('T', ' ');
                                iDate = iDate.replace('Z', '');
                                newAst.iConn_Installation_Date__c = Datetime.valueOfGMT(iDate);
                            }
                            catch(exception e){
                                System.debug('Error Parsing Date: ==>'+e.getMessage());
                            }
                        }
                        
                    }

                    //Modem
                    newAst.iConn_Modem_Notes__c = icon.c8y_Notes;
                    if(icon.c8y_Hardware != null){
                        newAst.iConn_Modem_Hardware__c = icon.c8y_Hardware.model;
                    }

                    updateAst.add(newAst); 
            }
            
            update updateAst;
            
        } 
        catch(Exception e){
            System.debug('Error: ==>'+e.getMessage());
        }
        
    }
    
    public static  void alarmEvreyFiveMins(){

        iConn_Admin__c iconnAdmin = iConn_Admin__c.getOrgDefaults();
        gdiUserName = iconnAdmin.Username__c;
        gdiPassword = iconnAdmin.Password__c;
        baseURL = iconnAdmin.API_URL__c;

        
        try{
            string endPointToUse = '';
            string endPointToUsedForMeasurement = '';
            string dateTimefrom = string.valueof(DateTime.now().addMinutes(-6).format('yyyy-MM-dd HH:mm:ss', 'GMT'));// reason of using -6 is that to avoid to miss alerts that are being created between time of the excution
            list<object> dateTimefromList =dateTimefrom.split(' ');
            string dateTimeTo = string.valueof(DateTime.now().format('yyyy-MM-dd HH:mm:ss', 'GMT'));
            list<object> dateTimeToList =dateTimeTo.split(' ');
            list<Alert__c> updatesAlts = new list<Alert__c>();
            
            endPointToUse = baseURL + '/alarm/alarms?'+'&pageSize=2000&dateFrom='+dateTimefromList[0]+'T'+dateTimefromList[1]+'.000Z&dateTo='+dateTimeToList[0]+'T'+dateTimeToList[1]+'.000Z&pageSize=2000';
            
            system.debug('kk'+endPointToUse);
            HTTPRequest r = new HTTPRequest();
            r.setEndpoint(endPointToUse);
            r.setMethod('GET');
            
            Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            r.setHeader('authorization', authorizationHeader);
            r.setHeader('Accept', '*/*');
            //r.setHeader('Accept','application/json-stream');
            r.setHeader('Content-Type','application/json');    
            
            HTTP h = new HTTP();
            HTTPResponse resp = h.send(r);
            response = resp.getBody();
            //response = response.substring(1,response.length()-1);
            
            
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
            
            List<Object> lstalarms = (List<Object>)results.get('alarms');
            list<integer> CumulocityID = new list<integer>();
            Map<integer,Asset> astsMaped = new Map<integer,Asset>();
            
            // add all alarm source id into a list in order to make a select statement WHERE Cumulocity_ID__c IN :alarm source id
            for(Object lstalarm:lstalarms){
                Map<String, Object> customerAttributes = (Map<String, Object>)lstalarm;
                Map<String, Object> customerAttributesById = (Map<String, Object>)customerAttributes.get('source');
                if(!CumulocityID.contains(integer.valueof(customerAttributesById.get('id')))){
                    CumulocityID.add(integer.valueof(customerAttributesById.get('id')));
                    System.debug('WHY IS IT NOT SHOWING**: '+CumulocityID);
                }
            }
            
            // select all assets by Cumulocity ID 
            list<Asset> asts = [SELECT id, name,IMEI__c,Cumulocity_ID__c,Current_Servicer_Owner__c,Current_Servicer__c FROM Asset WHERE Cumulocity_ID__c IN :CumulocityID];
            list<Asset>defaultAsset = [SELECT id, name FROM asset WHERE Name='iConnMachine' LIMIT 1];
            system.debug('check'+asts);
            // mapp all assets by Cumulocity ID in order to know which alarm source id does not have assets when you do compare in the next for loop
            for(Asset ast:asts){
                astsMaped.put(integer.valueof(ast.Cumulocity_ID__c),ast);
            }
            
            
            for(Object lstalarm:lstalarms){
                Map<String, Object> customerAttributes = (Map<String, Object>)lstalarm;
                Map<String, Object> customerAttributesById = (Map<String, Object>)customerAttributes.get('source');
                Alert__c tempAlert =null;
                tempAlert = new Alert__c();
                if(astsMaped.get(integer.valueof(customerAttributesById.get('id')))!=null){ // here is where you compare which alarm source id that is in the assets 
                    // System.debug('is not null'+astsMaped.get(integer.valueof(customerAttributesById.get('id'))).Cumulocity_ID__c);
                    
                    tempAlert.Alert_ID__c = string.valueof(customerAttributes.get('id'));
                    tempAlert.Alert_Message__c= string.valueof(customerAttributes.get('text'));
                    tempAlert.Severity__c = string.valueof(customerAttributes.get('severity'));
                    tempAlert.Servicer__c = astsMaped.get(integer.valueof(customerAttributesById.get('id'))).Current_Servicer__c;
                    tempAlert.Serial_Number__c = astsMaped.get(integer.valueof(customerAttributesById.get('id'))).id;
                    tempAlert.IMEI__c = astsMaped.get(integer.valueof(customerAttributesById.get('id'))).IMEI__c;
                    tempAlert.Source_ID__c  = integer.valueof(customerAttributesById.get('id'));// put Cumulocity ID 
                    tempAlert.Status__c = string.valueof(customerAttributes.get('status'));
                    tempAlert.No_Match_Found__c=false;
                    
                }
                else{
                    // run here ,because there is no matching asset
                    System.debug('check:'+string.valueof(customerAttributes.get('status')));
                    tempAlert.Source_ID__c  = integer.valueof(customerAttributesById.get('id'));// put Cumulocity ID 
                    tempAlert.Serial_Number__c=defaultAsset[0].id;
                    tempAlert.Alert_ID__c =string.valueof(customerAttributes.get('id'));
                    tempAlert.Alert_Message__c= string.valueof(customerAttributes.get('text'));
                    tempAlert.Severity__c = string.valueof(customerAttributes.get('severity'));
                    tempAlert.Status__c = string.valueof(customerAttributes.get('status'));
                    tempAlert.No_Match_Found__c=true;
                }
                
                
                updatesAlts.add(tempAlert);
                
            }
            
            
            if(updatesAlts.size()>0){
                //run upsert 
                upsert updatesAlts Alert_ID__c;
                
                for(integer x=0;x<updatesAlts.size();x++){
                    updatesAlts[x].Code_Block_Status__c = true;
                }
                upsert updatesAlts Alert_ID__c;
                
            }
        }
        catch(Exception e){
            System.debug('Error: ==>'+e.getMessage());
        }
    }
    
    
    public class Measurments{
        public string imei;
        public string total;
        public string load;
        public string service;
        public string stal;
        
        public Measurments()
        {
            
        }
    }    
}