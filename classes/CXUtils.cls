public without sharing class CXUtils {
    public static DateTime dateTimeForTimezone(
        Integer year,
        Integer month,
        Integer day,
        Integer hour,
        Integer minute,
        Integer second,
        String timezone
    ) {
        DateTime dt = DateTime.newInstanceGmt(year, month, day, hour, minute, second);
        String dateTimeStr = dt.format('yyyy-MM-dd HH:mm:ss',  timezone);
        String dateTimeGmtStr = dt.formatGMT('yyyy-MM-dd HH:mm:ss');
        Datetime localDateTime = DateTime.valueOf(dateTimeStr);
        Datetime baseGMTTime = DateTime.valueOf(dateTimeGmtStr);
        Long milliSecDiff =  baseGMTTime.getTime() - localDateTime.getTime();
        Long minDiff = milliSecDiff / 1000 / 60;
        return dt.addMinutes(minDiff.intValue());
    }


    public static Integer getMonthNum(String monthName) {
        Map<String, Integer> monthNum = new Map<String, Integer>{
            'JAN' => 1,
            'FEB' => 2,
            'MAR' => 3,
            'APR' => 4,
            'MAY' => 5,
            'JUN' => 6,
            'JUL' => 7,
            'AUG' => 8,
            'SEP' => 9,
            'OCT' => 10,
            'NOV' => 11,
            'DEC' => 12
        };
        return monthNum.get(monthName.substring(0, 3).toUpperCase());
    }
}