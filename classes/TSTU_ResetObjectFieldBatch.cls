@isTest
public with sharing class TSTU_ResetObjectFieldBatch
{
    public static testMethod void test_UTIL_ResetObjectFieldBatchString()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'String', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchBoolean()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Boolean', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchInteger()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Integer', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchDecimal()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Decimal', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchLong()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Long', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchDouble()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Double', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchDatetime()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Datetime', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchDate()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Date', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchTime()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Time', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchDelete()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID FROM Account', '', 'Delete', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchElse()
    {
        createExistingAccount();
        Test.startTest();
        Database.executeBatch(new UTIL_ResetObjectFieldBatch(
            'SELECT ID, BillingStreet FROM Account WHERE BillingStreet != null', 'BillingStreet', 'Else', null));
        Test.stopTest();
    }

    public static testMethod void test_UTIL_ResetObjectFieldBatchError()
    {
        createExistingAccount();
        Test.startTest();
        try
        {
            Database.executeBatch(new UTIL_ResetObjectFieldBatch(
                'SELECT ID, Name FROM Account WHERE Name != null', 'Name', 'String', null));
        }
        catch (Exception e)
        {}
        Test.stopTest();
    }

    private static void createExistingAccount()
    {
        Account acct = TSTU_SFAccount.createTestAccount();
        acct.Name = 'Test Account';
        acct.BillingStreet = 'BillingStreet';
        insert acct;

        acct = TSTU_SFAccount.createTestAccount();
        acct.Name = 'Test Account';
        acct.BillingStreet = 'BillingStreet';
        insert acct;

        acct = TSTU_SFAccount.createTestAccount();
        acct.Name = 'Test Account';
        acct.BillingStreet = 'BillingStreet';
        insert acct;
    }
}