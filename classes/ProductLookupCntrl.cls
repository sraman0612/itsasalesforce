public class ProductLookupCntrl {
    @AuraEnabled(cacheable=true) 
    public static String searchDB(String quoteId, String objectName, String fld_API_Text, String fld_API_Val, 
                                  Integer lim,String fld_API_Search,String searchText ){
        
        SBQQ__Quote__c quote = [SELECT Id, Distribution_Channel__c from SBQQ__Quote__c WHERE Id =: quoteId LIMIT 1];
        
        searchText='\'' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
        
        String query = 'SELECT ' + fld_API_Text + ', ' + fld_API_Val + ', Description' + ',Special_Notes__c'+
                        ' FROM ' + objectName +
                        ' WHERE ' + fld_API_Search + ' LIKE ' + searchText +
                        ' AND FLD_Distribution_Channel__c = \'' + quote.Distribution_Channel__c + '\'' +
                        ' AND IsActive = TRUE ' +
                        ' LIMIT ' + lim;

        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        
        for(SObject s : sobjList){
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.text = String.valueOf(s.get(fld_API_Text)) ;
            obj.val = String.valueOf(s.get(fld_API_Val))  ; 
            obj.descr = String.valueOf(s.get('Description'));
            obj.notes = String.valueOf(s.get('Special_Notes__c'));
            lstRet.add(obj);
        } 
         return JSON.serialize(lstRet) ;
    }
    
    public class ResultWrapper{
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
        public String descr{get;set;}
        public String notes{get;set;}
    }
}