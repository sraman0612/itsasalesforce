@IsTest 
public class AccountGeocodeAddressTest {
Static testMethod void testAccountGeocodeAdress(){

Account kiosk = new Account(Name = 'Test');

    kiosk.BillingStreet = '4500 Truxel rd';
    kiosk.BillingCity = 'Sacramento';
    kiosk.BillingState = 'California';
    kiosk.BillingPostalCode = '95833';
    kiosk.BillingCountry = 'United States';
    insert kiosk;
    
    test.startTest();
    string status;
    HttpRequest request = new HttpRequest();
    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
    mock.setStaticResource('googlejson');
    mock.setStatusCode(200);
    mock.setHeader('Content-Type', 'application/json');
    mock.respond(request);

    HttpResponse response = new HttpResponse();
    JSONParser responseParser = JSON.createParser(response.getBody());
    String Loc = responseParser.getText();
    double latitude;
    double longitude;
    // Set the mock callout mode
    Test.setMock(HttpCalloutMock.class, mock);
    system.debug('kiosk');
    // Call the method that performs the callout
    AccountGeocodeAddress.DoAddressGeocode(kiosk.Id);
    test.stopTest();
    }
    
    Static testMethod void testkioskGeocodeAdress1(){
    Account kiosk = new Account(Name = 'Test');
    kiosk.BillingStreet = '';
    kiosk.BillingCity = 'Sacramento';
    Kiosk.BillingState = 'California';
    Kiosk.BillingPostalCode = '95833';
    kiosk.BillingCountry = 'United States';
    kiosk.Location__Latitude__s = 33.3333;
    kiosk.Location__Longitude__s = -27.346334;
    insert kiosk;
    AccountGeocodeAddress.DoAddressGeocode(kiosk.Id);
    }
    
    Static testMethod void testAccountGeocodeAdress2(){
    Account kiosk = new Account(Name = 'Test');
    kiosk.BillingStreet = '';
    kiosk.BillingCity = 'Sacramento';
    Kiosk.BillingState = 'California';
    Kiosk.BillingPostalCode = '95833';
    kiosk.BillingCountry = 'United States';
    kiosk.Location__Latitude__s = 33.3333;
    kiosk.Location__Longitude__s = -27.346334;
    insert kiosk;
    kiosk.BillingStreet = '4500 Truxel rd';
    upsert kiosk;
    AccountGeocodeAddress.DoAddressGeocode(kiosk.Id);

    }
}