public with sharing class UTIL_SFOpportunity implements I_SFSObjectDoc
{
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_SFOpportunity.class);

    private SObject loadedSObject;
    private string loadedId;
    private string loadedSapType;
    private string loadedSapDocNum;
    private Map<Id, SObject> loadedLineMap;

	public SObject getSObject(String id)
	{
        return getSObject(id, null, null);
    }

	public SObject getSObject(String sapType, String sapDocNum)
	{
        return getSObject(null, sapType, sapDocNum);
    }

	private SObject getSObject(String id, String sapType, String sapDocNum)
	{
        Boolean isIdMatch = (loadedId == id || (String.isNotEmpty(Id) && String.isNotEmpty(loadedId) && (loadedId.startsWith(id) || id.startsWith(loadedId))));
        if (loadedSObject != null && (isIdMatch || (loadedSapType == sapType && loadedSapDocNum == sapDocNum)))
        {
            return loadedSObject;
        }

        loadedId = id;
        loadedSapType = sapType;
        loadedSapDocNum = sapDocNum;
        loadedLineMap = null;

		SObject result = null;

        string errorString = '';
        if (String.isNotEmpty(id) || (String.isNotEmpty(sapType) && String.isNotEmpty(sapDocNum)))
        {
            try
            {
                string selectCmd = 'SELECT Id, Name, Description, StageName,' +
                            ' Amount, Type, NextStep, Probability, HasOpportunityLineItem,' +
                            ' LeadSource, AccountID, CloseDate, Pricebook2Id, ENSX_EDM__Quote_Number__c,' +
                            ' ENSX_EDM__OrderNumber__c' +
                            ' FROM Opportunity';

                if (String.isNotEmpty(id))
                {
                    selectCmd += ' WHERE Id = \'' + id + '\'';
                    errorString = ' for the provided Id ' + id;
                }
                else
                {
                    if (sapType == 'Quote')
                    {
                        selectCmd += ' WHERE ENSX_EDM__Quote_Number__c = \'' + sapDocNum + '\'';
                        errorString = ' for the provided SAP Quote Number ' + sapDocNum;
                    }
                    else if (sapType == 'Order')
                    {
                        selectCmd += ' WHERE ENSX_EDM__OrderNumber__c = \'' + sapDocNum + '\'';
                        errorString = ' for the provided SAP Order Number ' + sapDocNum;
                    }
                    else
                    {
                        selectCmd = null;
                    }
                }
                if (selectCmd != null)
                {
                    selectCmd += ' Limit 1';

                    List<SObject> resultList = Database.query(selectCmd);

                    if (resultList.size() > 0)
                    {
                        result = resultList[0];
                    }
                    loadedId = result.Id;
                }
            }
            catch (Exception e)
            {
                System.debug(e);
            }
        }

        if (result == null && String.isNotEmpty(id))
        {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 
                'There was an issue retrieving an Opportunity record' + errorString));
        }

        loadedSObject = result;
		return result;
	}

	public Map<Id, SObject> getSObjectLineItems(String id)
	{
        Boolean isIdMatch = (loadedId == id || (String.isNotEmpty(Id) && String.isNotEmpty(loadedId) && (loadedId.startsWith(id) || id.startsWith(loadedId))));
        if (loadedLineMap != null && isIdMatch)
        {
            return loadedLineMap;
        }

        loadedId = id;

		Map<Id, SObject> lineMap = new Map<Id, SObject>();

		try
		{
            string selectCmd = 'SELECT Id, OpportunityId, SortOrder, PricebookEntryId, Product2Id, Product2.Name, Name,' +
                        ' Quantity, TotalPrice, UnitPrice, ListPrice, ServiceDate, Description, FLD_SAP_Item_Number__c' +
                        ' FROM OpportunityLineItem WHERE OpportunityId = \'' + id + '\'';
            List<SObject> lineList = Database.query(selectCmd);
            
            for (SObject lineItem : lineList)
			{
				lineMap.put(lineItem.Id, lineItem);
			}

		}
		catch (Exception e)
		{
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'There was an issue retrieving the Opportunity line items'));
		}
        loadedLineMap = lineMap;
        return lineMap;
	}

	public String getAccountId(SObject sfSObject)
	{
        return sfSObject == null || sfSObject.getSObjectType().getDescribe().getName() != 'Opportunity' ? null : ((Opportunity) sfSObject).AccountId;
	}

	public String getName(SObject sfSObject)
	{
        return sfSObject == null || sfSObject.getSObjectType().getDescribe().getName() != 'Opportunity' ? '' : ((Opportunity) sfSObject).Name;
	}

    public String getQuoteNumber(SObject sfSObject)
    {
        return sfSObject == null || sfSObject.getSObjectType().getDescribe().getName() != 'Opportunity' ? '' : ((Opportunity) sfSObject).ENSX_EDM__Quote_Number__c;
    }

    public String getOrderNumber(SObject sfSObject)
    {
        return sfSObject == null || sfSObject.getSObjectType().getDescribe().getName() != 'Opportunity' ? '' : ((Opportunity) sfSObject).ENSX_EDM__OrderNumber__c;
    }

    public Opportunity getOpportunity(SObject sfSObject)
    {
        return (Opportunity) sfSObject;
    }

    public Id getPriceBookId(SObject sfSObject)
    {
        return sfSObject == null || sfSObject.getSObjectType().getDescribe().getName() != 'Opportunity' ? null : ((Opportunity) sfSObject).Pricebook2Id;
    }

    public Id getProductId(SObject sfsObjectLine)
    {
        return sfsObjectLine == null || sfsObjectLine.getSObjectType().getDescribe().getName() != 'OpportunityLineItem' ? null : ((OpportunityLineItem) sfsObjectLine).Product2.Id;
    }

    private Map<Id, String> productSAPMaterialNumberList;

    public String getMaterial(SObject sfSObject, SObject sfsObjectLine)
    {
        if (productSAPMaterialNumberList == null)
        {
            productSAPMaterialNumberList = UTIL_SFSObjectDoc.loadProductSAPMaterialNumberList(sfsObject);
        }
        String material = productSAPMaterialNumberList.get(getProductId(sfsObjectLine));
        return material == null ? '' : material;
    }

    public String getItemNumber(SObject sfsObjectLine)
    {
        return sfsObjectLine == null || sfsObjectLine.getSObjectType().getDescribe().getName() != 'OpportunityLineItem' ? '' : ((OpportunityLineItem) sfsObjectLine).FLD_SAP_Item_Number__c;
    }

    public void initializeQuoteFromSfSObject(
        String calledFrom, 
        SObject sfSObject, 
        SBO_EnosixQuote_Detail.EnosixQuote quoteDetail, 
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap,
        Map<String, UTIL_Quote.QuoteLineValue> quoteLineValueMap,
        Integer itemIncrement)
    {
        for (SObject sfsObjectLine : getSObjectLineItems(sfsObject.Id).values())
        {
            OpportunityLineItem oppLine = (OpportunityLineItem) sfsObjectLine;
            string materialNumber = getMaterial(sfSObject, sfsObjectLine);
            SBO_EnosixQuote_Detail.ITEMS quoteItem = translateLineItemToQuoteItem(oppLine, materialNumber);
            if (quoteItem != null)
            {
                UTIL_Quote.addItemToQuote(quoteDetail, quoteItem, itemIncrement);
                sfSObjectLineIdMap.put(quoteItem.ItemNumber.replaceFirst('^0+(?!$)', ''), new UTIL_SFSObjectDoc.SfSObjectItem(sfsObjectLine.Id));
            }
        }
    }

    @testVisible
    private SBO_EnosixQuote_Detail.ITEMS translateLineItemToQuoteItem(OpportunityLineItem oppLine, string materialNumber)
    {
        SBO_EnosixQuote_Detail.ITEMS item = null;
        if (null != materialNumber)
        {
            item = new SBO_EnosixQuote_Detail.ITEMS();
            item.Material = materialNumber;
            if (oppLine.ServiceDate != null)
            {
                item.ScheduleLineDate = oppLine.ServiceDate;
            }
            item.OrderQuantity = oppLine.Quantity;
            item.NetItemPrice = oppLine.UnitPrice;
            item.ItemDescription = oppLine.Description;
        }
        else
        {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR,
                'Material Detail information could not be derived from the provided Product - ' + oppLine.Name
            ));
        }
        return item;
    }

    public void finalizeQuoteAndUpdateSfsObject(
        String calledFrom, 
        SObject sfSObject, 
        SBO_EnosixQuote_Detail.EnosixQuote quoteDetail, 
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap,
        Id pricebookId,
        List<SBO_EnosixQuote_Detail.ITEMS> quoteItems,
        Map<string, Id> materialToProductIdMap,
        Map<Id, PricebookEntry> productToPricebookEntryMap)
    {
        // Initial save of opportunity to get Id if new from Quick Quote
        Opportunity opp = (Opportunity) sfSObject;
        opp.Pricebook2Id = pricebookId;
        if (calledFrom != CTRL_OpportunityQuotePricing.calledFrom) opp.ENSX_EDM__Quote_Number__c = quoteDetail.SalesDocument;
        opp.Amount = quoteDetail.NetOrderValue;
        opp.CloseDate = System.today();
        upsert opp;

        // Add opportunity lines
        if (UTIL_Quote.isAddMaterial || UTIL_Quote.isEditMaterial || UTIL_Quote.isCloneMaterial || UTIL_Quote.isMoveMaterial)
        {
            upsertLineItemsFromQuoteItems(opp, quoteItems, materialToProductIdMap, productToPricebookEntryMap, sfSObjectLineIdMap);

            // Save the opportunity
            upsert opp;
        }
    }

    // upsertLineItemsFromQuoteItems()
    //
    // Map the given quote items to new opportunity lines and insert them into the db
    @testVisible
    private void upsertLineItemsFromQuoteItems(
        Opportunity opp,
        List<SBO_EnosixQuote_Detail.ITEMS> quoteItems,
        Map<string, Id> materialToProductIdMap,
        Map<Id, PricebookEntry> productToPricebookEntryMap,
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap)
    {
        List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();

        for (SBO_EnosixQuote_Detail.ITEMS item : quoteItems)
        {
            Id productId = materialToProductIdMap.get(item.Material);
            PricebookEntry price = productToPricebookEntryMap.get(productId);

            OpportunityLineItem oli = new OpportunityLineItem();

            if (sfSObjectLineIdMap.containsKey(item.ItemNumber.replaceFirst('^0+(?!$)', '')) 
                && !sfSObjectLineIdMap.get(item.ItemNumber.replaceFirst('^0+(?!$)', '')).isDeleted)
            {
                oli.id = sfSObjectLineIdMap.get(item.ItemNumber.replaceFirst('^0+(?!$)', '')).id;
                System.debug('Updating Opportunity Line Item');
            }
            else 
            {
                oli.Opportunity = opp;
                oli.OpportunityId = opp.Id;
                oli.PricebookEntry = price;
                oli.PricebookEntryId = price.Id;
                System.debug('Adding Opportunity Line Item');
            }
            oli.Description = item.ItemDescription;
            oli.Quantity = item.OrderQuantity;
            oli.UnitPrice = item.NetItemPrice;
            oli.FLD_SAP_Item_Number__c = item.ItemNumber;            

            if (null != item.ScheduleLineDate)
                oli.ServiceDate = item.ScheduleLineDate;

            System.debug(oli);

            lineItems.add(oli);
        }

        upsert lineItems;
        opp.OpportunityLineItems.addAll(lineItems);
    }

    public void initializeOrderFromSfSObject(
        String calledFrom, 
        SObject sfSObject, 
        SBO_EnosixSO_Detail.EnosixSO orderDetail, 
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap,
        Map<String, UTIL_Order.OrderLineValue> orderLineValueMap,
        Integer itemIncrement)
    {
        orderDetail.CustomerPurchaseOrderNumber = getName(sfSObject);

        for (SObject sfsObjectLine : getSObjectLineItems(sfsObject.Id).values())
        {
            OpportunityLineItem oppLine = (OpportunityLineItem) sfsObjectLine;
            string materialNumber = getMaterial(sfSObject, sfsObjectLine);
            SBO_EnosixSO_Detail.ITEMS orderItem = translateLineItemToOrderItem(oppLine, materialNumber);
            if (orderItem != null)
            {
                UTIL_Order.addItemToOrder(orderDetail, orderItem, itemIncrement);
                sfSObjectLineIdMap.put(orderItem.ItemNumber.replaceFirst('^0+(?!$)', ''), new UTIL_SFSObjectDoc.SfSObjectItem(sfsObjectLine.Id));
            }
        }
    }

    @testVisible
    private SBO_EnosixSO_Detail.ITEMS translateLineItemToOrderItem(OpportunityLineItem oppLine, string materialNumber)
    {
        SBO_EnosixSO_Detail.ITEMS item = null;
        if (null != materialNumber)
        {
            item = new SBO_EnosixSO_Detail.ITEMS();
            item.Material = materialNumber;
            if (oppLine.ServiceDate != null)
            {
                item.ScheduleLineDate = oppLine.ServiceDate;
            }
            item.OrderQuantity = oppLine.Quantity;
        }
        else
        {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR,
                'Material Detail information could not be derived from the provided Product - ' + oppLine.Name
            ));
        }
        return item;
    }

    public void finalizeOrderAndUpdateSfsObject(
        String calledFrom, 
        SObject sfSObject, 
        SBO_EnosixSO_Detail.EnosixSO orderDetail, 
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap,
        Id pricebookId,
        List<SBO_EnosixSO_Detail.ITEMS> orderItems,
        Map<string, Id> materialToProductIdMap,
        Map<Id, PricebookEntry> productToPricebookEntryMap)
    {
        Opportunity opp = (Opportunity) sfSObject;
        opp.Amount = orderDetail.NetOrderValue;
        opp.StageName = 'Closed Won';
        opp.Probability = 100;
        opp.CloseDate = System.today();

        if (string.isNotEmpty(orderDetail.SalesDocument))
        {
            if (calledFrom != CTRL_OpportunityQuotePricing.calledFrom) opp.ENSX_EDM__OrderNumber__c = orderDetail.SalesDocument;
        }
        opp.Pricebook2Id = pricebookId;
        upsert opp;

        // Add opportunity lines
        if (UTIL_Order.isAddMaterial || UTIL_Order.isEditMaterial || UTIL_Order.isCloneMaterial || UTIL_Order.isMoveMaterial)
        {
            upsertLineItemsFromOrderItems(opp, orderItems, materialToProductIdMap, productToPricebookEntryMap, sfSObjectLineIdMap);

            // Save the opportunity
            upsert opp;
        }
    }

    // upsertLineItemsFromOrderItems()
    //
    // Map the given order items to new opportunity lines and insert them into the db
    @testVisible
    private void upsertLineItemsFromOrderItems(
        Opportunity opp,
        List<SBO_EnosixSO_Detail.ITEMS> orderItems,
        Map<string, Id> materialToProductIdMap,
        Map<Id, PricebookEntry> productToPricebookEntryMap,
        Map<String, UTIL_SFSObjectDoc.SfSObjectItem> sfSObjectLineIdMap)
    {
        List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();

        for (SBO_EnosixSO_Detail.ITEMS item : orderItems)
        {
            Id productId = materialToProductIdMap.get(item.Material);
            PricebookEntry price = productToPricebookEntryMap.get(productId);

            OpportunityLineItem oli = new OpportunityLineItem();

            if (sfSObjectLineIdMap.containsKey(item.ItemNumber.replaceFirst('^0+(?!$)', '')) 
                && !sfSObjectLineIdMap.get(item.ItemNumber.replaceFirst('^0+(?!$)', '')).isDeleted)
            {
                oli.id = sfSObjectLineIdMap.get(item.ItemNumber.replaceFirst('^0+(?!$)', '')).id;
                System.debug('Updating Opportunity Line Item');
            }
            else 
            {
                oli.Opportunity = opp;
                oli.OpportunityId = opp.Id;
                oli.PricebookEntry = price;
                oli.PricebookEntryId = price.Id;
                System.debug('Adding Opportunity Line Item');
            }
            oli.Description = item.ItemDescription;
            oli.Quantity = item.OrderQuantity;
            oli.UnitPrice = item.NetItemPrice;
            oli.FLD_SAP_Item_Number__c = item.ItemNumber;            

            if (null != item.ScheduleLineDate)
                oli.ServiceDate = item.ScheduleLineDate;

            System.debug(oli);

            lineItems.add(oli);
        }

        upsert lineItems;
        opp.OpportunityLineItems.addAll(lineItems);
    }
}