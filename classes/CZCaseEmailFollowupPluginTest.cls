@isTest(seeAllData=true)
public class CZCaseEmailFollowupPluginTest {
    
    private static Case createCase(){
        Case record = new Case();
        record.Subject = 'Test';
        insert record;
        return record;
    }
     
    static testmethod void flowTaskTest(){
        Case record = createCase();
         
         Attachment attach=new Attachment(); 
         
         attach.Name='quote Test Attachment.pdf';

        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');

        attach.body=bodyBlob;
		
        attach.parentId=record.id;

        insert attach;
        
        Contact cont = new Contact();
        
        cont.Email = 'abpatton@gmail.com';
        cont.FirstName = 'Test';
        cont.LastName = 'Contact';
        
        insert cont;
        
        EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE Name = 'Case Initial Notification - Managed Care Payment Over Due 1' LIMIT 1];

        CZCaseEmailFollowupPlugin plugin = new CZCaseEmailFollowupPlugin();
        
        Map<String, Object> inputParams = new Map<String, Object>();
        string taskNum = record.Id;		//50063000003AEik-SBX | 500o000000GCryb-PROD
        string taskSub = 'Test Class';
        
        inputParams.put('caseId', record.Id);
        inputParams.put('contactId', cont.Id);
        inputParams.put('emailTemplateId', template.Id);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        plugin.invoke(request);
        Process.PluginDescribeResult result = plugin.describe();

    }

}