@isTest
global class SFS_MockCalloutClass1 {
    global static HttpResponse invokeMockResponse(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.com/test/test');
        req.setMethod('GET');
        req.setBody('{"currencyCode" : "USD","headerAttributeValues" : {"serviceType":"certifyTax","oracleCustomerNumber":"23895","shipToFullstate":"Georgia","sFUniqId":"CTS-002"},"items" : [ {"itemIdentifier" : "1234","partNumber" : "00250506"}]}');
        Http h = new Http();
        HttpResponse res = h.send(req);
        res.setStatusCode(200);
        return res;
    }
}