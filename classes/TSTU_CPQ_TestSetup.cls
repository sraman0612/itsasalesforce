@isTest public with sharing class TSTU_CPQ_TestSetup {

    public static Account createAccount(String customerNumber) {
        Account account = TSTU_SFAccount.createTestAccount();
        account.Name = 'Mock Account';
        account.AccountNumber = customerNumber;
        account.BillingCity = 'Toledo';
        account.BillingStreet = '1234 Test St';
        account.BillingState = 'OH';
        account.BillingPostalCode = '43613';
        account.BillingCountry = 'USA';
        account.put(UTIL_SFAccount.CustomerFieldName, customerNumber);
        insert account;

        return account;
    }

    public static SBQQ__Quote__c createQuote(Account account) {
        Opportunity opp = TSTU_SFOpportunity.createTestOpportunity();
        opp.Name = 'opp 1';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        opp.ENSX_EDM__Quote_Number__c = 'TEST-QUOTE';
        opp.AccountId = account.Id;
        TSTU_SFOpportunity.upsertOpportunity(opp);

        SBQQ.TriggerControl.disable();
        SBQQ__Quote__c quote = TSTU_SFCPQQuote.createTestQuote();
        quote.SBQQ__Key__c = 'Mock Quote';
        quote.SBQQ__Opportunity2__c = opp.Id;
        quote.SBQQ__Account__c = account.Id;
        insert quote;
        
        Product2 product = createProduct2();
        
        SBQQ__QuoteLine__c quoteLine = TSTU_SFCPQQuote.createTestQuoteLine();
        quoteLine.SBQQ__Quote__c = quote.Id;
        quoteLine.SBQQ__Product__c = product.Id;
        quoteLine.SBQQ__Quantity__c = 1;
        quoteLine.SBQQ__ListPrice__c = 20.00;
        quoteLine.SBQQ__Description__c = 'TEST DESC';
        insert quoteLine;

        return quote;
    }

    public static SBQQ__QuoteLine__c createQuoteLine() {
        SBQQ.TriggerControl.disable();
        SBQQ__Quote__c quote = TSTU_SFCPQQuote.createTestQuote();
        quote.SBQQ__Key__c = 'Mock Quote';
        insert quote;
        
        Product2 product = createProduct2();
        
        SBQQ__QuoteLine__c quoteLine = TSTU_SFCPQQuote.createTestQuoteLine();
        quoteLine.SBQQ__Quote__c = quote.Id;
        quoteLine.SBQQ__Product__c = product.Id;
        quoteLine.SBQQ__Quantity__c = 1;
        quoteLine.SBQQ__ListPrice__c = 20.00;
        quoteLine.SBQQ__Description__c = 'TEST DESC';
        insert quoteLine;
        upsert quote;

        return quoteLine;
    }

    public static List<SObject> createAccountQuoteLinked() {
        Account account = createAccount('TEST');

        SBQQ__Quote__c quote = createQuote(account);

        return new List<SObject> { account, quote };
    }

    public static Product2 createProduct2() {
        Product2 product = TSTU_SFProduct.createTestProduct2();
        product.put(UTIL_SFProduct.MaterialFieldName, 'TEST');
        insert product;
        return product;
    }

    public static Address createAddress() {
        Account a = createAccount('TEST');
        a = [SELECT BillingAddress FROM Account WHERE Id = :a.Id];
        return (Address)a.get('BillingAddress');
    }

}