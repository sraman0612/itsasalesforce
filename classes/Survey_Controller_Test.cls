@isTest
private class Survey_Controller_Test {

    @testSetup
    static void setupData(){
    
        Id customerRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id serviceRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Service Center').getRecordTypeId();
    
        Account a1 = new Account();
        a1.Name = 'Customer';
        a1.shippingStreet = '123 Fake St';
        a1.shippingCity = 'Milwaukee';
        a1.shippingState = 'WI';
        a1.shippingPostalCode = '53211';
        a1.RecordTypeId = customerRTId;
        a1.Location__Latitude__s = 43.0389;
        a1.Location__Longitude__s = 87.9065;
        insert a1;
        
        Contact cont = new Contact();
        cont.firstName = 'Can';
        cont.lastName = ' Pango';
        cont.email = 'service-gdi@canpango.com';
        cont.AccountId = a1.Id;
        insert cont;
        
        Case cse = new Case();
        cse.AccountId = a1.Id;
        cse.Contact = cont;
        cse.ContactId = cont.Id;
        cse.Subject = 'Test Case';        
        insert cse;       
        
        Account a2 = new Account();
        a2.Name = 'Service Center';
        a2.billingStreet = '123 Fake St';
        a2.billingCity = 'Racine';
        a2.billingState = 'WI';
        a2.billingPostalCode = '53402';
        a2.RecordTypeId = serviceRTId;
        a2.Location__Latitude__s = 43.0389;
        a2.Location__Longitude__s = 88.9065;
        insert a2;
        
        Account a3 = new Account();
        a3.Name = 'Test Center';
        a3.billingStreet = '123 Main St';
        a3.billingCity = 'Racine';
        a3.billingState = 'WI';
        a3.billingPostalCode = '53404';
        a3.RecordTypeId = serviceRTId;
        a3.Location__Latitude__s = 43.0489;
        a3.Location__Longitude__s = 88.8065;
        insert a3;
        
        Google_Maps_Settings__c gms = new Google_Maps_Settings__c();
        gms.API_Access_Key__c = 'testKey123_abc';
        gms.name = 'Google Maps API Key';
        insert gms;
        
        Account_Association__c aa1 = new Account_Association__c();
        aa1.Customer__c = a1.id;
        aa1.Service_Center__c = a2.id;
        aa1.Active__c = true; 
        aa1.Priority__c = 'Primary';
        aa1.Hours__c = 'Business Hours';
        insert aa1;
        
        Account_Association__c aa2 = new Account_Association__c();
        aa2.Customer__c = a1.id;
        aa2.Service_Center__c = a2.id;
        aa2.Active__c = true;
        aa2.Priority__c = 'Secondary';
        aa2.Hours__c = 'After Hours';
        insert aa2;
        
        Account_Association__c aa3 = new Account_Association__c();
        aa3.Customer__c = a1.id;
        aa3.Service_Center__c = a3.id;
        aa3.Active__c = true;
        aa3.Priority__c = 'Secondary';
        aa3.Hours__c = 'Business Hours';
        insert aa3;
    
    	Site_Visit__c sv = new Site_Visit__c();
        sv.Case__c  = cse.Id;
        sv.Service_Center__c = a2.Id;
        sv.Customer__c = a1.Id;
        insert sv;
       
    }
    
     private static testmethod void testControllerCancel(){
        Case c = [SELECT Id FROM Case LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Survey'));
        
        Survey_Controller sc = new Survey_Controller();
        
        System.currentPageReference().getParameters().put('CaseId', c.Id);
        sc = new Survey_Controller();
     	sc.cancel();
         
     }
         
    private static testmethod void testControllerPoor(){
        Case c = [SELECT Id FROM Case LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Survey'));
        
        Survey_Controller sc = new Survey_Controller();
        
        System.currentPageReference().getParameters().put('CaseId', c.Id);
        sc = new Survey_Controller();
        
        sc.getYesNo();
        sc.refreshGeneral();
        sc.scParts = 'true';
        sc.SCRecParts = 'true';
        sc.theSurvey.Number_of_Trips__c = '1';
        sc.refreshGeneral();
        sc.theSurvey.Number_of_Trips__c = '3';
        sc.refreshGeneral();
        
        sc.theSurvey.Service_Response_Time__c = 'Poor';//fair,good
        sc.theSurvey.Followed_Special_Instructions__c = true;
        sc.theSurvey.Quote_Paperwork__c = 'Poor';//fair,good,NA
        sc.theSurvey.Invoice_Work_Order__c = 'Poor';//fair,good 
        sc.theSurvey.Excessive_Charges__c = true;
        
        sc.saveSurvey();
        
    }
    
    private static testmethod void testControllerFair(){
        Case c = [SELECT Id FROM Case LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Survey'));
        
        Survey_Controller sc = new Survey_Controller();
        
        System.currentPageReference().getParameters().put('CaseId', c.Id);
        sc = new Survey_Controller();
        
        sc.getYesNo();
        sc.refreshGeneral();
        sc.scParts = 'true';
        sc.SCRecParts = 'true';
        sc.theSurvey.Number_of_Trips__c = '1';
        sc.refreshGeneral();
        sc.theSurvey.Number_of_Trips__c = '3';
        sc.refreshGeneral();
        
        sc.theSurvey.Service_Response_Time__c = 'Fair';//fair,good
        sc.theSurvey.Followed_Special_Instructions__c = true;
        sc.theSurvey.Quote_Paperwork__c = 'Fair';//fair,good,NA
        sc.theSurvey.Invoice_Work_Order__c = 'Fair';//fair,good 
        sc.theSurvey.Excessive_Charges__c = true;
        
        sc.saveSurvey();
        
    }
    
    private static testmethod void testControllerGood(){
        Case c = [SELECT Id FROM Case LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Survey'));
        
        Survey_Controller sc = new Survey_Controller();
        
        System.currentPageReference().getParameters().put('CaseId', c.Id);
        sc = new Survey_Controller();
        
        sc.getYesNo();
        sc.refreshGeneral();
        sc.scParts = 'true';
        sc.SCRecParts = 'true';
        sc.theSurvey.Number_of_Trips__c = '1';
        sc.refreshGeneral();
        sc.theSurvey.Number_of_Trips__c = '3';
        sc.refreshGeneral();
        
        sc.theSurvey.Service_Response_Time__c = 'Good';//fair,good
        sc.theSurvey.Followed_Special_Instructions__c = false;
        sc.theSurvey.Quote_Paperwork__c = 'Good';//fair,good,NA
        sc.theSurvey.Invoice_Work_Order__c = 'Good';//fair,good 
        sc.theSurvey.Excessive_Charges__c = false;
        
        sc.saveSurvey();
        
    }
}