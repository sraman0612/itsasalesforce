/*
 * Author   : Nocks Emmanuel Mulea 
 * Company  : Canpango LLC
 * Email    : emmanuel.mulea@canpango.com
 * 
 * 
 * 
 */

global class C_GDIiConnMeasurmentsServiceBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {

    global final String query;
    
    global C_GDIiConnMeasurmentsServiceBatch(String q){
        
        if(!string.isBlank(q)){
            query=q;
           }
            else{
                query ='SELECT Id, Name, IMEI__c, Cumulocity_ID__c FROM Asset WHERE Cumulocity_ID__c != null';
            }
            
    }
    global Database.QueryLocator start(Database.BatchableContext bacthC){
        
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext batchC,List<Asset>scope){
        
        list<id> AstID = new list<id>();
        for(Asset a : scope){
            AstID.add(a.id);
        }
        
        C_GDIiConnMeasurmentsService.syncMeasurements('',AstID);
    }
 
    global void finish(Database.BatchableContext batchC){
        /*
        Datetime dt = system.now().addHours(5);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = '00';
        String year = string.valueOf(dt.year());
        String jobName ='C_GDIiConnMeasurmentsServiceSchedule-'+year+'-'+month+'-'+day+'-'+hour+'-'+minute+'-'+second;
        String cronExp = '00 '+minute+' '+hour+' '+day+' '+month+' ?'+' '+year;
        System.schedule(jobName, cronExp, new C_GDIiConnMeasurmentsServiceSchedule()); 
        */
    }
}