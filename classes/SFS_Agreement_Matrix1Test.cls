@isTest
public class SFS_Agreement_Matrix1Test {
    Static testmethod void getYearsTest(){
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[1].IRIT_Customer_Number__c='123';
        accList[1].RecordTypeId=accRecID;
        insert accList[1];
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].Type='Prospect';
        insert accList[0];
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        insert svcAgreementList;
        List<MaintenancePlan> mpList=SFS_TestDataFactory.createMaintenancePlan(1,svcAgreementList[0].Id,false);
        insert mpList;
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1,false);
        insert assetList;
        List<MaintenanceAsset> maList=SFS_TestDataFactory.createMaintenanceAsset(1,mpList[0].Id,assetList[0].Id,false);
        insert maList;
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        List<MaintenanceWorkRule> mwrList=SFS_TestDataFactory.createMaintenanceWorkRule(1,maList[0].Id,workTypeList[0].Id,false);
        mwrList[0].RecurrencePattern='FREQ=MONTHLY;COUNT=1;';
        insert mwrList;
        Test.startTest();
        SFS_Agreement_Matrix1.getYears(svcAgreementList[0].Id);
        SFS_Agreement_Matrix1.getAssets(svcAgreementList[0].Id);
        SFS_Agreement_Matrix1.getWorkRules(svcAgreementList[0].Id);
        SFS_Agreement_Matrix1.getMaintenanceAsset(svcAgreementList[0].Id);
        Test.stopTest();
    }
}