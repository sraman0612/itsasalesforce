/* Author: Ryan Reddish - Capgemini
 * Date: 1/23/2022
 * Purpose: Use this class to store all HttpCallouts for SFS Outbound Integrations.

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Srikanth P          M-001     2/22/2022   createIntegrationLog - Utility Method to create the Integration Log record
============================================================================================================================================================*/
public class SFS_Outbound_Integration_Callout {
    //All integrations will use this method. They will pass a identifier 'interfaceDetail'. This contains the RICE ID
    //Query uses RICE Id to identify the endpoint that needs to be called.
    @future(Callout = true)
    public static void HttpMethod(String xmlString, String interfaceDetail, String interfaceLabel){
        //Http Method to send xml and recieve success or failure code.

        //Escape all problem characters in the XML:
        List<String> records = new List<String>();
        System.debug('@@@@Records: ' + interfaceDetail);

        SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT Label,SFS_Endpoint_URL__c, SFS_Username__c, SFS_Password__c,SFS_ContentType__c, SFS_RICE_ID__c, SFS_Interface_Destination__c
                                                  FROM SFS_SFS_Integration_Endpoint__mdt
                                                  WHERE Label =: interfaceDetail];
        Boolean cpqint;
        //Encoding credentials for Authorization Header
       	Blob headervalue = Blob.valueOf(endpoint.SFS_Username__c + ':' + endpoint.SFS_Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        String endpointUrl = endpoint.Label!='CPQ Get Price'&& endpoint.Label!='CertifyTax API'&& endpoint.Label!='Labor Rate' ?endpoint.SFS_Endpoint_URL__c:'callout:SFS_CPQ_DevBox_Endpoint';
        //String endpointUrl = endpoint.SFS_Endpoint_URL__C;
        //Http Request
        if(endpoint.Label =='OracleCPQVersionQuote'){
            system.debug('---->Before'+endpointUrl);
            endpointUrl = endpointUrl.replace(':id', xmlString);
            system.debug('---->After'+endpointUrl);
            system.debug('Username---->'+endpoint.SFS_Username__c+'password--->'+endpoint.SFS_Password__c+'header'+ authorizationHeader + 'headerValue-->'+headervalue);
        	cpqint = true;
        }else{
            cpqint = false;
        }
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpointUrl);
        request.setMethod('POST');
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type',endpoint.SFS_ContentType__c);
        request.setTimeout(120000);
        if(endpoint.Label !='OracleCPQVersionQuote'){
            request.setBody(xmlString);
            System.debug('Payload: ' + xmlString);
        }
        //Get Http Response
        SFS_Integrations_Toggle__mdt integrationSwitch = SFS_Integrations_Toggle__mdt.getInstance('SFS_Integration_Toggles');
        //SFS_Integrations_Toggle__mdt cpqSwitch = SFS_Integrations_Toggle__mdt.getInstance('SFS_Disable_CPQ_Outbound_Calls__c');
        HttpResponse response = new HttpResponse();
        If(cpqint == true && integrationSwitch.SFS_Disable_CPQ_Outbound_Calls__c != true){
            If(!Test.isRunningTest()){
                try{
                    response = http.send(request);
                }catch(System.exception e){
                    response.SetBody(e.getMessage() + '/n' + e.getCause());
                }

            }
        }else if (cpqInt == false && integrationSwitch.SFS_Disable_Oracle_Outbound_Calls__c != true){
            If(!Test.isRunningTest()){try{response = http.send(request);}catch(System.exception e){response.SetBody(e.getMessage() + '/n' + e.getCause());}
            }
        }else if(cpqint == true && integrationSwitch.SFS_Disable_CPQ_Outbound_Calls__c != false){
            response.SetBody('CPQ Integrations are currently down. Please contact your System Administrator');
        }else{
            response.setBody('Oracle Integrations are currently down. Please contact your System Administrator');
        }

    	System.debug('STATUS: ' + response.getStatusCode());
        System.debug('Error Message: ' +response.getBody());


        if(endpoint.Label=='CPQ Get Price' && response.getStatusCode() == 200){
                SFS_CPQ_Get_Price_Request.getPriceResponse(response);
        }
        if(endpoint.Label=='INT-155'){
            system.debug('interfaceLabel******'+interfaceLabel);
            if(interfaceLabel.contains('SARevenue')){
             		SFS_Revenue_SAgr_Outbound_Handler.getResponse(response, interfaceLabel);
            }
            if(interfaceLabel.contains('WorkorderBilling')){
                    SFS_Billing_WorkOrder_Outbound_Handler.getResponse(response, interfaceLabel);
                    }
            if(interfaceLabel.contains('WorkOrderRevenue')){
                    SFS_InvoiceWoOutboundHandler.getResponse(response, interfaceLabel);
                    }

            }
            if(endpoint.Label=='XXPA1951'){
             		//SFS_Product_Transfer_Outbound_Handler.Response(response, interfaceLabel);//Nivetha -commmenting for prod validation - 10/14
            }
           if(endpoint.Label=='XXPA1951_PC'){
                    SFS_ProductConsumed_Outbound_Handler.IntegrationResponse(response, interfaceLabel);
            }
            if(endpoint.Label=='XXPA1951_RO'){
             		//SFS_ReturnOrder_Outbound_Handler.IntegrationResponse(response, interfaceLabel);//Nivetha -commmenting for prod validation - 10/14
            }
            if(endpoint.Label == 'XXPA2381'){
                if(interfaceLabel.contains('Service Agreement with SALI')){
                    if(!Test.isRunningTest()){SFS_ServiceAgreementOutboundHandler.InitialResponse(response, interfaceLabel);}

                }else if(interfaceLabel.contains('Work Order with Service Agreement')){
                    if(!Test.isRunningTest()){
                        SFS_WorkOrderWithSvgOutboundHandler.IntegrationResponse(response, interfaceLabel);
                    }

                }else{
                    if(!Test.isRunningTest()){
                        System.debug('@@@@@OutboundCalloutClass:InterfaceLabel: ' + interfaceLabel );
                        SFS_WorkOrderWithoutSvgOutboundHandler.IntegrationResponse(response, interfaceLabel);

                    }

                }
            }
            if(endpoint.Label == 'XXPA2589'){
                if(interfaceLabel.contains('Activity Cost Labor Hour')){
                    SFS_LaborHourOutboundHandler.IntegrationRespone(response, InterfaceLabel);
                }else{
                    //SFS_ExpenseOutboundHandler.integrationResponse(response, InterfaceLabel);//Nivetha -commmenting for prod validation - 10/14
                }
            }

            //Add if statement. Check endpoint.Label = XXPA1951
            // If true pass the response and the label back to the Product Consumed handler.

        // BELOW ADDED BY PARAG
        /*if(endpoint.Label=='OracleCPQVersionQuote' && response.getStatusCode() == 200){
          // versionQuoteController.getCPQVersionResponse(response);

        } */
        if(endpoint.Label=='CertifyTax API' && response.getStatusCode() == 200){
            if(!Test.isRunningTest()){
           //SFS_CPQ_CertifyTax_Interface.getCertifyTaxResponse(response);
            }
        }
        if(endpoint.Label=='Labor Rate'){

        SFS_Get_LaborRate_Interface.getLaborRateResponse(response,interfaceLabel);

        }
        if(endpoint.Label=='IRMW0169' && response.getStatusCode() == 200){

           string recordId = interfaceLabel.substringAfter(';');
           system.debug('@@recordId'+recordId);
           SFS_warrantyClaimOutboundHandler.getClaimSubmissionResponse(response,recordId);

        }
            if(endpoint.Label!='OracleCPQVersionQuote'){
                createIntegrationLog(response, xmlString, interfaceLabel, endpoint);

            }
}

    @future(Callout = true)
    public static void HttpCalloutTest(String ObjectType, String Query, String xmlString, String interfaceDetail, String interfaceLabel){
        //Http Method to send xml and recieve success or failure code.
        SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT Label,SFS_Endpoint_URL__c, SFS_Username__c, SFS_Password__c,SFS_ContentType__c, SFS_RICE_ID__c, SFS_Interface_Destination__c
                                                  FROM SFS_SFS_Integration_Endpoint__mdt
                                                  WHERE Label =: interfaceDetail];
        // parOb = new SObject();
        if(interfaceDetail != 'XXPA1951' || !interfaceDetail.contains('XXPA2381') ){
            //SObject parOb = Database.query(Query);
        }


        //Encoding credentials for Authorization Header
       	Blob headervalue = Blob.valueOf(endpoint.SFS_Username__c + ':' + endpoint.SFS_Password__c);
        String authorizationHeader = 'Basic' + EncodingUtil.base64Encode(headerValue);
        String endpointUrl = endpoint.Label!='CPQ Get Price'&& endpoint.Label!='CertifyTax API'?endpoint.SFS_Endpoint_URL__c:'callout:SFS_CPQ_DevBox_Endpoint';
        //String endpointUrl = endpoint.SFS_Endpoint_URL__C;
        //Http Request
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpointUrl);
        request.setMethod('POST');
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type',endpoint.SFS_ContentType__c);
        request.setTimeout(120000);
        request.setBody(xmlString);
        System.debug('Payload: ' + xmlString);

        //Get Http Response
        If(!Test.isRunningTest()){
        HttpResponse response = http.send(request);
    	System.debug('STATUS: ' + response.getStatusCode());
        System.debug('Error Message: ' +response.getBody());

        }
        else{
                Test.setMock(HttpCalloutMock.class, new SFS_MockHTTPResponseGenerator());
                HttpResponse response = SFS_MockCalloutClass.invokeMockResponseReturnOrder();
                createIntegrationLog(response, xmlString, interfaceLabel, endpoint);
               if(interfaceLabel=='Return Order'){
                   //SFS_ReturnOrder_Outbound_Handler.IntegrationResponse(response, interfaceLabel);//commented for code deployment- manimozhi(10/14)
               }

        }
    }

    public static void createIntegrationLog(HttpResponse response, String xmlString, String interfaceLabel, SFS_SFS_Integration_Endpoint__mdt integMeta){
        SFS_Integration_Log__c log = new SFS_Integration_Log__c();
        DateTime occurredAt = datetime.now();
        String label = interfaceLabel + ' ' + occurredAt;
        log.Occurred_At__c = occurredAt;
        log.SFS_Label__c = label.abbreviate(254);
        log.SFS_Label_Full_Length__c = label;
        log.RICE_ID__c = integMeta.SFS_RICE_ID__c;
        log.Interface_Destination__c = integMeta.SFS_Interface_Destination__c;
        log.SFS_Integration_Status_Code__c = String.ValueOf(response.getStatusCode());
        if(response.getStatusCode() == 200 || response.getStatusCode() == 202){
            log.SFS_Integration_Status__c = 'Success';
            log.SFS_Integration_Response__c = response.getBody();
        } else {
            log.SFS_Integration_Status__c = 'Fail';
            log.SFS_Integration_Response__c = response.getBody();
        }
        splitInterfaceLabelIntoFields(log, interfaceLabel);
        insert log;

        Attachment att = new Attachment();
        att.ParentId = log.Id;
        att.name = 'XSD ' + DateTime.now();
        att.ContentType = 'text/xml';
        att.Body = Blob.ValueOf(xmlString);
        insert att;
    }

    /**
     * Attempt to split interface label into different fields
     */
    private static void splitInterfaceLabelIntoFields(SFS_Integration_Log__c log, String interfaceLabel) {
        try {
            String[] parts = interfaceLabel.contains('|')
                ? interfaceLabel.split('\\|')
                : interfaceLabel.split('!');

            log.Entity_Name__c = parts.remove(0).trim();
            // log.Interface_Destination__c = log.Entity_Name__c;
            if (parts.size() > 0) {
                String delim = '|';
                String ids = '';
                for (String id : parts) {
                    ids += delim + id.trim();
                }
                String entityIds = ids.removeStart(delim);
                log.Entity_ID__c = entityIds.abbreviate(254);
            }
        } catch (Exception e) {
            // Nothing. Couldn't split interfaceLabel
            System.debug('Could not split interfaceLabel for integ log: ' + e.getMessage());
        }
    }
}