@isTest
private with sharing class GDInsideSyncAcctToContactBatchTest {

    @testSetup 
    private static void setup() {
        List<contact> cList = new List<contact>();
        
        Account a1 = new account(name='Accnt Parent Test');
        insert a1;
        Account a = new account(name='Accnt Test',ParentId = a1.id);
        insert a;
        
        contact con5 = new contact(firstname='j2', lastname='b2', email='jbtest7@test.com', accountId = a.id);
        cList.add(con5);
        
        contact con6 = new contact(firstname='j', lastname='b', email='jbtest6@test.com', accountId = a.id);
        clist.add(con6);
        
        insert clist;
    }
    
    private static testmethod void doTest() {
        Test.setMock(HttpCalloutMock.class, new GDInside_Mock_HTTP_ContactSync());
        string dtString = string.valueof(date.valueof(system.today()));
        GDInsideSyncAcctToContactBatch batch = new GDInsideSyncAcctToContactBatch(dtString);	
        Test.startTest();
        Database.executeBatch(batch);
        Test.stopTest();
    }
}