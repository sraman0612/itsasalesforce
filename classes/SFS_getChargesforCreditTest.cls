@istest
public class SFS_getChargesforCreditTest {
    @istest public static void SFS_getChargesforCreditTest() {    
        
        RecordType rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='Site Account - NA Air' AND SObjectType = 'Account'];
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        Double latitude = 48.7646475292616;
        Double longitude =-98.1987222787954;
        
        Account acct = new Account();
        acct.name = 'test account';
        acct.Currency__c='USD';
        acct.RecordTypeId = accRecID; 
        acct.ShippingLatitude =   latitude;
        acct.ShippingLongitude =   longitude;
        acct.ShippingCity ='Montreat';
        acct.ShippingCountry ='United States';
        acct.ShippingGeocodeAccuracy ='Zip';
        acct.ShippingPostalCode='28757';
        acct.ShippingState='NC';
        acct.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct.IRIT_Customer_Number__c='1234';
        insert acct;
        Double latitude1 = 48.7646475292616;
        Double longitude2 =-98.1987222787954;
        
        Account acct2 = new Account();
        
        acct2.name = 'test account2';
        acct.Currency__c='USD';
        acct2.RecordTypeId = rtAcc.Id; 
        acct2.Bill_To_Account__c =acct.id;
        acct2.ShippingLatitude =   latitude1;
        acct2.ShippingLongitude =   longitude2;
        acct2.ShippingCity ='Montreat';
        acct2.ShippingCountry ='United States';
        acct2.ShippingGeocodeAccuracy ='Zip';
        acct2.ShippingPostalCode='28757';
        acct2.ShippingState='NC';
        acct2.Type='Prospect';
        acct2.ShippingStreet='2342 Appalachian Way\nUnit 156';
        acct2.IRIT_Customer_Number__c='1234';
        insert acct2;
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, true);
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct2.Id ,true);
        
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct2.Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,wo[0].Id,false);
        
        Map<Id,CAP_IR_Charge__c> oldMap = new Map<id,CAP_IR_Charge__c>();
        Map<Id,CAP_IR_Charge__c> oldMap1 = new Map<id,CAP_IR_Charge__c>();
        String WOinvoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('WO Invoice').getRecordTypeId();
        
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(2,false);
        inv[0].SFS_Work_Order__c = wo[0].Id;
        inv[0].RecordTypeId=WOinvoiceRecordTyeId;
        inv[1].RecordTypeId=WOinvoiceRecordTyeId;
        insert inv;
        
        charge[0].SFS_Invoice__c = inv[0].id;
        charge[0].CAP_IR_Amount__c=500;
        charge[0]. SFS_Charge_Type__c='Ordered Item'; 
        insert charge;
        
        List<SFS_Invoice_Line_Item__c> inlist =new List<SFS_Invoice_Line_Item__c>();
        SFS_Invoice_Line_Item__c invLi = new SFS_Invoice_Line_Item__c();
        invLi.SFS_Invoice__c=inv[0].id;
        invLi.SFS_Type__c = 'Ordered Item'; 
        invLi.SFS_Sell_Price__c =1076.92;
        invLi.SFS_Amount__c =   700;
        
        insert invLi;
        inlist.add(invLi);
        
        List<SFS_getChargesforCredit.InputVariables> returnVarsList = new List<SFS_getChargesforCredit.InputVariables>();
        SFS_getChargesforCredit.InputVariables returnVars = new SFS_getChargesforCredit.InputVariables();
        returnVars.Invli =inlist;
        returnVarsList.add(returnVars);
        Test.startTest();
        SFS_getChargesforCredit.credit(returnVarsList);
        Test.stopTest();
    }
    
}