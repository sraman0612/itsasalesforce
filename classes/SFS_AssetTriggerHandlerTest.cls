/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 22/11/2021
* @description: Test Class for SFS_AssetTrigger and SFS_AssetTriggerHandler
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001    SIF-175     22/11/2021     Test Class for checkChildRecords method in SFS_AssetTriggerHandler
============================================================================================================*/
@isTest
public class SFS_AssetTriggerHandlerTest {
//To cover checkChildRecords method in SFS_AssetTriggerHandler    
    Static testmethod void testCheckChildRecords(){
        Test.startTest();
        List<Asset> AstList = new List<Asset>();
        Id RentalRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('SFS_Asset_Potential').getRecordTypeId();
        List<Asset> AssetList = SFS_TestDataFactory.createAssets( 3 , true);
        List<CTS_IOT_Frame_Type__c> createFrameTypes = SFS_TestDataFactory.createFrameTypes(2,true,'Dryer');
        for(Asset ast : AssetList){
           ast.RecordTypeId  = RentalRecordTypeId;
            ast.CTS_Frame_Type__c = createFrameTypes[0].Id;
            AstList.add(ast);
            
        }
        update AstList;
        String str = JSON.serialize(AstList);
        List<Product2> ProductList=SFS_TestDataFactory.createProduct(1,false);
        ProductList[0].SFS_Oracle_Orderable__c='Y';
        ProductList[0].IsActive =true;
        insert ProductList[0];
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,null,null,null,null,false);
        insert woList;
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(2,false);
        workTypeList[0].CTS_Frame_Type__c=AstList[0].CTS_Frame_Type__c;
        workTypeList[0].SFS_Product__c=ProductList[0].Id;
        insert workTypeList;
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,workTypeList[0].Id,false);
        woliList[0].AssetId=assetList[0].Id;
        insert woliList;
        List<ProductRequired> productRequiredList=SFS_TestDataFactory.createProductRequired(1,workTypeList[0].Id,ProductList[0].Id,false);
        insert productRequiredList;

        SFS_AssetTriggerHandler.HttpMethod('RentalSelectionOracleDataTable',str,'update');//Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
        SFS_AssetTriggerHandler.checkChildRecords(AstList);

        SFS_AssetTriggerHandler.createAssetRecords(AstList);
       /* RecordType assetRecordType = [Select Id, Name, SObjectType FROM RecordType where SObjectType = 'Asset' limit 1];
        List<Asset> assetList=new List<Asset>();
        Id RentalRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('SFS_Asset_Rental').getRecordTypeId();
        Asset asset1=new Asset(Name='test asset',CurrencyIsoCode='USD',IRIT_RMS_Flag__c=true,recordtypeId=RentalRecordTypeId);
        insert asset1;
        assetList.add(asset1);
        Test.startTest();
        try{
			SFS_AssetTriggerHandler.checkChildRecords(assetList);
        }Catch(Exception e){
            System.assert(e.getMessage().contains('You cannot delete a Connected Asset.'));
        }
        asset1.IRIT_RMS_Flag__c=false;
        update asset1;
        CAP_IR_Reading__c reading=new CAP_IR_Reading__c(CAP_IR_Asset__c=asset1.Id,CurrencyIsoCode='USD');
        insert reading;
        try{
			SFS_AssetTriggerHandler.checkChildRecords(assetList);
            SFS_AssetTriggerHandler.Assetnamechanged(assetList,null);
            SFS_SFS_Integration_Endpoint__mdt mm=new SFS_SFS_Integration_Endpoint__mdt();
            mm.Label='Test';
            mm.SFS_Endpoint_URL__c='www.test.com';
            mm.SFS_Username__c='Test';
            mm.SFS_Password__c='Test';
            mm.SFS_ContentType__c='Text';
                                      
            //SFS_AssetTriggerHandler.HttpMethod('Test',string.valueOf(asset1),'Insert');
        }Catch(Exception e){
            //System.assert(e.getMessage().contains('All child records must be deleted before deleting Asset.'));
        }*/
        Test.stopTest();
    }

//To cover Assetnamechanged method in SFS_AssetTriggerHandler  
    @isTest    
    public static void AssetnamechangedTest(){
        Asset asset1=new Asset(Name='test asset',CurrencyIsoCode='USD',Model_Name__c='test');
        insert asset1;
         User usr = SFS_TestDataFactory.createTestUser('SFS Service Technician', True);
        
        //Map<Id,Asset> oldMap = new Map<id,Asset>();
        //OldMap.put(asset1.id,asset1);
        Test.startTest();
        //asset1.Name!=OldMap;
        try{
            asset1.Name='test2';
            update asset1;            
        }
        catch(Exception e){
            System.assert(e.getMessage().contains('Serial Number cannot be edited'));
            
            Test.stopTest();
            
        }
    }
    
    
    // To cover syncRentalAssetFieldsOnOracleDB in SFS_AssetTriggerHandler
    //Created By -- Parag Bhatt
    //Date: 13/10/2022
    @isTest    
    public static void syncRentalAssetFieldsOnOracleDBTest(){
        Id RentalRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('SFS_Asset_Rental').getRecordTypeId();
        System.debug('RentalRecordTypeId:'+RentalRecordTypeId);
        Test.setMock(HttpCalloutMock.class, new MockForSFS_assetsRentalService());
        Asset ast = new Asset();
        ast.Name = 'Parag Test';
        ast.recordtypeId = RentalRecordTypeId;
        insert ast;
        System.debug('ast:'+ast);
        List<Asset> astLst = new List<Asset>();
        astLst.add(ast);

        Test.startTest();//Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
            SFS_AssetTriggerHandler.syncRentalAssetFieldsOnOracleDB(astLst, 'Insert', null);//Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
        Test.stopTest();//Commented by vidya on 10/11 for Prod validation check and will be reverted back once complete
        
    }
}