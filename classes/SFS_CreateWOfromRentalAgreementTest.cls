/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 13/03/2022
* @description: SFS_CreateWOfromRentalAgreementTest
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001     SIF-124  13/03/2022  Test class for SFS_CreateWOfromRentalAgreement
============================================================================================================================================================*/
@isTest
public class SFS_CreateWOfromRentalAgreementTest {
    
    private static final String AS_RECORDTYPEID = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('NA Air Edit').getRecordTypeId();
       
    Static testmethod void testEntitlementTrigger(){
        //Inserting Frametype
        CTS_IOT_Frame_Type__c frmeType = new CTS_IOT_Frame_Type__c(Name = 'testFilter',CTS_IOT_Type__c = 'Filter');
        insert frmeType;
        
        //Inserting Asset
         Asset ast = new Asset(Name = 'testAsset', CurrencyIsoCode = 'USD', Model_Name__c = '1211',CTS_Frame_Type__c = frmeType.Id, RecordTypeId = AS_RECORDTYPEID);
         insert ast;
        
        //Inserting WorkTypes
        List<WorkType> wtList = new List<WorkType>();
        WorkType wtDev = new WorkType(SFS_Work_Scope1__c = '2', Name = 'test', EstimatedDuration = 8, 
                                   CTS_Frame_Type__c = frmeType.Id, SFS_Work_Order_Type__c = 'Preventive Maintenance',SFS_Delivery__c = true );
        WorkType wtPC = new WorkType(SFS_Work_Scope1__c = '2', Name = 'test', EstimatedDuration = 8, 
                                   CTS_Frame_Type__c = frmeType.Id, SFS_Work_Order_Type__c = 'Preventive Maintenance',SFS_Pickup__c = true);
        WorkType wtCI = new WorkType(SFS_Work_Scope1__c = '2', Name = 'test', EstimatedDuration = 8, 
                                   CTS_Frame_Type__c = frmeType.Id, SFS_Work_Order_Type__c = 'Preventive Maintenance',SFS_Check_In__c = true);
        wtList.add(wtDev);wtList.add(wtPC);wtList.add(wtCI);
        insert wtList;
        
       
        //get Account
        List<Account> accList=SFS_TestDataFactory.createAccounts(2, false);
        accList[1].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        insert accList;
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].type='Prospect';
        update accList;
        
        //get Contact
        Contact con = SFS_TestDataFactory.getContact();
        
        //get PriceBook
        Pricebook2 standardPricebook = SFS_TestDataFactory.getPricebook2();
        
        //get ServiceContract
        String sA_RECORDTYPEID = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Rental').getRecordTypeId();
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, accList[0].Id ,false);
        sc[0].SFS_Invoice_Frequency__c = 'Weekly';
        sc[0].RecordTypeId = sA_RECORDTYPEID;
        sc[0].SFS_Status__c = 'APPROVED';
        insert sc[0];
        
        //get ContractLineItem
        List<ContractLineItem> scli = SFS_TestDataFactory.createServiceAgreementLineItem(1, sc[0].Id ,true);
        
        //get Entitlement
        List<Entitlement> en = SFS_TestDataFactory.createEntitlement(1, accList[0].Id, ast.Id, sc[0].Id, scli[0].Id ,true);
        Test.startTest();
        List<Id> svcId = new List<Id>();
        svcId.add(sc[0].Id);
        SFS_CreateWOfromRentalAgreement.Svc(svcId);
        Test.stopTest();
       }
}