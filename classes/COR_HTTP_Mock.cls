global class COR_HTTP_Mock implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"destination_addresses" : [ "492 W Sharon Rd, Cincinnati, OH 45246, USA" ], "origin_addresses" : [ "320 E Chicago St, Milwaukee, WI 53202, USA" ], "rows" : [{"elements" : [{"distance" : {"text" : "631 km", "value" : 630563 }, "duration" : {"text" : "5 hours 55 mins", "value" : 21275 }, "status" : "OK"} ] } ], "status" : "OK"}');
        res.setStatusCode(200);
        return res;
    }

}