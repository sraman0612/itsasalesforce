@IsTest
public class CreateAlertFromEventTest {
	@IsTest
    static void addAlert() {
        // Create needed Account/Assets
        List<Account> acctList = New List<Account>();
        Account testAccount1 = New Account(Name = 'Test Account 1');
        Account testAccount2 = New Account(Name = 'Test Account 2');
        acctList.add(testAccount1);
        acctList.add(testAccount2);
        
        insert acctList;
        
        List<Asset> assetList = New List<Asset>();
        Asset defaultAsset = New Asset(Name = 'iConnMachine', DChl__c = '000000');
        defaultAsset.AccountId = testAccount1.Id;
        Asset cumulocityAsset = New Asset(Name='Cumulocity Test Asset', DChl__c = '111111');
        cumulocityAsset.AccountId = testAccount2.Id;
        cumulocityAsset.Cumulocity_ID__c = 234567;
        cumulocityAsset.Current_Servicer__c = testAccount2.Id;
        assetList.add(defaultAsset);
        assetList.add(cumulocityAsset);
        
        insert assetList;
            
        // Test the Platform Event trigger for UPDATE
        iConn_Alert__e upTest = New iConn_Alert__e();
        upTest.Alert_String__c = '{"data":{"severity":"TEST WARNING","firstOccurrenceTime":"2020-03-19T23:41:56.699Z","creationTime":"2020-03-20T04:44:21.581+01:00","count":1,"self":"https://www.google.com","source":{"self":"https://www.test.source","id":"123456"},"time":"2020-03-20T16:40:05.011Z","text":"This is a test update alert","id":"987654","type":"TestType","status":"ACTIVE"},"realtimeAction":"UPDATE"}';
        
        // Test the Platform Event trigger for CREATE
        iConn_Alert__e createTest = New iConn_Alert__e();
        createTest.Alert_String__c = '{"data":{"severity":"TEST WARNING","firstOccurrenceTime":"2020-03-19T23:41:56.699Z","creationTime":"2020-03-20T04:44:21.581+01:00","count":1,"self":"https://www.google.com","source":{"self":"https://www.test.source","id":"234567"},"time":"2020-03-20T16:40:05.011Z","text":"This is a test creation alert","id":"098765","type":"TestType","status":"ACTIVE"},"realtimeAction":"CREATE"}';
		
        // Test the Platform Event trigger for CREATE with no matching Cumulocity ID
        iConn_Alert__e createTestNoMatch = New iConn_Alert__e();
        createTestNoMatch.Alert_String__c = '{"data":{"severity":"TEST WARNING","firstOccurrenceTime":"2020-03-19T23:41:56.699Z","creationTime":"2020-03-20T04:44:21.581+01:00","count":1,"self":"https://www.google.com","source":{"self":"https://www.test.source","id":"345678"},"time":"2020-03-20T16:40:05.011Z","text":"This is a test non-matching creation alert","id":"098765","type":"TestType","status":"ACTIVE"},"realtimeAction":"CREATE"}';
		
        Test.startTest();
        
		// Publish UPDATE event
        Database.SaveResult resultUp = EventBus.publish(upTest);
        
        if (resultUp.isSuccess()) {
            System.debug('Successfully published UPDATE event.');
        } else {
            for(Database.Error err : resultUp.getErrors()) {
                System.debug('Error returned from UPDATE event: ' +
                            err.getStatusCode() +
                            ' - ' +
                            err.getMessage());
            }
        }
        
        // Publish CREATE event
        Database.SaveResult resultCreate = EventBus.publish(createTest);
        
        if (resultCreate.isSuccess()) {
            System.debug('Successfully published CREATE event.');
        } else {
            for(Database.Error err : resultCreate.getErrors()) {
                System.debug('Error returned from CREATE event: ' +
                            err.getStatusCode() +
                            ' - ' +
                            err.getMessage());
            }
        }
        
        // Publish non-matching CREATE event
        Database.SaveResult resultNoMatchCreate = EventBus.publish(createTestNoMatch);
        
        if (resultNoMatchCreate.isSuccess()) {
            System.debug('Successfully published non-matching CREATE event.');
        } else {
            for(Database.Error err : resultNoMatchCreate.getErrors()) {
                System.debug('Error returned from non-matching CREATE event: ' +
                            err.getStatusCode() +
                            ' - ' +
                            err.getMessage());
            }
        }
        
        Test.stopTest();
    }
}