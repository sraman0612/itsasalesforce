public with sharing class COR_GMapsDistanceFinder {
    
    public static List <GMapsDistance> orderServiceCentersByDistance (Account originAccount, List <Account> destinationAccounts) {
        
        List <GMapsDistance> destinations = new List <GMapsDistance> ();
        
        string originAddress = getFormattedAddressFromAccount(originAccount);

        
        string destinationAddresses = '';
        for (Account d : destinationAccounts) {
            destinations.add(new GMapsDistance(d));
            string destinationAddress = getFormattedAddressFromAccount(d);
            System.debug('INPUT DESTINATION: '+destinationAddress);
            if (destinationAddresses != '') destinationAddresses += '|';
            destinationAddresses += destinationAddress;
        }
        
        string accessToken = ((Google_Maps_Settings__c)[Select API_Access_Key__c From Google_Maps_Settings__c Where Name = 'Google Maps API Key' Limit 1]).API_Access_Key__c;
        string endpoint = 'https://maps.googleapis.com/maps/api/distancematrix/json?key='+accessToken+'&origins='+originAddress+'&destinations='+destinationAddresses;
        
        System.debug('ENDPOINT: '+ endpoint);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeout(60000);
        
        HttpResponse res = new Http().send(req);
        
        string responseBody = res.getBody();
        System.debug('RESPONSE: '+ responseBody);
        
        COR_JSONParser parser = new COR_JSONParser(responseBody);
        COR_JSONParser.JSONObject jsonObject = parser.objects[0];
        //System.debug('STRUCTURE: ');
        //parser.printStructure();
        try {
            Integer index = 0;
            List <string> destinationsFound = jsonObject.arraysOfValues.get('destination_addresses');
            List <COR_JSONParser.JSONObject> travelMetricElements = jsonObject.arraysOfObjects.get('rows')[0].arraysOfObjects.get('elements');
            
            
            for (string token : destinationsFound) {
                GMapsDistance distanceMetric = destinations[index];
                distanceMetric.outputShippingAddress = destinationsFound[index];
                
                COR_JSONParser.JSONObject travelMetric = travelMetricElements[index];
                    
                string returnCode = travelMetric.values.get('status');
                distanceMetric.isValid = parseBoolean(returnCode);
                
                if (distanceMetric.isValid) {
                    COR_JSONParser.JSONObject distance = travelMetric.objects.get('distance');
                    COR_JSONParser.JSONObject duration = travelMetric.objects.get('duration');
                    
                    distanceMetric.setDistanceText(distance.values.get('text'));
                    
                    //distanceMetric.distanceNumber     = Integer.valueOf(distance.values.get('value'));
                    distanceMetric.setDistanceNumber(distance.values.get('value'));
                    distanceMetric.durationText     = duration.values.get('text');
                    //distanceMetric.durationNumber = Integer.valueOf(duration.values.get('value'));
                    distanceMetric.setDurationNumber(duration.values.get('value'));
                }
                System.debug('DESTINATION '+ destinations[index].toString());
                index++;
                
            }
        }
        catch (Exception ex) {
            System.debug(ex.GetMessage());
        }
        
        destinations = sortByDistances(destinations);
        
        return destinations;
    }
    
    
    private static List <GMapsDistance> sortByDistances (List <GMapsDistance> distanceMetricList) {
        List <COR_GMapsDistanceSortWrapper> sortingList = new List <COR_GMapsDistanceSortWrapper> ();
        for (GMapsDistance distanceMetric : distanceMetricList) {
            sortingList.add(new COR_GMapsDistanceSortWrapper(distanceMetric));
        }
        sortingList.sort();
        distanceMetricList.clear();
        for (COR_GMapsDistanceSortWrapper sortedMetric : sortingList) {
            distanceMetricList.add(sortedMetric.distanceMetric);
        }
        return distanceMetricList;
    }
    
    
    private static string getFormattedAddressFromAccount (Account a) {
        string street   = '';
        string city     = '';
        string state    = '';
        string zip      = '';
        string country  = '';
        
        string whichAddress = determineMoreCompleteAddress(a);
        if (whichAddress == 'shipping') {
            street      = getSafeString(a.ShippingStreet);
            city        = getSafeString(a.ShippingCity);
            state       = getSafeString(a.ShippingState);
            zip         = getSafeString(a.ShippingPostalCode);
            country     = getSafeString(a.ShippingCountry);
        }
        else if (whichAddress == 'billing') {
            street      = getSafeString(a.BillingStreet);
            city        = getSafeString(a.BillingCity);
            state       = getSafeString(a.BillingState);
            zip         = getSafeString(a.BillingPostalCode);
            country     = getSafeString(a.BillingCountry);
        }
        else {
            // This condition shouldn't ever be reached, but just in case there's an error somewhere...
            System.debug('UNABLE TO DETERMINE WHICH ADDRESS TO USE FOR ACCOUNT: '+a.Id);
        }
        
        string address = street+' '+city+' '+state+' '+zip+' '+country;
        address = EncodingUtil.urlEncode(address, 'UTF-8');
        return address;
    }
    
    private static string determineMoreCompleteAddress (Account a) {
        Integer shippingParts   = 0;
        Integer billingParts    = 0;
        
        if (!String.isEmpty(a.ShippingStreet))      shippingParts++;
        if (!String.isEmpty(a.ShippingCity))        shippingParts++;
        if (!String.isEmpty(a.ShippingState))       shippingParts++;
        if (!String.isEmpty(a.ShippingPostalCode))  shippingParts++;
        if (!String.isEmpty(a.ShippingCountry))     shippingParts++;
        
        if (!String.isEmpty(a.BillingStreet))       billingParts++;
        if (!String.isEmpty(a.BillingCity))         billingParts++;
        if (!String.isEmpty(a.BillingState))        billingParts++;
        if (!String.isEmpty(a.BillingPostalCode))   billingParts++;
        if (!String.isEmpty(a.BillingCountry))      billingParts++;
        
        if (shippingParts > billingParts) return 'shipping';
        else if (billingParts > shippingParts) return 'billing';
        else return 'shipping';
    }
    
    
    private static string getSafeString (string input) {
        try {
            if (input != null) return input;
            else return '';
        }
        catch (Exception ex) {
            return '';
        }
    } 
    
    private static string getFormattedAddress (string address) {
        return EncodingUtil.urlEncode(address, 'UTF-8');
    }
    
    
    private static boolean parseBoolean (string input) {
        if (input.contains('OK'))   return true;
        else                        return false;
    }
    
    
    public class GMapsDistance {
        public Account  account                     {get; private set;}
        public boolean  isValid                     {get; set;}
        public string   inputShippingAddress        {get; private set;}
        public string   outputShippingAddress       {get; set;}
        public string   distanceText                {get; private set;}
        public Integer  distanceNumber              {get; private set;}
        public Integer  distanceInMiles             {get; private set;}
        public string   durationText                {get; set;}
        public Integer  durationNumber              {get; private set;}
        public Integer  durationInMinutes           {get; private set;}
        public string   newDisposition              {get; set;}
        public string   newDeclineReason            {get; set;}
        public string   designation                 {get; set;}
        public decimal  managedAverage              {get; private set;}
        
        // This will most likely contain HTML to satisfy an edge requirement.
        public string   accountName                 {get; set;}
        
        public GMapsDistance (Account a) {
            this.account                    = a;
            this.isValid                    = false;        
            this.inputShippingAddress       = COR_GMapsDistanceFinder.getFormattedAddressFromAccount(a);
            this.outputShippingAddress      = '';
            this.distanceText               = '';
            this.distanceNumber             = -1;
            this.durationText               = '';
            this.durationNumber             = -1;
            this.newDisposition             = a.Tertiary_Disposition__c;
            this.newDeclineReason           = a.Tertiary_Decline_Reason__c;
            this.accountName                = this.account.Name;
            this.managedAverage             = this.account.Managed_Care_Survey_Average__c;
        }
        
        public void setDistanceText (string distanceText) {
            string[] tokens = distanceText.split(' ');
            string output = '';
            string noCommaDistance = tokens[0].replace(',', '');
            Decimal floatingPointDistance = Decimal.valueOf(noCommaDistance).round(System.RoundingMode.HALF_EVEN);
            string milesDistance = Integer.valueOf((Integer.valueOf(floatingPointDistance) * 0.621371))+'';
            output += milesDistance+' mi';
            this.distanceText = output;
        }
        
        public override string toString () {
            return 'Destination: '+ this.account.Id +' | '+ this.isValid +' | '+ this.inputShippingAddress +' | '+ this.outputShippingAddress +' | '+ this.distanceText +' | '+ this.distanceNumber +' | '+ this.durationText +' | '+ this.durationNumber + ' | '+ this.distanceInMiles +' | '+ this.durationInMinutes;
        }
        
        
        public void setDurationNumber (String durationNumber) {
            this.durationNumber = Integer.valueOf(durationNumber);
            this.durationInMinutes = this.durationNumber / 60;
        }
        
        public void setDistanceNumber (String distanceNumber) {
            this.distanceNumber = Integer.valueOf(distanceNumber);
            this.distanceInMiles = Integer.valueOf((Integer.valueOf(this.distanceNumber / 1000) * 0.621371));
        }
        
        
        public String getDialablePhoneNumber () {
            if (this.account != null) {
                String phoneNumber = this.account.phone;
                
                if (!String.isEmpty(phoneNumber)) {
                    phoneNumber = phoneNumber.replaceAll(' ', '');
                    phoneNumber = phoneNumber.replaceAll('\\(', '');
                    phoneNumber = phoneNumber.replaceAll('\\)', '');
                    phoneNumber = phoneNumber.replaceAll('\\+', '');
                    phoneNumber = phoneNumber.replaceAll('-', '');
                    phoneNumber = phoneNumber.replaceAll('\\[', '');
                    phoneNumber = phoneNumber.replaceAll('\\]', '');
                    
                    return phoneNumber;
                }
            }
            
            return '';
        }
        
        
        
        public void setNameBold () {
            this.accountName = '<b><div style="background-color:#d9d9d9">'+ this.accountName +'</div></b>';
        }
    }

}