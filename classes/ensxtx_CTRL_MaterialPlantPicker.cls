public with sharing class ensxtx_CTRL_MaterialPlantPicker {
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ensxtx_CTRL_MaterialPlantPicker.class);

    @AuraEnabled
    public static ensxtx_UTIL_Aura.Response getMaterial(String materialNumber) {
        ensxtx_SBO_EnosixMaterial_Detail.EnosixMaterial material = null;
        logger.enterAura('getMaterial', new Map<String, Object> {
            'materialNumber' => materialNumber
        });
        try {
            material = new ensxtx_SBO_EnosixMaterial_Detail().getDetail(materialNumber);
            ensxtx_UTIL_PageMessages.addFrameworkMessages(material.getMessages());
        } catch (Exception ex) {
            ensxtx_UTIL_PageMessages.addExceptionMessage(ex);
        } finally {
            logger.exit();
        }
        return ensxtx_UTIL_Aura.createResponse(material);
    }
}