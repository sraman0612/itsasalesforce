@isTest(SeeAllData=false)
public with sharing class TSTU_EnosixZAPRSearch
{
    class MockSearch implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        Boolean isSuccess;
        public MockSearch(Boolean isSuccess)
        {
            this.isSuccess = isSuccess;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext sc)
        {
            SBO_EnosixZAPR_Search.EnosixZAPR_SC searchContext = (SBO_EnosixZAPR_Search.EnosixZAPR_SC)sc;
            SBO_EnosixZAPR_Search.EnosixZAPR_SR searchResult =
                new SBO_EnosixZAPR_Search.EnosixZAPR_SR();

            if (isSuccess)
            {
                SBO_EnosixZAPR_Search.SEARCHRESULT result = new SBO_EnosixZAPR_Search.SEARCHRESULT();
                result.SalesOrganization = searchContext.SEARCHPARAMS.SalesOrganization;
                result.DistributionChannel = searchContext.SEARCHPARAMS.DistributionChannel;
                result.Country = searchContext.SEARCHPARAMS.Country;
                result.Region = searchContext.SEARCHPARAMS.Region;
                result.County = searchContext.SEARCHPARAMS.County;
                result.CustomerNumber = '12345';
                result.Sequence = '1';
                result.SalesDistrict = '43';
                result.SalesDistrictName = 'McTester,Testy';

                searchResult.SearchResults.add(result);

                result = new SBO_EnosixZAPR_Search.SEARCHRESULT();
                result.SalesOrganization = searchContext.SEARCHPARAMS.SalesOrganization;
                result.DistributionChannel = searchContext.SEARCHPARAMS.DistributionChannel;
                result.Country = searchContext.SEARCHPARAMS.Country;
                result.Region = searchContext.SEARCHPARAMS.Region;
                result.County = searchContext.SEARCHPARAMS.County;
                result.CustomerNumber = '32355';
                result.Sequence = '2';
                result.SalesDistrict = '43';
                result.SalesDistrictName = 'Nasty McRudey';

                searchResult.SearchResults.add(result);
            }

            searchResult.setSuccess(isSuccess);
            searchContext.baseResult = searchResult;

            return searchContext;
        }
    }

    @isTest
    static void test_createleadhappypath()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSearch(true));

        User testTSM = UTIL_EnosixZAPRSearch.createTestUser('Testy', 'McTester');

        String recTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'lead' AND Name = 'Manual (External)'].Id;

        Test.startTest();

        Lead myLead = new Lead();
        myLead.LastName = 'McTesty';
        myLead.Company = 'McTesty Testing';
        myLead.Country = 'US';
        myLead.State = 'IL';
        myLead.ProductCategory__c = 'Blower';
        myLead.FLD_Sales_Organization__c = 'GDMI';
        myLead.FLD_County__c = 'ADAMS';
        myLead.RecordTypeId = recTypeId;
        insert myLead;

        Lead myLead2 = new Lead();
        myLead2.LastName = 'McTesty2';
        myLead2.Company = 'McTesty Testing2';
        myLead2.Country = 'US';
        myLead2.State = 'IL';
        myLead2.ProductCategory__c = 'ChampionUS';
        myLead2.FLD_Sales_Organization__c = 'GDMI';
        myLead2.FLD_County__c = 'ADAMS';
        myLead2.RecordTypeId = recTypeId;
        insert myLead2;

        Lead myLead3 = new Lead();
        myLead3.LastName = 'McTesty3';
        myLead3.Company = 'McTesty Testing3';
        myLead3.Country = 'US';
        myLead3.State = 'IL';
        myLead3.ProductCategory__c = 'Compressor';
        myLead3.FLD_Sales_Organization__c = 'GDMI';
        myLead3.FLD_County__c = 'ADAMS';
        myLead3.RecordTypeId = recTypeId;
        insert myLead3;

        Lead myLead4 = new Lead();
        myLead4.LastName = 'McTesty4';
        myLead4.Company = 'McTesty Testing4';
        myLead4.Country = 'US';
        myLead4.State = 'IL';
        myLead4.ProductCategory__c = 'DVSystems';
        myLead4.FLD_Sales_Organization__c = 'GDMI';
        myLead4.FLD_County__c = 'ADAMS';
        myLead4.RecordTypeId = recTypeId;
        insert myLead4;

        Lead myLead6 = new Lead();
        myLead6.LastName = 'McTesty6';
        myLead6.Company = 'McTesty Testing6';
        myLead6.Country = 'US';
        myLead6.Street = '123 Easy Street';
        myLead6.City = 'Quincy';
        myLead6.State = 'IL';
        myLead6.PostalCode = '62305-1234';
        myLead6.ProductCategory__c = 'LeROI';
        myLead6.FLD_Sales_Organization__c = 'GDMI';
        myLead6.RecordTypeId = recTypeId;
        insert myLead6;

        Lead myLead7 = new Lead();
        myLead7.LastName = 'McTesty7';
        myLead7.Company = 'McTesty Testing7';
        myLead7.Country = 'US';
        myLead7.State = 'IL';
        myLead7.ProductCategory__c = 'Vacuum';
        myLead7.FLD_Sales_Organization__c = 'GDMI';
        myLead7.FLD_County__c = 'ADAMS';
        myLead7.RecordTypeId = recTypeId;
        insert myLead7;

        Lead myLead8 = new Lead();
        myLead8.LastName = 'McTesty8';
        myLead8.Company = 'McTesty Testing8';
        myLead8.Country = 'US';
        myLead8.State = 'IL';
        myLead8.ProductCategory__c = 'Locomotive';
        myLead8.FLD_Sales_Organization__c = 'GDMI';
        myLead8.FLD_County__c = 'ADAMS';
        myLead8.RecordTypeId = recTypeId;
        insert myLead8;

        Lead myLead9 = new Lead();
        myLead9.LastName = 'McTesty9';
        myLead9.Company = 'McTesty Testing9';
        myLead9.Country = 'US';
        myLead9.State = 'IL';
        myLead9.ProductCategory__c = 'OFCompressor';
        myLead9.FLD_Sales_Organization__c = 'GDMI';
        myLead9.FLD_County__c = 'ADAMS';
        myLead9.RecordTypeId = recTypeId;
        insert myLead9;

        Lead myLead10 = new Lead();
        myLead10.LastName = 'McTesty10';
        myLead10.Company = 'McTesty Testing10';
        myLead10.Country = 'US';
        myLead10.State = 'IL';
        myLead10.ProductCategory__c = 'Transport';
        myLead10.FLD_Sales_Organization__c = 'GDMI';
        myLead10.FLD_County__c = 'ADAMS';
        myLead10.RecordTypeId = recTypeId;
        insert myLead10;

        Lead myLead11 = new Lead();
        myLead11.LastName = 'McTesty11';
        myLead11.Company = 'McTesty Testing11';
        myLead11.Country = 'US';
        myLead11.State = 'IL';
        myLead11.ProductCategory__c = 'Vacuum';
        myLead11.FLD_Sales_Organization__c = 'GDMI';
        myLead11.FLD_County__c = 'ADAMS';
        myLead11.RecordTypeId = recTypeId;
        insert myLead11;

        Test.stopTest();

        Lead thisLead = [Select Id, OwnerId, Owner.Name, FLD_Customer_Numbers__c from Lead where Id =: myLead.Id LIMIT 1];

        //System.assertEquals(testTSM.Id,thisLead.OwnerId);
        //System.assertEquals('12345,32355', thisLead.FLD_Customer_Numbers__c);

        User dummyUser = UTIL_EnosixZAPRSearch.getDummyUser();

        User testUser = [Select Id,LastName from User where FirstName =: UTIL_EnosixZAPRSearch.UNASSIGNED_USER_FNAME and LastName =: UTIL_EnosixZAPRSearch.UNASSIGNED_USER_LNAME];
        String manager =[Select Id from User where FirstName = 'Nathan' and LastName = 'Holthaus' LIMIT 1].Id;
        testUser.LastName = 'DummyUser999';
        testUser.ManagerId = manager;
        update testUser;

        User dummyUser2 = UTIL_EnosixZAPRSearch.getDummyUser();
    }

    @isTest
    static void test_createleadmissinginfo()
    {
        ensxsdk.EnosixFramework.setMock(SBO_EnosixZAPR_Search.class, new MockSearch(false));

        User testTSM = UTIL_EnosixZAPRSearch.createTestUser('Sir', 'TestAlot');

        Test.startTest();
            Lead myLead12 = new Lead();
            myLead12.LastName = 'McTesty10101010';
            myLead12.Company = 'McTesty Testing10101010';
            myLead12.Country = 'AAA';
            myLead12.State = 'BB';
            insert myLead12;
        Test.stopTest();

        Lead thisLead = [Select Id, OwnerId, Owner.Name, FLD_Customer_Numbers__c from Lead where Id =: myLead12.Id LIMIT 1];

        //System.assertEquals(null,thisLead.FLD_Customer_Numbers__c);

        User dummyUser = UTIL_EnosixZAPRSearch.getDummyUser();
    }
}