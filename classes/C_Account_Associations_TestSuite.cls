@isTest
private class C_Account_Associations_TestSuite {

    @testSetup
    static void setupData(){
    
        Id customerRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id serviceRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Service Center').getRecordTypeId();
    
        Account a1 = new Account();
        a1.Name = 'Customer';
        a1.shippingStreet = '123 Fake St';
        a1.shippingCity = 'Milwaukee';
        a1.shippingState = 'WI';
        a1.shippingPostalCode = '53211';
        a1.RecordTypeId = customerRTId;
        insert a1;
        
        
        
        Account a2 = new Account();
        a2.Name = 'Service Center';
        a2.billingStreet = '123 Fake St';
        a2.billingCity = 'Racine';
        a2.billingState = 'WI';
        a2.billingPostalCode = '53402';
        a2.RecordTypeId = serviceRTId;
        insert a2;
        
        Google_Maps_Settings__c gms = new Google_Maps_Settings__c();
        gms.API_Access_Key__c = 'testKey123_abc';
        gms.name = 'Google Maps API Key';
        insert gms;
    
    }
    
    static testMethod void runCOR(){
    
        Account cust = [SELECT Id FROM Account WHERE Name = 'Customer'];
        Account sc = [SELECT Id FROM Account WHERE Name = 'Service Center'];
        
        Test.setMock(HttpCalloutMock.class, new COR_HTTP_Mock());
        
        Account_Association__c aa = new Account_Association__c();
        aa.Customer__c = cust.id;
        aa.Service_Center__c = sc.id;
        aa.Active__c = true;                
        
        Test.startTest();
        insert aa;
        Test.stopTest();
    }
    
    static testMethod void runCOR_Standalone(){
        Account cust = [SELECT Id, Managed_Care_Survey_Average__c, Tertiary_Disposition__c, Tertiary_Decline_Reason__c, Name, ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode FROM Account WHERE Name = 'Customer'];
        Account sc = [SELECT Id, Managed_Care_Survey_Average__c, Tertiary_Disposition__c, Tertiary_Decline_Reason__c, Name, ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode FROM Account WHERE Name = 'Service Center'];
        cust.Phone = '(414)544-7100';
        COR_GMapsDistanceFinder.GMapsDistance gmd = new COR_GMapsDistanceFinder.GMapsDistance(cust);
        gmd.getDialablePhoneNumber();
        gmd.setNameBold();
        
        COR_GMapsDistanceFinder.GMapsDistance gmd2 = new COR_GMapsDistanceFinder.GMapsDistance(sc);
        
        COR_GMapsDistanceSortWrapper sort1 = new COR_GMapsDistanceSortWrapper(gmd);
        COR_GMapsDistanceSortWrapper sort2 = new COR_GMapsDistanceSortWrapper(gmd);
        sort1.compareTo(sort2);
        sort2.compareTo(sort1);
        
        COR_GMapsDistanceSortWrapper sort3 = new COR_GMapsDistanceSortWrapper(gmd);
        COR_GMapsDistanceSortWrapper sort4 = new COR_GMapsDistanceSortWrapper(gmd2);
        sort3.compareTo(sort4);
        sort4.compareTo(sort3);

    }
}