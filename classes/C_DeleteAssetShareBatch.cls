global class C_DeleteAssetShareBatch implements Database.Batchable<sObject> {
    global final String Query;
    global final List<string> aIDs;
    
    global C_DeleteAssetShareBatch(string q, List<string> acctIDs)
    {
        aIDs = acctIDs;
        if(q != null)
        	Query=q;
        else
            Query='Select id from assetshare where userorgroupid IN (Select id from User where accountID IN :aIDs and toLabel(Community_User_Type__c)=\'Global Account\') ';   
        
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug(aids);
        system.debug(query);
      return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<assetshare> scope){
        List<AssetShare>assetShareToDel = scope;
        delete assetShareToDel;
       
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('Finished deleting rights now to create!!!!!');
        C_CreateAssetShareBatch cBatch = new C_CreateAssetShareBatch(null, aIDs);
        Database.executeBatch(cBatch);
    }
}