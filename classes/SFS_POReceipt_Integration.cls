/*=========================================================================================================
* @author Naresh Ponneri, Capgemini
* @date 13/05/2022
* @description: PO Receipt_Integration
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/

public class SFS_POReceipt_Integration {
    public static Map<String,HttpResponse> responseXMLMap = new Map<String,HttpResponse>();
    public class getpoReceipt{
        @InvocableVariable
        public ID woliId;
        @InvocableVariable
        public id saId;
        @InvocableVariable
        public id workOrderId;
    }
    public static SFS_SFS_Integration_Endpoint__mdt endpoint = [SELECT SFS_Endpoint_URL__c,SFS_ContentType__c,SFS_Username__c, SFS_Password__c, SFS_RICE_ID__c, SFS_Interface_Destination__c
                                                                FROM SFS_SFS_Integration_Endpoint__mdt WHERE Label =:'XXINV2638'];

    @InvocableMethod(label='SFS_POReceipt_Integration' description='PO Receipt Integration' category='Service Agrement')
    public static void Run(List<getpoReceipt> woliId){
    List<ProductRequest> prIdList = new List<ProductRequest>();
        if(woliId[0].woliId != null){
            //These are not created by Maintenance Plans. We should get all Product Requests under the WOLI.
            priDlist=[Select Id,WorkOrderLineItemId,WorkOrderId from ProductRequest Where SFS_Auto_Receipt_Sent__c = False AND WorkOrderLineItemId=:woliId[0].woliId];
        }else{
            //These are created by Maintenance Plans. We should get all Product Requests under the Work Order.
            priDlist=[Select Id,WorkOrderLineItemId,WorkOrderId from ProductRequest Where SFS_Auto_Receipt_Sent__c = False AND WorkOrderId =:woliId[0].workOrderId];
        }
        List<Id> PrIds=new List<Id>();
        //List<ProductRequest> priDlist=[Select Id,WorkOrderLineItemId,WorkOrderId from ProductRequest Where WorkOrderLineItemId=:woliId[0].woliId];
       // List<ProductRequest> priDlist=[Select Id,WorkOrderLineItemId,WorkOrderId from ProductRequest Where WorkOrderId =:woliId[0].woliIds];
        for(ProductRequest pr:priDlist){
            PrIds.add(pr.id);
        }
        SFS_POReceipt_Integration_Batch poBatch=new SFS_POReceipt_Integration_Batch(PrIds);
        Database.executeBatch(poBatch,1);
    }
    public static void ProcessData(ProductRequest pr,List<ShipmentItem> siList,Integer splitShipmentCount){
        //try{
            String xmlString = '';
            Map<Double, String> prXmlMap = new Map<Double, String>();
            SFS_PO_Receipt_Integrations__mdt[] poReceiptMAP = [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c, SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Hardcoded_Flag__c
                                                               FROM SFS_PO_Receipt_Integrations__mdt Order By SFS_Node_Order__c asc];
            System.debug('@@@@ProductRequest'+pr);
            System.debug('@@@@ProductRequestLineItem'+siList);
            MAP<String,Object> finalProductRequestFields = new MAP<String,Object>();
            Map<String, Object> prFields = new Map <String, Object>();
            prFields = pr.getPopulatedFieldsAsMap();

            Map<Integer,Map<String,Object>> lineItemsMap = new Map<Integer,Map<String,Object>>();
            for(ShipmentItem si:siList){
                Map<String,Object> ShipmentItemFields = new Map <String, Object>();
                ShipmentItemFields = si.getPopulatedFieldsAsMap();
                Map<String,Object> finalSIFields = new Map<String,Object>();
                for(String field : ShipmentItemFields.keyset()){
                    finalSIFields.put(field,si.get(field));
                    if(field=='Quantity'){
                        String quantityString =string.valueOf(si.get(field));
                        system.debug('@@@@@@@@@@@@@'+quantityString);
                        string quantity=quantityString.removeEnd('.00');
                        finalSIFields.put(field,quantity);
                    }
                    if(field=='SFS_Oracle_Requisition_Line_Number__c'){
                        String documentString =string.valueOf(si.get(field));
                        string document=documentString.substringAfter('-');
                        finalSIFields.put(field,document);
                    }
                    system.debug('@@@@finalPRLIFields'+finalSIFields);
                }
                lineItemsMap.put(siList.indexOf(si),finalSIFields);
                system.debug('@@@@lineItemsMap'+lineItemsMap);
            }
            for(String field : prFields.keyset())
            {
                finalProductRequestFields.put(field,pr.get(field));
                if(field=='SFS_External_Id__c'){
                    String Partials = pr.get(field) + String.valueOf(System.Today());
                    finalProductRequestFields.put(field,Partials);
                }
            }
            List<String> prXMLList = new List<String>();
            SFS_POReceipt_Integration serviceOrder = new SFS_POReceipt_Integration();
            prXMLList = serviceOrder.buildXmlStructure(poReceiptMAP,finalProductRequestFields,'ProductRequest');
            List<String> prliXMLList = new List<String>();
            for(Integer i=0;i<lineItemsMap.Size();i++){
                Map<String,Object> lineItemsFieldsMap = (Map<String, Object>)lineItemsMap.get(i);
                system.debug('@@@@Keyset'+lineItemsFieldsMap.keySet());
                prliXMLList = serviceOrder.buildXmlStructure(poReceiptMAP,lineItemsFieldsMap,'ShipmentItem');
                prXMLList.addAll(prliXMLList);
                system.debug('@@@@prXMLList'+prXMLList);
            }
            SFS_PO_Receipt_Integrations__mdt so = SFS_PO_Receipt_Integrations__mdt.getInstance('SFS_Headers');
            prXMLList.add(so.SFS_XML_Full_Name__c);
            for(String s : prXMLList){
                xmlString = xmlString + s;
                System.debug('XML: ' + s);
            }
            system.debug('@@@@@@@@@@@xmlString'+xmlString);
            String interfaceLabel = 'PO Receipt Integration';
            HttpMethod(xmlString,pr,splitShipmentCount);
        }
        //catch(exception e){

       // }
    //}
    public List<String> buildXmlStructure(SFS_PO_Receipt_Integrations__mdt[] poReceiptMap,Map<String,Object>lineItemsFieldsMap,String ObjectName){
        List<String> xmlStructure = new List<String>();
        for(SFS_PO_Receipt_Integrations__mdt m: poReceiptMap){
            if(m.SFS_Salesforce_Object__c==ObjectName){
                if(m.SFS_HardCoded_Flag__c == false){
                    String sfField = m.SFS_Salesforce_Field__c;
                    system.debug('@@@@HI@@@@@');
                    if(lineItemsFieldsMap.containsKey(sfField)){
                        system.debug('---Enter Into If Condition----');
                        if(lineItemsFieldsMap.get(sffield) != null){
                            String xmlfullName = m.SFS_XML_Full_Name__c;
                            string replacement = String.ValueOf(lineItemsFieldsMap.get(sffield));
                            String newpa = XMLFullName.replace(sffield,replacement);
                            xmlStructure.add(newpa);
                        }
                        else if(lineItemsFieldsMap.get(sffield) == null){
                            xmlStructure.add(m.SFS_XML_Full_Name__c);
                        }
                    }
                    else if(!lineItemsFieldsMap.ContainsKey(sffield)){
                        String empty = '';
                        String replacement = m.SFS_XML_Full_Name__c.replace(sffield,empty);
                        xmlStructure.add(replacement);
                    }
                }
                else if(m.SFS_HardCoded_Flag__c == true){
                    xmlStructure.add(m.SFS_XML_Full_Name__c);
                }
            }
        }
        return xmlStructure;
    }
    public static void HttpMethod(String xmlString,ProductRequest pr,Integer splitShipmentCount){
        try{
            system.debug('insideHttpCallout'+endpoint.SFS_Endpoint_URL__c);
            //Http Request
            Blob headervalue = Blob.valueOf(endpoint.SFS_Username__c + ':' + endpoint.SFS_Password__c);
            String authorizationHeader = 'Basic' + EncodingUtil.base64Encode(headerValue);
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint.SFS_Endpoint_URL__c);
            request.setMethod('POST');
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('Content-Type',endpoint.SFS_ContentType__c);
            request.setTimeout(120000);
            request.setBody(xmlString);

            System.debug('Payload: ' + xmlString);

            //Get Http Response
            if(!Test.isRunningTest()){
                HttpResponse response = http.send(request);
                responseXMLMap.put(xmlString,response);

                system.debug('splitShipmentCount' + splitShipmentCount);
                system.debug('responseXMLMap' + responseXMLMap.keyset().size());
                if(splitShipmentCount == responseXMLMap.keyset().size()){
                    createIntegrationLog(responseXMLMap, 'PO Receipt', endpoint);
                    system.debug('@@@response'+response);
                    System.debug('STATUS: ' + response.getStatusCode());
                    System.debug('Error Message: ' +response.getBody());
                    If(response.getStatusCode() != 200){
                        pr.SFS_Integration_Status__c = 'ERROR';
                        pr.SFS_Integration_Response__c = response.getBody();
                        pr.SFS_Auto_Receipt_Sent__c = True;
                    } else{
                        //pr.SFS_Integration_Status__c = 'SYNCING';
                        pr.SFS_Auto_Receipt_Sent__c = True;
                    }
                    update pr;
                }
            }
        }
        catch(SObjectException e){
            system.debug('@@@exception'+e.getMessage());
        }
    }


    public static void createIntegrationLog(Map<String,HttpResponse> responseXMLMap, String interfaceLabel, SFS_SFS_Integration_Endpoint__mdt integMeta){

        for(String xmlString : responseXMLMap.keyset()){
            SFS_Integration_Log__c log = new SFS_Integration_Log__c();
            HttpResponse  response = responseXMLMap.get(xmlString);
            String label = interfaceLabel +' '+ datetime.now();
            log.SFS_Label__c = label.abbreviate(254);
            log.SFS_Label_Full_Length__c = label;
            log.RICE_ID__c = integMeta.SFS_RICE_ID__c;
            log.Interface_Destination__c = integMeta.SFS_Interface_Destination__c;
            log.SFS_Integration_Status_Code__c = String.ValueOf(response.getStatusCode());
            if(response.getStatusCode() == 200 ||response.getStatusCode() ==202){
                log.SFS_Integration_Status__c = 'Success';
                log.SFS_Integration_Response__c = response.getBody();
            }
            else{
                log.SFS_Integration_Status__c = 'Fail';
                log.SFS_Integration_Response__c = response.getBody();
            }
            insert log;

            Attachment att = new Attachment();
            att.ParentId = log.Id;
            att.name = 'XSD ' +DateTime.now();
            att.ContentType = 'text/xml';
            att.Body = Blob.ValueOf(xmlString);
            insert att;
        }
    }
}