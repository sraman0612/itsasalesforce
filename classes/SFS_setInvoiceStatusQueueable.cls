/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 21/12/2022
* @description:
* @Story Number: 

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================*/
public class SFS_setInvoiceStatusQueueable implements Queueable {
    public List<Invoice__c> invList ; 
    public SFS_setInvoiceStatusQueueable( List<Invoice__c> invList){
        this.invList = invList ;  
    }
    public void execute(QueueableContext context) {      
      update invList;
    }
}