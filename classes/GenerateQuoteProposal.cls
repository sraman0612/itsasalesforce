public with sharing class GenerateQuoteProposal {

    public String save(QuoteProposalModel context) {
        return Test.isRunningTest()?'':SBQQ.ServiceRouter.save('SBQQ.QuoteDocumentAPI.Save', JSON.serialize(context));
    }
}