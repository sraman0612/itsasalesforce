public without sharing class ExpenseClaimInvocationHandler {
    private static final ensxsdk.Logger LOGGER = new ensxsdk.Logger(ExpenseClaimInvocationHandler.class);

    @InvocableMethod(
        label='Create Warranty Expense Item'
        description='Creates Warranty Expense Items from the supplied JSON and associated to the supplied warranty claim id.'
    )
    public static void createWarrantyExpenseItems(List<FlowInput> input) {
        LOGGER.enterAura('createWarrantyExpenseItems', new Map<String, Object>{ 'input' => input });

        try {
            List<OBJ_WarrantyClaimItem> expenseItems = getExpenseItems(input);

            if (!expenseItems.isEmpty()) {
                saveWarrantyClaimExpenses(input[0].warrantyClaimId, expenseItems);
            }
        } catch (Exception e) {
            LOGGER.error(e);
        } finally {
            LOGGER.exit();
        }
    }

    private static List<OBJ_WarrantyClaimItem> getExpenseItems(List<FlowInput> input) {
        List<OBJ_WarrantyClaimItem> retVal = new List<OBJ_WarrantyClaimItem>();

        if (String.isNotBlank(input[0].itemJSON)) {
            List<OBJ_WarrantyClaimItem> otherExpenses = (List<OBJ_WarrantyClaimItem>) JSON.deserialize(
                input[0].itemJSON,
                List<OBJ_WarrantyClaimItem>.class
            );

            return otherExpenses;
        }

        return retVal;
    }

    public static void saveWarrantyClaimExpenses(String warrantyClaimId, List<OBJ_WarrantyClaimItem> warrantyClaimItems) {
        List<Warranty_Claim_Expenses__c> toUpsert = new List<Warranty_Claim_Expenses__c>();

        List<String> productCodes = new List<String>();

        for (OBJ_WarrantyClaimItem curItem : warrantyClaimItems) {
            if (!String.isBlank(curItem.PartNo)) {
                productCodes.add(curItem.PartNo);
            }
        }

        List<Product2> parts = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode IN :productCodes];
        Map<String, Id> productCodeMap = new Map<String, Id>();

        for (Product2 part : parts) {
            productCodeMap.put(part.ProductCode, part.Id);
        }

        for (OBJ_WarrantyClaimItem curItem : warrantyClaimItems) {
            if (String.isBlank(curItem.PartNo)) {
                continue;
            }

            Warranty_Claim_Expenses__c expense = new Warranty_Claim_Expenses__c();
            expense.Warranty_Claim__c = warrantyClaimId;
            toUpsert.add(expense);

            expense.SAP_Item_Number__c = curItem.ItemNo;
            expense.Part_Number__c = productCodeMap.get(curItem.PartNo);
            expense.Part_No__c = curItem.PartNo;

            expense.Quantity__c = curItem.Quantity;
            expense.Price_EA__c = curItem.Price;
            expense.Unit__c = curItem.Unit;
            expense.Discount_Percent__c = curItem.DiscountPercent;
            expense.GD_Invoice_number__c = curItem.GDInvoiceNumber;
            expense.GD_Invoice_Item_num__c = curItem.GDInvoiceItemNumber;
        }

        if (toUpsert.size() > 0) {
            upsert toUpsert;
        }
    }

    public class FlowInput {
        @InvocableVariable
        public String warrantyClaimId;

        @InvocableVariable
        public String itemJSON;
    }
}