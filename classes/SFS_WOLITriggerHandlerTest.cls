/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 21/10/2021
* @description: Test class for SFS_WOLITrigger and SFS_WOLITriggerHandler
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number  				Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001    SIF-69     21/10/2021      Test Class for CheckStatus method in SFS_WOLITriggerHandler
Vidya Ratan			 M-002	  SIF-208	 5/11/2021       Test Class for ValidateWorkOrderLineItem method in SFS_WOLITriggerHandler
============================================================================================================*/
@IsTest
public class SFS_WOLITriggerHandlerTest {
//To cover CheckStatus method in SFS_WOLITriggerHandler
//To cover ValidateWorkOrderLineItem method in SFS_WOLITriggerHandler
        @testSetup static void createtestData(){
        RecordType contactRecordType = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp AIRD Contact' AND SObjectType = 'Contact'];  
        ID rtAcc = [Select Id, Name, SObjectType FROM RecordType where Name ='IR Comp Bill To Account' AND SObjectType = 'Account'].id;
        List<Account> accList=SFS_TestDataFactory.createAccounts(2, false);
        insert accList;

        Contact con = new Contact(Salutation = 'Ms.',LastName = 'test', FirstName='contact', Title = 'KS', Email = 'example@ir.com',RecordTypeId = contactRecordType.Id,
                                   Phone ='8890009', AccountId = accList[0].Id, CTS_TechDirect_Support_Region__c = 'Latin America',
                                    CTS_TechDirect_Support_Sub_Region__c = 'Latin America');
        
        insert con;
       
        Division__c div=new Division__c(Name='and',	EBS_System__c='MfgPRO',CurrencyIsoCode='USD');
        insert div;
            
        ServiceContract Serc = new ServiceContract(AccountId = accList[0].Id,SFS_Bill_To_Account__c=accList[0].id,
                                                  SFS_Type__c='Advanced Billing',SFS_Division__c=div.Id,
                                                  SFS_Status__c='Draft',SFS_Agreement_Value__c=23,CurrencyIsoCode='USD',
                                                  SFS_Consumables_Ship_To__c=accList[0].Id,SFS_Shipping_Instructions__c='the tagah',
                                                  SFS_Portable_Required__c='Yes',SFS_Invoice_Type__c='Prepaid',SFS_Invoice_Format__c='Detail',
                                                  Name='agree',StartDate=Date.newInstance(2021, 11, 5),EndDate=Date.newInstance(2021, 11, 30));
        insert Serc;
        
        WorkOrder wo = new WorkOrder(AccountId = accList[0].Id, 
                                    ContactId = con.Id,
                                    //SFS_Work_Order_Type__c = 'Installation',
                                    SFS_Work_Order_Type__c = 'Preventive Maintenance',
                                    SFS_Invoice_Submitted__c=true,
                                    SFS_Severity__c = '1 - Plant or Production Down',
                                    subject = 'test',
                                    Shipping_Account__c=accList[0].Id,
                                    SFS_Requested_Payment_Terms__c='NET 30',
                                    SFS_PO_Number__c='123',
                                    SFS_Estimated_Work_Order_Value__c=1000,
                                    ServiceContractId=Serc.Id);
        insert wo;
        
    }
        Static testmethod void testCheckStatus(){
        List<WorkOrder> woList = [Select Id from WorkOrder];
        List<Contact> conList = [Select Id from Contact];
        List<Account> accList = [Select Id from Account];
        Test.startTest();     
        List<WorkType> workTypeList = SFS_TestDataFactory.createWorkType(1,false);
        workTypeList[0].SFS_Work_Scope1__c='0';
        insert workTypeList;
        
        List<Asset> assetList = SFS_TestDataFactory.createAssets(1,false);
        assetList[0].AccountId=accList[0].Id;
        insert assetList;
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId=woList[0].Id,
                                                       SFS_Quote_Number__c='1236589',
                                                       WorkTypeId=workTypeList[0].Id,
                                                       assetId=assetList[0].Id);
        insert woli;
        woliList.add(woli);
        Map<Id,WorkOrderLineItem> newMap = new Map<id,WorkOrderLineItem>();
        newMap.put(woliList[0].Id, woliList[0]);
        ServiceAppointment sa=new ServiceAppointment(ContactId=conList[0].Id,
                                                     Status='None',
                                                     ParentRecordId=woliList[0].Id,
                                                     DueDate=system.now()+10,
                                                     EarliestStartTime=system.now(),
                                                     DurationType='Hours');
        insert sa;
        
        
        woliList[0].Status='Completed';
        try{
        update woliList;            
        }
        catch(Exception e){
           System.assert(e.getMessage().contains('All Service Appointment’s must be completed before Work Order Line Item Status is moved to completed or Closed.'));
        }
        Test.stopTest();
    }
    static testmethod void testCreateSkillReq(){
        
        List<WorkOrder> woList = [Select Id from WorkOrder];
        List<Contact> conList = [Select Id from Contact];
        List<Account> accList = [Select Id from Account];
        Test.startTest();  
        List<Skill> skillList = [Select Id from Skill limit 2];
        List<Schema.Location> locationList = SFS_TestDataFactory.createLocations(2, null, false);
        insert locationList;
        List<WorkType> workTypeList = SFS_TestDataFactory.createWorkType(1,false);
        workTypeList[0].SFS_Work_Scope1__c='0';
        insert workTypeList;
        
        List<Asset> assetList = SFS_TestDataFactory.createAssets(1,false);
        assetList[0].AccountId=accList[0].Id;
        insert assetList;
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId=woList[0].Id,
                                                       SFS_Quote_Number__c='1236589',
                                                       LocationId=locationList[0].Id,
                                                       Status = 'In Progress',
                                                       WorkTypeId=workTypeList[0].Id,
                                                       assetId=assetList[0].Id);
        insert woli;
        
        List<SkillRequirement> skillReqs = SFS_TestDataFactory.createSkillReq(1, skillList[0].Id, woli.Id, false);
        insert skillReqs;
        
        woliList.add(woli);
        
        Map<Id,WorkOrderLineItem> newMap = new Map<id,WorkOrderLineItem>();
        newMap.put(woliList[0].Id, woliList[0]);
        
        List<SFS_Location_Skill_Requirement__c> lsrList = SFS_TestDataFactory.createLocationSkillReq(4,skillList[0].Id, locationList[0].Id, false);
        lsrList[1].Skill_Id__c=skillList[1].Id;
        lsrList[2].Skill_Id__c=skillList[1].Id;
        lsrList[2].Location__c=locationList[1].Id;
        lsrList[3].Location__c=locationList[1].Id;
        
        insert lsrList;        
        
        SFS_WOLITriggerHandler.createSkillReq(woliList, newMap);
        
        woli.LocationId=locationList[1].Id;
        update woli;
        SFS_WOLITriggerHandler.deleteOldLocSkill(woliList, newMap);
        Test.stopTest();
     }
       static testmethod void createPartsRequiredTest(){
        List<WorkOrder> woList = [Select Id from WorkOrder];
        List<Contact> conList = [Select Id from Contact];
        List<Account> accList = [Select Id from Account];
        accList[0].ShippingStreet='street1';
        update accList[0];
        
        List<WorkType> workTypeList = SFS_TestDataFactory.createWorkType(1,false);
        workTypeList[0].SFS_Work_Scope1__c='0';
        insert workTypeList;
        
        List<Asset> assetList = SFS_TestDataFactory.createAssets(1,false);
        assetList[0].AccountId=accList[0].Id;
        insert assetList;
        
        List<Product2> productList = SFS_TestDataFactory.createProduct(2,false);
        productList[0].SFS_Superceded__c=false;
        productList[0].Status__c='Active';
        insert productList;
        
        List<CAP_IR_Asset_Part_Required__c> aprList= SFS_TestDataFactory.createPartsRequired(3,assetList[0].Id, productList[0].Id,false);
        aprList[0].SFS_1k__c=true;
        aprList[0].SFS_4k__c=true;
        aprList[0].SFS_8k__c=true;
        aprList[1].SFS_2k__c=true;
        aprList[1].SFS_0k__c=true;
        aprList[1].SFS_6k__c=true;
        aprList[1].Product__c=productList[1].Id;
        aprList[2].Product__c=productList[1].Id;
        aprList[2].SFS_1k__c=true;
        aprList[2].SFS_4k__c=true;
        aprList[2].SFS_8k__c=true;
        aprList[2].SFS_Delete_After_Use__c=true;
        /*aprList[3].SFS_2k__c=true;
        aprList[3].SFS_0k__c=true;
        aprList[3].SFS_6k__c=true;*/
        insert aprList[0];
        insert aprList[1];
        
         List<ServiceContract> agreementList= [Select Id from ServiceContract];
           
        List<MaintenancePlan> planList =SFS_TestDataFactory.createMaintenancePlan(1,agreementList[0].Id,false);
        insert planList; 
        
        List<MaintenanceAsset> maList = SFS_TestDataFactory.createMaintenanceAsset(1, planList[0].Id, assetList[0].Id, false);
        insert maList;
        
        List<MaintenanceWorkRule> mwrList = SFS_TestDataFactory.createMaintenanceWorkRule(1, maList[0].Id, workTypeList[0].Id, false);
        insert mwrList;
        
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId=woList[0].Id,
                                                       SFS_Quote_Number__c='1236589',
                                                       Status = 'new',
                                                       WorkTypeId=workTypeList[0].Id,
                                                       assetId=assetList[0].Id);
        
        insert woli;
        woliList.add(woli);
        Map<Id,WorkOrderLineItem> newMap = new Map<id,WorkOrderLineItem>();
        newMap.put(woliList[0].Id, woliList[0]);
        Test.startTest();
        List<ProductRequired> prodRequiredList = SFS_TestDataFactory.createProductRequired(1, woli.Id, productList[0].Id, false);
        insert prodRequiredList;
           
        List<ProductRequest> prtreq =new List<ProductRequest>();
        ProductRequest prodreq = new ProductRequest(WorkOrderId=woList[0].Id,
                                                    WorkOrderLineItemId=woliList[0].Id,
                                                    SFS_Consumables_Ship_To_Account__c=accList[0].Id,
                                                    SFS_Freight_Terms__c='Prepaid',
                                                    SFS_No_Partials__c=true,
                                                    SFS_Shipping_Instructions__c=woliList[0].Id,
                                                    SFS_Shipping_Method__c=woList[0].Id,
                                                    SFS_Division__c=woliList[0].Id,
                                                    Status='Open',
                                                    SFS_Priority__c='150');
        insert prtreq;
           
        ServiceAppointment sa=new ServiceAppointment(ContactId=conList[0].Id,
                                                     Status='None',
                                                     ParentRecordId=woliList[0].Id,
                                                     DueDate=system.now()+10,
                                                     EarliestStartTime=system.now(),
                                                     DurationType='Hours');
        insert sa;
        
        
        
        SFS_WOLITriggerHandler.createPartsRequired(woliList);
        insert aprList[2];
        SFS_WOLITriggerHandler.createPartsRequired(woliList);
        SFS_WOLITriggerHandler.createParts(woliList);
        delete woliList[0];
        Test.stopTest();
    }
}