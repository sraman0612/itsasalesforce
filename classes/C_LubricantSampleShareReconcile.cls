global class C_LubricantSampleShareReconcile implements Database.Batchable<sObject>{  
    global final String Query;
    
    global C_LubricantSampleShareReconcile(){
        
        Query='select id, Distributor__c, Account_Parent_ID__c  From Oil_Sample__c';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }   
    
    
    global void execute(Database.BatchableContext info, List<Oil_Sample__c> scope){
        List<Oil_Sample__Share> jobShrs  = new List<Oil_Sample__Share>();
        List<id> shID = new List<id>();
        List<id> ohID = new List<id>();
        List<user> usrList = new List<user>();
        Map<Id,Profile> profileIds = new Map<id,profile>([SELECT Id,UserLicenseId FROM Profile where UserLicenseId  in 
                                                          (SELECT Id FROM UserLicense where name ='Partner Community')]);

        system.debug(scope);
        if(scope.size()>0)
        {
            for(Oil_Sample__c os: scope)
            {
                system.debug('*****SHARE HERE INSERT***'+ os); 
                if(!string.isBlank(os.Distributor__c))
                {
                    for(User u : [Select id,Partner_Account_ID__c from user 
                                  where Partner_Account_ID__c = :string.valueOf(os.Distributor__c).substring(0,15)
                                 and isActive = true and profileId in:profileIds.Keyset()])
                    {
                        Oil_Sample__Share osShare = new Oil_Sample__Share();
                        
                        // Set the ID of record being shared
                        osShare.ParentId = os.Id;
                        
                        // Set the ID of user or group being granted access
                        osShare.UserOrGroupId = u.id;
                        
                        // Set the access level
                        osShare.AccessLevel = 'Read';
                        
                        // Set the Apex sharing reason for hiring manager and recruiter
                        osShare.RowCause = 'Manual';
                        
                        // Add objects to list for insert 
                        jobShrs.add(osShare);
                    }
                }
                
                //Create sharing for Global Account users
                if(!string.isBlank(os.Account_Parent_ID__c)){
                    for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' 
                                  and Partner_Account_Parent_ID__c  = :string.valueOf(os.Account_Parent_ID__c).substring(0,15) 
                                  and isActive = true and profileId in:profileIds.Keyset()])
                    {
                        Oil_Sample__Share osShare = new Oil_Sample__Share();
                        
                        // Set the ID of record being shared
                        osShare.ParentId = os.Id;
                        
                        // Set the ID of user or group being granted access
                        osShare.UserOrGroupId = u.id;
                        
                        // Set the access level
                        osShare.AccessLevel = 'Read';
                        
                        // Set the Apex sharing reason for hiring manager and recruiter
                        osShare.RowCause = 'Manual';
                        
                        // Add objects to list for insert
                        jobShrs.add(osShare);
                    }
                }
            }
            
            if(jobShrs.size()>0)
                system.debug(jobShrs);
            insert jobShrs;
        }
    }     
    global void finish(Database.BatchableContext info){     
    } 
}