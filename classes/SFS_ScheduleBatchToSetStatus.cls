/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 08/10/2021
* @description: This class is used to schedule the batch SFS_batchClassToSetInvoiceStatus at 10PM CDT everyday
* @Story Number: SIF-24

Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/

global class SFS_ScheduleBatchToSetStatus implements schedulable {
    
   global void execute(SchedulableContext sc) {
     SFS_batchClassToSetInvoiceStatus b = new SFS_batchClassToSetInvoiceStatus(); 
     database.executebatch(b,1);
   }
}