public with sharing class CaseDispatchPaperworkPDF {

    @AuraEnabled
    public static Case getCase(Id caseId){
        //make your SOQL here from your desired object where you want to place your lightning action
        return [SELECT Id, Status, Re_Opened__c FROM Case WHERE Id = :caseId];
    }

    @AuraEnabled
    public static Case updateCase(Case currCase){
        //you can make your update here.
        currCase.Status = 'Assigned';
        currCase.Re_Opened__c = false;
        upsert currCase;

        return currCase;
    }

}