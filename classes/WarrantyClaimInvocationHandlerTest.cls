@isTest
public class WarrantyClaimInvocationHandlerTest {
    
    @isTest
    public static void testme() {
        Account account = [SELECT Id, Name from Account where Name = 'PortalAccount' LIMIT 1];
        Account outsiderAccount = new Account(
            Name = 'OutsiderAccount'
        );
        Database.insert(outsiderAccount);
        
        Asset asset = new Asset(
            AccountId = outsiderAccount.Id,
            Name = 'test',
            End_User_Account__c = account.Id
        );
        Database.insert(asset);

        User salesRep = [SELECT Id, Name from User where Name = 'SalesRep User' LIMIT 1];
        
        System.runAs(salesRep) {
            WarrantyClaimInvocationHandler.updateSharing(null);
            runme(account.Id, asset.Id);
            runme(outsiderAccount.Id, asset.Id);
        }
    }
    
    private static void runme(String accountId, String serialNumberId) {
        WarrantyClaimInvocationHandler.FlowInput input = new WarrantyClaimInvocationHandler.FlowInput();
        input.accountId = accountId;
        input.serialNumberId = serialNumberId;
        
        WarrantyClaimInvocationHandler.updateSharing(new List<WarrantyClaimInvocationHandler.FlowInput> { input });
    }
    
    @testSetup
    private static void createData() {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = 'testerson' + System.now().millisecond() + '@test.com',
            Alias = 'batman',
            CompanyName = 'TEST',
            Title = 'title',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            community_user_type__c='Single Account'
        );
        Database.insert(portalAccountOwner1);
        
        System.runAs ( portalAccountOwner1 ) {
            Account repAccount = new Account(
                Name = 'RepAccount',
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(repAccount);
            
            Account parentAccount = new Account(
                Name = 'ParentAccount',
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(parentAccount);
            
            Account portalAccount = new Account(
                Name = 'PortalAccount',
                OwnerId = portalAccountOwner1.Id,
                Rep_Account2__c = repAccount.Id,
                ParentId = parentAccount.Id,
                RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'End_Customer_Location' AND sObjectType = 'Account'].Id
            );
            Database.insert(portalAccount);
            
            Contact contact1 = new Contact(
                FirstName = 'SalesRep',
                Lastname = 'User',
                AccountId = repAccount.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
            Database.insert(contact1);
            
            Profile portalProfile = [SELECT Id FROM Profile where name =: AccountSharingHelper.STR_SALES_REP_ACCOUNT_USER_PROFILE_NAME Limit 1];
            User user1 = new User(
                Username = System.now().millisecond() + 'test@test.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                FirstName = 'SalesRep',
                LastName = 'User',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
        }
    }
    
}