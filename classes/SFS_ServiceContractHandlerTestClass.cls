/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 25/09/2021
* @description: SFS_ServiceContractHandler TestClass.
Modification Log:
------------------------------------------------------------------------------------
Developer             Mod Number            Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001  SIF-34     25/09/2021   Test Class for SFS_ServiceContractHandler
Srikanth P              M-002  SIF-24     11/10/2021   Updated test data
Bhargavi Nerella        M-003  SIF-59     25/10/2021   Updated test data in callPolygonUtilsTest for code coverage of assignOwner method
============================================================================================================================================================*/
@isTest
public class SFS_ServiceContractHandlerTestClass {
    @isTest
    public static void callbatchClassToCreateInvoicesTest()
    {     
        Map<Id,ServiceContract> oldMap9 = new Map<id,ServiceContract>(); 
         Map<Id,ServiceContract> oldMap10 = new Map<id,ServiceContract>();
        List<ServiceContract> newList9 = new List<ServiceContract>();
        List<ServiceContract> newList8 = new List<ServiceContract>();
        List<ServiceContract> newList7 = new List<ServiceContract>();
         String rtCharge = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByName().get('Agreement Charge').getRecordTypeId();  
        List<Division__c> divisionList=new List<Division__c>();
        Division__c div1=new Division__c(Name='div1',CurrencyIsoCode='USD',Division_Type__c='Customer Center',EBS_System__c='Oracle 11i');
        Division__c div2=new Division__c(Name='division',CurrencyIsoCode='USD',Division_Type__c='Customer Center',EBS_System__c='Oracle 11i');
        divisionList.add(div1);
        divisionList.add(div2);
        insert divisionList;
        
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        String rtSC = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Advanced Billing').getRecordTypeId();        
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Account_Division__c =divisionList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0];        
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
        svcAgreementList[0].SFS_Invoice_Frequency__c='Monthly';
        svcAgreementList[0].Service_Charge_Frequency__c = 'Monthly';
        svcAgreementList[0].SFS_Status__c = 'Draft';
        svcAgreementList[0].SFS_Invoice_Format__c = 'Summary';
        svcAgreementList[0].SFS_PO_Number__c = '2';
        svcAgreementList[0].SFS_Invoice_Type__c = 'Prepaid';
        svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList[0].SFS_Invoice_Submission__c='Automatic';
        svcAgreementList[0].SFS_Invoice_Print_Code__c='Send to Customer';
        insert svcAgreementList[0]; 
        
        List<CAP_IR_Charge__c> charge = SFS_TestDataFactory.createCharge(1,null,false);
        Test.startTest();
        oldMap9.Put(svcAgreementList[0].id,svcAgreementList[0]);
        svcAgreementList[0].SFS_Status__c = 'Approved';
        Update svcAgreementList[0];
        newList9.add(svcAgreementList[0]);
        charge[0].CAP_IR_Service_Contract__c=svcAgreementList[0].id;
        charge[0].RecordTypeId=rtCharge;
        insert charge;
        SFS_ServiceContractHandler.callbatchClassToCreateInvoices(newList9,oldMap9);  
     
        svcAgreementList[0].RecordTypeId =rtSC;       
        svcAgreementList[0].SFS_Status__c = 'Draft';
        Update svcAgreementList[0];
        newList8.add(svcAgreementList[0]);
        SFS_ServiceContractHandler.updateChargeType(oldMap9,newList8);
        Test.stopTest();
     
    }
 
    
    @isTest
    public static void assignOwnerTest()
    {
        List<ServiceContract> newList7 = new List<ServiceContract>();
        List<ServiceContract> newList3 = new List<ServiceContract>();
        List<Division__c> divisionList=new List<Division__c>();
        Division__c div1=new Division__c(Name='div1',CurrencyIsoCode='USD',Division_Type__c='Customer Center',EBS_System__c='Oracle 11i');
        Division__c div2=new Division__c(Name='division',CurrencyIsoCode='USD',Division_Type__c='Customer Center',EBS_System__c='Oracle 11i');
        divisionList.add(div1);
        divisionList.add(div2);
        insert divisionList;
        
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
       
        List<Account> accountList=SFS_TestDataFactory.createAccounts(3, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Account_Division__c =divisionList[0].Id;
        accountList[1].Type='Prospect';
        accountList[2].IRIT_Customer_Number__c='123';
        accountList[2].Bill_To_Account__c = accountList[0].Id;
        accountList[2].Account_Division__c =divisionList[1].Id;
        accountList[2].Type='Prospect';
        insert accountList;   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0]; 
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        List<User> u1 = [SELECT Id,ProfileId,IsActive,UserName FROM User WHERE ProfileId =: p1.Id AND IsActive = True ];
        
         Group queueId = [SELECT Id, DeveloperName FROM Group WHERE Type = 'Queue' and DeveloperName = 'SFS_CARE_Team_Service_Agreements'];
        List<SFS_Mapping_Rule__c> ruleList=new List<SFS_Mapping_Rule__c>();
        SFS_Mapping_Rule__c rule1=new SFS_Mapping_Rule__c(SFS_Division__c=divisionList[0].Id,SFS_Ownership_Type__c='User',
                                                         SFS_Ownership_Value__c=u1[0].UserName);
        insert rule1;
        SFS_Mapping_Rule__c rule2=new SFS_Mapping_Rule__c(SFS_Division__c=divisionList[1].Id,SFS_Ownership_Type__c='Queue',
                                                         SFS_Ownership_Value__c=queueId.DeveloperName);
        
        insert rule2;
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
        svcAgreementList[0].SFS_Invoice_Frequency__c='Monthly';
        svcAgreementList[0].Service_Charge_Frequency__c = 'Monthly';
        svcAgreementList[0].SFS_Status__c = 'Draft';
        svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
        svcAgreementList[0].SFS_PO_Number__c = '2';
        svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
        svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList[0].SFS_Invoice_Submission__c='Automatic';
        svcAgreementList[0].SFS_Invoice_Print_Code__c='Send to Customer';
        insert svcAgreementList[0]; 
        
        List<ServiceContract> svcAgreementList1=SFS_TestDataFactory.createServiceAgreement(1,accountList[2].Id,false);
        svcAgreementList1[0].SFS_Invoice_Frequency__c='Monthly';
        svcAgreementList1[0].Service_Charge_Frequency__c = 'Monthly';
        svcAgreementList1[0].SFS_Status__c = 'Draft';
        svcAgreementList1[0].SFS_Invoice_Format__c = 'Detail';
        svcAgreementList1[0].SFS_PO_Number__c = '2';
        svcAgreementList1[0].SFS_Invoice_Type__c = 'Receivable';
        svcAgreementList1[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList1[0].SFS_Invoice_Submission__c='Automatic';
        svcAgreementList1[0].SFS_Invoice_Print_Code__c='Send to Customer';
        insert svcAgreementList1[0]; 
        
        newList7.add(svcAgreementList[0]);
        Test.startTest();
        SFS_ServiceContractHandler.assignOwner(newList7);     
        newList3.add(svcAgreementList1[0]);
        SFS_ServiceContractHandler.assignOwner(newList3);
        Test.stopTest(); 
    }

@isTest
    public static void callPolygonUtilsTest()
    {
        Map<Id,ServiceContract> oldMap0 = new Map<id,ServiceContract>();  
        Map<Id,ServiceContract> newMap0 = new Map<id,ServiceContract>();  
        List<ServiceContract> newList0 = new List<ServiceContract>();
        List<ServiceContract> newList1 = new List<ServiceContract>();
       List<ServiceContract> newList6 = new List<ServiceContract>();
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0];        
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
        svcAgreementList[0].SFS_Invoice_Frequency__c='Monthly';
        svcAgreementList[0].Service_Charge_Frequency__c = 'Monthly';
        svcAgreementList[0].SFS_Status__c = 'Draft';
        svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
        svcAgreementList[0].SFS_PO_Number__c = '2';
        svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
        svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList[0].SFS_Invoice_Submission__c='Automatic';
        svcAgreementList[0].SFS_Invoice_Print_Code__c='Send to Customer';
        insert svcAgreementList[0]; 
        oldMap0.Put(svcAgreementList[0].id,svcAgreementList[0]);
        
        List<ServiceContract> svcAgreementList1=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
        svcAgreementList1[0].SFS_Invoice_Frequency__c='Monthly';
        svcAgreementList1[0].Service_Charge_Frequency__c = 'Monthly';
        svcAgreementList1[0].SFS_Status__c = 'Pending';
        svcAgreementList1[0].SFS_Invoice_Format__c = 'Detail';
        svcAgreementList1[0].SFS_PO_Number__c = '2';
        svcAgreementList1[0].SFS_Invoice_Type__c = 'Receivable';
        svcAgreementList1[0].SFS_PO_Expiration__c=system.today().addDays(60);
        svcAgreementList1[0].SFS_Invoice_Submission__c='Automatic';
        svcAgreementList1[0].SFS_Invoice_Print_Code__c='Send to Customer';
        insert svcAgreementList1[0]; 
        newMap0.Put(svcAgreementList1[0].id,svcAgreementList1[0]);
        svcAgreementList1[0].SFS_Status__c = 'APPROVED';
        
        List<ContractLineItem> AgrLineItemList=SFS_TestDataFactory.createServiceAgreementLineItem(1,svcAgreementList1[0].Id,false);
        insert AgrLineItemList;
        List<Entitlement> EntList=SFS_TestDataFactory.createEntitlement(1,accountList[1].Id,assetList[0].Id,svcAgreementList1[0].Id,AgrLineItemList[0].Id,false);
        insert EntList;
            
        Test.startTest();     
        newList0.add(svcAgreementList[0]);
        newList1.add(svcAgreementList1[0]);
        SFS_ServiceContractHandler.callPolygonUtils(oldMap0,newList0);
        SFS_ServiceContractHandler.checkProductCode(newList1,newMap0);
        Test.stopTest();
        
    }
    

}