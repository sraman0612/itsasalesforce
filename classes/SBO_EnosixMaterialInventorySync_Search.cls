/// enosiX Inc. Generated Apex Model
/// Generated On: 6/27/2019 9:41:45 AM
/// SAP Host: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// CID: From REST Service On: https://gdi--DevDave.my.salesforce.com
/// Generator Version: 2.5.0.0, Target Framework ensxapp.v1.2

public with sharing class SBO_EnosixMaterialInventorySync_Search extends ensxsdk.EnosixFramework.SearchSBO 
{
        static void registerReflectionInfo()
    {
        ensxsdk.EnosixFramework.registerReflectionResource('SBO_EnosixMaterialInventorySync_Search_Meta', new Type[] {
            SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC.class
            , SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR.class
            , SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT.class
            , SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS.class
            , SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT.class
            } 
        );
    }

    public SBO_EnosixMaterialInventorySync_Search() 
    {
        super('EnosixMaterialInventorySync', SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC.class, SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR.class);
    }
    
    public override Type getType() { return SBO_EnosixMaterialInventorySync_Search.class; }

    public EnosixMaterialInventorySync_SC search(EnosixMaterialInventorySync_SC sc) 
    {
        return (EnosixMaterialInventorySync_SC)super.executeSearch(sc);
    }

    public EnosixMaterialInventorySync_SC initialize(EnosixMaterialInventorySync_SC sc) 
    {
        return (EnosixMaterialInventorySync_SC)super.executeInitialize(sc);
    }

    public class EnosixMaterialInventorySync_SC extends ensxsdk.EnosixFramework.SearchContext 
    { 		
        public EnosixMaterialInventorySync_SC() 
        {		
            super(new Map<string,type>		
                {		
                    'SEARCHPARAMS' => SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS.class		
                });		
        }

        public override Type getType() { return SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SC.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixMaterialInventorySync_Search.registerReflectionInfo();
        }

        public EnosixMaterialInventorySync_SR result { get { return (EnosixMaterialInventorySync_SR)baseResult; } }


        @AuraEnabled public SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS SEARCHPARAMS 
        {
            get
            {
                return (SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS)this.getStruct(SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS.class);
            }
        }
        
        }

    public class EnosixMaterialInventorySync_SR extends ensxsdk.EnosixFramework.SearchResult 
    {
        public EnosixMaterialInventorySync_SR() 
        {
            super(new Map<string,type>{'SEARCHRESULT' => SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT.class } );
        }
        
        public ensxsdk.EnosixFramework.FrameworkCollection SearchResults
        {
            get { return super.getCollection(SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT.class); }
        }
        
        public List<SEARCHRESULT> getResults() 
        {
            List<SEARCHRESULT> results = new List<SEARCHRESULT>();
            SearchResults.copyTo(results);
            return results;
        }

        public override Type getType() { return SBO_EnosixMaterialInventorySync_Search.EnosixMaterialInventorySync_SR.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixMaterialInventorySync_Search.registerReflectionInfo();
        }
    }

    public class SEARCHPARAMS extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixMaterialInventorySync_Search.SEARCHPARAMS.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixMaterialInventorySync_Search.registerReflectionInfo();
        }
        @AuraEnabled public String SalesOrganization
        { 
            get { return this.getString ('VKORG'); } 
            set { this.Set (value, 'VKORG'); }
        }

        @AuraEnabled public String DistributionChannel
        { 
            get { return this.getString ('VTWEG'); } 
            set { this.Set (value, 'VTWEG'); }
        }

        @AuraEnabled public String CustomerNumber
        { 
            get { return this.getString ('KUNNR'); } 
            set { this.Set (value, 'KUNNR'); }
        }

    }

    public class SEARCHRESULT extends ensxsdk.EnosixFramework.ValueObject 
    {
        public override Type getType() { return SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT.class; }

        public override void registerReflectionForClass()
        {
            SBO_EnosixMaterialInventorySync_Search.registerReflectionInfo();
        }
        @AuraEnabled public Integer Quantity {
            get { 
                if (this.Quantity == null) {
                    Quantity = 1;
                }

                return Quantity;
             }
             set;
        }
        @AuraEnabled public String Material
        { 
            get { return this.getString ('MATNR'); } 
            set { this.Set (value, 'MATNR'); }
        }

        @AuraEnabled public String MaterialDescription
        { 
            get { return this.getString ('MAKTX'); } 
            set { this.Set (value, 'MAKTX'); }
        }

        @AuraEnabled public String MaterialFamily
        { 
            get { return this.getString ('STDPD'); } 
            set { this.Set (value, 'STDPD'); }
        }

        @AuraEnabled public Decimal Stock
        { 
            get { return this.getDecimal ('LBKUM'); } 
            set { this.Set (value, 'LBKUM'); }
        }

        @AuraEnabled public Decimal Scheduled
        { 
            get { return this.getDecimal ('MNG01'); } 
            set { this.Set (value, 'MNG01'); }
        }

        @AuraEnabled public String UOM
        { 
            get { return this.getString ('MEINS'); } 
            set { this.Set (value, 'MEINS'); }
        }

        @AuraEnabled public String SalesOrganization
        { 
            get { return this.getString ('VKORG'); } 
            set { this.Set (value, 'VKORG'); }
        }

        @AuraEnabled public String DistributionChannel
        { 
            get { return this.getString ('VTWEG'); } 
            set { this.Set (value, 'VTWEG'); }
        }

    }

    public class SEARCHRESULT_COLLECTION extends ensxsdk.EnosixFramework.FrameworkCollection
    {
        public SEARCHRESULT_COLLECTION()
        {
            super('SEARCHRESULT', SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT.class, null);
        }

        public List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT> getAsList()
        {
            return (List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT>)this.buildList(List<SBO_EnosixMaterialInventorySync_Search.SEARCHRESULT>.class);
        }
    }


}