global class C_Batch_MVPL_Sched implements Schedulable {
    
    global void execute(SchedulableContext sc) {
		Database.executeBatch(new C_Batch_Machine_Version_Parts_Assign(), 10);
    }
    
}