@IsTest
public class TSTU_SFContact
{
    @isTest
    static void test_getContactFromId()
    {
        Test.startTest();
        Contact contact = createTestContact(null);
        insert contact;
        Contact fetched = UTIL_SFContact.getContactFromId(contact.Id);
        System.assertEquals(contact.LastName, fetched.LastName);
        System.assertEquals(contact.Id, fetched.Id);
        Account acct = TSTU_SFAccount.createTestAccount();
        insert acct;
        UTIL_SFContact.getContactFromId(acct.Id);
        Test.stopTest();
    }

    @isTest
    static void test_getContactNumberFromContact()
    {
        Test.startTest();
        Contact contact = createTestContact(null);
        insert contact;
        String testContactNumber = 'TestContact';
        UTIL_SFContact.setContactContactNumber(contact, testContactNumber);
        System.assertEquals(UTIL_SFContact.getContactNumberFromContact(contact), testContactNumber);
        String ContactCustomerFieldName = UTIL_SFContact.ContactCustomerFieldName;
        Test.stopTest();
    }

    public static Contact createTestContact(Account acct)
    {
        Contact contact = new Contact();
        contact.LastName = 'TestContact';
        if (acct != null) contact.AccountId = acct.id;
        return contact;
    }
}