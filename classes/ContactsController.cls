public with sharing class ContactsController 
{
    private ApexPages.StandardSetController standardController;

    public ContactsController(ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
    }

    public PageReference bumpContacts()
    {       
        // Get the selected records
        List<Contact> selectedContacts = (List<Contact>) standardController.getSelected();

        // Update records       
        for(Contact selectedContact : selectedContacts)
        {
            if(selectedContact.LastName == null) selectedContact.LastName = 'Test';
        }       

        return null;        
    }

    public PageReference updateContacts()
    {       
        // Call StandardSetController 'save' method to update
        return standardController.save();   
    }
}