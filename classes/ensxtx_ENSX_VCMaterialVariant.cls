public with sharing class ensxtx_ENSX_VCMaterialVariant
{
    @AuraEnabled
    public String VariantDescription{get;set;}
    @AuraEnabled
    public String VariantId{get;set;}
    @AuraEnabled
    public List<ensxtx_ENSX_VCCharacteristicValues> CharacteristicValues {get;set;}

}