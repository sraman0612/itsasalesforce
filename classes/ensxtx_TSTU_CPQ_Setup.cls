@IsTest
public with sharing class ensxtx_TSTU_CPQ_Setup
{
    @isTest
    static void test_getFromAppSettings()
    {
        ensxtx_UTIL_CPQ_Setup.getFromAppSettings();
        ensxtx_UTIL_CPQ_Setup.installCustomScript();
        ensxtx_UTIL_CPQ_Setup.getFromAppSettings().installThisCustomScript();
    }
    @IsTest
    public static void test_installCustomScript()
    {
        Test.startTest();
        ensxtx_UTIL_CPQ_Setup util = new ensxtx_UTIL_CPQ_Setup();
        SBQQ__CustomScript__c cs = new SBQQ__CustomScript__c();
        cs.Name = util.customScriptName;
        ensxtx_TSTU_SFTestObject.upsertWithRetry(cs);       
        //util.installThisCustomScript();
        ensxtx_UTIL_CPQ_Setup.installCustomScript();
        Test.stopTest();
        System.assert(null != [SELECT Id FROM SBQQ__CustomScript__c WHERE Name = :(util.customScriptName)].Id);
    }

    

}