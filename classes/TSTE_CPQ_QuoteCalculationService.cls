@isTest
public class TSTE_CPQ_QuoteCalculationService
{
    public class MOC_RFC_SD_GET_DOC_TYPE_VALUES implements ensxsdk.EnosixFramework.RFCMock
    {
        private boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        public ensxsdk.EnosixFramework.FunctionObject executeFunction()
        {
            RFC_SD_GET_DOC_TYPE_VALUES.RESULT result = new RFC_SD_GET_DOC_TYPE_VALUES.RESULT();
            RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT sditm = new RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT();
            
            sditm.DocumentType = 'YSOR';
            sditm.BEZEI = 'Standard';
            sditm.INCPO = '10';
            result.getCollection(RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT.class).add(sditm);
            
            for (integer mocCnt = 0; mocCnt < 20; mocCnt++)
            {
                RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT out = new RFC_SD_GET_DOC_TYPE_VALUES.ET_OUTPUT();
                out.DocumentType = 'tst' + mocCnt;
                out.BEZEI = 'tst' + mocCnt;
                result.ET_OUTPUT_List.add(out);
            }
            
            result.setSuccess(this.success);
            return result;
        }
    }

    public class MockSBO_EnosixOpportunityPricing_Detail implements
        ensxsdk.EnosixFramework.DetailSBOInitMock,
        ensxsdk.EnosixFramework.DetailSBOCommandMock,
        ensxsdk.EnosixFramework.DetailSBOGetMock,
        ensxsdk.EnosixFramework.DetailSBOSaveMock
    {
        private boolean success = true;

        public void setSuccess(boolean successful)
        {
            this.success = successful;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.DetailObject executeInitialize(ensxsdk.EnosixFramework.DetailObject initialState) 
        { 
            return null;
        }
        
        public ensxsdk.EnosixFramework.DetailObject executeCommand(String command, ensxsdk.EnosixFramework.DetailObject obj) 
        { 
            if (throwException)
            {
                throw new ENSX_CPQ_Exceptions.SimulationException();
            }
            return this.executeGetDetail(obj);
        }

        public ensxsdk.EnosixFramework.DetailObject executeGetDetail(object key) 
        { 
            SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing result = new SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing();           
            result.setSuccess(success);
            
            SBO_EnosixOpportunityPricing_Detail.ITEMS topItem = new SBO_EnosixOpportunityPricing_Detail.ITEMS();
            topItem.ItemNumber = '10';
            topItem.HigherLevelItemNumber = '0';
            topItem.CostInDocCurrency = 5;
            topItem.OrderQuantity = 1;
            topItem.NetItemPrice = 10;
            
            SBO_EnosixOpportunityPricing_Detail.ITEMS childItem = new SBO_EnosixOpportunityPricing_Detail.ITEMS();
            childItem.ItemNumber = '11';
            childItem.Material = 'MAT1';
            childItem.HigherLevelItemNumber = '10';
            childItem.CostInDocCurrency = 6;
            childItem.OrderQuantity = 1;
            childItem.NetItemPrice = 10;

            SBO_EnosixOpportunityPricing_Detail.ITEMS childItem2 = new SBO_EnosixOpportunityPricing_Detail.ITEMS();
            childItem2.ItemNumber = '12';
            childItem2.Material = 'MAT2';
            childItem2.HigherLevelItemNumber = '10';
            childItem2.CostInDocCurrency = 5;
            childItem2.OrderQuantity = 2;
            childItem2.NetItemPrice = 10;

            SBO_EnosixOpportunityPricing_Detail.ITEMS childItem3 = new SBO_EnosixOpportunityPricing_Detail.ITEMS();
            childItem3.ItemNumber = '13';
            childItem3.Material = 'MAT2';
            childItem3.HigherLevelItemNumber = '10';
            childItem3.CostInDocCurrency = 5;
            childItem3.OrderQuantity = 2;
            childItem3.NetItemPrice = 10;

            SBO_EnosixOpportunityPricing_Detail.ITEMS childItem4 = new SBO_EnosixOpportunityPricing_Detail.ITEMS();
            childItem4.ItemNumber = '14';
            childItem4.Material = 'MAT2';
            childItem4.HigherLevelItemNumber = '10';
            childItem4.CostInDocCurrency = 5;
            childItem4.OrderQuantity = 2;
            childItem4.NetItemPrice = 10;
            
            result.ITEMS.add(topItem);
            result.ITEMS.add(childItem);
            result.ITEMS.add(childItem2);
            result.ITEMS.add(childItem3);
            result.ITEMS.add(childItem4);
            
            SBO_EnosixOpportunityPricing_Detail.ITEMS_SCHEDULE itemSched = new SBO_EnosixOpportunityPricing_Detail.ITEMS_SCHEDULE();
        	itemSched.ItemNumber = '10';
        	itemSched.ConfirmedQuantity = 1;
        	result.ITEMS_SCHEDULE.add(itemSched);

            SBO_EnosixOpportunityPricing_Detail.CONDITIONS itemCond = new SBO_EnosixOpportunityPricing_Detail.CONDITIONS();
            itemCond.ConditionItemNumber = '1';
            itemCond.ConditionType = 'TEST';
            itemCond.Rate = 2;
            result.CONDITIONS.add(itemCond);
            SBO_EnosixOpportunityPricing_Detail.CONDITIONS itemCond2 = new SBO_EnosixOpportunityPricing_Detail.CONDITIONS();
            itemCond2.ConditionItemNumber = '1';
            itemCond2.ConditionType = 'SECOND';
            itemCond2.Rate = 10;
            result.CONDITIONS.add(itemCond2);
            
            return result;
        }

        public ensxsdk.EnosixFramework.DetailObject executeSave(ensxsdk.EnosixFramework.DetailObject obj) { 
            SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing result = new SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing();
            result.setSuccess(success);
            return result;
        }
    }

    @isTest
    static public void test_doPost()
    {
        UTIL_AppSettings.resourceJson = 
            '{"CPQSimulation.ListPriceConditionType":"TEST",' + 
            '"CPQSimulation.UnitCostConditionType":"TEST",' + 
            '"CPQSimulation.SecondaryUnitCostConditionType":"SECOND"}';

        MockSBO_EnosixOpportunityPricing_Detail sbo = new MockSBO_EnosixOpportunityPricing_Detail();
        ensxsdk.EnosixFramework.setMock(SBO_EnosixOpportunityPricing_Detail.class, sbo);
        
        MOC_RFC_SD_GET_DOC_TYPE_VALUES mocRfc = new MOC_RFC_SD_GET_DOC_TYPE_VALUES();
        ensxsdk.EnosixFramework.setMock(RFC_SD_GET_DOC_TYPE_VALUES.class, mocRfc);

        ENSX_Quote qte = new ENSX_Quote();

        Product2 prod = TSTU_CPQ_TestSetup.createProduct2();

        // initialize sObjects
        List<SObject> acctAndQuote = TSTU_CPQ_TestSetup.createAccountQuoteLinked();
        Account acct = (Account)acctAndQuote[0];
        SBQQ__Quote__c quote = (SBQQ__Quote__c)acctAndQuote[1];
        ENSX_Quote quoteHeaderConfig = new ENSX_Quote();
        quote.SAP_Configuration__c = JSON.serialize(quoteHeaderConfig);
        update quote;
        
        ENSX_Characteristic chars = new ENSX_Characteristic();
        chars.CharacteristicID = 'TEST';
        chars.CharacteristicName = 'TEST';
        chars.CharacteristicValue = 'TEST';

        List<ENSX_Characteristic> lstChars = new List<ENSX_Characteristic>();
        lstChars.add(chars);

        ENSX_ItemConfiguration conf = new ENSX_ItemConfiguration();
        conf.plant = 'PLANT';
        conf.materialNumber = 'TEST';
        conf.selectedCharacteristics = lstChars;
        conf.SalesDocumentCurrency = 'TEST';
        conf.OrderQuantity = 0;
        
        ENSX_QuoteLine qLine = new ENSX_QuoteLine();
        qLine.Product = prod.Id;
        qLine.LineItem = 1;
        qLine.SAPMaterialNumber = 'TEST';
        qLine.Quantity = 1;
        qLine.ListPrice = 5;
        qLine.NetPrice = 5;
        qLine.Plant = 'Plant';
        qLine.NetWeight = 11;
        qLine.Description = 'TEST';
        qLine.NetCost = 2;
        qLine.UnitCost = 2;
        qLine.CostPrice = 2;
        qLine.DiscountPercent = 10;
        qLine.ATPDate = date.Today();
        qLine.ItemJSON = JSON.serialize(conf);
        qLine.IsProductFeature = false;
        qLine.ItemConfiguration = conf;
        qLine.RealProductId = '';

        ENSX_QuoteLine qLine2 = new ENSX_QuoteLine();
        qLine2.Product = prod.Id;
        qLine2.ParentLineItem = 1;
        qLine2.LineItem = 2;
        qLine2.SAPMaterialNumber = 'TEST';
        qLine2.Quantity = 1;
        qLine2.ListPrice = 5;
        qLine2.NetPrice = 5;
        qLine2.Plant = 'Plant';
        qLine2.NetWeight = 11;
        qLine2.Description = 'TEST';
        qLine2.NetCost = 2;
        qLine2.UnitCost = 2;
        qLine2.CostPrice = 2;
        qLine2.DiscountPercent = 10;
        qLine2.ATPDate = date.Today();
        qLine2.ItemJSON = JSON.serialize(conf);
        qLine2.IsProductFeature = true;
        qLine2.ItemConfiguration = conf;
        qLine2.RealProductId = 'Real';        

        List<ENSX_QuoteLine> lstQuoteLine = new List<ENSX_QuoteLine>();
        lstQuoteLine.add(qLine);
        lstQuoteLine.add(qLine2);

        qte.LinkedQuoteLines = lstQuoteLine;
        qte.instanceUrl = 'example.com';
        qte.QuoteId = quote.Id;
        qte.salesOrg = 'TEST';
        qte.distChannel = 'TEST';
        qte.division = 'TEST';
        qte.soldToParty = 'TEST';
        qte.shipToParty = 'TEST';
        qte.salesDocType = 'TEST';

		// Test affirmative case:
        Test.startTest();
        String response = ENSX_CPQ_QuoteCalculationService.doPost(JSON.serialize(qte));
        
        sbo.setSuccess(false);
        response = ENSX_CPQ_QuoteCalculationService.doPost(JSON.serialize(qte));

        sbo.setSuccess(true);
        sbo.setThrowException(true);
        response = ENSX_CPQ_QuoteCalculationService.doPost(JSON.serialize(qte));

        sbo.setThrowException(false);
        ENSX_QuoteLineMapping qlm = ENSX_CPQ_QuoteCalculationService.preCalculateState.get(1);
        qlm.SAPLineItem = 30;
        SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing sapQuote = (SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing) sbo.executeGetDetail(null);
        ENSX_CPQ_QuoteCalculationService.updateENSX_QuoteForSBOResults(qte, sapQuote);

        ENSX_CPQ_QuoteCalculationService.getLastPriceConditionRatePerUnitPerLinePerType(null, null);
        ENSX_CPQ_QuoteCalculationService.getLastPriceConditionRatePerUnitPerLinePerType(new Set<String>(), new SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing());
        ENSX_QuoteLine ql = new ENSX_QuoteLine();
        ql.ItemNumber = '10';
        ql.LineItem = 1;
        ql.Quantity = 1;
        SBO_EnosixOpportunityPricing_Detail.ITEMS itm = new SBO_EnosixOpportunityPricing_Detail.ITEMS();
        itm.ItemNumber = '10';
        itm.NetItemPrice = 1;
        itm.CostInDocCurrency = 1;
        itm.OrderQuantity = 1;
        itm.Material = 'Material';
        ENSX_CPQ_QuoteCalculationService.materialNumberId.put('Material', new ENSX_QuoteLine());
        ENSX_CPQ_QuoteCalculationService.updateQuoteLineFromSAP(ql, new Map<Integer,Date>(), itm, new Map<String,Decimal>(), new Map<String,Decimal>(), new Map<String,Decimal>(), new Map<String,Decimal>(), true);

		Test.stopTest();
    }

    @isTest
    static public void test_concatSboMessages()
    {
        Test.startTest();
        ENSX_CPQ_QuoteCalculationService.concatSboMessages(new List<ensxsdk.EnosixFramework.Message>{
            new ensxsdk.EnosixFramework.Message(ensxsdk.EnosixFramework.MessageType.ERROR,'TEST1'),
            new ensxsdk.EnosixFramework.Message(ensxsdk.EnosixFramework.MessageType.ERROR,'TEST2')
        });
        ENSX_CPQ_QuoteCalculationService.concatSboMessages(null);
        Test.stopTest();
    }
}