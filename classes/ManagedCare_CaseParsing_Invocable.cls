public class ManagedCare_CaseParsing_Invocable {
    
    @InvocableMethod
    public static List<Case> parseCaseData (List<Case> incomingCases) {
        
        
        // Store Number [Store Id || Location Name]
        // 		Store_Number__c
        // Priority [Priority]
        // 		Priority__c
        // PO# [PO#]
        // 		Customer_WO__c
        // NTE [NTE (USD )]
        // 		DNE_Value__c
        // Store Contact Phone [Phone]
        // 		Store_Contact_Phone__c
        // Store Contact Name [CALLER:]
        // 		Store_Contact_Name__c
        
        for(Case c : incomingCases){
            integer counter = 0;
            List<String> splitDesc = c.Description.split('\n');
            List<String> splitDescNoBlank = new List<String>();
            
            for(String s : splitDesc){
                if(String.isNotBlank(s)){
                    splitDescNoBlank.add(s);
                }

            }
            
            
            for(String s : splitDescNoBlank){
                
                if(s.contains('from CALLER:')){
                    integer li = s.lastIndexOf('from CALLER:');
                    c.Store_Contact_Name__c = s.right(s.length() - li - 12);
                }
                
                String sTemp = s.deleteWhitespace();
                
                System.debug(sTemp);     
                
                if(sTemp.equalsIgnoreCase('StoreId') || sTemp.equalsIgnoreCase('LocationId')){
                    c.Store_Number__c = splitDescNoBlank[counter + 1];
                }
                
                if(sTemp.equalsIgnoreCase('Priority')){
                    c.Priority__c = splitDescNoBlank[counter + 1];
                }
                
                if(sTemp.equalsIgnoreCase('NTE(USD)')){
                    c.DNE_Value__c = splitDescNoBlank[counter + 1];
                }
                
                if(sTemp.equalsIgnoreCase('PO#')){
                    c.Customer_WO__c = splitDescNoBlank[counter + 1];
                }
                
                if(sTemp.equalsIgnoreCase('Phone')){
                    c.Store_Contact_Phone__c = splitDescNoBlank[counter + 1];
                }
                
                
                counter++;
            }
            
            System.debug(c.Store_Number__c);
            System.debug(c.Priority__c);
            System.debug(c.DNE_Value__c);
            System.debug(c.Customer_WO__c);
            System.debug(c.Store_Contact_Phone__c);
            System.debug(c.Store_Contact_Name__c);
            
        }
        
        return incomingCases; 
    }
    
}