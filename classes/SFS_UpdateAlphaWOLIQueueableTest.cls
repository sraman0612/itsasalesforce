@isTest
public class SFS_UpdateAlphaWOLIQueueableTest { 
	static testmethod void updateWorkRulesTest(){
         String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Division__c> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[0].IRIT_Customer_Number__c='1333';
        accList[1].IRIT_Customer_Number__c='1533';
        accList[1].IRIT_Payment_Terms__c='NET 30';
        accList[1].RecordTypeId=accRecID;
        accList[1].Account_Division__c=divisionsList[0].Id;
        insert accList[1];
        accList[0].Type='Prospect';
        accList[0].Bill_To_Account__c=accList[1].Id; 
        accList[0].Account_Division__c=divisionsList[0].Id;
        accList[0].IRIT_Payment_Terms__c='NET 30';
        insert accList[0];
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        svcAgreementList[0].SFS_Status__c='APPROVED';
        svcAgreementList[0].SFS_PO_Number__c='1222';
        svcAgreementList[0].AccountId=accList[0].Id;
        svcAgreementList[0].SFS_Bill_To_Account__c=accList[1].Id;
        svcAgreementList[0].SFS_Consumables_Ship_To__c=accList[0].Id;
        svcAgreementList[0].SFS_Division__c=divisionsList[0].Id;
        insert svcAgreementList;
        List<MaintenancePlan> mpList=SFS_TestDataFactory.createMaintenancePlan(1,svcAgreementList[0].Id,false);
        insert mpList;
        List<Product2> productsList=SFS_TestDataFactory.createProduct(2,false);
        insert productsList;
      
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,null,divisionsList[0].Id,svcAgreementList[0].Id,false);
        woList[0].MaintenancePlanId=mpList[0].Id;
        woList[0].SuggestedMaintenanceDate=system.Today().addYears(1);
        woList[0].SFS_Bill_To_Account__c=accList[1].id;
        woList[0].SFS_Requested_Payment_Terms__c='BANKCARD';
        woList[0].Shipping_Account__c=accList[0].Id;
        woList[0].SFS_Division__c=divisionsList[0].Id;
        woList[0].Status='Open';
        insert woList;
        
        //update woList;
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(2,woList[0].Id,workTypeList[0].Id,false);
        insert woliList;
        List<ProductRequest> requestList= SFS_TestDataFactory.createProductRequest(1,woList[0].Id,woliList[1].Id,false);
        requestList[0].SFS_Integration_Status__c ='APPROVED';
        insert requestList;
        List<ProductRequestLineItem> prliList= SFS_TestDataFactory.createPRLI(1,requestList[0].Id,productsList[0].Id,false);
        prliList[0].WorkOrderLineItemId=woliList[1].Id;
        insert prliList;
        /*List<ProductRequest> requestList1= SFS_TestDataFactory.createProductRequest(1,woList[0].Id,woliList[0].Id,false);
        insert requestList1;
        List<ProductRequestLineItem> prliList1= SFS_TestDataFactory.createPRLI(1,requestList1[0].Id,productsList[0].Id,false);
        insert prliList1;*/
        //List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, divisionsList[0].Id, true);       
        Skill skillItem = [Select Id From Skill Limit 1];
        List<SkillRequirement> skillReqList=SFS_TestDataFactory.createSkillReq(1,skillItem.Id,woliList[0].Id,false);
        insert skillReqList;
        List<ServiceAppointment> saList=SFS_TestDataFactory.createServiceAppointment(1,woliList[0].Id,false);
        insert saList[0];
       
        Test.startTest();
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1,false);
        insert assetList;
        List<MaintenanceAsset> maList=SFS_TestDataFactory.createMaintenanceAsset(1,mpList[0].Id,assetList[0].Id,false);
        insert maList;
        List<MaintenanceWorkRule> mwrList=SFS_TestDataFactory.createMaintenanceWorkRule(1,maList[0].Id,workTypeList[0].Id,false);
        mwrList[0].RecurrencePattern='FREQ=MONTHLY;COUNT=1;';
        insert mwrList;
        
        List<Id> mpIds=new List<Id>();
        mpIds.add(mpList[0].Id);
        System.enqueueJob(new SFS_UpdateAlphaWOLIQueueable(mpIds));
        Test.stopTest();
    }  
}