@isTest

public with sharing class ShareMyInvntory_Ctrl_Test {


    @TestSetup static void createData(){

        Account distributor = new Account(
            Name = 'Test Account'
        );
        insert distributor;
    }
    
    @isTest static void testUpdateAccount() {
        Account acct = new Account(Name = 'Test Account');
        
        insert acct;
        
        acct.Name = 'Test Account2';
        
        ShareMyInventory_Ctrl.updateAccount(acct);
    }

    @isTest static void getPartnersAccount(){
        User profile =[SELECT ProfileId FROM  User WHERE Profile_Name__c=:'Customer Service' LIMIT 1];
        User distributor = [
            SELECT  Id FROM User WHERE ProfileId =:profile.ProfileId AND IsActive = TRUE LIMIT 1
        ];
            Account acc = [select Id from Account limit 1];

        System.runAs(distributor){

            System.Test.startTest();
                ShareMyInventory_Ctrl.doInit(acc);
            System.Test.stopTest();
        }
    }
}