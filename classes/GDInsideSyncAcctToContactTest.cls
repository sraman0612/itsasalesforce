@isTest
public class GDInsideSyncAcctToContactTest {
    @isTest static void testSync()
    {
        Account a1 = new Account(name = 'test1', Account_Number__c = '12345');
        insert a1;
        
        Account a2 = new Account(name = 'Test2', Account_Number__c = '12346');
		insert a2;
        
        Contact c = new Contact(firstname = 'Christopher', lastname='Knitter', email='Christopher.Knitter@canpango.com.mock');
        insert c;
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new GDInside_Mock_HTTP_ContactSync());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        GDInsideSyncAcctToContactSched sh1 = new GDInsideSyncAcctToContactSched();
		String sch = '0 0 23 * * ?'; 
        system.schedule('Test CDInsideSync Check', sch, sh1);
        GDInsideSyncAcctToContact.sync('2017-10-16');
        
        // Verify response received contains fake values
        //String contentType = res.getHeader('Content-Type');
        //System.assert(contentType == 'application/json');
        //String actualValue = res.getBody();
        /*String expectedValue = '[{"id":"11314","first_name":"Christopher","last_name":"Knitter","phone_number":"","company_name":"Canpango","account_number":"12345",' + 
            '"alt_address_1":"","alt_address_2":"","alt_city":"","alt_state":"","alt_zip":"","country":"USA","address_1":"","address_2":"","city":""' + 
            ',"state":"","zip":"","alt_country":"USA","email":null,"sap_account_number":"12345","sap_company_name":"Ben\u0027s Distro 3","sap_sales_office":"","disabled":"0",' +
            '"date_disabled":null,"created_by":null,"account_type":"1","default_access":"16","concessions":"0","salesman":"","notes":"","request_date":null,"date_approved":"2017-07-11",' + 
            '"date_denied":null,"status":"2","allow_asset_submission":"0","temp_password":"0","temp_password_date":null,"account_info_updated":"0","subscriptions_updated":"0",' + 
            '"concession_manager":null,"username":"christopher.knitter@canpango.com.mock","pass":"7f2ababa423061c509f4923dd04b6cf1","token":"266bec3a7c1fe04ff211f1f305c23fd1",' +
            '"distributor_account_manager":"0","password_changed":"2017-07-11","activity_email":null,"distributor_contact":"0","ldap_username":null,"dn":null,"lms_user":"0",' + 
            '"ams_user_role":"","additional_sap_account_numbers":"","eval_user_role":"","distributor_profile":"0","access_template_id":null,"sales_force_profile_id":"3",' +
            '"attribute_id":null,"created_at":"2017-07-11 15:30:37","updated_at":"2017-10-16 15:28:57","Zones":"16","User Types":null,"Admin Groups":null},'+
            '{"id":"11314","first_name":"Jonathan","last_name":"Brooks","phone_number":"","company_name":"Canpango","account_number":"12346",' + 
            '"alt_address_1":"","alt_address_2":"","alt_city":"","alt_state":"","alt_zip":"","country":"USA","address_1":"","address_2":"","city":""' + 
            ',"state":"","zip":"","alt_country":"USA","email":null,"sap_account_number":"12346","sap_company_name":"Ben\u0027s Distro 3","sap_sales_office":"","disabled":"0",' +
            '"date_disabled":null,"created_by":null,"account_type":"1","default_access":"16","concessions":"0","salesman":"","notes":"","request_date":null,"date_approved":"2017-07-11",' + 
            '"date_denied":null,"status":"2","allow_asset_submission":"0","temp_password":"0","temp_password_date":null,"account_info_updated":"0","subscriptions_updated":"0",' + 
            '"concession_manager":null,"username":"jonathan.brooks@canpango.com.mock","pass":"7f2ababa423061c509f4923dd04b6cf1","token":"266bec3a7c1fe04ff211f1f305c23fd1",' +
            '"distributor_account_manager":"0","password_changed":"2017-07-11","activity_email":null,"distributor_contact":"0","ldap_username":null,"dn":null,"lms_user":"0",' + 
            '"ams_user_role":"","additional_sap_account_numbers":"","eval_user_role":"","distributor_profile":"0","access_template_id":null,"sales_force_profile_id":"3",' +
            '"attribute_id":null,"created_at":"2017-07-11 15:30:37","updated_at":"2017-10-16 15:28:57","Zones":"16","User Types":null,"Admin Groups":null}]';
        //System.assertEquals(actualValue, expectedValue);*/
        //System.assertEquals(200, res.getStatusCode());
    }

}