public class AssetTriggerHandler {
    public static void before(List<Asset> assets){
		If(!TrggrUtility.RunOnce) {
            assetbefore(assets);
            setDistributorChannelChildAccount(assets);
            TrggrUtility.RunOnce = true;
        }
    }
    public static void after(Map<Id, Asset> assets, Map<Id, Asset> oldAssets){
        System.debug('TrggrUtility.RunOnceupdat' + TrggrUtility.RunOnceupdate);
		If(!TrggrUtility.RunOnceupdate) {
            assetafter(assets.values());
            system.debug('before method call');
            recalculatePointsForChildAccounts(assets.values());
            TrggrUtility.RunOnceupdate = true;
            System.debug('TrggrUtility.RunOnceupdat' + TrggrUtility.RunOnceupdate);
        }
        DistiAsset.createChild(assets, oldAssets);
    }
    public static void assetbefore(List<Asset> assets){
        //there is no sap mapping in the metadata table
        //assets = AssetHelper.SAPMapping(assets);
        List<String> extIds = new List<String>();
        // 06-13-2019 jhm: Get a list of the Accounts that have Points, so we can summarize them.
        List<Id> accountIdsWithPoints = new List<Id>();
        for (Asset a : assets){
            if (a.External_ID__c == null){
                String ext = String.valueof(system.now()) +  EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(0,15);
                a.External_ID__c = ext;
            }
            // 06-13-2019 jhm: Collect the Account Ids for the Assets with Points.
            //if( null != a.Points__c && 0 < a.Points__c ) {
            
            if(a.AccountId != null){
            	accountIdsWithPoints.add( a.AccountId ); 
            }
            //}
        }
        // 06-13-2019 jhm: If we caught anything, go summarize it.
        if( 0 < accountIdsWithPoints.size() ) {
        	List<AggregateResult> lAR = [SELECT AccountId, SUM(Points__c) Points
        								 FROM Asset
        								 WHERE AccountId IN :accountIdsWithPoints
                                         AND Show_in_DW__c = true 
        								 AND DelDate__c > :date.today().addMonths(-24)
        								 GROUP BY AccountId];
            system.debug('running asset aggregate'+lAR);
        	List<Account> accountsToUpdate = new List<Account>();
        	for( AggregateResult ar : lAR ) {
        		accountsToUpdate.add( new Account( Id = (String)ar.get('AccountId'), Points__c = (Decimal)ar.get('Points') ) );
        	}
        	if( 0 < accountsToUpdate.size() ) {
        		database.update( accountsToUpdate, false );
        	}
        }
    } 
     public static void assetafter(List<Asset> assets){
        List<Asset_Share__c> shares = new List<Asset_Share__c>();

        for (Asset a : assets){
            if (a.Distributor_Warehouse__c == null){
                Asset_Share__c ash = new Asset_Share__c(External_Id__c = a.External_ID__c, serial_number__c = a.Id);
                shares.add(ash);
            }
        }

        if(!shares.isEmpty()) {
            database.insert(shares, false);
        }
    }

    public static void setDistributorChannelChildAccount (List<Asset> triggerAssets) {
        Map<String, Asset> mapAccountDCToAsset = new Map<String, Asset>();
        Set<String> whereKeys = new Set<String>();
        String selectClause = 'SELECT Id, Name, ParentId, DC__c, DChannel_First_2__c, Points__c FROM Account ';
        String whereClause = '';

        Set<Id> accountIds = new Set<Id>();

        for(Asset triggerAsset : triggerAssets) {
            accountIds.add(triggerAsset.AccountId);
        }
        Map<Id, Account> mapIdAccount = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE Id IN :accountIds]);
        List<String> wherePairs = new List<String>();
        for (Asset triggerAsset : triggerAssets) {
            Account assetAccount = mapIdAccount.get(triggerAsset.AccountId);
            if(assetAccount != null && String.isNotBlank(assetAccount.Name) && String.isNotBlank(triggerAsset.Dchl__c)) {
                String key = assetAccount.Name + ',' + triggerAsset.Dchl__c;
                if (!whereKeys.contains(key)) {
                    whereKeys.add(key);
                    mapAccountDCToAsset.put(key, triggerAsset);
                    wherePairs.add('(ParentId = \'' + assetAccount.Id + '\' AND Name = \'' + assetAccount.Name + '\' AND DChannel_First_2__c = \'' + triggerAsset.Dchl__c + '\')');
                }
            }            
        }

        List<Account> matchedAccounts = new List<Account>();
        if (wherePairs.size() > 0) {
            whereClause = ' WHERE ' + String.join(wherePairs, ' OR ');
            matchedAccounts = Database.query(selectClause + whereClause);
        }
        
        Set<String> matchedAccountSet = new Set<String>();
        List<Error_Log__c> errorLogsToInsert = new List<Error_Log__c>();

        if(!matchedAccounts.isEmpty()) {
            for(Account matchedAccount : matchedAccounts) {
                String accountKey = matchedAccount.ParentId + matchedAccount.Name + matchedAccount.DChannel_First_2__c;
                if(!matchedAccountSet.contains(accountKey)) {
                    matchedAccountSet.add(accountKey);
                }
            }
        }

        for(Account matchedAccount : matchedAccounts) {
            String mapKey = matchedAccount.Name + ',' + matchedAccount.DChannel_First_2__c;

            if(whereKeys.contains(mapKey)) {
                System.debug('inside setting');
                Asset triggerAsset = mapAccountDCToAsset.get(mapKey);

                triggerAsset.DC_Child_Account__c = matchedAccount.Id;
                // assetsToUpdate.add(assetToUpdate);
            }
        }

        // System.debug(assetsToUpdate);

        // try {
        //     update assetsToUpdate;
        // }
        // catch(DmlException e) {
        //     List<Error_Log__c> errorLogsToInsert = new List<Error_Log__c>();

        //     for(Integer i = 0; i < e.getNumDml(); i++) {
        //         errorLogsToInsert.add(ErrorLogService.buildErrorLog(
        //             'Exception on update of Assets in AssetTriggerHandler',
        //             e.getDmlMessage(i),
        //             e.getDmlId(i),
        //             e.getTypeName(),
        //             'Trigger'
        //         ));
        //     }
        //     insert errorLogsToInsert;
        // }
    }

    private static void recalculatePointsForChildAccounts(List<Asset> triggerAssets) {
        List<Id> accountIds = new List<Id>();

        for (Asset a : triggerAssets) {
            if(String.isNotBlank(a.DC_Child_Account__c)) {
                accountIds.add(a.DC_Child_Account__c);
            }
        }

        if( 0 < accountIds.size() ) {
        	List<AggregateResult> lAR = [SELECT DC_Child_Account__c, SUM(Points__c) Points
        								 FROM Asset
        								 WHERE DC_Child_Account__c IN :accountIds
                                         AND Show_in_DW__c = true 
                                         AND DelDate__c > :date.today().addMonths(-24)
                                         GROUP BY DC_Child_Account__c];
                                         
        	List<Account> accountsToUpdate = new List<Account>();
        	for( AggregateResult ar : lAR ) {
        		accountsToUpdate.add( new Account( Id = (String)ar.get('DC_Child_Account__c'), Points__c = (Decimal)ar.get('Points') ) );
        	}
        	if( 0 < accountsToUpdate.size() ) {
        		database.update( accountsToUpdate, false );
        	}
        }
    }
}