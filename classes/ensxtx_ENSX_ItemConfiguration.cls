global with sharing class ensxtx_ENSX_ItemConfiguration 
{
    public String materialNumber{get;set;}
    public String plant{get;set;}
    public String SalesDocumentCurrency { get; set;}
    public Decimal OrderQuantity { get; set;}
    public List<ensxtx_ENSX_Characteristic> selectedCharacteristics {get;set;}
}