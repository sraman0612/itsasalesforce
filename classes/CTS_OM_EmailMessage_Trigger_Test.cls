@IsTest
public with sharing class CTS_OM_EmailMessage_Trigger_Test {
    
    @TestSetup
    private static void createData(){
        EmailMessage_TriggerTestHelper.createData();
  
    }
    
    private static testMethod void createNewCaseAndReparentEmail(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmail(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId);
    }
    
    private static testMethod void createNewCaseAndReparentEmailPreexistingOpenChildCase(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailPreexistingOpenChildCase(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId);
    }    
    
    private static testMethod void createNewCaseAndReparentEmailInactiveOwner(){
    	Email_Message_Setting__mdt settings = [SELECT DeveloperName,Email_Message_Trigger_Enabled__c,CTS_Default_Case_Owner_ID__c,CTS_OM_ZEKS_Default_Case_Owner_Id__c,CTS_OM_Americas_Default_Case_Owner_Id__c,CTS_OM_Email_Case_Record_Type_IDs_1__c,	CTS_OM_Email_Case_Record_Type_IDs_2__c,	CTS_OM_Email_Case_Record_Type_IDs_3__c,	CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Email_Case_Record_Type_IDs_5__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case' LIMIT 1];
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailInactiveOwner(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, settings.CTS_Default_Case_Owner_ID__c);        
    }    
    
    private static testMethod void createNewCaseAndReparentEmailOwnedByQueue(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailOwnedByQueue(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId);        
    }        
    
    private static testMethod void testPreventDuplicateEmailsDisabled(){
    	
    	Email_Message_Setting__mdt settings = [SELECT DeveloperName,Email_Message_Trigger_Enabled__c,CTS_Default_Case_Owner_ID__c,CTS_OM_ZEKS_Default_Case_Owner_Id__c,CTS_OM_Americas_Default_Case_Owner_Id__c,CTS_OM_Email_Case_Record_Type_IDs_1__c,CTS_OM_Email_Case_Record_Type_IDs_5__c,CTS_OM_Email_Case_Record_Type_IDs_2__c,	CTS_OM_Email_Case_Record_Type_IDs_3__c,	CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case' LIMIT 1];
        settings.CTS_OM_Email_Trigger_Enabled__c = false;
        //upsert settings;       	
    	
      //  EmailMessage_TriggerTestHelper.testPreventDuplicateEmails(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, false);         
    }    
    
    private static testMethod void testPreventDuplicateEmailsEnabled(){
      //  EmailMessage_TriggerTestHelper.testPreventDuplicateEmails(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, true);         
    }
}