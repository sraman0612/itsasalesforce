/*=========================================================================================================
* @author : Naresh P, Capgemini
* @date : 1/09/2022
* @description: Billing Events (Workorder Billing) integration
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

==============================================================================================================*/

public without sharing class SFS_Billing_WorkOrder_Outbound_Handler {
    @invocableMethod(label = 'Outbound Invoice to Oracle' description = 'Send to CPI' Category = 'Invoice')
    public static void Run(List<Id> invoiceId){
        //Main Method
        String xmlString = '';
        Map<Double, String> invoiceMap = new Map<Double, String>();
        ProcessData(invoiceId, invoiceMap,xmlString);
        system.debug('@@invoiceId'+invoiceId);
    }
    public static void ProcessData (List<Id>invoiceId,Map<Double, String> invoiceMap,String xmlString ){
        SFS_WorkOrder_Billling_XML_Structure__mdt[] billingMetaMap = [SELECT SFS_Salesforce_Field__c,SFS_Salesforce_Object__c,SFS_Node_Order__c, SFS_XML_Full_Name__c, SFS_XML_Object__c, SFS_Charge_Type__c,SFS_Hardcoded_Flag__c
                                                                      FROM SFS_WorkOrder_Billling_XML_Structure__mdt Order By SFS_Node_Order__c asc];


            Invoice__c inv = [Select Id,Name,SFS_Approval_Flag__c,SFS_Invoice_Description__c,SFS_External_Id__c,SFS_Invoice_Print_Code__c,Invoice_Date__c,
                          SFS_Invoice_Status__c,SFS_Invoice_Amount__c,SFS_Invoice_Type__c,SFS_Oracle_Invoice_Type__c,SFS_Canceled__c,CurrencyIsoCode,
                          SFS_Credit_Memo_Reason__c,SFS_Project_Number__c,SFS_Total_InvLI_Amount__c, SFS_No_Billing_Calculator__c,SFS_Invoice_Format__c,CreatedBy.Name,SFS_Work_Order__r.SFS_External_Id__c,
                          SFS_Work_Order__r.SFS_WOorAgreementWO__c,SFS_Work_Order__r.WorkOrderNumber,SFS_Work_Order__r.SFS_Oracle_End_Date__c,SFS_Work_Order__r.SFS_Oracle_Start_Date__c,SFS_Authorization_Number__c,
                          SFS_Expiration_Month__c,SFS_Expiration_Year__c,SFS_Selected_Credit_Card__c,SFS_SubscriptionID__c,SFS_Request_Token__c,SFS_Oracle_CreditCard_Type__c,SFS_Authorization_Date__c
                          FROM Invoice__c WHERE Id =: InvoiceId];



             List<SFS_Invoice_Line_Item__c> invoiceList = [Select Id,SFS_Activity_Id__c,Name,SFS_Sell_Price__c,SFS_Description__c,CurrencyIsoCode,SFS_Integration_Status__c,SFS_Type__c,
                                                      SFS_Invoice__r.Name,SFS_Invoice__r.SFS_PO_Number__c,SFS_Invoice__r.Invoice_Date__c,SFS_Work_Order__r.SFS_External_Id__c,
                                                      SFS_Labor__r.Name,SFS_Expense__r.ExpenseNumber,SFS_Expense__r.ExpenseType,SFS_Part_Consumed__r.SFS_Product_Code__c,SFS_Part_Consumed__r.ProductConsumedNumber from SFS_Invoice_Line_Item__c WHERE SFS_Invoice__r.id =: inv.id];


        //Map Queries, then populate using getPopulatedFieldAsMap()

        MAP<String,Object> finalInvoiceFieldsMap = new MAP<String,Object>();
        Map<String, Object> invMap = new Map<String, Object>();
        invMap = inv.getPopulatedFieldsAsMap();
        System.debug('@@invMap'+invMap);

        Map<String,Object> woFields = new Map<String,Object>();
       try{
        WorkOrder wo = (WorkOrder)invMap.get('SFS_Work_Order__r');
        woFields = wo.getPopulatedFieldsAsMap();
        }catch(exception e){}

        Map<String,Object> wUserFields = new Map<String,Object>();
        //try{
        User wuser = (User)invMap.get('CreatedBy');
        wUserFields = wuser.getPopulatedFieldsAsMap();
       // }catch(exception e){}

        SFS_Billing_WorkOrder_Outbound_Handler billing = new SFS_Billing_WorkOrder_Outbound_Handler();
        List<String> billingXMLList = new List<String>();
        String chargeType = '';
        for(String field : invMap.keyset()){
            if(!field.contains('SFS_Work_Order__r') && !field.contains('CreatedBy') && field!='Invoice_Date__c' && !field.contains('RecordType.DeveloperName')) {
                finalInvoiceFieldsMap.put(field,invMap.get(field));
            }
        }
        for(String field : woFields.keyset()){
            if(woFields.get(field)!='' && (field=='SFS_Oracle_Start_Date__c' || field=='SFS_Oracle_End_Date__c')){
                Map<String,Integer> monthNameMap=new Map<String,Integer>{'JAN' =>01, 'FEB'=>02, 'MAR'=>03, 'APR'=>04, 'MAY'=>05,
                                                                    'JUN'=>06, 'JUL'=>07, 'AUG'=>08, 'SEP'=>09,'OCT'=>10,
                                                                    'NOV'=>11, 'DEC'=>12};
                String OracleDATE=String.valueOf(woFields.get(field));
                Integer dd = integer.valueOf(OracleDATE.substring(0,2)) ;
                Integer mo = integer.valueOf(monthNameMap.get(OracleDATE.substring(3,6)));
                Integer yr = integer.valueOf(OracleDATE.substring(7,11));

                Date DT = Date.newInstance(yr, mo, dd);
                string fieldstring = 'SFS_Work_Order__r'+'-'+field;
                finalInvoiceFieldsMap.put(fieldstring,DT);

            }else if(field == 'SFS_External_Id__c'){
                string fieldstring = 'SFS_Work_Order__r'+'-'+field;
                finalInvoiceFieldsMap.put(fieldstring,woFields.get(field));

            }
            else{
                string fieldstring = 'SFS_Work_Order__r'+'-'+field;
                finalInvoiceFieldsMap.put(fieldstring, woFields.get(field));
            }
        }
        for(String field : wUserFields.keyset()){
            string fieldstring = 'CreatedBy'+'-'+field;
            finalInvoiceFieldsMap.put(fieldstring, wUserFields.get(field));
        }
        Map<Integer,Map<String,Object>> lineItemsMap = new Map<Integer,Map<String,Object>>();
        for(SFS_Invoice_Line_Item__c invl:invoiceList){

            Map<String,Object> invoiceLineItemFields = new Map <String, Object>();
            invoiceLineItemFields = invl.getPopulatedFieldsAsMap();

            Map<String,Object> invoiceFields = new Map<String,Object>();
         try{
            Invoice__c invli = (Invoice__c)invoiceLineItemFields.get('SFS_Invoice__r');
            InvoiceFields = invli.getPopulatedFieldsAsMap();
            System.debug('InvoiceFields: '+InvoiceFields);
         }catch(exception e){}

            Map<String,Object> inlvWoFields = new Map<String,Object>();
          try{
            WorkOrder wof = (WorkOrder)invoiceLineItemFields.get('SFS_Work_Order__r');
            inlvWoFields = wof.getPopulatedFieldsAsMap();
            }
           catch(exception e){}

            Map<String,Object> inlproductConFields = new Map<String,Object>();
           try{
                ProductConsumed prof = (ProductConsumed)invoiceLineItemFields.get('SFS_Part_Consumed__r');
                inlproductConFields = prof.getPopulatedFieldsAsMap();


           }
           catch(exception e){
          }

            Map<String,Object> inlLaborConFields = new Map<String,Object>();
           try{
                CAP_IR_Labor__c labor = (CAP_IR_Labor__c)invoiceLineItemFields.get('SFS_Labor__r');
                inlLaborConFields = labor.getPopulatedFieldsAsMap();


          }
          catch(exception e){
           }

            Map<String,Object> inlExpenseFields = new Map<String,Object>();
           try{
                Expense exp = (Expense)invoiceLineItemFields.get('SFS_Expense__r');
                                inlExpenseFields = exp.getPopulatedFieldsAsMap();

          }
          catch(exception e){
          }

            Map<String,Object> finalinvlFields = new Map<String,Object>();
            for(String field : invoiceLineItemFields.keyset()){
                if(!field.contains('SFS_Invoice__r') && !field.contains('SFS_Work_Order__r') && !field.contains('SFS_Part_Consumed__r') && !field.contains('SFS_Labor__r') && !field.contains('SFS_Expense__r')){
                    finalinvlFields.put(field,invl.get(field));
                }
                if(field == 'SFS_Type__c'){
                    finalinvlFields.put(field,invl.get(field));
                    finalInvoiceFieldsMap.put(field,invl.get(field));
                }
            }
            for(String field : InvoiceFields.keyset()){
                string fieldstring = 'SFS_Invoice__r'+'-'+field;
                finalinvlFields.put(fieldstring, InvoiceFields.get(field));
            }
            for(String field : inlvWoFields.keyset()){
                string fieldstring = 'SFS_Work_Order__r'+'-'+field;
                finalinvlFields.put(fieldstring, inlvWoFields.get(field));
            }
            for(String field : inlproductConFields.keyset()){
                string fieldstring = 'SFS_Part_Consumed__r'+'-'+field;
                finalinvlFields.put(fieldstring, inlproductConFields.get(field));
            }
            for(String field : inlLaborConFields.keyset()){
                string fieldstring = 'SFS_Labor__r'+'-'+field;
                finalinvlFields.put(fieldstring, inlLaborConFields.get(field));
            }
            for(String field : inlExpenseFields.keyset()){
                string fieldstring = 'SFS_Expense__r'+'-'+field;
                finalinvlFields.put(fieldstring, inlExpenseFields.get(field));
            }
        finalinvlFields.put('Count',invoiceList.indexOf(invl)+1);
        lineItemsMap.put(invoiceList.indexOf(invl),finalinvlFields);
        }
        billingXMLList = billing.buildXmlStructure(billingMetaMap,finalInvoiceFieldsMap,'Invoice','Header');
        List<String> invoiceLXMLList = new List<String>();
        //Charges XML build logic
           for(Integer i=0;i<lineItemsMap.Size();i++){
                  Map<String,Object> lineItemsFieldsMap = (Map<String, Object>)lineItemsMap.get(i);
                  system.debug('@invoiceLXMLList'+invoiceLXMLList);
                  chargeType = string.valueOf(lineItemsFieldsMap.get('SFS_Type__c'));
                  system.debug('@chargeType'+chargeType);
                  invoiceLXMLList = billing.buildXmlStructure(billingMetaMap,lineItemsFieldsMap,'InvoiceLineItem',chargeType);
                  system.debug('@reXMLList'+billingXMLList);
                  billingXMLList.addAll(invoiceLXMLList);
          }
        SFS_WorkOrder_Billling_XML_Structure__mdt re = SFS_WorkOrder_Billling_XML_Structure__mdt.getInstance('SFS_End_Tag');
        billingXMLList.add(re.SFS_XML_Full_Name__c);
        for(String s : billingXMLList){
            xmlString = xmlString + s;
            System.debug('XML:  ' + s);
        }
        System.debug('XMLString ' + xmlString);
        //Callout
        String IdString = String.ValueOf(InvoiceId[0]);
        String interfaceDetail ='INT-155';
        String InterfaceLabel = 'WorkorderBilling|' + IdString;
        SFS_Outbound_Integration_Callout.HttpMethod(xmlString, interfaceDetail, InterfaceLabel);
    }
    public List<String> buildXmlStructure(SFS_WorkOrder_Billling_XML_Structure__mdt[] billingMetaMap,Map<String,Object>fieldsMap,String ObjectName,String ChargeType){
        List<String> xmlStructure = new List<String>();
        for(SFS_WorkOrder_Billling_XML_Structure__mdt m: billingMetaMap){
            System.debug('Map Meta: ' + m.SFS_XML_Full_Name__c);
            if(m.SFS_Salesforce_Object__c==ObjectName || m.SFS_Charge_Type__c==ChargeType){
                if(m.SFS_HardCoded_Flag__c == false){
                    String sfField = m.SFS_Salesforce_Field__c;
                    If(sfField=='Invoice_Date__c'){
                         String xmlfullName = m.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(System.today());
                            replacement = replacement.escapeXML();
                            String newpa = XMLFullName.replace(sffield,replacement);
                            xmlStructure.add(newpa);
                    }
                    else if(fieldsMap.containsKey(sfField)){
                        if(fieldsMap.get(sffield) != null){
                            String xmlfullName = m.SFS_XML_Full_Name__c;
                            String replacement = String.ValueOf(fieldsMap.get(sffield));
                            replacement = replacement.escapeXML();
                            String newpa = XMLFullName.replace(sffield,replacement);
                            xmlStructure.add(newpa);
                        }
                        else if(fieldsMap.get(sffield) == null){
                            String replacement = m.SFS_XML_Full_Name__c.escapeXML();
                            //xmlStructure.add(m.SFS_XML_Full_Name__c);
                            xmlStructure.add(replacement);
                        }
                    }
                    else if(!fieldsMap.ContainsKey(sffield)){
                        try{
                        String empty = '';
                        String replacement = m.SFS_XML_Full_Name__c.replace(sffield,empty);
                        replacement = replacement.escapeXML();
                        xmlStructure.add(replacement);
                        }
                        catch(exception e){}
                    }
                }
                else if(m.SFS_HardCoded_Flag__c == true){
                    xmlStructure.add(m.SFS_XML_Full_Name__c);
                }
            }
        }
        return xmlStructure;
    }
    Public static void getResponse(HttpResponse res, String interfaceLabel){
       List<String> InvoiceId = interfaceLabel.split('\\|');
        Id InvoiceIDRes = Id.ValueOf(InvoiceId[1]);

        Invoice__c invoice = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM Invoice__c WHERE Id =: InvoiceIDRes];
        List<SFS_Invoice_Line_Item__c> INVline = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM SFS_Invoice_Line_Item__c WHERE SFS_Invoice__c =: InvoiceIDRes];
        //List<CAP_IR_Charge__c> Charge1 = [SELECT Id, SFS_Integration_Status__c, SFS_Integration_Response__c FROM CAP_IR_Charge__c WHERE SFS_Invoice__c =: InvoiceIDRes];
        If(res.getStatusCode() != 200){
            if(res.getStatusCode() == 0 || res.getBody() == 'Read timed out/nnull')
            {
               invoice.SFS_Integration_Status__c = 'Submit-Timed Out';
               invoice.SFS_Integration_Response__c = res.getBody();
            for(SFS_Invoice_Line_Item__c c: INVline){
                c.SFS_Integration_Status__c = 'Timed Out';
                c.SFS_Integration_Response__c =  res.getBody();
            }
                /*for(CAP_IR_Charge__c ch: Charge1){
                ch.SFS_Integration_Status__c = 'Timed Out';
                ch.SFS_Integration_Response__c =  res.getBody();
            }  */
            }
            else{
            invoice.SFS_Integration_Status__c = 'Submit-Failed';
            invoice.SFS_Integration_Response__c = res.getBody();
            for(SFS_Invoice_Line_Item__c c: INVline){
                c.SFS_Integration_Status__c = 'Failed';
                c.SFS_Integration_Response__c =  res.getBody();
            }
                /*for(CAP_IR_Charge__c ch: Charge1){
                ch.SFS_Integration_Status__c = 'Failed';
                ch.SFS_Integration_Response__c =  res.getBody();
            }  */
            }
        }
        else{
            invoice.SFS_Integration_Status__c = 'Submitted';
            invoice.SFS_Integration_Response__c = res.getBody();
            for(SFS_Invoice_Line_Item__c c: INVline){
                c.SFS_Integration_Status__c = 'Submitted';
                c.SFS_Integration_Response__c = res.getBody();
                //invoice.SFS_Integration_Response__c = res.getBody();
            }
            /*for(CAP_IR_Charge__c ch: Charge1){
                ch.SFS_Integration_Status__c = 'Submitted';
                ch.SFS_Integration_Response__c =  res.getBody();
            }  */
        }
        update INVline;
        update invoice;
        //update INVline;


    }
}