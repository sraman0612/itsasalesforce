public with sharing class ensxtx_ENSX_VCCharacteristicValues
{
    @AuraEnabled
    public String CharacteristicId { get; set;}
    @AuraEnabled
    public String CharacteristicDescription {get;set;}
    @AuraEnabled
    public String Value { get; set;}
    @AuraEnabled
    public String ValueDescription { get; set;}

    public static ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES getSBOAllowedValuesForModel(ensxtx_ENSX_VCCharacteristicValues val)
    {
        // System.debug('ensxtx_ENSX_VCCharacteristicValues.getSBOAllowedValuesForModel');
        ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES av = new ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES();
        av.CharacteristicValueDescription = val.ValueDescription;
        av.CharacteristicValue = val.Value;
        av.CharacteristicID = val.CharacteristicId;
        return av;
    }
    public static ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES getSBOASelectedValuesForModel(ensxtx_ENSX_VCCharacteristicValues val)
    {
        ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES sv = new ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES();
        sv.CharacteristicValue = val.Value;
        sv.CharacteristicID = val.CharacteristicId;
        return sv;
    }
}