@isTest
global class SFS_AssetStartUpDateOutbound_Test {
@isTest
    public static void SFS_AssetStartUpDateOutboundTest()
    {
        test.setMock(HttpCalloutMock.class, new mockCallout());
        //Inserting Account
        List<Id> assetId=new List<Id>();
        account acct=new account(name='Test',type='Prospect',ShippingStreet = '1412 Fox Run Parkway',ShippingPostalCode = '36804',ShippingState = 'AL',ShippingCountry = 'USA');
        insert acct;
        CTS_IOT_Frame_Type__c frmeTypeFil = new CTS_IOT_Frame_Type__c(Name = 'testFilter', CTS_IOT_Type__c = 'Filter');
        insert frmeTypeFil;
        
        //Account acct =SFS_TestDataFactory.getAccount();
        List<Asset> ast = SFS_TestDataFactory.createAssets(1,false);
        ast[0].AccountId = acct.Id;
        ast[0].CTS_Frame_Type__c=frmeTypeFil.id;
        insert ast[0];
        assetId.add(ast[0].id);
       
        SFS_AssetStartUpDateOutboundIntegration startUp=new SFS_AssetStartUpDateOutboundIntegration();
        SFS_AssetStartUpDateOutboundIntegration.Run(assetId);
        
    }
   global class mockCallout implements HttpCalloutMock
    {
        global HttpResponse respond(HttpRequest request)
        {
            string body='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://www.w3.org/2005/08/addressing"> <env:Header> <wsa:Action>execute</wsa:Action> <wsa:MessageID>urn:06dbca19-9650-11ec-b011-02001708e866</wsa:MessageID> <wsa:ReplyTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> <wsa:ReferenceParameters> <instra:tracking.ecid xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">efdbe64f-8c4f-4d51-970a-9a23aca858b4-019cdae0</instra:tracking.ecid> <instra:tracking.FlowEventId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">65910440</instra:tracking.FlowEventId> <instra:tracking.FlowId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">10236104</instra:tracking.FlowId> <instra:tracking.CorrelationFlowId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">0000NwlWN346qIB_vXh8iX1Y5u^f00000k</instra:tracking.CorrelationFlowId> <instra:tracking.quiescing.SCAEntityId xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">3760029</instra:tracking.quiescing.SCAEntityId> </wsa:ReferenceParameters> </wsa:ReplyTo> <wsa:FaultTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> </wsa:FaultTo> </env:Header> <env:Body> <ATPResponse xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns="http://xmlns.irco.com/ATPPriceOutResponseABCSImpl"> <ControlArea xmlns=""> <PartnerCode>CPQ</PartnerCode> <RequestType>QUERY</RequestType> <ExternalMessageID>CPQ TestArjun Test Opty 10012021_CTS-76421645803040608</ExternalMessageID> <Database>ISOADEV1</Database> <SourceSystem>EBS</SourceSystem> <Status>SUCCESS</Status> <Messsage>Request Processed Successfully</Messsage> <MessageID>D5DF380550575F90E0540021280BE0EF</MessageID> </ControlArea> <DataArea xmlns=""> <GItem> <Item> <ItemNumber>23231806</ItemNumber> <BestWHSE>DLC (DCL)</BestWHSE> <GAvailability> <Availability> <WHSE>DLC (DCL)</WHSE> <ResultCode>SUCCESS</ResultCode> <AvailableDate>25-FEB-2022</AvailableDate> </Availability> </GAvailability> </Item> </GItem> </DataArea> </ATPResponse> </env:Body> </env:Envelope>';
            HttpResponse res = new HttpResponse();
            res.setBody(body);
            res.setStatusCode(200);
            return res;
        } 
    }  
}