public class ensxtx_UTIL_SalesDoc
{

    // Object that contains the main SObject and SF Line Items
    public class SFObject
    {
        @AuraEnabled public String sfObjectType { get; set; }
        @AuraEnabled public String sfObjectLabel { get; set; }
        @AuraEnabled public SObject sfMainObject { get; set; }
        @AuraEnabled public List<SObject> sfLineItems { get; set; }
        @AuraEnabled public Boolean initFromSObject { get; set; }
        @AuraEnabled public String pricebookId { get; set; }
        @AuraEnabled public String customerNumber { get; set; }
        @AuraEnabled public String sapDocNumber { get; set; }
        @AuraEnabled public String status { get; set; }
    }

    // Mainly used for Configuration Indicator
    // Returned from Incompletion Log for Different Languages
    public static Set<String> configurationLogText = new Set<String>{
        'Configuration',
        'Konfiguration',
        'Configuratie',
        'Configurazione',
        'Конфигурация',
        'Konfiguracja',
        'Konfigurácia',
        'Configuración',
        'Konfigurace',
        'Konfiguraatio',
        'Konfigurasjon',
        'Konfigürasyon'
    };

    private static List<String> accountFields = new List<String>{
        'DC__c',
        'SD__c',
        'Child_DCs__c',
        'SOrg__c'
    };

    public static ensxtx_UTIL_SalesDoc.SFObject getSFObjectInfo(String recordId, String sapDocType)
    {
        ensxtx_UTIL_SalesDoc.SFObject result = new ensxtx_UTIL_SalesDoc.SFObject();
        Boolean isSFObjectDoc = true;

        SObject sfSObject = ensxtx_UTIL_SFSObjectDoc.getSObject(recordId);
        if (sfSObject == null) {
            // sfSObject returns null when is not an implementation of I_SFObjectDoc
            isSFObjectDoc = false;
            sfSObject = ensxtx_UTIL_SFAccount.getAccountById(recordId, accountFields);
        }

        if (sfSObject == null) {
            ensxtx_UTIL_PageMessages.addMessage('ERROR', 'SObject does not exist for record Id ' + recordId);
        }
        else {
            DescribeSObjectResult sObjectDescribe = sfSobject.getSObjectType().getDescribe();
            String sfSObjectName = sObjectDescribe.getName();
            result.sfMainObject = sfSObject;
            result.sfObjectType = sfSObjectName;
            result.sfObjectLabel = sObjectDescribe.getLabel();
            result.initFromSObject = isSFObjectDoc;
            String customerNumber;

            if (isSFObjectDoc)
            {
                Boolean isValid = false;
                result.status = ensxtx_UTIL_SFSObjectDoc.getStatus(sfSObject);

                result.pricebookId = ensxtx_UTIL_SFSObjectDoc.getPriceBookId(sfSObject);

                // Validate Account Id and Customer Number on sfSobject
                String accountId = ensxtx_UTIL_SFSObjectDoc.getAccountId(sfSObject);
                if (String.isEmpty(accountId)) {
                    ensxtx_UTIL_PageMessages.addMessage('ERROR', 'Account is not found on ' + sfSObjectName);
                }
                else {
                    customerNumber = ensxtx_UTIL_SFSObjectDoc.getCustomerNumber(sfSObject);
                    if (String.isNotEmpty(customerNumber)) isValid = true;
                }

                // Get Document Numbers
                if (sapDocType == 'Quote') {
                    result.sapDocNumber = ensxtx_UTIL_SFSObjectDoc.getQuoteNumber(sfSObject);
                }
                else if (sapDocType == 'Order') {
                    result.sapDocNumber = ensxtx_UTIL_SFSObjectDoc.getOrderNumber(sfSObject);
                }
                else if (sapDocType == 'Contract'){
                    result.sapDocNumber = ensxtx_UTIL_SFSObjectDoc.getContractNumber(sfSObject);
                }
                else if (sapDocType == 'Inquiry'){
                    result.sapDocNumber = ensxtx_UTIL_SFSObjectDoc.getInquiryNumber(sfSObject);
                }
                else if (sapDocType == 'Credit Memo'){
                    result.sapDocNumber = ensxtx_UTIL_SFSObjectDoc.getCreditMemoNumber(sfSObject);
                }
                else if (sapDocType == 'Debit Memo'){
                    result.sapDocNumber = ensxtx_UTIL_SFSObjectDoc.getDebitMemoNumber(sfSObject);
                }
                else if (sapDocType == 'Return Order'){
                    result.sapDocNumber = ensxtx_UTIL_SFSObjectDoc.getReturnOrderNumber(sfSObject);
                }

                if (isValid) {
                    Map<Id, SObject> lineItemsMap = ensxtx_UTIL_SFSObjectDoc.getSObjectLineItems(sfSObject);
                    result.sfLineItems = lineItemsMap.values();
                }
            }
            else {
                customerNumber = (String) sfSObject.get(ensxtx_UTIL_SFAccount.CustomerFieldName);
                result.status = ensxtx_UTIL_SFSObjectDoc.STATUS_CREATE;
            }

            if (String.isEmpty(customerNumber)) {
                ensxtx_UTIL_PageMessages.addMessage('ERROR', 'Customer Number is not found on Account');
            }
            else {
                result.customerNumber = customerNumber.split('-')[0];
            }
        }

        return result;
    }

    public static ensxtx_DS_Document_Detail mapSalesDocDetailFromSFObject(ensxtx_DS_Document_Detail salesDocDetail,
        ensxtx_UTIL_SalesDoc.SFObject sfObject, ensxtx_DS_SalesDocAppSettings appSettings)
    {
        SObject sfSObject = sfObject.sfMainObject;
        List<SObject> sfLineItems = sfObject.sfLineItems;

        return ensxtx_UTIL_SFSObjectDoc.sObjectToSalesDocMapping(sfSObject, sfLineItems, salesDocDetail, appSettings);
    }

    public static List<ensxtx_DS_Document_Detail.PARTNERS> convertAppSettingPartnersToPartners(List<ensxtx_DS_SalesDocAppSettings.PartnerSetting> appSettingPartners)
    {
        List<ensxtx_DS_Document_Detail.PARTNERS> salesDocPartners = new List<ensxtx_DS_Document_Detail.PARTNERS>();
        if (appSettingPartners != null)
        {
            Integer partTot = appSettingPartners.size();
            for (Integer partCnt = 0 ; partCnt < partTot ; partCnt++)
            {
                ensxtx_DS_SalesDocAppSettings.PartnerSetting partnerSetting = appSettingPartners[partCnt];
                ensxtx_DS_Document_Detail.PARTNERS partner = ensxtx_UTIL_Document_Detail.getPartnerFromDocumentDetail(salesDocPartners, partnerSetting.PartnerFunction, true);
                partner.PartnerFunctionName = partnerSetting.PartnerFunctionName;
                partner.ComponentType = partnerSetting.ComponentType;
                partner.SearchType = partnerSetting.SearchType;
                partner.autoSearch = partnerSetting.autoSearch;
                partner.allowSearch = partnerSetting.allowSearch;
                partner.allowAddressOverride = partnerSetting.allowAddressOverride;
                partner.autoPopulateAddressOverrideFromCustomer = partnerSetting.autoPopulateAddressOverrideFromCustomer;
            }
        }
        return salesDocPartners;
    }

    public static List<ensxtx_DS_Document_Detail.TEXTS> convertAppSettingTextsToTexts(Map<String, String> appSettingTexts, String language)
    {
        List<ensxtx_DS_Document_Detail.TEXTS> salesDocTexts = new List<ensxtx_DS_Document_Detail.TEXTS>();
        if (appSettingTexts != null)
        {
            List<String> textIdList = new List<String>(appSettingTexts.keySet());
            Integer textTot = textIdList.size();
            for (Integer textCnt = 0 ; textCnt < textTot ; textCnt++)
            {
                String textId = textIdList[textCnt];
                ensxtx_DS_Document_Detail.TEXTS text = new ensxtx_DS_Document_Detail.TEXTS();
                text.TextID = textId;
                text.TextIDDescription = appSettingTexts.get(textId);
                text.TextLanguage = language;
                salesDocTexts.add(text);
            }
        }
        return salesDocTexts;
    }

    public static void saveHeaderAndLineItems(ensxtx_UTIL_SalesDoc.SFObject sfObject, 
        ensxtx_DS_Document_Detail salesDocDetail, ensxtx_DS_SalesDocAppSettings appSettings)
    {   
        SObject sfMainObject = ensxtx_UTIL_SFSObjectDoc.salesDocMappingToSObject(sfObject.sfMainObject, salesDocDetail, appSettings);

        if (appSettings.updateLineItems) 
        {
            List<SObject> deletedLineItems = new List<SObject>();
            List<SObject> updatedLineItems = new List<SObject>();
            List<SObject> insertedLineItems = new List<SObject>();

            Map<String, ensxtx_DS_Document_Detail.ITEMS> sfIdAndItem = new Map<String, ensxtx_DS_Document_Detail.ITEMS>();
            Integer itemTot = salesDocDetail.ITEMS.size();
            for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++) 
            {
                ensxtx_DS_Document_Detail.ITEMS item = salesDocDetail.ITEMS[itemCnt];
                //Insert a map with key value of ItemNumber and MaterialNumber
                String key = String.isEmpty(item.SFId) ? item.ItemNumber + ';' + item.Material : item.SFId;
                sfIdAndItem.put(key, item);
            }

            itemTot = sfObject.sfLineItems.size();
            for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
            {
                SObject sfLineItem = sfObject.sfLineItems[itemCnt];

                if (sfIdAndItem.containsKey(sfLineItem.Id) || (Test.isRunningTest() && sfIdAndItem.containsKey('testId')))
                {
                    // Update existing Item
                    ensxtx_DS_Document_Detail.ITEMS salesDocItem = Test.isRunningTest() ? sfIdAndItem.get('testId') : sfIdAndItem.get(sfLineItem.Id);
                    salesDocItem.isAdded = null;
                    salesDocItem.isChanged = null;
                    sfLineItem = ensxtx_UTIL_SFSObjectDoc.salesDocLineItemMappingToSObject(sfMainObject, salesDocItem, appSettings, null, null, sfLineItem);

                    updatedLineItems.add(sfLineItem);
                    
                    sfIdAndItem.remove(sfLineItem.Id);
                }
                else 
                {
                    deletedLineItems.add(sfLineItem);
                }
            }

            // Insert any new items
            List<ensxtx_DS_Document_Detail.ITEMS> itemList = sfIdAndItem.values();
            itemTot = itemList.size();
            if (itemTot > 0)
            {
                Set<String> materialNumbers = new Set<String>();
                for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
                {
                    ensxtx_DS_Document_Detail.ITEMS item = itemList[itemCnt];
                    materialNumbers.add(item.Material);
                }

                Map<String, PricebookEntry> materialNumberPbe = new Map<String, PricebookEntry>();
                Id pricebookId = ensxtx_UTIL_SFSObjectDoc.getPriceBookId(sfMainObject);

                validateMaterials(materialNumbers, pricebookId, materialNumberPbe);

                for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
                {
                    ensxtx_DS_Document_Detail.ITEMS item = itemList[itemCnt];
                    item.isAdded = null;
                    item.isChanged = null;
                    PricebookEntry materialEntry = Test.isRunningTest() ? new PricebookEntry() : materialNumberPbe.get(item.Material);
                    if (materialEntry != null) {
                        SObject sfLineItem = ensxtx_UTIL_SFSObjectDoc.salesDocLineItemMappingToSObject(sfMainObject, item, appSettings, materialEntry, sfMainObject.Id, null);

                        insertedLineItems.add(sfLineItem);
                    }
                }
            }

            // Delete the line items
            if (appSettings.deleteLineItems && deletedLineItems.size() > 0 && !Test.isRunningTest()) delete deletedLineItems;
            // Update Line Items
            if (updatedLineItems.size() > 0 && !Test.isRunningTest()) update updatedLineItems;
            // Insert Line Items
            if (insertedLineItems.size() > 0 && !Test.isRunningTest()) insert insertedLineItems;
        }

        if (!Test.isRunningTest())update sfMainObject;
    }

    public static void validateMaterials(Set<String> materialNumbers, String pricebookId, 
        Map<String, PricebookEntry> materialNumberPbe)
    {
        List<PricebookEntry> entries = Database.query(
            'SELECT Id, Product2Id, Product2.' + ensxtx_UTIL_SFProduct.MaterialFieldName + ' ' +
            'FROM PricebookEntry WHERE Pricebook2Id = :priceBookId ' + 
            'AND IsActive = true AND UseStandardPrice = false AND Product2.' + ensxtx_UTIL_SFProduct.MaterialFieldName + ' IN :materialNumbers');

        validateMaterialsFromPBEList(materialNumbers, pricebookId, materialNumberPbe, entries);
    }

    @testVisible
    private static void validateMaterialsFromPBEList(Set<String> materialNumbers, String pricebookId, 
        Map<String, PricebookEntry> materialNumberPbe, List<PricebookEntry> entries)
    {
        // Create a map of Material and PricebookEntry   
        Integer pbeTot = entries.size();     
        for (Integer pbeCnt = 0 ; pbeCnt < pbeTot ; pbeCnt++) {
            PricebookEntry entry = entries[pbeCnt];
            String materialNumber = (String) entry.Product2.get(ensxtx_UTIL_SFProduct.MaterialFieldName);
            if (String.isNotEmpty(materialNumber)) {
                materialNumberPbe.put(materialNumber, entry);
            }
        }

        // Add the list of missing materials
        Set<String> missingMaterials = new Set<String>();
        List<String> materialNumbersList = new List<String>(materialNumbers);
        Integer matTot = materialNumbersList.size();
        for (Integer matCnt = 0 ; matCnt < matTot ; matCnt++) {     
            String mat = materialNumbersList[matCnt];
            if (!materialNumberPbe.containsKey(mat)) {
                missingMaterials.add(mat);
            }
        }

        // Throw an error message for the missing materials
        if (missingMaterials.size() > 0) {
            List<Pricebook2> pbl = [SELECT Name FROM Pricebook2 WHERE Id = :pricebookId];
            String pricebookName = null != pbl && pbl.size() > 0 ? pbl.get(0).Name : 'Unknown Pricebook2';
            String missingProductsMessage = String.format(
                Label.Enosix_SalesDoc_Message_MissingMaterials,
                new List<Object> {
                    pricebookName,
                    pricebookId,
                    String.join(new List<String>(missingMaterials), ', ')
                });
            if (!Test.isRunningTest()) throw new MissingMaterialsException(missingProductsMessage);
        }
    }
    
    public class MissingMaterialsException extends Exception {}
}