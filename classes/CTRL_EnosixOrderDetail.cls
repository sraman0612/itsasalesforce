public with sharing class CTRL_EnosixOrderDetail {
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(CTRL_EnosixOrderDetail.class);

    @AuraEnabled
    public static UTIL_Aura.Response getOrderDetail(String SalesDocument) {
        SBO_EnosixSO_Detail sbo = new SBO_EnosixSO_Detail();

        SBO_EnosixSO_Detail.EnosixSO orderDetail = sbo.getDetail(SalesDocument);

        System.debug('orderDetail=' + orderDetail);

        System.debug('orderDetail.SalesDocument=' + orderDetail.SalesDocument);

        return UTIL_Aura.createResponse(orderDetail);
    }

    @AuraEnabled
    public static UTIL_Aura.Response getDocumentFlow(String SalesDocument) {
        SBO_EnosixSalesDocFlow_Search sbo = new SBO_EnosixSalesDocFlow_Search();
        SBO_EnosixSalesDocFlow_Search.EnosixSalesDocFlow_SC searchContext = 
            new SBO_EnosixSalesDocFlow_Search.EnosixSalesDocFlow_SC();

        if (null != SalesDocument)
        {
            searchContext.SEARCHPARAMS.SalesDocument = SalesDocument;
        }

        //searchContext.SEARCHPARAMS.DocumentCategory = docCategory;
        searchContext.SEARCHPARAMS.X_PrecedingDocuments = true;
        searchContext.SEARCHPARAMS.X_SubsequentDocuments = true;

        searchContext = sbo.search(searchContext);

        SBO_EnosixSalesDocFlow_Search.EnosixSalesDocFlow_SR result = searchContext.result;

        if (!result.isSuccess()) {
            UTIL_PageMessages.addFrameworkMessages(searchContext.getMessages());

            return UTIL_Aura.createResponse(null);
        }

        return UTIL_Aura.createResponse(result.getResults());
    }

    @AuraEnabled
    public static UTIL_Aura.Response downloadInvoice(String SalesDocument) {
        logger.enterAura('downloadInvoice', new Map < String, Object > {
            'SalesDocument' => SalesDocument
        });

        Object responseData = null;

        try
        {
            SBO_EnosixSalesDocOutput_Search searchSBO = new SBO_EnosixSalesDocOutput_Search();
            SBO_EnosixSalesDocOutput_Search.EnosixSalesDocOutput_SC context = UTIL_EnosixOutput_Search.getInvoiceSearchContext(SalesDocument);

            context = searchSBO.Search(context);
            responseData = context.result.SearchResults.get(0);

            if (null == responseData)
            {
                UTIL_PageMessages.addMessage(UTIL_PageMessages.ERROR, 'No Results were found');
            }

            UTIL_PageMessages.addFrameworkMessages(context.getMessages());

        } catch (Exception ex) {
            UTIL_PageMessages.addExceptionMessage(ex);
        } finally {
            logger.exit();
        }

        return UTIL_Aura.createResponse(responseData);
    }

    @AuraEnabled
    public static UTIL_Aura.Response trackDelivery(String SalesDocument) {
        logger.enterAura('trackDelivery', new Map<String, Object> {
            'SalesDocument' => SalesDocument
        });

        SBO_SFCIDelivery_Detail sbo = new SBO_SFCIDelivery_Detail();
        SBO_SFCIDelivery_Detail.SFCIDelivery result = new SBO_SFCIDelivery_Detail.SFCIDelivery();
        List<SBO_SFCIDelivery_Detail.PARTNERS> partners = new List<SBO_SFCIDelivery_Detail.PARTNERS>();
        DELIVERY_SPECIFIC_INFO retVal = new DELIVERY_SPECIFIC_INFO();

        try {
            result = sbo.getDetail(SalesDocument);
            partners = result.PARTNERS.getAsList();
            retVal.Delivery = result.Delivery;
            retVal.DeliveryDate = result.DeliveryDate;
            retVal.DeliveryStatus = result.DeliveryStatus;
            retVal.TrackingNumber = result.BillofLading;

            for (SBO_SFCIDelivery_Detail.PARTNERS ptnr : partners) {
                //system.debug('Function, FunctionName, Vendor and Name ====> '+ptnr.PartnerFunction+', '+ptnr.PartnerFunctionName+', '+ptnr.Vendor+' and '+ptnr.PartnerName);
                if (ptnr.PartnerFunction == 'CR' && ptnr.PartnerFunctionName == 'Carrier') {
                    retVal.Vendor = ptnr.Vendor;
                    retVal.VendorName = ptnr.PartnerName;
                }
            }

            Map<String,GDI_Delivery_Vendors__c> urlMap = new Map<String,GDI_Delivery_Vendors__c>();
            for (GDI_Delivery_Vendors__c vend : [SELECT FLD_Tracking_URL__c,FLD_Tracking_URL_Label__c,FLD_Vendor_Code__c,
                                                FLD_Vendor_Description__c,FLD_Vendor_Num__c,Id,Name FROM GDI_Delivery_Vendors__c]) {
                System.debug('vend.FLD_Vendor_Num__c=' + vend.FLD_Vendor_Num__c);
                if (vend.FLD_Vendor_Num__c != null) urlMap.put(vend.FLD_Vendor_Num__c,vend);
            }
            
            System.debug('retVal.Vendor=' + retVal.Vendor);
            System.debug('urlMap.size()=' + urlMap.size());

            if (urlMap.containsKey(retVal.Vendor)) {
                System.debug('found one ' + retVal.Vendor);
                retVal.TrackingURL = urlMap.get(retVal.Vendor).FLD_Tracking_URL__c + retVal.TrackingNumber;
                retVal.URL_Label = urlMap.get(retVal.Vendor).FLD_Tracking_URL_Label__c;
            }

        } catch (Exception e) {
            UTIL_PageMessages.addExceptionMessage(e);
        } finally {
            logger.exit();
        }

        return UTIL_Aura.createResponse(JSON.serialize(retVal));
    }

    public class DELIVERY_SPECIFIC_INFO {
        public String Delivery {get;set;}
        public Date DeliveryDate {get;set;}
        public String DeliveryStatus {get;set;}
        public String Vendor {get;set;}
        public String VendorName {get;set;}
        public String TrackingNumber {get;set;}
        public String TrackingURL {get;set;}
        public String URL_Label {get;set;}
    }
}