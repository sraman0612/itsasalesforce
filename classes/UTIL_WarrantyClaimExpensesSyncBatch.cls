/*
Paste the below code into the Anonymous Apex Window
To know how many total records are there to sync
========== START CODE ==========
new ensxsdk.Logger(null);

SBO_EnosixSOSync_Search.EnosixSOSync_SC searchContext = new SBO_EnosixSOSync_Search.EnosixSOSync_SC();
searchContext.pagingOptions.pageSize = 1;
searchContext.pagingOptions.pageNumber = 1;
searchContext.SEARCHPARAMS.DateFrom = Date.parse('01/01/2000');
searchContext.SEARCHPARAMS.InitialLoad = false;
SBO_EnosixSOSync_Search.DOC_TYPE docType = new SBO_EnosixSOSync_Search.DOC_TYPE();
docType.SalesDocumentType = 'ZIDW';
searchContext.DOC_TYPE.add(docType);

SBO_EnosixSOSync_Search sbo = new SBO_EnosixSOSync_Search();
sbo.search(searchContext);
System.debug(searchContext.result.isSuccess());
System.debug(searchContext.pagingOptions.totalRecords);
========== END CODE ==========
*/
public with sharing class UTIL_WarrantyClaimExpensesSyncBatch
    implements Database.Batchable<Object>,
    Database.AllowsCallouts,
    Database.Stateful,
    I_ParameterizedSync
{
    @TestVisible
    private static String SFSyncKeyField = 'SAP_Item_Number__c';
    @TestVisible
    private static String BatchClassName = 'UTIL_WarrantyClaimExpensesSyncBatch';
    private static String ScheduleClassName = 'UTIL_WarrantyClaimExpensesSyncSchedule';

    private static final ensxsdk.Logger logger = new ensxsdk.Logger(UTIL_WarrantyClaimExpensesSyncBatch.class);
    public void logCallouts(String location)
    {
        if ((Boolean)UTIL_AppSettings.getValue(BatchClassName + '.Logging', false))
        {
            logger.enterVfpConstructor(location, null);
        }
    }

    // jobInfo contains debug information persisted across contexts since start(),
    // execute(), and finish() all run in separate contexts with separate debug logs
    private List<String> jobInfo = new List<String>();

    // In this case, we will store the largest change date/time as the param
    private UTIL_SyncHelper.LastSync fromLastSync = new UTIL_SyncHelper.LastSync();
    private static String ObjectType = 'Warranty_Claim_Expenses__c';

    private Set<String> SAPSalesOrderNumbers = new Set<String>();
    private Map<String,Warranty_Claim__c> currentWarrantyClaimsMap = new Map<String,Warranty_Claim__c>();
    private Map<String,Boolean> currentWarrantyClaimsRejectedMap = new Map<String,Boolean>();


    /* I_ParameterizedSync methods - setBatchParam() */
    public void setBatchParam(Object value)
    {
        this.fromLastSync = (UTIL_SyncHelper.LastSync) value;
    }
    /* end I_ParameterizedSync methods */

    // Sync Filter collections
    // Get the filters from the AppSettings

    // End Sync filter collections

    /* Database.Batchable methods start(), execute(), and finish() */
    // start()
    //
    // Calls SBO and returns search results of update materials
    public List<Object> start(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.start');
        System.debug(context.getJobId() + ' started');

        SBO_EnosixSOSync_Search sbo = new SBO_EnosixSOSync_Search();
        SBO_EnosixSOSync_Search.EnosixSOSync_SC searchContext = new SBO_EnosixSOSync_Search.EnosixSOSync_SC();

        SBO_EnosixSOSync_Search.DOC_TYPE docType = new SBO_EnosixSOSync_Search.DOC_TYPE();
        docType.SalesDocumentType = 'ZW1';
        searchContext.DOC_TYPE.add(docType);

        this.fromLastSync = UTIL_SyncHelper.getLastSyncFromTable(
            ScheduleClassName,
            this.fromLastSync);

        this.fromLastSync.pageNumber = this.fromLastSync.pageNumber + 1;

        if (this.fromLastSync.retryCnt == -1)
        {
            UTIL_SyncHelper.resetPage(this.fromLastSync, (Integer) UTIL_AppSettings.getValue(
                BatchClassName + '.SAPPageSize',
                9999));
        }
        if (this.fromLastSync.lastSyncDate != null)
        {
            searchContext.SEARCHPARAMS.DateFrom = this.fromLastSync.lastSyncDate;
        }
        if (this.fromLastSync.lastSyncDate == null)
        {
            searchContext.SEARCHPARAMS.InitialLoad = true;
        }

        searchContext.pagingOptions.pageSize = this.fromLastSync.pageSize;
        searchContext.pagingOptions.pageNumber = this.fromLastSync.pageNumber;
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());
        System.debug('fromLastSync:' + this.fromLastSync.toString());


        // Execute the search
        SBO_EnosixSOSync_Search.EnosixSOSync_SR result;
        try
        {
            sbo.search(searchContext);
            result = searchContext.result;
        }
        catch (Exception ex)
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, ex, this.jobInfo);
        }

        // Write any response messages to the debug log
        String errorMessage = UTIL_SyncHelper.buildErrorMessage(BatchClassName, result.getMessages());

        if (!result.isSuccess())
        {
            return UTIL_SyncHelper.checkRetry(this.fromLastSync, new UTIL_SyncHelper.SyncException(errorMessage), this.jobInfo);
        }

        List<Object> searchResults = result.getResults();
        System.debug('Result size: ' + searchResults.size());

        // let finish() know to queue up another instance
        this.fromLastSync.isAnotherBatchNeeded = searchResults.size() > 0;
        this.fromLastSync.retryCnt = -1;

        this.jobInfo.add('searchResultsSize:' + searchResults.size());
        this.jobInfo.add('fromLastSync:' + this.fromLastSync.toString());

        return searchResults;
    }

    // execute()
    //
    // Given the updated search results, does the work of updating the object table.
    public void execute(
        Database.BatchableContext context,
        List<Object> searchResults)
    {
        logCallouts(BatchClassName + '.execute');
        System.debug(context.getJobId() + ' executing');

        if (null == searchResults || 0 == searchResults.size()) return;

        List<SObject> errors = new List<SObject>();
        Map<String, Object> searchResultMap = createObjectKeyMap(searchResults);

        // Get all the SalesDocument numbers from the searchResults
        for (Object searchResult : searchResults)
        {
            String key = getSboKey(searchResult);
            if (String.isNotEmpty(key))
            {
                String orderNum = key.substringBefore('-');
                SAPSalesOrderNumbers.add(orderNum);
            }
        }
        // Load currentWarrantyClaimsMap collection with existing records
        Map<Id, Warranty_Claim__c> currentWarrantyClaimsIdMap = new Map<Id, Warranty_Claim__c>();
        for (Warranty_Claim__c claim : [SELECT Id, SAP_Service_Notification_Number__c, SAP_Sales_Order_Number__c, SAP_Warranty_Claim_Text__c FROM Warranty_Claim__c WHERE SAP_Sales_Order_Number__c IN : SAPSalesOrderNumbers]) {
            currentWarrantyClaimsMap.put(claim.SAP_Sales_Order_Number__c, claim);
            currentWarrantyClaimsRejectedMap.put(claim.SAP_Sales_Order_Number__c, null);
            currentWarrantyClaimsIdMap.put(claim.Id, claim);
        }

        // First, update matching existing objects
        List<String> selectedFieldsList = new List<String>();
        selectedFieldsList.add('SAP_Item_Number__c');
        List<SObject> currentObjectList = UTIL_SyncHelper.getCurrentObjects(ObjectType, 'Warranty_Claim__r.SAP_Sales_Order_Number__c', SAPSalesOrderNumbers, selectedFieldsList);
        for (SObject currentObject : currentObjectList) {
            Warranty_Claim_Expenses__c claimExpense = (Warranty_Claim_Expenses__c) currentObject;
            Warranty_Claim__c claim = currentWarrantyClaimsIdMap.get(claimExpense.Warranty_Claim__c);
            claimExpense.SAP_Sales_Order_Number__c = claim.SAP_Sales_Order_Number__c;
        }
        
        List<SObject> updateObjectList = updateExistingObjects(searchResultMap, currentObjectList, errors);
        List<SObject> insertObjectList = createNewObjects(searchResultMap, currentObjectList, errors);
        Set<Id> savedIdSet = new Set<Id>();

        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Update', errors, savedIdSet, updateObjectList, BatchClassName, 'Id');
        UTIL_SyncHelper.insertUpdateResults(ObjectType, 'Insert', errors, savedIdSet, insertObjectList, BatchClassName, 'SAP_Sales_Order_Number__c');

        for (String salesOrderNumber : currentWarrantyClaimsRejectedMap.keySet())
        {
            if (currentWarrantyClaimsRejectedMap.get(salesOrderNumber) != null && currentWarrantyClaimsRejectedMap.get(salesOrderNumber)) currentWarrantyClaimsMap.get(salesOrderNumber).Disposition_Status__c = 'Denied';
        }
        UTIL_SyncHelper.insertUpdateResults('Warranty_Claim__c', 'Update', errors, savedIdSet, currentWarrantyClaimsMap.values(), BatchClassName, 'SAP_Service_Notification_Number__c');

        UTIL_SyncHelper.insertUpdateResults('Error', 'Insert', errors, savedIdSet, errors, BatchClassName, null);
    }

    // finish()
    //
    // queues up another batch when isAnotherBatchNeeded is true
    public void finish(Database.BatchableContext context)
    {
        logCallouts(BatchClassName + '.finish');
        System.debug(context.getJobId() + ' finished');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);
        if (this.fromLastSync.retryCnt >= 0)
        {
            System.debug('Retry=' + this.fromLastSync.retryCnt + ' ' + System.Now());
        }

        UTIL_SyncFailureNotifier.syncFinished(context);

        UTIL_SyncHelper.launchAnotherBatchIfNeeded(
            this.fromLastSync.isAnotherBatchNeeded, ScheduleClassName, this.fromLastSync);

        Database.executeBatch(new UTIL_WarrantyClaimExpenseItemCleanup());
    }

    private SBO_EnosixSOSync_Search.SEARCHRESULT getSboResult(Object searchResult)
    {
        return (SBO_EnosixSOSync_Search.SEARCHRESULT) searchResult;
    }

    // createObjectKeyMap()
    //
    // create map of product key / search result.
    private Map<String, Object> createObjectKeyMap(
        List<Object> searchResults)
    {
        Map<String, Object> result =
            new Map<String, Object>();

        for (Object searchResult : searchResults)
        {
            String key = getSboKey(searchResult);
            if (String.isNotEmpty(key))
            {
                result.put(key, searchResult);
            }
        }

        return result;
    }

    private List<SObject> updateExistingObjects(
        Map<String, Object> searchResultMap,
        List<SObject> currentObjectList,
        List<SObject> errors)
    {
        List<SObject> updateObjectList = new List<SObject>();

        for (SObject currentObject : currentObjectList)
        {
            String syncKey = (String) currentObject.get('SAP_Sales_Order_Number__c') + '-' + (String) currentObject.get('SAP_Item_Number__c');
            if (searchResultMap.containsKey(syncKey)) {
                Object searchResult = searchResultMap.get(syncKey);

                // Updates fields and adds to objectList list for later commit
                syncObject(currentObject, searchResult, errors, updateObjectList);
                searchResultMap.remove(syncKey);
            }
        }

        System.debug('Existing Object Size: ' + updateObjectList.size());

        return updateObjectList;
    }

    private List<SObject> createNewObjects(
        Map<String, Object> searchResultMap,
        List<SObject> currentObjectList,
        List<SObject> errors)
    {
        List<SObject> insertObjectList = new List<SObject>();

        List<Object> srList = searchResultMap.values();
        Integer srTot = srList.size();
        for (Integer srCnt = 0 ; srCnt < srTot ; srCnt++)
        {
            Object searchResult = srList[srCnt];
            syncObject(null, searchResult, errors, insertObjectList);
        }

        System.debug('New Object Size: ' + insertObjectList.size());

        return insertObjectList;
    }

    private void syncObject(
        SObject currentObject,
        Object searchResult,
        List<SObject> errors,
        List<SObject> objectList)
    {
        SBO_EnosixSOSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        Warranty_Claim_Expenses__c claimExpense = null;
        if (currentObject == null) {
            claimExpense = new Warranty_Claim_Expenses__c();
            if (!currentWarrantyClaimsMap.containsKey(sboResult.SalesDocument)) return;
            claimExpense.Warranty_Claim__c = currentWarrantyClaimsMap.get(sboResult.SalesDocument).Id;
        } else claimExpense = (Warranty_Claim_Expenses__c) currentObject;
        claimExpense.SAP_Sales_Order_Number__c = sboResult.SalesDocument;
        claimExpense.SAP_Item_Number__c = sboResult.ItemNumber;
        claimExpense.SAP_Item_Description__c = sboResult.ItemDescription;
        claimExpense.SAP_Item_Rejection_Flag__c = sboResult.OrderItemRejectionFlag == 'X';
        claimExpense.SAP_Item_Rejection_Reason__c = sboResult.ReasonForRejection;
        claimExpense.SAP_Item_Rejection_Reason_Description__c = sboResult.ReasonForRejectionDescription;
        claimExpense.SAP_Material_Number__c = sboResult.Material;
        claimExpense.SAP_Net_Price__c = sboResult.NetItemPrice;
        claimExpense.SAP_Net_Value__c = sboResult.NetOrderItemValue;
        claimExpense.SAP_Order_Quantity__c = sboResult.OrderQuantity;
        String SAP_Warranty_Claim_Text = sboResult.SpecifyWarrantyConditions;

        if (currentWarrantyClaimsMap.containsKey(sboResult.SalesDocument)) {
            currentWarrantyClaimsMap.get(sboResult.SalesDocument).SAP_Warranty_Claim_Text__c = SAP_Warranty_Claim_Text;
            if (currentWarrantyClaimsRejectedMap.get(sboResult.SalesDocument) == null) currentWarrantyClaimsRejectedMap.put(sboResult.SalesDocument, true);
            if (!claimExpense.SAP_Item_Rejection_Flag__c) currentWarrantyClaimsRejectedMap.put(sboResult.SalesDocument, false);
            objectList.add(claimExpense);
        }
    }

    private String getSboKey(Object searchResult)
    {
        SBO_EnosixSOSync_Search.SEARCHRESULT sboResult = getSboResult(searchResult);
        return sboResult == null ? '' : sboResult.SalesDocument + '-' + sboResult.ItemNumber;
    }
}