public with sharing class CaseAccept {

    @AuraEnabled
    public static Case getCase(Id caseId){
        //make your SOQL here from your desired object where you want to place your lightning action
        return [SELECT Id, OwnerId, Status, Email_Waiting_Icon__c, GDI_Department__c FROM Case WHERE Id = :caseId];
    }

    @AuraEnabled
    public static Case updateCase(Case currCase){
        //you can make your update here.
        User currUser = [SELECT Id, Department_Formula__c FROM User WHERE Id = :UserInfo.getUserId()];
        system.debug('currUser ====> '+currUser);
        system.debug('currCase ====> '+currCase);

        //if (currCase != null) {
        currCase.OwnerId = currUser.Id;
        currCase.Status = 'Open';
        currCase.Email_Waiting_Icon__c = false;
        currCase.GDI_Department__c = currUser.Department_Formula__c;
        upsert currCase;
        //}
        return currCase;
    }

}