@isTest
public class ensxtx_TSTC_PartnerSearchLtng
{
    public class MOC_ensxtx_SBO_SFCIPartner_Search implements ensxsdk.EnosixFramework.SearchSBOSearchMock
    {
        public Boolean success = true;

        public void setSuccess(Boolean successfull)
        {
            this.success = successfull;
        }

        private boolean throwException = false;

        public void setThrowException(boolean throwException)
        {
            this.throwException = throwException;
        }

        public ensxsdk.EnosixFramework.SearchContext executeSearch(ensxsdk.EnosixFramework.SearchContext searchContext)
        {
            if (throwException) throw new CalloutException();

            ensxtx_SBO_SFCIPartner_Search.SFCIPartner_SR sr = new ensxtx_SBO_SFCIPartner_Search.SFCIPartner_SR();
            ensxtx_SBO_SFCIPartner_Search.SEARCHRESULT result = new ensxtx_SBO_SFCIPartner_Search.SEARCHRESULT();
            result.PartnerNumber = '1234';
            result.VendorNumber = '1234';
            result.PartnerName = 'Bob';
            result.Street = 'Angel Grove St';
            result.City = 'New York';
            result.PostalCode = '4566';
            result.Region = 'New York';
            result.Country = 'USA';
            sr.SearchResults.add(result);
            sr.setSuccess(this.success);
            searchContext.baseResult = sr;
			return searchContext;
        }
    }

    @isTest
    public static void test_searchPartners()
    {
        MOC_ensxtx_SBO_SFCIPartner_Search mocSboEnosixPartnerSearch = new MOC_ensxtx_SBO_SFCIPartner_Search();
        ensxsdk.EnosixFramework.setMock(ensxtx_SBO_SFCIPartner_Search.class, mocSboEnosixPartnerSearch);

        String customerNumber = '111';
        String partnerFunction = 'CR';
        String salesOrg = '1000';
        String distChannel = '10';
        String division = '00';
        Map<String, Object> pagingOptions = new Map<String, Object>();

        ensxtx_UTIL_Aura.Response response = ensxtx_CTRL_PartnerSearchLtng.searchPartners(customerNumber, partnerFunction, salesOrg, distChannel, division, 'ADD', new List<String>{'1234'}, pagingOptions);
        mocSboEnosixPartnerSearch.setThrowException(true);
        response = ensxtx_CTRL_PartnerSearchLtng.searchPartners(customerNumber, partnerFunction, salesOrg, distChannel, division, 'ADD', new List<String>{'1234'}, pagingOptions);
    }
}