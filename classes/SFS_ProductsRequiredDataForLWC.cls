public class SFS_ProductsRequiredDataForLWC {
    public class TableColumns {
        @AuraEnabled
        public Id rowId {get;set;}
        @AuraEnabled public Id productId {get;set;}
        @AuraEnabled public String productName {get;set;}
        @AuraEnabled public Integer quantity {get;set;}
        @AuraEnabled public String consumableDate {get;set;}
        @AuraEnabled public Boolean active {get;set;}
        @AuraEnabled public Boolean oracle {get;set;}
        @AuraEnabled public Integer qtyNextvisit {get;set;}
        @AuraEnabled public Id deleteAfterUseRecId{get;set;}
        @AuraEnabled public String workScope {get;set;}
        @AuraEnabled public Boolean partRequired {get;set;}
        @AuraEnabled public Boolean workScope0 {get;set;}
        @AuraEnabled public Boolean workScope1 {get;set;}
        @AuraEnabled public Boolean workScope2 {get;set;}
        @AuraEnabled public Boolean workScope4 {get;set;}
        @AuraEnabled public Boolean workScope6 {get;set;}
        @AuraEnabled public Boolean workScope8 {get;set;}
        @AuraEnabled public Boolean workScope12 {get;set;}
        @AuraEnabled public Boolean workScope16 {get;set;}
        @AuraEnabled public Boolean workScope24 {get;set;}
        @AuraEnabled public Boolean workScope48 {get;set;}
        @AuraEnabled public Boolean addedtoList {get;set;}
        @AuraEnabled public Boolean removeFromTable {get;set;}
        @AuraEnabled public String productDescription {get;set;}
        public TableColumns(){

        }
    }
    @AuraEnabled(cacheable=true)
    public static List<TableColumns> getproductsRequired(String assetId){
        try{
            List<TableColumns> partsRequiredColumns=new List<TableColumns>();
            List<TableColumns> productsRequiredColumns=new List<TableColumns>();
            List<TableColumns> finalData=new List<TableColumns>();
            Asset assetRecord=[Select Id,Name,CTS_Frame_Type__c,
                               (Select Id,Product__c,Product__r.name,Product__r.description,Product__r.Part_Number__c,Product__r.isActive,Product__r.SFS_Oracle_Orderable__c,SFS_Quantity__c,SFS_0k__c, SFS_1k__c, SFS_2k__c, 
                               SFS_4k__c, SFS_6k__c, SFS_8k__c, SFS_12k__c, SFS_16k__c, SFS_24k__c, SFS_48k__c, Part_Number__c,SFS_Consumable_Last_Changed__c,LastModifiedDate,CreatedDate,SFS_Last_Changed_Date__c from CAP_IR_Asset_Product_Required__r 
                               where SFS_Delete_After_Use__c=false) from Asset where Id=:assetId];
            Asset assetWithDeleteAfterUse=[Select Id,Name,CTS_Frame_Type__c,
                               (Select Id,Product__c,Product__r.name,Product__r.description,Product__r.Part_Number__c,Product__r.isActive,Product__r.SFS_Oracle_Orderable__c,SFS_Quantity__c,SFS_0k__c, SFS_1k__c, SFS_2k__c, 
                               SFS_4k__c, SFS_6k__c, SFS_8k__c, SFS_12k__c, SFS_16k__c, SFS_24k__c, SFS_48k__c,Part_Number__c,SFS_Consumable_Last_Changed__c,LastModifiedDate,CreatedDate,SFS_Last_Changed_Date__c from CAP_IR_Asset_Product_Required__r 
                               where SFS_Delete_After_Use__c=true) from Asset where Id=:assetId];
            //creating wrapper data from the asset parts required           
            For(CAP_IR_Asset_Part_Required__c partRequired:assetRecord.CAP_IR_Asset_Product_Required__r){
                Boolean deleteAfterUseExists=false;
                Id deleteAfterUseRecId;
                Integer quantityNextVisit;
                if(assetWithDeleteAfterUse.CAP_IR_Asset_Product_Required__r.size()>0){
                    for(CAP_IR_Asset_Part_Required__c record:assetWithDeleteAfterUse.CAP_IR_Asset_Product_Required__r){
                        if(record.Product__c == partRequired.Product__c){                            
                            deleteAfterUseExists=true;
                            deleteAfterUseRecId=record.Id;
                            quantityNextVisit=Integer.valueOf(record.SFS_Quantity__c);
                        }
                    }
                }
             
                DateTime  dt;
                String dateOutput ;
                if(partRequired.SFS_Last_Changed_Date__c!=null){
                    dt= partRequired.SFS_Last_Changed_Date__c;
                    dateOutput = dt.format('MM-dd-yyyy');
                }

                        Boolean oracleOrder=true;
                        if(partRequired.Product__r.SFS_Oracle_Orderable__c!='Y'){
                            oracleOrder=false;

                        }
                       
                if(deleteAfterUseExists==true){                       
                        TableColumns newRow=new TableColumns();
                        newRow.rowId=partRequired.Id;
                        newRow.productName=partRequired.Product__r.Part_Number__c;
                        newRow.productId=partRequired.Product__c;
                        newRow.quantity=Integer.valueOf(partRequired.SFS_Quantity__c);
                        newRow.active=partRequired.Product__r.isActive;
                        newRow.oracle=oracleOrder;
                        newRow.consumableDate=dateOutput;
                        newRow.partRequired=true;
                        newRow.qtyNextvisit=QuantityNextVisit;
                        newRow.deleteAfterUseRecId=deleteAfterUseRecId;
                        newRow.removeFromTable=null;
                        newRow.productDescription=partRequired.Product__r.name;
                        newRow.workScope0=partRequired.SFS_0k__c;
                        newRow.workScope1=partRequired.SFS_1k__c;
                        newRow.workScope2=partRequired.SFS_2k__c;
                        newRow.workScope4=partRequired.SFS_4k__c;
                        newRow.workScope6=partRequired.SFS_6k__c;
                        newRow.workScope8=partRequired.SFS_8k__c;
                        newRow.workScope12=partRequired.SFS_12k__c;
                        newRow.workScope16=partRequired.SFS_16k__c;
                        newRow.workScope24=partRequired.SFS_24k__c;
                        newRow.workScope48=partRequired.SFS_48k__c;
                        partsRequiredColumns.add(newRow);
                    }else{
                        TableColumns newRow=new TableColumns();
                        newRow.rowId=partRequired.Id;
                        newRow.productName=partRequired.Product__r.Part_Number__c;
                        newRow.productId=partRequired.Product__c;
                        newRow.quantity=Integer.valueOf(partRequired.SFS_Quantity__c);
                        newRow.consumableDate=dateOutput; 
                        newRow.active=partRequired.Product__r.isActive;
                        newRow.oracle=oracleOrder;
                        newRow.partRequired=true;
                        newRow.qtyNextvisit=null;
                        newRow.deleteAfterUseRecId=null;
                        newRow.removeFromTable=null;
                        newRow.productDescription=partRequired.Product__r.name;
                        newRow.workScope0=partRequired.SFS_0k__c;
                        newRow.workScope1=partRequired.SFS_1k__c;
                        newRow.workScope2=partRequired.SFS_2k__c;
                        newRow.workScope4=partRequired.SFS_4k__c;
                        newRow.workScope6=partRequired.SFS_6k__c;
                        newRow.workScope8=partRequired.SFS_8k__c;
                        newRow.workScope12=partRequired.SFS_12k__c;
                        newRow.workScope16=partRequired.SFS_16k__c;
                        newRow.workScope24=partRequired.SFS_24k__c;
                        newRow.workScope48=partRequired.SFS_48k__c;
                        partsRequiredColumns.add(newRow);
                }
            }
                return partsRequiredColumns;
        }Catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }
    public class updateRecWrapper{
        @Auraenabled public Id recId{get;set;}
        @Auraenabled public String workScope{get;set;}
        @Auraenabled public Integer quantityNextVisit{get;set;}
        @Auraenabled public Boolean updateWorkScope{get;set;}
        @Auraenabled public Boolean updateQuantity{get;set;}
        @Auraenabled public Boolean workScope0{get;set;}
        @Auraenabled public Boolean workScope1{get;set;}
        @Auraenabled public Boolean workScope2{get;set;}
        @Auraenabled public Boolean workScope4{get;set;}
        @Auraenabled public Boolean workScope6{get;set;}
        @Auraenabled public Boolean workScope8{get;set;}
        @Auraenabled public Boolean workScope12{get;set;}
        @Auraenabled public Boolean workScope16{get;set;}
        @Auraenabled public Boolean workScope24{get;set;}
        @Auraenabled public Boolean workScope48{get;set;}
    }
    @AuraEnabled
    public static void updateRecords(String wrapperText){
        try {          
            List<updateRecWrapper> listToSave = (List<updateRecWrapper>)JSON.deserialize(wrapperText, List<updateRecWrapper>.class);
            List<CAP_IR_Asset_Part_Required__c> updatedList=new List<CAP_IR_Asset_Part_Required__c>();           
            for(updateRecWrapper record:listToSave){
                    CAP_IR_Asset_Part_Required__c newRecord=new CAP_IR_Asset_Part_Required__c();
                    newRecord.id=record.recId;
                    if(record.updateWorkScope==true){
                        newRecord.SFS_0k__c=record.workScope0;
                        newRecord.SFS_1k__c=record.workScope1;
                        newRecord.SFS_2k__c=record.workScope2;
                        newRecord.SFS_4k__c=record.workScope4;
                        newRecord.SFS_6k__c=record.workScope6;
                        newRecord.SFS_8k__c=record.workScope8;
                        newRecord.SFS_12k__c=record.workScope12;
                        newRecord.SFS_16k__c=record.workScope16;
                        newRecord.SFS_24k__c=record.workScope24;
                        newRecord.SFS_48k__c=record.workScope48;
                    }
                    if(record.updateQuantity==true){
                        newRecord.SFS_Quantity__c=record.quantityNextVisit;
                    }
                    updatedList.add(newRecord);
            }
            update updatedList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    public class createRecordsWrapper{
        @Auraenabled public String workScope{get;set;}
        @Auraenabled public Integer quantity{get;set;}
        @Auraenabled public Id productRecId{get;set;}
        @Auraenabled public Id assetId{get;set;}
        @Auraenabled public Boolean deleteAfterUse{get;set;}
        @Auraenabled public Boolean workScope0{get;set;}
        @Auraenabled public Boolean workScope1{get;set;}
        @Auraenabled public Boolean workScope2{get;set;}
        @Auraenabled public Boolean workScope4{get;set;}
        @Auraenabled public Boolean workScope6{get;set;}
        @Auraenabled public Boolean workScope8{get;set;}
        @Auraenabled public Boolean workScope12{get;set;}
        @Auraenabled public Boolean workScope16{get;set;}
        @Auraenabled public Boolean workScope24{get;set;}
        @Auraenabled public Boolean workScope48{get;set;}
    }
    @AuraEnabled
    public static void createPartsRequired(String wrapperText){
        try {           
            List<createRecordsWrapper> listToSave = (List<createRecordsWrapper>)JSON.deserialize(wrapperText, List<createRecordsWrapper>.class);
            List<CAP_IR_Asset_Part_Required__c> createdList=new List<CAP_IR_Asset_Part_Required__c>();
            for(createRecordsWrapper record:listToSave){
                CAP_IR_Asset_Part_Required__c newRecord=new CAP_IR_Asset_Part_Required__c();
                newRecord.SFS_Quantity__c=record.quantity;
                newRecord.Product__c=record.productRecId;
                newRecord.SFS_0k__c=record.workScope0;
                newRecord.SFS_1k__c=record.workScope1;
                newRecord.SFS_2k__c=record.workScope2;
                newRecord.SFS_4k__c=record.workScope4;
                newRecord.SFS_6k__c=record.workScope6;
                newRecord.SFS_8k__c=record.workScope8;
                newRecord.SFS_12k__c=record.workScope12;
                newRecord.SFS_16k__c=record.workScope16;
                newRecord.SFS_24k__c=record.workScope24;
                newRecord.SFS_48k__c=record.workScope48;
                newRecord.Asset__c=record.assetId;
                newRecord.SFS_Delete_After_Use__c=record.deleteAfterUse;
                createdList.add(newRecord);
            }
            insert createdList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}