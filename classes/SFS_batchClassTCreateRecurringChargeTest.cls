/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 12/10/2021
* @description: SFS_batchClassToCreateRecurringCharge Test Class.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================================================================*/

@isTest
public class SFS_batchClassTCreateRecurringChargeTest {
    
    @isTest 
    public static void monthlyInvoiceBatchTestMethod() {
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0]; 
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Monthly';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Monthly';
           svcAgreementList[0].SFS_Status__c = 'Draft';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        svcAgreementList[0].SFS_Status__c = 'Approved';
        Update svcAgreementList[0];
     
        
    }
    @isTest 
    public static void AnnuallyInvoiceBatchTestMethod() {
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0]; 
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Annually';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Annually';
           svcAgreementList[0].SFS_Status__c = 'Draft';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        svcAgreementList[0].SFS_Status__c = 'Approved';
        Update svcAgreementList[0];
     
  
    }
    
     @isTest 
    public static void QuarterlyInvoiceBatchTestMethod() {
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0]; 
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Quarterly';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Quarterly';
           svcAgreementList[0].SFS_Status__c = 'Draft';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_Invoice_Submission__c = 'Automatic';
           svcAgreementList[0].SFS_Invoice_Print_Code__c = 'Send to Customer';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        svcAgreementList[0].SFS_Status__c = 'Approved';
        Update svcAgreementList[0];
     
  
    }
    
     @isTest 
    public static void xperYearInvoiceBatchTestMethod() {
        String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0]; 
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = '3x per Year';
           svcAgreementList[0].Service_Charge_Frequency__c = '3x per Year';
           svcAgreementList[0].SFS_Status__c = 'Draft';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_Invoice_Submission__c = 'Automatic';
           svcAgreementList[0].SFS_Invoice_Print_Code__c = 'Send to Customer';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        svcAgreementList[0].SFS_Status__c = 'Approved';
       Update svcAgreementList[0];
     
  
    }
    @isTest 
    public static void DailyInvoiceBatchTestMethod() {
         String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0]; 
         String sA_RECORDTYPEID = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Contracting').getRecordTypeId();
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Daily';
           svcAgreementList[0].recordTypeId=sA_RECORDTYPEID;
           svcAgreementList[0].SFS_Status__c = 'Draft';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='Contracting';
           insert svcAgreementList[0];
        
        svcAgreementList[0].SFS_Status__c = 'Approved';
        Update svcAgreementList[0];
     
      
        
    }
    @isTest 
    public static void semiAnuallyInvoiceBatchTestMethod() {
         String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0]; 
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Semi Annually';
           svcAgreementList[0].Service_Charge_Frequency__c = 'Semi Annually';
           svcAgreementList[0].SFS_Status__c = 'Draft';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
           svcAgreementList[0].SFS_Type__c='PlannedCare';
           insert svcAgreementList[0];
        
        svcAgreementList[0].SFS_Status__c = 'Approved';
        Update svcAgreementList[0];
     
        system.assertEquals('Semi Annually',  svcAgreementList[0].SFS_Invoice_Frequency__c);
        system.assertEquals('Semi Annually',  svcAgreementList[0].Service_Charge_Frequency__c);
        
    }
    @isTest 
    public static void weeklyInvoiceBatchTestMethod() {
         String accRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
        ID rentalRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'ServiceContract' AND DeveloperName = 'SFS_Rental'].id;
        List<Account> accountList=SFS_TestDataFactory.createAccounts(2, false);
        accountList[0].IRIT_Customer_Number__c='123';
        accountList[0].RecordTypeId=accRecID;
        insert accountList[0];
        accountList[1].IRIT_Customer_Number__c='123';
        accountList[1].Bill_To_Account__c = accountList[0].Id;
        accountList[1].Type='Prospect';
        insert accountList[1];   
        
        List<Asset> assetList=SFS_TestDataFactory.createAssets(1, false);
        assetList[0].AccountId = accountList[1].Id;
        insert assetList[0]; 
         String sA_RECORDTYPEID = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Rental').getRecordTypeId();
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accountList[1].Id,false);
      
           svcAgreementList[0].SFS_Invoice_Frequency__c = 'Weekly';
           svcAgreementList[0].SFS_Status__c = 'Draft';
           svcAgreementList[0].SFS_Invoice_Format__c = 'Detail';
           svcAgreementList[0].SFS_PO_Number__c = '2';
           svcAgreementList[0].recordTypeId=sA_RECORDTYPEID;
           svcAgreementList[0].SFS_Invoice_Type__c = 'Receivable';
           svcAgreementList[0].SFS_PO_Expiration__c=system.today().addDays(60);
              svcAgreementList[0].SFS_Type__c='Rental';
           insert svcAgreementList[0];
        
        svcAgreementList[0].SFS_Status__c = 'Approved';
        Update svcAgreementList[0];
     
       
        
    }

   
}