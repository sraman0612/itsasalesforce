@isTest
public with sharing class TSTU_CustomerGeocodeBatch {

    @isTest
    public static void test_UTIL_CustomerGeocodeBatch()
    {
        createAccounts();
        test.startTest();
            Google gro = new Google();
            HttpRequest request = new HttpRequest();
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('googleApiAddressJSON');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/json');
            mock.respond(request);

            HttpResponse response = new HttpResponse();
            JSONParser responseParser = JSON.createParser(response.getBody());
            while(responseParser.nextToken() != null)
            {
                if(responseParser.getCurrentToken()==JSONToken.START_OBJECT){
                    gro = (Google) responseParser.readValueAs(Google.class);
                }
            }
            // Set the mock callout mode
            Test.setMock(HttpCalloutMock.class, mock);
            Database.executeBatch(new UTIL_CustomerGeocodeBatch());
        test.stopTest();
    }
    
    @isTest
    public static void test_Google()
    {
    	String resourceJson = UTIL_StaticResource.getStringResourceContents('googleApiAddressJSON');
    	Google testGoogle = Google.parse(resourceJson);
    }

    private static void createAccounts()
    {
        List<Account> newAccounts = new List<Account>();
        for (Integer i=1; i <= 5; i++) {
            Account Acct = new Account();
            Acct.Name = 'SAP Test '+i;
            Acct.BillingStreet = 4500+i+' Park Place';
            Acct.BillingCity = 'Sacramento';
            Acct.BillingState = 'California';
            Acct.BillingPostalCode = '95833';
            Acct.BillingCountry = 'United States';
            newAccounts.add(Acct);
        }
        insert newAccounts;
    }

}