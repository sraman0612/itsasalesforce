@isTest
public class ensxtx_TSTD_VCMaterialConfiguration
{
    @isTest static void test_ensxtx_DS_VCMaterialConfiguration ()
    {
        Test.startTest();
        ensxtx_SBO_EnosixVC_Detail.EnosixVC enosiXVC = new ensxtx_SBO_EnosixVC_Detail.EnosixVC();
        enosiXVC.Material = '001234';
        enosiXVC.CHARACTERISTICS.add(new ensxtx_SBO_EnosixVC_Detail.CHARACTERISTICS());
        enosiXVC.ALLOWEDVALUES.add(new ensxtx_SBO_EnosixVC_Detail.ALLOWEDVALUES());
        ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES selectedValue = new ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES();
        selectedValue.CharacteristicName = 'CharacteristicName';
        enosiXVC.SELECTEDVALUES.add(selectedValue);
        Product2 prod = ensxtx_TSTU_SFTestObject.createTestProduct2();
        ensxtx_TSTU_SFTestObject.upsertWithRetry(prod);

        ensxtx_DS_VCMaterialConfiguration dsVCMaterialConfiguration = new ensxtx_DS_VCMaterialConfiguration(null);
        dsVCMaterialConfiguration = new ensxtx_DS_VCMaterialConfiguration(enosiXVC);
        enosiXVC.Material = '00123456';
        dsVCMaterialConfiguration = new ensxtx_DS_VCMaterialConfiguration(enosiXVC);
        List<ensxtx_DS_VCCharacteristicValues> selectedValues = new List<ensxtx_DS_VCCharacteristicValues>();
        selectedValues.add(new ensxtx_DS_VCCharacteristicValues());
        dsVCMaterialConfiguration.indexedSelectedValues.put('1', new List<ensxtx_SBO_EnosixVC_Detail.SELECTEDVALUES>{selectedValue});
        dsVCMaterialConfiguration.convertToSBO(null);
        dsVCMaterialConfiguration.convertToSBO(selectedValues);
        dsVCMaterialConfiguration.prepIndexedCollections(null);
        dsVCMaterialConfiguration.prepIndexedCollections(enosiXVC);
        dsVCMaterialConfiguration.indexedSelectedValues.clear();
        dsVCMaterialConfiguration.getAllowedValuesFromSBOModel(null);
        dsVCMaterialConfiguration.getAllowedValuesFromSBOModel(enosiXVC);
        dsVCMaterialConfiguration.getSelectedValuesFromSBOModel(null);
        dsVCMaterialConfiguration.getSelectedValuesFromSBOModel(enosiXVC);
        Test.stopTest();
    }
}