public with sharing class UTIL_CustomerSyncBatchCleanup
    implements Database.Batchable<SObject>, Database.Stateful
{
    // Cleanup process for UTIL_CustomerSyncBatch()
    // Mainly to link the ShipTo(Child) Account to the SoldTo(Parent) Account

    // jobInfo contains debug information persisted across contexts since start(),
    // execute(), and finish() all run in separate contexts with separate debug logs
    private List<String> jobInfo = new List<String>();
    private List<SObject> errors = new List<SObject>();
    private string AccountType = 'Child';

    // start()
    //
    // Start the batch job
    public Database.QueryLocator start(Database.BatchableContext context)
    {
        System.debug(context.getJobId() + ' Starts');

        try
        {
            String query = buildQueryString();
            return Database.getQueryLocator(query);
        }
        catch(Exception ex)
        {
            System.debug('Failed querying New SAP Child Accounts: ' + ex.getMessage());
        }

        // Returning null causes "System.UnexpectedException: Start did not return a valid iterable object."
        // So to NOOP we must return a query that will always give 0 results. Id should never be
        // null in any table so we can arbitrarily pick Account.
        return Database.getQueryLocator([SELECT Id FROM Account WHERE Id = null]);
    }

    // execute()
    //
    // Execute the batch job
    public void execute(Database.BatchableContext context, List<SObject> searchResults)
    {
        System.debug(context.getJobId() + ' Executing');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);

        Set<String> SAPCustomerNumbers = new Set<String>();
        Set<String> SAPRepAcctNumbers = new Set<String>();
        List<Account> updatedAccounts = new List<Account>();

        if (searchResults.size() > 0)
        {
            // Get the Parent customer numbers from the searchResults
            for (SObject result : searchResults)
            {
                Account acct = (Account) result;
                if (acct.ENSX_EDM__SAP_Customer_Number__c != null) SAPCustomerNumbers.add(acct.ENSX_EDM__SAP_Customer_Number__c);
                if (acct.Rep_Account__c != null) SAPRepAcctNumbers.add(acct.Rep_Account__c);
            }

            Map<String, Account> parentAccounts = getRelatedAccounts(SAPCustomerNumbers,'Parent');
            Map<String, Account> repAccounts = getRelatedAccounts(SAPRepAcctNumbers,'Rep');

            for (SObject result : searchResults)
            {
                boolean recFound = false;
                Account acct = (Account) result;
                String parentId = null;
                if (parentAccounts.get(acct.ENSX_EDM__SAP_Customer_Number__c) != null) {
                    parentId = parentAccounts.get(acct.ENSX_EDM__SAP_Customer_Number__c).Id;
                }
                if (parentId != acct.ParentId) {
                    acct.ParentId = parentId;
                    recFound = true;
                }
                String repAccountId = null;
                if (repAccounts.get(acct.Rep_Account__c) != null) {
                    repAccountId = repAccounts.get(acct.Rep_Account__c).Id;
                }
                if (repAccountId != acct.Rep_Account2__c) {
                    acct.Rep_Account2__c = repAccountId;
                    recFound = true;
                }
                if (recFound == true) updatedAccounts.add(acct);
            }
        }

        if (updatedAccounts.size() > 0)
        {
            UTIL_SyncHelper.insertUpdateResults('Account', 'Update', null, null, updatedAccounts, 'UTIL_CustomerSyncBatchCleanup', null);
        }
    }

    // finish()
    //
    // Finish the batch job
    public void finish(Database.BatchableContext context)
    {
        System.debug(context.getJobId() + ' finished');
        UTIL_SyncHelper.printJobInfo(this.jobInfo);
    }

    private static Map<String, Account> getRelatedAccounts(Set<String> SAPCustomerNumbers, String AccType)
    {
        Map<String, Account> result = new Map<String, Account>();
        String custField1 = 'AccountNumber';
        String custField2 = 'Account_Number__c';
        String acctType = 'Parent';

        String QueryParents = 'SELECT Id, ' + custField1 + ',' + custField2 + ' FROM Account WHERE Account_Type__c = \''+acctType+'\' AND (' + custField1 + ' IN :SAPCustomerNumbers OR '+ custField2 + ' IN :SAPCustomerNumbers)';
        String QueryReps = 'SELECT Id, ' + custField1 + ',' + custField2 + ' FROM Account WHERE Account_Type__c = \''+acctType+'\' AND (' + custField1 + ' IN :SAPCustomerNumbers OR '+ custField2 + ' IN :SAPCustomerNumbers)';

        List<Account> accounts = new List<Account>();

        If (AccType == 'Parent') accounts = Database.query(QueryParents);
        If (AccType == 'Rep') accounts = Database.query(QueryReps);

        for (Account acct : accounts)
        {
            result.put(acct.AccountNumber, acct);
        }
        return result;
    }

    // buildQueryString
    //
    // Build the query String
    private String buildQueryString()
    {
        String query = 'SELECT Id, Name, ParentId, AccountNumber, Account_Number__c, ENSX_EDM__SAP_Customer_Number__c, Rep_Account__c, Rep_Account2__c ' +
            'FROM Account WHERE Account_Number__c != null AND Account_Type__c = \''+AccountType+'\' AND ENSX_EDM__SAP_Customer_Number__c != null';
        return query;
    }
}