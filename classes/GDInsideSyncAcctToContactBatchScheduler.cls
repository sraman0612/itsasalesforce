global class GDInsideSyncAcctToContactBatchScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        string dtString = string.valueof(date.valueof(system.today()));
        GDInsideSyncAcctToContactBatch batch = new GDInsideSyncAcctToContactBatch(dtString);
    }
}