public class GDInsideSyncAcctToContact {
    static String gdiUserName = 'salesForce';
    static String gdiPassword = 'EfkQrXBYhTEk12b8m2NH';
    
    static Map<string,contact> conMap = new Map<string,contact>();
    static Map<String, GDInsideSync.Account> gdiMap = new Map<String, GDInsideSync.Account>();
    static map<string, GDInsideSync.Account> contactsToCreate = new map<string, GDInsideSync.Account>();
    static Map<string,string> sfAcctIds = new Map<string,string>();
    static List<string> sapAcctNumbs = new List<string>();
    static List<Contact> createC = new List<Contact>();
    static Contact c;
    static String response;
    
    @future(callout=true)
    public static void sync(string myDate){
        
        Date startDate = Date.valueOf(myDate);//go live 2013-03-13
        integer i=0;
        
        //Map of all salesforce contacts to compare to accounts from GDInside
        for(Contact c:[select id, email, firstname, lastname from contact where email!=''])
        {
            conMap.put(c.email.toLowerCase(), c);
        }
        system.debug(Logginglevel.ERROR , conMap.size());
        
        List<string> dupPrev = new List<string>();
        //while(i<=1)
        //{
            HTTP h = new HTTP();
            HTTPRequest r = new HTTPRequest();
            r.setEndpoint('https://www.gdinside.com/api/public/Accounts?updated_at='+string.valueOf(startDate)+'&limit=1000');
            Blob headerValue = Blob.valueOf(gdiUserName + ':' + gdiPassword);
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            r.setHeader('Authorization', authorizationHeader);
            r.setMethod('GET');
            HTTPResponse resp = h.send(r);
            response = resp.getBody();
            
            string newResp = '{"Accounts":'+response+'}';
            
            //startDate = startDate.addDays(1); when reconciling...
            
            GDInsideSync gs = GDInsideSync.parse(newResp);
            
            
            //map of all the GDInside Accounts pulled back
            for(GDInsideSync.Account result: gs.accounts) {
                gdiMap.put(result.username.remove('/').toLowerCase(), result);
                system.debug(Logginglevel.ERROR , result);
            }
            
            
            For(String tmpKey : gdiMap.keySet())
            {
                if(!conMap.containsKey(tmpKey) && gdiMap.get(tmpKey).last_name != '' && !string.isBlank(gdiMap.get(tmpKey).sap_account_number))
                {
                    //put information in a map to be created
                    contactsToCreate.put(tmpKey,gdiMap.get(tmpKey));
                    //grabbing account numbers to look for
                    sapAcctNumbs.add((string)gdiMap.get(tmpKey).sap_account_number);
                }
            }
            system.debug(Logginglevel.ERROR , contactsToCreate.size() + '** '+contactsToCreate);            

            for(account acct: [SELECT Id,account_number__c FROM Account WHERE Account_Number__c IN :sapAcctNumbs])
            {
                sfAcctIds.put(acct.account_number__c, acct.id);
            }	
            
            try{
                //List<string> sapNumber = new List<string>();
                for(String email: contactsToCreate.keySet())
                {
                    string sap;
                    if(!string.isBlank((String)contactsToCreate.get(email).sap_account_number))
                        sap = (String)contactsToCreate.get(email).sap_account_number;                
                    
                    c = new Contact();
                    if(sfAcctIds.containsKey(sap))
                        c.accountId = sfAcctIds.get(sap);
                    c.email = (String) contactsToCreate.get(email).username;
                    c.firstName = (String) contactsToCreate.get(email).first_name;
                    c.lastName = (String)contactsToCreate.get(email).last_name;
                    c.Relationship__c='Customer';
                    c.SAP_Account_Number__c = sap;
                    if(!string.isBlank((String)contactsToCreate.get(email).last_name) && !dupPrev.contains((String) contactsToCreate.get(email).username)){
                        system.debug(Logginglevel.ERROR , 'The username to find DUPE: ' + (String) contactsToCreate.get(email).username);
                        createC.add(c);
                    }
                    dupPrev.add((String) contactsToCreate.get(email).username);
                }
               
                
            }Catch(Exception e)
            {
                system.debug(Logginglevel.ERROR , 'There was an error somewhere in the code check it out!!! '+e.getMessage() + ' '+e.getLineNumber());
            }
            
            system.debug(Logginglevel.ERROR , 'Size of contacts being inserted: ' +CreateC.size());
            //i++;
        //}
        //run queable job
        ID jobID = System.enqueueJob(new GDInsideCreateContact(createC));
        system.debug(Logginglevel.ERROR , 'Size of contacts being inserted: ' +CreateC.size());
        
    }
    
}