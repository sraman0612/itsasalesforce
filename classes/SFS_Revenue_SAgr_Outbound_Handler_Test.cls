@IsTest
public class SFS_Revenue_SAgr_Outbound_Handler_Test {
    
    @IsTest
    Public static Void SAOuntboundHandler_Test()
    {
        
        List<Account> accList=new List<Account>();
            Account acct = new Account();
        	acct.name = 'test account1';
        	acct.Currency__c='USD';
            acct.BillingCountry='USA';
            acct.AccountSource='Web';
            acct.BillingState='AP';
        acct.IRIT_Customer_Number__c='1234';
        acct.type = 'ship to';
        	insert acct;
            accList.add(acct);

       Pricebook2 standardPricebook=SFS_TestDataFactory.getPricebook2();
        List<PricebookEntry> entriesList=SFS_TestDataFactory.createPricebookEntry(1,true);
        
     String sA_RECORDTYPEID = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('PackageCARE').getRecordTypeId();
        
            ServiceContract agreement=new ServiceContract();
            agreement.Name='agreement';
            agreement.AccountId=accList[0].id;
            agreement.SFS_Type__c='PackageCARE';
            agreement.SFS_Portable_Required__c='Yes';
            agreement.StartDate=System.today();
            agreement.EndDate=System.today().addYears(2);
            agreement.SFS_Freight_Terms__c='Prepaid';
            agreement.recordTypeId=sA_RECORDTYPEID;
            agreement.SFS_Invoice_Format__c = 'Detail';
            agreement.SFS_PO_Number__c = '2';
            agreement.SFS_Invoice_Frequency__c = 'Semi Annually';
            agreement.Pricebook2Id = standardPricebook.id;
            agreement.SFS_Invoice_Type__c = 'Receivable';
            agreement.SFS_PO_Expiration__c=system.today().addDays(60);
            insert agreement;
        
       
        
        ContractLineItem lineItem=new ContractLineItem();
            lineItem.ServiceContractId=agreement.id;
            lineItem.Quantity=3;
            lineItem.UnitPrice=2.00;
        lineItem.PricebookEntryId=entriesList[0].Id;
           
            insert lineItem;
        
     
        String invoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Service Invoice').getRecordTypeId();
       

            List<Invoice__c> INVL= new List<Invoice__c>();
             Invoice__c inv = new Invoice__c(recordTypeId = invoiceRecordTyeId,
                                       SFS_Service_Agreement__c=agreement.Id,
                                       	SFS_Status__c='Submitted',
                                       SFS_Invoice_Format__c=agreement.SFS_Invoice_Format__c,
                                       SFS_Billing_Period_End_Date__c=system.today(),
                                        SFS_Service_Agreement_Line_Item__c=lineItem.Id,
                                       SFS_Billing_Period_Start_Date__c= system.today());
        INVL.add(inv);
            insert INVL;

            
            
        /*
        
         
        String chargeRecordTyeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        List<CAP_IR_Charge__c> chargeList=new List<CAP_IR_Charge__c>();
       
            CAP_IR_Charge__c charge=new CAP_IR_Charge__c();
            charge.SFS_Service_Agreement__c=sc[0].id;
        charge.CAP_IR_Service_Contract__c=sc[0].id;
        charge.SFS_Invoice__c = invoiceList[0].id;
            //charge.CAP_IR_Amount__c=1000;
           charge.SFS_Sell_Price__c=1200;
            chargeList.add(charge);
            insert chargeList;
        
        */
        
              
      
         CAP_IR_Charge__c charge = new CAP_IR_Charge__c(CAP_IR_Amount__c=100,CAP_IR_Date__c=system.today(),CAP_IR_Service_Contract__c=agreement.id,CAP_IR_Contract_Line_Item__c=lineItem.id,CAP_IR_Description__c='Test',CAP_IR_Type__c = 'Expense');
         insert charge;
         
        charge.SFS_Invoice__c=inv.Id;
         update charge;
       
        
            List<ID> ID1= new List<ID>();
            ID1.add(INVL[0].id);
        
        
    test.startTest();
        //try{
        SFS_Revenue_SAgr_Outbound_Handler.Run(ID1);
        
        /*}
        catch(exception e){
            
        }*/
        test.stopTest();
        
        
        
        
    }
    
    
    @IsTest
    Public static Void SAOuntboundHandler_Test1()
    {
        
        List<Account> accList=new List<Account>();
            Account acct = new Account();
        	acct.name = 'test account1';
        	acct.Currency__c='USD';
            acct.BillingCountry='USA';
            acct.AccountSource='Web';
            acct.BillingState='AP';
        acct.IRIT_Customer_Number__c='1234';
        acct.type = 'ship to';
        	insert acct;
            accList.add(acct);

       Pricebook2 standardPricebook=SFS_TestDataFactory.getPricebook2();
        List<PricebookEntry> entriesList=SFS_TestDataFactory.createPricebookEntry(1,true);
        
     String sA_RECORDTYPEID = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('PackageCARE').getRecordTypeId();
        
            ServiceContract agreement=new ServiceContract();
            agreement.Name='agreement';
            agreement.AccountId=accList[0].id;
            agreement.SFS_Type__c='PackageCARE';
            agreement.SFS_Portable_Required__c='Yes';
            agreement.StartDate=System.today();
            agreement.EndDate=System.today().addYears(2);
            agreement.SFS_Freight_Terms__c='Prepaid';
            agreement.recordTypeId=sA_RECORDTYPEID;
            agreement.SFS_Invoice_Format__c = 'Detail';
            agreement.SFS_PO_Number__c = '2';
            agreement.SFS_Invoice_Frequency__c = 'Semi Annually';
            agreement.Pricebook2Id = standardPricebook.id;
            agreement.SFS_Invoice_Type__c = 'Receivable';
            agreement.SFS_PO_Expiration__c=system.today().addDays(60);
            insert agreement;
        
       
        
        ContractLineItem lineItem=new ContractLineItem();
            lineItem.ServiceContractId=agreement.id;
            lineItem.Quantity=3;
            lineItem.UnitPrice=2.00;
        lineItem.PricebookEntryId=entriesList[0].Id;
           
            insert lineItem;
        
     
        String invoiceRecordTyeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Service Invoice').getRecordTypeId();
       

            List<Invoice__c> INVL= new List<Invoice__c>();
             Invoice__c inv = new Invoice__c(recordTypeId = invoiceRecordTyeId,
                                       SFS_Service_Agreement_1__c=agreement.Id,
                                       	SFS_Status__c='Submitted',
                                       SFS_Invoice_Format__c=agreement.SFS_Invoice_Format__c,
                                       SFS_Billing_Period_End_Date__c=system.today(),
                                        SFS_Service_Agreement_Line_Item__c=lineItem.Id,
                                       SFS_Billing_Period_Start_Date__c= system.today());
        INVL.add(inv);
            insert INVL;

            List<Expense> expensesList=new List<Expense>();
             Expense exp=new Expense();
            //exp.WorkOrderId=woId;
           // exp.SFS_Work_Order_Line_Item__c=woliId;
            exp.Amount=100;
            exp.TransactionDate=System.today();
            expensesList.add(exp);
        
        if(expensesList.size()>0)
        	insert expensesList;
       
    
            
        /*
        
         
        String chargeRecordTyeId = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        List<CAP_IR_Charge__c> chargeList=new List<CAP_IR_Charge__c>();
       
            CAP_IR_Charge__c charge=new CAP_IR_Charge__c();
            charge.SFS_Service_Agreement__c=sc[0].id;
        charge.CAP_IR_Service_Contract__c=sc[0].id;
        charge.SFS_Invoice__c = invoiceList[0].id;
            //charge.CAP_IR_Amount__c=1000;
           charge.SFS_Sell_Price__c=1200;
            chargeList.add(charge);
            insert chargeList;
        
        */
        
         Product2 pro = new Product2(Name = 'testProduct');
        insert pro;     
      
         CAP_IR_Charge__c charge = new CAP_IR_Charge__c(CAP_IR_Amount__c=100,CAP_IR_Date__c=system.today(),CAP_IR_Service_Contract__c=agreement.id,CAP_IR_Contract_Line_Item__c=lineItem.id,CAP_IR_Description__c='Test',CAP_IR_Type__c = 'Expense');
         
        insert charge;
        charge.SFS_Expense__c = expensesList[0].id;
        charge.SFS_Invoice__c=inv.Id;
        charge.SFS_Product__c = pro.id;
         update charge;
       
        
            List<ID> ID1= new List<ID>();
            ID1.add(INVL[0].id);
        
        
    test.startTest();
        //try{
        SFS_Revenue_SAgr_Outbound_Handler.Run(ID1);
        
        /*}
        catch(exception e){
            
        }*/
        test.stopTest();
        
        
        
        
    }
}