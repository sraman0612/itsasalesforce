@isTest
private class GDInside_Auth_Handler_2_Test {
  
    static string gdiResponse = '{"id":"11314","first_name":"Christopher","last_name":"Knitter","phone_number":"","company_name":"Canpango","account_number":"12345","alt_address_1":"","alt_address_2":"","alt_city":"","alt_state":"","alt_zip":"","country":"USA","address_1":"","address_2":"","city":"","state":"","zip":"","alt_country":"USA","email":null,"sap_account_number":"12345","sap_company_name":"Ben\u0027s Distro 3","sap_sales_office":"","disabled":"0","date_disabled":null,"created_by":null,"account_type":"1","default_access":"16","concessions":"0","salesman":"","notes":"","request_date":null,"date_approved":"2017-07-11","date_denied":null,"status":"2","allow_asset_submission":"0","temp_password":"0","temp_password_date":null,"account_info_updated":"0","subscriptions_updated":"0","concession_manager":null,"username":"christopher.knitter@canpango.com","pass":"7f2ababa423061c509f4923dd04b6cf1","token":"266bec3a7c1fe04ff211f1f305c23fd1","distributor_account_manager":"0","password_changed":"2017-07-11","activity_email":null,"distributor_contact":"0","ldap_username":null,"dn":null,"lms_user":"0","ams_user_role":"","additional_sap_account_numbers":"","eval_user_role":"","distributor_profile":"0","access_template_id":null,"sales_force_profile_id":"3","attribute_id":null,"created_at":"2017-07-11 15:30:37","updated_at":"2017-10-16 15:28:57","Zones":"16","User Types":null,"Admin Groups":null}';
    
    @testSetup
    private static void setup(){
        Account newAcct = new Account();
        newAcct.Name = 'SSO Testing Account';
        newAcct.Account_Number__c = '12345';
        insert newAcct;
        
        newAcct = [SELECT Id FROM Account WHERE Account_Number__c = '12345'];
        
        Contact newCtc = new Contact();
        newCtc.FirstName = 'ChrisTest';
        newCtc.LastName = 'KnitterTest';
        newCtc.Email = 'christopher.knitter@canpango.com';
        newCtc.AccountId = newAcct.id;
        insert newCtc;
        
    }
    
    private static testmethod void doCreateUser(){
        
        // Fake a request and call createUser
        Test.setMock(HttpCalloutMock.class, new GDInside_Mock_HTTP());
        Auth.UserData xud = new Auth.UserData('testId', 'testFirst', 'testLast',
                                              'testFirst testLast', 'testuser@example.org', null, 'testuserlong', 'en_US', 'GDInside',
                        null, new Map<String, String>{'language' => 'en_US'});
        new GDInside_Auth_Handler_2().createUser(null, xud);
        
        // Check for the created user
        User xusr = [SELECT Username, Email, FirstName, LastName, Alias, EmailEncodingKey, TimeZoneSidKey FROM User WHERE Email = 'christopher.knitter@canpango.com.mock'];
        System.assertEquals('christopher.knitter@canpango.com.mock.gdinside', xusr.username);
        System.assertEquals('christopher.knitter@canpango.com.mock', xusr.email);
        System.assertEquals('Christopher', xusr.FirstName);
        System.assertEquals('Knitter', xusr.LastName);
        System.assertEquals('christopher.knitter@canpango.com.mock'.substring(0,8), xusr.alias);
        System.assertEquals('UTF-8', xusr.EmailEncodingKey);
        System.assertEquals('America/Chicago', xusr.TimeZoneSidKey);

        
    }
    
    private static testmethod void doUpdateUser(){
        // Create the user so it already exists.
        Contact c = [SELECT Id FROM Contact WHERE LastName = 'KnitterTest'];
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Partner Community User'];
        User xusr = new User(
            alias='christop', email='christopher.knitter@canpango.com',
            firstname='Christopher', lastname='Knitter',
            username='christopher.knitter@canpango.com.mock',
            ProfileId=p.Id, ManagerId = UserInfo.getUserId(), ContactId = c.Id,
            TimeZoneSidKey='America/Chicago', LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='UTF-8'
        );
        insert xusr;
        
        Test.startTest();
        xusr = [SELECT ID FROM User WHERE username='christopher.knitter@canpango.com.mock'];
        
        // Fake a request and call updateUser
        Test.setMock(HttpCalloutMock.class, new GDInside_Mock_HTTP());
        Auth.UserData xud = new Auth.UserData('testId', 'testFirst', 'testLast',
                                              'testFirst testLast', 'testuser@example.org', null, 'testuserlong', 'en_US', 'GDInside',
                        null, new Map<String, String>{'language' => 'en_US'});
        new GDInside_Auth_Handler_2().updateUser(xusr.id, null, xud);   
        Test.stopTest();
    }
}