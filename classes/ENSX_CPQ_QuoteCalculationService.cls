@RestResource(urlMapping='/ensxCPQQuoteCalculationService/*')
global with sharing class ENSX_CPQ_QuoteCalculationService {
    private static final ensxsdk.Logger logger = new ensxsdk.Logger(ENSX_CPQ_QuoteCalculationService.class);

    //todo: these should be defaulted in metadata, then overrideable per quote
    private static String conditionType = (String)UTIL_AppSettings.getValue('CPQSimulation.ConditionType', '');
    private static String defaultSalesDocType = (String)UTIL_AppSettings.getValue('CPQSimulation.SalesDocType', '');
    private static String defaultSalesOrg = (String)UTIL_AppSettings.getValue('CPQSimulation.SalesOrg', '');
    private static String defaultDistributionChannel = (String)UTIL_AppSettings.getValue('CPQSimulation.DistributionChannel', '');
    private static String defaultDivision = (String)UTIL_AppSettings.getValue('CPQSimulation.Division', '');
    private static String defaultCustomerNumber = (String)UTIL_AppSettings.getValue('CPQSimulation.CustomerNumber', '');
    private static String defaultSoldToPartnerFunction = (String)UTIL_AppSettings.getValue('CPQSimulation.SoldToPartnerFunction', 'SP');
    private static String VCListPriceConditionType = (String)UTIL_AppSettings.getValue('CPQSimulation.VCListPriceConditionType', '');
    private static String ListPriceConditionType = (String)UTIL_AppSettings.getValue('CPQSimulation.ListPriceConditionType', '');
    private static String BundlePriceConditionType = (String)UTIL_AppSettings.getValue('CPQSimulation.BundlePriceConditionType', '');
    private static String DiscountPriceConditionType = (String)UTIL_AppSettings.getValue('CPQSimulation.DiscountPriceConditionType', '');
    private static String defaultItemPlant = (String)UTIL_AppSettings.getValue('CPQSimulation.ItemPlant', '');

    // Indexed by the CPQ Product Id;
    @testVisible
    private static Map<Integer, ENSX_QuoteLineMapping> preCalculateState = new Map<Integer, ENSX_QuoteLineMapping>();
    @testVisible
    private static Map<String, ENSX_QuoteLine> materialNumberId = new Map<String, ENSX_QuoteLine>();

    @HttpPost
    global static String doPost(String quote)
    {
        DateTime startinit = System.now();
        Integer startCpu = Limits.getCpuTime();
        Integer startHeap = Limits.getHeapSize();
        System.debug('Start doPost '+ startinit);
        System.debug('Start getCpuTime() '+startCpu);
        System.debug('Start getHeapSize() '+startHeap);

        logger.enterAura('doPost', new Map<String, Object> {
            'quote' => quote
        });
        try
        {
            System.debug('/ensxCPQQuoteCalculationService/doPost');
            ENSX_Quote qte  = (ENSX_Quote)JSON.deserialize(quote,ENSX_Quote.class);
            Set<String> productIds = new Set<String>();
            Integer qlTot = qte.LinkedQuoteLines.size();
            for (Integer qlCnt = 0 ; qlCnt < qlTot ; qlCnt++)
            {
                ENSX_QuoteLine ql = qte.LinkedQuoteLines[qlCnt];
                productIds.add(ql.Product);
            }

            Map<Id, Product2> productMap = UTIL_SFProduct.getProductsByField('Id', new List<String>(productIds), null);

            Integer itemIncrement = 1000;
            Integer parentItemNumber = 0;
            Integer childItem = 0;
            Map<String, Integer> placeholderChildItemsCounter = new Map<String, Integer>();
            for (Integer qlCnt = 0 ; qlCnt < qlTot ; qlCnt++)
            {
                ENSX_QuoteLine ql = qte.LinkedQuoteLines[qlCnt];
                ql.ItemConfiguration = String.isBlank(ql.itemJSON) ? new ENSX_ItemConfiguration() : (ENSX_ItemConfiguration)JSON.deserialize(ql.itemJSON, ENSX_ItemConfiguration.class);
                // Reduce response size by removing duplicate version of the same data
                ql.itemJSON = null;

                Product2 prod = productMap.get(ql.Product);
                if (UTIL_SFProduct.isProductLinkedToMaterial(prod))
                {
                    ql.SAPMaterialNumber = (String) prod.get(UTIL_SFProduct.MaterialFieldName);
                    String childItemKey = String.valueOf(parentItemNumber) + ';' + ql.SAPMaterialNumber;
                    Integer counter = placeholderChildItemsCounter.containsKey(childItemKey) ? 
                        placeholderChildItemsCounter.get(childItemKey) : -1;
                    counter++;
                    if (counter > 0) ql.SAPMaterialNumber += ' (' + counter + ')';
                    placeholderChildItemsCounter.put(childItemKey, counter);
    
                    materialNumberId.put(ql.SAPMaterialNumber, ql);
                    childItem++;
                    if (ql.ParentLineItem == null)
                    {
                        parentItemNumber += itemIncrement;
                        childItem = 0;
                    }
                    preCalculateState.put(ql.LineItem, new ENSX_QuoteLineMapping(ql, parentItemNumber + childItem));
                    ql.ItemNumber = String.valueOf(parentItemNumber + childItem);
                }
            }

            List<SBQQ__Quote__c> q = [SELECT SAP_Configuration__c, SBQQ__Account__c, FLD_SAP_Order_Type__c FROM SBQQ__Quote__c WHERE Id = :qte.QuoteId LIMIT 1];
            if (q.size() == 1) {
                String headerJSON = q[0].SAP_Configuration__c;
                if (String.isNotBlank(headerJSON)) {
                    ENSX_Quote headerConfig = (ENSX_Quote)JSON.deserialize(headerJSON, ENSX_Quote.class);
                    headerConfig.LinkedQuoteLines = qte.LinkedQuoteLines;
                    qte = headerConfig;
                }
                if (String.isBlank(qte.soldToParty) && !String.isBlank(q[0].SBQQ__Account__c))
                {
                    qte.soldToParty = UTIL_SFAccount.getCustomerNumberFromAccount(UTIL_SFAccount.getAccountById(q[0].SBQQ__Account__c));
                } 
                if (String.isBlank(qte.salesDocType)) qte.salesDocType = q[0].FLD_SAP_Order_Type__c;
            }
            if (String.isBlank(qte.soldToParty)) qte.soldToParty = defaultCustomerNumber;
            if (String.isBlank(qte.salesDocType)) qte.salesDocType = defaultSalesDocType;
            if (String.isBlank(qte.distChannel)) qte.distChannel = defaultDistributionChannel;
            if (String.isBlank(qte.division)) qte.division = defaultDivision;
            if (String.isBlank(qte.salesOrg)) qte.salesOrg = defaultSalesOrg;

            try
            {
                SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing sbo = UTIL_CPQ_QuotePricing.getSBOForENSX_Quote(qte, preCalculateState, productMap);
                updateENSX_QuoteForSBOResults(qte, sbo);
                SAPSimulationResults simresults = new SAPSimulationResults();
                simresults.Success = true;
                simresults.Message = 'Simulated Pricing in SAP for ' + sbo.ITEMS.size() + ' items';
                simresults.Quote = qte;
                System.debug('simulation Success ' );
                DateTime endInit = System.now();
                Integer endCpu = Limits.getCpuTime();
                Integer endHeap = Limits.getHeapSize();
                System.debug('End getHeapSize() '+endHeap);
                System.debug('End getCpuTime() '+Limits.getCpuTime());
                System.debug('End doPost '+ endInit);
                System.debug('Heap diff ' + (endHeap - startHeap));
                System.debug('CPU diff ' + (endCpu - startCpu));
                System.debug('MilliSecond Calculate ' + (endInit.getTime() - startinit.getTime()));
                return JSON.serialize(simresults);
            }
            catch (ENSX_CPQ_Exceptions.SimulationException simEx)
            {
                System.debug('simulation failure ' );
                SAPSimulationResults simresults = new SAPSimulationResults();
                simresults.Success = false;
                simresults.Message = simEx.getMessage();
                simresults.Quote = qte;
                DateTime endInit = System.now();
                Integer endCpu = Limits.getCpuTime();
                Integer endHeap = Limits.getHeapSize();
                System.debug('End getHeapSize() '+endHeap);
                System.debug('End getCpuTime() '+Limits.getCpuTime());
                System.debug('End doPost '+ endInit);
                System.debug('Heap diff ' + (endHeap - startHeap));
                System.debug('CPU diff ' + (endCpu - startCpu));
                System.debug('MilliSecond Calculate ' + (endInit.getTime() - startinit.getTime()));
                return JSON.serialize(simresults);
            }
        }   
        catch (Exception ex) {
            logger.log(LoggingLevel.ERROR, 'There was an exception thrown when attempting to doPost ', ex);
            SAPSimulationResults simresults = new SAPSimulationResults();
            simresults.Success = false;
            simresults.Message = ex.getMessage();
            simresults.Quote = new ENSX_Quote();
            simresults.Quote.LinkedQuoteLines = new List<ENSX_QuoteLine>();
            DateTime endInit = System.now();
            Integer endCpu = Limits.getCpuTime();
            Integer endHeap = Limits.getHeapSize();
            System.debug('End getHeapSize() '+endHeap);
            System.debug('End getCpuTime() '+Limits.getCpuTime());
            System.debug('End doPost '+ endInit);
            System.debug('Heap diff ' + (endHeap - startHeap));
            System.debug('CPU diff ' + (endCpu - startCpu));
            System.debug('MilliSecond Calculate ' + (endInit.getTime() - startinit.getTime()));
            return JSON.serialize(simresults);
        }
        finally
        {
            logger.exit();
        }
    }

    @testVisible
    private static String concatSboMessages(List<ensxsdk.EnosixFramework.Message> messages)
    {
        String exceptionMessage = '';
        if (null == messages || messages.size() == 0)
        {
            exceptionMessage = 'Quote Calculation Failed';
        }
        else
        {
            for (integer i = 0; i < messages.size(); i++ )
            {
                exceptionMessage += messages[i].Text;
                if (i != messages.size() - 1)
                {
                    exceptionMessage += ', ';
                }
            }
        }
        return exceptionMessage;
    }

    // updateENSX_QuoteForSBOResults
    //
    // Maps results of the SAP pricing simulation back to the incoming quote
    @testVisible
    private static void updateENSX_QuoteForSBOResults(ENSX_Quote qte, SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing sbo)
    {
        Map<Integer,SBO_EnosixOpportunityPricing_Detail.ITEMS> itms = new Map<Integer,SBO_EnosixOpportunityPricing_Detail.ITEMS>();

        // Indexed by the line item number of the lists parent, and then indexed by the material number
        Map<Integer, Map<String, SBO_EnosixOpportunityPricing_Detail.ITEMS>> childItems = new Map<Integer, Map<String, SBO_EnosixOpportunityPricing_Detail.ITEMS>>();
        Map<String, Integer> placeholderChildItemsCounter = new Map<String, Integer>();
        Map<String, String> childItemHigherLevelItem = new Map<String, String>();          

        List<SBO_EnosixOpportunityPricing_Detail.ITEMS> itemList = sbo.ITEMS.getAsList();
        Integer itemTot = itemList.size();
        for (Integer itemCnt = 0 ; itemCnt < itemTot ; itemCnt++)
        {
            SBO_EnosixOpportunityPricing_Detail.ITEMS itm = itemList[itemCnt];
            Integer higherLevelItemNumber = Integer.valueOf(itm.HigherLevelItemNumber);
            // A higher item level number of '0' indicates that the item is root parent item
            if(0 != higherLevelItemNumber)
            {                
                // A child item could have its own child item
                // This is a workaround that replace the HigherLevelItemNumber with the top parent
                if (childItemHigherLevelItem.containsKey(itm.HigherLevelItemNumber)) 
                {
                    itm.HigherLevelItemNumber = childItemHigherLevelItem.get(itm.HigherLevelItemNumber);
                    higherLevelItemNumber = Integer.valueOf(itm.HigherLevelItemNumber);
                }
                childItemHigherLevelItem.put(itm.ItemNumber, itm.HigherLevelItemNumber);

                Map<String, SBO_EnosixOpportunityPricing_Detail.ITEMS> materialItemMap = 
                    childItems.containsKey(higherLevelItemNumber) ? childItems.get(higherLevelItemNumber) :
                    new Map<String, SBO_EnosixOpportunityPricing_Detail.ITEMS>();
                if (materialItemMap.containsKey(itm.Material))
                {
                    String childItemKey = String.valueOf(higherLevelItemNumber) + ';' + itm.Material;
                    Integer counter = placeholderChildItemsCounter.containsKey(childItemKey) ? 
                        placeholderChildItemsCounter.get(childItemKey) : 0;
                    counter++;
                    materialItemMap.put(itm.Material + ' (' + counter + ')', itm);
                    placeholderChildItemsCounter.put(childItemKey, counter);
                }
                else materialItemMap.put(itm.Material, itm);

                childItems.put(higherLevelItemNumber, materialItemMap);
            }
            else
            {
                itms.put(Integer.valueOf(itm.ItemNumber),itm);
            }
        }

        Map<Integer,Date> atp = getItemsAvailableToPromiseDate(sbo);

        Map<String,Decimal> listPriceCondition = new Map<String,Decimal>();
        Map<String,Decimal> vcListPriceCondition = new Map<String,Decimal>();
        Map<String,Decimal> bundlePriceCondition = new Map<String,Decimal>();
        Map<String,Decimal> discountPriceCondition = new Map<String,Decimal>();
        Map<String,Map<String,Decimal>> conditions = 
            getLastPriceConditionRatePerUnitPerLinePerType(
                new Set<String>{ ListPriceConditionType, VCListPriceConditionType, BundlePriceConditionType, DiscountPriceConditionType }, sbo);

        if (null != conditions)
        {
            if (null != conditions.get(ListPriceConditionType))
            {
                listPriceCondition = conditions.get(ListPriceConditionType);
            }
            if (null != conditions.get(VCListPriceConditionType))
            {
                vcListPriceCondition = conditions.get(VCListPriceConditionType);
            }
            if (null != conditions.get(BundlePriceConditionType))
            {
                bundlePriceCondition = conditions.get(BundlePriceConditionType);
            }
            if (null != conditions.get(DiscountPriceConditionType))
            {
                discountPriceCondition = conditions.get(DiscountPriceConditionType);
            }
        }
        System.debug('iterating and updating pricing from SAP on the quote');

        List<ENSX_QuoteLine> newLinkedQuoteLines = new List<ENSX_QuoteLine>();
        Integer qlTot = qte.LinkedQuoteLines.size();
        for (Integer qlCnt = 0 ; qlCnt < qlTot ; qlCnt++)
        {
            ENSX_QuoteLine ql = qte.LinkedQuoteLines[qlCnt];
            ENSX_QuoteLineMapping priorMapping = preCalculateState.get(ql.LineItem);
            // Gets the item for the current mapped field
            if(priorMapping != null && !priorMapping.IsProductFeature)
            {
                SBO_EnosixOpportunityPricing_Detail.ITEMS parentItm = itms.get(priorMapping.SAPLineItem);
                updateQuoteLineFromSAP(ql, atp, parentItm, listPriceCondition, vcListPriceCondition, bundlePriceCondition, discountPriceCondition, false);
                newLinkedQuoteLines.add(ql);
            }
            else if (priorMapping != null)
            {                
                ENSX_QuoteLineMapping parentMapping = preCalculateState.get(priorMapping.CPQParentLineItem);
                if (null != parentMapping && null != childItems.get(parentMapping.SAPLineItem))
                {
                    SBO_EnosixOpportunityPricing_Detail.ITEMS parentItm = itms.get(parentMapping.SAPLineItem);
                    Map<String, SBO_EnosixOpportunityPricing_Detail.ITEMS> childItemMap = childItems.get(parentMapping.SAPLineItem);
                    SBO_EnosixOpportunityPricing_Detail.ITEMS childItm = childItemMap.get(ql.SAPMaterialNumber);
                    if (childItm != null)
                    {
                        updateQuoteLineFromSAP(ql, atp, childItm, listPriceCondition, vcListPriceCondition, bundlePriceCondition, discountPriceCondition, false);
                        childItemMap.remove(ql.SAPMaterialNumber);
                    }
                }
                else
                {
                    logger.debug('updateENSX_QuoteForSBOResults:IsProductFeature:UnexpectedState', new Map<String, Object> {
                        'ql' => ql,
                        'parentMapping' => parentMapping,
                        'childItems.get(parentMapping.SAPLineItem)' => parentMapping != null ? childItems.get(parentMapping.SAPLineItem) : null
                    });
                }
                newLinkedQuoteLines.add(ql);
            }       
        }
        qte.LinkedQuoteLines = newLinkedQuoteLines;
    }

    @testVisible
    private static void updateQuoteLineFromSAP(
        ENSX_QuoteLine ql, 
        Map<Integer,Date> atp, 
        SBO_EnosixOpportunityPricing_Detail.ITEMS itm, 
        Map<String,Decimal> listPriceCondition,
        Map<String,Decimal> vcListPriceCondition,
        Map<String,Decimal> bundlePriceCondition,
        Map<String,Decimal> discountPriceCondition,
        Boolean isFeature)
    {
        if (null != itm)
        {
            Decimal listPrice = 0;
            Decimal vcListPrice = 0;
            Decimal bundlePrice = 0;
            Decimal discountPrice = 0;
            if (listPriceCondition.get(ql.ItemNumber) != null) {
                listPrice = listPriceCondition.get(ql.ItemNumber);
            }
            
            if (vcListPriceCondition.get(ql.ItemNumber) != null) {
                vcListPrice = vcListPriceCondition.get(ql.ItemNumber);
            }

            if (bundlePriceCondition.get(ql.ItemNumber) != null) {
                bundlePrice = bundlePriceCondition.get(ql.ItemNumber);
            }

            if (discountPriceCondition.get(ql.ItemNumber) != null) {
                discountPrice = discountPriceCondition.get(ql.ItemNumber);
            }

            if (null == listPrice || 0 == ListPrice) {
                listPrice = vcListPrice;
            }

            if (listPrice != null && listPrice > 0) {
                listPrice = listPrice / ql.Quantity;
            }

            if (discountPrice != null && discountPrice > 0) {
                discountPrice = discountPrice / ql.Quantity;
            }

            Decimal unitCost = itm.CostInDocCurrency / ql.Quantity;

            // Calculate netCost based on CPQ quote-line quantity since at some clients have
            // observed SAP quantities are changed during simulation
            Decimal netCost = unitCost * ql.Quantity;

            System.debug(
                'Found a match for line item, itm.OrderQuantity is ' + itm.OrderQuantity +
                ', itm.NetItemPrice is ' + itm.NetItemPrice + ', listPrice is ' + listPrice +
                '; unitCost is ' + unitCost + ', itm.CostInDocCurrency is ' +
                itm.CostInDocCurrency + ', netCost is ' + netCost
            );

            ql.ListPrice = listPrice;
            ql.UnitCost = unitCost;
            ql.Plant = itm.Plant;
            ql.NetCost = netCost;
            ql.NetWeight = itm.Netweight;
            ql.CostPrice = itm.NetItemPrice;
            ql.NetPrice = ql.ListPrice * ql.Quantity;

            Integer itemNumber = Integer.valueOf(ql.ItemNumber);
            ql.ATPDate = atp.get(itemNumber);
            if (isFeature)
            {
                ENSX_QuoteLine quoteLine = materialNumberId.get(itm.Material);
                if (null != quoteLine)
                {
                    ql.RealProductId = quoteLine.Product;
                    ql.RealPricebookEntryId = quoteLine.PricebookEntry;
                }
            } 
        }
    }

    @testVisible
    private static Map<String,Map<String,Decimal>> getLastPriceConditionRatePerUnitPerLinePerType(Set<String> conditionTypes, SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing sbo)
    {
        if (null == conditionTypes || null == sbo)
        {
            return null;
        }
        conditionTypes.removeAll(new Set<String> { null, '' });
        if (0 == conditionTypes.size())
        {
            return null;
        }
        Map<String,Map<String,Decimal>> result = new Map<String,Map<String,Decimal>>();
        List<SBO_EnosixOpportunityPricing_Detail.CONDITIONS> cList = sbo.CONDITIONS.getAsList();
        Integer cTot = cList.size();
        for (Integer cCnt = 0 ; cCnt < cTot ; cCnt++)
        {
            SBO_EnosixOpportunityPricing_Detail.CONDITIONS condition = cList[cCnt];
            if (conditionTypes.contains(condition.ConditionType))
            {
                Map<String,Decimal> priceConditionMap = result.get(condition.ConditionType);
                if (null == priceConditionMap)
                {
                    priceConditionMap = new Map<String,Decimal>();
                    result.put(condition.ConditionType, priceConditionMap);
                }
                Decimal pricingUnit = condition.ConditionPricingUnit;
                if(null == pricingUnit || 0 == pricingUnit)
                {
                    pricingUnit = 1;
                }

                Decimal newValue = (condition == null || condition.KWERT == null?1:condition.KWERT) / pricingUnit;
                Decimal existingValue = 0;
                String itemNumber = condition.ConditionItemNumber.replaceFirst('^0+(?!$)', '');

                if (priceConditionMap.get(itemNumber) != null) {
                    existingValue = priceConditionMap.get(itemNumber);
                }

                priceConditionMap.put(itemNumber, newValue + existingValue);
                
            }
        }
        return result;
    }

    // getItemsAvailableToPromiseDate
    //
    // Calculates Available To Promise (ATP) dates for all items by taking the
    // greatest schedule date per item with a confirmed quantity.
    private static Map<Integer,Date> getItemsAvailableToPromiseDate(SBO_EnosixOpportunityPricing_Detail.EnosixOpportunityPricing sbo) {
        Map<Integer,Date> atp = new Map<Integer,Date>();
        List<SBO_EnosixOpportunityPricing_Detail.ITEMS_SCHEDULE> schedList = sbo.ITEMS_SCHEDULE.getAsList();
        Integer schedTot = schedList.size();
        for (Integer schedCnt = 0 ; schedCnt < schedTot ; schedCnt++)
        {
            SBO_EnosixOpportunityPricing_Detail.ITEMS_SCHEDULE schedule = schedList[schedCnt];
            Integer itemNumber = Integer.valueOf(schedule.ItemNumber);
            if (schedule.ConfirmedQuantity > 0 && (null == atp.get(itemNumber) || schedule.ScheduleLineDate > atp.get(itemNumber)))
            {
                atp.put(itemNumber, schedule.ScheduleLineDate);
            }
        }
        return atp;
    }

    public class SAPSimulationResults
    {
        @AuraEnabled
        public Boolean Success { get; set; }

        @AuraEnabled
        public String Message { get; set; }

        @AuraEnabled
        public ENSX_Quote Quote {get;set;}
    }
}