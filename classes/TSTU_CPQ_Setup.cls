@IsTest
public with sharing class TSTU_CPQ_Setup
{
    @IsTest
    public static void test_installCustomScript()
    {
        Test.startTest();
        UTIL_CPQ_Setup util = new UTIL_CPQ_Setup();
        SBQQ__CustomScript__c cs = new SBQQ__CustomScript__c();
        cs.Name = util.customScriptName;
        insert cs;
        util.installCustomScript();
        Test.stopTest();
        System.assert(null != [SELECT Id FROM SBQQ__CustomScript__c WHERE Name = :(util.customScriptName)].Id);
    }
}