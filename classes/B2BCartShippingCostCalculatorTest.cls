@IsTest
private class B2BCartShippingCostCalculatorTest {

    @testSetup static void setup() {
        Account testAccount = new Account(Name='TestAccount');
        insert testAccount;
        WebStore testWebStore = new WebStore(Name='TestWebStore');
        insert testWebStore;

        Account account = [SELECT Id FROM Account WHERE Name='TestAccount' LIMIT 1];
        WebStore webStore = [SELECT Id FROM WebStore WHERE Name='TestWebStore' LIMIT 1];
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id);
        insert cart;

        CartDeliveryGroup cartDeliveryGroup = new CartDeliveryGroup(CartId=cart.Id, Name='Default Delivery 1');
        insert cartDeliveryGroup;

        CartItem cartItem = new CartItem(CartId=cart.Id, Type='Product', Name='TestProduct', CartDeliveryGroupId=cartDeliveryGroup.Id, Quantity = 4);
        insert cartItem;
    }


//    @IsTest
//    static void calculateShippingCostSuccessTest() {
//        B2BCartShippingCostCalculator cartShippingCostCalculator = new B2BCartShippingCostCalculator();
//        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
//        WebCart webCart = [SELECT Id FROM WebCart WHERE Name='Cart' LIMIT 1];
//        integInfo.jobId = null;
//
//        Test.startTest();
//        sfdc_checkout.IntegrationStatus integrationResult = cartShippingCostCalculator.startCartProcessAsync(integInfo, webCart.Id);
//        System.assertEquals(sfdc_checkout.IntegrationStatus.Status.SUCCESS, integrationResult.status);
//        Test.stopTest();
//    }

    @isTest
    static void calculateShippingCostFailTest() {
        B2BCartShippingCostCalculator cartShippingCostCalculator = new B2BCartShippingCostCalculator();
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        integInfo.jobId = null;
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
        List<cartItem> cartItems = [SELECT Id, Quantity from cartItem WHERE CartId = :webCart.Id AND Type = 'Product' WITH SECURITY_ENFORCED];
        cartItems[0].Quantity = null;
        update cartItems[0];

        Test.startTest();
        sfdc_checkout.IntegrationStatus integrationResult = cartShippingCostCalculator.startCartProcessAsync(integInfo, webCart.Id);

        System.assertEquals(sfdc_checkout.IntegrationStatus.Status.FAILED, integrationResult.status);
        List<CartValidationOutput> cartValidationOutputs = [SELECT Id FROM CartValidationOutput WHERE Level = 'Error'];
        System.assertEquals(1, cartValidationOutputs.size());
        Test.stopTest();
    }

    @isTest
    static void calculateShippingCostFailThrowExceptionTest() {
        B2BCartShippingCostCalculator cartShippingCostCalculator = new B2BCartShippingCostCalculator();
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        integInfo.jobId = null;
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
        delete [SELECT Id, DeliveryMethodId FROM CartDeliveryGroup WHERE CartId = :webCart.Id];

        Test.startTest();
        sfdc_checkout.IntegrationStatus integrationResult = cartShippingCostCalculator.startCartProcessAsync(integInfo, webCart.Id);
        System.assertEquals(sfdc_checkout.IntegrationStatus.Status.FAILED, integrationResult.status);
        List<CartValidationOutput> cartValidationOutputs = [SELECT Id FROM CartValidationOutput WHERE Level = 'Error'];
        System.assertEquals(1, cartValidationOutputs.size());
        Test.stopTest();
    }
}