/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 07/12/2021
* @description:TestClass for Invoice Trigger Handler class
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Srikanth P       M-001        26/07/2022  Added method to cover method createInvoiceAndLineItems() 
=======================================================================================================*/
@isTest
public class SFS_InvoiceTriggerHandlerTest {   
    @istest  
    public static void invoicecurrencyTest() {
        
        List<Account> acct=SFS_TestDataFactory.createAccounts(2, false);
        acct[0].AccountSource='Web';
        acct[0].IRIT_Customer_Number__c='1234';
        acct[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acct[0].IRIT_Payment_Terms__c='BANKCARD';
        insert acct[0]; 
        
        acct[1].Bill_To_Account__c = acct[0].Id;
        acct[1].AccountSource='Web';
        acct[1].name = 'test account';
        acct[1].CurrencyIsoCode = 'USD';
        acct[1].IRIT_Customer_Number__c='1234';
        acct[1].type='Prospect';	
        acct[1].ShippingPostalCode='28759';	
        acct[1].ShippingCity ='Montreat2';
        insert acct[1];
        
        //get Asset
        List<Asset> ast = SFS_TestDataFactory.createAssets(1, true);
        
        //get PriceBook
        Pricebook2 standardPricebook = SFS_TestDataFactory.getPricebook2();
        
        //get PriceBookEntry
        List<PricebookEntry> Pe = SFS_TestDataFactory.createPricebookEntry(1,true);
        
        //get ServiceContract
        List<ServiceContract> sc = SFS_TestDataFactory.createServiceAgreement(1, acct[1].Id ,true);
        
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, false);
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContractLineItem
        List<ContractLineItem> scli = SFS_TestDataFactory.createServiceAgreementLineItem(1, sc[0].Id ,true);
        
        //get Entitlement
        List<Entitlement> ent = SFS_TestDataFactory.createEntitlement(1,acct[1].Id,ast[0].Id,sc[0].Id,scli[0].Id,false);
        
        //get WorkOrder
        List<WorkOrder> wo = SFS_TestDataFactory.createWorkOrder(1, acct[1].Id ,loc[0].Id, div[0].Id, sc[0].Id, true);
        
        //get Invoice
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(2,false);
        // inv[0].SFS_Work_Order__c = wo[0].Id;
        inv[1].SFS_Service_Agreement__c = sc[0].Id;
        inv[0].SFS_Billing_Period_Start_Date__c=System.today();
        inv[0].SFS_Billing_Period_End_Date__c=System.today().addYears(2);
        inv[1].SFS_Billing_Period_Start_Date__c=System.today();
        inv[1].SFS_Billing_Period_End_Date__c=System.today().addYears(2);
        insert inv;
        
        Test.startTest();
        SFS_InvoiceTriggerHandler.invoicecurrency(inv);
        Test.stopTest();
    }
    
    @istest  
    public static void assignTaskOnInvoiceSubmissionTest() {
        
        
        List<Account> acct=SFS_TestDataFactory.createAccounts(2, false);
        acct[0].AccountSource='Web';
        acct[0].IRIT_Customer_Number__c='1234';
        acct[0].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        acct[0].IRIT_Payment_Terms__c='BANKCARD';
        insert acct[0]; 
        
        acct[1].Bill_To_Account__c = acct[0].Id;
        acct[1].AccountSource='Web';
        acct[1].name = 'test account';
        acct[1].CurrencyIsoCode = 'USD';
        acct[1].IRIT_Customer_Number__c='1234';
        acct[1].type='Prospect';	
        acct[1].ShippingPostalCode='28759';	
        acct[1].ShippingCity ='Montreat2';
        insert acct[1];
        //get Asset
        List<Asset> ast = SFS_TestDataFactory.createAssets(1, true);
        
        //get PriceBook
        Pricebook2 standardPricebook = new Pricebook2(Name = 'Standard Price Book',IsActive = true);
        insert standardPricebook;
        
        //get PriceBookEntry
        List<PricebookEntry> Pe = SFS_TestDataFactory.createPricebookEntry(1,true);
        
        //get ServiceContract
        List<ServiceContract> sc=SFS_TestDataFactory.createServiceAgreement(1,acct[1].Id,false); 
        
        List<Division__c> div = SFS_TestDataFactory.createDivisions(1, false);
        
        List<Schema.Location>  loc = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        
        //get ServiceContractLineItem
        List<ContractLineItem> scli = SFS_TestDataFactory.createServiceAgreementLineItem(1, sc[0].Id ,false);
        
        //get Entitlement
        List<Entitlement> ent = SFS_TestDataFactory.createEntitlement(1,acct[1].Id,ast[0].Id,sc[0].Id,scli[0].Id,false);
         
        List<Schema.Location>  loc1 = SFS_TestDataFactory.createLocations(1, div[0].Id, true);
        //get ServiceContract
        List<ServiceContract> sc1 = SFS_TestDataFactory.createServiceAgreement(1, acct[1].Id ,true);
        //Test.startTest();
        Map<Id,Invoice__c> invMap=new Map<Id,Invoice__c>();
        List<Invoice__c> inv3 = SFS_TestDataFactory.createInvoice(1,false);
        
        inv3[0].SFS_Service_Agreement__c = sc1[0].Id;
        inv3[0].SFS_Status__c= 'Pending';
        inv3[0].SFS_Billing_Period_Start_Date__c=System.today();
        inv3[0].SFS_Billing_Period_End_Date__c=System.today().addYears(2);
        insert inv3[0];
        
        
        List<Invoice__c> inv1 = SFS_TestDataFactory.createInvoice(1,false);
        inv1[0].SFS_Billing_Period_Start_Date__c=System.today().addDays(7);
        inv1[0].SFS_Billing_Period_End_Date__c=System.today().addYears(2);
        inv1[0].SFS_Service_Agreement__c = sc1[0].Id;
        inv1[0].SFS_Status__c= 'Pending';
        insert inv1[0];
        invMap.put(inv3[0].id,inv1[0]);
        
        Map<Id,Invoice__c> oldMap = new Map<id,Invoice__c>();
        List<Invoice__c> newList1 = new List<Invoice__c>();
        
        inv3[0].SFS_Status__c= 'Submitted';
        //inv3[0].SFS_Work_Order__c=null;
        Update inv3[0];
        
        newList1.add(inv3[0]);
        Test.startTest();
        SFS_InvoiceTriggerHandler.assignTaskOnInvoiceSubmission(newList1,invMap);
        Test.stopTest();
        
    }
    @isTest
    Public static void createInvoiceAndLineItems(){
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[1].Currency__c ='USD';
        accList[1].AccountSource='Web';
        accList[1].IRIT_Customer_Number__c='1234';
        accList[1].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        accList[1].IRIT_Payment_Terms__c='BANKCARD';
        insert accList[1];
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].Currency__c ='USD'; 
        accList[0].AccountSource='Web';
        accList[0].IRIT_Customer_Number__c='1234';
        accList[0].type='Prospect';	
        accList[0].ShippingPostalCode='28759';	
        accList[0].ShippingCity ='Montreat2';
        insert accList[0];
        Contact con = SFS_TestDataFactory.getContact();
        
        RecordType rtSC = [Select Id, Name, SObjectType FROM RecordType where Name ='Rental' AND SObjectType = 'ServiceContract'];
        
        List<ServiceContract> svcAgreementList = SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        svcAgreementList[0].SFS_PO_Value__c=100;
        insert svcAgreementList[0];
        
        OperatingHours op= new OperatingHours(Name='Normal',TimeZone='America/Chicago');
        insert op;
        
        ServiceTerritory territory=new ServiceTerritory(Name='abc',OperatingHoursId=op.Id,
                                                        SFS_Service_Report_Name__c='test service report',SFS_Service_Report_Phone__c='12364778',
                                                        IsActive=true);
        insert territory;
        
        List<Product2> productsList=SFS_TestDataFactory.createProduct(2,false);
        insert productsList;
        Pricebook2 pb=SFS_TestDataFactory.getPricebook2();
        
        List<PricebookEntry> priceBookEntryList= SFS_TestDataFactory.createPricebookEntry(1,false);
        priceBookEntryList[0].Pricebook2Id= pb.Id;
        insert priceBookEntryList;
                
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        
        List<WorkOrder> woList=SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,null,divisionsList[0].Id,null,false);
        woList[0].Status='Open';
        woList[0].SFS_PO_Number__c='1234';
        woList[0].SFS_Bill_to_Account__c=accList[1].Id;
        insert woList;
        
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,workTypeList[0].Id,false);
        woliList[0].SFS_PO_Number__c='1234';
        insert woliList;
        woliList[0].Status ='Completed';
        woliList[0].SFS_Asset_Run_Hours__c =2;
        woliList[0].SFS_Coolant_Changed__c ='No';
        update woliList[0];           
        
        List<CAP_IR_Charge__c> chargeList = SFS_TestDataFactory.createCharge(4,woList[0].Id,false);
        chargeList[1].SFS_Charge_Type__c='Expense';
        chargeList[1].SFS_Expense_Type__c = 'Air Fare';
        chargeList[2].SFS_Charge_Type__c='Expense';
        chargeList[2].SFS_Expense_Type__c = 'Freight / S&H';
        chargeList[2].CAP_IR_Description__c ='Freight / S&H';
        chargeList[3].SFS_Charge_Type__c='Expense';
        chargeList[3].SFS_Expense_Type__c = 'Freight / S&H';
        chargeList[3].CAP_IR_Description__c ='Emergency Freight Fee';
        
        String chargeRecordTypeID = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        for(CAP_IR_Charge__c c :chargeList){ 
            c.RecordTypeId = chargeRecordTypeID;
            c.SFS_Work_Order_Line_Item__c=woliList[0].Id; 
            c.CAP_IR_Date__c=system.Today();
            c.SFS_Charge_Type__c ='Expense';
        }   
        insert chargeList; 
        
        List<String> woliIds = new List<String>{woList[0].id};  
            Test.startTest();    
        SFS_InvoiceTriggerHandler.createInvoiceAndLineItems(woliIds);
           
        system.assert(!woliIds.isEmpty());
        
        
        
        List<Invoice__c> inv = SFS_TestDataFactory.createInvoice(1, false);
        inv[0].RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('SFS_Agreement_Invoice').getRecordTypeId();
        inv[0].SFS_Service_Agreement__c = svcAgreementList[0].Id;
        inv[0].SFS_Status__c = 'Submitted';
        inv[0].SFS_Billing_Period_Start_Date__c = System.today();
        inv[0].SFS_Billing_Period_End_Date__c = System.today().addYears(2);
        insert inv[0];
       
        delete inv[0];
        Test.stopTest();
    } 
    
    @isTest
    Public static void createQuotedInvoiceAndLineItems(){
        List<Account> accList=SFS_TestDataFactory.createAccounts(2,false);
        accList[1].Currency__c ='USD';
        accList[1].AccountSource='Web';
        accList[1].IRIT_Customer_Number__c='1234';
        accList[1].RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
        accList[1].IRIT_Payment_Terms__c='BANKCARD';
        insert accList[1];
        
        accList[0].Bill_To_Account__c=accList[1].Id;
        accList[0].Currency__c ='USD'; 
        accList[0].AccountSource='Web';
        accList[0].IRIT_Customer_Number__c='1234';
        accList[0].type='Prospect';	
        accList[0].ShippingPostalCode='28759';	
        accList[0].ShippingCity ='Montreat2';
        insert accList[0];
        Contact con = SFS_TestDataFactory.getContact();
        
        RecordType rtSC = [Select Id, Name, SObjectType FROM RecordType where Name ='Rental' AND SObjectType = 'ServiceContract'];
        
        List<ServiceContract> svcAgreementList=SFS_TestDataFactory.createServiceAgreement(1,accList[0].Id,false);
        insert svcAgreementList;
        
        OperatingHours op= new OperatingHours(Name='Normal',TimeZone='America/Chicago');
        insert op;
        
        List<Product2> productsList=SFS_TestDataFactory.createProduct(2,false);
        insert productsList;
        Pricebook2 pb=SFS_TestDataFactory.getPricebook2();
        
        List<PricebookEntry> priceBookEntryList= SFS_TestDataFactory.createPricebookEntry(1,false);
        priceBookEntryList[0].Pricebook2Id= pb.Id;
        insert priceBookEntryList;
        
        List<Division__C> divisionsList= SFS_TestDataFactory.createDivisions(1, true);
        
        cafsl__Oracle_Quote__c oq = new cafsl__Oracle_Quote__c(Name = 'Test');
        insert oq;
        
        List<WorkOrder> woList = SFS_TestDataFactory.createWorkOrder(1,accList[0].Id,null,divisionsList[0].Id,null,false);
        woList[0].Status = 'Open';
        woList[0].SFS_PO_Number__c  ='1234';
        woList[0].SFS_Bill_to_Account__c = accList[1].Id;
        woList[0].SFS_Oracle_Quote__c = oq.Id;
        woList[0].SFS_Requested_Payment_Terms__c='BANKCARD';
        insert woList;
        
        List<WorkType> workTypeList=SFS_TestDataFactory.createWorkType(1,false);
        insert workTypeList;
        
        List<WorkOrderLineItem> woliList=SFS_TestDataFactory.createWorkOrderLineItem(1,woList[0].Id,workTypeList[0].Id,false);
        woliList[0].SFS_PO_Number__c='1234';
        insert woliList;
        woliList[0].Status ='Completed';
        woliList[0].SFS_Asset_Run_Hours__c =2;
        woliList[0].SFS_Coolant_Changed__c ='No';
        update woliList[0];
        
        List<CAP_IR_Charge__c> chargeList = SFS_TestDataFactory.createCharge(2,woList[0].Id,false);
        String chargeRecordTypeID = Schema.SObjectType.CAP_IR_Charge__c.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        for(CAP_IR_Charge__c c :chargeList){ 
            c.RecordTypeId = chargeRecordTypeID;
            c.SFS_Work_Order__c =woList[0].Id;
            c.CAP_IR_Date__c=system.Today();
            c.SFS_Charge_Type__c= 'Time';
            c.SFS_Quoted_Charge__c = true;
        }   
        insert chargeList;
        
        List<String> woIds = new List<String>{woList[0].id};   
            Test.startTest();    
        SFS_InvoiceTriggerHandler.createInvoiceAndLineItems(woIds);
        Test.stopTest();   
        system.assert(!woIds.isEmpty());    
    }
}