<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lead_Mis_assigned</fullName>
        <description>Lead - Mis-assigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>njh@gdi.prd</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>gdcomp.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Lead_Mis_assigned</template>
    </alerts>
    <alerts>
        <fullName>Lead_Mis_assigned_Emco</fullName>
        <ccEmails>assist@emcowheaton.com</ccEmails>
        <description>Lead - Mis-assigned - Emco</description>
        <protected>false</protected>
        <recipients>
            <recipient>sfdcadmin2@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>GDI_System_Emails/Lead_Mis_assigned</template>
    </alerts>
    <alerts>
        <fullName>Lead_Mis_assigned_HP</fullName>
        <ccEmails>HighPressure.RED@gardnerdenver.com</ccEmails>
        <description>Lead - Mis-assigned - High Pressure</description>
        <protected>false</protected>
        <recipients>
            <recipient>sfdcadmin2@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>GDI_System_Emails/Lead_Mis_assigned</template>
    </alerts>
    <alerts>
        <fullName>Lead_Mis_assigned_Hoffman</fullName>
        <ccEmails>info.hoffmanlamson@gardnerdenver.com</ccEmails>
        <description>Lead - Mis-assigned - Hoffman</description>
        <protected>false</protected>
        <recipients>
            <recipient>sfdcadmin2@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>GDI_System_Emails/Lead_Mis_assigned</template>
    </alerts>
    <alerts>
        <fullName>Lead_Mis_assigned_Medical</fullName>
        <ccEmails>thomas.products@gardnerdenvermedical.com</ccEmails>
        <description>Lead - Mis-assigned - Medical</description>
        <protected>false</protected>
        <recipients>
            <recipient>sfdcadmin2@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>GDI_System_Emails/Lead_Mis_assigned</template>
    </alerts>
    <alerts>
        <fullName>Lead_Mis_assigned_Nash</fullName>
        <ccEmails>nash@gardnerdenver.com</ccEmails>
        <description>Lead - Mis-assigned - Nash</description>
        <protected>false</protected>
        <recipients>
            <recipient>sfdcadmin2@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>GDI_System_Emails/Lead_Mis_assigned</template>
    </alerts>
    <alerts>
        <fullName>Lead_Mis_assigned_Other</fullName>
        <ccEmails>dgleads.americas@gardnerdenver.com</ccEmails>
        <description>Lead - Mis-assigned - Other</description>
        <protected>false</protected>
        <recipients>
            <recipient>sfdcadmin2@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>GDI_System_Emails/Lead_Mis_assigned</template>
    </alerts>
    <alerts>
        <fullName>Lead_Mis_assigned_PIP</fullName>
        <ccEmails>info@pumpingperfected.com</ccEmails>
        <description>Lead - Mis-assigned - PIP</description>
        <protected>false</protected>
        <recipients>
            <recipient>sfdcadmin2@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>GDI_System_Emails/Lead_Mis_assigned</template>
    </alerts>
    <alerts>
        <fullName>Lead_Send_notification_to_Distributor</fullName>
        <description>Lead - Send notification to Distributor</description>
        <protected>false</protected>
        <recipients>
            <field>Distributor_Lead_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GDI_System_Emails/Lead_Send_to_Assigned_Disti</template>
    </alerts>
    <alerts>
        <fullName>Lead_Send_notification_to_Owner</fullName>
        <description>Lead - Send notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>gdcomp.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_Region</fullName>
        <description>assign region to leads for Demand Generation reporting</description>
        <field>Demand_Generation_Region__c</field>
        <formula>Case( FLD_Country__c,
&quot;TJ&quot;, &quot;Middle East&quot;,
&quot;DZ&quot;, &quot;MED1 (France)&quot;,
&quot;AD&quot;, &quot;MED3 (Spain)&quot;,
&quot;AG&quot;, &quot;Latin America&quot;,
&quot;AO&quot;, &quot;Africa&quot;,
&quot;AR&quot;, &quot;Latin America&quot;,
&quot;AU&quot;, &quot;Australasia&quot;,
&quot;AT&quot;, &quot;Central Europe&quot;,
&quot;AZ&quot;, &quot;CIS&quot;,
&quot;BS&quot;, &quot;Latin America&quot;,
&quot;BH&quot;, &quot;Middle East&quot;,
&quot;BD&quot;, &quot;Middle East&quot;,
&quot;BB&quot;, &quot;Latin America&quot;,
&quot;BY&quot;, &quot;CIS&quot;,
&quot;BE&quot;, &quot;MED4 (Benelux)&quot;,
&quot;BO&quot;, &quot;Latin America&quot;,
&quot;BA&quot;, &quot;NE Europe&quot;,
&quot;BR&quot;, &quot;Latin America&quot;,
&quot;BN&quot;, &quot;SE Asia&quot;,
&quot;BM&quot;, &quot;Latin America&quot;,
&quot;BZ&quot;, &quot;Latin America&quot;,
&quot;BG&quot;, &quot;NE Europe&quot;,
&quot;CA&quot;, &quot;Canada&quot;,
&quot;CW&quot;, &quot;Latin America&quot;,
&quot;TD&quot;, &quot;MED1 (France)&quot;,
&quot;CL&quot;, &quot;Latin America&quot;,
&quot;CN&quot;, &quot;Greater China&quot;,
&quot;CO&quot;, &quot;Latin America&quot;,
&quot;CI&quot;, &quot;MED1 (France)&quot;,
&quot;CG&quot;, &quot;MED1 (France)&quot;,
&quot;CD&quot;, &quot;Africa&quot;,
&quot;CR&quot;, &quot;Latin America&quot;,
&quot;CU&quot;, &quot;Latin America&quot;,
&quot;HR&quot;, &quot;NE Europe&quot;,
&quot;CY&quot;, &quot;MED4 (Benelux)&quot;,
&quot;CZ&quot;, &quot;Central Europe&quot;,
&quot;DK&quot;, &quot;NE Europe&quot;,
&quot;DO&quot;, &quot;Latin America&quot;,
&quot;EC&quot;, &quot;Latin America&quot;,
&quot;EG&quot;, &quot;Middle East&quot;,
&quot;SV&quot;, &quot;Latin America&quot;,
&quot;EE&quot;, &quot;NE Europe&quot;,
&quot;FI&quot;, &quot;NE Europe&quot;,
&quot;FJ&quot;, &quot;Australasia&quot;,
&quot;FR&quot;, &quot;MED1 (France)&quot;,
&quot;GA&quot;, &quot;MED1 (France)&quot;,
&quot;GE&quot;, &quot;CIS&quot;,
&quot;DE&quot;, &quot;Central Europe&quot;,
&quot;GH&quot;, &quot;Africa&quot;,
&quot;GI&quot;, &quot;MED3 (Spain)&quot;,
&quot;GR&quot;, &quot;MED4 (Benelux)&quot;,
&quot;GP&quot;, &quot;MED1 (France)&quot;,
&quot;GT&quot;, &quot;Latin America&quot;,
&quot;GG&quot;, &quot;NE Europe&quot;,
&quot;GN&quot;, &quot;Africa&quot;,
&quot;GQ&quot;, &quot;Africa&quot;,
&quot;GY&quot;, &quot;Latin America&quot;,
&quot;HN&quot;, &quot;Latin America&quot;,
&quot;HK&quot;, &quot;Greater China&quot;,
&quot;HT&quot;, &quot;Latin America&quot;,
&quot;HU&quot;, &quot;Central Europe&quot;,
&quot;IS&quot;, &quot;NE Europe&quot;,
&quot;IN&quot;, &quot;India&quot;,
&quot;ID&quot;, &quot;SE Asia&quot;,
&quot;IR&quot;, &quot;Middle East&quot;,
&quot;IQ&quot;, &quot;Middle East&quot;,
&quot;IE&quot;, &quot;NE Europe&quot;,
&quot;IL&quot;, &quot;Middle East&quot;,
&quot;IT&quot;, &quot;MED2 (Italy)&quot;,
&quot;JM&quot;, &quot;Latin America&quot;,
&quot;JP&quot;, &quot;Japan&quot;,
&quot;JO&quot;, &quot;Middle East&quot;,
&quot;KZ&quot;, &quot;CIS&quot;,
&quot;KE&quot;, &quot;Africa&quot;,
&quot;KR&quot;, &quot;Korea&quot;,
&quot;KW&quot;, &quot;Middle East&quot;,
&quot;LV&quot;, &quot;NE Europe&quot;,
&quot;LB&quot;, &quot;Middle East&quot;,
&quot;LC&quot;, &quot;Latin America&quot;,
&quot;LR&quot;, &quot;MED1 (France)&quot;,
&quot;LY&quot;, &quot;Middle East&quot;,
&quot;LI&quot;, &quot;Central Europe&quot;,
&quot;LT&quot;, &quot;ME Europe&quot;,
&quot;LU&quot;, &quot;MED4 (Benelux)&quot;,
&quot;MK&quot;, &quot;NE Europe&quot;,
&quot;MW&quot;, &quot;Africa&quot;,
&quot;MY&quot;, &quot;SE Asia&quot;,
&quot;ML&quot;, &quot;MED1 (France)&quot;,
&quot;MT&quot;, &quot;MED4 (Benelux)&quot;,
&quot;MU&quot;, &quot;MED1 (France)&quot;,
&quot;YT&quot;, &quot;MED1 (France)&quot;,
&quot;MX&quot;, &quot;Mexico&quot;,
&quot;MD&quot;, &quot;CIS&quot;,
&quot;MC&quot;, &quot;MED1 (France)&quot;,
&quot;MA&quot;, &quot;MED1 (France)&quot;,
&quot;MM&quot;, &quot;SE Asia&quot;,
&quot;MR&quot;, &quot;Africa&quot;,
&quot;NA&quot;, &quot;Africa&quot;,
&quot;NL&quot;, &quot;MED4 (Benelux)&quot;,
&quot;NC&quot;, &quot;MED1 (France)&quot;,
&quot;NZ&quot;, &quot;Australasia&quot;,
&quot;NI&quot;, &quot;Latin America&quot;,
&quot;NG&quot;, &quot;Africa&quot;,
&quot;NO&quot;, &quot;NE Europe&quot;,
&quot;OM&quot;, &quot;Middle East&quot;,
&quot;PK&quot;, &quot;Middle East&quot;,
&quot;PA&quot;, &quot;Latin America&quot;,
&quot;PG&quot;, &quot;Australasia&quot;,
&quot;PY&quot;, &quot;Latin America&quot;,
&quot;PE&quot;, &quot;Latin America&quot;,
&quot;PR&quot;, &quot;Latin America&quot;,
&quot;PH&quot;, &quot;SE Asia&quot;,
&quot;PL&quot;, &quot;Central Europe&quot;,
&quot;PS&quot;, &quot;Middle East&quot;,
&quot;PT&quot;, &quot;MED3 (Spain)&quot;,
&quot;QA&quot;, &quot;Middle East&quot;,
&quot;RO&quot;, &quot;NE Europe&quot;,
&quot;RU&quot;, &quot;CIS&quot;,
&quot;SM&quot;, &quot;MED2 (Italy)&quot;,
&quot;SA&quot;, &quot;Middle East&quot;,
&quot;SN&quot;, &quot;MED1 (France)&quot;,
&quot;RS&quot;, &quot;NE Europe&quot;,
&quot;SC&quot;, &quot;MED1 (France)&quot;,
&quot;SL&quot;, &quot;MED1 (France)&quot;,
&quot;SG&quot;, &quot;SE Asia&quot;,
&quot;SK&quot;, &quot;Central Europe&quot;,
&quot;SI&quot;, &quot;Central Europe&quot;,
&quot;ZA&quot;, &quot;Africa&quot;,
&quot;ES&quot;, &quot;MED3 (Spain)&quot;,
&quot;LK&quot;, &quot;India&quot;,
&quot;SD&quot;, &quot;Africa&quot;,
&quot;SE&quot;, &quot;NE Europe&quot;,
&quot;CH&quot;, &quot;Central Europe&quot;,
&quot;ST&quot;, &quot;Africa&quot;,
&quot;SY&quot;, &quot;Middle East&quot;,
&quot;SZ&quot;, &quot;Africa&quot;,
&quot;TW&quot;, &quot;Greater China&quot;,
&quot;TZ&quot;, &quot;Africa&quot;,
&quot;TH&quot;, &quot;SE Asia&quot;,
&quot;TG&quot;, &quot;MED1 (France)&quot;,
&quot;TT&quot;, &quot;Latin America&quot;,
&quot;TN&quot;, &quot;MED1 (France)&quot;,
&quot;TR&quot;, &quot;Middle East&quot;,
&quot;UG&quot;, &quot;Africa&quot;,
&quot;UA&quot;, &quot;NE Europe&quot;,
&quot;AE&quot;, &quot;Middle East&quot;,
&quot;GB&quot;, &quot;NE Europe&quot;,
&quot;US&quot;, &quot;United States&quot;,
&quot;UY&quot;, &quot;Latin America&quot;,
&quot;UZ&quot;, &quot;CIS&quot;,
&quot;VE&quot;, &quot;Latin America&quot;,
&quot;VN&quot;, &quot;SE Asia&quot;,
&quot;XK&quot;, &quot;NE Europe&quot;,
&quot;YE&quot;, &quot;Middle East&quot;,
&quot;ZM&quot;, &quot;Africa&quot;,
&quot;ZW&quot;, &quot;Africa&quot;,
NULL)</formula>
        <name>Assign Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Record_Type_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Trade_Show</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_SLA_Met_Checked</fullName>
        <field>Lead_SLA_Met2__c</field>
        <literalValue>1</literalValue>
        <name>Lead SLA Met Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Accepted_Date</fullName>
        <description>Update the Lead Accepted Date to NOW()</description>
        <field>Distributor_Accepted_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Lead Accepted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>DG Region</fullName>
        <actions>
            <name>Assign_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Assign region for Demand Generation reporting</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead SLA Met</fullName>
        <actions>
            <name>Lead_SLA_Met_Checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.TSM_Assigned_within_48_Hrs__c</field>
            <operation>equals</operation>
            <value>TRUE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Distributor_Accepted_within_48_Hrs__c</field>
            <operation>equals</operation>
            <value>TRUE</value>
        </criteriaItems>
        <description>If TSM assigns lead to distributor and distributor accepts lead within 24 hours, then box is checked.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead is Accepted</fullName>
        <actions>
            <name>Update_Lead_Accepted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Distributor_Lead_Status__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Distributor_Accepted_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If a lead is converted but the Accepted Date is Null, update the accepted date to Now()</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead is Converted but Not Accepted</fullName>
        <actions>
            <name>Update_Lead_Accepted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Distributor_Accepted_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If a lead is converted but the Accepted Date is Null, update the accepted date to Now()</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Record Type</fullName>
        <actions>
            <name>Lead_Record_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Trade Show</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
