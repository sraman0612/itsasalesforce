<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SFS_Update_RA_Is_Approved</fullName>
        <field>FSL__Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update RA Is Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_RA_Is_Approved1</fullName>
        <field>FSL__Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update RA Is Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_RA_Status_to_Approved</fullName>
        <field>SFS_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update RA Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_RA_Status_to_Approved1</fullName>
        <field>SFS_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update RA Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_RA_Status_to_New</fullName>
        <field>SFS_Status__c</field>
        <literalValue>New</literalValue>
        <name>Update RA Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_RA_Status_to_New1</fullName>
        <field>SFS_Status__c</field>
        <literalValue>New</literalValue>
        <name>Update RA Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_RA_Status_to_New2</fullName>
        <field>SFS_Status__c</field>
        <literalValue>New</literalValue>
        <name>Update RA Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_RA_Status_to_Submitted</fullName>
        <field>SFS_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update RA Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_RA_Status_to_Submitted1</fullName>
        <field>SFS_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update RA Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
