<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_SCA_Record_Name</fullName>
        <field>Name</field>
        <formula>IF(Active__c = TRUE,

&quot;Active: &quot; + TEXT(Priority__c) + &quot; - &quot; + TEXT(Hours__c) + &quot; - &quot; + Service_Center__r.Name,

&quot;Inactive: &quot; + TEXT(Priority__c) + &quot; - &quot; + TEXT(Hours__c) + &quot; - &quot; + Service_Center__r.Name)</formula>
        <name>Account SCA - Record Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Account SCA - Default Name</fullName>
        <actions>
            <name>Account_SCA_Record_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The name of the Account SCA record should default to the name of the Service Center to ensure logical searching when requesting a Service Center on an Account or Case.</description>
        <formula>Service_Center__c &lt;&gt; null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
