<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AirFilterQTY</fullName>
        <field>Air_Filter_Qty__c</field>
        <formula>VALUE(RIGHT(TEXT(FIND(&quot;(&quot;, Air_Filter_Installed__c)),1))</formula>
        <name>AirFilterQTY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AirFilterQTY_2</fullName>
        <field>Air_Filter_Qty__c</field>
        <formula>VALUE(SUBSTITUTE( 

SUBSTITUTE(Air_Filter_Installed__c, LEFT(Air_Filter_Installed__c, FIND(&quot;)&quot;, Air_Filter_Installed__c) - 1 ), NULL), 

RIGHT(Air_Filter_Installed__c, LEN(Air_Filter_Installed__c) - FIND(&quot;(&quot;, Air_Filter_Installed__c) + 1), NULL))</formula>
        <name>AirFilterQTY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Air_Filter_Number</fullName>
        <field>Air_Filter_Number__c</field>
        <formula>LEFT( Air_Filter_Installed__c , FIND(&quot; &quot;, Air_Filter_Installed__c))</formula>
        <name>Air Filter Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Air_Filter_QTY</fullName>
        <field>Air_Filter_Qty__c</field>
        <formula>VALUE(IF(ISBLANK(Air_Filter_Installed__c), &quot;0&quot;, 
(( 
MID( 
Air_Filter_Installed__c, 
FIND(&quot;(&quot;, Air_Filter_Installed__c)+1, 
FIND(&quot;)&quot;, Air_Filter_Installed__c) - FIND(&quot;(&quot;, Air_Filter_Installed__c)-1 
))))) 
+ 
VALUE(IF(ISBLANK(Air_Filter_Left_For_Inventory__c), &quot;0&quot;, 
(( 
MID( 
Air_Filter_Left_For_Inventory__c, 
FIND(&quot;(&quot;, Air_Filter_Left_For_Inventory__c)+1, 
FIND(&quot;)&quot;, Air_Filter_Left_For_Inventory__c) - FIND(&quot;(&quot;, Air_Filter_Left_For_Inventory__c)-1 
)))))</formula>
        <name>Air Filter QTY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cabinet_Filter_Number</fullName>
        <field>Cabinet_Filter_Number__c</field>
        <formula>LEFT(Cabinet_Filter_Installed__c, FIND(&quot; &quot;, Cabinet_Filter_Installed__c))</formula>
        <name>Cabinet Filter Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cabinet_Filter_Qty</fullName>
        <field>Cabinet_Filter_Qty__c</field>
        <formula>VALUE(IF(ISBLANK(Cabinet_Filter_Installed__c), &quot;0&quot;,
(( 
MID( 
Cabinet_Filter_Installed__c, 
FIND(&quot;(&quot;, Cabinet_Filter_Installed__c)+1, 
FIND(&quot;)&quot;, Cabinet_Filter_Installed__c) - FIND(&quot;(&quot;, Cabinet_Filter_Installed__c)-1 
))))) 
+ 
VALUE(IF(ISBLANK(Cabinet_Filter_Left_For_Inventory__c), &quot;0&quot;, 
(( 
MID( 
Cabinet_Filter_Left_For_Inventory__c, 
FIND(&quot;(&quot;, Cabinet_Filter_Left_For_Inventory__c)+1, 
FIND(&quot;)&quot;, Cabinet_Filter_Left_For_Inventory__c) - FIND(&quot;(&quot;, Cabinet_Filter_Left_For_Inventory__c)-1 
)))))</formula>
        <name>Cabinet Filter Qty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Control_Box_Filter_Number</fullName>
        <field>Control_Box_Filter_Number__c</field>
        <formula>LEFT( Control_Box_Filter_Installed__c , FIND(&quot; &quot;, Control_Box_Filter_Installed__c))</formula>
        <name>Control Box Filter Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Control_Box_Filter_Qty</fullName>
        <field>Control_Box_Filter_Qty__c</field>
        <formula>VALUE(IF(ISBLANK(Control_Box_Filter_Installed__c), &quot;0&quot;, 
(( 
MID( 
Control_Box_Filter_Installed__c, 
FIND(&quot;(&quot;, Control_Box_Filter_Installed__c)+1, 
FIND(&quot;)&quot;, Control_Box_Filter_Installed__c) - FIND(&quot;(&quot;, Control_Box_Filter_Installed__c)-1 
))))) 
+ 
VALUE(IF(ISBLANK(Control_Box_Filter_Left_for_Inventory__c), &quot;0&quot;, 
(( 
MID( 
Control_Box_Filter_Left_for_Inventory__c, 
FIND(&quot;(&quot;, Control_Box_Filter_Left_for_Inventory__c)+1, 
FIND(&quot;)&quot;, Control_Box_Filter_Left_for_Inventory__c) - FIND(&quot;(&quot;, Control_Box_Filter_Left_for_Inventory__c)-1 
)))))</formula>
        <name>Control Box Filter Qty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lubricant_Filter_Number</fullName>
        <field>Lubricant_Filter_Number__c</field>
        <formula>LEFT( Lubricant_Filter_Installed__c , FIND(&quot; &quot;, Lubricant_Filter_Installed__c))</formula>
        <name>Lubricant Filter Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lubricant_Filter_Qty</fullName>
        <field>Lubricant_Filter_Qty__c</field>
        <formula>VALUE(IF(ISBLANK(Lubricant_Filter_Installed__c), &quot;0&quot;, 
(( 
MID( 
Lubricant_Filter_Installed__c, 
FIND(&quot;(&quot;, Lubricant_Filter_Installed__c)+1, 
FIND(&quot;)&quot;, Lubricant_Filter_Installed__c) - FIND(&quot;(&quot;, Lubricant_Filter_Installed__c)-1 
))))) 
+ 
VALUE(IF(ISBLANK(Lubricant_Filter_Left_For_Inventory__c), &quot;0&quot;, 
(( 
MID( 
Lubricant_Filter_Left_For_Inventory__c, 
FIND(&quot;(&quot;, Lubricant_Filter_Left_For_Inventory__c)+1, 
FIND(&quot;)&quot;, Lubricant_Filter_Left_For_Inventory__c) - FIND(&quot;(&quot;, Lubricant_Filter_Left_For_Inventory__c)-1 
)))))</formula>
        <name>Lubricant Filter Qty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lubricant_Number</fullName>
        <field>Lubricant_Part_Number__c</field>
        <formula>LEFT(Lubricant__c, FIND(&quot;:&quot;, Lubricant__c) - 1)</formula>
        <name>Lubricant Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Separator_Number</fullName>
        <field>Separator_Number__c</field>
        <formula>LEFT( Separator_Installed__c , FIND(&quot; &quot;, Separator_Installed__c))</formula>
        <name>Separator Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Separator_Qty</fullName>
        <field>Separator_Qty__c</field>
        <formula>VALUE(IF(ISBLANK(Separator_Installed__c), &quot;0&quot;, 
(( 
MID( 
Separator_Installed__c, 
FIND(&quot;(&quot;, Separator_Installed__c)+1, 
FIND(&quot;)&quot;, Separator_Installed__c) - FIND(&quot;(&quot;, Separator_Installed__c)-1 
))))) 
+ 
VALUE(IF(ISBLANK(Separator_Left_For_Inventory__c), &quot;0&quot;, 
(( 
MID( 
Separator_Left_For_Inventory__c, 
FIND(&quot;(&quot;, Separator_Left_For_Inventory__c)+1, 
FIND(&quot;)&quot;, Separator_Left_For_Inventory__c) - FIND(&quot;(&quot;, Separator_Left_For_Inventory__c)-1 
)))))</formula>
        <name>Separator Qty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Parse Part numbers</fullName>
        <actions>
            <name>Air_Filter_QTY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cabinet_Filter_Qty</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Control_Box_Filter_Qty</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lubricant_Filter_Qty</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lubricant_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Separator_Qty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_History__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
