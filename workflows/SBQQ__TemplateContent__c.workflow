<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>QS_Fix_Template_Content_Color</fullName>
        <description>Used to prevent Quote Document errors due to migration using csv files. Some csv editors truncate &quot;000000&quot; to a &quot;0&quot; which does not resolve to a proper hex color and causes the document generation engine to fail</description>
        <field>SBQQ__TextColor__c</field>
        <formula>&quot;000000&quot;</formula>
        <name>QS Fix Template Content Color</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>QS Fix Template Content Color</fullName>
        <actions>
            <name>QS_Fix_Template_Content_Color</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__TemplateContent__c.SBQQ__TextColor__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Used to prevent Quote Document errors due to migration using csv files.  Some csv editors truncate &quot;000000&quot; to a &quot;0&quot; which does not resolve to a proper hex color and causes the document generation engine to fail</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
