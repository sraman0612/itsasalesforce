<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Task_Summary_Field_Update</fullName>
        <field>Summary__c</field>
        <formula>IF(LEN(Description) &lt;= 250, Description, &quot;Description is too long to display.&quot;)</formula>
        <name>Task Summary Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Task - Comments to Summary Copy</fullName>
        <actions>
            <name>Task_Summary_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the information in a Task Comment field to a custom &apos;Summary&apos; field to display on related lists.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
