<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SFS_Cybersource_Authorization_Fail_Email_Notification</fullName>
        <description>Cybersource Authorization Fail Email Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>ingersollrand.service@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SFS_CyberSource_Auth_Failed</template>
    </alerts>
    <alerts>
        <fullName>SFS_Service_Agreement_Activation_Alert_Scheduling_Contact</fullName>
        <description>SFS Service Agreement Activation Alert-Scheduling Contact</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/SFS_ServiceAgreement_Noti_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>SFS_Service_Agreement_Activation_Alert_Service_Contact</fullName>
        <description>SFS Service Agreement Activation Alert-Service Contact</description>
        <protected>false</protected>
        <recipients>
            <field>SFS_Service_Contact1__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SFS_ServiceAgreement_Noti_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>SFS_Approved</fullName>
        <description>once approved move credit status to Approved - SIF-4667</description>
        <field>SFS_Credit_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_In_Approval_Status</fullName>
        <description>once Approval is submitted credit status will be moved to In Approval - SIF-4667</description>
        <field>SFS_Credit_Status__c</field>
        <literalValue>In Approval Process</literalValue>
        <name>In Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Rejected</fullName>
        <description>Once Rejected move the credit status to Rejected -SIF-4667</description>
        <field>SFS_Credit_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_SC_Update_Status</fullName>
        <field>SFS_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>SFS_SC_Update_Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_UpdateToPriorValue</fullName>
        <field>SFS_Agreement_Value__c</field>
        <formula>SFS_Prior_Agreement_Value__c</formula>
        <name>UpdateToPriorValue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Update_AgValue</fullName>
        <field>SFS_Agreement_Value__c</field>
        <formula>0</formula>
        <name>Update AgValue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Activation_Date</fullName>
        <field>ActivationDate</field>
        <formula>DATETIMEVALUE( NOW() )</formula>
        <name>Update Activation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>SFS_Status__c</field>
        <literalValue>APPROVED</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>NextValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_MidAtlantic</fullName>
        <field>SFS_Status__c</field>
        <literalValue>APPROVED</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_NorthCentral</fullName>
        <field>SFS_Status__c</field>
        <literalValue>APPROVED</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Northeast</fullName>
        <field>SFS_Status__c</field>
        <literalValue>APPROVED</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_SouthCentral</fullName>
        <field>SFS_Status__c</field>
        <literalValue>APPROVED</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Southwest</fullName>
        <field>SFS_Status__c</field>
        <literalValue>APPROVED</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_UpperMidwest</fullName>
        <field>SFS_Status__c</field>
        <literalValue>APPROVED</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_default</fullName>
        <field>SFS_Status__c</field>
        <literalValue>APPROVED</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>