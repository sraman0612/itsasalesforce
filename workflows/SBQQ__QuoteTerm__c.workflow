<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>QS_Set_Standard_Body_Text</fullName>
        <description>Populates the Standard Quote Term Body field with the body of the original Quote Term. Used for reporting and/or approval steps</description>
        <field>QS_Standard_Body__c</field>
        <formula>SBQQ__StandardTerm__r.SBQQ__Body__c</formula>
        <name>QS Set Standard Body Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>QS Set Standard Quote Term Body Text</fullName>
        <actions>
            <name>QS_Set_Standard_Body_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If a Quote-specific custom Quote Term is created, ie a standard Quote Term is customized, populate the Standard Quote Term Body field with the body of the original Quote Term.  Used for reporting and/or approval steps</description>
        <formula>SBQQ__StandardTerm__c &lt;&gt; null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
