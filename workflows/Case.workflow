<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CRC_Accept_Decline</fullName>
        <ccEmails>nathan.holthaus@gardnerdenver.com</ccEmails>
        <description>CRC Accept / Decline</description>
        <protected>false</protected>
        <senderAddress>national.accounts@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Managed_Care_Templates/CRC</template>
    </alerts>
    <alerts>
        <fullName>CRC_Disposition_Response_Email_Alert</fullName>
        <description>CRC Disposition Response Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>national.accounts@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Managed_Care_Templates/CRC_Disposition_Response_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Bellis_Service_Date_in_Three_Months</fullName>
        <description>Case - Bellis - Service Date in Three Months</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Service Representative</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Service Supervisor</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GDI_System_Emails/Case_Bellis_Service_Date_in_Three_Months</template>
    </alerts>
    <alerts>
        <fullName>Case_Invoice_Needs_to_be_Paid</fullName>
        <description>Case Invoice Needs to be Paid</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Center_Email_Process_Builder__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>dispatch.ms.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Initial_Notification_Managed_Care_Payment_Over_Due_1</template>
    </alerts>
    <alerts>
        <fullName>Case_Invoice_Needs_to_be_Paid2</fullName>
        <description>Case Invoice Needs to be Paid 2</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Center_Email_Process_Builder__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>dispatch.ms.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Initial_Notification_Managed_Care_Payment_Over_Due_2</template>
    </alerts>
    <alerts>
        <fullName>Case_Invoice_Needs_to_be_Paid3</fullName>
        <description>Case Invoice Needs to be Paid 3</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Center_Email_Process_Builder__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>dispatch.ms.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Initial_Notification_Managed_Care_Payment_Over_Due_3</template>
    </alerts>
    <alerts>
        <fullName>Case_Invoice_Needs_to_be_Paid4</fullName>
        <description>Case Invoice Needs to be Paid 4</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Center_Email_Process_Builder__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>dispatch.ms.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Initial_Notification_Managed_Care_Payment_Over_Due_4</template>
    </alerts>
    <alerts>
        <fullName>Case_Invoice_Needs_to_be_Paid5</fullName>
        <description>Case Invoice Needs to be Paid 5</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Center_Email_Process_Builder__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>dispatch.ms.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Initial_Notification_Managed_Care_Payment_Over_Due_5</template>
    </alerts>
    <alerts>
        <fullName>Case_Service_Report_Due</fullName>
        <description>Case Service Report Due 1</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Service_Tech_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>belliss.service.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Service_Report_Follow_Up_1</template>
    </alerts>
    <alerts>
        <fullName>Case_Service_Report_Due2</fullName>
        <description>Case Service Report Due 2</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Service_Tech_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>kevin.green@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>belliss.service.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Service_Report_Follow_Up_2</template>
    </alerts>
    <alerts>
        <fullName>Case_Service_Report_Due3</fullName>
        <description>Case Service Report Due 3</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Service_Tech_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>kevin.green@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paul.duesterhaus@gardnerdenver.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>belliss.service.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Service_Report_Follow_Up_3</template>
    </alerts>
    <alerts>
        <fullName>Send_Back_to_AI_Email_Notification</fullName>
        <ccEmails>ra1452621323611@18bca9a12dlqo8mgrvtwj22lhl7gn7dq5izqrijoymllyzwh2k.j-1qiyoeaa.na16.case.salesforce.com</ccEmails>
        <description>Send Back to AI Email Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>GDI_System_Emails/Internal_Send_Back_To_AI</template>
    </alerts>
    <alerts>
        <fullName>Send_Initial_Action_Item_Email_Include_Email_Body</fullName>
        <ccEmails>ra1438264606875@1-qf0kuyblrw5lhykja5dbrc5w4lwzwlnoc9buv32b9fk51eebl.11-c1ic9ean.cs18.case.sandbox.salesforce.com</ccEmails>
        <description>Send Initial Action Item Email - Include Email Body</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Action_Item_Escalations/Action_Item_Task_With_Email_Body</template>
    </alerts>
    <alerts>
        <fullName>Send_Initial_Action_Item_Email_Includes_Email_Body_Internal</fullName>
        <ccEmails>ra1452621323611@18bca9a12dlqo8mgrvtwj22lhl7gn7dq5izqrijoymllyzwh2k.j-1qiyoeaa.na16.case.salesforce.com</ccEmails>
        <description>Send Initial Action Item Email - Includes Email Body INTERNAL</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>GDI_System_Emails/Internal_Send_Back_To_AI</template>
    </alerts>
    <alerts>
        <fullName>Send_Initial_Action_Item_Email_No_Email_Body</fullName>
        <ccEmails>ra1438264606875@1-qf0kuyblrw5lhykja5dbrc5w4lwzwlnoc9buv32b9fk51eebl.11-c1ic9ean.cs18.case.sandbox.salesforce.com</ccEmails>
        <description>Send Initial Action Item Email - No Email Body</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Action_Item_Escalations/Action_Item_Task_No_Email_Body</template>
    </alerts>
    <alerts>
        <fullName>bellissparts_cs_qcy_with</fullName>
        <description>New Case Creation Notification - With Contact Information - bellissparts.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>bellissparts.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_with_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>bellissparts_cs_qcy_without</fullName>
        <description>New Case Creation Notification - Without Contact Information - bellissparts.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>bellissparts.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_without_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>blower_cs_qcy_with</fullName>
        <description>New Case Creation Notification - With Contact Information - blower.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>blower.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_with_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>blower_cs_qcy_without</fullName>
        <description>New Case Creation Notification - Without Contact Information - blower.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>blower.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_without_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>champion_cs_qcy_with</fullName>
        <description>New Case Creation Notification - With Contact Information - champion.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>champion.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_with_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>champion_cs_qcy_without</fullName>
        <description>New Case Creation Notification - Without Contact Information - champion.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>champion.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_without_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>highpressuremachines_cs_qcy_with</fullName>
        <description>New Case Creation Notification - With Contact Information - highpressuremachines.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>highpressuremachines.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_with_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>highpressuremachines_cs_qcy_without</fullName>
        <description>New Case Creation Notification - Without Contact Information - highpressuremachines.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>highpressuremachines.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_without_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>locomotive_cs_qcy_with</fullName>
        <description>New Case Creation Notification - With Contact Information - locomotive.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>locomotive.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_with_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>locomotive_cs_qcy_without</fullName>
        <description>New Case Creation Notification - Without Contact Information - locomotive.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>locomotive.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_without_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>mobile_cs_qcy_with</fullName>
        <description>New Case Creation Notification - With Contact Information - mobile.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>mobile.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_with_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>mobile_cs_qcy_without</fullName>
        <description>New Case Creation Notification - Without Contact Information - mobile.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>mobile.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_without_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>vacuum_cs_qcy_with</fullName>
        <description>New Case Creation Notification - With Contact Information - vacuum.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>vacuum.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_with_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>vacuum_cs_qcy_without</fullName>
        <description>New Case Creation Notification - Without Contact Information - vacuum.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>vacuum.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_without_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>vacuumsystems_cs_qcy_with</fullName>
        <description>New Case Creation Notification - With Contact Information - vacuumsystems.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>vacuumsystems.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_with_Contact_Info</template>
    </alerts>
    <alerts>
        <fullName>vacuumsystems_cs_qcy_without</fullName>
        <description>New Case Creation Notification - Without Contact Information - vacuumsystems.cs.qcy@gardnerdenver.com</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>vacuumsystems.cs.qcy@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GDI_System_Emails/Auto_Email_New_Case_without_Contact_Info</template>
    </alerts>
    <fieldUpdates>
        <fullName>AI_Ref_ID_Field_Update</fullName>
        <field>Action_Item_Reference_ID__c</field>
        <formula>IF( NOT(ISBLANK(Email_Ref_ID__c)) ,  Email_Ref_ID__c , &apos;&apos;)</formula>
        <name>AI Ref ID Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRC_Email_Sent_Field_Update</fullName>
        <field>Email_Sent_to_CRC__c</field>
        <literalValue>1</literalValue>
        <name>CRC Email Sent Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CRC_Field_Update</fullName>
        <field>CRC__c</field>
        <literalValue>1</literalValue>
        <name>CRC Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Action_Item_Subject_Name_Update</fullName>
        <field>Subject</field>
        <formula>AI_Email_Subject__c</formula>
        <name>Case Action Item Subject - Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CBRE_Walmart_Contact_Name</fullName>
        <field>Contact_Name__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;Location Contact: &apos;) , CONTAINS(Description, &apos;Location Notes&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Contact: &quot;, Description) + 17), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Notes&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - CBRE Walmart - Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CBRE_Walmart_Customer_Priority</fullName>
        <field>Priority__c</field>
        <formula>IF( 

CONTAINS(Description, &apos;WO Edited:&apos;), 

SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Status:&quot;, Description) + 1), NULL), 


IF( 
AND( CONTAINS(Description, &apos;Priority:&apos;) , CONTAINS(Description, &apos;Tracking #:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Tracking #:&quot;, Description) + 1), NULL), 
NULL))</formula>
        <name>Case - CBRE Walmart - Customer Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CBRE_Walmart_DNE_Value</fullName>
        <field>DNE_Value__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;NTE: &apos;) , CONTAINS(Description, &apos;Scheduled Date:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;NTE: &quot;, Description) + 4), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Scheduled Date:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - CBRE Walmart - DNE Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CBRE_Walmart_Email_Description</fullName>
        <field>Email_Description__c</field>
        <formula>IF( 

AND(CONTAINS(Description, &apos;Description1: &apos;), CONTAINS(Description, &apos;Category&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Description1: &quot;, Description) + 13), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Category&quot;, Description) + 1), NULL), 

NULL)</formula>
        <name>Case - CBRE Walmart - Email Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CBRE_Walmart_Location_Phone</fullName>
        <field>Store_Contact_Phone__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;Location Phone:&apos;) , CONTAINS(Description, &apos;Location Fax:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Phone:&quot;, Description) + 15), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Fax:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - CBRE Walmart - Location Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CBRE_Walmart_Store_Contact_Name</fullName>
        <field>Store_Contact_Name__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;Caller:&apos;) , CONTAINS(Description, &apos;Problem:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Caller:&quot;, Description) + 7), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Problem:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - CBRE Walmart - Store Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CBRE_Walmart_Store_Number</fullName>
        <field>Store_Number__c</field>
        <formula>IF( 

CONTAINS(Description, &apos;Store ID: &apos;), 

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Store ID: &quot;, Description) + 14), NULL), 4)), 

IF( 

CONTAINS(Description, &apos;Location: &apos;), 

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location: &quot;, Description) + 9), NULL), 4)), 

NULL))</formula>
        <name>Case - CBRE Walmart - Store Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CBRE_Walmart_WO_Num</fullName>
        <field>Customer_WO__c</field>
        <formula>IF( 

CONTAINS(Description, &apos;WO #: &apos;), 

LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;WO #: &quot;, Description) + 5), NULL), 9), 

NULL)</formula>
        <name>Case - CBRE Walmart - WO Num</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CarMax_Contact_Name</fullName>
        <field>Contact_Name__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;Location Contact: &apos;) , CONTAINS(Description, &apos;Location Notes&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Contact: &quot;, Description) + 17), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Notes&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - CarMax - Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CarMax_DNE_Value</fullName>
        <field>DNE_Value__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;NTE: &apos;) , CONTAINS(Description, &apos;Scheduled Date:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;NTE: &quot;, Description) + 4), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Scheduled Date:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - CarMax - DNE Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CarMax_Email_Description</fullName>
        <field>Email_Description__c</field>
        <formula>IF( 

AND(CONTAINS(Description, &apos;Description1: &apos;), CONTAINS(Description, &apos;Category&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Description1: &quot;, Description) + 13), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Category&quot;, Description) + 1), NULL), 

NULL)</formula>
        <name>Case - CarMax Email Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CarMax_Location_Phone</fullName>
        <field>Store_Contact_Phone__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;Location Phone:&apos;) , CONTAINS(Description, &apos;Location Fax:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Phone:&quot;, Description) + 15), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Fax:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - CarMax - Location Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CarMax_Priority</fullName>
        <field>Priority__c</field>
        <formula>IF( 

CONTAINS(Description, &apos;WO Edited:&apos;), 

SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Status:&quot;, Description) + 1), NULL), 


IF( 
AND( CONTAINS(Description, &apos;Priority:&apos;) , CONTAINS(Description, &apos;Tracking #:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Tracking #:&quot;, Description) + 1), NULL), 
NULL))</formula>
        <name>Case - CarMax - Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_CarMax_Store_Contact_Name</fullName>
        <field>Store_Contact_Name__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;Caller:&apos;) , CONTAINS(Description, &apos;Problem:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Caller:&quot;, Description) + 7), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Problem:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - CarMax - Store Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Clear_Unread_Action_Item_Flag</fullName>
        <field>Unread_Action_Items__c</field>
        <literalValue>0</literalValue>
        <name>Case - Clear Unread Action Item Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Closed_Field_Update</fullName>
        <field>Case_Close_Date__c</field>
        <formula>NOW()</formula>
        <name>Case Closed Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Company_Update_Walmart</fullName>
        <field>Company__c</field>
        <formula>/*IF(CONTAINS(IF(

CONTAINS(Description, &apos;Store Name: &apos;),


TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Store Name: &quot;, Description) + 11), NULL), 4)),

NULL), &quot;SAM&apos;s&quot;), &quot;Sam&apos;s Club&quot;,

&quot;Walmart&quot;)*/

IF(

CONTAINS(Description, &apos;Store Name: SAM&apos;), &quot;Sam&quot;, &quot;Walmart&quot;)</formula>
        <name>Case - Company Update - Walmart</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Do_Not_Send_Acknowledgement_box</fullName>
        <field>Do_Not_Send_New_Case_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Case - Do Not Send Acknowledgement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Emergency_Priority_Update</fullName>
        <field>Priority</field>
        <literalValue>Emergency (within 4 hours)</literalValue>
        <name>Case - Emergency Priority Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Parsed_Details_CarMax_Store_No</fullName>
        <field>Store_Number__c</field>
        <formula>/*IF( 

CONTAINS(Description, &apos;Store ID: &apos;), 

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Store ID: &quot;, Description) + 9), NULL), 6)), 

IF( 

CONTAINS(Description, &apos;Location: &apos;), 

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location: &quot;, Description) + 9), NULL), 6)), 

NULL))*/ 

/*Modify formula below */ 

&quot;CM&quot;&amp;(IF( 

CONTAINS(Description, &apos;Store ID: &apos;), 

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Store ID: &quot;, Description) + 9), NULL), 6)), 

IF( 

CONTAINS(Description, &apos;Location: &apos;), 

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location: &quot;, Description) + 9), NULL), 6)), 

NULL)))</formula>
        <name>Case - Parsed Details - CarMax Store No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Parsed_Details_CarMax_WO_Num</fullName>
        <field>Customer_WO__c</field>
        <formula>IF( 

CONTAINS(Description, &apos;WO #: &apos;), 

LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;WO #: &quot;, Description) + 5), NULL), 9), 

NULL)</formula>
        <name>Case - Parsed Details - CarMax - WO Num</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Parsed_Details_Walmart_Store_No</fullName>
        <field>Store_Number__c</field>
        <formula>IF(

CONTAINS(Description, &apos;Store ID: &apos;),

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Store ID: &quot;, Description) + 9), NULL), 4)),

IF(

CONTAINS(Description, &apos;Location: &apos;),

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location: &quot;, Description) + 9), NULL), 4)),

NULL))</formula>
        <name>Case - Parsed Details - Walmart Store No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Parsed_Details_Walmart_WO_Num</fullName>
        <field>Customer_WO__c</field>
        <formula>IF(

CONTAINS(Description, &apos;WO #: &apos;), 

LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;WO #: &quot;, Description) + 5), NULL), 9),

NULL)</formula>
        <name>Case - Parsed Details - Walmart - WO Num</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_Company_Update</fullName>
        <field>Company__c</field>
        <formula>&quot;Pep Boys&quot;</formula>
        <name>Case - PepBoys - Company Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_Contact_Name</fullName>
        <field>Contact_Name__c</field>
        <formula>/*IF( 

AND( CONTAINS(Description, &apos;Requester Name: &apos;) , CONTAINS(Description, &apos;Alternate Contact:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Requester Name: &quot;, Description) + 15), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Alternate Contact:&quot;, Description) + 1), NULL),


NULL)*/

/*Modify formula below*/


IF( 

AND( CONTAINS(Description, &apos;Requesting Contact:&apos;) , CONTAINS(Description, &apos;Alternate Contact:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Requesting Contact:&quot;, Description) + 19), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Alternate Contact:&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - PepBoys - Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_DNE_Value</fullName>
        <field>DNE_Value__c</field>
        <formula>/*IF( 

AND( CONTAINS(Description, &apos;WORK ORDER NUMBER:&apos;) , CONTAINS(Description, &apos;Date Reported:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;WORK ORDER NUMBER:&quot;, Description) + 18), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Date Reported:&quot;, Description) + 1), NULL), 


NULL)*/

/*Modify formula below */ 

IF( 

AND( CONTAINS(Description, &apos;DNE:&apos;) , CONTAINS(Description, &apos;Telephone:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;DNE:&quot;, Description) + 4), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Telephone:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - PepBoys - DNE Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_Description</fullName>
        <field>Email_Description__c</field>
        <formula>/*IF( 

AND( CONTAINS(Description, &apos;Description:&apos;) , CONTAINS(Description, &apos;Mall Name:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Description:&quot;, Description) + 12), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Mall Name:&quot;, Description) + 1), NULL),


NULL)*/

/*Modify formula below */




IF( 

AND( CONTAINS(Description, &apos;WorkOrder Description&apos;) , CONTAINS(Description, &apos;Contacts&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;WorkOrder Description&quot;, Description) + 21), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Contacts&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - PepBoys - Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_Priority</fullName>
        <field>Priority__c</field>
        <formula>/*IF( 

AND( CONTAINS(Description, &apos;Priority:&apos;) , CONTAINS(Description, &apos;Work Order Status:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 12), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Work Order Status:&quot;, Description) + 1), NULL), 


NULL)*/

/*Modify formula below */


IF( 

AND( CONTAINS(Description, &apos;Priority:&apos;) , CONTAINS(Description, &apos;Work Order Status:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Work Order Status:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - PepBoys - Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_Store_Contact_Name</fullName>
        <field>Store_Contact_Name__c</field>
        <formula>/*IF( 

AND( CONTAINS(Description, &apos;Requester Name: &apos;) , CONTAINS(Description, &apos;Alternate Contact:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Requester Name: &quot;, Description) + 15), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Alternate Contact:&quot;, Description) + 1), NULL),


NULL)*/

/*Modify formula below*/


IF( 

AND( CONTAINS(Description, &apos;Requesting Contact:&apos;) , CONTAINS(Description, &apos;Alternate Contact:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Requesting Contact:&quot;, Description) + 19), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Alternate Contact:&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - PepBoys - Store Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_Store_Contact_Phone</fullName>
        <field>Store_Contact_Phone__c</field>
        <formula>/*IF( 

AND( CONTAINS(Description, &apos;DNE:&apos;) , CONTAINS(Description, &apos;Telephone:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;DNE:&quot;, Description) + 4), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Telephone:&quot;, Description) + 1), NULL), 


NULL)*/

/*Modify formula below */ 

IF( 

AND( CONTAINS(Description, &apos;Location Phone:&apos;) , CONTAINS(Description, &apos;Location Address:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Phone:&quot;, Description) + 15), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Address:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - PepBoys - Store Contact Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_Store_Number</fullName>
        <field>Store_Number__c</field>
        <formula>/*IF( 

AND( CONTAINS(Description, &apos;LocationID:&apos;) , CONTAINS(Description, &apos;Location Phone:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;LocationID:&quot;, Description) + 11), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Phone:&quot;, Description) + 1), NULL),


NULL)*/

/*Modify formula below */

&quot;PB&quot;&amp;(IF( 

AND( CONTAINS(Description, &apos;LocationID:&apos;) , CONTAINS(Description, &apos;Location Phone:&apos;)), 

SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;LocationID:&quot;, Description) + 20), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Phone:&quot;, Description) + 1), NULL),


NULL))</formula>
        <name>Case - PepBoys - Store Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PepBoys_Work_Order</fullName>
        <field>Customer_WO__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;WORK ORDER NUMBER:&apos;) , CONTAINS(Description, &apos;Date Reported:&apos;)), 



SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;WORK ORDER NUMBER:&quot;, Description) + 18), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Date Reported:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - PepBoys - Work Order#</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Precision_Tune_Store_Contact</fullName>
        <field>Store_Contact_Name__c</field>
        <formula>Account.Store_Contact_Name__c</formula>
        <name>Case - Precision Tune - Store Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Priority_Field_Update</fullName>
        <field>Priority</field>
        <literalValue>Standard (within 48 hours)</literalValue>
        <name>Case - Priority Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Service_King_Contact_Name</fullName>
        <field>Contact_Name__c</field>
        <formula>IF(

AND( CONTAINS(Description, &apos;Location Contact: &apos;) , CONTAINS(Description, &apos;Location Notes&apos;)),


SUBSTITUTE(

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Contact: &quot;, Description) + 17), NULL),

RIGHT(Description, LEN(Description) - FIND(&quot;Location Notes&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - Service King - Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Service_King_DNE_Value</fullName>
        <field>DNE_Value__c</field>
        <formula>IF(

AND( CONTAINS(Description, &apos;NTE: &apos;) , CONTAINS(Description, &apos;Scheduled Date:&apos;)),


SUBSTITUTE(

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;NTE: &quot;, Description) + 4), NULL),

RIGHT(Description, LEN(Description) - FIND(&quot;Scheduled Date:&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - Service King - DNE Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Service_King_Email_Description</fullName>
        <field>Email_Description__c</field>
        <formula>IF(

AND(CONTAINS(Description, &apos;Description1: &apos;), CONTAINS(Description, &apos;Category&apos;)),


SUBSTITUTE(

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Description1: &quot;, Description) + 13), NULL),

RIGHT(Description, LEN(Description) - FIND(&quot;Category&quot;, Description) + 1), NULL),

NULL)</formula>
        <name>Case - Service King - Email Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Service_King_Location_Phone</fullName>
        <field>Store_Contact_Phone__c</field>
        <formula>IF(

AND( CONTAINS(Description, &apos;Location Phone:&apos;) , CONTAINS(Description, &apos;Location Fax:&apos;)),


SUBSTITUTE(

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Phone:&quot;, Description) + 15), NULL),

RIGHT(Description, LEN(Description) - FIND(&quot;Location Fax:&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - Service King - Location Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Service_King_Priority</fullName>
        <field>Priority__c</field>
        <formula>IF(

CONTAINS(Description, &apos;WO Edited:&apos;),

SUBSTITUTE(

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL),

RIGHT(Description, LEN(Description) - FIND(&quot;Status:&quot;, Description) + 1), NULL),


IF(
AND( CONTAINS(Description, &apos;Priority:&apos;) , CONTAINS(Description, &apos;Tracking #:&apos;)),


SUBSTITUTE(

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL),

RIGHT(Description, LEN(Description) - FIND(&quot;Tracking #:&quot;, Description) + 1), NULL),
NULL))</formula>
        <name>Case - Service King - Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Service_King_Store_Contact_Name</fullName>
        <field>Store_Contact_Name__c</field>
        <formula>IF(

AND( CONTAINS(Description, &apos;Caller:&apos;) , CONTAINS(Description, &apos;Problem:&apos;)),


SUBSTITUTE(

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Caller:&quot;, Description) + 7), NULL),

RIGHT(Description, LEN(Description) - FIND(&quot;Problem:&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - Service King - Store Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Service_King_Store_ID</fullName>
        <field>Store_Number__c</field>
        <formula>IF(

CONTAINS(Description, &apos;Store ID: &apos;),

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Store ID: &quot;, Description) + 9), NULL), 5)),

IF(

CONTAINS(Description, &apos;Location: &apos;),

TRIM(LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location: &quot;, Description) + 9), NULL), 5)),

NULL))</formula>
        <name>Case - Service King - Store ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Service_King_WO_Num</fullName>
        <field>Customer_WO__c</field>
        <formula>IF(

CONTAINS(Description, &apos;WO #: &apos;),

LEFT(SUBSTITUTE(Description, LEFT(Description, FIND(&quot;WO #: &quot;, Description) + 5), NULL), 10),

NULL)</formula>
        <name>Case - Service King - WO Num</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Walmart_Contact_Name</fullName>
        <field>Contact_Name__c</field>
        <formula>IF(

AND( CONTAINS(Description, &apos;Location Contact: &apos;) , CONTAINS(Description, &apos;Location Notes&apos;)),


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Contact: &quot;, Description) + 17), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Notes&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - Walmart - Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Walmart_DNE_Value</fullName>
        <description>Parses DNE from the email-to-case email.</description>
        <field>DNE_Value__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;NTE: &apos;) , CONTAINS(Description, &apos;Scheduled Date:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;NTE: &quot;, Description) + 4), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Scheduled Date:&quot;, Description) + 1), NULL),


NULL)</formula>
        <name>Case - Walmart - DNE Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Walmart_Email_Description</fullName>
        <field>Email_Description__c</field>
        <formula>IF(

AND(CONTAINS(Description, &apos;Description1: &apos;), CONTAINS(Description, &apos;Category&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Description1: &quot;, Description) + 13), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Category&quot;, Description) + 1), NULL),

NULL)</formula>
        <name>Case - Walmart Email Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Walmart_Location_Phone</fullName>
        <field>Store_Contact_Phone__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;Location Phone:&apos;) , CONTAINS(Description, &apos;Location Fax:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Location Phone:&quot;, Description) + 15), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Location Fax:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - Walmart - Location Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Walmart_Priority</fullName>
        <field>Priority__c</field>
        <formula>IF( 

CONTAINS(Description, &apos;WO Edited:&apos;), 

SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Status:&quot;, Description) + 1), NULL),


IF(
AND( CONTAINS(Description, &apos;Priority:&apos;) , CONTAINS(Description, &apos;Tracking #:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Priority:&quot;, Description) + 9), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Tracking #:&quot;, Description) + 1), NULL), 
NULL))</formula>
        <name>Case - Walmart - Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Walmart_Store_Contact_Name</fullName>
        <field>Store_Contact_Name__c</field>
        <formula>IF( 

AND( CONTAINS(Description, &apos;Caller:&apos;) , CONTAINS(Description, &apos;Problem:&apos;)), 


SUBSTITUTE( 

SUBSTITUTE(Description, LEFT(Description, FIND(&quot;Caller:&quot;, Description) + 7), NULL), 

RIGHT(Description, LEN(Description) - FIND(&quot;Problem:&quot;, Description) + 1), NULL), 


NULL)</formula>
        <name>Case - Walmart - Store Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Extended_Ware</fullName>
        <field>Extended_Wear__c</field>
        <literalValue>1</literalValue>
        <name>Check Extended Ware</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Connected_Customer_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>preston.swift@gardnerdenver.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Connected Customer Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalation_Count_Clear_Field</fullName>
        <field>Escalation_Count__c</field>
        <formula>0</formula>
        <name>Escalation Count Clear Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Full_Email_Chain_Field_Update</fullName>
        <field>Full_Case_Description__c</field>
        <formula>AI_Original_Email_Body__c</formula>
        <name>Full Email Chain Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Escalation_Count</fullName>
        <field>Escalation_Count__c</field>
        <formula>(Escalation_Count__c) + 1</formula>
        <name>Increment Escalation Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inquiry_List_Field_Update</fullName>
        <field>Inquiry_Call_Type_List__c</field>
        <formula>IF( Adding_to_Order__c,&apos;Adding to Order, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Discount_Question__c,&apos;Discount Question, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Dispatch_Distributor__c,&apos;Dispatch Distributor, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Documentation_Request__c,&apos;Documentation_Request, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Dryer__c,&apos;Dryer, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Expedite__c,&apos;Expedite, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Field_Support__c,&apos;Field Support, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Freight_Damage__c,&apos;Freight Damage, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Freight_Quotes__c,&apos;Freight Quotes, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Invoice__c,&apos;Invoice, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Lead_Time__c,&apos;Lead Time, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Locate_Distributor__c,&apos;Locate Distributor, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Mis_Directed__c,&apos;Mis_Directed, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Modify_Order__c,&apos;Order Modify, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Online_Tool_Help__c,&apos;Online Tool Help, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Other__c,&apos;Other, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Order_Status__c,&apos;Order Status, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Part_Inquiry__c,&apos;Part Inquiry, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Place_Order__c,&apos;Place Order, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Pricing_Availability__c,&apos;Pricing/Availability/Lead Time, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Reman__c,&apos;Reman, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Sales_Support__c,&apos;Sales Support, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Special_Quote__c,&apos;Special Quote, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Tech_Question__c,&apos;Technical Question, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Warranty_Eligibility__c,&apos;Warranty Eligibility, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Disputed_Invoice__c,&apos;Disputed Invoice, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Quality_Complaint__c,&apos;Quality Complaint, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( SOP_Direction__c,&apos;SOP Direction, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Technical_Support__c,&apos;Technical Support, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Marketing_Sales_Corresp__c,&apos;Marketing Sales Corresp, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( NACT_WARR_Inquiry__c,&apos;NACT WARR Inquiry, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( NACT_Hand_off__c,&apos;NACT Hand off, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Advance_Notice_of_Failure__c,&apos;Advance_Notice_of_Failure, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Startup_Claim_Status__c,&apos;Startup Claim Status, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Claim_Entry_Assistance__c,&apos;Claim Entry Assistance, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Appeal_of_Disposition__c,&apos;Appeal of Disposition, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Response_to_Info_Request__c,&apos;Response to Info Request, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Question_on_Coverage__c,&apos;Question on Coverage, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Warranty_Claim_Status__c,&apos;Warranty Claim Status, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( GDI_Request_for_Info__c,&apos;GDI Request for Info, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Warranty_Eligibility_Coverage__c,&apos;Warranty Eligibility/Coverage, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Extended_Warranty__c,&apos;Extended Warranty, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Freight_Damage_Quote__c,&apos;Freight/Damage/Quote, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Ancillary_Dryer__c,&apos;Ancillary/Dryer, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Reman_Repair_Return__c,&apos;Reman - Repair/Return, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Order_Inquiry__c,&apos;Order Inquiry, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Cancel_Order__c,&apos;Order Cancel, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Information_Request__c,&apos;Information Request, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( New_Order_Return__c,&apos;Order New/RE/DR/CR, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Shipping_Issue__c,&apos;Shipping Issue, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Marketing__c,&apos;Marketing, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Warranty__c,&apos;Warranty, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Engineering__c,&apos;Engineering, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Warranty_Status__c,&apos;Warranty Status, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( ECN_Review__c,&apos;ECN Review, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Part_Number_Extension__c,&apos;Part Number Ext, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( iConn__c,&apos;iConn, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Connected_Customer__c,&apos;Connected Customer, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( ECN_Problem__c, &apos;ECN Problem, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Part_Number_New__c,&apos;Part Number New, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Part_Repl_Obs__c,&apos;Part REPL-OBS, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Sales_Views__c,&apos;Sales Views, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Stockable_Variant__c,&apos;Stockable Variant, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( CC360_Help__c,&apos;CC360 Help, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( Machine_Version_Parts_List__c,&apos;Machine Version Parts List, &apos;, &apos;&apos;)&amp;&apos; &apos;&amp;
IF( HTS_CoO_ECCN__c,&apos;HTS-CoO-ECCN, &apos;, &apos;&apos;)</formula>
        <name>Inquiry List Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsEscalatedCheckbox</fullName>
        <field>IsEscalated</field>
        <literalValue>0</literalValue>
        <name>IsEscalatedCheckbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Machine_Down_Date_Update</fullName>
        <field>Machine_Down_Date__c</field>
        <formula>TODAY()</formula>
        <name>Machine Down Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Priority_Update_to_High</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Priority Update to &apos;High&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopen_Parent_Case</fullName>
        <field>Re_opened__c</field>
        <literalValue>1</literalValue>
        <name>Reopen Parent Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_Back_to_AI_Field_Update</fullName>
        <field>Send_Back_to_AI__c</field>
        <literalValue>0</literalValue>
        <name>Send Back to AI Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serial_Number_Searchable_Field_Update</fullName>
        <field>Serial_Number_Searchable__c</field>
        <formula>Serial_Number_CC__r.Name</formula>
        <name>Serial Number (Searchable) Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Submitted_Date</fullName>
        <description>Set the quote Submitted date to Today</description>
        <field>Quote_Submitted__c</field>
        <formula>Today()</formula>
        <name>Set Quote Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Send_Initial_Email</fullName>
        <field>AI_Send_Initial_Email__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Send Initial Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Send_Initial_Email_2</fullName>
        <field>AI_Send_Initial_Email__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Send Initial Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Unread_Email_Checkbox</fullName>
        <field>Email_Waiting_Icon__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Unread Email Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Type_of_Maintenance</fullName>
        <field>Actual_Type_of_Maintenance__c</field>
        <name>Update Actual Type of Maintenance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>NextValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Blanket_PO</fullName>
        <field>Purchase_Order__c</field>
        <formula>Account.Parent.Blanket_PO__c</formula>
        <name>Update Blanket PO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Do_Not_Send_Case_Notification</fullName>
        <description>This will update the Do Not Send Case Notification Email box to be checked.</description>
        <field>Do_Not_Send_New_Case_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Update Do Not Send Case Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Initial_Email_Sent</fullName>
        <description>Updates the &apos;Initial Email Sent&apos; checkbox when an Action Item record is saved for the first time and the criteria to send is met.</description>
        <field>Initial_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Update &apos;Initial Email Sent&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Initial_Email_Sent_Email_Body</fullName>
        <field>Initial_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Update &apos;Initial Email Sent&apos; - Email Body</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_With_Prior_Value</fullName>
        <field>Prior_Value_MC__c</field>
        <formula>TEXT(PRIORVALUE( Status ))</formula>
        <name>Update With Prior Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Belliss Case - Extended Ware</fullName>
        <actions>
            <name>Check_Extended_Ware</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the serial number relating to a Belliss case has extended ware checked check the field on the new case</description>
        <formula>AND(  RecordType.Name = &apos;Belliss &amp; Morcom&apos;,  Serial_Number_CC__r.Extended_Wear__c = TRUE )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Belliss PM Date Update</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Action Item - Name Default</fullName>
        <actions>
            <name>Case_Action_Item_Subject_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>External Action Item,Internal Action Item</value>
        </criteriaItems>
        <description>Updates the Action Item &apos;Subject&apos; name to the Case Parent Name + the Contact Name.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Action Item - Uncheck Email Waiting Icon With Open AI</fullName>
        <actions>
            <name>Uncheck_Unread_Email_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>External Action Item,Internal Action Item</value>
        </criteriaItems>
        <description>When the status of an Action Item is &apos;Open&apos;, uncheck the Unread Email checkbox which drives the email icon.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - After Hours %2F Walmart Emergency %281%29</fullName>
        <actions>
            <name>CRC_Accept_Decline</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CRC_Email_Sent_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CRC_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  CONTAINS(TEXT( Priority ), &apos;Emergency&apos;),  CONTAINS( Account.Parent.Name , &apos;WAL-MART STORES INC.&apos;)/*,    AND (CreatedBy.Alias = &quot;somealias&quot;, OR(MOD( DATEVALUE(CreatedDate)  - DATE(1900, 1, 6), 7) &lt; 2, VALUE(MID(TEXT(CreatedDate),12,2))&lt;9, VALUE(MID(TEXT(CreatedDate),12,2))&gt;17))*/  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Assign Priority %27High%27 for %27High Priority Contacts%27</fullName>
        <actions>
            <name>Priority_Update_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.High_Priority_Contact__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Assign the Priority picklist to a value of &apos;High&apos; for Cases that are created for High Priority Contacts.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Bellis - Service Date in a month</fullName>
        <active>true</active>
        <description>When a case is created send a notification one month before service date to alert case owner</description>
        <formula>AND( RecordType.Name = &quot;Bellis &amp; Morcom&quot;, IF(Next_Service_Date__c - TODAY() &gt; 30, TRUE, FALSE ))</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Bellis_Service_Date_in_Three_Months</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Next_Service_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Bellis - Set Type of Main%2E</fullName>
        <actions>
            <name>Update_Actual_Type_of_Maintenance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Belliss &amp; Morcom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Belliss_Service_Type__c</field>
            <operation>equals</operation>
            <value>Preventative Maintenance</value>
        </criteriaItems>
        <description>Sets type of maintenance to be performed, based off field on the serial number. Only for Preventative Maintenance</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - CRC Disposition Response Email %283%29</fullName>
        <actions>
            <name>CRC_Disposition_Response_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.CRC_Status__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Clear Unread Action Item Flag</fullName>
        <actions>
            <name>Case_Clear_Unread_Action_Item_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Connected Customer Support</fullName>
        <actions>
            <name>Connected_Customer_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Connected Customer,iConn Pilot</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Do Not Send Acknowledgement</fullName>
        <actions>
            <name>Case_Do_Not_Send_Acknowledgement_box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.MC_Aftercooler__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Email Parsing - CBRE GWS-SA</fullName>
        <actions>
            <name>Case_CBRE_Walmart_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CBRE_Walmart_Customer_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CBRE_Walmart_DNE_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CBRE_Walmart_Email_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CBRE_Walmart_Location_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CBRE_Walmart_Store_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CBRE_Walmart_Store_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CBRE_Walmart_WO_Num</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>servicerequest@scalert.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>srvrec CBRE GWS-SA</value>
        </criteriaItems>
        <description>If a Case belongs to the Managed Care: CBRE queue for its Walmart stores, this will parse out case details from the originating email.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Email Parsing - CarMax</fullName>
        <actions>
            <name>Case_CarMax_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CarMax_DNE_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CarMax_Email_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CarMax_Location_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CarMax_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_CarMax_Store_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Parsed_Details_CarMax_Store_No</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Parsed_Details_CarMax_WO_Num</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>servicerequest@scalert.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>srvrec CarMax</value>
        </criteriaItems>
        <description>If a Case belongs to the Managed Care: CarMax queue, this will parse out case details from the originating email.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Email Parsing - Pep Boys</fullName>
        <actions>
            <name>Case_PepBoys_Company_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_PepBoys_DNE_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_PepBoys_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_PepBoys_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_PepBoys_Store_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_PepBoys_Store_Contact_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_PepBoys_Store_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_PepBoys_Work_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>PEPBoys-AutoPlus@fmpilot2.com</value>
        </criteriaItems>
        <description>If a Case belongs to the Managed Care: Pep Boys queue, this will parse out case details from the originating email.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Email Parsing - Service King</fullName>
        <actions>
            <name>Case_Service_King_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Service_King_DNE_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Service_King_Email_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Service_King_Location_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Service_King_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Service_King_Store_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Service_King_Store_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Service_King_WO_Num</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>servicerequest@scalert.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>srvrec Service King</value>
        </criteriaItems>
        <description>If a Case belongs to the Managed Care: Service King queue, this will parse out case details from the originating email.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Email Parsing - Walmart</fullName>
        <actions>
            <name>Case_Parsed_Details_Walmart_Store_No</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Parsed_Details_Walmart_WO_Num</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Walmart_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Walmart_DNE_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Walmart_Email_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Walmart_Location_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Walmart_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Walmart_Store_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>servicerequest@scalert.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>srvrec WALMART</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>srvrec Sam&amp;#39;s</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>srvrec Sam&apos;s</value>
        </criteriaItems>
        <description>If a Case belongs to the Managed Care: Walmart queue, this will parse out case details from the originating email.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Escalation Count Clear</fullName>
        <actions>
            <name>Escalation_Count_Clear_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Whenever a Case &apos;Status&apos; is changed, this means that any Escalations should be reset; meaning the Escalation Count returns to zero.</description>
        <formula>ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Increment Escalation Count</fullName>
        <actions>
            <name>Increment_Escalation_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IsEscalatedCheckbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsEscalated</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Each time Case criteria triggers an Escalation, increment the &apos;Escalation Count&apos; field on the Case by one, which will determine the Status color on the Case.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Initial Action Email Send - Includes Email Body</fullName>
        <actions>
            <name>Send_Initial_Action_Item_Email_Include_Email_Body</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Send_Initial_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Initial_Email_Sent_Email_Body</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sends an Initial Action email when a record is saved for the first time with the &apos;Send Initial Email&apos; box is checked.  Includes the original email body</description>
        <formula>OR(RecordType.Name == &apos;External Action Item&apos;, RecordType.Name == &apos;Internal Action Item&apos;) &amp;&amp;  AI_Send_Initial_Email__c == TRUE &amp;&amp;  Initial_Email_Sent__c == FALSE &amp;&amp;  AI_Add_Body_of_Original_Email__c  == TRUE &amp;&amp; ISNULL(Send_To_Queue__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Initial Action Email Send - No Email Body</fullName>
        <actions>
            <name>Send_Initial_Action_Item_Email_No_Email_Body</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Send_Initial_Email_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Initial_Email_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sends an Initial Action email when a record is saved for the first time with the &apos;Send Initial Email&apos; box is checked.  Does not include the original email body</description>
        <formula>OR(RecordType.Name == &apos;Internal Action Item&apos;, RecordType.Name == &apos;External Action Item&apos;) &amp;&amp;  AI_Send_Initial_Email__c == TRUE &amp;&amp;  Initial_Email_Sent__c == FALSE &amp;&amp;  AI_Add_Body_of_Original_Email__c  == FALSE &amp;&amp; ISNULL(Send_To_Queue__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Internal Action Item Response</fullName>
        <actions>
            <name>Send_Back_to_AI_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Back_to_AI_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Action_Item_Reference_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Back_to_AI__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send Email Alert back into Action Item when the ‘Close/Send Back to AI button on the case is selected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Priority Update</fullName>
        <actions>
            <name>Case_Emergency_Priority_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Priority__c</field>
            <operation>contains</operation>
            <value>EMG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority__c</field>
            <operation>contains</operation>
            <value>PE</value>
        </criteriaItems>
        <description>Updates the Priority on MC cases when the parsed email priority is emergency</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Searchable Serial Number Field Update</fullName>
        <actions>
            <name>Serial_Number_Searchable_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates a text field with the Case&apos;s Serial Number for searching abilities.</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Send Back To Action Item</fullName>
        <actions>
            <name>Send_Initial_Action_Item_Email_Includes_Email_Body_Internal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Send_Back_to_AI__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends Email from Case back into Action Item for Internal Purposes.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Set Ref ID Internal</fullName>
        <actions>
            <name>AI_Ref_ID_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Action Item Reference ID field as a way to send back emails into the Action Item record that created it.</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Store Contact - Precision Tune</fullName>
        <actions>
            <name>Case_Precision_Tune_Store_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>contains</operation>
            <value>Precision Tune Auto Care</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update Blanket PO</fullName>
        <actions>
            <name>Update_Blanket_PO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Parent_Record_ID__c</field>
            <operation>equals</operation>
            <value>001j000000dHZu0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Repair,Warranty,Billable &amp; Warranty,Electrical,Preventative Maintenance,Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Purchase_Order__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update Inquiry %2F Call Type List</fullName>
        <actions>
            <name>Inquiry_List_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Inquiry / Call Type List field with a list of all Inquiry Types selected.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update Prior Stage</fullName>
        <actions>
            <name>Update_With_Prior_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used for Managed Care Cases (Status)</description>
        <formula>ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - iConn IMEI Do Not Send Case Notification Email</fullName>
        <actions>
            <name>Update_Do_Not_Send_Case_Notification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iConn IMEI</value>
        </criteriaItems>
        <description>When a case is created with the iConn IMEI record type, then a field update will check the Do Not Send Notification Email box.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case-Case Closed Custom Field Update</fullName>
        <actions>
            <name>Case_Closed_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When a case is marked as &apos;Closed&apos; update the Case Closed Date custom field for reporting purposes.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Machine Down Date Update</fullName>
        <actions>
            <name>Machine_Down_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Machine Down flag gets set this sets the Date field to TODAY</description>
        <formula>AND(
((Machine_Down__c)=TRUE),
ISNULL(Machine_Down_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Submitted</fullName>
        <actions>
            <name>Set_Quote_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Quote Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Submitted__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When Case is Set to Quote Submitted status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Time Trigger Test</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Action Item,Internal Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_Relationship__c</field>
            <operation>equals</operation>
            <value>Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Open_Tasks__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Open_Tasks__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Increment_Escalation_Count</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Increment_Escalation_Count</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
