<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Concession_Won_Email</fullName>
        <ccEmails>cindy.courcelle@codezeroconsulting.com</ccEmails>
        <description>Send Concession Won Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Concession_Request_Won</template>
    </alerts>
    <fieldUpdates>
        <fullName>Blower_Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Blower</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Blower - Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CM_Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CM_Compressor</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CM-Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CP_Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CP</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CP - Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flip_Record_Type_for_DV_Systems</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DV_Systems</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Flip Record Type for DV Systems</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flip_Record_Type_for_Quick_Quote</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Quick_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Flip Record Type for Quick Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flip_Record_Type_on_Close</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Flip Record Type on Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mobile_Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Mobile</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Mobile - Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_KV_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Kinney_Vacuum</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KV Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MD_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MD_Industrial</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set MD Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MT_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MD_Truck</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set MT Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Closed_Field</fullName>
        <field>Date_Closed__c</field>
        <formula>NOW()</formula>
        <name>Update Date Closed Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Vacuum_Update_Rexord_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Vacuum</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Vacuum - Update Rexord Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Concession Request Won</fullName>
        <actions>
            <name>Send_Concession_Won_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Primary_Quote_has_Approved_Concession__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Sends an email to the TSM if this opp contains a primary quote with an approved concession request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for Blower</fullName>
        <actions>
            <name>Blower_Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISPICKVAL(Sales_Channel__c,&quot;SB&quot;) || ISPICKVAL(Sales_Channel__c,&quot;DI&quot;) || ISPICKVAL(Sales_Channel__c,&quot;GI&quot;)) &amp;&amp;  Quick_Order__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for CM</fullName>
        <actions>
            <name>CM_Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Sales_Channel__c</field>
            <operation>equals</operation>
            <value>CM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Quick_Order__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for CP</fullName>
        <actions>
            <name>CP_Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Sales_Channel__c</field>
            <operation>equals</operation>
            <value>CP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Quick_Order__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for DV Systems</fullName>
        <actions>
            <name>Flip_Record_Type_for_DV_Systems</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISPICKVAL(Sales_Channel__c,&quot;DS&quot;)) &amp;&amp;  Quick_Order__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for ER</fullName>
        <actions>
            <name>Vacuum_Update_Rexord_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Sales_Channel__c</field>
            <operation>equals</operation>
            <value>ER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Quick_Order__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for Kinney Vacuum</fullName>
        <actions>
            <name>Set_KV_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( (ISNEW()&amp;&amp; ISPICKVAL(Sales_Channel__c,&quot;KV&quot;)), (ISCHANGED( Sales_Channel__c )&amp;&amp; ISPICKVAL(Sales_Channel__c,&quot;KV&quot;)) )&amp;&amp; Quick_Order__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for MD Industrial</fullName>
        <actions>
            <name>Set_MD_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR((ISNEW()&amp;&amp; ISPICKVAL(Sales_Channel__c,&quot;MD&quot;)),(ISCHANGED( Sales_Channel__c )&amp;&amp; ISPICKVAL(Sales_Channel__c,&quot;MD&quot;)))&amp;&amp; Quick_Order__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for MD Truck</fullName>
        <actions>
            <name>Set_MT_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR((ISNEW()&amp;&amp; ISPICKVAL(Sales_Channel__c,&quot;MT&quot;)),(ISCHANGED( Sales_Channel__c )&amp;&amp; ISPICKVAL(Sales_Channel__c,&quot;MT&quot;)))&amp;&amp; Quick_Order__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for Mobile</fullName>
        <actions>
            <name>Mobile_Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISPICKVAL(Sales_Channel__c,&quot;FH&quot;) || ISPICKVAL(Sales_Channel__c,&quot;WT&quot;) || ISPICKVAL(Sales_Channel__c,&quot;MV&quot;) || ISPICKVAL(Sales_Channel__c,&quot;GT&quot;) || ISPICKVAL(Sales_Channel__c,&quot;DV&quot;) || ISPICKVAL(Sales_Channel__c,&quot;DT&quot;)) &amp;&amp;  Quick_Order__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type for Quick Quote</fullName>
        <actions>
            <name>Flip_Record_Type_for_Quick_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Quick_Order__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Stage 5 Date</fullName>
        <actions>
            <name>Flip_Record_Type_on_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Date_Closed_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Stage 5 - Closed PO Placed,Stage 0 - Not Interested or Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
