<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Concession_Req_Send_TSM_Submitted_email</fullName>
        <description>Concession Req - Send TSM Submitted email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Concession_Request_Submitted</template>
    </alerts>
</Workflow>
