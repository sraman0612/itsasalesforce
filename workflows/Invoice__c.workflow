<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SFS_Approval_Pending</fullName>
        <field>SFS_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>SFS Approval Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Approval_Required_back_to_True</fullName>
        <field>SFS_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>SFS Approval Required back to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Approval_Required_to_false</fullName>
        <field>SFS_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>SFS Approval Required to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Change_Status_to_Approved</fullName>
        <field>SFS_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>SFS Change Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFS_Rejected_Update_To_Created</fullName>
        <field>SFS_Status__c</field>
        <literalValue>Created</literalValue>
        <name>SFS Rejected -Update To Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
