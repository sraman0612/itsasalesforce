<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Machine_Startup_Form_Submission_Notification</fullName>
        <description>Machine Startup Form Submission Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Startup_Form_Completion_Notification</template>
    </alerts>
</Workflow>
