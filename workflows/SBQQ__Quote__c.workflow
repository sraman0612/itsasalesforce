<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>QS_Quote_Approved_Email</fullName>
        <description>QS Quote Approved Email</description>
        <protected>false</protected>
        <recipients>
            <field>SBQQ__SalesRep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Concession_Request_Won</template>
    </alerts>
    <fieldUpdates>
        <fullName>SF_CPQ_Set_Watermark_Shown</fullName>
        <field>SBQQ__WatermarkShown__c</field>
        <literalValue>1</literalValue>
        <name>SF CPQ: Set Watermark Shown</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_prior_value_of_markup</fullName>
        <field>SBQQ__MarkupRate__c</field>
        <formula>PRIORVALUE(SBQQ__MarkupRate__c)</formula>
        <name>Set prior value of markup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bill_To_City</fullName>
        <field>SBQQ__BillingCity__c</field>
        <formula>End_Customer_Account__r.BillingCity</formula>
        <name>Update Bill To City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bill_To_Country</fullName>
        <field>SBQQ__BillingCountry__c</field>
        <formula>End_Customer_Account__r.BillingCountry</formula>
        <name>Update Bill To Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bill_To_Name</fullName>
        <field>SBQQ__BillingName__c</field>
        <formula>End_Customer_Account__r.Name</formula>
        <name>Update Bill To Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bill_To_Postal_Code</fullName>
        <field>SBQQ__BillingPostalCode__c</field>
        <formula>End_Customer_Account__r.BillingPostalCode</formula>
        <name>Update Bill To Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bill_To_State</fullName>
        <field>SBQQ__BillingState__c</field>
        <formula>End_Customer_Account__r.BillingState</formula>
        <name>Update Bill To State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bill_To_Street</fullName>
        <field>SBQQ__BillingStreet__c</field>
        <formula>End_Customer_Account__r.BillingStreet</formula>
        <name>Update Bill To Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ship_To_City</fullName>
        <field>SBQQ__ShippingCity__c</field>
        <formula>End_Customer_Account__r.BillingCity</formula>
        <name>Update Ship To City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ship_To_Country</fullName>
        <field>SBQQ__ShippingCountry__c</field>
        <formula>End_Customer_Account__r.BillingCountry</formula>
        <name>Update Ship To Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ship_To_Name</fullName>
        <field>SBQQ__ShippingName__c</field>
        <formula>End_Customer_Account__r.Name</formula>
        <name>Update Ship To Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ship_To_Postal_Code</fullName>
        <field>SBQQ__ShippingPostalCode__c</field>
        <formula>End_Customer_Account__r.BillingPostalCode</formula>
        <name>Update Ship To Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ship_To_State</fullName>
        <field>SBQQ__ShippingState__c</field>
        <formula>End_Customer_Account__r.BillingState</formula>
        <name>Update Ship To State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ship_To_Street</fullName>
        <field>SBQQ__ShippingStreet__c</field>
        <formula>End_Customer_Account__r.BillingStreet</formula>
        <name>Update Ship To Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_USD_conversion_rate</fullName>
        <description>Whenever USD is selected for currency, this will update the conversion rate to 1.</description>
        <field>Currency_Conversion_Rate__c</field>
        <formula>1</formula>
        <name>Update USD conversion rate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Populate Bill%2FShip to fields on Quote 1</fullName>
        <actions>
            <name>Update_Bill_To_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Bill_To_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Bill_To_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Bill_To_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Bill_To_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Bill_To_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Ship_To_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Ship_To_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Ship_To_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Ship_To_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED( End_Customer_Account__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Bill%2FShip to fields on Quote 2</fullName>
        <actions>
            <name>Update_Ship_To_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Ship_To_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED( End_Customer_Account__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QS Quote Approved</fullName>
        <actions>
            <name>QS_Quote_Approved_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>When a Quote is approved via Advanced Approvals, send an email to the Sales Rep</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SF CPQ%3A Set Watermark Shown</fullName>
        <actions>
            <name>SF_CPQ_Set_Watermark_Shown</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Concession_Rollup__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Checks to see if there are any pending concession requests. If so, it will set the Watermark shown field, which will display DRAFT on the quote document(s)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Converison Rate for USD</fullName>
        <actions>
            <name>Update_USD_conversion_rate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Currency__c</field>
            <operation>equals</operation>
            <value>USD</value>
        </criteriaItems>
        <description>Sets the conversion rate to 1.00000 for USD currency.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Store Prior value for Markup Rate</fullName>
        <actions>
            <name>Set_prior_value_of_markup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( SBQQ__MarkupRate__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
