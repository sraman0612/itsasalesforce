<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Concession_Req_Send_TSM_Submitted_email2</fullName>
        <ccEmails>cindy.courcelle@codezeroconsulting.com</ccEmails>
        <description>Concession Req - Send TSM Submitted email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Concession_Request_Submitted_CRObj</template>
    </alerts>
    <alerts>
        <fullName>Concession_Req_Send_status_update_email2</fullName>
        <description>Concession Req - Send status update email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Status_Changed_CRObj</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Approved_Multiplier2</fullName>
        <field>Approved_Multiplier__c</field>
        <formula>Requested_Multiplier__c</formula>
        <name>Update Approved Multiplier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Quote_Line__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ZCON_Percentage</fullName>
        <field>ZCON_Percentage__c</field>
        <formula>ZCON_Percentage__c</formula>
        <name>Update ZCON Percentage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Quote_Line__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Concession Request Status Changed</fullName>
        <actions>
            <name>Concession_Req_Send_status_update_email2</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(ApprovalStatus__c ) &amp;&amp; (ISPICKVAL(ApprovalStatus__c , &quot;Approved&quot;) ||ISPICKVAL(ApprovalStatus__c , &quot;Rejected&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Concession Request Submitted</fullName>
        <actions>
            <name>Concession_Req_Send_TSM_Submitted_email2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to the TSM when the concession request has been submitted</description>
        <formula>ISCHANGED(ApprovalStatus__c)&amp;&amp; ISPICKVAL(ApprovalStatus__c , &quot;Pending&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Distribution Channel</fullName>
        <active>false</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Approved Multiplier</fullName>
        <actions>
            <name>Update_Approved_Multiplier2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(ApprovalStatus__c ) &amp;&amp; ISPICKVAL(ApprovalStatus__c,&quot;Approved&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update ZCON Percentage</fullName>
        <actions>
            <name>Update_ZCON_Percentage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(ApprovalStatus__c ) &amp;&amp; ISPICKVAL(ApprovalStatus__c,&quot;Approved&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
