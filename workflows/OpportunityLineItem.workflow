<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_SN_Checkbox</fullName>
        <field>Serial_Number_Populated__c</field>
        <literalValue>1</literalValue>
        <name>Populate SN Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Serial_Number_Populated_Field</fullName>
        <field>Serial_Number_Populated__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Serial Number Populated Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Serial Number Populated on Opp Product</fullName>
        <actions>
            <name>Populate_SN_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Fires when the Serial Number field On Opp Product is Populated</description>
        <formula>NOT(ISBLANK(Serial_Number__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Serial Number Unpopulated on Opp Product</fullName>
        <actions>
            <name>Uncheck_Serial_Number_Populated_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Fires when the Serial Number field On Opp Product is Unpopulated</description>
        <formula>ISBLANK(Serial_Number__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
