<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AssetRecordType_NAAirEdit</fullName>
        <field>RecordTypeId</field>
        <lookupValue>NA_Air_Edit</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>AssetRecordType=NA Air Edit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Compressor_Usage_Code_Field_Update</fullName>
        <field>Compressor_Usage_Code__c</field>
        <formula>IF(AND(LEN(char_Text1__c) == 3, OR( BEGINS(char_Text1__c, &quot;H&quot;) , BEGINS(char_Text1__c, &quot;K&quot;), BEGINS(char_Text1__c, &quot;P&quot;), BEGINS(char_Text1__c, &quot;W&quot;))), char_Text1__c, 
IF(AND(LEN(char_Text2__c) == 3, OR( BEGINS(char_Text2__c, &quot;H&quot;) , BEGINS(char_Text2__c, &quot;K&quot;), BEGINS(char_Text2__c, &quot;P&quot;), BEGINS(char_Text2__c, &quot;W&quot;))), char_Text2__c, 
IF(AND(LEN(char_Text3__c) == 3, OR( BEGINS(char_Text3__c, &quot;H&quot;) , BEGINS(char_Text3__c, &quot;K&quot;), BEGINS(char_Text3__c, &quot;P&quot;), BEGINS(char_Text3__c, &quot;W&quot;))), char_Text3__c, 
IF(AND(LEN(char_Text4__c) == 3, OR( BEGINS(char_Text4__c, &quot;H&quot;) , BEGINS(char_Text4__c, &quot;K&quot;), BEGINS(char_Text4__c, &quot;P&quot;), BEGINS(char_Text4__c, &quot;W&quot;))), char_Text4__c, 
IF(AND(LEN(char_Text5__c) == 3, OR( BEGINS(char_Text5__c, &quot;H&quot;) , BEGINS(char_Text5__c, &quot;K&quot;), BEGINS(char_Text5__c, &quot;P&quot;), BEGINS(char_Text5__c, &quot;W&quot;))), char_Text5__c, 
IF(AND(LEN(char_Text6__c) == 3, OR( BEGINS(char_Text6__c, &quot;H&quot;) , BEGINS(char_Text6__c, &quot;K&quot;), BEGINS(char_Text6__c, &quot;P&quot;), BEGINS(char_Text6__c, &quot;W&quot;))), char_Text6__c, 
IF(AND(LEN(char_Text7__c) == 3, OR( BEGINS(char_Text7__c, &quot;H&quot;) , BEGINS(char_Text7__c, &quot;K&quot;), BEGINS(char_Text7__c, &quot;P&quot;), BEGINS(char_Text7__c, &quot;W&quot;))), char_Text7__c, 
IF(AND(LEN(char_Text8__c) == 3, OR( BEGINS(char_Text8__c, &quot;H&quot;) , BEGINS(char_Text8__c, &quot;K&quot;), BEGINS(char_Text8__c, &quot;P&quot;), BEGINS(char_Text8__c, &quot;W&quot;))), char_Text8__c, 
IF(AND(LEN(char_Text9__c) == 3, OR( BEGINS(char_Text9__c, &quot;H&quot;) , BEGINS(char_Text9__c, &quot;K&quot;), BEGINS(char_Text9__c, &quot;P&quot;), BEGINS(char_Text9__c, &quot;W&quot;))), char_Text9__c, 

IF(AND(LEN(char_Text10__c) == 3, OR( BEGINS(char_Text10__c, &quot;H&quot;) , BEGINS(char_Text10__c, &quot;K&quot;), BEGINS(char_Text10__c, &quot;P&quot;), BEGINS(char_Text10__c, &quot;W&quot;))), char_Text10__c, 
IF(AND(LEN(char_Text11__c) == 3, OR( BEGINS(char_Text11__c, &quot;H&quot;) , BEGINS(char_Text11__c, &quot;K&quot;), BEGINS(char_Text11__c, &quot;P&quot;), BEGINS(char_Text11__c, &quot;W&quot;))), char_Text11__c, 
IF(AND(LEN(char_Text12__c) == 3, OR( BEGINS(char_Text12__c, &quot;H&quot;) , BEGINS(char_Text12__c, &quot;K&quot;), BEGINS(char_Text12__c, &quot;P&quot;), BEGINS(char_Text12__c, &quot;W&quot;))), char_Text12__c, 
IF(AND(LEN(char_Text13__c) == 3, OR( BEGINS(char_Text13__c, &quot;H&quot;) , BEGINS(char_Text13__c, &quot;K&quot;), BEGINS(char_Text13__c, &quot;P&quot;), BEGINS(char_Text13__c, &quot;W&quot;))), char_Text13__c, 
IF(AND(LEN(char_Text14__c) == 3, OR( BEGINS(char_Text14__c, &quot;H&quot;) , BEGINS(char_Text14__c, &quot;K&quot;), BEGINS(char_Text14__c, &quot;P&quot;), BEGINS(char_Text14__c, &quot;W&quot;))), char_Text14__c, 
IF(AND(LEN(char_Text15__c) == 3, OR( BEGINS(char_Text15__c, &quot;H&quot;) , BEGINS(char_Text15__c, &quot;K&quot;), BEGINS(char_Text15__c, &quot;P&quot;), BEGINS(char_Text15__c, &quot;W&quot;))), char_Text15__c, 
IF(AND(LEN(char_Text16__c) == 3, OR( BEGINS(char_Text16__c, &quot;H&quot;) , BEGINS(char_Text16__c, &quot;K&quot;), BEGINS(char_Text16__c, &quot;P&quot;), BEGINS(char_Text16__c, &quot;W&quot;))), char_Text16__c, 
IF(AND(LEN(char_Text17__c) == 3, OR( BEGINS(char_Text17__c, &quot;H&quot;) , BEGINS(char_Text17__c, &quot;K&quot;), BEGINS(char_Text17__c, &quot;P&quot;), BEGINS(char_Text17__c, &quot;W&quot;))), char_Text17__c,
IF(AND(LEN(char_Text18__c) == 3, OR( BEGINS(char_Text18__c, &quot;H&quot;) , BEGINS(char_Text18__c, &quot;K&quot;), BEGINS(char_Text18__c, &quot;P&quot;), BEGINS(char_Text18__c, &quot;W&quot;))), char_Text18__c,




&quot;*&quot;))))))))))))))))))</formula>
        <name>Compressor Usage Code Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Compressor_Usage_Code_Field_Update_2</fullName>
        <field>Compressor_Usage_Code__c</field>
        <formula>/*IF(AND(LEN(char_Text10__c) == 3, OR( BEGINS(char_Text10__c, &quot;H&quot;) , BEGINS(char_Text10__c, &quot;K&quot;), BEGINS(char_Text10__c, &quot;P&quot;), BEGINS(char_Text10__c, &quot;W&quot;))), char_Text10__c, 
IF(AND(LEN(char_Text11__c) == 3, OR( BEGINS(char_Text11__c, &quot;H&quot;) , BEGINS(char_Text11__c, &quot;K&quot;), BEGINS(char_Text11__c, &quot;P&quot;), BEGINS(char_Text11__c, &quot;W&quot;))), char_Text11__c, 
IF(AND(LEN(char_Text12__c) == 3, OR( BEGINS(char_Text12__c, &quot;H&quot;) , BEGINS(char_Text12__c, &quot;K&quot;), BEGINS(char_Text12__c, &quot;P&quot;), BEGINS(char_Text12__c, &quot;W&quot;))), char_Text12__c, 
IF(AND(LEN(char_Text13__c) == 3, OR( BEGINS(char_Text13__c, &quot;H&quot;) , BEGINS(char_Text13__c, &quot;K&quot;), BEGINS(char_Text13__c, &quot;P&quot;), BEGINS(char_Text13__c, &quot;W&quot;))), char_Text13__c, 
IF(AND(LEN(char_Text14__c) == 3, OR( BEGINS(char_Text14__c, &quot;H&quot;) , BEGINS(char_Text14__c, &quot;K&quot;), BEGINS(char_Text14__c, &quot;P&quot;), BEGINS(char_Text14__c, &quot;W&quot;))), char_Text14__c, 
IF(AND(LEN(char_Text15__c) == 3, OR( BEGINS(char_Text15__c, &quot;H&quot;) , BEGINS(char_Text15__c, &quot;K&quot;), BEGINS(char_Text15__c, &quot;P&quot;), BEGINS(char_Text15__c, &quot;W&quot;))), char_Text15__c, 
IF(AND(LEN(char_Text16__c) == 3, OR( BEGINS(char_Text16__c, &quot;H&quot;) , BEGINS(char_Text16__c, &quot;K&quot;), BEGINS(char_Text16__c, &quot;P&quot;), BEGINS(char_Text16__c, &quot;W&quot;))), char_Text16__c, 
IF(AND(LEN(char_Text17__c) == 3, OR( BEGINS(char_Text17__c, &quot;H&quot;) , BEGINS(char_Text17__c, &quot;K&quot;), BEGINS(char_Text17__c, &quot;P&quot;), BEGINS(char_Text17__c, &quot;W&quot;))), char_Text17__c, 
IF(AND(LEN(char_Text18__c) == 3, OR( BEGINS(char_Text18__c, &quot;H&quot;) , BEGINS(char_Text18__c, &quot;K&quot;), BEGINS(char_Text18__c, &quot;P&quot;), BEGINS(char_Text18__c, &quot;W&quot;))), char_Text18__c,*/

IF(AND(LEN(char_Text19__c) == 3, OR( BEGINS(char_Text19__c, &quot;H&quot;) , BEGINS(char_Text19__c, &quot;K&quot;), BEGINS(char_Text19__c, &quot;P&quot;), BEGINS(char_Text19__c, &quot;W&quot;))), char_Text19__c, 
IF(AND(LEN(char_Text20__c) == 3, OR( BEGINS(char_Text20__c, &quot;H&quot;) , BEGINS(char_Text20__c, &quot;K&quot;), BEGINS(char_Text20__c, &quot;P&quot;), BEGINS(char_Text20__c, &quot;W&quot;))), char_Text20__c, 
IF(AND(LEN(char_Text21__c) == 3, OR( BEGINS(char_Text21__c, &quot;H&quot;) , BEGINS(char_Text21__c, &quot;K&quot;), BEGINS(char_Text21__c, &quot;P&quot;), BEGINS(char_Text21__c, &quot;W&quot;))), char_Text21__c, 
IF(AND(LEN(char_Text22__c) == 3, OR( BEGINS(char_Text22__c, &quot;H&quot;) , BEGINS(char_Text22__c, &quot;K&quot;), BEGINS(char_Text22__c, &quot;P&quot;), BEGINS(char_Text22__c, &quot;W&quot;))), char_Text22__c, 
IF(AND(LEN(char_Text23__c) == 3, OR( BEGINS(char_Text23__c, &quot;H&quot;) , BEGINS(char_Text23__c, &quot;K&quot;), BEGINS(char_Text23__c, &quot;P&quot;), BEGINS(char_Text23__c, &quot;W&quot;))), char_Text23__c, 
IF(AND(LEN(char_Text24__c) == 3, OR( BEGINS(char_Text24__c, &quot;H&quot;) , BEGINS(char_Text24__c, &quot;K&quot;), BEGINS(char_Text24__c, &quot;P&quot;), BEGINS(char_Text24__c, &quot;W&quot;))), char_Text24__c, 
IF(AND(LEN(char_Text25__c) == 3, OR( BEGINS(char_Text25__c, &quot;H&quot;) , BEGINS(char_Text25__c, &quot;K&quot;), BEGINS(char_Text25__c, &quot;P&quot;), BEGINS(char_Text25__c, &quot;W&quot;))), char_Text25__c,
 




&quot;&quot;)))))))/*)))))))))*/</formula>
        <name>Compressor Usage Code Field Update 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_Servicing_Machine_Status</fullName>
        <description>Checks End Customer Servicing field.</description>
        <field>End_Customer_Servicing__c</field>
        <literalValue>1</literalValue>
        <name>Customer Servicing Machine Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inactivate_machine_status</fullName>
        <field>Inactive__c</field>
        <literalValue>1</literalValue>
        <name>Inactivate machine status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Most_Recent_Date_of_Service_Check</fullName>
        <field>Most_Recent_Date_of_Service_Updated__c</field>
        <literalValue>1</literalValue>
        <name>Most Recent Date of Service Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_Customer_Servicing_Machine_Status</fullName>
        <description>End Customer Servicing flag is unchecked.</description>
        <field>End_Customer_Servicing__c</field>
        <literalValue>0</literalValue>
        <name>Not Customer Servicing Machine Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reactivate_machine_status</fullName>
        <field>Inactive__c</field>
        <literalValue>0</literalValue>
        <name>Reactivate machine status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_true</fullName>
        <field>SFS_Rejected__c</field>
        <literalValue>1</literalValue>
        <name>Rejected = true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serial_Number_Inactive_Flag</fullName>
        <description>The Serial Number Inactive Flag is checked.</description>
        <field>Inactive__c</field>
        <literalValue>1</literalValue>
        <name>Serial Number - Inactive Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Measure_Change_Date</fullName>
        <field>iConn_Measurement_Last_Change__c</field>
        <formula>NOW()</formula>
        <name>Set Measure Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Warranty_End_Date</fullName>
        <field>Warranty_End_Date__c</field>
        <formula>IF(OR(
Warranty_No__c == &apos;136&apos;,
Warranty_No__c == &apos;137&apos;,
Warranty_No__c == &apos;143&apos;,
Warranty_No__c == &apos;145&apos;,
Warranty_No__c == &apos;147&apos;,
Warranty_No__c == &apos;162&apos;,
Warranty_No__c == &apos;163&apos;,
Warranty_No__c == &apos;202&apos;),

(DATE( YEAR(Warranty_Start_Date__c)+10,
      MONTH(Warranty_Start_Date__c),
      DAY(Warranty_Start_Date__c))),

IF(OR(
Warranty_No__c == &apos;6&apos;,
Warranty_No__c == &apos;12&apos;,
Warranty_No__c == &apos;34&apos;,
Warranty_No__c == &apos;144&apos;,
Warranty_No__c == &apos;232&apos;,
Warranty_No__c == &apos;243&apos;),

(DATE( YEAR(Warranty_Start_Date__c)+5,
      MONTH(Warranty_Start_Date__c),
      DAY(Warranty_Start_Date__c))),


IF(AND(OR(
Warranty_No__c == &apos;9&apos;,
Warranty_No__c == &apos;7&apos;,
Warranty_No__c == &apos;122&apos;,
Warranty_No__c == &apos;182&apos;,
Warranty_No__c == &apos;203&apos;,
Warranty_No__c == &apos;206&apos;,
Warranty_No__c == &apos;208&apos;),
NOT(ISBLANK(Start_up_Date__c))),


(DATE( YEAR(Warranty_Start_Date__c)+2,
      MONTH(Warranty_Start_Date__c),
      DAY(Warranty_Start_Date__c))),

IF(AND(OR(
Warranty_No__c == &apos;9&apos;,
Warranty_No__c == &apos;7&apos;,
Warranty_No__c == &apos;122&apos;,
Warranty_No__c == &apos;182&apos;,
Warranty_No__c == &apos;203&apos;,
Warranty_No__c == &apos;206&apos;,
Warranty_No__c == &apos;208&apos;),
ISBLANK(Start_up_Date__c)),

((Warranty_Start_Date__c)+821.5),

IF(AND(OR(
Warranty_No__c == &apos;132&apos;,
Warranty_No__c == &apos;204&apos;,
Warranty_No__c == &apos;209&apos;,
Warranty_No__c == &apos;210&apos;),
NOT(ISBLANK(Start_up_Date__c))),

(DATE( YEAR(Warranty_Start_Date__c)+2,
      MONTH(Warranty_Start_Date__c),
      DAY(Warranty_Start_Date__c))),

IF(AND(OR(
Warranty_No__c == &apos;132&apos;,
Warranty_No__c == &apos;204&apos;,
Warranty_No__c == &apos;209&apos;,
Warranty_No__c == &apos;210&apos;),
ISBLANK(Start_up_Date__c)),

((Warranty_Start_Date__c)+900),

IF(AND(OR(
Warranty_No__c == &apos;5&apos;,
Warranty_No__c == &apos;10&apos;,
Warranty_No__c == &apos;212&apos;),
NOT(ISBLANK(Start_up_Date__c))),


(DATE( YEAR(Warranty_Start_Date__c)+1,
      MONTH(Warranty_Start_Date__c),
      DAY(Warranty_Start_Date__c))),

IF(AND(OR(
Warranty_No__c == &apos;5&apos;,
Warranty_No__c == &apos;10&apos;,
Warranty_No__c == &apos;212&apos;),
ISBLANK(Start_up_Date__c)),


((Warranty_Start_Date__c)+456),



null))))))))</formula>
        <name>Set Warranty End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Warranty_End_Date_Leap_Year</fullName>
        <field>Warranty_End_Date__c</field>
        <formula>IF(OR(
Warranty_No__c == &apos;136&apos;,
Warranty_No__c == &apos;137&apos;,
Warranty_No__c == &apos;143&apos;,
Warranty_No__c == &apos;145&apos;,
Warranty_No__c == &apos;147&apos;,
Warranty_No__c == &apos;162&apos;,
Warranty_No__c == &apos;163&apos;,
Warranty_No__c == &apos;202&apos;),

(DATE( YEAR(Warranty_Start_Date__c)+10,
MONTH(Warranty_Start_Date__c)+1,
DAY(Warranty_Start_Date__c)-28)),

IF(OR(
Warranty_No__c == &apos;6&apos;,
Warranty_No__c == &apos;12&apos;,
Warranty_No__c == &apos;34&apos;,
Warranty_No__c == &apos;144&apos;,
Warranty_No__c == &apos;232&apos;,
Warranty_No__c == &apos;243&apos;),

(DATE( YEAR(Warranty_Start_Date__c)+5,
MONTH(Warranty_Start_Date__c)+1,
DAY(Warranty_Start_Date__c)-28)),


IF(AND(OR(
Warranty_No__c == &apos;9&apos;,
Warranty_No__c == &apos;7&apos;,
Warranty_No__c == &apos;122&apos;,
Warranty_No__c == &apos;182&apos;,
Warranty_No__c == &apos;203&apos;,
Warranty_No__c == &apos;206&apos;,
Warranty_No__c == &apos;208&apos;),
NOT(ISBLANK(Start_up_Date__c))),


(DATE( YEAR(Warranty_Start_Date__c)+2,
MONTH(Warranty_Start_Date__c)+1,
DAY(Warranty_Start_Date__c)-28)),

IF(AND(OR(
Warranty_No__c == &apos;9&apos;,
Warranty_No__c == &apos;7&apos;,
Warranty_No__c == &apos;122&apos;,
Warranty_No__c == &apos;182&apos;,
Warranty_No__c == &apos;203&apos;,
Warranty_No__c == &apos;206&apos;,
Warranty_No__c == &apos;208&apos;),
ISBLANK(Start_up_Date__c)),

(DATE( YEAR(Warranty_Start_Date__c)+2,
MONTH(Warranty_Start_Date__c)+4,
DAY(Warranty_Start_Date__c)-28)),

IF(AND(OR(
Warranty_No__c == &apos;132&apos;,
Warranty_No__c == &apos;204&apos;,
Warranty_No__c == &apos;209&apos;,
Warranty_No__c == &apos;210&apos;),
NOT(ISBLANK(Start_up_Date__c))),

(DATE( YEAR(Warranty_Start_Date__c)+2,
MONTH(Warranty_Start_Date__c)+1,
DAY(Warranty_Start_Date__c)-28)),

IF(AND(OR(
Warranty_No__c == &apos;132&apos;,
Warranty_No__c == &apos;204&apos;,
Warranty_No__c == &apos;209&apos;,
Warranty_No__c == &apos;210&apos;),
ISBLANK(Start_up_Date__c)),

(DATE( YEAR(Warranty_Start_Date__c)+2,
MONTH(Warranty_Start_Date__c)+7,
DAY(Warranty_Start_Date__c)-28)),

IF(AND(OR(
Warranty_No__c == &apos;5&apos;,
Warranty_No__c == &apos;10&apos;,
Warranty_No__c == &apos;212&apos;),
NOT(ISBLANK(Start_up_Date__c))),


(DATE( YEAR(Warranty_Start_Date__c)+1,
MONTH(Warranty_Start_Date__c)+1,
DAY(Warranty_Start_Date__c)-28)),

IF(AND(OR(
Warranty_No__c == &apos;5&apos;,
Warranty_No__c == &apos;10&apos;,
Warranty_No__c == &apos;212&apos;),
ISBLANK(Start_up_Date__c)),


(DATE( YEAR(Warranty_Start_Date__c)+1,
MONTH(Warranty_Start_Date__c)+4,
DAY(Warranty_Start_Date__c)-28)),



null))))))))</formula>
        <name>Set Warranty End Date - Leap Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Avg_Yearly_Run_Hours</fullName>
        <field>Avg_Yearly_Run_Hours_use__c</field>
        <formula>IF(OR( 
ISNULL(Hours__c), 
Hours__c&lt;=0, 
Days_from_start_to_service__c&lt;=30), 4000, 
((Hours__c/(Days_from_start_to_service__c))*365))</formula>
        <name>Update Avg Yearly Run Hours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Expected_Service_Date_from_Startu</fullName>
        <field>Expected_Next_Service_Date__c</field>
        <formula>ADDMONTHS( Start_up_Date__c , 6)</formula>
        <name>Update Expected Service Date from Startu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Customer Servicing Machine Status</fullName>
        <actions>
            <name>Customer_Servicing_Machine_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Machine_Status__c</field>
            <operation>equals</operation>
            <value>Customer Servicing - with OEM Parts,Customer Servicing - without OEM Parts</value>
        </criteriaItems>
        <description>If Machine Status is Customer Servicing - with or Customer Servicing - without, then the End Customer Servicing flag is checked.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inactivate machine status</fullName>
        <actions>
            <name>Inactivate_machine_status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Asset.Machine_Status__c</field>
            <operation>equals</operation>
            <value>Administrative,Decommissioned,Distributor Separation - No End User Known,Off-line,Replaced</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Equipment_Status__c</field>
            <operation>contains</operation>
            <value>INAC</value>
        </criteriaItems>
        <description>If Machine Status is Off-line, Replaced or Decommissioned, then the Inactive flag is checked.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Not Customer Servicing Machine Status</fullName>
        <actions>
            <name>Not_Customer_Servicing_Machine_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Machine_Status__c</field>
            <operation>notEqual</operation>
            <value>Customer Servicing - with OEM Parts,Customer Servicing - without OEM Parts</value>
        </criteriaItems>
        <description>If Machine Status is not Customer Servicing - with or Customer Servicing - without, then the End Customer Servicing flag is not checked.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reactivate machine status</fullName>
        <actions>
            <name>Reactivate_machine_status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Machine_Status__c</field>
            <operation>notEqual</operation>
            <value>Administrative,Decommissioned,Distributor Separation - No End User Known,Off-line,Replaced</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Equipment_Status__c</field>
            <operation>notContain</operation>
            <value>INAC</value>
        </criteriaItems>
        <description>If Machine Status is not equal to Off-line, Replaced or Decommissioned, then Inactive field is not checked.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Serial Number - Most Recent Date of Service</fullName>
        <actions>
            <name>Most_Recent_Date_of_Service_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Due to Limitations of Process Builder, this workflow will update a &apos;Most Recent Date of Service Check&apos; field (checkbox) which will then trigger the &apos;Most Recent Date of Service Rollup Updated&apos; Process Builder.  Unchecks when finished. Criteria changed.</description>
        <formula>ISCHANGED(  Name  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Serial Number - Update HP</fullName>
        <actions>
            <name>Compressor_Usage_Code_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Compressor_Usage_Code__c</field>
            <operation>equals</operation>
            <value>*</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Serial Number - Update HP 2</fullName>
        <actions>
            <name>Compressor_Usage_Code_Field_Update_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Asset.Compressor_Usage_Code__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Compressor_Usage_Code__c</field>
            <operation>equals</operation>
            <value>*</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Startup Service Date</fullName>
        <actions>
            <name>Update_Expected_Service_Date_from_Startu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Next Expected Service Date when the startup date gets set</description>
        <formula>AND(
ISBLANK(Expected_Next_Service_Date__c),
ISCHANGED( Start_up_Date__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Avg Yearly Run Hours</fullName>
        <actions>
            <name>Update_Avg_Yearly_Run_Hours</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Warranty Start End Date</fullName>
        <actions>
            <name>Set_Warranty_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(MONTH(Warranty_Start_Date__c) !=2 &amp;&amp; DAY(Warranty_Start_Date__c)!=29, TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Warranty Start End Date-Leap Year</fullName>
        <actions>
            <name>Set_Warranty_End_Date_Leap_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(MONTH(Warranty_Start_Date__c) =2 &amp;&amp; DAY(Warranty_Start_Date__c)=29, TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>iConn Measurement Change</fullName>
        <actions>
            <name>Set_Measure_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( iConn_c8y_H_total__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>