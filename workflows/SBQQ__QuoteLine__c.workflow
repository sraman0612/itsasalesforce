<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Concession_Req_Send_TSM_Submitted_email</fullName>
        <description>Concession Req - Send TSM Submitted email</description>
        <protected>false</protected>
        <recipients>
            <field>User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Concession_Request_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Concession_Req_Send_status_update_email</fullName>
        <description>Concession Req - Send status update email</description>
        <protected>false</protected>
        <recipients>
            <field>User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Status_Changed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Approved_Multiplier</fullName>
        <field>Approved_Multiplier__c</field>
        <formula>Requested_Multiplier__c</formula>
        <name>Update Approved Multiplier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Concession Request Status Changed</fullName>
        <actions>
            <name>Concession_Req_Send_status_update_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Sends an email to the TSM stating the new status of the concession request</description>
        <formula>ISPICKVAL(ApprovalStatus__c , &quot;Approved&quot;) ||ISPICKVAL(ApprovalStatus__c , &quot;Rejected&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Concession Request Submitted</fullName>
        <actions>
            <name>Concession_Req_Send_TSM_Submitted_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Sends an email to the TSM when a concession request has been submitted</description>
        <formula>ISPICKVAL(ApprovalStatus__c , &quot;Pending&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Approved Multiplier</fullName>
        <actions>
            <name>Update_Approved_Multiplier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SBQQ__QuoteLine__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Upon approval of a quote line, copy the Requested Multiplier value into the Approved Multiplier field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
