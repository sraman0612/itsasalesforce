<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Name_Searchable_Field_Update</fullName>
        <description>Update Account Name (Searchable) field on Contact for global search abilities.</description>
        <field>Account_Name_Searchable__c</field>
        <formula>Account.Name</formula>
        <name>Account Name (Searchable) Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Manager_Approved_Checkbox</fullName>
        <field>Manager_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>Check &apos;Manager Approved&apos; Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Email_Field_Update</fullName>
        <description>Updates standard email field with &apos;Email Address Copy&apos; when changed.</description>
        <field>Email</field>
        <formula>Email_Address_Copy__c</formula>
        <name>Contact - Email Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Record_Type_Update</fullName>
        <description>Updates the Contact Record Type to &apos;Customer&apos;.</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contact Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Field_Update</fullName>
        <field>Email_Address_Copy__c</field>
        <name>Email Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incomplete_Contact_RT_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Incomplete Contact RT Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Internal_Clear_Value_Update</fullName>
        <description>Clears the &apos;Pending-Internal&apos; field.</description>
        <field>Pending_Internal__c</field>
        <name>Pending-Internal Clear Value Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Contact - Email Copy Field Update</fullName>
        <actions>
            <name>Email_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the standard Email field value is removed, also remove the Email Copy field value.</description>
        <formula>AND(ISCHANGED( Email ), ISBLANK(Email))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Email Field Update from External File</fullName>
        <actions>
            <name>Contact_Email_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Because standard Email fields cannot be used as External ID&apos;s, Jitterbit integration maps to a custom Email field that is an External ID.  This workflow populates the standard Email field when the custom field is changed.</description>
        <formula>NOT(ISBLANK(( Email_Address_Copy__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Incomplete%2FComplete Record Type Update</fullName>
        <actions>
            <name>Incomplete_Contact_RT_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If a Contact created without an Account (Email-to-Case) is saved WITH an Account, this workflow will switch the Record Type from &apos;Incomplete Customer&apos; to the Standard Customer RT.</description>
        <formula>RecordType.Name = &apos;Incomplete Contact&apos;
&amp;&amp;
LastName != &apos;Enter Last Name&apos;
&amp;&amp;
Account.Name != &apos;Gardner Denver: Incomplete Contacts&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - New Contact Manager Approval</fullName>
        <actions>
            <name>Check_Manager_Approved_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer</value>
        </criteriaItems>
        <description>When a new &apos;Customer&apos; contact is created, this workflow will check a &apos;Manager Approval Needed&apos; checkbox that only the manager can edit.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Searchable Account Field Update</fullName>
        <actions>
            <name>Account_Name_Searchable_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates a text field with the Contact&apos;s Account for searching abilities.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Uncheck %27Manager Approved%27 on Record Edit</fullName>
        <actions>
            <name>Check_Manager_Approved_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Unchecks the &apos;Manager Approved&apos; checkbox when Contact record is edited. Used for List View for Manager to review Contact record changes.</description>
        <formula>OR( ISCHANGED( FirstName ), ISCHANGED(  Phone  ), ISCHANGED(  Email  ), ISCHANGED(  Title  ), ISCHANGED(  MailingPostalCode  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Update on %27Internal%27 to %27Customer%27 Relationship Change</fullName>
        <actions>
            <name>Contact_Record_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Pending_Internal_Clear_Value_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Contact&apos;s &apos;Relationship&apos; field is updated from &apos;Internal&apos; to &apos;Customer&apos;, this workflow will clear the value in &apos;Pending-Internal&apos; and update the Record Type to &apos;Customer&apos;.</description>
        <formula>ISCHANGED(Relationship__c ) &amp;&amp; ISPICKVAL( PRIORVALUE( Relationship__c ) , &apos;Internal&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
