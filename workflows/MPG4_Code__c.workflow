<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Concat_Name_Description</fullName>
        <field>Name</field>
        <formula>MPG4_Number__c  +&apos; &apos;+ Description__c</formula>
        <name>Concat Name + Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>MPG4 Codes - Concatenate MPG4 Code %28Name field%29 and Description</fullName>
        <actions>
            <name>Concat_Name_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Concatenates the MPG4 Number + Description and populates the record Name field with this value.</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
