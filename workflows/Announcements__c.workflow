<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>KV_MDK_Communication</fullName>
        <description>KV MDK Communication</description>
        <protected>false</protected>
        <recipients>
            <recipient>KV_Documents</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>vacuum.cs.spr@md-kinney.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CC360/MDK_Announcement_Email</template>
    </alerts>
    <alerts>
        <fullName>MDK_MD_Communication</fullName>
        <description>MDK MD Communication</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Documents</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>blower.cs.spr@md-kinney.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CC360/MDK_Announcement_Email</template>
    </alerts>
    <alerts>
        <fullName>MDK_MT_Communication</fullName>
        <description>MDK MT Communication</description>
        <protected>false</protected>
        <recipients>
            <recipient>MT_Documents</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>mobile.cs.spr@md-kinney.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CC360/MDK_Announcement_Email</template>
    </alerts>
    <alerts>
        <fullName>MDK_Test_Communication</fullName>
        <description>MDK Test Communication</description>
        <protected>false</protected>
        <recipients>
            <recipient>Announcement_Test</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CC360/MDK_Announcement_Email</template>
    </alerts>
    <rules>
        <fullName>KV Announcement Communicaiton</fullName>
        <actions>
            <name>KV_MDK_Communication</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Announcements__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Announcements__c.Dist_Channel__c</field>
            <operation>includes</operation>
            <value>KV</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MD Announcement Communicaiton</fullName>
        <actions>
            <name>MDK_MD_Communication</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Announcements__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Announcements__c.Dist_Channel__c</field>
            <operation>includes</operation>
            <value>MD</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MT Announcement Communicaiton</fullName>
        <actions>
            <name>MDK_MT_Communication</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Announcements__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Announcements__c.Dist_Channel__c</field>
            <operation>includes</operation>
            <value>MT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Test Announcement Communicaiton</fullName>
        <actions>
            <name>MDK_Test_Communication</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Announcements__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Announcements__c.Dist_Channel__c</field>
            <operation>includes</operation>
            <value>Test</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
