<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>129|70422</label>
    <protected>false</protected>
    <values>
        <field>Dealer_Name__c</field>
        <value xsi:type="xsd:string">IRAC - EDISON</value>
    </values>
    <values>
        <field>SFS_Site_Number__c</field>
        <value xsi:type="xsd:string">129|205114|</value>
    </values>
</CustomMetadata>
