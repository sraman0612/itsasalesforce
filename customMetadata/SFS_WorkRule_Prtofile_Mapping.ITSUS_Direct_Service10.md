<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ITSUS - Direct Service</label>
    <protected>false</protected>
    <values>
        <field>SFS_Destination_Status__c</field>
        <value xsi:type="xsd:string">Completed</value>
    </values>
    <values>
        <field>SFS_Source_Status__c</field>
        <value xsi:type="xsd:string">In Progress</value>
    </values>
    <values>
        <field>SFS_State__c</field>
        <value xsi:type="xsd:string">Washington</value>
    </values>
    <values>
        <field>SFS_Work_Rule_Name__c</field>
        <value xsi:type="xsd:string">ITSUS DS-WA-On Site</value>
    </values>
</CustomMetadata>
