<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Create Date Descending</label>
    <protected>false</protected>
    <values>
        <field>ensxapp__Precedence__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>ensxapp__SAP_Invoice_Search_Preset__c</field>
        <value xsi:type="xsd:string">ensxapp__All_Invoices_Default</value>
    </values>
    <values>
        <field>ensxapp__Sort_Direction__c</field>
        <value xsi:type="xsd:string">Descending</value>
    </values>
    <values>
        <field>ensxapp__Sort_Field__c</field>
        <value xsi:type="xsd:string">ERDAT</value>
    </values>
</CustomMetadata>
