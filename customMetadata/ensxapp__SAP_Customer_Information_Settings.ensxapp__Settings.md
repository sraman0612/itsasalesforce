<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Settings</label>
    <protected>false</protected>
    <values>
        <field>ensxapp__Customer_Number_Field__c</field>
        <value xsi:type="xsd:string">AccountNumber</value>
    </values>
    <values>
        <field>ensxapp__Entity__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
</CustomMetadata>
