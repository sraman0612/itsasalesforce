<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Source_Siebel</label>
    <protected>false</protected>
    <values>
        <field>SFS_Child_Start__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">41.03</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:type="xsd:string">siebel_id__c</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">Service Contract</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;SRC_ATTRIBUTE&gt;&lt;NAME&gt;SIEBEL_ID&lt;/NAME&gt;&lt;VALUE&gt;siebel_id__c&lt;/VALUE&gt;&lt;/SRC_ATTRIBUTE&gt;&lt;/G_SRC_ATTRIBUTES&gt;&lt;/PA_PROJECT_CUSTOMERS&gt;&lt;/G_PA_PROJECT_CUSTOMERS&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">SRC Attributes</value>
    </values>
</CustomMetadata>
