<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Canal_Zone</label>
    <protected>false</protected>
    <values>
        <field>SFS_State_Province_Abbreviated__c</field>
        <value xsi:type="xsd:string">CZ</value>
    </values>
    <values>
        <field>SFS_State_Province_Name__c</field>
        <value xsi:type="xsd:string">Canal Zone</value>
    </values>
</CustomMetadata>
