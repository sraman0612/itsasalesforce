<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Username</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">Asset</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;soapenv:Envelope
	xmlns:soapenv=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot;
	xmlns:req=&quot;http://www.tavant.com/SerialCreationAndUpdateInterface/claimsubmission/request&quot;&gt;
	&lt;soapenv:Header&gt;
		&lt;wsse:Security soapenv:mustUnderstand=&quot;0&quot;
			xmlns:wsse=&quot;http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd&quot;&gt;
			&lt;wsse:UsernameToken&gt;
				&lt;wsse:Username&gt;IRPrtnrP5hSalesforce&lt;/wsse:Username&gt;
				&lt;wsse:Password&gt;IRPrtnrP5hMoz3f3ffo3&lt;/wsse:Password&gt;
			&lt;/wsse:UsernameToken&gt;
		&lt;/wsse:Security&gt;
	&lt;/soapenv:Header&gt;
	&lt;soapenv:Body&gt;
		&lt;req:SNTransactionRequest&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">Tavant - Serial Number</value>
    </values>
</CustomMetadata>
