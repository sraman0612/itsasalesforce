<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CPQ Get Price</label>
    <protected>false</protected>
    <values>
        <field>SFS_ContentType__c</field>
        <value xsi:type="xsd:string">application/json</value>
    </values>
    <values>
        <field>SFS_Endpoint_URL__c</field>
        <value xsi:type="xsd:string">https://ircompanytest3.bigmachines.com/rest/v13/pricing/actions/calculatePrice</value>
    </values>
    <values>
        <field>SFS_Interface_Destination__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Password__c</field>
        <value xsi:type="xsd:string">dsC]3^Es^4</value>
    </values>
    <values>
        <field>SFS_RICE_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Username__c</field>
        <value xsi:type="xsd:string">CPQINTUSER1</value>
    </values>
</CustomMetadata>
