<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>P PM EVENT REFERENCE</label>
    <protected>false</protected>
    <values>
        <field>Full_XML__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Hardcode_Flag__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Node_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">Service Agreement</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;DATA_AREA&gt;&lt;G_PA_BILLING_EVENT&gt; &lt;PA_BILLING_EVENT&gt; &lt;P_PM_EVENT_REFERENCE&gt;1-15022022838&lt;/P_PM_EVENT_REFERENCE&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">Data Area</value>
    </values>
    <values>
        <field>Salesforce_Field__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
