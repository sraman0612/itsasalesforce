<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pressure</label>
    <protected>false</protected>
    <values>
        <field>Filter_Field__c</field>
        <value xsi:type="xsd:string">Pressure</value>
    </values>
    <values>
        <field>Product_Field__c</field>
        <value xsi:type="xsd:string">pressure_capability_psig__c</value>
    </values>
</CustomMetadata>
