<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TimeOut</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">ERROR: Server returned HTTP response code: 504 for URL: https://oci-icosbomwpsh.ingersollrand.com/CPQ_CRM/ATPPriceOutProvABCSImpl/ProxyService/ATPPriceOutProvABCSImpl_PS - &lt;html&gt;
&lt;head&gt;
&lt;title&gt;504 Gateway Time-out&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;center&gt;
&lt;h1&gt;504 Gateway Time-out&lt;/h1&gt;
&lt;/center&gt;
&lt;hr&gt;
&lt;center/&gt;
&lt;/body&gt;
&lt;/html&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
