<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OracleCPQVersionQuote</label>
    <protected>false</protected>
    <values>
        <field>SFS_ContentType__c</field>
        <value xsi:type="xsd:string">application/json</value>
    </values>
    <values>
        <field>SFS_Endpoint_URL__c</field>
        <value xsi:type="xsd:string">https://ircompanytest3.bigmachines.com/rest/v12/commerceDocumentsOraclecpqo_bmClone_1Transaction/:id/actions/versionTransactionCustom_t</value>
    </values>
    <values>
        <field>SFS_Interface_Destination__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Password__c</field>
        <value xsi:type="xsd:string">b22_14E(13</value>
    </values>
    <values>
        <field>SFS_RICE_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Username__c</field>
        <value xsi:type="xsd:string">cpqapiuser</value>
    </values>
</CustomMetadata>
