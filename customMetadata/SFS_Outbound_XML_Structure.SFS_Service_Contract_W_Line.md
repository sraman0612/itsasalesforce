<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Service Contract W Line</label>
    <protected>false</protected>
    <values>
        <field>SFS_Interface_Name__c</field>
        <value xsi:type="xsd:string">ServiceAgreementWLine</value>
    </values>
    <values>
        <field>SFS_PA_Agreement_All__c</field>
        <value xsi:type="xsd:string">&lt;G_PA_AGREEMENTS_ALL&gt;&lt;PA_AGREEMENTS_ALL&gt;&lt;PM_AGREEMENT_REFERENCE&gt;contractnumber&lt;/PM_AGREEMENT_REFERENCE&gt;&lt;CUSTOMER_NUM&gt;account.oracle_site_use_id__c&lt;/CUSTOMER_NUM&gt;&lt;AGREEMENT_NUM&gt;customernumber&lt;/AGREEMENT_NUM&gt;&lt;AGREEMENT_TYPE&gt;+RTDEVNAME+&lt;/AGREEMENT_TYPE&gt;&lt;AMOUNT&gt;sfs_agreement_value__c&lt;/AMOUNT&gt;&lt;AGREEMENT_CURRENCY_CODE&gt;currencyisocode&lt;/AGREEMENT_CURRENCY_CODE&gt;&lt;CUSTOMER_ORDER_NUMBER&gt;sfs_ponumber__c&lt;/CUSTOMER_ORDER_NUMBER&gt;&lt;START_DATE&gt;startdate&lt;/START_DATE&gt;&lt;PM_PRODUCT_CODE&gt;SALESFORCE&lt;/PM_PRODUCT_CODE&gt;&lt;TERM_NAME&gt;account.irit_payment_terms__c&lt;/TERM_NAME&gt;&lt;OWNING_ORGANIZATION_CODE&gt;sfs_division__r.sfs_org_code__c&lt;/OWNING_ORGANIZATION_CODE&gt;&lt;UPDATE_AGREEMENT_ALLOWED&gt;Y&lt;/UPDATE_AGREEMENT_ALLOWED&gt;&lt;/PA_AGREEMENTS_ALL&gt;&lt;/G_PA_AGREEMENTS_ALL&gt;</value>
    </values>
    <values>
        <field>SFS_PA_Project_All__c</field>
        <value xsi:type="xsd:string">&lt;G_PA_PROJECTS_ALL&gt;&lt;PA_PROJECTS_ALL&gt;&lt;PM_PROJECT_REFERENCE&gt;+AGRNUMBER+&lt;/PM_PROJECT_REFERENCE&gt;&lt;PA_PROJECT_NUMBER&gt;+AGRNUMBER+&lt;/PA_PROJECT_NUMBER&gt;&lt;PROJECT_NAME&gt;+AGRNAME+&lt;/PROJECT_NAME&gt;&lt;PROJECT_STATUS_CODE&gt;+AGRSTATUS+&lt;/PROJECT_STATUS_CODE&gt;&lt;DESCRIPTION&gt;+AGRDESCRIPTION+&lt;/DESCRIPTION&gt;&lt;START_DATE&gt;+AGRSTARTDATE+&lt;/START_DATE&gt;&lt;COMPLETION_DATE&gt;+AGRCOMPLETEDATE+&lt;/COMPLETION_DATE&gt;&lt;ATTRIBUTE_CATEGORY&gt;+AGRDIVISIONCAT+&lt;/ATTRIBUTE_CATEGORY&gt;&lt;ATTRIBUTE1&gt;+AGRATTRIBUTE1+&lt;/ATTRIBUTE1&gt;&lt;ATTRIBUTE3&gt;+AGRPONUMBER+&lt;/ATTRIBUTE3&gt;&lt;ATTRIBUTE9&gt;+AGRDIVORGCODE+&lt;/ATTRIBUTE9&gt;&lt;CARRYING_OUT_ORGANIZATION_CODE&gt;+AGRDIVORGCODE+&lt;/CARRYING_OUT_ORGANIZATION_CODE&gt;&lt;PROJECT_TYPE&gt;RTDEVNAME&lt;/PROJECT_TYPE&gt;&lt;/PA_PROJECTS_ALL&gt;&lt;/G_PA_PROJECTS_ALL&gt;</value>
    </values>
    <values>
        <field>SFS_Sender__c</field>
        <value xsi:type="xsd:string">&lt;SENDER&gt;&lt;ID&gt;+SENDID+&lt;/ID&gt;&lt;REFERENCE&gt;+REFERENCEID+&lt;/REFERENCE&gt;&lt;/SENDER&gt;</value>
    </values>
    <values>
        <field>SFS_TXN_Controls_Flag__c</field>
        <value xsi:type="xsd:string">&lt;LIMIT_TO_TXN_CONTROLS_FLAG&gt;N&lt;/LIMIT_TO_TXN_CONTROLS_FLAG&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Attribute_1__c</field>
        <value xsi:type="xsd:string">&lt;ATTRIBUTE1&gt;product2.productcode&lt;/ATTRIBUTE1&gt;&lt;/TASKS&gt;&lt;/G_TASKS&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Billable_Flag__c</field>
        <value xsi:type="xsd:string">&lt;BILLABLE_FLAG&gt;sfs_billable__c&lt;/BILLABLE_FLAG&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Chargeable_Flag__c</field>
        <value xsi:type="xsd:string">&lt;CHARGEABLE_FLAG&gt;sfs_charegeable__c&lt;/CHARGEABLE_FLAG&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Description__c</field>
        <value xsi:type="xsd:string">&lt;TASK_DESCRIPTION&gt;product2.description&lt;/TASK_DESCRIPTION&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Name__c</field>
        <value xsi:type="xsd:string">&lt;TASK_NAME&gt;lineitemnumber&lt;/TASK_NAME&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Number__c</field>
        <value xsi:type="xsd:string">&lt;PA_TASK_NUMBER&gt;lineitemnumber&lt;/PA_TASK_NUMBER&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Ready_To_Bill__c</field>
        <value xsi:type="xsd:string">&lt;READY_TO_BILL_FLAG&gt;sfs_ready_to_bill__c&lt;/READY_TO_BILL_FLAG&gt;&lt;/TASKS&gt;&lt;/G_TASKS&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Ready_To_Distribute__c</field>
        <value xsi:type="xsd:string">&lt;READY_TO_DISTRIBUTE_FLAG&gt;sfs_ready_to_distribute__c&lt;/READY_TO_DISTRIBUTE_FLAG&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Reference__c</field>
        <value xsi:type="xsd:string">&lt;G_TASKS&gt;&lt;TASKS&gt;&lt;PM_TASK_REFERENCE&gt;lineitemnumber&lt;/PM_TASK_REFERENCE&gt;</value>
    </values>
    <values>
        <field>SFS_Task_Start_Date__c</field>
        <value xsi:type="xsd:string">&lt;TASK_START_DATE&gt;startdate&lt;/TASK_START_DATE&gt;</value>
    </values>
    <values>
        <field>SFS_testXML__c</field>
        <value xsi:type="xsd:string">&lt;SENDER&gt;&lt;ID&gt;+SENDID+&lt;/ID&gt;&lt;REFERENCE&gt;enddate&lt;/REFERENCE&gt;&lt;/SENDER&gt;</value>
    </values>
</CustomMetadata>
