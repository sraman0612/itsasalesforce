<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DCHL</label>
    <protected>false</protected>
    <values>
        <field>Filter_Field__c</field>
        <value xsi:type="xsd:string">DCHL</value>
    </values>
    <values>
        <field>Product_Field__c</field>
        <value xsi:type="xsd:string">FLD_Distribution_Channel__c</value>
    </values>
</CustomMetadata>
