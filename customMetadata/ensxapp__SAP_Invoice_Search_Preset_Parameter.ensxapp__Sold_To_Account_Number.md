<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sold To Account Number</label>
    <protected>false</protected>
    <values>
        <field>ensxapp__Custom_Field_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ensxapp__Custom__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ensxapp__Mapped_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ensxapp__Mapped_Object__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ensxapp__Parameter_Type__c</field>
        <value xsi:type="xsd:string">SAP Customer Number</value>
    </values>
    <values>
        <field>ensxapp__SAP_Field__c</field>
        <value xsi:type="xsd:string">SoldToParty</value>
    </values>
    <values>
        <field>ensxapp__SAP_Invoice_Search_Preset__c</field>
        <value xsi:type="xsd:string">ensxapp__All_Invoices_Default</value>
    </values>
    <values>
        <field>ensxapp__Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
