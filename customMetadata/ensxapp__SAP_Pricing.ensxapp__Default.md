<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default</label>
    <protected>false</protected>
    <values>
        <field>ensxapp__Missing_Mapping_Behavior__c</field>
        <value xsi:type="xsd:string">Price</value>
    </values>
    <values>
        <field>ensxapp__Order_Type__c</field>
        <value xsi:type="xsd:string">QT</value>
    </values>
</CustomMetadata>
