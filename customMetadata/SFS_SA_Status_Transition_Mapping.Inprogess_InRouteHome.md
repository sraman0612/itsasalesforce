<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Inprogess-InRouteHome</label>
    <protected>false</protected>
    <values>
        <field>Next_Status_Label__c</field>
        <value xsi:type="xsd:string">Multi Day Service Appointment</value>
    </values>
    <values>
        <field>SFS_Next_Status__c</field>
        <value xsi:type="xsd:string">Multi Day Service Appointment</value>
    </values>
    <values>
        <field>SFS_Order__c</field>
        <value xsi:type="xsd:double">5.2</value>
    </values>
    <values>
        <field>SFS_Status__c</field>
        <value xsi:type="xsd:string">In Progress</value>
    </values>
</CustomMetadata>
