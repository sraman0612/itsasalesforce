<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>StartupDate</label>
    <protected>false</protected>
    <values>
        <field>SFS_ContentType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Endpoint_URL__c</field>
        <value xsi:type="xsd:string">https://oci-icosbomwpsh.ingersollrand.com/WTY_TAVANT/TavantSerialCreationAndStartUpDateReqABCSImpl/ProxyService/TAVANTSerialCreationAndStartUpDateService_PS?wsdl</value>
    </values>
    <values>
        <field>SFS_Interface_Destination__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Password__c</field>
        <value xsi:type="xsd:string">IRPrtnrP5hMoz3f3ffo3</value>
    </values>
    <values>
        <field>SFS_RICE_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Username__c</field>
        <value xsi:type="xsd:string">IRPrtnrP5hSalesforce</value>
    </values>
</CustomMetadata>
