<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Independence Day</label>
    <protected>false</protected>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">The Gardner Denver Customer Care Department in Quincy, IL will be closed on Thursday July 2nd and will be staffed with a skeleton crew on Friday July 3rd. 

Our domestic manufacturing facilities will be closed on Thursday July 2nd and Friday July 3rd in observance of the July 4th Holiday.</value>
    </values>
</CustomMetadata>
