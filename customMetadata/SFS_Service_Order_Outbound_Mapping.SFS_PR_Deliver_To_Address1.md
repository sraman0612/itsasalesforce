<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PR Deliver To Address1</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">25.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:type="xsd:string">SFS_Consumables_Ship_To_Account__r-ShippingStreet</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">ProductRequest</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;SOURCE_ATTRIBUTE&gt;&lt;NAME&gt;DELIVER_TO_ADDRESS1&lt;/NAME&gt;&lt;VALUE&gt;SFS_Consumables_Ship_To_Account__r-ShippingStreet&lt;/VALUE&gt;&lt;/SOURCE_ATTRIBUTE&gt;&lt;/G_ADDL_SRC_ATTRIBUTES&gt;&lt;G_REQ_LINES&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">SOURCE_ATTRIBUTE</value>
    </values>
</CustomMetadata>
