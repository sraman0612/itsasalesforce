<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Expenditure Type</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:type="xsd:string">sfs_oracle_expenditure_type__c</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;EXPENDITURE_TYPE&gt;sfs_oracle_expenditure_type__c&lt;/EXPENDITURE_TYPE&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">PA_EXPENDITURE_ITEMS</value>
    </values>
</CustomMetadata>
