<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>108|70422</label>
    <protected>false</protected>
    <values>
        <field>Dealer_Name__c</field>
        <value xsi:type="xsd:string">IRAC - INDIANAPOLIS</value>
    </values>
    <values>
        <field>SFS_Site_Number__c</field>
        <value xsi:type="xsd:string">108|205119|</value>
    </values>
</CustomMetadata>
