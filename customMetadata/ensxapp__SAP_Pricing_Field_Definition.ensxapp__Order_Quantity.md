<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Order Quantity</label>
    <protected>false</protected>
    <values>
        <field>ensxapp__Can_Input__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ensxapp__Can_Output__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ensxapp__Description__c</field>
        <value xsi:type="xsd:string">Number of items ordered</value>
    </values>
    <values>
        <field>ensxapp__Field_Name__c</field>
        <value xsi:type="xsd:string">OrderQuantity</value>
    </values>
    <values>
        <field>ensxapp__Field_Object__c</field>
        <value xsi:type="xsd:string">Item</value>
    </values>
</CustomMetadata>
