<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>INT-155</label>
    <protected>false</protected>
    <values>
        <field>SFS_ContentType__c</field>
        <value xsi:type="xsd:string">text/xml</value>
    </values>
    <values>
        <field>SFS_Endpoint_URL__c</field>
        <value xsi:type="xsd:string">https://irbtpdev-otsydqmv.it-cpi013-rt.cfapps.us21.hana.ondemand.com/http/salesforce/projectbillingnrevenew/11i/uat</value>
    </values>
    <values>
        <field>SFS_Interface_Destination__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Password__c</field>
        <value xsi:type="xsd:string">LGD@BasisCPI21</value>
    </values>
    <values>
        <field>SFS_RICE_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Username__c</field>
        <value xsi:type="xsd:string">S0023675326</value>
    </values>
</CustomMetadata>
