<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Good Friday</label>
    <protected>false</protected>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Gardner Denver will be closed Friday, April 10th, in observance of Good Friday. During this time, all support processes as well as the plants will be on shut-down. 

However, Online Order Entry as well as all other online systems will still be available. Please excuse the delay in response to your inquiry. We will resume full operations on Monday, April 13th.</value>
    </values>
</CustomMetadata>
