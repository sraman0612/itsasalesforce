<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Test</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;env:Envelope
	xmlns:env=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot;
	xmlns:wsa=&quot;http://www.w3.org/2005/08/addressing&quot;&gt;
	&lt;env:Header&gt;
		&lt;wsa:Action&gt;execute&lt;/wsa:Action&gt;
		&lt;wsa:MessageID&gt;urn:06dbca19-9650-11ec-b011-02001708e866&lt;/wsa:MessageID&gt;
		&lt;wsa:ReplyTo&gt;
			&lt;wsa:Address&gt;http://www.w3.org/2005/08/addressing/anonymous&lt;/wsa:Address&gt;
			&lt;wsa:ReferenceParameters&gt;
				&lt;instra:tracking.ecid
					xmlns:instra=&quot;http://xmlns.oracle.com/sca/tracking/1.0&quot;&gt;efdbe64f-8c4f-4d51-970a-9a23aca858b4-019cdae0
				&lt;/instra:tracking.ecid&gt;
				&lt;instra:tracking.FlowEventId
					xmlns:instra=&quot;http://xmlns.oracle.com/sca/tracking/1.0&quot;&gt;65910440
				&lt;/instra:tracking.FlowEventId&gt;
				&lt;instra:tracking.FlowId
					xmlns:instra=&quot;http://xmlns.oracle.com/sca/tracking/1.0&quot;&gt;10236104
				&lt;/instra:tracking.FlowId&gt;
				&lt;instra:tracking.CorrelationFlowId
					xmlns:instra=&quot;http://xmlns.oracle.com/sca/tracking/1.0&quot;&gt;0000NwlWN346qIB_vXh8iX1Y5u^f00000k
				&lt;/instra:tracking.CorrelationFlowId&gt;
				&lt;instra:tracking.quiescing.SCAEntityId
					xmlns:instra=&quot;http://xmlns.oracle.com/sca/tracking/1.0&quot;&gt;3760029
				&lt;/instra:tracking.quiescing.SCAEntityId&gt;
			&lt;/wsa:ReferenceParameters&gt;
		&lt;/wsa:ReplyTo&gt;
		&lt;wsa:FaultTo&gt;
			&lt;wsa:Address&gt;http://www.w3.org/2005/08/addressing/anonymous&lt;/wsa:Address&gt;
		&lt;/wsa:FaultTo&gt;
	&lt;/env:Header&gt;
	&lt;env:Body&gt;
		&lt;ATPResponse
			xmlns:wsa=&quot;http://www.w3.org/2005/08/addressing&quot;
			xmlns=&quot;http://xmlns.irco.com/ATPPriceOutResponseABCSImpl&quot;&gt;
			&lt;ControlArea
				xmlns=&quot;&quot;&gt;
				&lt;PartnerCode&gt;CPQ&lt;/PartnerCode&gt;
				&lt;RequestType&gt;QUERY&lt;/RequestType&gt;
				&lt;ExternalMessageID&gt;CPQ TestArjun Test Opty 10012021_CTS-76421645803040608&lt;/ExternalMessageID&gt;
				&lt;Database&gt;ISOADEV1&lt;/Database&gt;
				&lt;SourceSystem&gt;EBS&lt;/SourceSystem&gt;
				&lt;Status&gt;SUCCESS&lt;/Status&gt;
				&lt;Messsage&gt;Request Processed Successfully&lt;/Messsage&gt;
				&lt;MessageID&gt;D5DF380550575F90E0540021280BE0EF&lt;/MessageID&gt;
			&lt;/ControlArea&gt;
			&lt;DataArea
				xmlns=&quot;&quot;&gt;
				&lt;GItem&gt;
					&lt;Item&gt;
						&lt;ItemNumber&gt;23231806&lt;/ItemNumber&gt;
						&lt;BestWHSE&gt;DLC (DCL)&lt;/BestWHSE&gt;
						&lt;GAvailability&gt;
							&lt;Availability&gt;
								&lt;WHSE&gt;DLC (DCL)&lt;/WHSE&gt;
								&lt;ResultCode&gt;SUCCESS&lt;/ResultCode&gt;
								&lt;AvailableDate&gt;25-FEB-2022&lt;/AvailableDate&gt;
							&lt;/Availability&gt;
						&lt;/GAvailability&gt;
					&lt;/Item&gt;
				&lt;/GItem&gt;
			&lt;/DataArea&gt;
		&lt;/ATPResponse&gt;
	&lt;/env:Body&gt;
&lt;/env:Envelope&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
