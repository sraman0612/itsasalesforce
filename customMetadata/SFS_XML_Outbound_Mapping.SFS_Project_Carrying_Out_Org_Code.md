<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Project Carrying Out Org Code</label>
    <protected>false</protected>
    <values>
        <field>SFS_Child_Start__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">26.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:type="xsd:string">sfs_org_code__c</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">Service Contract</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;CARRYING_OUT_ORGANIZATION_CODE&gt;sfs_org_code__c&lt;/CARRYING_OUT_ORGANIZATION_CODE&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">PA Projects All</value>
    </values>
</CustomMetadata>
