<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <label>Email Signature Main</label>
    <protected>false</protected>
    <values>
        <field>Email_Signature_Message__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Email_Signature_Special2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Email_Signature_Special3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Email_Signature_Special__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
