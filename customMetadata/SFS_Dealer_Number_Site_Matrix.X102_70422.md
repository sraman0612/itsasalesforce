<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>102|70422</label>
    <protected>false</protected>
    <values>
        <field>Dealer_Name__c</field>
        <value xsi:type="xsd:string">IRAC - DALLAS</value>
    </values>
    <values>
        <field>SFS_Site_Number__c</field>
        <value xsi:type="xsd:string">102|205113|</value>
    </values>
</CustomMetadata>
