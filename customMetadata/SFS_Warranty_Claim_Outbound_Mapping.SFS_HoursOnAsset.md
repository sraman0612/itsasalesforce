<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>HoursOnAsset</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">14.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:type="xsd:string">SFS_Asset_Run_Hours__c</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">Warranty Claim</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;req:HoursInService&gt;SFS_Asset_Run_Hours__c&lt;/req:HoursInService&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">Tavant</value>
    </values>
</CustomMetadata>
