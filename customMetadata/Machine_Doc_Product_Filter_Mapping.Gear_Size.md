<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Gear Size</label>
    <protected>false</protected>
    <values>
        <field>Filter_Field__c</field>
        <value xsi:type="xsd:string">gear_size</value>
    </values>
    <values>
        <field>Product_Field__c</field>
        <value xsi:type="xsd:string">Gear_Size__c</value>
    </values>
</CustomMetadata>
