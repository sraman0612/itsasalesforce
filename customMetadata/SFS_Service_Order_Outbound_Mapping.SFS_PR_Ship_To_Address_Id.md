<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PR Ship To Address Id</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">23.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:type="xsd:string">SFS_Consumables_Ship_To_Account__r-Oracle_Site_Use_Id__c</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">ProductRequest</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;SOURCE_ATTRIBUTE&gt;&lt;NAME&gt;SHIP_TO_ADDRESS_ID&lt;/NAME&gt;&lt;VALUE&gt;SFS_Consumables_Ship_To_Account__r-Oracle_Site_Use_Id__c&lt;/VALUE&gt;&lt;/SOURCE_ATTRIBUTE&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">SOURCE_ATTRIBUTE</value>
    </values>
</CustomMetadata>
