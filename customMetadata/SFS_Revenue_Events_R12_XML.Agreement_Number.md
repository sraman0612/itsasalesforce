<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Agreement Number</label>
    <protected>false</protected>
    <values>
        <field>Full_XML__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Hardcode_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Node_Order__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">Service Agreement</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;P_AGREEMENT_NUMBER&gt;AGR-00000409&lt;/P_AGREEMENT_NUMBER&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">Data Area</value>
    </values>
    <values>
        <field>Salesforce_Field__c</field>
        <value xsi:type="xsd:string">agreementnumber</value>
    </values>
</CustomMetadata>
