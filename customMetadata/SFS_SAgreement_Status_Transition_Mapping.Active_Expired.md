<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Active-Expired</label>
    <protected>false</protected>
    <values>
        <field>SFS_Next_Status__c</field>
        <value xsi:type="xsd:string">Expired</value>
    </values>
    <values>
        <field>SFS_RecordType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Status__c</field>
        <value xsi:type="xsd:string">APPROVED</value>
    </values>
</CustomMetadata>
