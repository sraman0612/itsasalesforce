<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Amount</label>
    <protected>false</protected>
    <values>
        <field>SFS_Charge_Type__c</field>
        <value xsi:type="xsd:string">NRC Charge</value>
    </values>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">62.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:type="xsd:string">CAP_IR_Amount__c</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">Charge Details</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;Amount&gt;CAP_IR_Amount__c&lt;/Amount&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">Charge Details</value>
    </values>
</CustomMetadata>
