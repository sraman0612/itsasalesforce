<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Partner Code</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">ProductRequest</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;EBS_RCV_TRANSACTION&gt;
  &lt;CONTROLAREA&gt;
    &lt;PARTNER_CODE&gt;SFS-NA&lt;/PARTNER_CODE&gt;
    &lt;INSTANCE_NAME&gt;ISOADEV1&lt;/INSTANCE_NAME&gt;
    &lt;REQUEST_TYPE&gt;EXP_RECEIVE&lt;/REQUEST_TYPE&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">CONTROLAREA</value>
    </values>
</CustomMetadata>
