<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Christmas</label>
    <protected>false</protected>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">In observance of the Christmas and New Year’s holidays – Gardner Denver will be closed Tuesday and Wednesday, December 24th and 25th as well as Tuesday and Wednesday, December 31st and January 1st, 2020. On Tuesday, December 31st a limited number of Customer Support personnel will be available to handle orders from 8am to Noon. We will resume normal business hours on Thursday, January 2nd.

The Princeton and Sedalia facilities will be closed for physical inventory January 1st through January 3rd. Only Emergency Orders will be processed for shipment during this time.Online Order Entry will remain operational during this time. Have a Safe and Happy Holiday Season!</value>
    </values>
</CustomMetadata>
