<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Material Group</label>
    <protected>false</protected>
    <values>
        <field>ensxapp__Editable__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ensxapp__Entity__c</field>
        <value xsi:type="xsd:string">Product2</value>
    </values>
    <values>
        <field>ensxapp__Parent__c</field>
        <value xsi:type="xsd:string">ensxapp__Settings</value>
    </values>
    <values>
        <field>ensxapp__Product2_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ensxapp__SAP_Field__c</field>
        <value xsi:type="xsd:string">MaterialGroup</value>
    </values>
    <values>
        <field>ensxapp__Visible__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
