<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>FR RelatedChargeType</label>
    <protected>false</protected>
    <values>
        <field>SFS_Charge_Type__c</field>
        <value xsi:type="xsd:string">Freight / S&amp;H</value>
    </values>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">67.12</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">Invoice Line Item Details</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;IRITRelatedChargeType&gt;&lt;/IRITRelatedChargeType&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">FsInvoiceLineItemDetails</value>
    </values>
</CustomMetadata>
