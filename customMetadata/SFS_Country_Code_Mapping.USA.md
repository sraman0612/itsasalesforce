<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>USA</label>
    <protected>false</protected>
    <values>
        <field>Country_Short_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SFS_Locale__c</field>
        <value xsi:type="xsd:string">en_US</value>
    </values>
    <values>
        <field>Two_Digit_ISO_Code__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
