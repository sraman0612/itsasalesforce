<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Labor Day</label>
    <protected>false</protected>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Gardner Denver will be closed Monday, September 7th, in observance of Labor Day. During this time, all support processes, as well as the plants, will be on shut-down. 

However, Online Order Entry along with all other online systems will still be available. Please excuse the delay in response to your inquiry. We will resume full operations on Tuesday, September 8th.</value>
    </values>
</CustomMetadata>
