<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pk-Con</label>
    <protected>false</protected>
    <values>
        <field>SFS_Coverage_Type__c</field>
        <value xsi:type="xsd:string">PackageCARE</value>
    </values>
    <values>
        <field>SFS_GanttIconUrl__c</field>
        <value xsi:type="xsd:string">https://ircoam--uat--c.sandbox.vf.force.com/resource/1682322376000/SFS_ContPkCare?</value>
    </values>
    <values>
        <field>SFS_Work_Order_Type__c</field>
        <value xsi:type="xsd:string">Contracting</value>
    </values>
</CustomMetadata>
