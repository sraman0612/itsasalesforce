<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Funding Project Number</label>
    <protected>false</protected>
    <values>
        <field>SFS_Hardcoded_Flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SFS_Node_Order__c</field>
        <value xsi:type="xsd:double">52.0</value>
    </values>
    <values>
        <field>SFS_Salesforce_Field__c</field>
        <value xsi:type="xsd:string">sfs_external_id__c</value>
    </values>
    <values>
        <field>SFS_Salesforce_Object__c</field>
        <value xsi:type="xsd:string">WorkOrder</value>
    </values>
    <values>
        <field>SFS_XML_Full_Name__c</field>
        <value xsi:type="xsd:string">&lt;PA_PROJECT_NUMBER&gt;sfs_external_id__c&lt;/PA_PROJECT_NUMBER&gt;&lt;/PA_PROJECT_FUNDING&gt;&lt;/G_PA_PROJECT_FUNDINGS&gt;&lt;/PA_AGREEMENTS_ALL&gt;&lt;/G_PA_AGREEMENTS_ALL&gt;&lt;/G_XXPA2381_INT&gt;</value>
    </values>
    <values>
        <field>SFS_XML_Object__c</field>
        <value xsi:type="xsd:string">PA Project Fundings</value>
    </values>
</CustomMetadata>
