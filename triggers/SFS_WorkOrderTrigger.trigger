/*
Author: Sucharitha Suragala, Capgemini
Updated by: Ryan Reddish, Capgemini
Date: 10/05/2021
Description: This trigger populates the Service Territory field on WorkOrder with the contents of the Address Field.

*/
trigger SFS_WorkOrderTrigger on WorkOrder (before insert, before update, after insert,after update) {
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('WorkOrder');
    if(skipTrigger!=True){
        Boolean accountExists=false;
        Boolean ServiceTerr=true;
        if(trigger.isBefore){
            if(trigger.isInsert){
                for(WorkOrder wo:Trigger.New){
                    if(wo.MaintenancePlanId!=null){
                        ServiceTerr=false;
                        break;
                    }
                }
                if(ServiceTerr==true)
                   SFS_WorkOrderHandler.callPolygonUtilsForST(null,trigger.new); 
            }          
            else if(trigger.isUpdate){                               
                SFS_WorkOrderHandler.checkPurchaseExpense(trigger.oldMap,trigger.new);
                Boolean isWOAdmin = FeatureManagement.checkPermission('Work_Order_Admin');
                for(WorkOrder wo:Trigger.New){
                
                    WorkOrder oldRecord=Trigger.OldMap.get(wo.Id);              
                    if(oldRecord.status=='Closed' && isWOAdmin != TRUE && wo.SFS_Integration_Status__c == oldRecord.SFS_Integration_Status__c && wo.SFS_Integration_Response__c ==oldRecord.SFS_Integration_Response__c  && (wo.SFS_Asset_Merge__c==false ||(wo.SFS_Asset_Merge__c==true && oldRecord.SFS_Asset_Merge__c!=false))){
                        wo.addError('You cannot edit a closed WO');
                    }
                }
                
                for(WorkOrder wo:Trigger.New){
                    if(wo.MaintenancePlanId!=null){
                        ServiceTerr=false;
                        break;
                    }
                }
                if(ServiceTerr==true)
                   SFS_WorkOrderHandler.callPolygonUtilsForST(trigger.oldMap,trigger.new); 
            }            
        }  
        if(trigger.isAfter){             
            if(trigger.isUpdate){
                Boolean createCharges=false;
                for(WorkOrder wo:Trigger.New){
                    WorkOrder oldRecord=Trigger.OldMap.get(wo.Id);
                    if(wo.status=='Completed' && oldRecord.Status!=wo.status){
                        createCharges=true;
                        break;
                    }
                }

                if(createCharges==true){
                    SFS_WorkOrderHandler.createChargesForWOLI(Trigger.new,trigger.oldMap);
                }
                SFS_WorkOrderHandler.updateWarrantyJobCode(trigger.oldMap,trigger.new);
            }
           
        }
    }
}