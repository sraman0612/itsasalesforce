trigger AssetTrigger on Asset (before insert,before update, after insert, after update,before delete) {
    System.debug('in Asset Old Trigger1');  
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Asset');    
    if(skipTrigger!=True){ 
        System.debug('in Asset Old Trigger');
        List<Asset> newAssetList = new List<Asset>();
        List<Asset> LIR_AssetList = new List<Asset>();
        List<Asset> LGD_AssetList = new List<Asset>();
        List<Asset> LIR_After_AssetList = new List<Asset>();
        List<Asset> LGD_After_AssetList = new List<Asset>();
        List<Asset> SFS_Asset_List = new List<Asset>();
        Map<Id,Asset> LGD_Map = new Map<Id,Asset>();
        List<Asset> sTrchannel = new List<Asset>();

        if (trigger.isbefore && !Trigger.IsDelete){
            sTrchannel=CGCommonUtility.branchingAndChannelPopulation(trigger.new);         
        }

        if(!trigger.ISDelete){
            for(Asset a :trigger.new){
                if(trigger.isAfter){
                    if(a.CG_Org_Channel__c!=null){
                        if(a.CG_Org_Channel__c ==System.Label.CG_GD_Trigger){
                            LGD_After_AssetList.add(a);
                        }
                        else if(a.CG_Org_Channel__c ==System.Label.CG_IR_Trigger){
                            LIR_After_AssetList.add(a);
                        }
                    }
                    
                }
            }
            for(Asset ass:sTrchannel){
                if(ass.RecordTypeId <> null)
                    ass.RecordTypeName__c = Schema.SObjectType.Asset.getRecordTypeInfosById().get(ass.recordTypeId).getDeveloperName();
                
                if(ass.CG_Org_Channel__c == System.Label.CG_IR_Trigger){
                    LIR_AssetList.add(ass);
                    
                }else if(ass.CG_Org_Channel__c == System.Label.CG_GD_Trigger){
                    LGD_AssetList.add(ass);
                    LGD_Map.put(ass.id, ass);
                  }
            }
        }  
        Set<Id> assetIds = new Set<Id>();
        Id airCreateRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('NA Air Create').getRecordTypeId();
        Id airEditRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('NA Air Edit').getRecordTypeId();
        Id rentalRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Rental').getRecordTypeId();
        if(!trigger.IsDelete){
            for(Asset record : Trigger.New){
                if(record.recordTypeId == airEditRecordTypeId || record.recordTypeId == rentalRecordTypeId || record.recordTypeId == airCreateRecordTypeId){
                    SFS_Asset_List.add(record);
                    assetIds.add(record.Id);
                }
            }
        }

        switch on Trigger.operationType {
            When BEFORE_INSERT{ 
                if(LIR_AssetList.size() > 0){
                    LIR_Asset_triggerHandler.beforeInsert(LIR_AssetList);
                }
                //SFS_AssetTriggerHandler.frameTypeProductcodeMatch(Trigger.new);
            }
            When BEFORE_UPDATE{ 
                if(LGD_AssetList.size() > 0){
                    LGD_Asset_TriggerHandler.beforeUpdate(LGD_AssetList,LGD_Map, trigger.oldMap);
                }
                if(SFS_Asset_List.size() > 0){
                    SFS_AssetTriggerHandler.Assetnamechanged(SFS_Asset_List,trigger.oldMap);
                }    
            }
            When AFTER_INSERT{
                if(LIR_After_AssetList.size() > 0){
                    LIR_Asset_triggerHandler.afterInsert(LIR_After_AssetList, trigger.old);
                }
                if(LGD_Map.size() > 0){
                    LGD_Asset_TriggerHandler.afterInsert(LGD_Map, trigger.oldMap);
                }         
                
                if(SFS_Asset_List.size() > 0){
                    //Product Code of Frame Type should match the Product Code of the above Product
                    Id profileId=userInfo.getProfileId();
                    String profileName=[Select id, Name from Profile where Id=:profileId].Name;
                    Map<Id,Asset> assetMap = new Map<Id,Asset>([Select id ,RecordType.DeveloperName,CG_Org_Channel__c,CTS_Frame_Type__r.Product_Code__c,Product2.SFS_Product_Code__c from Asset where id IN: assetIds]);
                    for(Asset st:trigger.new){ 
                        System.debug('product code' + assetMap.get(st.Id).Product2.SFS_Product_Code__c);
                        System.debug('frame type code' + assetMap.get(st.Id).CTS_Frame_Type__r.Product_Code__c);
                        if(assetMap.get(st.Id)!=null && assetMap.get(st.Id).CG_Org_Channel__c == System.Label.CG_IR_Trigger && assetMap.get(st.Id).Product2.SFS_Product_Code__c!= assetMap.get(st.Id).CTS_Frame_Type__r.Product_Code__c && profileName != 'API_Profile' && assetMap.get(st.Id).RecordType.DeveloperName!='SFS_Asset_Potential'){
                            st.addError('Product Code of the Frame Type MUST match the Product Code of the Asset');
                        }
                    }
                    
                    SFS_AssetTriggerHandler.syncRentalAssetFieldsOnOracleDB(trigger.new, 'create', trigger.oldMap);
                    List<Asset> AssList=new List<Asset>();               
                    for(Asset ast:SFS_Asset_List){
                        if(LIR_After_AssetList.contains(ast)){
                            AssList.add(ast); 
                        }
                    }
                    if(AssList.size()>0){
                        SFS_AssetTriggerHandler.createAssetRecords(AssList);
                    }
                }
            }
            When AFTER_UPDATE{
                if(LIR_After_AssetList.size() > 0){
                    LIR_Asset_triggerHandler.afterUpdate(LIR_After_AssetList, trigger.old);
                }
                if(LGD_After_AssetList.size() > 0){
                    LGD_Asset_TriggerHandler.afterUpdate(LGD_Map, trigger.oldMap);
                }
                if(SFS_Asset_List.size() > 0){
                    SFS_AssetTriggerHandler.syncRentalAssetFieldsOnOracleDB(SFS_Asset_List, 'update', trigger.oldMap);
                    
                    
                    List<Asset> AssList1=new List<Asset>();               
                    Id OldValue;
                    for(Asset ast:SFS_Asset_List){
                        OldValue = Trigger.oldmap.get(ast.Id).CTS_Frame_Type__c;
                        if(OldValue!=ast.CTS_Frame_Type__c && LIR_After_AssetList.contains(ast)){
                            AssList1.add(ast); 
                        }
                    }
                    if(AssList1.size()>0){
                        SFS_AssetTriggerHandler.createAssetRecords(AssList1);
                    }
                }
            }
            
            When AFTER_DELETE{
                if(LIR_After_AssetList.size() > 0){
                    LIR_Asset_triggerHandler.afterDelete(LIR_After_AssetList, trigger.old);
                }
            }
            When BEFORE_DELETE{
                if(SFS_Asset_List.size() > 0){
                    SFS_AssetTriggerHandler.checkChildRecords(Trigger.Old);
                }
            }
        }
        
    }
    
    
}