//When an EmailMessage record is created, modify the 'To' address to all lower case characters for standardization for any workflows

trigger C_To_Address_To_LowerCase on EmailMessage (before insert) {

    List<Email_Field_Map__c> emailList = [SELECT GDI_From_Address__c FROM Email_Field_Map__c WHERE GDI_From_Address__c != null];
    Set<String> gdiAddresses = new Set<String>();
    for(Email_Field_Map__c efm : emailList){
        gdiAddresses.add(efm.GDI_From_Address__c);
    }

    for (EmailMessage em: Trigger.new) {    
        if(!String.isEmpty(em.ToAddress)){    
            em.ToAddress = em.ToAddress.toLowerCase();
            if(em.Incoming){
                String[] addresses = em.ToAddress.split(';');
                String newTo = '';
                //move any addresses that aren't @gardnerdenver to the CC list
                for(String s : addresses){
                    if(!gdiAddresses.contains(s)){
                        if(String.isEmpty(em.CcAddress)){
                            em.CcAddress = s;
                        }
                        else{
                            em.CcAddress += (';'+s);
                        }
                    }
                    else{
                        newTo += s;
                    }
                }
                em.ToAddress = newTo;
            }
        }
    }
}