trigger iConn_Alert_Proxy_event on iConn_Alert__e (after insert) {
    for (iConn_Alert__e alert : Trigger.New) {
        System.debug('Platform Event created! Calling Apex...');
        //System.debug(alert);
        
        CreateAlertFromEvent.addAlert(alert);
    }
}