trigger SFS_ChargeTrigger on CAP_IR_Charge__c (after insert,after update,after delete,after Undelete, before Update, before delete) {
    
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('CAP_IR_Charge__c');
    if(skipTrigger!=true){  
        
        switch on Trigger.operationType {
            When AFTER_INSERT{
                List<CAP_IR_Charge__c> insertCList=new List<CAP_IR_Charge__c>();
                for(CAP_IR_Charge__c charge : Trigger.New){
                    if(charge.SFS_Sell_Price__c!=null){
                        insertCList.add(charge);              
                    }
                }
                SFS_ChargeTriggerHandler.invoiceAmountRollUpSummary(Trigger.New,null);
                SFS_ChargeTriggerHandler.updateChargeCount(Trigger.New,null);
                SFS_ChargeTriggerHandler.assignChargeAmount(Trigger.New,null);
                SFS_ChargeTriggerHandler.rollUpAmountToWorkOrder(insertCList,Trigger.oldMap);
                SFS_ChargeTriggerHandler.handleChargeTrigger(Trigger.new, Trigger.oldMap, false);
            }
           
            When BEFORE_Update{           
                SFS_ChargeTriggerHandler.assignSellPrice(Trigger.New,Trigger.oldMap);                
                
            }
            When AFTER_UPDATE{
                List<CAP_IR_Charge__c> chargeList=new List<CAP_IR_Charge__c>();
                for(CAP_IR_Charge__c charge : Trigger.New){
                    CAP_IR_Charge__c oldRecord=Trigger.OldMap.get(charge.Id);
                    if(charge.CAP_IR_Work_Order__c !=oldRecord.CAP_IR_Work_Order__c || 
                       charge.SFS_Sell_Price__c != oldRecord.SFS_Sell_Price__c){
                           chargeList.add(charge);
                       }
                }
                
                SFS_ChargeTriggerHandler.invoiceAmountRollUpSummary(Trigger.New,Trigger.oldMap);
                SFS_ChargeTriggerHandler.assignChargeAmount(Trigger.New, Trigger.oldMap);
                SFS_ChargeTriggerHandler.rollUpAmountToWorkOrder(chargeList,Trigger.oldMap);         
               SFS_ChargeTriggerHandler.handleChargeTrigger(Trigger.new, Trigger.oldMap, false);
            }
            When AFTER_Delete{
                SFS_ChargeTriggerHandler.invoiceAmountRollUpSummary(Trigger.old,null);
                SFS_ChargeTriggerHandler.assignChargeAmount(Trigger.old,null);
                SFS_ChargeTriggerHandler.rollUpAmountToWorkOrder(Trigger.Old, Trigger.oldMap);
                SFS_ChargeTriggerHandler.handleChargeTrigger(new List<CAP_IR_Charge__c>(), Trigger.oldMap, true);
            }
            When BEFORE_DELETE{
                //Restrict charge deletion when related invoice is submitted for care team and service coordinator
                If(Trigger.isBefore && Trigger.isDelete){   
                    Id profileId = userInfo.getProfileId();
                    String profilename=[SELECT Id, Name from Profile WHERE Id=:profileId].Name;                    
                    Id charid;
                    for(CAP_IR_Charge__c Ch:Trigger.old){
                        if(Ch.SFS_Invoice__c!=NULL){
                            charid=Ch.SFS_Invoice__c;
                            
                        }                       
                    }
                    Invoice__c char1 = New Invoice__c();
                    if(charid!=NULL){
                    char1=[Select Id,SFS_Status__c From Invoice__c where Id=:charid];
                    }
                    for(CAP_IR_Charge__c chr : Trigger.old){
                        If(Char1.SFS_Status__c =='Submitted' && (profilename=='SFS Service'|| profilename=='SFS Care Team')){
                            chr.addError('Charge cannot be deleted when related Invoice is submitted.');
                        }
                    }
                }
            }
            When AFTER_Undelete{
                SFS_ChargeTriggerHandler.invoiceAmountRollUpSummary(Trigger.New,null);
                SFS_ChargeTriggerHandler.assignChargeAmount(Trigger.New,null); 
                SFS_ChargeTriggerHandler.handleChargeTrigger(Trigger.new, new Map<Id, CAP_IR_Charge__c>(), false);
            }
        }
        
    }
    
}