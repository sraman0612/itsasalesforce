//trigger C_AssetAfterTrigger on Asset (after delete) {
//test class C_iConnContactAfterTriggerTest
trigger C_AssetAfterTrigger on Asset (after insert, before update, after update) {
    if(Trigger.isInsert)
    {
        //Sends newly inserted asset to get sharing permissions created
        //C_AssetSharingCreation.createAssetShareOnAssetInsert(Trigger.new);
        
    }
    if(trigger.isUpdate)
    { 
        if(trigger.isBefore){
            for(Asset a: Trigger.new){
                Asset oldAsset = Trigger.oldMap.get(a.Id);
                if(String.isBlank(a.IMEI__c) && String.isNotBlank(oldAsset.IMEI__c) && a.iConn_Retro_Fit_Kit__c){
                    //prevent IMEI nulling out if Retro Fit kit is checked
                    a.IMEI__c = oldAsset.IMEI__c;
                }
            }
        }
        else{
            
            //List that will hold all Asset Sharing until time for insert
            List<AssetShare> assetShareToCreate = new List<AssetShare>();
            List<Serial_Number_Contact__Share> jobShrs  = new List<Serial_Number_Contact__Share>();
            List<string> aID = new List<string>();
            
            for(Asset a: Trigger.new){
                //Get old value of currentservicer
                Asset oldAsset = Trigger.oldMap.get(a.Id);
                
                //If Value has been updated
                if(oldAsset.Current_Servicer_ID__c != a.Current_Servicer_ID__c)
                {
                    
                    aID.add(a.id);
                    
                    //all enduser contacts related to the asset 
                    Serial_Number_Contact__c[] shList =[select id from Serial_Number_Contact__c where Serial_Number__c = :a.id and Relationship__c = 'End User'];
                    System.debug(shList);
                    for(Serial_Number_Contact__c sc : shList)
                    {
                        if(!string.isBlank(a.Current_Servicer_ID__c)){
                            for(User u : [Select id, Partner_Account_ID__c from user where Partner_Account_ID__c = :string.valueOf(a.Current_Servicer_ID__c).substring(0,15) ])
                            {
                                Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
                                share.AccessLevel='Edit';
                                share.ParentId=sc.id;
                                share.RowCause='Manual';
                                share.UserOrGroupId=u.id;
                                
                                jobShrs.add(share);
                                
                            }
                        }
                        if(!string.isBlank(a.Current_Servicer_Parent_ID__c )){
                            for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :string.valueOf(a.Current_Servicer_Parent_ID__c ).substring(0,15)])
                            {
                                Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
                                share.AccessLevel='Edit';
                                share.ParentId=sc.id;
                                share.RowCause='Manual';
                                share.UserOrGroupId=u.id;
                                
                                jobShrs.add(share);
                            }
                        }
                    }
                    /*Serial_Number_Contact__c[] shList2 =[select id from Serial_Number_Contact__c where Serial_Number__c = :a.id and Relationship__c = 'Distributor'];

for(Serial_Number_Contact__c sc : shList2)
{
if(!string.isBlank(a.accountid)){
for(User u : [Select id, Partner_Account_ID__c from user where Partner_Account_ID__c = :string.valueOf(a.accountid).substring(0,15) ])
{
Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
share.AccessLevel='Edit';
share.ParentId=sc.id;
share.RowCause='Manual';
share.UserOrGroupId=u.id;

jobShrs.add(share);

}
}

if(!string.isBlank(a.Parent_AccountID__c))
{
for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :string.valueOf(a.Parent_AccountID__c).substring(0,15) ])
{
Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
share.AccessLevel='Edit';
share.ParentId=sc.id;
share.RowCause='Manual';
share.UserOrGroupId=u.id;

jobShrs.add(share);
}
}
}*/
                    
                    //Create sharing for users who have the Current Servicer ID as their Partner Account ID
                    if(!string.isBlank(a.Current_Servicer_ID__c)){
                        for(User u : [Select id, Partner_Account_ID__c from user where Partner_Account_ID__c = :string.valueOf(a.Current_Servicer_ID__c).substring(0,15) ])
                        {
                            AssetShare share = new AssetShare();
                            share.assetAccessLevel='Edit';
                            share.AssetId=a.id;
                            share.RowCause='Manual';
                            share.UserOrGroupId=u.id;
                            
                            assetShareToCreate.add(share);
                        }
                    }
                    if(!string.isBlank(a.Current_Servicer_Parent_ID__c )){
                        for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :string.valueOf(a.Current_Servicer_Parent_ID__c ).substring(0,15)])
                        {
                            AssetShare share = new AssetShare();
                            share.assetAccessLevel='Edit';
                            share.AssetId=a.id;
                            share.RowCause='Manual';
                            share.UserOrGroupId=u.id;
                            
                            assetShareToCreate.add(share);
                        }
                    }
                    if(!string.isBlank(a.Parent_AccountID__c))
                    {
                        for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :string.valueOf(a.Parent_AccountID__c).substring(0,15) ])
                        {
                            AssetShare share = new AssetShare();
                            share.assetAccessLevel='Edit';
                            share.AssetId=a.id;
                            share.RowCause='Manual';
                            share.UserOrGroupId=u.id;
                            
                            assetShareToCreate.add(share);
                        }
                    }
                }
                
                
                
            }
            
            Database.SaveResult[] srList = null;
            if(assetShareToCreate.size()>0){
                srList = Database.insert(assetShareToCreate, false);
                
                // Iterate through each returned result
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Asset Share. Asset ID: ' + sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Asset fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }
            srList = Database.insert(jobShrs, false);
            
        }
    }
}