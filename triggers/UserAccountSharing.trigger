trigger UserAccountSharing on User (after insert, after update) {

    List<String> usersToUpdate = new List<String>();
    for (User u : Trigger.new) {
        if (u.AccountId != null && (u.Profile_Name__c == AccountSharingHelper.STR_SALES_REP_ACCOUNT_USER_PROFILE_NAME || u.Profile_Name__c == AccountSharingHelper.GLOBAL_ACCOUNT_USER_PROFILE_NAME )&& (Trigger.isInsert || (Trigger.isUpdate && Trigger.oldMap.get(u.Id).Profile_Name__c != u.Profile_Name__c))) {
            usersToUpdate.add(u.Id);
        }
    }

    if (usersToUpdate.size() > 0) {
        AccountSharingHelper.updateShares(usersToUpdate);
    }
}