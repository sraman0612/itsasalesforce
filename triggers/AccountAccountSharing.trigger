trigger AccountAccountSharing on Account (after insert, after update) {

    List<String> processAccountIds = new List<String>();
    List<String> processAssociatedRepAccountIds = new List<String>();

    for (Account a : Trigger.new) {
        if ((Trigger.isInsert && a.Rep_Account2__c != null) || (Trigger.isUpdate 
            && (Trigger.oldMap.get(a.Id).Rep_Account2__c != a.Rep_Account2__c || Trigger.oldMap.get(a.Id).Owner != a.Owner))) {
            processAccountIds.add(a.Id);
        }
        else if ((Trigger.isInsert && a.Associated_Rep_Account__c != null) || (Trigger.isUpdate 
                 && (Trigger.oldMap.get(a.Id).Associated_Rep_Account__c != a.Associated_Rep_Account__c || Trigger.oldMap.get(a.Id).Owner != a.Owner))) {
            processAssociatedRepAccountIds.add(a.Associated_Rep_Account__c);
        }
    }

    if (processAccountIds.size() > 0) {
        AccountSharingHelper.processAccounts(processAccountIds);
    }

    if(processAssociatedRepAccountIds.size() > 0) {
        AccountSharingHelper.processAssociatedRepAccounts(processAssociatedRepAccountIds);
    }
}