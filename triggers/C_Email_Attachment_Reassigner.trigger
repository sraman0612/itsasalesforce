//When an EmailMessage record is created, re-parent any attachments tied to that record to that record's Parent Case record

trigger C_Email_Attachment_Reassigner on Attachment (before insert) {
    if (AttchmntRecursiveTriggerHandler.isFirstTime){
        AttchmntRecursiveTriggerHandler.isFirstTime = false;
        
        List<Id> parentIds = new List<Id> {};
        for (Attachment a :trigger.new) {
            if (a.ParentId !=null && a.ParentId.getSObjectType() == EmailMessage.sObjectType) {
                parentIds.add(a.ParentId);
            }
        }
    
        Map<Id, EmailMessage> messageMap = new Map<Id, EmailMessage> ([SELECT Id, ParentId, Incoming FROM EmailMessage where Id = :parentIds]);
        List <Attachment> clonedAttachments = new List <Attachment> ();
        
        for( Attachment a : trigger.new ) {
            EmailMessage msg = messageMap.get(a.ParentId);
            
            
            
            a.Description = 'Reparented From Case:  ' + a.parentid + ' On Date: ' + datetime.now();
              
            if (msg != null && msg.incoming == TRUE) {
                
                // If the message has a parent, and that parent is a case object...
                if (!String.IsEmpty(msg.ParentId) && msg.ParentId.GetSObjectType() == Case.GetSObjectType()) {
                
                    Id trueParentId;
                    
                    // Get the email's parent case.
                    Case emailParentCase = [Select Id, ParentId From Case Where Id = :msg.ParentId Limit 1];
                    
                    // Does this case also have a parent, and is thus an Action Item?
                    if (!String.IsEmpty(emailParentCase.ParentId) && emailParentCase.ParentId.GetSObjectType() == Case.GetSObjectType()) {
                        trueParentId = emailParentCase.ParentId;
                        /*
                        // Find the parent account for the case.
                        Account parentAccountForCase = [Select Id From Account Where Id = :emailParentCase.ParentId Limit 1];
                        if (parentAccountForCase != null) {
                            // Create a clone of the current attachment to assign to the account.
                            Attachment aClone = new Attachment(name = a.name, body = a.body, parentid = parentAccountForCase.id);
                            clonedAttachments.add(aClone);
                        }
                        */
                    }
                    else {
                        trueParentId = emailParentCase.Id;
                    }
                    
                    // Create a clone of the current attachment to remain on this object.
                    Attachment aClone = new Attachment(name = a.name, body = a.body, parentid = a.parentid);
                    clonedAttachments.add(aClone);
                    
                    
                    
                    a.ParentId = trueParentId;
                }   
            }
        }
        
        
        upsert clonedAttachments;
    }
}