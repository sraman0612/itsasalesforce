/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 19/10/2021
* @description: Trigger on Work Order Line Item
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number               Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001    SIF-69     19/10/2021      Trigger on Work Order Line Item
============================================================================================================*/
trigger SFS_WOLITrigger on WorkOrderLineItem (before Update, before Insert,after Update,after Insert,before delete) {
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('WorkOrderLineItem');
    if(skipTrigger!=True){
        
        If(Trigger.isupdate && Trigger.isBefore){
            SFS_WOLITriggerHandler.checkPurchaseExpense(trigger.oldMap,trigger.new);
            for(WorkOrderLineItem woli:Trigger.New){
                if(woli.Status=='Completed' || woli.Status=='Closed'){
                    SFS_WOLITriggerHandler.CheckStatus(Trigger.New,trigger.newMap);
                    Id ProfileId = userInfo.getProfileId();
                    string ProfileName=[SELECT Id, Name From Profile where ID=:ProfileId].Name;
                    WorkOrderLineItem oldRecord=Trigger.OldMap.get(woli.Id);
                    if(oldRecord.Status==woli.status && ProfileName=='SFS Service Technician' && woli.SFS_Total_Amount__c==oldRecord.SFS_Total_Amount__c && woli.SFS_PO_Number__c==oldRecord.SFS_PO_Number__c && woli.Additional_Options_Completed_Notes__c == Null){
                        woli.addError('Cannot edit Completed WOLI');
                    }
                }
                if(woli.Status == 'Canceled' ){
                    SFS_WOLITriggerHandler.checkWOLICancellation(Trigger.new, Trigger.oldMap);
                }
            }
            SFS_WOLITriggerHandler.checkWOEntitlementMatches(Trigger.New);
        }
        else if(Trigger.isInsert && Trigger.isAfter ){
            
            //SFS_WOLITriggerHandler.createSkillReqOnCreation(Trigger.New);
           SFS_WOLITriggerHandler.createSkillReq(Trigger.New,null);
            SFS_WOLITriggerHandler.createPartsRequired(Trigger.New);
            SFS_WOLITriggerHandler.createParts(Trigger.New);
        }
        else if(Trigger.isupdate && Trigger.isAfter ){
            
            for(WorkOrderLineItem woli:Trigger.New){               
                String MPId=woli.MaintenancePlanId;
                if(String.isEmpty(MPId)){
                    SFS_WOLITriggerHandler.deleteOldLocSkill(Trigger.New,trigger.oldMap);
                    SFS_WOLITriggerHandler.createSkillReq(Trigger.New,trigger.oldMap);
                }
            }
            
        }
        else if(Trigger.isInsert && Trigger.isBefore){
            SFS_WOLITriggerHandler.checkWOEntitlementMatches(Trigger.New);
        }

        if(trigger.isDelete){
                Id ProfileId = userInfo.getProfileId();
                //string ProfileName=[SELECT Id, Name From Profile where ID=:ProfileId].Name;
                User usr = [select Id,Profile.Name,name from User where Id = :userInfo.getUserId()];
                for(WorkOrderLineItem woli:Trigger.old){                    
                    if(usr.Profile.Name!=null){
                        if((usr.Profile.Name.toLowerCase()=='sfs service') || (usr.Name.toLowerCase()=='automated process')){
                            woli.addError('You do not have access to delete please contact admin');
                        }
                    }
                }
                /*for(WorkOrderLineItem woli:Trigger.old){
                    if(ProfileName=='SFS Service'){
                        woli.addError('You do not have access to delete please contact admin');
                    }
                
            }*/
            
        }
        
    }
}