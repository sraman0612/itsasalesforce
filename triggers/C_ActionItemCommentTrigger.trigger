trigger C_ActionItemCommentTrigger on Action_Item_Comment__c  (after insert) 
{
    list<string> caseIDs = new List<string>();
    for(Action_Item_Comment__c cc : trigger.new)
    {
        caseIDs.add(cc.Case__c);
    }
    
    Map<string,case> cases = new Map<string,case>([select id, createdbyid,recordTypeId,OwnerId,IC_Acton_Item_Owner__c   from case where id IN :caseIDs]);
    
    ID InternalCaseRT = [Select id, developername from RecordType where Developername = 'Internal_Case'].Id;
    
    List<Case> cList = new List<Case>();
    for(Action_Item_Comment__c cc:trigger.new)
    {
        if(cases.containsKey(cc.Case__c) && cc.CreatedById == cases.get(cc.Case__c).createdbyid)
        {
            Case c = new Case();
            c.id = cases.get(cc.Case__c).id;
            
            If (cases.get(cc.Case__c).recordTypeId == InternalCaseRT){
             System.debug('User Info *** '+UserInfo.getUserId());  
             System.debug('IC_Acton_Item_Owner__c  *** '+cases.get(cc.Case__c).IC_Acton_Item_Owner__c); 
             System.debug('Owner ID  *** '+cases.get(cc.Case__c).OwnerId);
             
            
                if(cases.get(cc.Case__c).IC_Acton_Item_Owner__c == UserInfo.getUserId()){                 
                     c.Status = 'More Info Required';                              
                }                
                if(cases.get(cc.Case__c).OwnerId == UserInfo.getUserId()){ 
                     c.Status = 'Pending Approval';                                      
                }            
            }    
            cList.add(c);            
        }
    }
    if(!cList.IsEmpty()){
        update cList;
    }
}