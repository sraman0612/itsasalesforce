/*=========================================================================================================
* @author Srikanth P, Capgemini
* @date 24/11/2021
* @description: SFS_InvoiceTrigger Apex Trigger.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

============================================================================================================*/

trigger SFS_InvoiceTrigger on Invoice__c (before insert,before update,after update,before delete) {
    
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Invoice__c');
    if(skipTrigger!=True){
        if(trigger.isBefore){
            If(Trigger.isUpdate){
                Id profileIds = userInfo.getProfileId();
                String profilenames=[SELECT Id, Name from Profile WHERE Id=:profileIds].Name;
                for(Invoice__c inv:Trigger.New){
                    Invoice__c oldInvoice=Trigger.OldMap.get(inv.Id);
                    System.debug('oldInvoice'+oldInvoice);
                    if(oldInvoice.SFS_Status__c=='Submitted' && inv.SFS_Status__c != 'Pending' && profilenames=='SFS Service' && inv.Invoice_Date__c == oldInvoice.Invoice_Date__c && inv.SFS_Integration_Response__c == oldInvoice.SFS_Integration_Response__c && inv.SFS_Integration_Resubmit__c == oldInvoice.SFS_Integration_Resubmit__c  && inv.SFS_Integration_Status__c  == oldInvoice.SFS_Integration_Status__c && inv.SFS_Credit_Amount_Track__c == oldInvoice.SFS_Credit_Amount_Track__c && inv.SFS_Amount_After_Credit__c == oldInvoice.SFS_Amount_After_Credit__c){
                          inv.addError('You are not allowed to edit submitted Invoice.');
                           
                       }
                    List<CAP_IR_Charge__c> chargeToUpdate = new List<CAP_IR_Charge__c>();
                    Id agrInvoiceId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('SFS_Agreement_Invoice').getRecordTypeId();
                    Id careInvoiceId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('SFS_CARE_Invoice').getRecordTypeId();
                    if((inv.RecordTypeId == agrInvoiceId || inv.RecordTypeId == careInvoiceId)&& oldInvoice.SFS_Status__c=='Created'&& inv.SFS_Status__c =='Submitted' ){
                        chargeToUpdate = [Select id from CAP_IR_Charge__c where SFS_Invoice__c=:inv.id];
                        if(chargeToUpdate.size()==0 ){
                            chargeToUpdate = [Select id from CAP_IR_Charge__c where CAP_IR_Invoice__c=:inv.id];
                        }                        
                        if(chargeToUpdate.size()==0 ){
                            inv.addError('Invoices can’t be submitted if there are no charges.');
                        }
                    }
                }
                
            }
            if(trigger.isInsert){
                SFS_InvoiceTriggerHandler.invoicecurrency(Trigger.New);
                //List<Invoice__c> Invoice = new List<Invoice__c>(Trigger.new);
                //Invoice[0].CurrencyIsoCode='TWD';
                //system.debug('CurrencyIsoCode'+Invoice[0].CurrencyIsoCode);
                //SFS_InvoiceTriggerHandler.checkForFullCredit(Trigger.New);
            }
            //Restrict invoice deletion when in submitted state for care team and service coordinator
            If(Trigger.isDelete) 
            {   Id profileId = userInfo.getProfileId();
             String profilename=[SELECT Id, Name from Profile WHERE Id=:profileId].Name;
             for(Invoice__c inv : Trigger.old){
                 If(inv.SFS_Status__c=='Submitted' && (profilename=='SFS Service'|| profilename=='SFS Care Team')){
                     inv.addError('You are not allowed to delete submitted Invoice.');
                 }
             }
            }
        } 
        If(Trigger.isAfter){
            If(Trigger.isUpdate){
                SFS_InvoiceTriggerHandler.assignTaskOnInvoiceSubmission(Trigger.New,Trigger.oldMap);
              
            }
        }
    }
}