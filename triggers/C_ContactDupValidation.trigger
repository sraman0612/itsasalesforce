trigger C_ContactDupValidation on Contact (before insert, before update) {
    Map<ID,Schema.RecordTypeInfo> rt_Map = Contact.sObjectType.getDescribe().getRecordTypeInfosById();
    for (Contact c : Trigger.new) {
        if(!String.isEmpty(c.Email) && !rt_map.get(c.recordTypeID).getName().containsIgnoreCase('End User-Distributor') ){   
            c.Email_Address_Copy__c = c.Email;
        }
        if(Trigger.isInsert && !rt_map.get(c.recordTypeID).getName().containsIgnoreCase('End User-Distributor') ){
            c.Email = c.Email_Address_Copy__c;
        }
    }

}