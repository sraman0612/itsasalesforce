trigger C_AccountAfterTrigger on Account (after insert, after update) {
    List<string> aID = new List<string>();
    Boolean addressChangedFlag = false;
    
    if(Trigger.isInsert){
        for (Account account : trigger.new) {
            
            //when new account created if parent record id is populated add accoutid to list for batch
            if(!String.isBlank(account.Parent_Record_ID__c))    
                aID.add(account.id);
        }
        if(aID.size()>0){
            //Send accountID to batch class to delete AND ADD asset share rights
            C_DeleteAssetShareBatch dBatch = new C_DeleteAssetShareBatch(null,aID);//initialize batch class(passid)
            Database.executeBatch(dBatch);//execute batch
        }
        
        // if address is null or has been changed, geocode it
        codeIT(Trigger.new);
    }
    
    
    
    if (Trigger.isUpdate) {
        
        List<string> aID2 = new List<string>();
        List<string> ofpID = new List<string>();
        List<string> nfpID = new List<string>();
        booleaN fpChange = false;
        for (Account account : trigger.new) {
            Account oldAccount = Trigger.oldMap.get(account.Id);
            
            //Setting up sharing rules when account's parent is updated
            if((account.Parent_Record_ID__c != oldAccount.Parent_Record_ID__c) && (!String.isBlank(account.Parent_Record_ID__c)))
            {
                //when account is updated if parent record id is populated add accoutid to list for batch
                if(!String.isBlank(account.Parent_Record_ID__c))    
                    aID.add(account.id);
            }
            
            //check to see if floor plan account id whas changed if so add to list
            if((account.Floor_Plan_Account_ID__c != oldAccount.Floor_Plan_Account_ID__c))
            {
                
                fpChange = true;
                aID2.add(string.valueOF(account.id).substring(0,15));
                
                //grab old floor plan accounts to delete share records
                if(oldaccount.Floor_Plan_Account_ID__c != null)
                    ofpID.add(oldaccount.Floor_Plan_Account_ID__c);
                //grab new floor plan accounts to add share records
                if(account.Floor_Plan_Account_ID__c != null)    
                    nfpID.add(account.Floor_Plan_Account_ID__c);
                
                system.debug(ofpID+ ' '+oldaccount.Floor_Plan_Account_ID__c + 'New: '+ nfpID +' '+account.Floor_Plan_Account_ID__c);
            }
            
            
            //check if Billing Address has been updated
            if ((account.BillingStreet != oldAccount.BillingStreet) || (account.BillingCity != oldAccount.BillingCity) ||
                (account.BillingCountry != oldAccount.BillingCountry) || (account.BillingPostalCode != oldAccount.BillingPostalCode)) {
                    
                    addressChangedFlag = true;
                    
                    System.debug(LoggingLevel.DEBUG, '***Address changed for - ' + oldAccount.Name);
                }
        }
        
        //Send accountID to batch class to delete AND ADD asset share rights
        if(aID.size()>0){
            C_DeleteAssetShareBatch dBatch = new C_DeleteAssetShareBatch(null,aID);//initialize batch class(passid)
            Database.executeBatch(dBatch);//execute batch
        }
        
        List<Asset> assetsRef = new List<Asset>();
        system.debug(ofpID);
        if(ofpID.size()>0) 
        {
            C_DeleteFPAssetShareBatch dBatch = new C_DeleteFPAssetShareBatch(null,ofpID,nfpID,aID2);//initialize batch class(passid)
            Database.executeBatch(dBatch);//execute batch
        }
        else if(nfpID.size()>0)
        {
            C_CreateFPAssetShareBatch cBatch = new C_CreateFPAssetShareBatch(null, nfpID, aID2);
            Database.executeBatch(cBatch); 
        }
        
        // if address is null or has been changed, geocode it
        codeIT(Trigger.new);
    }
    
    public void codeIT(List<account> aList){
        // if address is null or has been changed, geocode it
        for(Account account: aList){
            if ((account.Location__Latitude__s == null) || (addressChangedFlag == true)) {
                System.debug(LoggingLevel.DEBUG, '***Geocoding Account - ' + account.Name);
                AccountGeocodeAddress.DoAddressGeocode(account.id);
            }
        }
    }
    
}