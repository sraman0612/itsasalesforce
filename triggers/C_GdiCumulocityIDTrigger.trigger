trigger C_GdiCumulocityIDTrigger on Asset (before update) {
    //*********************************************Start Update Cumulocity ID**********************************************************   
    if(trigger.isUpdate){
        list<id> AssId = new list<id>();
        
        try{
                for( Id AssetID : Trigger.newMap.keySet() )
                {
                    System.debug('same :'+ Trigger.oldMap.get(AssetID).IMEI__c +'-'+Trigger.newMap.get( AssetID ).IMEI__c );
                    if( Trigger.oldMap.get(AssetID).IMEI__c != Trigger.newMap.get( AssetID ).IMEI__c )
                    {
                        AssId.add(Trigger.newMap.get( AssetID ).id);
                        
                    }
                }
                if(AssId.size()>0){ 
                   // C_GDIiconnInventoryTrigger.iConnInventorySync(AssId);
                   C_GDIiConnInventoryTriggerSyncBatch btch = new C_GDIiConnInventoryTriggerSyncBatch(AssId);
                   id batchprocessid = Database.executebatch(btch,1);
                }

        }
        catch(Exception e){
            System.debug('Error:'+e.getMessage());
        }
    }
    //***************************************************End Update Cumulocity ID*************************************************************************
    
}