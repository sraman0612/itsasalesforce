trigger TRG_ContentDocumentLink on ContentDocumentLink (after insert) {
    List<ContentDocumentLink> claimAttachments = new List<ContentDocumentLink>();

    for (ContentDocumentLink attachment : Trigger.new) {
        if (attachment.LinkedEntityId.getSObjectType() == Warranty_Claim__c.sObjectType || attachment.LinkedEntityId.getSObjectType() == Machine_Startup_Form__c.sObjectType) {
            claimAttachments.add(attachment);
        }
    }

    if (!claimAttachments.isEmpty()) {
        UTIL_ClaimAttachment.handleAttachmentInsert(claimAttachments);
    }
}