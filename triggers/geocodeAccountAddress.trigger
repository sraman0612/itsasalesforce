trigger geocodeAccountAddress on Account (after insert, after update) {  
        
    if(Trigger.isInsert){
        // if address is null or has been changed, geocode it
        codeIT(Trigger.new);
    }
    
    Boolean addressChangedFlag = false;
    if (Trigger.isUpdate) {
        
        for (Account account : trigger.new) {
            Account oldAccount = Trigger.oldMap.get(account.Id);
           
            //check if Billing Address has been updated
            if ((account.BillingStreet != oldAccount.BillingStreet) || (account.BillingCity != oldAccount.BillingCity) ||
                (account.BillingCountry != oldAccount.BillingCountry) || (account.BillingPostalCode != oldAccount.BillingPostalCode)) {
                    
                addressChangedFlag = true;    
                System.debug(LoggingLevel.DEBUG, '***Address changed for - ' + oldAccount.Name);
            }
        }
        
        // if address is null or has been changed, geocode it
        codeIT(Trigger.new);
    }
    
    public void codeIT(List<account> aList){
        // if address is null or has been changed, geocode it
        for(Account account: aList){
            if ((account.Location__Latitude__s == null || addressChangedFlag == true) && (!Test.isRunningTest())) {
                System.debug(LoggingLevel.DEBUG, '***Geocoding Account - ' + account.Name);
                AccountGeocodeAddress.DoAddressGeocode(account.id);
            }
        }
    }
}