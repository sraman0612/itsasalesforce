trigger UpdateMileage on Account_Association__c (after insert) {
    for (Account_Association__c a : Trigger.new) {
        AccountAssociationMileageUpdater.scheduleJob(a);
    }
}