trigger OpportunityOwnerTrigger on Opportunity (after insert,after update) {
    List<Id> oppsToProcess = new List<Id>();
    if (Trigger.isUpdate) {
        for (Opportunity opp : Trigger.new) {
            Opportunity oldOpp = trigger.oldMap.get(opp.Id);
            if (opp.Account_Name__c != null && !opp.Account_Name__c.toUpperCase().contains('GARDNER DENVER') && (opp.AccountId != oldOpp.AccountId || opp.Sales_Channel__c != oldOpp.Sales_Channel__c)) {
                oppsToProcess.add(opp.Id);
            }
        }
    }
    else if (Trigger.isInsert) {
        for (Opportunity opp : Trigger.new) {
            if (opp.Account_Name__c != null && !opp.Account_Name__c.toUpperCase().contains('GARDNER DENVER') && null != opp.Sales_Channel__c) {
                oppsToProcess.add(opp.Id);
            }
        }
    }

    if (!oppsToProcess.isEmpty()) {
        UTIL_Opportunity.updateOwner(oppsToProcess);
    }
}