/**=========================================================================================================
* @author: Bhargavi Nerella, Capgemini
* @date: 04/05/2022
* @description: SFS_ProductRequiredTrigger Apex Trigger.
  Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

=============================================================================================================*/
trigger SFS_ProductRequiredTrigger on ProductRequired (after insert,before insert,after update) {
	Boolean skipTrigger=CGCommonUtility.sKipTrigger('ProductRequired');
    if(skipTrigger!=True){
        if(Trigger.isInsert){
            if(Trigger.isBefore){
                
            }
            if(Trigger.isAfter){
                SFS_ProductRequiredTriggerHandler.deleteRecords(Trigger.New);
                SFS_ProductRequiredTriggerHandler.createAssetRecords(Trigger.New);
            }
        }
        if(Trigger.isUpdate){
            if(Trigger.isAfter){
                SFS_ProductRequiredTriggerHandler.updateAssetRecords(Trigger.New);
            }
        }
    }
}