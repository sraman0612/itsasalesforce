trigger SFS_ServiceAppointmentTrigger on ServiceAppointment (before update,after update) {
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('ServiceAppointment');
    if(skipTrigger!=True){
        Boolean Sapp=false;
        if(trigger.isBefore){
            if(trigger.isUpdate){
                Id profileIds = userInfo.getProfileId();
                User usr = [select Id,Profile.Name,name from User where Id = :userInfo.getUserId()];
                //String profilenames=[SELECT Id, Name from Profile WHERE Id=:profileIds].Name;
                List<AssignedResource> arList = new List<AssignedResource>();
                for(ServiceAppointment sa: Trigger.new){
                    ServiceAppointment oldSA=Trigger.OldMap.get(sa.Id);
                    if(sa.Status == 'In Progress'){
                        Sapp=true;
                        SFS_ServiceAppointmentHandler.checkOutsideOperatingHours(trigger.new);
                        system.debug('SAUpdate'+Sapp);
                        
                    }
                    if(Sapp==false){
                        system.debug('SAUpdate1'+oldSA);
                        system.debug('SAUpdate1'+sa);
                        if(oldSA.Status == sa.Status && oldSA.SchedEndTime == sa.SchedEndTime && oldSA.ActualEndTime == sa.ActualEndTime && oldSA.ActualDuration == sa.ActualDuration && oldSA.SFS_Labor_End__c == sa.SFS_Labor_End__c && oldSA.OwnerId == sa.OwnerId && oldSA.FSL__Schedule_Mode__c==sa.FSL__Schedule_Mode__c){
                            if(oldSA.Status == 'Completed' && usr.Profile.Name=='SFS Service Technician'){
                                
                                sa.addError('Cannot Edit Completed Service Appointment ');
                                Sapp=true;
                            }
                        }
                    }
                }
                //SFS_ServiceAppointmentHandler.checkOutsideOperatingHours(trigger.new);
                //SFS_ServiceAppointmentHandler.checkAuthorize(trigger.oldMap,trigger.new);
            }
        }
        if(trigger.isAfter){
            if(trigger.isUpdate){
                List<Id> ParentIds=new List<Id>();
                List<Id> SaIds = new List<Id>();
                for(ServiceAppointment sa: Trigger.new){
                    ServiceAppointment oldRecord=Trigger.OldMap.get(sa.Id);
                    if(sa.Status=='Completed'&& oldRecord.Status!=sa.status)
                        ParentIds.add(sa.ParentRecordId);
                        SaIds.add(sa.Id);
                }
                if(ParentIds.size()>0)
                    System.enqueueJob(new SFS_ServiceAppointment_Queueable(ParentIds,SaIds));
                    //SFS_ServiceAppointmentHandler.updateSAonCompletion(ParentIds);
            }
        }       
        
    }
}