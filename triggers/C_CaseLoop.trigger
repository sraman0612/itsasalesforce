trigger C_CaseLoop on Case(before insert) {

    Map<Id, RecordType> recordTypes = new Map<Id,RecordType>([SELECT Id,Name FROM RecordType WHERE SobjectType='Case' and DeveloperName IN ('Customer_Care', 'Managed_Care')]);
    Map<Id, RecordType> recordTypesAI = new Map<Id,RecordType>([SELECT Id,Name FROM RecordType WHERE SobjectType='Case' and DeveloperName IN ('Action_Item', 'Internal_Case')]);
    
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            Datetime timeFilter = System.now().addMinutes(-3);
        
            for(Case c : trigger.new){
                
                if(recordTypes.keySet().contains(c.RecordTypeId)){
                    
                    if(String.isNotEmpty(c.SuppliedEmail)){
                        
                        if(String.isNotEmpty(c.Description)) c.Body_Hash__c = c.Description.hashCode();
                        if(String.isNotEmpty(c.Subject)) c.Subject_Hash__c = c.Subject.hashCode();
                        
                        List<Case> existingCases = [SELECT Id, ContactId FROM Case WHERE Body_Hash__c =: c.Body_Hash__c AND Subject_Hash__c =: c.Subject_Hash__c AND CreatedDate >= :timeFilter AND SuppliedEmail =: c.SuppliedEmail];
                        System.debug('existingCases: ' + existingCases.size()); 
                        
                        if(existingCases.size() > 2){
                            //c.addError('Duplicate Case Loop Detected');
                            contact loopContact;
                            if(existingCases[0].ContactId != null){               
                                Contact con = [SELECT ID, Do_Not_Send_Auto_Emails_for_Cases__c FROM Contact where Id =: existingCases[0].ContactId];
                                loopContact = con;
                                con.Do_Not_Send_Auto_Emails_for_Cases__c = true;
                                update con;               
                                C_ErrorReporter.sendInfoReport('Email Loop Detected', c.Subject, 'A loop was detected upon processing this email.<br/> It was sent by <b>'+c.SuppliedEmail+'</b>.<br>Contact Record: https://na16.salesforce.com/' + con.Id, c.Description);
                            }
                            else{
                                C_ErrorReporter.sendInfoReport('Email Loop Detected', c.Subject, 'A loop was detected upon processing this email.<br/> It was sent by <b>'+c.SuppliedEmail+'</b>.<br> No Contact is associated with the Case.', c.Description);
                            }
                        }
                    }
                }
            
            }
                
            //update Case status for Action Items
            for(Case c : trigger.new){
                if(recordTypesAI.keySet().contains(c.RecordTypeId) && c.parentId != NULL){
                    C_Utils.updateParentStatus(c.parentId);       
                }
            }
        }
    }     
}