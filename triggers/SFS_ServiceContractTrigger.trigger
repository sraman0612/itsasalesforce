/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 22/09/2021
* @description: SFS_ServiceContractTrigger Apex Trigger.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001   SIF-34    22/09/2021  Apex Trigger to call SFS_ServiceContractHandler Apex class
Srikanth P              M-002   SIF-24    30/09/2021  Calls callbatchClassToCreateInvoices method from the SFS_ServiceContractHandler Apex class
Bhargavi Nerella        M-003   SIF-59    21/10/2021  Calls AssignOwner method in SFS_ServiceContractHandler
Sucharitha Suragala     M-004   SIF-65    09/11/2021  Method(checkServiceAgreement) to check When Closing ServiceAgreement, WorkOrder Status and WOLI Status must be completed(Validation) 
Ryan Reddish            M-005   SIF-369   29/11/2021  Calls ServiceAgreementOutboundHandler class to send new Service Agreements to middleware
====================================================================================================================================================*/
trigger SFS_ServiceContractTrigger on ServiceContract (after insert,before insert, before update, after update) {
   
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('ServiceContract');
    if(skipTrigger!=True){
        if(trigger.isBefore){
            if(trigger.isInsert ){
                SFS_ServiceContractHandler.callPolygonUtils(null,trigger.new);//Method to get ServiceTerritory Id - SIF 34
                SFS_ServiceContractHandler.assignOwner(trigger.new);
                    
            }
            else if(trigger.isUpdate){
              	Boolean isCPQUser = FeatureManagement.checkPermission('SFS_CPQ_User');
                Boolean isDataMigrUser = FeatureManagement.checkPermission('Data_Migration_User');
                for(ServiceContract sc:Trigger.New){
               		ServiceContract oldRecord=Trigger.OldMap.get(sc.Id);  
                	if(oldRecord.SFS_Status__c!=sc.SFS_Status__c && sc.SFS_Status__c=='APPROVED' && oldRecord.SFS_Status__c=='Pending' && isDataMigrUser==FALSE && isCPQUser==FALSE){
                        SFS_ServiceContractHandler.checkProductCode(Trigger.New,trigger.newMap);
                	}
                }
                
                SFS_ServiceContractHandler.callPolygonUtils(trigger.oldMap,trigger.new);//Method to get ServiceTerritory Id - SIF 34              
            }
        }  
        if(trigger.isAfter){            
            if(trigger.isUpdate){
                SFS_ServiceContractHandler.callbatchClassToCreateInvoices(trigger.new,trigger.oldMap); 
                SFS_ServiceContractHandler.updateChargeType(trigger.oldMap,trigger.new);            
            }
        }   
        
    }    
}