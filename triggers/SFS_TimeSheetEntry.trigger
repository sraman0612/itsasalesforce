trigger SFS_TimeSheetEntry on TimeSheetEntry (before insert, before update) {
    for(TimeSheetEntry tse : Trigger.New){
        tse.LocationTimeZone = UserInfo.getTimeZone().toString();
        
        if(tse.StartTime != null){
            tse.Local_Punch_Date__c = tse.StartTime.format('MM/dd/YYYY', UserInfo.getTimeZone().toString());
            tse.Local_Punch_Time__c = tse.StartTime.format('HH:mm', UserInfo.getTimeZone().toString());
            tse.Local_Timezone__c = UserInfo.getTimeZone().toString();          
        }

        if(tse.EndTime != null){
            tse.Local_Punchout_Time__c =  tse.EndTime.format('HH:mm', UserInfo.getTimeZone().toString());
        }
    }
}