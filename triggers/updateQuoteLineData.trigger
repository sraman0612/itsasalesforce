trigger updateQuoteLineData on SBQQ__QuoteLine__c (after insert, after update) {
    
    if (trigger.isAfter && !System.isFuture()) {
        for (SBQQ__QuoteLine__c curQL : trigger.new) {
            if (trigger.isInsert) {
                if (curQL.SAP_Configuration__c != null) {
                    // Process the newly inserted Quote Line
                    UTIL_QuoteLineProdCharacteristicValues.updateQuoteLine(curQL.Id);              
                }
            } else if (trigger.isUpdate) {
                SBQQ__QuoteLine__c oldQL = trigger.oldMap.get(curQL.Id);
                
                if (oldQL.SAP_Configuration__c != curQL.SAP_Configuration__c) {
                    // Process the recently updated Quote Line
                    UTIL_QuoteLineProdCharacteristicValues.updateQuoteLine(curQL.Id);
                }
            }
        }
    }
}