trigger C_Service_History_Trigger on Service_History__c (before insert, after insert) {
    
    
    Asset sn;
    
    if(trigger.isBefore)
    {  
        for(Service_History__c sh : Trigger.new)
        {   
            sn = [SELECT Id, Machine_Version_Parts_List__c,Last_Logged_Service_Visit__c FROM Asset WHERE Id = :sh.Serial_Number__c];
            
            
            if(!string.isBlank(sh.Technician_Email__c))
            {
                List<Contact> techContact = new List<Contact>();
                techContact = [Select Id, AccountId from Contact where Email = :sh.Technician_Email__c];
                
                if(techContact.size()>0){
                    sh.Account__c = techContact[0].AccountId;
                }
                
            }
            
        }
    }
    
    if(trigger.isAfter)
    {
        List<Service_History__c> shUpdate = new List<Service_History__c>();
        List<Service_History__Share> jobShrs  = new List<Service_History__Share>();
        List<Asset> serialsToUpdate = new List<Asset>();
        Service_History__c currSH;
        
        for(Service_History__c sh : Trigger.new) {
            
            Asset sn = [SELECT Id, Machine_Version_Parts_List__c,Last_Logged_Service_Visit__c FROM Asset WHERE Id = :sh.Serial_Number__c];
            if (sh.Date_of_Service__c >=sn.Last_Logged_Service_Visit__c || sn.Last_Logged_Service_Visit__c==null)
            {
                sn.Last_Logged_Service_Visit__c = sh.Date_of_Service__c;
                serialsToUpdate.add(sn);
                
                if (sn.Machine_Version_Parts_List__c != null) {
                    if (!test.isrunningtest()) {
                        C_Warranty_Checker cwc = new C_Warranty_Checker(sh.Id);
                    }
                }
            }
            
            if(!string.isBlank(sh.Serial_Number__c)) 
            {
                if(SH.Date_of_Service__c  >= sn.Last_Logged_Service_Visit__c)
                {
                    //SOQL current service history after insert and update
                    currSH = [Select id,Serial_Number__r.Parent_AccountID__c,Serial_Number__r.Current_Servicer_ID__c,Serial_Number__r.Current_Servicer__r.parentID,Oil_Filter_Compliance__c,Hours_Lubricant__c,Compliant_Oil_Filter__c,Inventory_Oil_Filter__c,Compliance_Exception_Oil_Filter__c,Hours_Oil_Filter__c,Oil_Filter_Next_Expected_Date__c,Compliant_Lubricant__c,Inventory_Lubricant__c,Compliance_Exception_Lubricant__c,Lubricant_Next_Expected_Date__c,Separator_Compliance__c,Compliant_Separator__c,Inventory_Separator__c,Hours_Separator__c, Compliance_Exception_Separator__c,Separator_Next_Expected_Date__c,Air_Filter_Compliance__c,Compliant_Air_Filter__c,Inventory_Air_Filter__c,
                              Hours_Air_Filter__c,Compliance_Exception_Air_Filter__c,Air_Filter_Next_Expected_Date__c,Control_Box_Filter_Compliance__c,Compliant_Control_Box_Filter__c,Inventory_Control_Box_Filter__c,Hours_Control_Box_Filter__c,Compliance_Exception_Control_Box__c,
                              Control_Box_Filter_Next_Expected_Date__c,Cabinet_Filter_Compliance__c,Compliant_Cabinet_Filter__c,Inventory_Cabinet_Filter__c,Compliance_Exception_Cabinet_Filter__c,Hours_Cabinet_Filter__c,Cabinet_Filter_Next_Expected_Date__c,
                              Warranty_Compliance_Level__c,Last_Logged_Service_Visit__c,Compliant_Time__c,Expected_Next_Service_Date__c,X12_Mo_Visit__c,Lubricant_Compliance__c from Service_History__c where id = :sh.id];
                    
                    //SOQL old asset warranty compliance information to place on service history BEFORE warr
                    Asset snapshot = [Select id,Oil_Filter_Compliance__c,Hours_Lubricant__c,Compliant_Oil_Filter__c,Inventory_Oil_Filter__c,Compliance_Exception_Oil_Filter__c,Hours_Oil_Filter__c,Oil_Filter_Next_Expected_Date__c,Compliant_Lubricant__c,Inventory_Lubricant__c,Compliance_Exception_Lubricant__c,Lubricant_Next_Expected_Date__c,Separator_Compliance__c,Compliant_Separator__c,Inventory_Separator__c,Hours_Separator__c, Compliance_Exception_Separator__c,Separator_Next_Expected_Date__c,Air_Filter_Compliance__c,Compliant_Air_Filter__c,Inventory_Air_Filter__c,
                                      Hours_Air_Filter__c,Compliance_Exception_Air_Filter__c,Air_Filter_Next_Expected_Date__c,Control_Box_Filter_Compliance__c,Compliant_Control_Box_Filter__c,Inventory_Control_Box_Filter__c,Hours_Control_Box_Filter__c,Compliance_Exception_Control_Box__c,
                                      Control_Box_Filter_Next_Expected_Date__c,Cabinet_Filter_Compliance__c,Compliant_Cabinet_Filter__c,Inventory_Cabinet_Filter__c,Compliance_Exception_Cabinet_Filter__c,Hours_Cabinet_Filter__c,Cabinet_Filter_Next_Expected_Date__c,
                                      Warranty_Compliance_Level__c,Last_Logged_Service_Visit__c,Compliant_Time__c,Expected_Next_Service_Date__c,X12_Mo_Visit__c,Lubricant_Compliance__c from Asset where id = :sh.Serial_Number__c];
                    
                    system.debug(sh.Serial_Number__c  + ' ' + snapshot);
                    
                    //assign old asset warranty compliance information to service history record before Warranty Checker Logic
                    currSH.Oil_Filter_Compliance__c = snapshot.Oil_Filter_Compliance__c;
                    currSH.Compliant_Oil_Filter__c = snapshot.Compliant_Oil_Filter__c;
                    currSH.Inventory_Oil_Filter__c = snapshot.Inventory_Oil_Filter__c;
                    currSH.Compliance_Exception_Oil_Filter__c= snapshot.Compliance_Exception_Oil_Filter__c;
                    currSH.Hours_Oil_Filter__c = snapshot.Hours_Oil_Filter__c;
                    currSH.Oil_Filter_Next_Expected_Date__c= snapshot.Oil_Filter_Next_Expected_Date__c;
                    
                    currSH.Lubricant_Compliance__c = snapshot.Lubricant_Compliance__c;
                    currSH.Compliant_Lubricant__c = snapshot.Compliant_Lubricant__c;
                    currSH.Inventory_Lubricant__c = snapshot.Inventory_Lubricant__c;
                    currSH.Hours_Lubricant__c = snapshot.Hours_Lubricant__c;
                    currSH.Compliance_Exception_Lubricant__c= snapshot.Compliance_Exception_Lubricant__c;
                    currSH.Lubricant_Next_Expected_Date__c  = snapshot.Lubricant_Next_Expected_Date__c;
                    
                    currSH.Separator_Compliance__c = snapshot.Separator_Compliance__c;                    
                    currSH.Compliant_Separator__c = snapshot.Compliant_Separator__c;
                    currSH.Inventory_Separator__c = snapshot.Inventory_Separator__c;
                    currSH.Hours_Separator__c = snapshot.Hours_Separator__c;
                    currSH.Compliance_Exception_Separator__c= snapshot.Compliance_Exception_Separator__c;
                    currSH.Separator_Next_Expected_Date__c = snapshot.Separator_Next_Expected_Date__c;
                    
                    currSH.Air_Filter_Compliance__c = snapshot.Air_Filter_Compliance__c;
                    currSH.Compliant_Air_Filter__c = snapshot.Compliant_Air_Filter__c;
                    currSH.Inventory_Air_Filter__c = snapshot.Inventory_Air_Filter__c;
                    currSH.Hours_Air_Filter__c = snapshot.Hours_Air_Filter__c;
                    currSH.Compliance_Exception_Air_Filter__c = snapshot.Compliance_Exception_Air_Filter__c;
                    currSH.Air_Filter_Next_Expected_Date__c = snapshot.Air_Filter_Next_Expected_Date__c;
                    
                    currSH.Control_Box_Filter_Compliance__c = snapshot.Control_Box_Filter_Compliance__c;
                    currSH.Compliant_Control_Box_Filter__c = snapshot.Compliant_Control_Box_Filter__c;
                    currSH.Inventory_Control_Box_Filter__c = snapshot.Inventory_Control_Box_Filter__c;
                    currSH.Hours_Control_Box_Filter__c = snapshot.Hours_Control_Box_Filter__c;
                    currSH.Compliance_Exception_Control_Box__c = snapshot.Compliance_Exception_Control_Box__c;
                    currSH.Control_Box_Filter_Next_Expected_Date__c = snapshot.Control_Box_Filter_Next_Expected_Date__c;
                    
                    currSH.Cabinet_Filter_Compliance__c = snapshot.Cabinet_Filter_Compliance__c;
                    currSH.Compliant_Cabinet_Filter__c = snapshot.Compliant_Cabinet_Filter__c;
                    currSH.Inventory_Cabinet_Filter__c = snapshot.Inventory_Cabinet_Filter__c;
                    currSH.Compliance_Exception_Cabinet_Filter__c = snapshot.Compliance_Exception_Cabinet_Filter__c;
                    currSH.Hours_Cabinet_Filter__c = snapshot.Hours_Cabinet_Filter__c;
                    currSH.Cabinet_Filter_Next_Expected_Date__c = snapshot.Cabinet_Filter_Next_Expected_Date__c;
                    
                    currSH.Warranty_Compliance_Level__c = snapshot.Warranty_Compliance_Level__c;
                    currSH.Last_Logged_Service_Visit__c = snapshot.Last_Logged_Service_Visit__c;
                    currSH.Compliant_Time__c = snapshot.Compliant_Time__c;
                    currSH.Expected_Next_Service_Date__c = snapshot.Expected_Next_Service_Date__c;
                    currSH.X12_Mo_Visit__c = snapshot.X12_Mo_Visit__c;
                    
                    shUpdate.add(currSH);
                }
            }
            
        }
        
        if(serialsToUpdate.size() > 0){
            update serialsToUpdate;
        }
        
        update shUpdate;
    }
    
    
}