trigger LeadTrigger on Lead (after insert, after update) {

    if (trigger.isAfter && !System.isFuture()) {
    
        String recTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'lead' AND Name = 'Manual (Internal)'].Id;
    
        for (Lead curLead : trigger.new) {
            if (trigger.isInsert && curLead.RecordTypeId != recTypeId) {
                UTIL_EnosixZAPRSearch.updateOwner(curLead.Id);
            } else if (trigger.isUpdate && curLead.RecordTypeId != recTypeId) {
                Lead oldLead = trigger.oldMap.get(curLead.Id);

                if (oldLead.ProductCategory__c != curLead.ProductCategory__c
                        || oldLead.Country != curLead.Country
                        || oldLead.State != curLead.State
                        || oldLead.PostalCode != curLead.PostalCode) {

                    UTIL_EnosixZAPRSearch.updateOwner(curLead.Id);
                }
            }
        }
    }
}