trigger MachineStartupFormTrigger on Machine_Startup_Form__c (after insert, after update) {
    List<Id> toProcess = new List<Id>();
    if (Trigger.isInsert || Trigger.isUpdate) {
        for (Machine_Startup_Form__c startupForm : Trigger.new) {
            if (startupForm.SAP_Status__c == 'READY') {
                toProcess.add(startupForm.Id);
            }
        }
    }

    if (!toProcess.isEmpty()) {
        UTIL_MachineStartupForm.createServiceRequests(toProcess);
    }
}