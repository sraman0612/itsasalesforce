/*=========================================================================================================
* @author : Bhargavi Nerella, Capgemini
* @date : 28/01/2022
* @description: Trigger on Product Relationship
Modification Log:
------------------------------------------------------------------------------------
Developer          Mod Number               Date          Description
------------------------------------------------------------------------------------
Bhargavi Nerella     M-001    SIF-112     28/01/2022      Trigger on Product Relationship
Rahul Chauhan		 M-002	  SIF-4859	  25/07/2023	  Updated the logic for "Related" relation ship
============================================================================================================*/
trigger SFS_ProductRelationshipTrigger on SFS_Product_Relationship__c (after insert,after update) {
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('SFS_Product_Relationship__c');
    if(skipTrigger!=True){
        If(Trigger.isAfter){
            if(Trigger.isInsert){
                List<SFS_Product_Relationship__c> newList= new List<SFS_Product_Relationship__c>();
                Set<Id> productIds=new Set<Id>();
                Map<Id,Boolean> productToSuperseded=new Map<Id,Boolean>();
                for(SFS_Product_Relationship__c record:Trigger.New){
                    if(record.SFS_Product__c!=null){
                        productIds.add(record.SFS_Product__c);
                    }else{
                        Product2 pc = [SELECT Id, Part_Number__c FROM Product2 WHERE Part_Number__c =: record.SFS_Item__c];
                        if(pc != null){
                            record.sfs_product__c = pc.Id;
                            productIds.add(pc.Id);
                        }
                    }
                    System.debug('NULLLLLLL');
                }
                If(productIds.size()>0){
                    List<Product2> productList=[Select id,Name,ProductCode,SFS_Superceded__c from Product2 where Id IN :productIds];
                    for(Product2 prod : productList){
                        productToSuperseded.put(prod.Id,prod.SFS_Superceded__c);
                    }
                }system.debug('36');
                For(SFS_Product_Relationship__c record:Trigger.New){
                    If((record.SFS_Relationship_Type__c=='Supersession' || record.SFS_Relationship_Type__c=='Related') && record.SFS_Product__c!=null){
                           newList.add(record);
                       }
                }
                if(newList.size()>0)
                    System.enqueueJob(new SFS_ProductRelationshipQueueable(newList,null));
            }system.debug('45');
            if(Trigger.isUpdate){
                List<SFS_Product_Relationship__c> newList= new List<SFS_Product_Relationship__c>();
                Set<Id> productIds=new Set<Id>();
                Map<Id,Boolean> productToSuperseded=new Map<Id,Boolean>();
                for(SFS_Product_Relationship__c record:Trigger.New){
                    if(record.SFS_Product__c!=null){
                        productIds.add(record.SFS_Product__c);
                    }system.debug('53');
                }
                If(productIds.size()>0){
                    List<Product2> productList=[Select id,Name,ProductCode,SFS_Superceded__c from Product2 where Id IN :productIds];
                    for(Product2 prod : productList){
                        productToSuperseded.put(prod.Id,prod.SFS_Superceded__c);
                    }
                }
                for(SFS_Product_Relationship__c record:Trigger.New){
                    SFS_Product_Relationship__c oldRecord=Trigger.oldMap.get(record.Id);
                    if((record.SFS_Relationship_Type__c=='Supersession'  || record.SFS_Relationship_Type__c=='Related')
                       && record.SFS_Relationship_Type__c!=oldRecord.SFS_Relationship_Type__c
                       && record.SFS_Product__c!=null){
                           newList.add(record);
                           system.debug('record'+record);
                       }
                    
                }
                if(newList.size()>0)
                    system.debug('72');
                    System.enqueueJob(new SFS_ProductRelationshipQueueable(newList,NULL));
            }
        }
    }
}