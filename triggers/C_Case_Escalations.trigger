trigger C_Case_Escalations on Case (after update) {
    For(Case c : trigger.new){
        if(c.IsEscalated != Trigger.oldMap.get(c.Id).IsEscalated){
            System.debug('Case ' + c.CaseNumber + ' has been escalated');
        }
    }
}