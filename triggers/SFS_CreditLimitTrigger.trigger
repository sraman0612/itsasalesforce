/* Author: Srikanth P, Capgemini
* Date: 08/23/2023
* Description: Apex Trigger for Credit Limit Object
-------------------------------------------------------------------
*/

trigger SFS_CreditLimitTrigger on SFS_Credit_Limit__c (before insert,before update,after insert,after update){
 Boolean skipTrigger=CGCommonUtility.sKipTrigger('Credit_Limit');
    if(skipTrigger!=True){

        if(trigger.isBefore){
            if(trigger.isInsert){
                SFS_CreditLimitHandler.updateRelatedAccount(Trigger.New);
            }
            if(trigger.isUpdate){
                SFS_CreditLimitHandler.updateRelatedAccount(Trigger.New);
            }
        }
        if(trigger.isAfter){             
            if(trigger.isUpdate){
                SFS_CreditLimitHandler.updateRelatedCreditLimit(Trigger.New);
              }
             if(trigger.isInsert){
                SFS_CreditLimitHandler.updateRelatedCreditLimit(Trigger.New);
              }   
        }
    }      
}