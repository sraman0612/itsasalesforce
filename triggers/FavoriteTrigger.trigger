trigger FavoriteTrigger on SBQQ__Favorite__c (after insert) {
    if (trigger.isAfter && trigger.isInsert) {// && !Test.isRunningTest()
        FavoriteShareCreate.createFromFavorite(Trigger.new);
    }
}