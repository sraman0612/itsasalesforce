trigger C_UserAfterTrigger on User (after insert, after update) {

    if(Trigger.isInsert)
    {
        
        List<assetShare> assetShareToDel = new List<assetShare>();
        List<assetShare> assetShareToCreate = new List<assetShare>();
        List<Service_History__Share> ServiceHistoryShareToDel = new List<Service_History__Share>();
        List<Service_History__Share> ServiceHistoryShareToCreate = new List<Service_History__Share>();
        List<id> uID = new List<id>();
        List<id> uID2 = new List<id>();
        
        for(User u : Trigger.new)
        {
            
            if(u.Community_User_Type__c == 'Standard User' || u.Community_User_Type__c == 'Single Account')
            {
                //assetShareToDel = [Select id from assetshare where userorgroupid = :u.id];
                //assetShareToDel.add(delShare);
                
                if(!string.isBlank(u.Partner_Account_ID__c)){
                    for(Asset a : [Select id from Asset where accountID = :u.Partner_Account_ID__c ])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                    for(Asset a : [Select id from Asset where Current_Servicer_ID__c = :u.Partner_Account_ID__c ])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                    for(Service_History__c a : [Select id from Service_History__c where account__c = :u.Partner_Account_ID__c ])
                    {
                        Service_History__Share share = new Service_History__Share();
                        share.AccessLevel='Edit';
                        share.ParentId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        ServiceHistoryShareToCreate.add(share);
                    }
                }
                if(!string.isBlank(u.contact.account.Floor_Plan_Account_ID__c )){
                    for(Asset a : [Select id from Asset where accountID = :u.contact.account.Floor_Plan_Account_ID__c])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                }
                uID.add(u.id);
            }
            else if(u.Community_User_Type__c == 'Super User' || u.Community_User_Type__c == 'Global Account')
            {
                uID2.add(u.id);
                
            }
            
        }
        
        if(uID.size()>0 || uID2.size()>0)
        {
            //Delete previous share records
            assetShareToDel = [Select id from assetshare where userorgroupid IN :uID OR userorgroupid IN :uID2];
            ServiceHistoryShareToDel = [Select id from Service_History__Share where userorgroupid IN :uID OR userorgroupid IN :uID2];
        }
        if(assetShareToDel.size()>0)
            delete assetShareToDel;
        
        if(ServiceHistoryShareToDel.size()>0)
            delete ServiceHistoryShareToDel;
        
        if(assetShareToCreate.size()>0)
            insert assetShareToCreate;
        
        if(ServiceHistoryShareToCreate.size()>0)
            insert ServiceHistoryShareToCreate;
        
        if(uID.size()>0){
            C_AssetSharingCreation.createAssetShareSingleCommUsr(uID);
            C_AssetSharingCreation.createServiceHistoryShareUsr(uID);
        }
        
        if(uID2.size()>0){
            //Send user to batch class to delete AND ADD asset share rights
            //C_createAssetShareGlobalUsrBatch uBatch = new C_createAssetShareGlobalUsrBatch(null,uID2);//initialize batch class(passid)
            //id processedBatch = Database.executeBatch(uBatch,20);//execute batch
            C_AssetSharingCreation.createAssetShareGlobalUsr(uID2);        
            C_AssetSharingCreation.createServiceHistoryShareUsr(uID2);
        }
        
    }
    
    if(Trigger.isUpdate)
    {
        List<assetShare> assetShareToDel = new List<assetShare>();
        List<assetShare> assetShareToCreate = new List<assetShare>();
        List<Service_History__Share> ServiceHistoryShareToDel = new List<Service_History__Share>();
        List<Service_History__Share> ServiceHistoryShareToCreate = new List<Service_History__Share>();
        List<id> uID = new List<id>();
        List<id> uID2 = new List<id>();
        List<id> uID3 = new List<id>();
        
        For(User u : Trigger.new)
        {
            User oldU = Trigger.oldMap.get(u.Id);
            
            system.debug('***** New Value: '+u.Community_User_Type__c + ' ***** Old Value: '+oldU.Community_User_Type__c);
            
            if(u.Community_User_Type__c == 'Standard User' && oldU.Community_User_Type__c == 'Super User' || 
               (u.Community_User_Type__c == 'Single Account' &&oldU.Community_User_Type__c == 'Global Account')||
               ( u.Community_User_Type__c == 'Single Account' && u.Community_User_Type__c!=oldU.Community_User_Type__c) ) 
            {
                //assetShareToDel = [Select id from assetshare where userorgroupid = :u.id];
                uID3.add(u.id);
                /*if(!string.isBlank(u.Partner_Account_ID__c))
                {
                    for(Asset a : [Select id from Asset where accountID = :u.Partner_Account_ID__c ])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                    for(Asset a : [Select id from Asset where Current_Servicer_ID__c = :u.Partner_Account_ID__c ])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                    for(Service_History__c a : [Select id from Service_History__c where account__c = :u.Partner_Account_ID__c ])
                    {
                        Service_History__Share share = new Service_History__Share();
                        share.AccessLevel='Edit';
                        share.ParentId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        ServiceHistoryShareToCreate.add(share);
                    }
                }
                if(!string.isBlank(u.contact.account.Floor_Plan_Account_ID__c )){
                    for(Asset a : [Select id from Asset where accountID = :u.contact.account.Floor_Plan_Account_ID__c])
                    {
                        AssetShare share = new AssetShare();
                        share.assetAccessLevel='Edit';
                        share.AssetId=a.id;
                        share.RowCause='Manual';
                        share.UserOrGroupId=u.id;
                        
                        assetShareToCreate.add(share);
                    }
                }*/
            }
            else if(u.Community_User_Type__c == 'Super User'&& oldU.Community_User_Type__c == 'Standard User' || 
                    (u.Community_User_Type__c == 'Global Account' &&oldU.Community_User_Type__c == 'Single Account')||
                    (u.Community_User_Type__c == 'Super User' && u.Community_User_Type__c!=oldU.Community_User_Type__c)||
                   (u.Community_User_Type__c == 'Global Account' && u.Community_User_Type__c!=oldU.Community_User_Type__c))
            {
                //assetShareToDel = [Select id from assetshare where userorgroupid = :u.id];
                uID2.add(u.id);
            }
            else if( u.Community_User_Type__c == null && oldU.Community_User_Type__c != null)
            {
                //assetShareToDel = [Select id from assetshare where userorgroupid = :u.id];
                uID.add(u.id);
            }
        }
        if(uID.size()>0 || uID2.size()>0 || uID3.size()>0)
        {
            //Delete previous share records
            assetShareToDel = [Select id from assetshare where userorgroupid IN :uID OR userorgroupid IN :uID2 OR userorgroupid IN :uID3];
            ServiceHistoryShareToDel = [Select id from Service_History__Share where userorgroupid IN :uID OR userorgroupid IN :uID2 OR userorgroupid IN :uID3];
        }
        if(assetShareToDel.size()>0)
            delete assetShareToDel;
        
        if(ServiceHistoryShareToDel.size()>0)
            delete ServiceHistoryShareToDel;
        
        /*if(assetShareToCreate.size()>0)
            insert assetShareToCreate;
        
        if(ServiceHistoryShareToCreate.size()>0)
            insert ServiceHistoryShareToCreate;*

        system.debug(assetShareToCreate);
        
        if(uID3.size()>0)
        {
            if(!System.isFuture() && !System.isBatch())
            C_AssetSharingCreation.createAssetShareSingleCommUsr(uID3);
        }
        
        /*if(uID.size()>0){
            C_AssetSharingCreation.createAssetShareUsr(uID);
            C_AssetSharingCreation.createServiceHistoryShareUsr(uID);
        }*/
        
        if(uID2.size()>0){
            if(!System.isFuture() && !System.isBatch()){
                //Send user to batch class to delete AND ADD asset share rights
                //C_createAssetShareGlobalUsrBatch uBatch = new C_createAssetShareGlobalUsrBatch(null,uID2);//initialize batch class(passid)
                //id processedBatch = Database.executeBatch(uBatch,20);//execute batch
                C_AssetSharingCreation.createAssetShareGlobalUsr(uID2); 
            }
            if(!System.isFuture() && !System.isBatch())
                C_AssetSharingCreation.createServiceHistoryShareUsr(uID2);
        }
    }
}