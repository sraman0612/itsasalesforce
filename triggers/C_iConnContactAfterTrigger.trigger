trigger C_iConnContactAfterTrigger on Serial_Number_Contact__c (after insert) {
    if(Trigger.isInsert)
    {
        
        // Create a new list of sharing objects for Job
        List<Serial_Number_Contact__Share> jobShrs  = new List<Serial_Number_Contact__Share>();
        
        
        List<string> contactEmail = new List<string>();
        
        for(Serial_Number_Contact__c s : trigger.new)
        {
            system.debug(s.Email_Address__c );
            contactEmail.add(s.Email_Address__c);
        }
        Map<string, contact> aMap = new Map<string, contact>();
        system.debug(contactEmail);
        list<Contact> aList = [select id, name,email, account.parentID, AccountId  
                             from contact where email IN :contactEmail];
        
        for(Contact a : aList)
        {
            aMap.put(a.email, a);
        }
        
        for(Serial_Number_Contact__c sc : trigger.new)
        {
            if(sc.Relationship__c == 'End User')
            {
                for(Assetshare asH : [select id ,UserOrGroupId from assetshare where assetID = :sc.Serial_Number__c ])
                {
                    Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
                    share.AccessLevel='Edit';
                    share.ParentId=sc.id;
                    share.RowCause='Manual';
                    if(UserInfo.getUserId() !=ash.UserOrGroupId){
                        share.UserOrGroupId=ash.UserOrGroupId;
                        
                        jobShrs.add(share);
                    }
                }
            }
            else if(sc.relationship__c == 'Distributor')
            {
                for(Contact a : aList)
                {
                    //Create sharing for all Single account users
                    if(!string.isBlank(a.AccountId)){
                        for(User u : [Select id,Partner_Account_ID__c from user where  Partner_Account_ID__c = :string.valueOF(a.AccountId).substring(0,15)])
                        {
                            Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
                            share.AccessLevel='Edit';
                            share.ParentId=sc.id;
                            share.RowCause='Manual';
                            if(UserInfo.getUserId() !=u.id){
                                share.UserOrGroupId=u.id;
                                
                                jobShrs.add(share);
                            }
                        }
                    }
                    
                    //Create sharing for Global Account users
                    if(!string.isBlank(a.account.parentID)){
                        for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :string.valueOF(a.account.parentID).substring(0,15)])
                        {
                            Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
                            share.AccessLevel='Edit';
                            share.ParentId=sc.id;
                            share.RowCause='Manual';
                            if(UserInfo.getUserId() !=u.id){
                                share.UserOrGroupId=u.id;
                                
                                jobShrs.add(share);
                            }
                        }
                    }
                    
                    //Create sharing for users who have the Current Servicer ID as their Partner Account ID
                    /*if(!string.isBlank(a.Current_Servicer_ID__c)){
                        for(User u : [Select id, Partner_Account_ID__c from user where Partner_Account_ID__c = :a.Current_Servicer_ID__c ])
                        {
                            Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
                            share.AccessLevel='Edit';
                            share.ParentId=sc.id;
                            share.RowCause='Manual';
                            if(UserInfo.getUserId() !=u.id){
                                share.UserOrGroupId=u.id;
                                
                                jobShrs.add(share);
                            }
                        }
                    }
                    if(!string.isBlank(a.Current_Servicer_Parent_ID__c)){
                        for(User u : [Select id, Partner_Account_Parent_ID__c from user where toLabel(community_user_type__c) = 'Global Account' and Partner_Account_Parent_ID__c  = :string.valueOf(a.Current_Servicer_Parent_ID__c).substring(0,15)])
                        {
                            Serial_Number_Contact__Share share = new Serial_Number_Contact__Share();
                            share.AccessLevel='Edit';
                            share.ParentId=sc.id;
                            share.RowCause='Manual';
                            if(UserInfo.getUserId() !=u.id){
                                share.UserOrGroupId=u.id;
                                
                                jobShrs.add(share);
                            }
                        }
                    }*/
                }
            }
            
        }
        //Insert sharing after loop
        insert jobShrs;
    }
}