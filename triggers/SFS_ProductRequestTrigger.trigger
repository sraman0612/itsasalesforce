/**=========================================================================================================
* @author: Srikanth P, Capgemini
* @date: 07/12/2021
* @description: SFS_ProductRequestTrigger Apex Trigger.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------

====================================================================================================================================================*/

trigger SFS_ProductRequestTrigger on ProductRequest (before insert, before update, after update, after delete, after Undelete) {
    
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('ProductRequest');
    if(skipTrigger!=True){
        
        if(trigger.isbefore){ 
            if(trigger.isInsert){                 
                Profile svcProf =[SELECT Id from Profile where Name ='sfs service technician'];
                User usr;
                 usr = [select Id,Profile.Name from User where Id = :userInfo.getUserId()];
                for(ProductRequest pr:Trigger.New){                    
                    if(usr.Profile.Name!=null){
                        if(usr.Profile.Name.toLowerCase()=='sfs service technician'){
                            pr.addError('You cannot create a Part request');
                        }
                    }
                }
            }
            
            if(trigger.isUpdate){
                SFS_ProductRequestTriggerHandler.createProductTransferAndConsumed(Trigger.New,Trigger.OldMap);
                
            }
            
        } 
        if(trigger.isAfter){ 
            Double oldFreight,newFreight;
            if(trigger.isUpdate){
                for(ProductRequest pr1:Trigger.New){ 
                    ProductRequest oldRecord=Trigger.OldMap.get(pr1.Id);
                    oldFreight=oldRecord.SFS_Freight_Charge__c;
                    newFreight=pr1.SFS_Freight_Charge__c;
                }
                SFS_ProductRequestTriggerHandler.createProductTransferAndConsumed(Trigger.New,Trigger.OldMap);
                if(oldFreight!=newFreight)
                    SFS_ProductRequestTriggerHandler.updateFreightCharge(Trigger.New,Trigger.oldMap);
                SFS_ProductRequestTriggerHandler.updateLatestDates(Trigger.New,Trigger.oldMap);
                SFS_ProductRequestTriggerHandler.createChatterPosttoWOOwner(Trigger.New,Trigger.oldMap);
            }
            
            if(trigger.isDelete){ 
                SFS_ProductRequestTriggerHandler.updateFreightCharge(Trigger.old,null);
            } 
            
            if(trigger.isUndelete){ 
                SFS_ProductRequestTriggerHandler.updateFreightCharge(Trigger.New,null);
            }
        } 
        
    }
    
}