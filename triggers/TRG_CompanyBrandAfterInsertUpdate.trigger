trigger TRG_CompanyBrandAfterInsertUpdate on Company_Brand__c (after insert, after update) {
    List<Company_Brand__c> toProcess = new List<Company_Brand__c>();
    
    if (Trigger.isInsert) {
        for (Company_Brand__c cur : Trigger.new) {
            if (String.isNotBlank(cur.Distributor_Terms_and_Conditions__c)) {
                toProcess.add(cur);
            }
        }
    } else if (Trigger.isUpdate) {
        for (Company_Brand__c cur : Trigger.new) {
            if (cur.Distributor_Terms_and_Conditions__c != Trigger.oldMap.get(cur.Id).Distributor_Terms_and_Conditions__c) {
                toProcess.add(cur);
            }
        }        
    }
    
    if (!toProcess.isEmpty()) {
        UTIL_TermsAndConditions.updateDraftStatusQuotes(toProcess);
    }
}