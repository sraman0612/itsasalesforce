/*=========================================================================================================
* @author Sucharitha Suragala, Capgemini
* @date 22/09/2021
* @description: SFS_EntitlementTrigger Apex Trigger.
Modification Log:
------------------------------------------------------------------------------------
Developer         Mod Number  Date        Description
------------------------------------------------------------------------------------
Sucharitha Suragala     M-001   SIF-47   15/12/2021  Create Maintenance Plans (PM Schedule) for CARE Agreements
====================================================================================================================================================*/
trigger SFS_EntitlementTrigger on Entitlement (after insert) {
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Entitlement');
    if(skipTrigger!=True){
        SFS_EntitlementHandler.EntitlementHandler(Trigger.new);
    }
}