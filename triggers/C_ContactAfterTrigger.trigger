trigger C_ContactAfterTrigger on Contact (after insert, after update) {
    
    List<id> cID = new List<ID>();
    
    if(Trigger.isInsert)
    {
        for(Contact c : Trigger.new)
        {
            if(!string.isBlank(c.accountID))
            {   cID.add(c.id);
                                            
            }
        }
        //Send contact to batch class to delete AND ADD asset share rights
        if(cid.size()>0)
        {
            C_createAssetShareOnContactBatch cBatch = new C_createAssetShareOnContactBatch(null,cID);//initialize batch class(passid)
            id processedBatch = Database.executeBatch(cBatch,20);//execute batch
            //C_AssetSharingCreation.createAssetShareOnContact(cID);
            C_AssetSharingCreation.createServiceContractShareOnContact(cID);
            C_AssetSharingCreation.createLubricantSampleShareOnContact(cID);
        }
        
    }
    if(Trigger.isUpdate)
    {   
        cID = new List<ID>();
        for(Contact c : Trigger.new)
        {
            
            Contact oldCon = Trigger.oldMap.get(c.id);
            // if((c.AccountID != oldCon.AccountID) || (!String.isBlank(c.AccountID)))
            if((c.AccountID != oldCon.AccountID))
            {
                
                cID.add(c.id);
                
            }
        }
        if(cid.size()>0)
        {
            //Send contact to batch class to delete AND ADD asset share rights
            C_createAssetShareOnContactBatch cBatch = new C_createAssetShareOnContactBatch(null,cID);//initialize batch class(passid)
            id processedBatch = Database.executeBatch(cBatch,20);//execute batch
             //C_AssetSharingCreation.createAssetShareOnContact(cID);   
            C_AssetSharingCreation.createServiceContractShareOnContact(cID);
            C_AssetSharingCreation.createLubricantSampleShareOnContact(cID);
        }
    }
}