trigger CaseTrigger on Case(before insert,before update,after insert,after update) {
  /* Code updated by Vineet Capgemini on 11-Oct-21 to update Logic CG_Org_Channel__c on before events*/
  Boolean skipTrigger = CGCommonUtility.sKipTrigger('Case');
  if (skipTrigger != true) {
    List<Case> newCaseList = new List<Case>();
    List<Case> LIR_caseList = new List<Case>();
    List<Case> LGD_CaseList = new List<Case>();
    List<Case> after_LIR_CaseList = new List<Case>();
    List<Case> after_LGD_CaseList = new List<Case>();
    List<Case> sTrchannel = new List<Case>();

    if (Trigger.isbefore) {
      sTrchannel = CGCommonUtility.branchingAndChannelPopulation(Trigger.new);
    }

    for (case c : Trigger.new) {
      if (Trigger.isAfter) {
        if (c.CG_Org_Channel__c != null) {
          if (c.CG_Org_Channel__c == System.Label.CG_GD_Trigger) {
            after_LGD_CaseList.add(c);
          } else if (c.CG_Org_Channel__c == System.Label.CG_IR_Trigger) {
            after_LIR_CaseList.add(c);
          }
        }
      }
    }

    for (Case cs : sTrchannel) {
      if (cs.CG_Org_Channel__c == System.Label.CG_IR_Trigger) {
        LIR_caseList.add(cs);
      } else if (cs.CG_Org_Channel__c == System.Label.CG_GD_Trigger) {
        LGD_CaseList.add(cs);
      }
    }

    switch on Trigger.operationType {
      when BEFORE_INSERT {
        if (LIR_caseList.size() > 0) {
          LIR_Case_triggerHandler.beforeInsert(LIR_caseList);
          //SFS_CaseTriggerHandler.callPolygonUtils(null, Trigger.new);//Nivetha -commmenting for prod validation - 10/14
        }

        if (LGD_CaseList.size() > 0) {
          LGD_Case_TriggerHandler.beforeInsert(LGD_CaseList);
        }
      }
      when BEFORE_UPDATE {
        if (LIR_caseList.size() > 0) {
          LIR_Case_triggerHandler.beforeUpdate(LIR_caseList, Trigger.oldMap);
          //SFS_CaseTriggerHandler.callPolygonUtils(Trigger.oldMap, Trigger.new);//Nivetha -commmenting for prod validation - 10/14
        }

        //if(LGD_CaseList.size() > 0 ) LGD_Case_triggerHandler.beforeUpdate(LGD_CaseList, trigger.oldMap);
        //CaseStatusController.caseError(LIR_caseList,trigger.oldMap);
      }
      when AFTER_INSERT {
        if (LGD_CaseList.size() > 0)
          LGD_Case_TriggerHandler.afterInsert(after_LGD_CaseList);
      }

      /*When AFTER_UPDATE{
            if(LIR_caseList.size() > 0 ) LIR_Case_triggerHandler.afterUpdate(after_LIR_CaseList, trigger.old, trigger.newMap, trigger.oldMap);
            if(LGD_CaseList.size() > 0 ) LGD_Case_TriggerHandler.afterUpdate(after_LGD_CaseList, trigger.old);
            }*/
    }
  }

}