trigger C_LubricantSampleAfterTrigger on Oil_Sample__c (after insert, after update) {
/*Authored By: Jonathan Brooks
*Created: 1/29/2017
*Purpose: Replacing the process builder and flows to create proper sharing on Assets based on Community User Type of User.
*Updated by jonathan brooks 10/16/18 to fix the lube sample share for global users and on update 
* 
* 
*/
    List<Oil_Sample__Share> jobShrs  = new List<Oil_Sample__Share>();
    List<id> shID = new List<id>();
    List<id> ohID = new List<id>();
    List<user> usrList = new List<user>();
    
    if(Trigger.isInsert)
    {    
        for (Oil_Sample__c os : Trigger.new) {
            system.debug('*****OIL SMAPLE***** ' + os.Distributor__c);
            if(!string.isBlank(string.valueOf(os.Distributor__c)))
            {
                usrList = new List<user>();
                usrList = [Select id from user where Partner_Account_ID__c = :string.valueOf(os.Distributor__c).substring(0,15)
                           and toLabel(community_user_type__c) = 'Single Account'];
                if(usrList.size() >0)
                {
                    for(User u: usrList)
                    {
                        Oil_Sample__Share osShare = new Oil_Sample__Share();
                        
                        // Set the ID of record being shared
                        osShare.ParentId = os.Id;
                        
                        // Set the ID of user or group being granted access
                        osShare.UserOrGroupId = u.id;
                        
                        // Set the access level
                        osShare.AccessLevel = 'Read';
                        
                        // Set the Apex sharing reason for hiring manager and recruiter
                        osShare.RowCause = 'Manual';
                        
                        // Add objects to list for insert
                        jobShrs.add(osShare);
                    } 
                }
                if(os.Account_Parent_ID__c != null ){
                    // usrList = [Select id from user where Partner_Account_Parent_ID__c = :string.valueOf(os.Account_Parent_ID__c ).substring(0,15)
                    usrList = [Select id from user where Partner_Account_Parent_ID__c = :string.valueOf(os.Account_Parent_ID__c).substring(0,15)
                               and toLabel(community_user_type__c) = 'Global Account'];
                    if(usrList.size() >0)
                    {
                        for(User u: usrList)
                        {
                            Oil_Sample__Share osShare = new Oil_Sample__Share();
                            
                            // Set the ID of record being shared
                            osShare.ParentId = os.Id;
                            
                            // Set the ID of user or group being granted access
                            osShare.UserOrGroupId = u.id;
                            
                            // Set the access level
                            osShare.AccessLevel = 'Read';
                            
                            // Set the Apex sharing reason for hiring manager and recruiter
                            osShare.RowCause = 'Manual';
                            
                            // Add objects to list for insert
                            jobShrs.add(osShare);
                        } 
                    }
                }
            }          
        }
        if(jobShrs.size()>0)
            insert jobShrs;
    }
    
    if(trigger.isUpdate)
    {
        for (Oil_Sample__c os : Trigger.new) {
            system.debug('*****OIL SMAPLE***** ' + os.Distributor__c);
            if(!string.isBlank(string.valueOf(os.Distributor__c)))
            {
                usrList = new List<user>();
                usrList = [Select id from user where Partner_Account_ID__c = :string.valueOf(os.Distributor__c).substring(0,15)
                           and toLabel(community_user_type__c) = 'Single Account'];
                if(usrList.size() >0)
                {
                    for(User u: usrList)
                    {
                        Oil_Sample__Share osShare = new Oil_Sample__Share();
                        
                        // Set the ID of record being shared
                        osShare.ParentId = os.Id;
                        
                        // Set the ID of user or group being granted access
                        osShare.UserOrGroupId = u.id;
                        
                        // Set the access level
                        osShare.AccessLevel = 'Read';
                        
                        // Set the Apex sharing reason for hiring manager and recruiter
                        osShare.RowCause = 'Manual';
                        
                        // Add objects to list for insert
                        jobShrs.add(osShare);
                    } 
                }
                if(os.Account_Parent_ID__c!= null ){
                    // usrList = [Select id from user where Partner_Account_Parent_ID__c = :string.valueOf(os.Account_Parent_ID__c ).substring(0,15) 
                    usrList = [Select id from user where Partner_Account_Parent_ID__c = :string.valueOf(os.Account_Parent_ID__c ).substring(0,15)
                               and toLabel(community_user_type__c) = 'Global Account'];
                    if(usrList.size() >0)
                    {
                        for(User u: usrList)
                        {
                            Oil_Sample__Share osShare = new Oil_Sample__Share();
                            
                            // Set the ID of record being shared
                            osShare.ParentId = os.Id;
                            
                            // Set the ID of user or group being granted access
                            osShare.UserOrGroupId = u.id;
                            
                            // Set the access level
                            osShare.AccessLevel = 'Read';
                            
                            // Set the Apex sharing reason for hiring manager and recruiter
                            osShare.RowCause = 'Manual';
                            
                            // Add objects to list for insert
                            jobShrs.add(osShare);
                        } 
                    }
                }
            }          
        }
        if(jobShrs.size()>0)
            insert jobShrs;
    }
}