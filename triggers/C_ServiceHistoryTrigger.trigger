trigger C_ServiceHistoryTrigger on Service_History__c (before insert, before update, after insert, after update, before delete) 
{
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate ))
    {
        C_ServHistTrigHelperPartsCount.processPartCounts(Trigger.new);
    }
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate ))
    {
        List<Service_History__c> shUpdate = new List<Service_History__c>();
        List<Service_History__Share> jobShrs  = new List<Service_History__Share>();
        List<user> usrList = new List<user>();
        /*if(trigger.isUpdate)
        {
            for(Service_History__c sh : trigger.new){
                if(ServiceHistoryTriggerHelper.isFirstTime){
                    ServiceHistoryTriggerHelper.isFirstTime = false;
                    Service_History__c oldSH = Trigger.oldMap.get(sh.Id);
                    system.debug('*****SHARE HERE Update***'+ sh);
                    if(!string.isBlank(string.valueOf(sh.account__c)) && oldSH.Account__c == null)
                    {
                        usrList = new List<user>();
                        usrList = [Select id from user where Partner_Account_ID__c = :string.valueOf(sh.account__c).substring(0,15)];
                        if(usrList.size() >0)
                        {
                            for(User u: usrList)
                            {
                                Service_History__Share scShare = new Service_History__Share();
                                
                                // Set the ID of record being shared
                                scShare.ParentId = sh.Id;
                                
                                // Set the ID of user or group being granted access
                                scShare.UserOrGroupId = u.id;
                                
                                // Set the access level
                                scShare.AccessLevel = 'Edit';
                                
                                // Set the Apex sharing reason for hiring manager and recruiter
                                scShare.RowCause = 'Manual';
                                
                                // Add objects to list for insert
                                jobShrs.add(scShare);
                            } 
                        }
                    }    
                }
            }
        }*/
        if(C_ServHistTrigHelperRecLubUsed.runOnce())
        {
            C_ServHistTrigHelperRecLubUsed.servRollupAfterInsUpd(Trigger.newMap);
        }           
    }
    if(Trigger.isBefore && Trigger.isDelete)
    {
        C_ServHistTrigHelperRecLubUsed.servRollupBeforeDel(Trigger.oldMap);
    }
}